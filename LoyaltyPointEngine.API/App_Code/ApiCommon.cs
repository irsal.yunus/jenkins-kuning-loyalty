﻿using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.DataMember;
using System;
using LoyaltyPointEngine.PointLogicLayer;
using System.IO;
using LoyaltyPointEngine.Common;

namespace LoyaltyPointEngine.API
{
    public static class ApiCommon
    {
        public static Pool GetApps(this System.Net.Http.Headers.HttpRequestHeaders param)
        {

            foreach (var header in param)
            {
                if (header.Key.ToLower() == "authorization")
                {
                    foreach (var val in header.Value)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            if (val.ToLower().Contains("bearer"))
                            {

                                var arr = val.Split(' ');
                                if (arr != null && arr.Count() >= 2)
                                {
                                    var apps = PoolMemberToken.GetByToken(arr[1]);
                                    // var users = Member.GetAll();
                                    if (apps != null && apps.Pool != null)
                                    {
                                        return apps.Pool;
                                    }


                                }

                            }
                        }
                    }

                }
            }
            
            return null;

        }
        public static LoyaltyPointEngine.DataMember.Member GetMember(this System.Net.Http.Headers.HttpRequestHeaders param)
        {

            foreach (var header in param)
            {
                if (header.Key.ToLower() == "authorization")
                {
                    foreach (var val in header.Value)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            if (val.ToLower().Contains("bearer"))
                            {

                                var arr = val.Split(' ');
                                if (arr != null && arr.Count() >= 2)
                                {
                                    var poolMemberToken = PoolMemberToken.GetByToken(arr[1]);
                                    if (poolMemberToken != null)
                                    {
                                        return LoyaltyPointEngine.DataMember.Member.GetById(poolMemberToken.MemberId);
                                    }


                                }

                            }
                        }
                    }

                }
            }







            return null;

        }
        public static string GetToken(this System.Net.Http.Headers.HttpRequestHeaders param)
        {

            foreach (var header in param)
            {
                if (header.Key.ToLower() == "authorization")
                {
                    foreach (var val in header.Value)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            if (val.ToLower().Contains("bearer"))
                            {

                                var arr = val.Split(' ');
                                if (arr != null && arr.Count() >= 2)
                                {
                                    return arr[1];
                                }

                            }
                        }
                    }

                }
            }
            return null;

        }
        public static string GetToken(this System.Collections.Specialized.NameValueCollection param)
        {

            foreach (var key in param.AllKeys)
            {

                if (key.ToLower() == "authorization")
                {
                    var val = param[key];
                    if (!string.IsNullOrEmpty(val))
                    {
                        if (val.ToLower().Contains("bearer"))
                        {
                            var arr = val.Split(' ');
                            if (arr != null && arr.Count() >= 2)
                            {
                                return arr[1];
                            }

                        }
                    }


                }
            }
            return "";

        }

        public static void GlobalError(Exception ex, string controller, string action, object parameterValue, string AuthToken)
        {
            string ErrorMessage = ex.Message;
            string StackTrace = ex.StackTrace;
            string ExceptionType = ex.GetType().FullName;


            string errMessage = string.Format("errmsg: {0} , ExceptionType ={1} , StackTrace = {2} ", ErrorMessage,ExceptionType, StackTrace);
            string CurrentUrl = "";

            if (HttpContext.Current != null)
            {
                CurrentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                if (!string.IsNullOrEmpty(AuthToken))
                {
                    if (!CurrentUrl.Contains("?"))
                    {
                        CurrentUrl = CurrentUrl + "?token=" + AuthToken;
                    }
                    else
                    {
                        CurrentUrl = CurrentUrl + "&token=" + AuthToken;
                    }
                }

            }



            var currentController = controller;
            var currentAction = action;


            LoyaltyPointEngine.Common.EventLogLogic.LogSource source = new LoyaltyPointEngine.Common.EventLogLogic.LogSource();
            source = LoyaltyPointEngine.Common.EventLogLogic.LogSource.Global;
            if (!string.IsNullOrEmpty(currentController))
            {
                if (currentController.ToLower() == "Member".ToLower())
                {
                    source = LoyaltyPointEngine.Common.EventLogLogic.LogSource.Member;
                }
                else if (currentController.ToLower() == "Child".ToLower())
                {
                    source = LoyaltyPointEngine.Common.EventLogLogic.LogSource.Child;
                }
                else if (currentController.ToLower() == "Notification".ToLower())
                {
                    source = LoyaltyPointEngine.Common.EventLogLogic.LogSource.Notification;
                }
                else if (currentController.ToLower() == "Access".ToLower())
                {
                    source = LoyaltyPointEngine.Common.EventLogLogic.LogSource.Access;
                }
                else if (currentController.ToLower() == "SalesPerson".ToLower())
                {
                    source = LoyaltyPointEngine.Common.EventLogLogic.LogSource.SalesPerson;
                }
            }


            PointLogicLayer.EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, source, 
                LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Global, errMessage, parameterValue, CurrentUrl, "Api");
        }

        public static void ApiErrorLog(Exception ex, LoyaltyPointEngine.Common.EventLogLogic.LogSource source, object parameterValue, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode eventCode)
        {
            string ErrorMessage = ex.Message;
            string StackTrace = ex.StackTrace;
            string ExceptionType = ex.GetType().FullName;

            string CurrentUrl = "";
            string AuthToken = GetToken(HttpContext.Current.Request.Headers);
            if (HttpContext.Current != null)
            {
                CurrentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                if (!string.IsNullOrEmpty(AuthToken))
                {
                    if (!CurrentUrl.Contains("?"))
                    {
                        CurrentUrl = CurrentUrl + "?token=" + AuthToken;
                    }
                    else
                    {
                        CurrentUrl = CurrentUrl + "&token=" + AuthToken;
                    }
                }

            }
            string errMessage = string.Format("errmsg: {0} , ExceptionType ={1} , StackTrace = {2} ", ErrorMessage, ExceptionType, StackTrace);
            PointLogicLayer.EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, source, eventCode, errMessage, parameterValue, CurrentUrl, "Api");
        }

        public static void createLogCSV(string msg,string phone="all")
        {
            DateTime dt = DateTime.Now;
            string logFailed = "Log_Failed_" + phone + "_" + dt.ToString("yyyyMMdd") + ".txt";
            string pathSource = SiteSetting.PathLogERRORCsv;
            string fullpath = Path.Combine(pathSource, logFailed);
            StreamWriter w = null;
            using (w = File.AppendText(fullpath))
            {
                try
                {
                    if (!File.Exists(fullpath))
                    {
                        File.Create(fullpath).Close();

                    }
                    dt = DateTime.Now;
                    string stroreMsg = string.Format("[{0}]|{1}|", dt, msg);

                    w.WriteLine(stroreMsg);
                    w.Dispose();
                    w.Close();

                }
                catch (Exception ed)
                {
                    if (w != null)
                        w.Dispose();
                    w.Close();
                    throw new Exception("Error Create Log");
                }
            }
        }

        public static Pool GetAppsByClientId(string clientId)
        {

            if (!string.IsNullOrEmpty(clientId))
            {
                var poolData = Pool.GetByClientId(clientId);
                if (poolData != null)
                {
                    return poolData;
                }
            }
            return null;
        }
    }
}