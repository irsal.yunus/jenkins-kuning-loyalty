﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using LoyaltyPointEngine.Data;

namespace LoyaltyPointEngine.API
{
    public class LoyalFilter : ActionFilterAttribute
    {
        public Activity Activities { get; set; }
        public Action RuleAction { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {

            //string[] activities = !string.IsNullOrWhiteSpace(Activities.ToString())
            //                  ? Activities.ToString().Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries)
            //                  : new string[] { };

            //string[] ruleActions = !string.IsNullOrWhiteSpace(RuleAction.ToString())
            //               ? RuleAction.ToString().Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries)
            //               : new string[] { };


            var token = GetBearerToken(actionContext);
            if (!string.IsNullOrEmpty(token))
            {
                var apps = PoolMemberToken.GetByToken(token);

                if (apps != null && apps.Pool != null)
                {
                    //check expired token dulu
                    if(apps.VerificationTokenExpirationDate < DateTime.Now)
                    {
                        actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "token is expired");
                    }

                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "token is not valid");
                }

            }
            else
            {
                actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "token is not valid");
            }

        }


        private static string GetBearerToken(HttpActionContext actionContext)
        {

            foreach (var header in actionContext.Request.Headers)
            {
                if (header.Key.ToLower() == "authorization")
                {
                    foreach (var val in header.Value)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            if (val.ToLower().Contains("bearer"))
                            {

                                var arr = val.Split(' ');
                                if(arr != null && arr.Count() >= 2)
                                {
                                    return arr[1];

                                }

                            }
                        }
                    }

                }
            }


            return "";
        }


        [Flags]
        public enum Activity
        {
            None = 1,
            UserManagement = 2,
            RoleManagement = 4,
            PO = 8,



        }

        [Flags]
        public enum Action
        {
            None = 1,
            Create = 2,
            View = 4,
            Update = 8,
            Delete = 16,
            Print = 32

        }

    }

    public class LoyalSimpleFilter : ActionFilterAttribute
    {
        private bool _IsPublic = false;
        private bool _IsWhiteList = false;
        
        public bool IsPublic
        {
            get
            {
                return _IsPublic;
            }
            set
            {
                _IsPublic = value;
            }
        }

        public bool IsWhiteList
        {
            get
            {
                return _IsWhiteList;
            }
            set
            {
                _IsWhiteList = value;
            }
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                var token = GetBearerToken(actionContext);
                string clientId = "";
                if (!string.IsNullOrEmpty(token))
                {
                    //cek ip whitelist
                    if (IsWhiteList)
                    {
                        string ipAddress = GetIPAddress();
                        WhiteListIP whiteList = WhiteListIP.GetByIP(ipAddress);
                        if (whiteList == null)
                        {
                            actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "Client IP is not registered.");
                            return;
                        }
                    }
                    object parameterValue = actionContext.ActionArguments == null && !actionContext.ActionArguments.Any()
                        ? null
                        : actionContext.ActionArguments.First().Value;

                    var objModel = (parameterValue != null) ? parameterValue.GetType().GetProperty("ClientID") : null;
                    if (objModel != null)
                    {
                        clientId = (string)objModel.GetValue(parameterValue, null);
                    }
                    else
                    {
                        if (actionContext.Request.Method == HttpMethod.Get)
                        {
                            if (actionContext.ActionArguments.Any(x => x.Key == "client_id" || x.Key == "ClientID"))
                            {
                                clientId = (string)actionContext.ActionArguments.Where(x => x.Key == "client_id" || x.Key == "ClientID").First().Value;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(clientId) || string.IsNullOrWhiteSpace(clientId))
                    {
                        var test = actionContext.Request.GetQueryNameValuePairs();
                        clientId = actionContext.Request.GetQueryNameValuePairs().FirstOrDefault(x => x.Key.ToLower() == "clientid").Value;
                    }

                    if (!string.IsNullOrWhiteSpace(clientId))
                    {
                        var poolData = Pool.GetByClientId(clientId);
                        if (poolData != null)
                        {
                            if (poolData.IsPublic != IsPublic)
                            {
                                actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "Unauthorized.");
                            }
                            else
                            {
                                var tokenSystem = string.Format("{0}#{1}", poolData.ClientId, poolData.ClientSecret);
                                var tokenEncript = LoyaltyPointEngine.Common.Encrypt.LoyalEncript(tokenSystem);
                                if (tokenEncript == token)
                                {
                                    //Do something
                                }
                                else
                                {
                                    actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "Token is not matched");
                                }
                            }
                        }
                        else
                        {
                            actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "ClientID is not registered in system");
                        }
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "ClientID is required in parameter");
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.Unauthorized, "token is not valid");
                }
            }
            catch (Exception ex)
            {
                actionContext.Response = actionContext.Request.CreateResponse<string>(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private static string GetBearerToken(HttpActionContext actionContext)
        {

            foreach (var header in actionContext.Request.Headers)
            {
                if (header.Key.ToLower() == "authorization")
                {
                    foreach (var val in header.Value)
                    {
                        if (!string.IsNullOrEmpty(val))
                        {
                            if (val.ToLower().Contains("bearer"))
                            {

                                var arr = val.Split(' ');
                                if (arr != null && arr.Count() >= 2)
                                {
                                    return arr[1];

                                }

                            }
                        }
                    }

                }
            }


            return "";
        }

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}