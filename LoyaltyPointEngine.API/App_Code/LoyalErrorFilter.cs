﻿using LoyaltyPointEngine.PointLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace LoyaltyPointEngine.API
{
    public class LoyalErrorFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var currentController = "";
            var currentAction = "";
            var currentRouteData = context.ActionContext.ControllerContext.RouteData;
            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null &&
                    !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null &&
                    !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

           
            object parameterValue =
                   context.ActionContext.ActionArguments != null && context.ActionContext.ActionArguments.Any() 
                   ?  context.ActionContext.ActionArguments.FirstOrDefault().Value:null;


            var token = ApiCommon.GetToken(context.Request.Headers);

            ApiCommon.GlobalError(context.Exception, currentController, currentAction, parameterValue, token);
        }
    }
}