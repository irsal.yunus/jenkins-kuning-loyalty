﻿
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.DataMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.AccessToken;
using LoyaltyPointEngine.Model.Parameter.AccessToken;

namespace LoyaltyPointEngine.API.Controllers
{

    public class AccessController : BaseController
    {
        /// <summary>
        /// This api for get new token. Please encript your client_secret and password with SHA256 and then encript value with MD5
        /// </summary>
        /// <returns>success if value StatusCode =  '00' </returns>
        public ResultModel<AccessTokenModel> Token(AccessTokenParamModel param)
        {
            ResultModel<AccessTokenModel> result = new ResultModel<AccessTokenModel>();
            try
            {
                if (param != null)
                {
                    if (!string.IsNullOrEmpty(param.client_id) && !string.IsNullOrEmpty(param.client_secret))
                    {
                        var pool = Pool.GetByClientId(param.client_id);
                        if (pool != null)
                        {
                            var ClientSecretEncript = Encrypt.LoyalEncript(pool.ClientSecret);
                            if (ClientSecretEncript == param.client_secret)
                            {
                                //generate new token
                                if ((!string.IsNullOrEmpty(param.username) && (!string.IsNullOrEmpty(param.password)) || (!string.IsNullOrEmpty(param.username) && !string.IsNullOrEmpty(param.OTP))))
                                {
                                    var dataMember = DataMember.Member.GetByPhoneOrEmail(param.username, param.username);
                                    if (dataMember != null)
                                    {
                                        if ((dataMember.Password == param.password) || !string.IsNullOrEmpty(param.OTP))
                                        {
                                            if (dataMember.IsActive && dataMember.IsApproved && !dataMember.IsBlacklist)
                                            {
                                                string token = Guid.NewGuid().ToString();
                                                var dataMemberToken = new PoolMemberToken();
                                                dataMemberToken.ID = Guid.NewGuid();
                                                dataMemberToken.PoolID = pool.ID;
                                                dataMemberToken.MemberId = dataMember.ID;
                                                dataMemberToken.VerificationToken = token;
                                                dataMemberToken.VerificationTokenExpirationDate = param.RememberMe.GetValueOrDefault(false) ? DateTime.Now.AddDays(14) : DateTime.Now.AddMinutes(30);
                                                if (!string.IsNullOrEmpty(param.DeviceID) && !string.IsNullOrEmpty(param.DeviceType))
                                                {
                                                    dataMemberToken.DeviceID = param.DeviceID;
                                                    dataMemberToken.DeviceType = param.DeviceType;
                                                    dataMemberToken.VerificationTokenExpirationDate = DateTime.Now.AddDays(14);
                                                }
                                                dataMemberToken.Inserted();
                                                result.StatusCode = "00";
                                                result.StatusMessage = "success get acces token";
                                                AccessTokenModel model = new AccessTokenModel()
                                                {
                                                    PoolName = pool.PoolName,
                                                    AccessToken = token,
                                                    ExpiredDate = dataMemberToken.VerificationTokenExpirationDate.ToString("dd/MM/yyyy HH:mm:ss"),
                                                    IsApprovedKKImage = dataMember.IsApprovedKKImage,
                                                    IsUploadedKK = !string.IsNullOrEmpty(dataMember.KKImage) ? true : false
                                                };
                                                result.Value = model;
                                            }
                                            else
                                            {
                                                if (dataMember.IsBlacklist)
                                                {
                                                    result.StatusCode = "70";
                                                    result.StatusMessage = SiteSetting.LockingLoginValidationMessage;
                                                }
                                                else
                                                {
                                                    AccessTokenModel model = new AccessTokenModel(dataMember);

                                                    result.StatusCode = "71";
                                                    result.StatusMessage = SiteSetting.NoActiveLoginValidationMessage;
                                                    result.Value = model;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            result.StatusCode = "50";
                                            result.StatusMessage = SiteSetting.WrongEmailPasswordLoginValidationMessage;
                                        }

                                    }
                                    else
                                    {
                                        result.StatusCode = "50";
                                        result.StatusMessage = SiteSetting.WrongEmailPasswordLoginValidationMessage;
                                    }
                                }
                                else
                                {
                                    result.StatusCode = "40";
                                    result.StatusMessage = SiteSetting.UserPassBlankLoginValidationMessage;
                                }
                            }
                            else
                            {
                                result.StatusCode = "32";
                                result.StatusMessage = SiteSetting.IncorrectCSLoginValidationMessage;
                            }
                        }
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = SiteSetting.IncorrectCILoginValidationMessage;
                        }
                    }
                    else
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = SiteSetting.IncorrectCICSLoginValidationMessage;
                    }
                }
                else
                {
                    result.StatusCode = "10";
                    result.StatusMessage = SiteSetting.BlankParamLoginValidationMessage;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Internal server error: {0}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
                ApiCommon.ApiErrorLog(ex, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Access, param, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add);

            }
            return result;
        }

        /// <summary>
        /// This api for get new token. Please encript your client_secret and password with SHA256 and then encript value with MD5
        /// </summary>
        /// <returns>success if value StatusCode =  '00' </returns>
        public ResultModel<AccessTokenModel> TokenByFacebookID(MemberFacebookLoginModel param)
        {
            ResultModel<AccessTokenModel> result = new ResultModel<AccessTokenModel>();
            try
            {
                if (param != null)
                {
                    if (!string.IsNullOrEmpty(param.client_id) && !string.IsNullOrEmpty(param.client_secret))
                    {
                        var pool = Pool.GetByClientId(param.client_id);
                        if (pool != null)
                        {
                            var ClientSecretEncript = Encrypt.LoyalEncript(pool.ClientSecret);
                            if (ClientSecretEncript == param.client_secret)
                            {
                                //generate new token
                                if (!string.IsNullOrEmpty(param.FacebookID))
                                {
                                    var dataMember = DataMember.Member.GetByFacebookId(param.FacebookID);
                                    if (dataMember != null)
                                    {
                                        if (dataMember.IsActive && dataMember.IsApproved && !dataMember.IsBlacklist)
                                        {
                                            string token = Guid.NewGuid().ToString();
                                            var dataMemberToken = new PoolMemberToken();
                                            dataMemberToken.ID = Guid.NewGuid();
                                            dataMemberToken.PoolID = pool.ID;
                                            dataMemberToken.MemberId = dataMember.ID;
                                            dataMemberToken.VerificationToken = token;
                                            dataMemberToken.VerificationTokenExpirationDate = DateTime.Now.AddMinutes(30);
                                            if (!string.IsNullOrEmpty(param.DeviceID) && !string.IsNullOrEmpty(param.DeviceType))
                                            {
                                                dataMemberToken.DeviceID = param.DeviceID;
                                                dataMemberToken.DeviceType = param.DeviceType;
                                                dataMemberToken.VerificationTokenExpirationDate = DateTime.Now.AddDays(14);
                                            }
                                            dataMemberToken.Inserted();
                                            result.StatusCode = "00";
                                            result.StatusMessage = "success get acces token";
                                            AccessTokenModel model = new AccessTokenModel()
                                            {
                                                PoolName = pool.PoolName,
                                                AccessToken = token,
                                                ExpiredDate = dataMemberToken.VerificationTokenExpirationDate.ToString("dd/MM/yyyy HH:mm:ss")
                                            };
                                            result.Value = model;
                                        }
                                        else
                                        {
                                            result.StatusCode = "70";
                                            result.StatusMessage = "Member is non-active/locked. Please contact your administrator";
                                        }
                                    }
                                    else
                                    {
                                        result.StatusCode = "50";
                                        result.StatusMessage = "username or password is not correct.";
                                    }
                                }
                                else
                                {
                                    result.StatusCode = "40";
                                    result.StatusMessage = "username and password should be sent.";
                                }
                            }
                            else
                            {
                                result.StatusCode = "32";
                                result.StatusMessage = "client_secret is incorrect";
                            }
                        }
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = "client_id is incorrect";
                        }
                    }
                    else
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = "client_id and client_secret should be sent.";
                    }
                }
                else
                {
                    result.StatusCode = "10";
                    result.StatusMessage = "parameter is required";
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Internal server error: {0}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
                ApiCommon.ApiErrorLog(ex, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Access, param, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add);
            }
            return result;
        }

        /// <summary>
        /// Extend the expiry time of the token. Please encript your client_secret with SHA256 and then encript value with MD5
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ResultModel<AccessTokenModel> RefreshToken(RefreshTokenParamModel param)
        {
            ResultModel<AccessTokenModel> result = new ResultModel<AccessTokenModel>();
            try
            {
                if (param != null)
                {
                    if (!string.IsNullOrEmpty(param.client_id) && !string.IsNullOrEmpty(param.client_secret))
                    {

                        var pool = Pool.GetByClientId(param.client_id);
                        if (pool != null)
                        {
                            var ClientSecretEncript = Encrypt.LoyalEncript(pool.ClientSecret);
                            if (ClientSecretEncript == param.client_secret)
                            {
                                if (!string.IsNullOrEmpty(param.Token))
                                {
                                    var token = PoolMemberToken.GetByToken(param.Token);
                                    if (token != null)
                                    {
                                        TimeSpan ts = DateTime.Now - token.VerificationTokenExpirationDate;
                                        if (ts.TotalHours > 10)
                                        {
                                            token.DeviceID = string.Empty;
                                            token.DeviceType = string.Empty;
                                            token.Updated();

                                            result.StatusCode = "60";
                                            result.StatusMessage = "Time expired token can not be extended, because the token has been expired more than 1x10 hours";
                                        }
                                        else
                                        {
                                            if (token.TotalRefresh >= 5)
                                            {
                                                token.DeviceID = string.Empty;
                                                token.DeviceType = string.Empty;
                                                token.Updated();

                                                result.StatusCode = "65";
                                                result.StatusMessage = "Time expired token can not be extended, because the token has been refreshed 5 times";
                                            }
                                            else
                                            {
                                                var member = DataMember.Member.GetById(token.MemberId);

                                                if (member != null)
                                                {
                                                    if (member.IsActive)
                                                    {
                                                        token.VerificationTokenExpirationDate = DateTime.Now.AddMinutes(30);
                                                        token.TotalRefresh = token.TotalRefresh + 1;
                                                        token.Updated();

                                                        result.StatusCode = "00";
                                                        result.StatusMessage = "success refresh acces token";
                                                        AccessTokenModel model = new AccessTokenModel()
                                                        {
                                                            PoolName = pool.PoolName,
                                                            AccessToken = param.Token,
                                                            ExpiredDate = token.VerificationTokenExpirationDate.ToString("dd/MM/yyyy HH:mm:ss")
                                                        };
                                                        result.Value = model;
                                                    }
                                                    else
                                                    {
                                                        AccessTokenModel model = new AccessTokenModel(member);

                                                        result.StatusCode = "71";
                                                        result.StatusMessage = SiteSetting.NoActiveLoginValidationMessage;
                                                        result.Value = model;
                                                    }
                                                }
                                                else
                                                {
                                                    result.StatusCode = "50";
                                                    result.StatusMessage = SiteSetting.WrongEmailPasswordLoginValidationMessage;
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        result.StatusCode = "50";
                                        result.StatusMessage = "token is not found in system";
                                    }
                                }
                                else
                                {
                                    result.StatusCode = "40";
                                    result.StatusMessage = "token is required";
                                }

                            }
                            else
                            {
                                result.StatusCode = "32";
                                result.StatusMessage = "client_secret is incorrect";
                            }
                        }
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = "client_id is incorrect";
                        }

                    }
                    else
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = "client_id and client_secret should be sent.";
                    }
                }
                else
                {
                    result.StatusCode = "10";
                    result.StatusMessage = "parameter is required";
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Internal server error: {0}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
                ApiCommon.ApiErrorLog(ex, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Access, param, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add);
            }


            return result;
        }

        /// <summary>
        /// Logout current token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultModel<bool> LogOut()
        {
            ResultModel<bool> result = new ResultModel<bool>();

            result = MemberLogic.LogOff(AccessToken);

            return result;
        }

        /// <summary>
        /// validate if the token is valid or not
        /// </summary>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<bool> Validated()
        {
            return new ResultModel<bool>() { StatusCode = "00", StatusMessage = "Token Valid", Value = true };
        }
    }
}
