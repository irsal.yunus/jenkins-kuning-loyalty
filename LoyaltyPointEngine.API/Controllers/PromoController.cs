﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Promo;
using System;
using System.Collections.Generic;
using LoyaltyPointEngine.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace LoyaltyPointEngine.API.Controllers
{
    public class PromoController : BaseController
    {
       // [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<PromoParamAPI> PromoSMS(PromoParamAPI param)
        {
            string s = new JavaScriptSerializer().Serialize(param);
            new LogPromoSMS().Insert(param.Phone, s);
            string InMSG = string.Format("{0}|{1}|{2}|{3}", param.Phone, param.PromoCode, param.DOBAnak, param.Nama);
            //input Log Promo
            new LogPromoSMS().Insert(param.Phone, InMSG);
            ResultModel<PromoParamAPI> result = new ResultModel<PromoParamAPI>();
            result = PointLogicLayer.Promo.PromoLogic.PromoBySMS(param);
            string OutMSG = string.Format("Success, {0}|{1}|{2}  Status Code :" + result.StatusCode, param.Phone, param.PromoCode, result.StatusMessage);
            //Output Log Promo
            new LogPromoSMS().Insert(param.Phone, OutMSG);
            return result;
        }
    }
}
