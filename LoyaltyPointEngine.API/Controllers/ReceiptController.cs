﻿using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Model.Parameter.QuickWin2;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using LoyaltyPointEngine.PointLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ReceiptController : BaseController
    {
        /// <summary>
        /// Upload Receipt image
        /// Format:
        /// Image --> please using format Base64 string.
        /// TransactionDate --> dd/MM/yyyy HH:mm:ss
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<ReceiptModel> Upload(UploadReceiptModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataPool = ApiCommon.GetApps(Request.Headers);
            ResultModel<ReceiptModel> result = new ResultModel<ReceiptModel>();
            try
            {
                result = ReceiptLogic.Upload(param, dataMember.FirstName, dataMember.ID, dataPool.ID, IsMobileApp);
            }
            catch (Exception ex)
            {
                string message = string.Format("Internal server error: {0}, StackTrace : {1}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message), ex.StackTrace).Substring(0, 4000);
                result.StatusCode = "500";
                result.StatusMessage = message;
                EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Receipt,
                    Common.EventLogLogic.LogEventCode.Add, message, param, Request.RequestUri.AbsoluteUri, "LoyalAPI.Upload");
            }

            return result;
        }


        /// <summary>
        /// Reupload struck if struck reject but need to member fixing it
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<ReceiptModel> ReUpload(ReUploadReceiptModel param)
        {
            ResultModel<ReceiptModel> result = new ResultModel<ReceiptModel>();
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataPool = ApiCommon.GetApps(Request.Headers);
            string url = Request.RequestUri.AbsoluteUri;
            try
            {
                result = ReceiptLogic.ReUpload(param, dataMember.FirstName, dataMember.ID, dataPool.ID, url);
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.Message;
                EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Receipt,
                    Common.EventLogLogic.LogEventCode.Add, ex.StackTrace, param, Request.RequestUri.AbsoluteUri, "LoyalAPI.ReUpload");
            }

            return result;
        }



        /// <summary>
        /// Get member's  Receipts hitory
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<ReceiptDetailModel>> History(ParamReceiptPagination param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return ReceiptLogic.History(dataMember.ID, param);

        }

        /// <summary>
        /// Get detail information of receipt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<ReceiptDetailModel> Get(string id)
        {
            return ReceiptLogic.GetReceiptModelById(id);
        }


        /// <summary>
        /// Get Receipt Detail by Code System
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<ReceiptDetailModel> GetByCode(string id)
        {
            return ReceiptLogic.GetReceiptModelByCode(id);
        }


        [LoyalSimpleFilter(IsPublic = true, IsWhiteList = true)]
        [HttpPost]
        public ResultModel<ReceiptModel> PublicUpload(PublicUploadReceiptModel param)
        {
            ResultModel<ReceiptModel> result = new ResultModel<ReceiptModel>();
            Channel _channel = Channel.Website;
            if (string.IsNullOrEmpty(param.Channel))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Channel is required.";
                return result;
            }
            else
            {
                if (!Enum.TryParse<Channel>(param.Channel, out _channel))
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Invalid Channel.";
                    return result;
                }
            }

            var pool = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            if (param.Channel.Trim().ToLower() != pool.ClientId)
            {
                result.StatusCode = "50";
                result.StatusMessage = "Unmatched Channel.";
                return result;
            }

            try
            {
                result = ReceiptLogic.PublicUpload(param, pool);
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.Message;
                EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Receipt, Common.EventLogLogic.LogEventCode.Add, ex.StackTrace, param, Request.RequestUri.AbsoluteUri, "LoyalAPI.PublicUpload");
            }
            return result;
        }

        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultPaginationModel<List<SP_GetGridReceipt_Result>> PublicGetAll(string channel, string spgCode, int? eventCode, int? status, string startDate, string endDate, string orderColumn, string orderDirection, string search, int page = 1, int pageSize = 10)
        {
            return ReceiptLogic.PublicGetAll(channel, spgCode, eventCode, status, startDate, endDate, orderColumn, orderDirection, search, page, pageSize);
        }

        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultPaginationModel<List<GetReceiptReport_Result>> GetReceiptApprovalReport(string CMSUserID, string startDate, string endDate, string orderColumn, string orderDirection, int page = 1, int pageSize = 10)
        {
            return ReceiptLogic.GetReceiptApprovalReport(CMSUserID, startDate, endDate, orderColumn, orderDirection, page, pageSize);
        }

        [HttpGet]
        public ResultModel<List<string>> GetInactiveMemberReceipt()
        {
            return ReceiptLogic.GetInactiveMemberReceipt();
        }

        [HttpGet]
        public ResultModel<List<BottomPriceReceiptEmailModel>> GetBottomPriceReceiptEmail()
        {
            return ReceiptLogic.GetBottomPriceReceiptEmail(null, null);
        }

        [LoyalFilter]
        [HttpPost]
        public ResultModel<bool> UploadComment(ParamCreateQuickWin2Participation param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataPool = ApiCommon.GetApps(Request.Headers);
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                result = QuickWin2Logic.SaveComment(param, dataMember.ID);
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.Message;
                EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Receipt,
                    Common.EventLogLogic.LogEventCode.Add, ex.StackTrace, param, Request.RequestUri.AbsoluteUri, "LoyalAPI.Upload");
            }

            return result;
        }

    }
}