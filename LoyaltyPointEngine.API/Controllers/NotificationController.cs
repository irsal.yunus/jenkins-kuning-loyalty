﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Notification;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class NotificationController : BaseController
    {
        /// <summary>
        /// Get List notification by memberId and Pagination
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<NotificationModel>> Get([FromBody]ParamPagination param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return NotificationLogic.Get(param, dataMember.ID);
        }

        /// <summary>
        /// Get total unread notification
        /// </summary>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<int> TotalUnread()
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return NotificationLogic.TotalUnread(dataMember.ID);
        }

        /// <summary>
        /// Update notification status to be read (IsRead =1)
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<NotificationModel> Read([FromUri]string Id)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return NotificationLogic.Read(Id,dataMember.ID,dataMember.FirstName);

        }

        /// <summary>
        /// Update multiple notification status to be read (IsRead =1)
        /// </summary>
        /// <param name="Ids">Array of id</param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<List<NotificationModel>> Reads([FromUri]string Ids)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return NotificationLogic.Reads(Ids, dataMember.ID, dataMember.FirstName);
        }

        /// <summary>
        /// Create new notification.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<NotificationModel> Create(ParamCreateNotification param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return NotificationLogic.Create(param, dataMember.ID);
        }

        /// <summary>
        /// Get List fusion notification by memberId and Pagination
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<List<FusionNotificationModel>> GetFusionNotifications([FromBody]ParamPaginationWithDate param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return NotificationLogic.GetFusionNotification(param, dataMember.ID);
        }

        [HttpPost]
        public ResultModel<bool> CreateTelcomate(ParamNotifTelcomate param)
        {
            return NotificationLogic.CreateTelcomate(param);
        }
    }
}