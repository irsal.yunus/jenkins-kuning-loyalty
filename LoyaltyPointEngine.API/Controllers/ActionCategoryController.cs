﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ActionCategoryController : BaseController
    {
        /// <summary>
        /// Get Action category by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<ActionCategoryModel> Get(int id)
        {
            return ActionCategoryLogic.GetActionCategoryModelById(id);
        }

        /// <summary>
        /// get action category by pagination
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultModel<List<ActionCategoryModel>> Search(ParamPagination param)
        {
            return ActionCategoryLogic.Search(param);
        }


    }
}