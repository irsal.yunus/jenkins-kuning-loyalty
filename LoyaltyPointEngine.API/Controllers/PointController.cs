﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.Model.Object.BalanceLog;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.Notification;
using LoyaltyPointEngine.Model.Parameter.Point;
using LoyaltyPointEngine.PointLogicLayer;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.PointLogicLayer.Point;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Transactions;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class PointController : BaseController
    {
        private RavenClient _ravenClient;
        public PointController()
        {
            _ravenClient = new RavenClient("https://a7a32b61af984ae2a214858838cd8080@sentry.io/1731485");
        }

        /// <summary>
        /// Get Total point per user
        /// </summary>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<int> Get()
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return BalanceLogLogic.GetTotalPoint(dataMember.ID);
        }

        /// <summary>
        /// Get member point history (debit and kredit)
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<BalanceLogModel>> History(ParamPaginationWithDate param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return BalanceLogLogic.History(param, dataMember.ID);
        }

        /// <summary>
        /// Add point to login user based on given action code.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<Data.Collect> CreatePointByAction(ParamCreatePointByAction param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            //if (dataMember.ID != param.MemberId)
            //{
            //    return new ResultModel<Data.Collect>() {
            //        StatusCode = "50",
            //        StatusMessage = "Passed member id is not match to the token",
            //        Value = null
            //    };
            //}

            var pointResult = CollectLogic.InsertPointByActionIdAndMemberId(param.ActionCode, dataMember.ID, Request.RequestUri.AbsoluteUri);
            if (pointResult != null)
            {
                if (pointResult.StatusCode == "00")
                {
                    if (param.Notification != null)
                    {
                        ParamCreateNotification notificationParam = param.Notification;
                        var notifResult = NotificationLogic.Create(notificationParam, dataMember.ID);
                        if (notifResult != null)
                        {
                            if (notifResult.StatusCode != "00")
                            {
                                return new ResultModel<Data.Collect>
                                {
                                    StatusCode = notifResult.StatusCode,
                                    StatusMessage = notifResult.StatusMessage
                                };
                            }
                            else
                            {
                                return pointResult;
                            }
                        }
                    }
                    return new ResultModel<Data.Collect>
                    {
                        StatusCode = pointResult.StatusCode,
                        StatusMessage = pointResult.StatusMessage
                    };
                }
            }
            return new ResultModel<Data.Collect>
            {
                StatusCode = pointResult.StatusCode,
                StatusMessage = pointResult.StatusMessage
            };
        }

        //[LoyalSimpleFilter]
        [LoyalFilter]
        [HttpPost]
        public ResultModel<string> Update(UpdatePointModel param)
        {
            
            //var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataMember = DataMember.Member.GetByPhoneOrEmail(param.PhoneNumber, param.Email);
            var datas = PointLogicLayer.Point.CollectLogic.InsertPointUpdateProfile("UpdateProfile", dataMember.ID, Request.RequestUri.AbsoluteUri);
            ResultModel<string> result = new ResultModel<string>();
            if (datas.StatusCode == "00")
            {
                result.Value = "Success";
                result.StatusCode = "00";
            }
            else
            {
                result.Value = "Failed";
                result.StatusCode = "500";

                _ravenClient.Capture(new SentryEvent(new SentryMessage("Failed Update Poin from Update Profile, Message: ", result.StatusMessage)));
            }

            return result;
        }

        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<string> GetByGroupCode(UpdatePointModel param)
        {
            ResultModel<string> result = new ResultModel<string>();
            var data = ActionLogic.GetActionByCode(param.ID);
           
            if (data.StatusCode == "00")
            {
                result.Value = data.Value;
                result.StatusCode = "00";
            }
            else
            {
                result.Value = "Failed";
                result.StatusCode = "500";
            }

            return result;
        }
    }
}