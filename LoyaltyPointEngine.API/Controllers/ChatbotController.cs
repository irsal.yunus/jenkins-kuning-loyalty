﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Chatbot;
using LoyaltyPointEngine.Model.Parameter.Chatbot;
using LoyaltyPointEngine.PointLogicLayer.Chatbot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ChatbotController : BaseController
    {
        /// <summary>
        /// Check the availability of the email or phone number.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<bool> PublicChatbot(ParamChatbotRating param)
        {            
            return ChatbotLogic.ChatbotRatingAdd(param);
        }

    }
}