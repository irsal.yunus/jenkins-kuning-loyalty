﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class CollectController : BaseController
    {
        [HttpGet]
        [LoyalFilter]
        public ResultModel<CollectModel> Get(string ID)
        {
            return CollectLogic.Get(ID);
        }

        [LoyalSimpleFilter(IsPublic = true)]
        [HttpGet]
        public ResultModel<List<object>> PublicGetAllCollectByReceiptId(Guid receiptId)
        {
            return CollectLogic.PublicGetByReceiptId(receiptId);
        }
    }
}