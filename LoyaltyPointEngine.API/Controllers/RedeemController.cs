﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Redeem;
using LoyaltyPointEngine.Model.Parameter.Redeem;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class RedeemController : BaseController
    {
        /// <summary>
        /// Get detail info redemm by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<RedeemDetailModel> Get(string ID)
        {
            return RedeemLogic.Get(ID);
        }

        /// <summary>
        ///  submit redeem by member
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultModel<RedeemDetailModel> Create(SubmitRedeemParameterModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataApps = ApiCommon.GetApps(Request.Headers);
            return RedeemLogic.Create(param, dataMember.ID, dataMember.FirstName, dataApps.ID);
        }
        [HttpPost]
        [LoyalFilter]
        public ResultModel<RedeemDetailModel> SubmitRedeem(SubmitRedeemModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataApps = ApiCommon.GetApps(Request.Headers);
            return RedeemLogic.SubmitRedeem(param, dataMember.ID, dataMember.FirstName, dataApps.ID);
        }

        [LoyalFilter]
        public ResultModel<RedeemDetailModel> Data()
        {
            return new ResultModel<RedeemDetailModel>();
        }

        [HttpPost]
        [LoyalFilter]
        public ResultModel<RedeemModel> Void(string transactionCode)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return VoidLogic.Void(transactionCode, dataMember.ID, dataMember.FirstName);
        }

        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultPaginationModel<List<RedeemCMSViewModel>> GetRedeemReporting(string startDate, string endDate, string orderColumn = "CreatedDate", string orderDirection = "desc", int page = 1, int pageSize = 10)
        {
            return RedeemLogic.GetRedeemReport(startDate, endDate, orderColumn, orderDirection, page, pageSize);
        }

    }
}