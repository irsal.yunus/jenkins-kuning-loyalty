﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class DistrictController : BaseController
    {
        /// <summary>
        /// get list District by pagination and search criteria
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultPaginationModel<List<DistrictModel>> Search(ParamPagination param)
        {
            return DistrictLogic.Search(param);
        }

        /// <summary>
        /// Get District detail by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<DistrictDetailModel> Get(int ID)
        {
           return DistrictLogic.GetDistrictModelById(ID);
        }

        /// <summary>
        /// get list District by zone id
        /// </summary>
        /// <param name="ZoneID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<DistrictModel>> GetByRegionId(int RegionID)
        {
            return DistrictLogic.GetDistrictModelByRegionId(RegionID);
        }
    }
}