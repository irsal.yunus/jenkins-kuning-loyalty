﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ZoneController : BaseController
    {
        /// <summary>
        ///  get list of zone,  base on search criteria and Pagination.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public  ResultPaginationModel<List<ZoneModel>> Search(ParamPagination param)
        {
            return ZoneLogic.Search(param);
        }

        /// <summary>
        /// Get Detail information of zone
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public  ResultModel<ZoneModel> GetStageModelById(int ID)
        {
            return ZoneLogic.GetZoneModelById(ID);
        }
    }
}