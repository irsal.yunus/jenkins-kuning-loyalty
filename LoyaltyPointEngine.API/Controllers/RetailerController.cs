﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class RetailerController : BaseController
    {

        /// <summary>
        /// get list Retailer by pagination and search criteria
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultPaginationModel<List<RetailerModel>> Search(ParamPagination param)
        {
            return RetailerLogic.Search(param);
        }

        /// <summary>
        /// Get Retailer detail by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<RetailerDetailModel> Get(int ID)
        {
            return RetailerLogic.GetRetailerModelById(ID);
        }

        /// <summary>
        /// get list Retailer by City id
        /// </summary>
        /// <param name="CityID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<RetailerModel>> GetByCityId(int CityID)
        {
            return RetailerLogic.GetRetailerModelByCityId(CityID);
        }
    }
}