﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class RegionController : BaseController
    {
        /// <summary>
        /// get list Region by pagination and search criteria
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public  ResultPaginationModel<List<RegionModel>> Search(ParamPagination param)
        {
            return RegionLogic.Search(param);
        }

        /// <summary>
        /// Get Region detail by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public  ResultModel<RegionDetailModel> Get(int ID)
        {
            return RegionLogic.GetRegionModelById(ID);
        }

        /// <summary>
        /// get list region by zone id
        /// </summary>
        /// <param name="ZoneID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public  ResultModel<List<RegionModel>> GetByZoneId(int ZoneID)
        {
            return RegionLogic.GetRegionModelByZoneId(ZoneID);
        }
    }
}