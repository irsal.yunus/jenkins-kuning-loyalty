﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.MemberLogicLayer.Consent;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Parameter.Consent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ConsentController : BaseController
    {
        /// <summary>
        /// Save Cookie Consent
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        //[LoyalFilter] 
        [HttpPost]
        public ResultModel<ConsentCookieMember> Save(ConsentModel param)
        {

            ResultModel<ConsentCookieMember> result = new ResultModel<ConsentCookieMember>();

            try
            {
                result = ConsentLogic.SaveCookieConsent(param);
            }
            catch (Exception ex)
            {

                result.Value = null;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has encountered a problem, please try again later.";
            }

            return result;
        }
    }
}