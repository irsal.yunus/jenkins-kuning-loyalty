﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ChildController : BaseController
    {
        /// <summary>
        /// Add new child base on parent ID
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        //[LoyalFilter(Activities = LoyaltyPointEngine.API.LoyalFilter.Activity.None, RuleAction= LoyaltyPointEngine.API.LoyalFilter.Action.Create)]
        public ResultModel<ChildModel> Add(AddNewChildParameterModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return ChildLogic.Add(param, dataMember.FirstName);
        }
        
        /// <summary>
        /// Get List Of child by ParentID
        /// </summary>
        /// <param name="ParentId"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<ChildModel>> Get(int ParentId)
        {
            return ChildLogic.Get(ParentId);
        }

        /// <summary>
        /// update data child
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultModel<ChildModel> Update(EditChildParameterModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return ChildLogic.Update(param, dataMember.FirstName);
        }

        /// <summary>
        /// Deleted data child by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        [LoyalFilter]
        public ResultModel<ChildModel> Delete(int ID)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return ChildLogic.Delete(ID, dataMember.FirstName);

        }

    }
}