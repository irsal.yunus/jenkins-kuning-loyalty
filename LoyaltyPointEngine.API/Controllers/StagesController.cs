﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class StagesController : BaseController
    {
        /// <summary>
        ///  get list of stages base on search criteria and Pagination.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<StagesModel>> Search([System.Web.Http.FromBody]ParamPagination param)
        {
            return StagesLogic.Search(param);
        }


        /// <summary>
        /// Get Detail information of stages
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<StagesModel> Get(int ID)
        {
            return StagesLogic.GetStageModelById(ID);
        }

        [HttpGet]
        public ResultPaginationModel<List<StagesModel>> GetAll()
        {
            var param = new ParamPagination();
            return StagesLogic.Search(param);
        }

        /// <summary>
        /// Get Detail information of stages
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<StagesModel>> GetStages(int ID)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return StagesLogic.GetStageModel(ID, dataMember);
        }
    }
}