﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ProductBeforeController : BaseController
    {
        /// <summary>
        ///  get list of stages base on search criteria and Pagination.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultPaginationModel<List<ProductBeforeModel>> Search([System.Web.Http.FromBody]ParamPaginationWithClient param)
        {
            return ProductBeforeLogic.Search(param);
        }


        /// <summary>
        /// Get Detail information of stages
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<ProductBeforeModel> Get(int ID)
        {
            return ProductBeforeLogic.GetStageModelById(ID);
        }

    }
}