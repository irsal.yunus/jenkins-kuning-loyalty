﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class CityController : BaseController
    {

        /// <summary>
        /// get list City by pagination and search criteria
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultPaginationModel<List<CityModel>> Search(ParamPagination param)
        {
            return CityLogic.Search(param);
        }

        /// <summary>
        /// Get City detail by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<CityDetailModel> Get(int ID)
        {
            return CityLogic.GetCityModelById(ID);
        }

        /// <summary>
        /// get list City by Area id
        /// </summary>
        /// <param name="AreaID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<CityModel>> GetByAreaId(int AreaID)
        {
            return CityLogic.GetCityModelByAreaId(AreaID);
        }
    }
}