﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Object.BebeJourney;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.PointLogicLayer.Area;
using LoyaltyPointEngine.PointLogicLayer.BebeJourney;

namespace LoyaltyPointEngine.API.Controllers
{
    /// <summary>
    /// Bebe Journey API Collection
    /// </summary>
    public class BebeJouneyController : BaseController
    {
        /// <summary>
        /// Insert Data for Bebe Journey
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultModel<BebeJourneyModel> Add(BebeJourneyModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);

            //return BebeJourneyLogic.Add(param, dataMember.ID, dataMember.FirstName);

            ResultModel<BebeJourneyModel> result = new ResultModel<BebeJourneyModel>();

            try
            {
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    var res = BebeJourneyLogic.Add(param, dataMember.ID, dataMember.FirstName);
                    if (res.StatusCode == "00")
                    {
                        if (!string.IsNullOrEmpty(param.KTPImage) || !string.IsNullOrEmpty(param.AkteImage))
                        {
                            var resultUpdate = MemberLogic.UpdateMemberByBebeJourneyModel(param, dataMember.ID, dataMember.FirstName);
                            res.Value.KTPImage = resultUpdate.Value.KTPImage;
                            res.Value.AkteImage = resultUpdate.Value.AkteImage;
                            if (resultUpdate.StatusCode != "00")
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = resultUpdate.StatusMessage;

                                transScope.Dispose();

                                return result;
                            }
                        }

                    }
                    else
                    {
                        result.StatusCode = res.StatusCode;
                        result.StatusMessage = res.StatusMessage;

                        transScope.Dispose();

                        return result;
                    }

                    transScope.Complete();

                    result.StatusCode = "00";
                    result.StatusMessage = res.StatusMessage;
                    result.Value = res.Value;
                }

            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }

            return result;

            
        }

        /// <summary>
        /// Get by Pagination Bebe Journey
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        public ResultPaginationModel<List<BebeJourneyModel>> GetPagination(int page = 0, int size = 5)
        {
            if (page != 0)
            {
                page -= 1;
            }

            return BebeJourneyLogic.GetPagination(page, size);
        }
    }
}
