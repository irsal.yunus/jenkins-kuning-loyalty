﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.Action;
using LoyaltyPointEngine.PointLogicLayer.Action;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ActionController : BaseController
    {
        /// <summary>
        /// Get Action  by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<ActionModel> Get(int id)
        {
            return ActionLogic.GetActionModelById(id);
        }

        /// <summary>
        /// get action by pagination and ActionCategoryID
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultPaginationModel<List<ActionModel>> Search(ParamActionPagination param)
        {
            return ActionLogic.Search(param);
        }



        /// <summary>
        /// Get List Product Type
        /// </summary>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<List<string>> GetListProductType()
        {
            return ActionLogic.ProductGroup();
        }




        /// <summary>
        /// Get By Group Code
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<ActionModel>> GetByGroupCode(string id)
        {
            return ActionLogic.GetByCode(id);
        }




    }
}