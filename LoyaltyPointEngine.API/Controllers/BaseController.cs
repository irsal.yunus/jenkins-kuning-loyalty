﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Http;
using log4net;

namespace LoyaltyPointEngine.API.Controllers
{
    [LoyalErrorFilter]
    public class BaseController : ApiController
    {
        protected static ILog Log;

        protected void SaveLogError(Exception ex)
        {
            // Saving error to log
            string errorMessage = string.Format("Internal server error: {0}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            SaveLogError(errorMessage);
        }

        protected void SaveLogError(string errorMessage)
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(BaseController));
            }

            // Saving error to log
            Log.Error(errorMessage);
        }

        public string AccessToken
        {
            get
            {
                string token = Request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value.FirstOrDefault();
                if (token.ToLower().Contains("bearer"))
                {
                    return token.Split(' ')[1];
                }
                return token;
            }
        }

        public bool IsMobileApp
        {
            get
            {
                return Request.GetQueryNameValuePairs().Any(x => x.Key.ToLower() == "dvc" && (x.Value.ToLower() == "android" || x.Value.ToLower() == "ios"));
            }
        }

        public string BebeUmbracoAPISecretCode = "B3beS3CRE7C0D3";
    }
}
