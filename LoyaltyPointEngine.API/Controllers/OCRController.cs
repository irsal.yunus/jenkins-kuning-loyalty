﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.OCR;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.OCR;
using LoyaltyPointEngine.PointLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LoyaltyPointEngine.PointLogicLayer.OCR;
using LoyaltyPointEngine.Model.Parameter.Snapcart;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LoyaltyPointEngine.API.Controllers
{
    public class OCRController : BaseController
    {
        public OCRController()
        {
            Log = LogManager.GetLogger(typeof(OCRController));
        }

        [HttpGet]
        public ResultModel<List<OCRTemplateModel>> OCRGetStoreName()
        {
            //var dataMember = ApiCommon.GetMember(Request.Headers);
            //var dataPool = ApiCommon.GetApps(Request.Headers);
            return OCRLogic.OCRGetStoreName();
        }

        /// <summary>
        /// scan ocr string
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ResultModel<OCRResult> OCRScan(ScanResultParameterModel param)
        {
            //var dataMember = ApiCommon.GetMember(Request.Headers);
            //var dataPool = ApiCommon.GetApps(Request.Headers);

            //if(dataMember == null || dataPool == null)
            //{
            //    ResultModel<OCRResult> temp = new ResultModel<OCRResult>();
            //    temp.StatusCode = "01";
            //    temp.StatusMessage = "Header tidak lengkap.";

            //    return temp;
            //}

            return OCRLogic.ScanOcr(param.Value);
        }

        [LoyalFilter]
        [HttpPost]
        public ResultModel<ReceiptDetailModel> Upload(UploadReceiptModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataPool = ApiCommon.GetApps(Request.Headers);
            return ReceiptLogic.UploadByOCR(param, dataMember.FirstName, dataMember.ID, dataPool.ID);
        }

        [HttpPost]
        public ResultModel<bool> SnapcartCallback(JObject param)
        {
            Log.Info("Before Param : " + param.ToString());
            var result = OCRLogic.SnapcartCallback(param);
            Log.Info("After Param : " + JsonConvert.SerializeObject(result));
            return result;
        }
    }
}