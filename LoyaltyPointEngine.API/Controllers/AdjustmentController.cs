﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Adjustment;
using LoyaltyPointEngine.PointLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class AdjustmentController : BaseController
    {
        /// <summary>
        /// Get detail information of Adjustment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpGet]
        public ResultModel<AdjustmentModel> Get(string id)
        {
            return AdjustmentLogic.GetAdjustmentModelById(id);
        }

    }
}