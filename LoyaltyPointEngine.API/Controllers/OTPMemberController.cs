﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.OTP;
using LoyaltyPointEngine.PointLogicLayer;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.OTP;
using System.Transactions;
using LoyaltyPointEngine.Common;
using System.IO;
using Newtonsoft.Json;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.Member;
using log4net;
using Newtonsoft.Json.Linq;

namespace LoyaltyPointEngine.API.Controllers
{
    public class OTPMemberController : BaseController
    {
        static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: OTPMember
        /// <summary>
        /// Generate OTP and Send SMS OTP
        /// </summary>
        /// <param name="Phone">Input</param>
        /// <param name="ClientID">Input</param>
        /// <param name="OldPhone">Input</param
        /// <param name="IsLogin">Input</param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpGet]
        public ResultModel<OTPMemberModel> GenerateOTPMemberRegist(string Phone, string ClientID, string OldPhone, int IsLogin = 0)
        {
            var dataPool = ApiCommon.GetAppsByClientId(ClientID);
            return OTPLogic.OTPMemberRegister(Phone, OldPhone, Request.RequestUri.ToString(), dataPool.ID, dataPool.PoolName, IsLogin);
        }

        /// <summary>
        /// Generate OTP and Send SMS OTP
        /// </summary>
        /// <param name="Phone">Input</param>
        /// <param name="OTP">Input</param>
        /// <param name="ClientID">Input</param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpGet]
        public ResultModel<bool> ValidateOTPMember(string Phone, string OTP, string ClientID)
        {
            var dataPool = ApiCommon.GetAppsByClientId(ClientID);
            var res = OTPLogic.ValidateOTPOnly(Phone, OTP, Request.RequestUri.ToString(), dataPool.ID);
            return res;
        }

        /// <summary>
        /// Generate OTP and Send SMS OTP
        /// </summary>
        /// <param name="Phone">Input</param>
        /// <param name="OTP">Input</param>
        /// <param name="ClientID">Input</param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpGet]
        public ResultModel<bool> ValidateOTPLogin(ChangePhoneModel param)
        {
            var dataPool = ApiCommon.GetAppsByClientId(param.ClientID);
            string Phone = param.NewPhone;
            string OTP = param.OTP;
            var res = OTPLogic.ValidateOTPOnly(Phone, OTP, Request.RequestUri.ToString(), dataPool.ID);

            if (res.Value)
            {
                res = MemberLogic.UpdateMemberActive(param.OldPhone);

                if (res.Value && !param.NewPhone.Equals(param.OldPhone))
                {
                    res = MemberLogic.ChangePhone(param);
                }
            }
            return res;
        }

        
       // [LoyalSimpleFilter]
        [HttpGet]
        public ResultModel<bool> CallbackSMS(JObject param)
        {
            log.Info("Start proccess callbackSMS param : " + param.ToString());
            ResultModel<bool> res = new ResultModel<bool>();
            res = OTPLogic.ValidateCallbackSMS(param, Request.RequestUri.ToString());
            if (res.StatusCode == "00")
            {
                log.Info("End proccess callbackSMS param : " + param.ToString() + " is success with result : "+res.StatusMessage);
            }
            else
            {
                log.Error("End proccess callbackSMS param : " + param.ToString() + " is failed with result : " + JsonConvert.SerializeObject(res));
            }
            return res;
        }

    }
}