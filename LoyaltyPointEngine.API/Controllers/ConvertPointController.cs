﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.ConvertPoint;
using LoyaltyPointEngine.Model.Parameter.ConvertPoint;
using LoyaltyPointEngine.PointLogicLayer;
using System.Collections.Generic;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ConvertPointController : BaseController
    {
        /// <summary>
        /// Get Store for convert
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ResultModel<List<ConvertPointStoreModel>> GetActiveStore()
        {
            return ConvertPointLogic.GetActiveStores();
        }

        /// <summary>
        /// get list City by Area id
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [HttpGet]
        public ResultModel<ConvertPointRuleModel> GetRuleByStoreId(int storeId)
        {
            return ConvertPointLogic.GetConvertPointRuleByStoreId(storeId);
        }

        [HttpGet]
        [LoyalFilter]
        public ResultModel<bool> ValidateStoreMember(long storeId, string userId)
        {
            return ConvertPointLogic.ValidateStoreMembership(storeId, userId);
        }

        [HttpPost]
        [LoyalFilter]
        public ResultModel<long> ConvertPointOut(ParamConvertOut param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            var dataPool = ApiCommon.GetApps(Request.Headers);
            return ConvertPointLogic.CreateConvertOut(dataMember.ID, dataMember.FirstName, dataMember.Phone, param.RuleId, param.Points, dataPool.ID, 0, param.Description, param.DestinationNumber);
        }

        [HttpPost]
        [LoyalFilter]
        public ResultModel<bool> CheckConvertPointCap(ParamConvertOut param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return ConvertPointLogic.CheckConvertPointCap(dataMember.ID, param.RuleId, param.Points);
        }

        [HttpPost]
        [LoyalSimpleFilter]
        public ResultModel<long> ConvertPointIn(ParamConvertIn param)
        {
            var dataPool = ApiCommon.GetAppsByClientId(param.ClientID);
            return ConvertPointLogic.CreateConvertIn(param.phone, param.actionCode, param.points, dataPool.ID, 0, param.description, param.FromNumber);
        }
    }
}