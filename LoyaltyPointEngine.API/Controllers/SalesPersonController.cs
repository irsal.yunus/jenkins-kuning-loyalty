﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class SalesPersonController : BaseController
    {
        /// <summary>
        /// Search list of SalesPerson base on search criteria and Pagination.     
        /// </summary>
        /// <returns></returns>        
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<SalesPersonModel>> Search([System.Web.Http.FromBody]ParamPagination param)
        {
            var result = SalesPersonLogic.GetSalesPersonModelByPagination(param);
            if (result.StatusCode == "500")
            {
                ApiCommon.ApiErrorLog(new Exception(result.StatusMessage), LoyaltyPointEngine.Common.EventLogLogic.LogSource.SalesPerson, param, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.View);
            }

            return result;
        }

        /// <summary>
        /// Get Detail information of SPG with spesific ID include all relation table
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<SalesPersonModel> Get(int ID)
        {
            return SalesPersonLogic.GetSalesPersonModelById(ID);
        }

        /// <summary>
        /// Add new SPG to system.        
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<SalesPersonModel> Add(AddSalesPersonParameterModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return SalesPersonLogic.Add(param, dataMember.FirstName);
        }

        [LoyalFilter]
        [HttpPost]
        public ResultModel<SalesPersonModel> Update(SalesPersonModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return SalesPersonLogic.UpdateSPGBySPGModel(param, dataMember.FirstName);
        }

        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultPaginationModel<List<SalesPersonModel>> PublicGetAll(string search, string orderBy, string orderDirection, int page = 1, int pageSize = 10)
        {
            return SalesPersonLogic.PublicGetAll(search, orderBy, orderDirection, page, pageSize);
        }
    }
}