﻿using LoyaltyPointEngine.Model.Parameter.AccessToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.Member;
using LoyaltyPointEngine.MemberLogicLayer;
using System.Transactions;
using LoyaltyPointEngine.Common;
using System.IO;
using Newtonsoft.Json;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Parameter.UploadKK;
using log4net;
using System.Globalization;

namespace LoyaltyPointEngine.API.Controllers
{
    public class MemberController : BaseController
    {
        public MemberController()
        {
            Log = LogManager.GetLogger(typeof(MemberController));
        }

        /// <summary>
        /// Get list of member base on search criteria and Pagination.
        /// Relation table members will not appear in the results
        /// For all relation used api get by Id
        /// </summary>
        /// <returns></returns>        
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<MemberModel>> Search([System.Web.Http.FromBody] ParamPagination param)
        {
            return MemberLogic.GetMemberModelByPagination(param);
        }

        /// <summary>
        /// Get Detail information of member with spesific ID include all relation table
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<MemberDetailModel> Get(int ID)
        {
            //int a = 1;
            //int b = 0;
            //int c = a / b;
            return MemberLogic.GetMemberModelById(ID);
        }

        /// <summary>
        /// Get list of member base on search criteria.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>

        [LoyalSimpleFilter]
        [HttpGet]
        public ResultModel<List<MemberModel>> GetDataApprovalKK(MemberParameterModel param)
        {
            return MemberLogic.GetMemberModelByPeriod(param);
        }

        /// <summary>
        /// Register new member to system. 
        /// add Authorization header, with value "Bearer your-simple-access-token-here". 
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret). 
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5. 
        /// Please encript password with SHA256 and then encript value with MD5. 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberModel> Add(AddMemberParameterModel param)
        {
            // var dataMember = ApiCommon.GetMember(Request.Headers);
            var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            string url = Request.RequestUri.AbsoluteUri;
            if (string.IsNullOrEmpty(param.Channel))
            {
                param.Channel = Convert.ToString(ChannelType.web);
            }
            var result = MemberLogic.Add(param, clientData.PoolName, url);
            if (result.StatusCode == "00" && result.Value != null)
            {
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(
                        (!string.IsNullOrEmpty(result.Value.FacebookID) ? Common.Point.ActionType.Code.REGFB : Common.Point.ActionType.Code.REGMANUAL),
                        result.Value, url);
                    transScope.Complete();
                }

                PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.FirstName);
            }


            return new ResultModel<MemberModel>()
            {
                StatusCode = result.StatusCode,
                StatusMessage = result.StatusMessage,
                Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null
            };
        }
        /// <summary>
        /// Registering new member
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<MemberModel> PublicAdd(PublicAddMemberModel param)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();
            ChannelType _channel = ChannelType.web;
            if (string.IsNullOrEmpty(param.Channel))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Channel is required.";
                return result;
            }
            else
            {
                if (!Enum.TryParse<ChannelType>(param.Channel.Trim().ToLower(), out _channel))
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Invalid Channel.";
                    return result;
                }
            }

            var pool = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            if (param.Channel.Trim().ToLower() != pool.ClientId.Trim().ToLower())
            {
                result.StatusCode = "50";
                result.StatusMessage = "Unmatched Channel.";
                return result;
            }

            var member = MemberLogic.PublicAdd(param, pool);
            if (member.StatusCode == "00")
            {
                PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.REGMANUAL, member.Value, Request.RequestUri.AbsoluteUri);
                PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(member.Value.ID, param.FirstName);
                result.Value = new MemberModel(member.Value);
            }
            result.StatusCode = member.StatusCode;
            result.StatusMessage = member.StatusMessage;
            return result;
        }

        /// <summary>
        /// Register new member to system.
        /// add Authorization header, with value "Bearer your-simple-access-token-here".
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret).
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5.
        /// Please encript password with SHA256 and then encript value with MD5.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<MemberModel> PublicAddSortRegister(MemberRegisterModelWithUtm param)
        {
            // var dataMember = ApiCommon.GetMember(Request.Headers);
            LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.ApiCall, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Add, "PublicAddSortRegister", param, this.Request.RequestUri.AbsoluteUri, param.ClientID);

            ResultModel<MemberModel> resultModel = new ResultModel<MemberModel>();
            try
            {
                var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
                string url = Request.RequestUri.AbsoluteUri;
                var result = MemberLogic.AddSortRegister(param, clientData.PoolName, url);
                if (result.StatusCode == "00" && result.Value != null)
                {
                    var actionPoint = Common.Point.ActionType.Code.REGMANUAL;
                    if (result.Value.SpgID.HasValue)
                    {
                        actionPoint = Common.Point.ActionType.Code.REGNBA;
                    }

                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actionPoint, result.Value, url);
                        transScope.Complete();
                    }

                    PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Firstname);
                    string title = "Selamat Datang di Bebeclub";
                    string message = "Anda Mendapatkan point Registrasi";
                    var dataAction = Data.Action.GetByCode(Convert.ToString(actionPoint));
                    var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", dataAction.Points, result.Value.FirstName);
                    if (param.Channel.ToLower() == "woozin")
                    {
                        #region 1 Million Participations
                        var model1million = new Model.Parameter.Participations.ParticipationsModel();
                        model1million.NameUser = param.Firstname + " " + param.Lastname;
                        model1million.Phone = param.Phone;
                        model1million.ChildDOB = param.ChildDOB;
                        model1million.Channel = "Event";
                        model1million.UserId = clientData.ID;
                        model1million.Description = "Insert by Register channel Woozin with Member ID" + result.Value.ID.ToString();
                        model1million.By = clientData.PoolName;

                        var million = RESTHelper.Post<ResultModel<Model.Object.Participations.ResultParticipationsModel>>(Data.Settings.GetValueByKey("RewardsAPI") + "/Participations/AddParticipationsJson", model1million);
                        #endregion
                    }
                }

                //return new ResultModel<MemberModel>()
                //{
                //    StatusCode = result.StatusCode,
                //    StatusMessage = result.StatusMessage,
                //    Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null
                //};
                resultModel.StatusCode = result.StatusCode;
                resultModel.StatusMessage = result.StatusMessage;
                resultModel.Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null;

                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.ApiResult, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Add, "PublicAddSortRegister Result", resultModel, this.Request.RequestUri.AbsoluteUri, param.ClientID);
            }
            catch (Exception ex)
            {
                resultModel.StatusCode = "500";
                resultModel.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Add, ex.InnerException.ToString(), param, this.Request.RequestUri.AbsoluteUri, param.ClientID);
            }

            return resultModel;
        }

        /// <summary>
        /// Check the availability of the email or phone number.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<bool> PublicIsExist(PublicCheckMember param)
        {
            return MemberLogic.IsExist(param.PhoneOREmail);
        }
        /// <summary>
        /// Check the availability of the email or phone number.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<List<string>> PublicIsExistWithChildName(PublicCheckMember param)
        {
            var ret = MemberLogic.IsExistWithChildName(param.PhoneOREmail);
            //if (param.ClientID.ToLower() == "woozin" && ret.StatusCode == "00")
            //{
            //    var IsExist = DataMember.Member.GetByPhoneNew(param.PhoneOREmail);
            //    #region 1 Million Participations
            //    var model1million = new Model.Parameter.Participations.ParticipationsModel();
            //    model1million.NameUser = IsExist.FirstName + " " + IsExist.LastName;
            //    model1million.Phone = param.PhoneOREmail;
            //    //model1million.ChildName = ret.Value == "nama anak kosong" ? null : ret.Value;
            //    model1million.ChildName = ret.Value.FirstOrDefault();
            //    model1million.ChildDOB = IsExist.Child.FirstOrDefault().Birthdate.Value.ToString("dd/MM/yyyy");
            //    model1million.Channel = "Event";
            //    model1million.UserId = 14;
            //    model1million.Description = "Insert by Check Phone channel Woozin with Member ID" + IsExist.ID.ToString();
            //    model1million.By = param.ClientID;

            //    var million = RESTHelper.Post<ResultModel<Model.Object.Participations.ResultParticipationsModel>>(Data.Settings.GetValueByKey("RewardsAPI") + "/Participations/AddParticipationsJson", model1million);
            //    #endregion
            //}
            return ret;
        }

        /// <summary>
        /// Check the availability of the email or phone number.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<MemberDetailModel> PublicGetMemberDetailByPhone(MemberGetDetailModel param)
        {
            return MemberLogic.GetMemberModelByPhoneNo(param.Phone);
        }

        /// <summary>
        /// Register new member to system. 
        /// add Authorization header, with value "Bearer your-simple-access-token-here". 
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret). 
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5. 
        /// Please encript password with SHA256 and then encript value with MD5. 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberModel> AddSortRegister(MemberRegisterModelWithUtm param)
        {
            ResultModel<DataMember.Member> result = new ResultModel<DataMember.Member>();

            try
            {
                // var dataMember = ApiCommon.GetMember(Request.Headers);
                var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
                string url = Request.RequestUri.AbsoluteUri;
                result = MemberLogic.AddSortRegister(param, clientData.PoolName, url);
                if (result.StatusCode == "00" && result.Value != null)
                {
                    var actionPoint = Common.Point.ActionType.Code.REGMANUAL;
                    if (result.Value.SpgID.HasValue)
                    {
                        actionPoint = Common.Point.ActionType.Code.REGNBA;
                    }

                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actionPoint, result.Value, url);
                        transScope.Complete();
                    }

                    PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Firstname);
                    string title = "Selamat Datang di Bebeclub";
                    string message = "Anda Mendapatkan point Registrasi";
                    var dataAction = Data.Action.GetByCode(Convert.ToString(actionPoint));
                    var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", dataAction.Points, result.Value.FirstName);
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("Internal server error: {0}, StackTrace : {1}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message), ex.StackTrace).Substring(0, 4000);
                result.StatusCode = "500";
                result.StatusMessage = message;
                PointLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Member,
                    Common.EventLogLogic.LogEventCode.Add, message, param, Request.RequestUri.AbsoluteUri, "LoyalAPI.Add.AddSortRegister");
            }

            return new ResultModel<MemberModel>()
            {
                StatusCode = result.StatusCode,
                StatusMessage = result.StatusMessage,
                Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null
            };
        }

        // <summary>
        /// Register new member to system using Facebook. 
        /// add Authorization header, with value "Bearer your-simple-access-token-here". 
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret). 
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5. 
        /// Please encript password with SHA256 and then encript value with MD5. 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberModel> AddFacebookRegister(MemberRegisterModel param)
        {
            var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            string url = Request.RequestUri.AbsoluteUri;
            var result = MemberLogic.AddFacebookRegister(param, clientData.PoolName, url);
            if (result.StatusCode == "00" && result.Value != null)
            {
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.REGFB,
                        result.Value, url);
                    transScope.Complete();
                }

                PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Firstname);

                string title = "Selamat Datang di Bebeclub";
                string message = "Anda Mendapatkan point Registrasi";
                var dataAction = Data.Action.GetByCode(Common.Point.ActionType.Code.REGFB.ToString());
                var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", dataAction.Points, result.Value.FirstName);
            }


            return new ResultModel<MemberModel>()
            {
                StatusCode = result.StatusCode,
                StatusMessage = result.StatusMessage,
                Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null
            };
        }

        /// <summary>
        /// Register new member to system from mobile device.
        /// add Authorization header, with value "Bearer your-simple-access-token-here".
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret).
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5.
        /// Please encript password with SHA256 and then encript value with MD5.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberModel> AddMobileRegister(MobileRegisterModel param)
        {
            var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            string url = Request.RequestUri.AbsoluteUri;
            var result = MemberLogic.AddMobileRegister(param, clientData.PoolName, url);
            if (result.StatusCode == "00" && result.Value != null)
            {
                var actionPoint = Common.Point.ActionType.Code.REGMANUALMOBILE;
                if (result.Value.SpgID.HasValue)
                {
                    actionPoint = Common.Point.ActionType.Code.REGNBAMOBILE;
                }

                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actionPoint, result.Value, url);
                    transScope.Complete();
                }

                PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Firstname);
                string title = "Selamat Datang di Bebeclub";
                string message = "Anda Mendapatkan point Registrasi";
                var dataAction = Data.Action.GetByCode(Convert.ToString(actionPoint));
                var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", dataAction.Points, result.Value.FirstName);
            }

            return new ResultModel<MemberModel>()
            {
                StatusCode = result.StatusCode,
                StatusMessage = result.StatusMessage,
                Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null
            };
        }

        /// <summary>
        /// Register new member to system from mobile device by facebook.
        /// add Authorization header, with value "Bearer your-simple-access-token-here".
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret).
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5.
        /// Please encript password with SHA256 and then encript value with MD5.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberModel> AddMobileFacebookRegister(MobileRegisterFacebookModel param)
        {
            var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            string url = Request.RequestUri.AbsoluteUri;
            var result = MemberLogic.AddMobileFacebookRegister(param, clientData.PoolName, url);
            if (result.StatusCode == "00" && result.Value != null)
            {
                var actionPoint = Common.Point.ActionType.Code.REGFACEBOOKMOBILE;

                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actionPoint, result.Value, url);
                    transScope.Complete();
                }

                PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Firstname);
                string title = "Selamat Datang di Nutriclub";
                string message = "Anda Mendapatkan point Registrasi";
                var dataAction = Data.Action.GetByCode(Convert.ToString(actionPoint));
                var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", dataAction.Points, result.Value.FirstName);
            }

            return new ResultModel<MemberModel>()
            {
                StatusCode = result.StatusCode,
                StatusMessage = result.StatusMessage,
                Value = result.StatusCode == "00" ? new MemberModel(result.Value) : null
            };
        }

        /// <summary>
        /// Update data member.
        /// Please encript password with SHA256 and then encript value with MD5. 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<MemberModel> Update(EditMemberParameterModel param)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();

            try
            {
                // Get data member
                var dataMember = ApiCommon.GetMember(Request.Headers);
                if (dataMember == null)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = "Data member not found";

                    return result;
                }

                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    // Save data member
                    var res = MemberLogic.UpdateMemberByMemberModel(param, dataMember.FirstName, dataMember.ID);
                    if (res.StatusCode == "00")
                    {
                        // Get data member
                        var memberPoint = Data.Member.GetById(dataMember.ID);
                        if (memberPoint != null && !memberPoint.IsDeleted)
                        {
                            // Update point member
                            var Before = new
                            {
                                memberPoint.Name,
                                memberPoint.Email,
                                memberPoint.Phone
                            };
                            memberPoint.Name = !string.IsNullOrEmpty(param.FirstName) ? string.Format("{0} {1}", param.FirstName, param.LastName) : memberPoint.Name;
                            memberPoint.Email = !string.IsNullOrEmpty(param.Email) ? param.Email : memberPoint.Email;
                            memberPoint.Phone = !string.IsNullOrEmpty(param.Phone) ? param.Phone : memberPoint.Phone;
                            var resultUpdate = memberPoint.Update(memberPoint.Name, "", null, Before);
                            if (!resultUpdate.Success)
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = resultUpdate.ErrorMessage;

                                transScope.Dispose();

                                return result;
                            }
                        }
                        else
                        {
                            result.StatusCode = "500";
                            result.StatusMessage = "Data member not found";

                            transScope.Dispose();

                            return result;
                        }
                    }
                    else
                    {
                        result.StatusCode = res.StatusCode;
                        result.StatusMessage = res.StatusMessage;

                        transScope.Dispose();

                        return result;
                    }

                    transScope.Complete();

                    result.StatusCode = "00";
                    result.StatusMessage = res.StatusMessage;
                    result.Value = res.Value;
                }

            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }

            return result;
        }

        /// <summary>
        /// Add data anak.
        /// Please encript password with SHA256 and then encript value with MD5.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<bool> AddChildOnly(AddChildOnlyParameterModel param)
        {
            LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.ApiCall, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Add, "AddChildOnly", param, this.Request.RequestUri.AbsoluteUri, param.ClientID);

            ResultModel<bool> result = new ResultModel<bool>();
            var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            //var member = LoyaltyPointEngine.Data.Member.GetByPhone(param.Phone);
            var member = DataMember.Member.GetByPhone(param.Phone);

            try
            {
                if (param.Child != null && param.Child.Count > 1)
                {
                    // Validasi jika anak yg sama diinput lebih dari sekali
                    var sameChilds = param.Child.GroupBy(x => new { Name = x.Name.Trim().ToLower(), x.Birthdate })
                      .Where(x => x.Count() > 1)
                      .Select(x => new { Element = x.Key, Counter = x.Count() })
                      .ToList();

                    if (sameChilds.Count > 0)
                    {
                        result.StatusCode = "32";
                        result.StatusMessage = string.Format("Cannot add the same child data");
                        return result;
                    }
                }

                // Validasi jika anak diinput sudah pernah di register sebelumnya
                if (param.Child != null || param.Child.Count() > 0)
                {
                    foreach (var item in param.Child)
                    {
                        var birthDate = DateTime.ParseExact(item.Birthdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        var child = DataMember.Child.GetByParentId(member.ID).Where(x => x.Name.ToLower() == item.Name.ToLower() && x.Birthdate == birthDate);
                        if (child.ToList().Count() > 0)
                        {
                            result.StatusCode = "32";
                            result.StatusMessage = string.Format("Child {0} is already registered", item.Name);
                            return result;
                        }
                    }
                }

                result = MemberLogic.AddChildOnly(param.Child, clientData.PoolName, member.ID);

                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.ApiResult, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Add, "AddChildOnly Result", result, this.Request.RequestUri.AbsoluteUri, param.ClientID);
            }
            catch (Exception ex)
            {
                result.Value = false;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Add, ex.InnerException.ToString(), param, this.Request.RequestUri.AbsoluteUri, clientData.PoolName);
            }

            return result;
        }

        /// <summary>
        /// Set AcceptDate field on Member table with current date and time.
        /// </summary>
        /// <param name="param"></param>
        /// <returns>ResultModel</returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<MemberModel> AgreeToTermsAndConditions(EditAcceptDateModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();

            try
            {
                result = MemberLogic.UpdateMemberAcceptDate(dataMember.ID, param);
            }
            catch (Exception ex)
            {
                result.Value = null;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Edit, ex.StackTrace, null, this.Request.RequestUri.AbsoluteUri, "LoyalAPI.GenerateForgotPasswordToken");
            }

            return result;
        }

        [HttpPost]
        public ResultModel<MemberModel> UpdateStatVendor(UpdateStatVen param)
        {

            int sts = 2;
            var res = MemberLogic.UpdateMemberIsVendor(param.phone, sts, "SYS", param.vendor);
            ResultModel<MemberModel> _return = new ResultModel<MemberModel>();
            if (res.Success)
            {
                string msg = "";
                switch (param.vendor)
                {
                    case "1":
                        msg = SiteSetting.msgToPonta;
                        break;
                    case "2":
                        msg = "";
                        break;
                    default:
                        msg = "no vendor";
                        break;
                }
                MemberModel member = new MemberModel();
                var _member = LoyaltyPointEngine.DataMember.Member.GetByPhone(param.phone);

                member.Phone = _member.Phone;
                member.responsemsg = msg;
                member.IsAlfamart = Convert.ToInt32(_member.IsAlfamart.Value);
                _return.StatusCode = "00";
                _return.StatusMessage = msg;
                _return.Value = member;
            }
            else
            {
                _return.StatusCode = "500";
                _return.StatusMessage = res.ErrorMessage;
            }

            return _return;
        }

        /*
        //[LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberModel> UpdateStatVendor(string phone, string vendor)
        {
            
            int sts = 2;
            var res=MemberLogic.UpdateMemberIsVendor(phone,sts,"SYS",vendor);
            ResultModel<MemberModel> _return = new ResultModel<MemberModel>();
            if (res.Success)
            {
                string msg = "";
                switch (vendor)
                {
                    case "1":
                        msg = SiteSetting.msgToPonta;
                        break;
                    case "2":
                        msg = "";
                        break;
                    default:
                        msg = "no vendor";
                        break;
                }
                MemberModel member = new MemberModel();
                var _member = LoyaltyPointEngine.DataMember.Member.GetByPhone(phone);

                member.Phone = _member.Phone;
                member.responsemsg = msg;
                member.IsAlfamart = _member.IsAlfamart;
                _return.StatusCode = "200";
                _return.StatusMessage = "success";
                _return.Value = member;
            }
            else {
                _return.StatusCode ="500";
                _return.StatusMessage = res.ErrorMessage;
            }

                return _return;
        }


        */
        /// <summary>
        /// Register new member via SMS
        /// add Authorization header, with value "Bearer your-simple-access-token-here". 
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientId + '#' + ClientSecret). 
        /// Please encript "your-simple-access-token-here" with HMACSHA1.     
        /// </summary>
        /// <param name="param"></param>
        /// <returns>Send new password</returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<MemberSMSModel> Sms(AddMemberSMSParameterModel param)
        {
            string cm = "                ";
            string tsMSG = string.Format("{0}|{1}|{2}|{3}", (param.Phone + cm).Substring(0, 15), param.SMSID, param.ExBrand, param.SPGCode);

            ResultModel<MemberSMSModel> rsModel = new ResultModel<MemberSMSModel>();

            try
            {
                string url = Request.RequestUri.AbsoluteUri;
                var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);

                string newPassword = "";
                var result = MemberLogic.Sms(param, clientData.PoolName, out newPassword, url);
                if (result.StatusCode == "00")
                {
                    tsMSG = string.Format("Success, {0}|{1}|{2}", tsMSG, result.StatusCode, result.StatusMessage);
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        if (string.IsNullOrEmpty(param.SPGCode))
                        {
                            PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.REGSMS, result.Value, url);
                        }
                        else
                        {
                            PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.REGNBA, result.Value, url);
                        }

                        transScope.Complete();
                    }

                    PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Name);
                    rsModel.Value = new MemberSMSModel(result.Value, newPassword);

                    if (string.IsNullOrEmpty(param.SPGCode) ? false : param.SPGCode.ToUpper() == "GMD")
                    {
                        rsModel.Value.EventMsg = SiteSetting.msgGMD;
                        rsModel.Value.EventType = param.SPGCode.ToUpper();
                    }
                }
                else
                {
                    tsMSG = string.Format("Failed, {0}|{1}|{2}", tsMSG, result.StatusCode, result.StatusMessage);

                }

                new LogRegistrasiSMS().Insert(param.Phone, tsMSG);
                //ApiCommon.createLogCSV(tsMSG, param.Phone);
                rsModel.StatusCode = result.StatusCode;
                rsModel.StatusMessage = result.StatusMessage;

                if (rsModel.StatusCode == "500")
                    rsModel.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later";

                return rsModel;
                /*
                return new ResultModel<MemberSMSModel>()
                {
                    StatusCode = result.StatusCode,
                    StatusMessage = result.StatusMessage,
                    Value = result.StatusCode == "00" ? new MemberSMSModel(result.Value, newPassword) : null
                };*/
            }
            catch (Exception ed)
            {
                tsMSG = string.Format("{0}|{1}", tsMSG, ed.Message + "|" + (ed.InnerException != null ? ed.InnerException.Message : ed.StackTrace));
                new LogRegistrasiSMS().Insert(param.Phone, tsMSG);
                //ApiCommon.createLogCSV(tsMSG, param.Phone);
                return new ResultModel<MemberSMSModel>()
                {
                    StatusCode = "500",
                    StatusMessage = ed.Message + "\r\n" + (ed.InnerException != null ? ed.InnerException.Message : ed.StackTrace),
                    //StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later"

                };
            }

        }
        /// <summary>
        /// Scheduler SMS
        /// add Authorization header, with value "Bearer your-simple-access-token-here".
        /// generated "your-simple-access-token-here" with a combination of ClientId  ( ClientId ).
        /// Please encript "your-simple-access-token-here" with HMACSHA1.
        /// </summary>
        /// <param name="param"></param>
        /// <returns>Send new password</returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<string> Scheduler(ApiScheduler param)  ///By Ucup
        {
            ResultModel<string> ret = new ResultModel<string>();
            try
            {
                var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);

                return BlastSMS.MemberSMS(param.ListMember);  //Kirim ke Memberlogic.BlastSMS

            }
            catch (Exception ed)
            {
                ret.Value = string.Format("{0}|{1}", ed.Message + "|" + (ed.InnerException != null ? ed.InnerException.Message : ed.StackTrace));
                ret.StatusCode = "500";
                ret.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                return ret;
            }
        }
        /// <summary>
        /// Get user email by forgot password token
        /// </summary>
        /// <param name="param"></param>
        /// <returns>Return User Email</returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<string> EmailByForgotPasswordToken(EmailByForgotPasswordTokenParamModel param)
        {
            return MemberLogic.GetEmailByForgotPasswordToken(param);
        }

        /// <summary>
        /// Generate Token Validation forgot password and send it via email or sms
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<bool> GenerateForgotPasswordToken(ForgotPasswordByTokenModel param)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                result = MemberLogic.GenerateForgotPasswordToken(param);
            }
            catch (Exception ex)
            {
                result.Value = false;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Edit, ex.StackTrace, param, this.Request.RequestUri.AbsoluteUri, "LoyalAPI.GenerateForgotPasswordToken");
            }
            return result;
        }

        /// <summary>
        /// Check Validation Token is valid or not For Forgotten password
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [LoyalSimpleFilter]
        [HttpGet]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public ResultModel<bool> VerifyForgotPasswordToken(string id, string client_id)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                result = MemberLogic.VerifyForgotPasswordToken(id);
            }
            catch
            {
                result.Value = false;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
            }
            return result;
        }

        /// <summary>
        /// Change and reset password using Password Forgotten Token
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<bool> ChangeResetPassword(ChangeResetPasswordModel param)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                result = MemberLogic.ChangeResetPassword(param, BebeUmbracoAPISecretCode);
            }
            catch (Exception ex)
            {
                result.Value = false;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Edit, ex.StackTrace, param, this.Request.RequestUri.AbsoluteUri, "LoyalAPI.ChangeResetPassword");
            }
            return result;
        }

        /// <summary>
        /// Set user forgot password token
        /// </summary>
        /// <param name="param"></param>
        /// <returns>Return User Email</returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<bool> SetForgotPasswordTokenByEmail(SetForgotPasswordTokenByEmailParamModel param)
        {
            return MemberLogic.SetForgotPasswordTokenByEmail(param);
        }

        /// <summary>
        /// Member Detail by token
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<MemberDetailModel> Profile()
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return MemberLogic.GetMemberModelById(dataMember.ID);
        }

        /// <summary>
        /// Check whether member has agreed with terms and conditions or not.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<bool> IsAgreeWithTermsAndConditions()
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return MemberLogic.IsAgreeWithTermsAndConditions(dataMember.ID);
        }

        /// <summary>
        /// Changing Member's Password
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<bool> ChangePassword(ChangePasswordModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return MemberLogic.ChangePassword(param, dataMember.ID);
        }

        /// <summary>
        /// Changing Member's Password
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<bool> ChangePasswordNoOldPassword(ChangePasswordModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            return MemberLogic.ChangePasswordNoOldPassword(param, dataMember.ID);
        }

        /// <summary>
        /// Register new member to system. 
        /// add Authorization header, with value "Bearer your-simple-access-token-here". 
        /// generated "your-simple-access-token-here" with a combination of ClientId and ClientSecret ( ClientSecret + '#' + ClientSecret). 
        /// Please encript "your-simple-access-token-here" with SHA256 and then encript value with MD5. 
        /// Please encript password with SHA256 and then encript value with MD5. 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<bool> AddLongRegister(MemberLongRegisterModel param)
        {
            // var dataMember = ApiCommon.GetMember(Request.Headers);
            var clientData = LoyaltyPointEngine.Data.Pool.GetByClientId(param.ClientID);
            string url = Request.RequestUri.AbsoluteUri;


            var result = MemberLogic.AddLongRegister(param, clientData.PoolName, url);

            /**
             * status Code 00 for insert new data member
             * status Code 000 for update new data member
             **/
            if (result.StatusCode == "00" && result.Value != null)
            {
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(
                        (!string.IsNullOrEmpty(result.Value.FacebookID) ? Common.Point.ActionType.Code.REGFB : Common.Point.ActionType.Code.REGMANUAL),
                        result.Value, url);
                    transScope.Complete();
                }

                PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, param.Firstname);

                string title = "Selamat Datang di Bebeclub";
                string message = "Anda Mendapatkan point Registrasi";
                var dataAction = Data.Action.GetByCode(Common.Point.ActionType.Code.REGMANUAL.ToString());
                var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", dataAction.Points, result.Value.FirstName);

            }


            return new ResultModel<bool>()
            {
                StatusCode = result.StatusCode,
                StatusMessage = result.StatusMessage,
                Value = result.StatusCode == "00" ? true : false
            };
        }

        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultPaginationModel<List<MemberGridModel>> PublicGetAll(string search, string orderBy, string orderDirection, int page = 1, int pageSize = 10)
        {
            return MemberLogic.PublicGetAll(search, orderBy, orderDirection, page, pageSize);
        }

        /// <summary>
        /// Delete member if opt_out
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalSimpleFilter(IsPublic = true)]
        [HttpPost]
        public ResultModel<string> MemberOptOut(MemberOptOutModel param)
        {
            ResultModel<string> result = new ResultModel<string>();
            result = MemberLogic.MemberOptOut(param.Phone);
            return result;
        }

        /// <summary>
        /// Set AcceptDate field on Member table with current date and time.
        /// </summary>
        /// <param name="param"></param>
        /// <returns>ResultModel</returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<MemberModel> EditRatingFlag(EditRatingDateModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();

            try
            {
                result = MemberLogic.UpdateMemberRatingDate(dataMember.ID, param);
            }
            catch (Exception ex)
            {
                result.Value = null;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has enocuntered a problem, please try again later.";
                LoyaltyPointEngine.MemberLogicLayer.EventLogLogic.InsertEventLog(null, Common.EventLogLogic.LogType.Exception, Common.EventLogLogic.LogSource.Access,
                    Common.EventLogLogic.LogEventCode.Edit, ex.StackTrace, null, this.Request.RequestUri.AbsoluteUri, "LoyalAPI.GenerateForgotPasswordToken");
            }

            return result;
        }


        /// <summary>
        /// Set IsActive, ActiveDate, ActiveBy
        /// </summary>
        /// <param name="param"></param>
        /// <returns>ResultModel</returns>
        //[LoyalSimpleFilter]
        [HttpPost]
        public ResultModel<Model.Object.OTP.OTPMemberModel> UpdateMemberActive(UpdateMemberActiveModel param)
        {
            ResultModel<Model.Object.OTP.OTPMemberModel> result = new ResultModel<Model.Object.OTP.OTPMemberModel>();

            if (!string.IsNullOrEmpty(param.Key))
            {
                try
                {
                    // Get Client Data
                    var clientData = Pool.GetByClientId(param.ClientID);
                    if (clientData == null)
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = "Client data from Pool is null";
                        SaveLogError(result.StatusMessage);
                        return result;
                    }

                    var decryptedKey = Encrypt.DecryptString(param.Key).Split('-');
                    var phone = decryptedKey[0];

                    // Get Data Member
                    DataMember.Member member = DataMember.Member.GetAll().Where(x => x.Phone == phone && !x.IsActive).FirstOrDefault();
                    if (member == null)
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = "Member yang akan di aktifkan tidak ditemukan";
                        SaveLogError(result.StatusMessage);
                        return result;
                    }

                    // Get Data Receipt
                    List<Receipt> receipts = Receipt.GetByMemberId(member.ID).Where(x => x.Status == (int)Receipt.ReceiptStatus.Unverify || x.Status == (int)Receipt.ReceiptStatus.WaitingUnverify).ToList();

                    // Validate OTP Member
                    ResultModel<PoolOTPMember> resultValidateOTP = PointLogicLayer.OTP.OTPLogic.ValidateOTP(decryptedKey[0], decryptedKey[1], Request.RequestUri.ToString(), clientData.ID);
                    if (resultValidateOTP.StatusCode != "00" || resultValidateOTP.Value == null)
                    {
                        result.StatusCode = resultValidateOTP.StatusCode;
                        result.StatusMessage = resultValidateOTP.StatusMessage;
                        SaveLogError(result.StatusMessage);
                        return result;
                    }

                    // Get OTP Member data from result
                    PoolOTPMember otpMember = resultValidateOTP.Value;

                    // Save All Data
                    ResultModel<bool> resultSaveData = PointLogicLayer.OTP.OTPLogic.SaveActivateMember(member, receipts, otpMember, Request.RequestUri.ToString(), clientData.ID);
                    if (!resultSaveData.Value)
                    {
                        result.StatusCode = resultSaveData.StatusCode;
                        result.StatusMessage = resultSaveData.StatusMessage;
                        SaveLogError(resultSaveData.StatusMessage);
                        return result;
                    }

                    // Update receipt status to VerifyWaiting
                    var resReceipt = PointLogicLayer.ReceiptLogic.ReStatusUnverifyReceipt(member.Phone, member.FirstName + "" + member.LastName, Request.RequestUri.ToString(), member.ID);
                    if (resReceipt.Value)
                    {
                        var res = new Model.Object.OTP.OTPMemberModel();
                        res.Phone = decryptedKey[0];
                        res.OTPCode = decryptedKey[1];

                        result.StatusCode = resultSaveData.StatusCode;
                        result.StatusMessage = resultSaveData.StatusMessage;
                        result.Value = res;
                        return result;
                    }
                    else
                    {
                        result.StatusCode = resReceipt.StatusCode;
                        result.StatusMessage = resReceipt.StatusMessage;
                        SaveLogError(resReceipt.StatusMessage);
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "51";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = "sent key is not valid";
            }

            SaveLogError(result.StatusMessage);
            return result;
        }

        /// <summary>
        /// Upload KK
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultModel<MemberModel> UploadKK(UploadKKModel param)
        {
            var dataMember = ApiCommon.GetMember(Request.Headers);

            ResultModel<MemberModel> result = new ResultModel<MemberModel>();

            try
            {
                result = MemberLogic.AddUploadKK(param, dataMember.FirstName, dataMember.ID);
            }
            catch (Exception ex)
            {

                result.Value = null;
                result.StatusCode = "500";
                result.StatusMessage = "Sorry, seems like our system has encountered a problem, please try again later.";
            }

            return result;
        }
    }
}
