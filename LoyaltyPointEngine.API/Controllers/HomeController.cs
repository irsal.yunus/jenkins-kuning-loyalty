﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using LoyaltyPointEngine.PointLogicLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace LoyaltyPointEngine.API.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            // EventLogLogic.InsertEventLog(null, EventLogLogic.LogType.Warning, EventLogLogic.LogSource.Member, EventLogLogic.LogEventCode.View,"test", "aaa","", "Api");

           // var data = LoyaltyPointEngine.Data.Number.Generated(Data.Number.Type.RCP,"system");

             string clientSecretEncript = LoyaltyPointEngine.Common.Encrypt.LoyalEncript("12380123jn1k23hj12k");
            string passwordEncript = LoyaltyPointEngine.Common.Encrypt.LoyalEncript("password");

            string a = "abc!%@$^&#dasdkjs)(*(}:{}".RemoveSpecialCharacters();


            return View();
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            byte[] thePictureAsBytes = new byte[file.ContentLength];
            using (BinaryReader theReader = new BinaryReader(file.InputStream))
            {
                thePictureAsBytes = theReader.ReadBytes(file.ContentLength);
            }


            var fileBase = System.Convert.ToBase64String(thePictureAsBytes);

            string result = UploadStruct(fileBase);

            return View();
        }

        private string UploadStruct(string base64)
        {
            string result = "";
            using (WebClient client = new WebClient())
            {
                string url = "http://localhost:3160/api/Receipt/Upload";

                UploadReceiptModel param = new UploadReceiptModel()
                {
                 
                    TransactionDate =DateTime.Now.ToString()
                };

                param.Images = new List<string>();
                param.Images.Add(base64);
                param.Images.Add(base64);


                param.Details = new List<UploadReceiptDetail>();
                param.Details.Add(PopulateDetail(1, 2));
                param.Details.Add(PopulateDetail(4, 2));
                param.Details.Add(PopulateDetail(5, 2));

                var paramString= JsonConvert.SerializeObject(param);

                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Headers.Add(HttpRequestHeader.Authorization, string.Format("Bearer {0}", "833be030-add0-4cf6-b517-fcad9d2d76e4"));
                result = client.UploadString(url, paramString);

            }

            return result;
        }

        private static UploadReceiptDetail PopulateDetail(int id, int qty)
        {
            UploadReceiptDetail result = new UploadReceiptDetail();
            result.ActionID = id;
            result.Quantity = qty;
            return result;

        }
    }


    public static class Helper
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                string output = str; //Regex.Replace(str, "[^0-9A-Za-z]+", "-"); 



                var characters = output.Select(c =>
                {
                    if (!char.IsLetterOrDigit(c))
                    {
                        return '-';
                    }
                    return c;
                }).ToArray();

                output = string.Join("", characters);
                output = output.Replace("-", " ");
                output = output.Trim();

                //while (output.Contains("%"))
                //{
                //    output = output.Replace("%", "-");
                //}

                //while (output.Contains("_"))
                //{
                //    output = output.Replace("_", "-");
                //}

                //while (output.Contains("    "))
                //{
                //    output = output.Replace("   ", "-");
                //}
                //output = output.Replace(" ", "-");
                //while (output.Contains("--"))
                //{
                //    output = output.Replace("--", "-");
                //}


                //while (output.Contains("&"))
                //{
                //    output = output.Replace("&", "-");
                //}


                output = output.Replace(" ", "");





                return output;
            }

            return "";

        }

    }



}
