﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.PointLogicLayer.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class AreaController : BaseController
    {

        /// <summary>
        /// get list Area by pagination and search criteria
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [LoyalFilter]
        public ResultPaginationModel<List<AreaModel>> Search(ParamPagination param)
        {
            return AreaLogic.Search(param);
        }

        /// <summary>
        /// Get Area detail by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<AreaDetailModel> Get(int ID)
        {
            return AreaLogic.GetAreaModelById(ID);
        }

        /// <summary>
        /// get list Area by District id
        /// </summary>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<List<AreaModel>> GetByDistrictId(int DistrictID)
        {
            return AreaLogic.GetAreaModelByDistrictId(DistrictID);
        }
    }
}