﻿using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LoyaltyPointEngine.API.Controllers
{
    public class ChannelController : BaseController
    {
        /// <summary>
        ///  get list of channel base on search criteria and Pagination.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [LoyalFilter]
        [HttpPost]
        public ResultPaginationModel<List<ChannelModel>> Search([System.Web.Http.FromBody]ParamPagination param)
        {
            return ChannelLogic.Search(param);
        }


        /// <summary>
        /// Get Detail information of channel
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [LoyalFilter]
        public ResultModel<ChannelModel> Get(int ID)
        {
            return ChannelLogic.Get(ID);
        }
    }
}