﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.EventCode;
using LoyaltyPointEngine.PointLogicLayer.Area;

namespace LoyaltyPointEngine.API.Controllers
{
    public class EventCodeController : BaseController
    {
        [HttpGet]
        public ResultModel<List<EventCodeModel>> GetEventCodeMobile()
        {
            return EventCodeLogic.GetAllEventCodeMobile();
        }
    }
}
