﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Log;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace LoyaltyPointEngine.API
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        private class HttpDataPointContextDataStore : LoyaltyPointEngine.Data.IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        private class HttpDataMemberContextDataStore : LoyaltyPointEngine.DataMember.IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            LoyaltyPointEngine.Data.DataRepositoryStore.CurrentDataStore = new HttpDataPointContextDataStore();
            LoyaltyPointEngine.DataMember.DataRepositoryStore.CurrentDataStore = new HttpDataMemberContextDataStore();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Logger.Log = new Common.Log.Logger();

            log4net.Config.XmlConfigurator.Configure();
        }

        // Global error catcher
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                IRavenClient _ravenClient = new RavenClient(SiteSetting.RavenClientDsn);
                _ravenClient.Environment = SiteSetting.RavenClientEnvironment;
                _ravenClient.Capture(new SentryEvent(exception));
            }
        }

        //protected void Application_Error(Object sender, EventArgs e)
        //{
        //    var httpContext = ((WebApiApplication)sender).Context;
        //    Exception exception = HttpContext.Current.Server.GetLastError();
        //    if (exception == null)
        //    {
        //        exception = httpContext.Server.GetLastError();
        //    }
        //    Logger.Log.Push(httpContext.Request);
        //}

        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
        //    var currentController = " ";
        //    var currentAction = " ";
        //    if (currentRouteData != null)
        //    {
        //        if (currentRouteData.Values["controller"] != null &&
        //            !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
        //        {
        //            currentController = currentRouteData.Values["controller"].ToString();
        //        }
        //        if (currentRouteData.Values["action"] != null &&
        //            !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
        //        {
        //            currentAction = currentRouteData.Values["action"].ToString();
        //        }
        //    }
        //    var token = ApiCommon.GetToken( HttpContext.Current.Request.Headers);
        //    ApiCommon.GlobalError(Server.GetLastError().GetBaseException(), currentController, currentAction, null,token);
        //}
    }
}