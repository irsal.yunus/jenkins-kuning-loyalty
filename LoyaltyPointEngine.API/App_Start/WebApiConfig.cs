﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace LoyaltyPointEngine.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
          //  config.Formatters.Add(new XmlMediaTypeFormatter()); 

          
           

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            

           // config.Routes.MapHttpRoute("accestoken", "access-token", new { controller = "Access", action = "Token" });
        }
    }
}
