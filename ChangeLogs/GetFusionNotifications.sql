USE [LoyaltyMemberEngine]
GO

/****** Object:  StoredProcedure [dbo].[GetFusionNotifications]    Script Date: 06/09/2017 18:00:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Rendhi Agno Syifa
-- Create date: 2017-02-07
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetFusionNotifications]
	@MemberID int,
	@StartDate DATETIME,
	@EndDate DATETIME,
	@PageNumber INT,
	@RowspPage INT
AS
BEGIN
	--SET @MemberID = 65507
	--SET @StartDate = '2015-01-01'
	--SET @EndDate = '2018-01-01'
	--SET @PageNumber = 1
	--SET @RowspPage = 500

	DECLARE @DefaultDate DATETIME = '1901-01-01'

	IF(@StartDate IS NULL)
	BEGIN
		SET @StartDate = @DefaultDate
	END

	IF(@EndDate IS NULL)
	BEGIN
		SET @EndDate = @DefaultDate
	END

	IF(@PageNumber IS NULL)
	BEGIN 
		SET @PageNumber = 1
	END

	IF(@RowspPage IS NULL)
	BEGIN
		SET @RowspPage = 100
	END

	SELECT *, COUNT(*) OVER () AS [TotalRowCount] FROM (
	--------------------------------------------START REGION REDEEM--------------------------------------------
	SELECT 
		[NotificationID],
		[MemberID],
		[NotificationDate],
		[Title],
		[SubTitle],
		[Detail],
		[NotificationType],
		[ReferenceID],
		[ReferenceCode],
		[Point],
		[LastPoint],
		[IsRead],
		[LeftIcon],
		[RightIcon],
		[SourceData]
	FROM (
		SELECT ROW_NUMBER() OVER (PARTITION BY ReferenceCode ORDER BY NotificationDate DESC) AS IDX, * 
		FROM (
			--------------------------------REDEEM CHECKOUT---------------------------------------------------------------
			SELECT [NotificationID],
				[MemberID],
				[NotificationDate],
				[Title],
				[SubTitle],
				[Detail],
				[NotificationType],
				[ReferenceID],
				[ReferenceCode],
				[Point],
				[LastPoint],
				[IsRead],
				[LeftIcon],
				[RightIcon],
				[SourceData] 
			FROM 
				( SELECT ROW_NUMBER() OVER (PARTITION BY ReferenceCode ORDER BY NTF.[UpdatedDate] DESC) AS IDX,
					NTF.[ID] AS [NotificationID],
					NTF.[MemberID], 
					NTF.[UpdatedDate] AS NotificationDate,
					'Redeem ' + NTF.[ReferenceCode] AS [Title],
					'Redeem dalam proses' AS SubTitle,
					NTF.[Message] AS [Detail],
					NTF.[Type] AS [NotificationType], 
					CAST(RDM.[ID] AS NVARCHAR(255)) AS [ReferenceID],
					NTF.[ReferenceCode], 
					RDM.[Point],
					RDM.[PointEnd] AS [LastPoint],
					NTF.[IsRead], 
					NTF.[LeftIcon],
					NTF.[RightIcon],
					'Redeem' AS [SourceData]
				FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
					LEFT join [BebeRewards].[dbo].[Redemption] RDM on RDM.[Code] = NTF.[ReferenceCode]
				WHERE NTF.[Type] = 'CheckOut' AND NTF.[MemberID] = @MemberID ) TBL_CHECKOUT 
			WHERE IDX = 1
			----------------------------------------------------------------------------------------------------------------
			UNION ALL
			--------------------------------REDEEM ONSHIPPING---------------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				NTF.[Title],
				'Hadiah telah masuk proses pengiriman' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS [NotificationType], 
				CAST(RDM.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				RDM.[Point],
				RDM.[PointEnd] AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Redeem' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [BebeRewards].[dbo].[Redemption] RDM on RDM.[Code] = NTF.[ReferenceCode]
			WHERE NTF.[Type] = 'OnShipping' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
			UNION ALL
			--------------------------------REDEEM DELIVERED---------------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				NTF.[Title],
				'Hadiah telah diterima' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS NotificationType, 
				CAST(RDM.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				RDM.[Point],
				RDM.[PointEnd] AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Redeem' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [BebeRewards].[dbo].[Redemption] RDM on RDM.[Code] = NTF.[ReferenceCode]
			WHERE NTF.[Type] = 'Delivered' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
			UNION ALL
			--------------------------------REDEEM VOID---------------------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				NTF.Title,
				'Hadiah telah dikembalikan' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS [NotificationType], 
				CAST(RDM.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				RDM.[Point],
				RDM.[PointStart] AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Redeem' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [BebeRewards].[dbo].[Redemption] RDM on RDM.[Code] = NTF.[ReferenceCode]
			WHERE NTF.[Type] = 'Void' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
		) TBL_REDEEM 
	) TBL_REDEEM_RESULT WHERE IDX = 1
	--------------------------------------------END REGION REDEEM--------------------------------------------
	UNION ALL
	--------------------------------------------START REGION RECEIPT--------------------------------------------
	SELECT 
		[NotificationID],
		[MemberID],
		[NotificationDate],
		[Title],
		[SubTitle],
		[Detail],
		[NotificationType],
		[ReferenceID],
		[ReferenceCode],
		[Point],
		[LastPoint],
		[IsRead],
		[LeftIcon],
		[RightIcon],
		[SourceData]
	FROM (
		SELECT ROW_NUMBER() OVER (PARTITION BY ReferenceCode ORDER BY NotificationDate DESC) AS IDX, * FROM (
			--------------------------------RECEIPT ONPROGRESS---------------------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				NTF.[Title],
				'Struk sedang diproses' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS NotificationType, 
				CAST(RCP.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				SUM(CLT.[TotalPoints]) OVER(PARTITION BY CLT.ReceiptID) AS [Point],
				NULL AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Receipt' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [LoyaltyPointEngineNew].[dbo].[Receipt] RCP on RCP.[Code] = ntf.[ReferenceCode]
				LEFT JOIN [LoyaltyPointEngineNew].[dbo].[Collect] CLT ON CLT.[ReceiptID] = RCP.[ID] AND CLT.[IsDeleted] = 0
			WHERE NTF.[Type] = 'OnProgress' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
			UNION ALL
			--------------------------------RECEIPT REJECT------------------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				'Upload struk gagal ' + NTF.[ReferenceCode],
				'Struk ditolak oleh admin' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS NotificationType, 
				CAST(RCP.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				SUM(CLT.[TotalPoints]) OVER(PARTITION BY CLT.ReceiptID) AS [Point],
				NULL AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Receipt' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [LoyaltyPointEngineNew].[dbo].[Receipt] RCP on RCP.[Code] = ntf.[ReferenceCode]
				LEFT JOIN [LoyaltyPointEngineNew].[dbo].[Collect] CLT ON CLT.[ReceiptID] = RCP.[ID] AND CLT.[IsDeleted] = 0
			WHERE NTF.[Type] = 'Reject' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
			UNION ALL
			--------------------------------RECEIPT REJECTREUPLOAD----------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				'Upload struk gagal ' + NTF.[ReferenceCode],
				'Struk harus diupload ulang' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS NotificationType, 
				CAST(RCP.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				SUM(CLT.[TotalPoints]) OVER(PARTITION BY CLT.ReceiptID) AS Point,
				NULL AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Receipt' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [LoyaltyPointEngineNew].[dbo].[Receipt] RCP on RCP.[Code] = NTF.[ReferenceCode]
				LEFT JOIN [LoyaltyPointEngineNew].[dbo].[Collect] CLT ON CLT.[ReceiptID] = RCP.[ID] AND CLT.[IsDeleted] = 0
			WHERE NTF.[Type] = 'RejectReupload' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
			UNION ALL
			--------------------------------------RECEIPT APPROVED----------------------------------------------------------
			SELECT 
				NTF.[ID] AS [NotificationID],
				NTF.[MemberID], 
				NTF.[UpdatedDate] AS [NotificationDate],
				NTF.[Title],
				'Struk berhasil diproses' AS [SubTitle],
				NTF.[Message] AS [Detail],
				NTF.[Type] AS NotificationType, 
				CAST(RCP.[ID] AS NVARCHAR(255)) AS [ReferenceID],
				NTF.[ReferenceCode], 
				SUM(CLT.[TotalPoints]) OVER(PARTITION BY CLT.ReceiptID) AS Point,
				MAX(BLG.Balance) OVER(PARTITION BY BLG.ReferenceID) AS [LastPoint],
				NTF.[IsRead], 
				NTF.[LeftIcon],
				NTF.[RightIcon],
				'Receipt' AS [SourceData]
			FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
				LEFT join [LoyaltyPointEngineNew].[dbo].[Receipt] RCP on RCP.[Code] = ntf.[ReferenceCode]
				LEFT JOIN [LoyaltyPointEngineNew].[dbo].[Collect] CLT ON CLT.[ReceiptID] = RCP.[ID] AND CLT.[IsDeleted] = 0
				LEFT JOIN [LoyaltyPointEngineNew].[dbo].[BalanceLog] BLG ON BLG.[ReferenceID] = CLT.[ID] AND CLT.[IsDeleted] = 0 AND BLG.[Type] = 'C'
			WHERE NTF.[Type] = 'Approved' AND NTF.[MemberID] = @MemberID
			----------------------------------------------------------------------------------------------------------------
		) TBL_RECEIPT 
	) TBL_RECEIPT_RESULT WHERE IDX = 1 
	---------------------------------------END REGION RECEIPT---------------------------------------------------------------
	UNION ALL
	--------------------------------START REGION INFORMATION----------------------------------------------------------------
	SELECT 
		NTF.[ID] AS [NotificationID],
		NTF.[MemberID], 
		NTF.[UpdatedDate] AS [NotificationDate],
		NTF.[Title],
		'Info ' AS [SubTitle],
		NTF.[Message] AS [Detail],
		NTF.[Type] AS NotificationType, 
		NULL AS [ReferenceID],
		NTF.[ReferenceCode], 
		NTF.[Point],
		NULL AS [LastPoint],
		NTF.[IsRead], 
		NTF.[LeftIcon],
		NTF.[RightIcon],
		'Other' AS [SourceData]
	FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
	WHERE NTF.[Type] = 'Information' AND NTF.[MemberID] = @MemberID
	-------------------------------END REGION INFORMATION------------------------------------------------------
	UNION ALL
	--------------------------------START REGION WARNING-------------------------------------------------------
	SELECT 
		NTF.[ID] AS [NotificationID],
		NTF.[MemberID], 
		NTF.[UpdatedDate] AS [NotificationDate],
		NTF.[Title],
		'Warning ' AS [SubTitle],
		NTF.[Message] AS [Detail],
		NTF.[Type] AS NotificationType, 
		NULL AS [ReferenceID],
		NTF.[ReferenceCode], 
		NTF.[Point],
		NULL AS [LastPoint],
		NTF.[IsRead], 
		NTF.[LeftIcon],
		NTF.[RightIcon],
		'Other' AS [SourceData]
	FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
	WHERE NTF.[Type] = 'Warning' AND NTF.[MemberID] = @MemberID
	-----------------------------------END REGION WARNING-------------------------------------------------------------
	UNION ALL
	--------------------------------START REGION EVENT-------------------------------------------------------
	SELECT 
		NTF.[ID] AS [NotificationID],
		NTF.[MemberID], 
		NTF.[UpdatedDate] AS [NotificationDate],
		NTF.[Title],
		'Event ' AS [SubTitle],
		NTF.[Message] AS [Detail],
		NTF.[Type] AS NotificationType, 
		NULL AS [ReferenceID],
		NTF.[ReferenceCode], 
		NTF.[Point],
		NULL AS [LastPoint],
		NTF.[IsRead], 
		NTF.[LeftIcon],
		NTF.[RightIcon],
		'Other' AS [SourceData]
	FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
	WHERE NTF.[Type] = 'Event' AND NTF.[MemberID] = @MemberID 
	-----------------------------------END REGION EVENT-------------------------------------------------------------
	UNION ALL
	--------------------------------START REGION CUSTOM-------------------------------------------------------
	SELECT 
		NTF.[ID] AS [NotificationID],
		NTF.[MemberID], 
		NTF.[UpdatedDate] AS [NotificationDate],
		NTF.[Title],
		'' AS [SubTitle],
		NTF.[Message] AS [Detail],
		NTF.[Type] AS NotificationType, 
		NULL AS [ReferenceID],
		NTF.[ReferenceCode], 
		NTF.[Point],
		NULL AS [LastPoint],
		NTF.[IsRead], 
		NTF.[LeftIcon],
		NTF.[RightIcon],
		'Other' AS [SourceData]
	FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
	WHERE NTF.[Type] = 'Custom' AND NTF.[MemberID] = @MemberID  
	-----------------------------------END REGION CUSTOM-------------------------------------------------------------
	UNION ALL
	--------------------------------START REGION EXPIRED-------------------------------------------------------
	SELECT 
		NTF.[ID] AS [NotificationID],
		NTF.[MemberID], 
		NTF.[UpdatedDate] AS [NotificationDate],
		NTF.[Title],
		'Point sudah kadaluarsa' AS [SubTitle],
		NTF.[Message] AS [Detail],
		NTF.[Type] AS NotificationType, 
		CAST(CLT.ID AS NVARCHAR(255)) AS [ReferenceID],
		NTF.[ReferenceCode], 
		NTF.[Point],
		MIN(BLG.Balance) OVER(PARTITION BY BLG.ReferenceID) AS [LastPoint],
		NTF.[IsRead], 
		NTF.[LeftIcon],
		NTF.[RightIcon],
		'Collect' AS [SourceData]
	FROM [LoyaltyMemberEngine].[dbo].[Notification] NTF
		LEFT JOIN [LoyaltyPointEngineNew].[dbo].[Collect] CLT ON CLT.ID = NTF.[ReferenceCode] AND CLT.[IsDeleted] = 0
		LEFT JOIN [LoyaltyPointEngineNew].[dbo].[BalanceLog] BLG ON BLG.[ReferenceID] = CLT.[ID] AND CLT.[IsDeleted] = 0 AND BLG.[Type] = 'D'
	WHERE NTF.[Type] = 'Expired' AND NTF.[MemberID] = @MemberID 
	-----------------------------------END REGION EXPIRED-------------------------------------------------------------
	) TBL_NOTIF WHERE NotificationDate BETWEEN @StartDate AND @EndDate 
	ORDER BY NotificationDate DESC
	OFFSET ((@PageNumber - 1) * @RowspPage) ROWS
	FETCH NEXT @RowspPage ROWS ONLY;
END



GO


