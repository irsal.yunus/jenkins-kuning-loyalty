USE [BebeRewards]
GO

INSERT INTO [dbo].[Settings]
           ([Code]
           ,[Key]
           ,[Value]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[UpdatedDate]
           ,[UpdatedBy]
           ,[IsDeleted]
           ,[Type]
           ,[IsPrivate])
     VALUES
           (NULL
           ,'EndPoint_GenerateForgotPasswordToken'
           ,'/Member/GenerateForgotPasswordToken'
           ,'2017-11-08 00:00:00.000'
           ,'Admin'
           ,'2017-11-08 00:00:00.000'
           ,'Admin'
           ,0
           ,'FreeText'
           ,1),
		   (NULL
           ,'EndPoint_VerifyForgotPasswordToken'
           ,'/Member/VerifyForgotPasswordToken?id={0}&client_id={1}'
           ,'2017-11-08 00:00:00.000'
           ,'Admin'
           ,'2017-11-08 00:00:00.000'
           ,'Admin'
           ,0
           ,'FreeText'
           ,1),
		   (NULL
           ,'EndPoint_ChangeResetPassword'
           ,'/Member/ChangeResetPassword'
           ,'2017-11-08 00:00:00.000'
           ,'Admin'
           ,'2017-11-08 00:00:00.000'
           ,'Admin'
           ,0
           ,'FreeText'
           ,1)
GO


