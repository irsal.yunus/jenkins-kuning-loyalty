
ALTER Procedure [dbo].[GetListForUnikKonsumenReport](@StartDate DATETIME,@EndDate DATETIME)
AS
BEGIN
	--SET @MemberID = 64743
	--SET @StartDate = '2017-02-01'
	--SET @EndDate = '2017-02-07'

	DECLARE @DefaultDate DATETIME = '1901-01-01'
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @listUnikKsm table (
					MemberID bigint, 
					Gender varchar(50), 
					FirstName varchar(100),
					LastName varchar(100),
					Email varchar(100),
					Phone varchar(100),
					IsBacklist int null,
					MemberPoint int null,
					ChildName varchar(100),
					ChildBirthDate Date,
					OutletName varchar(100),
					SpgCode varchar(50),
					SpgName varchar(100),
					ChannelName varchar(100))

	IF(@StartDate IS NULL)
	BEGIN
		SET @StartDate = @DefaultDate
	END

	IF(@EndDate IS NULL)
	BEGIN
		SET @EndDate = @DefaultDate
	END
	--SET @SqlQuery = 'SELECT [Member].ID AS MemberID, [Member].Gender, [Member].FirstName, [Member].LastName, [Member].Email, [Member].Phone, [Child].Name AS ChildName, [Child].BirthDate AS ChildBirthDate, [SPG].OutletName, [SPG].Code AS SpgCode, [SPG].Name AS SpgName FROM [Member] 
	--	LEFT JOIN [SPG] ON [Member].SpgID = [SPG].[ID]
	--	LEFT JOIN [Child] ON [Member].ID = [Child].[ParentID]'
	--IF(@StartDate > @DefaultDate AND @EndDate = @DefaultDate)
	--	SET @SqlQuery = @SqlQuery + 'Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt] Where [NutriPointEngine].[dbo].[Receipt].CreatedDate >= ''' + CONVERT(CHAR(10),  @StartDate, 120) + ''')' 
	--ELSE IF(@StartDate = @DefaultDate AND @EndDate > @DefaultDate)
	--	SET @SqlQuery = @SqlQuery + 'Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt] Where [NutriPointEngine].[dbo].[Receipt].CreatedDate <= ''' + CONVERT(CHAR(10),  @EndDate, 120) + ''')' 
	--ELSE IF(@StartDate > @DefaultDate OR @EndDate > @DefaultDate)
	--	SET @SqlQuery = @SqlQuery + 'Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt] Where [NutriPointEngine].[dbo].[Receipt].CreatedDate BETWEEN ''' + CONVERT(CHAR(10),  @StartDate, 120) + ''' AND ''' + CONVERT(CHAR(10),  @EndDate, 120) + ''')' 
	--ELSE
	--	SET @SqlQuery = @SqlQuery + 'Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt])' 
	--PRINT @SqlQuery
	--EXEC(@SqlQuery)
	--SELECT @SqlQuery
	IF(@StartDate > @DefaultDate AND @EndDate = @DefaultDate)
		Insert into @listUnikKsm
		SELECT [Member].ID AS MemberID, [Member].Gender, [Member].FirstName, [Member].LastName, [Member].Email, [Member].Phone, [Member].IsBlacklist, [Member].MemberPoint, [Child].Name AS ChildName, [Child].BirthDate AS ChildBirthDate, [SPG].OutletName, [SPG].Code AS SpgCode, [SPG].Name AS SpgName, [Channel].Name AS ChannelName FROM [Member] 
		LEFT JOIN [SPG] ON [Member].SpgID = [SPG].[ID]
		LEFT JOIN [Child] ON [Member].ID = [Child].[ParentID]
		LEFT JOIN [Channel] ON [Member].ChannelID = [Channel].[ID] 
		Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt] Where [NutriPointEngine].[dbo].[Receipt].CreatedDate >= '' + CONVERT(CHAR(10),  @StartDate, 120) + '')
	ELSE IF(@StartDate = @DefaultDate AND @EndDate > @DefaultDate)
		Insert into @listUnikKsm
		SELECT [Member].ID AS MemberID, [Member].Gender, [Member].FirstName, [Member].LastName, [Member].Email, [Member].Phone, [Member].IsBlacklist, [Member].MemberPoint, [Child].Name AS ChildName, [Child].BirthDate AS ChildBirthDate, [SPG].OutletName, [SPG].Code AS SpgCode, [SPG].Name AS SpgName, [Channel].Name AS ChannelName FROM [Member] 
		LEFT JOIN [SPG] ON [Member].SpgID = [SPG].[ID]
		LEFT JOIN [Child] ON [Member].ID = [Child].[ParentID] 
		LEFT JOIN [Channel] ON [Member].ChannelID = [Channel].[ID] 
		Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt] Where [NutriPointEngine].[dbo].[Receipt].CreatedDate <= '' + CONVERT(CHAR(10),  @EndDate, 120) + '')
	ELSE IF(@StartDate > @DefaultDate OR @EndDate > @DefaultDate)
		Insert into @listUnikKsm
		SELECT [Member].ID AS MemberID, [Member].Gender, [Member].FirstName, [Member].LastName, [Member].Email, [Member].Phone, [Member].IsBlacklist, [Member].MemberPoint, [Child].Name AS ChildName, [Child].BirthDate AS ChildBirthDate, [SPG].OutletName, [SPG].Code AS SpgCode, [SPG].Name AS SpgName, [Channel].Name AS ChannelName FROM [Member] 
		LEFT JOIN [SPG] ON [Member].SpgID = [SPG].[ID]
		LEFT JOIN [Child] ON [Member].ID = [Child].[ParentID] 
		LEFT JOIN [Channel] ON [Member].ChannelID = [Channel].[ID] 
		Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt] Where [NutriPointEngine].[dbo].[Receipt].CreatedDate BETWEEN '' + CONVERT(CHAR(10),  @StartDate, 120) + '' AND '' + CONVERT(CHAR(10),  @EndDate, 120) + '')
	ELSE
		Insert into @listUnikKsm
		SELECT [Member].ID AS MemberID, [Member].Gender, [Member].FirstName, [Member].LastName, [Member].Email, [Member].Phone, [Member].IsBlacklist, [Member].MemberPoint, [Child].Name AS ChildName, [Child].BirthDate AS ChildBirthDate, [SPG].OutletName, [SPG].Code AS SpgCode, [SPG].Name AS SpgName, [Channel].Name AS ChannelName FROM [Member] 
		LEFT JOIN [SPG] ON [Member].SpgID = [SPG].[ID]
		LEFT JOIN [Child] ON [Member].ID = [Child].[ParentID] 
		LEFT JOIN [Channel] ON [Member].ChannelID = [Channel].[ID] 
		Where [Member].ID in (Select DISTINCT([NutriPointEngine].[dbo].[Receipt].MemberID) FROM [NutriPointEngine].[dbo].[Receipt])
	
	Select
					MemberID,
					Gender,
					FirstName,
					LastName,
					Email,
					Phone,
					IsBacklist,
					MemberPoint, 
					ChildName, 
					ChildBirthDate, 
					OutletName, 
					SpgCode, 
					SpgName,
					ChannelName
	From @listUnikKsm
	--SET NOCOUNT OFF
END
GO


