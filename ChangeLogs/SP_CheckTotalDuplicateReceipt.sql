/****** Object:  StoredProcedure [dbo].[SP_CheckTotalDuplicateReceipt]    Script Date: 10/20/2017 10:46:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Rendhi Agno Syifa
-- Create date: 2017-10-12
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_CheckTotalDuplicateReceipt]
	@MemberID INT,
	@TransDate DATETIME,
	@RetailerID INT,
	@ArrActionID VARCHAR(MAX)
AS
BEGIN
	--SET @MemberID = 974919
	--SET @TransDate = '2017-02-01'
	--SET @RetailerID = 8
	--SET @ArrActionID = '33'

	DECLARE @DefaultDate DATETIME = '1901-01-01'
	DECLARE @SqlQuery NVARCHAR(MAX)
	
	SET @SqlQuery = 'Select COUNT(*) AS Total FROM (Select rcp.ID, rcp.MemberID, rcp.TransactionDate, rcp.RetailerID,
					REPLACE(STUFF((SELECT ' + ''', ''' + ' + CAST(ActionID AS VARCHAR(10)) AS VarAct
							FROM Collect
							WHERE ParentID IS NULL AND ReceiptID = rcp.ID
							ORDER BY ActionID ASC
							FOR XML PATH(' + ''''+ +'''' + '), TYPE).value(' + '''.''' + ', ' + '''NVARCHAR(MAX)''' + '), 1, 1, ' + '''' + + '''' 
					+ '), ' + ''' ''' + ', ' + '''' + + '''' + ') List_ActionID,
					REPLACE(STUFF((SELECT ' + ''', ''' + ' + CAST(Quantity AS VARCHAR(10)) AS VarAct
							FROM Collect
							WHERE ParentID IS NULL AND ReceiptID = rcp.ID
							ORDER BY ActionID ASC
							FOR XML PATH(' + ''''+ +'''' + '), TYPE).value(' + '''.''' + ', ' + '''NVARCHAR(MAX)''' + '), 1, 1, ' + '''' + + '''' 
					+ '), ' + ''' ''' + ', ' + '''' + + '''' + ') List_Quantity '
					+ ' from Receipt rcp '

	SET @SqlQuery = @SqlQuery + ') TBL WHERE TBL.MemberID = ' + CONVERT(varchar, @MemberID)

	IF(@TransDate IS NOT NULL AND @TransDate > @DefaultDate)
	BEGIN
		SET @SqlQuery = @SqlQuery + ' AND TBL.TransactionDate = ''' + CONVERT( NVARCHAR, @TransDate , 121 ) + ''''
	END
	
	IF(@RetailerID IS NOT NULL AND @RetailerID > 0)
	BEGIN
		SET @SqlQuery = @SqlQuery + ' AND TBL.RetailerID = ' + CONVERT(varchar, @RetailerID)
	END
	
	IF(@ArrActionID IS NOT NULL OR @ArrActionID <> '')
	BEGIN
		SET @SqlQuery = @SqlQuery + ' AND TBL.List_ActionID = ''' + @ArrActionID + ''''
	END

	--IF(@ArrActionID IS NOT NULL OR @ArrActionID <> '')
	--BEGIN
	--	SELECT ROW_NUMBER() OVER(ORDER BY ITEM) AS Num, Item INTO #TempTblActionID FROM dbo.fn_SplitString(@ArrActionID, N',');
	--	DECLARE @SqlSubQuery NVARCHAR(MAX) = ''
	--	DECLARE @Count INT = 1
	--	WHILE (@Count) <= (SELECT COUNT(*) FROM #TempTblActionID)
	--	BEGIN
	--		DECLARE @var VARCHAR(255)
	--		SELECT @Var = Item From #TempTblActionID Where Num = @Count
	--		SET @SqlSubQuery = @SqlSubQuery + ' AND clt.ActionID = ' + @Var
	--		SET @Count += 1;
	--	END
	--	DROP TABLE #TempTblActionID
	--	SET @SqlQuery = @SqlQuery + @SqlSubQuery
	--END

	EXEC (@SqlQuery)
	SELECT @SqlQuery
	--PRINT (@SqlQuery)
	--Select 55 AS Total
END




GO


