﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoyaltyPointEngine.DataMember
{
    public interface IDataRepositoryStore
    {
        object this[string key] { get; set; }
    }
}
