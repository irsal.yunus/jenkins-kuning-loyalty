﻿using LoyaltyPointEngine.Common.Helper;
using System;
using System.Data.Objects;
using System.Linq;

namespace LoyaltyPointEngine.DataMember
{
    public partial class Member
    {
        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();

            Member userCheckUsernameAvailability = Member.GetByEmail(this.Email);
            if (userCheckUsernameAvailability != null)
            {
                model.ErrorEntity = "Email Exist";
                model.ErrorMessage = "Email already exist";
                model.Success = false;
                return model;
            }

            userCheckUsernameAvailability = Member.GetByPhone(this.Phone);
            if (userCheckUsernameAvailability != null)
            {
                model.ErrorEntity = "Phone Exist";
                model.ErrorMessage = "Phone already exist";
                model.Success = false;
                return model;
            }

            try
            {
                this.FirstName = StringHelper.SanitizeFromWhiteSpace(this.FirstName);
                this.LastName = StringHelper.SanitizeFromWhiteSpace(this.LastName);
                if (this.IsAlfamart == null)
                    this.IsAlfamart = 6;
                if (this.IsAlfamartMidi == null)
                    this.IsAlfamartMidi = 0;
                this.IsActive = true;
                this.IsBlacklist = false;
                this.IsDeleted = false;

                //langsung approve kah?
                this.IsApproved = true;
                this.ApprovedBy = By;
                this.ApprovedDate = DateTime.Now;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;

                // Fixing ProductBeforeID null
                if (this.ProductBeforeID == null)
                    this.ProductBeforeID = 0;

                this.ActiveBy = By;
                this.ActiveDate = DateTime.Now;
                this.Save<Member>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                // Fixing ProductBeforeID null
                if (this.ProductBeforeID == null)
                    this.ProductBeforeID = 0;

                this.FirstName = StringHelper.SanitizeFromWhiteSpace(this.FirstName);
                this.LastName = StringHelper.SanitizeFromWhiteSpace(this.LastName);
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Member>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse UpdatedVendorStat(int sts, string vendor)
        {
            EFResponse model = new EFResponse();

            try
            {
                // Fixing ProductBeforeID null
                if (this.ProductBeforeID == null)
                    this.ProductBeforeID = 0;

                this.UpdatedBy = "SYS";
                this.UpdatedDate = DateTime.Now;
                switch (vendor) {
                    case "1": this.IsAlfamart = (short)sts;break;
                    case "2": this.IsAlfamart = (short)sts;break;
                }
                
                this.UpdateSave<Member>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public void MigrationInserted(string By, DateTime RegisterDate)
        {
            if (RegisterDate <= DateTime.MinValue)
            {
                RegisterDate = DateTime.Now;
            }

            this.IsActive = false;
            this.IsBlacklist = false;
            this.IsDeleted = false;
            this.IsApproved = true;
            if (this.IsAlfamart == null)
                this.IsAlfamart = 6;
            if (this.IsAlfamartMidi == null)
                this.IsAlfamartMidi = 0;

            // Fixing ProductBeforeID null
            if (this.ProductBeforeID == null)
                this.ProductBeforeID = 0;

            this.ApprovedBy = By;
            this.ApprovedDate = RegisterDate;
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = By;

            this.ActiveBy = By;
            this.ActiveDate = RegisterDate;
            this.Save<Member>();
        }
      
        public EFResponse Delete(string By)
        {
            EFResponse model = new EFResponse();

            try
            {

                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = true;
                this.UpdateSave<Member>();
                model.Success = true;
                
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<Member> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted);
        }

        public static IQueryable<Member> GetMemberPastAWeek(DateTime startDate, DateTime endDate)
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted && x.CreatedDate >= startDate && x.CreatedDate <= endDate);
        }

        public static Member GetByPhoneOrEmail(string phone, string email)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted
                && (
                (x.Phone == phone && x.Phone != "") ||
                (x.Email != "" && x.Email.ToLower() == email.ToLower())
                    )
                );
              
        }
        //GetByPhoneChannel by Ucup
        public static Member GetByPhoneChannel(string phone, int channel)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.Phone == phone && x.ChannelId == channel && x.Phone != "");
        }
        public static Member GetByPhone(string phone)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.Phone == phone && x.Phone != "");
        }

        public static Member GetByPhoneNew(string phone)
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted && x.Phone == phone && x.Phone != "").FirstOrDefault();
        }

        public static Member GetByPhoneExceptID(int Id, string phone)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.ID != Id && x.Phone == phone && x.Phone != "");
        }

        public static Member GetByEmail(string email)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.Email != "" && x.Email.ToLower() == email.ToLower());
        }

        public static Member GetByEmailExceptID(int Id,string email)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.ID != Id && x.Email != "" && x.Email.ToLower() == email.ToLower());
        }

        public static Member GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => x.ID == ID && !x.IsDeleted);
        }

        public static Member GetByFacebookId(string FBID)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => x.FacebookID == FBID && !x.IsDeleted);
        }

        public static bool IsExist(string phoneOrEmail)
        {
            if (phoneOrEmail.Contains('@'))
            {
                var count = DataRepositoryFactory.CurrentRepository.Member.Count(x => x.IsDeleted == false && x.Email == phoneOrEmail);
                return DataRepositoryFactory.CurrentRepository.Member.Any(x => x.IsDeleted == false && x.Email == phoneOrEmail);
            }
            else
            {
                var count = DataRepositoryFactory.CurrentRepository.Member.Count(x => x.IsDeleted == false && x.Phone == phoneOrEmail);
                return DataRepositoryFactory.CurrentRepository.Member.Any(x => x.IsDeleted == false && x.Phone == phoneOrEmail);
            }
        }

        public static Member GetByForgotPasswordToken(string token)
        {
            return GetAll().FirstOrDefault(x => x.ForgetPasswordToken.ToLower() == token.ToLower());
        }

        public static IQueryable<Member> GetAllCreatedToday(DateTime dtNow)
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted && EntityFunctions.TruncateTime(x.CreatedDate) == EntityFunctions.TruncateTime(dtNow));
        }

        public static IQueryable<Member> GetAllUpdatedToday(DateTime dtNow)
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted && EntityFunctions.TruncateTime(x.UpdatedDate) == EntityFunctions.TruncateTime(dtNow));
        }

        public static int Count()
        {
            return DataRepositoryFactory.CurrentRepository.Member.Count();
        }

        public static IQueryable<Member> GetAllExpiredNotverify(DateTime dateExpired)
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted && !x.IsActive && x.CreatedDate < dateExpired);
        }

        //public void Inserted(string by)
        //{

        //    this.IsActive =true;
        //    this.IsBlacklist = false;
        //    this.IsDeleted = false;

        //    //langsung approve kah?
        //    this.IsApproved = true;
        //    this.ApprovedBy = by;
        //    this.ApprovedDate =DateTime.Now;
        //    this.CreatedDate = DateTime.Now;
        //    this.CreatedBy = by;

        //    this.ActiveBy = by;
        //    this.ActiveDate = DateTime.Now;


        //    this.InsertSave<Member>();
        //}

        //public void Updated(string by)
        //{
        //    this.UpdatedBy = by;
        //    this.UpdatedDate = DateTime.Now;

        //    this.UpdateSave<Member>();
        //}

    }
}
