﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class District
    {
        public static IQueryable<District> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.District.Where(x => !x.IsDeleted);
        }

        public static District GetByID(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.District.FirstOrDefault(x => !x.IsDeleted && x.ID == ID);
        }

        public static District GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.District.FirstOrDefault(x => !x.IsDeleted && x.DistrictCode.ToLower() == code.ToLower());
        }

        public static District GetByCodeNotSelf(string code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.District.FirstOrDefault(x => !x.IsDeleted && x.DistrictCode.ToLower() == code.ToLower()
               && x.ID != ID);
        }

        public static int? GetPointDistrictId(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.District.Where(x => !x.IsDeleted && x.ID == ID).Select(x => x.PointDistrictId).FirstOrDefault();
        }

        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = false;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<District>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated(string by)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<District>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }
    }
}
