﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class ApprovalImage
    {
        public static IQueryable<ApprovalImage> GetByMemberId(int memberID)
        {
            return DataRepositoryFactory.CurrentRepository.ApprovalImage.Where(x => x.MemberID == memberID);
        }

        public static ApprovalImage GetByMemberIdTop1(int memberID)
        {
            return DataRepositoryFactory.CurrentRepository.ApprovalImage.Where(x => x.MemberID == memberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        public EFResponse Inserted(string By)
        {
            EFResponse response = new EFResponse();
            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<ApprovalImage>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }
        public EFResponse Updated(string by)
        {
            EFResponse response = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<ApprovalImage>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }

      

    }
}
