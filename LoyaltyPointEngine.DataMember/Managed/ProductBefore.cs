﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class ProductBefore
    {
        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = false;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<ProductBefore>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse Updated(string by)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<ProductBefore>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public static IQueryable<ProductBefore> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ProductBefores.Where(x => !x.IsDeleted);
        }

        public static ProductBefore GetById(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.ProductBefores.FirstOrDefault(x => x.ID == ID);
        }

        public static string GetLastProductBeforeByMemberId(int memberId)
        {
            return DataRepositoryFactory.CurrentRepository.sp_GetLastProductBefore(memberId).FirstOrDefault();
        }
    }
}
