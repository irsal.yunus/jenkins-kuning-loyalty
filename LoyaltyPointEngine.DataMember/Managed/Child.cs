﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class Child
    {
        public static Child GetById(int id)
        {
            return DataRepositoryFactory.CurrentRepository.Child.FirstOrDefault(x => x.ID == id && !x.IsDeleted);
        }
        public static IQueryable<Child> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Child.Where(x => !x.IsDeleted);
        }
        public static IQueryable<Child> GetByParentId(int parentId)
        {
            return DataRepositoryFactory.CurrentRepository.Child.Where(x => x.ParentID == parentId && !x.IsDeleted);
        }

        public EFResponse Inserted(string By)
        {
            EFResponse response = new EFResponse();
            try
            {
                // Fixing ProductBeforeID null
                if (this.ProductBeforeID == null)
                    this.ProductBeforeID = 0;

                this.IsDeleted = false;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<Child>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }
        public EFResponse Updated(string by)
        {
            EFResponse response = new EFResponse();
            try
            {
                // Fixing ProductBeforeID null
                if (this.ProductBeforeID == null)
                    this.ProductBeforeID = 0;

                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<Child>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }

        public static Child GetByIdAndParentId(int id, int parentId)
        {
            return DataRepositoryFactory.CurrentRepository.Child.FirstOrDefault(x => x.ID == id && x.ParentID == parentId && !x.IsDeleted);
        }

        public EFResponse Deleted(string by)
        {
            EFResponse response = new EFResponse();
            try
            {
                this.IsDeleted = true;
                this.DeletedDate = DateTime.Now;
                this.DeletedBy = by;
                this.UpdateSave<Child>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }
    }
}
