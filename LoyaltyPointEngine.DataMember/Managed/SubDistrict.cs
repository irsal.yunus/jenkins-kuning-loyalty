﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class SubDistrict
    {
        public static IQueryable<SubDistrict> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.SubDistrict.Where(x => !x.IsDeleted);
        }
        public static SubDistrict GetByID(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.SubDistrict.FirstOrDefault(x => !x.IsDeleted && x.ID ==ID);
        }


        public static SubDistrict GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.SubDistrict.FirstOrDefault(x => !x.IsDeleted && x.SubDistrictCode.ToLower() == code.ToLower());
        }
        public static SubDistrict GetByCodeNotSelf(string code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.SubDistrict.FirstOrDefault(x => !x.IsDeleted && x.SubDistrictCode.ToLower() == code.ToLower()
               && x.ID != ID
                );
        }
        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = false;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<SubDistrict>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse Updated(string by)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<SubDistrict>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }



    }
}

