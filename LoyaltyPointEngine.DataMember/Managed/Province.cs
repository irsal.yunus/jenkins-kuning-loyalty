﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class Province
    {
        public static IQueryable<Province> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Province.Where(x => !x.IsDeleted);
        }
        public static Province GetByID(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Province.FirstOrDefault(x => !x.IsDeleted && x.ID ==ID);
        }


        public static Province GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.Province.FirstOrDefault(x => !x.IsDeleted && x.ProvinceCode.ToLower() == code.ToLower());
        }
        public static Province GetByCodeNotSelf(string code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Province.FirstOrDefault(x => !x.IsDeleted && x.ProvinceCode.ToLower() == code.ToLower()
               && x.ID != ID
                );
        }
        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = false;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<Province>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse Updated(string by)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<Province>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }



    }
}
