﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class Stages
    {
        public static IQueryable<Stages> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Stages.Where(x => !x.IsDeleted);
        }

        public static Stages GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Stages.FirstOrDefault(x => x.ID == ID);
        }


        public string Inserted(string By)
        {
            try
            {
                this.IsDeleted = false;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<Stages>();
                return "";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

    }
}
