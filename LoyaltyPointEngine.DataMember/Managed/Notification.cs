﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class Notification
    {

        public static Notification GetById(Guid Id)
        {
            return DataRepositoryFactory.CurrentRepository.Notification.FirstOrDefault(x => !x.IsDeleted && x.ID == Id);
        }

        public static IQueryable<Notification> GetByMemberId(int MemberId)
        {
            return DataRepositoryFactory.CurrentRepository.Notification.Where(x => !x.IsDeleted && x.MemberID == MemberId);
        }

        public static int GetTotalUnreadNotificationByMemberId(int memberId)
        {
            var data = DataRepositoryFactory.CurrentRepository.GetTotalUnreadNotification(memberId).ToList();
            return data != null && data.Count > 0 && data.FirstOrDefault().HasValue ? data.FirstOrDefault().Value : 0;

        }

        public static List<GetFusionNotifications_Result> GetFusionNotifications(int MemberID, DateTime StartDate, DateTime EndDate, int pageNumber, int rowPerPage)
        {
            var result = DataRepositoryFactory.CurrentRepository.GetFusionNotifications(MemberID, StartDate, EndDate, pageNumber, rowPerPage);
            if (result != null)
                return result.ToList();
            else
                return null;
        }

        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<Notification>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated(string by)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedBy = by;
                this.UpdatedDate = DateTime.Now;
                this.UpdateSave<Notification>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
    }
}
