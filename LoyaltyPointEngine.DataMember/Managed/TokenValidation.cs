﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class TokenValidation
    {
        public static IQueryable<TokenValidation> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.TokenValidation.Where(x => x.IsDeleted == false);
        }
        public static TokenValidation GetByID(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.TokenValidation.FirstOrDefault(x => x.IsDeleted == false && x.ID == ID);
        }

        public static TokenValidation GetByTypeValueUniqueCode(string strValue, string uniqueCode, string type)
        {
            return DataRepositoryFactory.CurrentRepository.TokenValidation.FirstOrDefault(x => x.IsDeleted == false && x.DataValue != null && x.DataValue.ToLower() == strValue.ToLower() && x.UniqueCode == uniqueCode);
        }

        public static TokenValidation GetByCodeAndType(string uniqueCode, string type)
        {
            return DataRepositoryFactory.CurrentRepository.TokenValidation.FirstOrDefault(x => x.IsDeleted == false && x.Type != null && x.Type == type && x.UniqueCode == uniqueCode);
        }

        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = false;
                this.Created = DateTime.Now;
                this.Updated = DateTime.Now;
                this.InsertSave<TokenValidation>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse Update(string by)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.Updated = DateTime.Now;
                this.UpdateSave<TokenValidation>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }
    }
}
