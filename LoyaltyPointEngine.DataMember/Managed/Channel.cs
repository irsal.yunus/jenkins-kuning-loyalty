﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class Channel
    {
        public enum ChannelId
        {
            Elina = 4,
            ElinaMobile = 7
        }

        public static IQueryable<Channel> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Channel.Where(x => !x.IsDeleted);
        }

        public static Channel GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Channel.FirstOrDefault(x => x.ID == ID);
        }

        public static Channel GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.Channel.FirstOrDefault(x => !x.IsDeleted && x.Code == Code);
        }

    }
}
