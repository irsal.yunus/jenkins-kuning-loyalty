﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class SPG
    {
        public static IQueryable<SPG> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.SPG.Where(x => !x.IsDeleted);
        }

        public static SPG GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.SPG.FirstOrDefault(x => x.ID == ID);
        }

        public static SPG GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.SPG.FirstOrDefault(x => x.Code == Code);
        }
        public static SPG GetByCodeNotSelf(string Code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.SPG.FirstOrDefault(x => x.Code == Code && x.ID != ID);
        }

        public void Inserted(string By)
        {
            this.UpdatedDate = this.CreatedDate = DateTime.Now;
            this.UpdatedBy = this.CreatedBy = By;

            if (!this.IsActive.HasValue || this.IsActive == null)
            {
                this.IsActive = true;
            }

            this.InsertSave<SPG>();
        }


        public void Updated(string by)
        {
            this.UpdatedBy = by;
            this.UpdatedDate = DateTime.Now;

            this.UpdateSave<SPG>();
        }

        public void Deleted(string by)
        {
            this.IsDeleted = true;
            this.DeletedBy = by;
            this.DeletedDate = DateTime.Now;

            this.UpdateSave<SPG>();
        }

    }
}
