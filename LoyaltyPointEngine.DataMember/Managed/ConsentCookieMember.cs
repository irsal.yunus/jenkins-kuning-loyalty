﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.DataMember
{
    public partial class ConsentCookieMember
    {
        public EFResponse Inserted(string By)
        {
            EFResponse response = new EFResponse();
            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.InsertSave<ConsentCookieMember>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }
        public EFResponse Updated(string by)
        {
            EFResponse response = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = by;
                this.UpdateSave<ConsentCookieMember>();
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                response.ErrorEntity = ex.InnerException != null ? ex.InnerException.Message : ex.StackTrace;
            }
            return response;
        }
        public static IQueryable<ConsentCookieMember> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ConsentCookieMember;
        }
        public static ConsentCookieMember GetById(Guid id)
        {
            return DataRepositoryFactory.CurrentRepository.ConsentCookieMember.FirstOrDefault(x => x.ID == id);
        }
    }
}
