﻿using LoyaltyPointEngine.DataMember.GenericRepository;
namespace LoyaltyPointEngine.DataMember
{
    public interface IRepositoryFactory
    {
        IRepository Repository();
    }
}
