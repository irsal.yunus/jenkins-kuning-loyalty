﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Action
{
    public class ParamActionPagination : ParamPagination
    {
        public int? ActionCategoryID { get; set; }
       
    }
}
