﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.AccessToken
{
    public class AccessTokenParamModel
    {
        [Required]
        public string client_id { get; set; }
        [Required]
        public string client_secret { get; set; }

        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        public string OTP { get; set; }

        public bool? RememberMe { get; set; }
        public string DeviceID { get; set; }
        public string DeviceType { get; set; }

    }
    public class MemberFacebookLoginModel
    {
        [Required]
        public string client_id { get; set; }
        [Required]
        public string client_secret { get; set; }
        [Required]
        public string FacebookID { get; set; }
        public string DeviceID { get; set; }
        public string DeviceType { get; set; }
    }
}
