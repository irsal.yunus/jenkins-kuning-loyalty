﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter
{
    public class ParamPagination
    {       
        public string Search { get; set; }
        public int? TotalRowPerPage { get; set; }
        public int? Page { get; set; }
        public string OrderByColumnName { get; set; }
        public string OrderByDirection { get; set; }
    }

    public class ParamPaginationWithDate : ParamPagination
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }       
    }

    public class ParamPaginationWithClient : ParamPagination
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }
}
