﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.OTP
{
    public class OTPMemberModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OTPCode { get; set; }
    }
}
