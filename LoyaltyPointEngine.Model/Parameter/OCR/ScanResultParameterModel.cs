﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.OCR
{
    public class ScanResultParameterModel
    {
        public string Value { get; set; }
    }
}
