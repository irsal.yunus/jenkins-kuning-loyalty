﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.ConvertPoint
{
    public class ParamConvertOut 
    {
        public int RuleId { get; set; }
        public int Points { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }
        public string DestinationNumber { get; set; }
        
    }

    
}
