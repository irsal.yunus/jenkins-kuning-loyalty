﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.ConvertPoint
{
    public class ParamConvertIn 
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string phone { get; set; }
        public string actionCode { get; set; }
        public int points { get; set; }
        public string description { get; set; }
        public string FromNumber { get; set; }
       

    }
}
