﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.ConvertPoint
{
    public class StoreParamModel
    {
        public long? ID { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
