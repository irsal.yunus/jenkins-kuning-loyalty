﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.ConvertPoint
{
    public class StoreRuleParamModel
    {
        public long? Id { get; set; }
        public long StoreId { get; set; }
        public long Point { get; set; }
        public long StorePoint { get; set; }
        public long Promo { get; set; }
        public bool IsDeleted { get; set; }
        public string IconImageUrl { get; set; }
        public string LogoImageUrl { get; set; }
        public string ConvertOutUrl { get; set; }
        public string VoidOutUrl { get; set; }
        public long ConvertCap { get; set; }
        public long MinConvert { get; set; }
        public int ConvertCount { get; set; }
    }
}
