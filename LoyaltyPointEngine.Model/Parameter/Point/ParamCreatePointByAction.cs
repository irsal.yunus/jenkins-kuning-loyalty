﻿using LoyaltyPointEngine.Model.Parameter.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Point
{
    public class ParamCreatePointByAction
    {
        public ParamCreateNotification Notification { get; set; }
        public string ActionCode { get; set; }
        public int MemberId { get; set; }
    }
}
