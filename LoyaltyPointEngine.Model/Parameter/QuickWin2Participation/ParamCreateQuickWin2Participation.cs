﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.QuickWin2
{
    public class ParamCreateQuickWin2Participation
    {
        public Guid ReceiptID { get; set; }
        public int MemberID { get; set; }
        public string Comment { get; set; }
        
    }
}
