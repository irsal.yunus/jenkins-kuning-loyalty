﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Participations
{
    public class ParticipationsModel
    {
        public long ID { get; set; }
        public string Channel { get; set; }
        public string NameUser { get; set; }
        public string Phone { get; set; }
        public string ChildName { get; set; }
        public string ChildDOB { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public string ReceiptID { get; set; }
        public string By { get; set; }
        public int UserId { get; set; }
    }
}
