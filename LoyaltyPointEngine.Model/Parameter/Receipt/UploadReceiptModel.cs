﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Receipt
{
    public class UploadReceiptModel
    {
        public List<string> Images { get; set; }
        public string ReceiptCode { get; set; }
        public string TransactionDate { get; set; }
        public string EventCode { get; set; }
        public string RequestDate { get; set; }
        public string Remark { get; set; }
        public string RetailerAddress { get; set; }
        public string Channel { get; set; }
        public int? RetailerID { get; set; }
        public List<UploadReceiptDetail> Details { get; set; }
        public bool IsOnline { get; set; }
    }

    public class PublicUploadReceiptModel
    {
        public string ClientID { get; set; }
        public string MemberPhone { get; set; }
        public List<string> Images { get; set; }
        public string ReceiptCode { get; set; }
        public string TransactionDate { get; set; }
        public string Remark { get; set; }
        public string RetailerAddress { get; set; }
        public string Channel { get; set; }
        public int RetailerID { get; set; }
        public List<UploadReceiptDetail> Details { get; set; }
    }

    public class ReUploadReceiptModel : UploadReceiptModel
    {
        public Guid ReceiptID { get; set; }

    }

    public class UploadReceiptDetail
    {
        public int ActionID { get; set; }
        public int Quantity { get; set; }
        public int? ChildID { get; set; }
    }

    public class UploadReceiptCMSModel : UploadReceiptModel
    {
        public int RejectCount { get; set; }
        public UploadReceiptCMSModel()
        {

        }
        public UploadReceiptCMSModel(Guid ReceiptID)
        {

            List<UploadReceiptDetailCMSModel> listdetail = new List<UploadReceiptDetailCMSModel>();
            Data.Receipt currReceipt = Data.Receipt.GetById(ReceiptID);
            if (currReceipt != null)
            {
                var member = Data.Member.GetById(currReceipt.MemberID);
                this.ID = currReceipt.ID;
                this.ReceiptCode = currReceipt.ReceiptCode;
                this.MemberID = currReceipt.MemberID;
                if (member != null)
                {
                    this.MemberName = member.Name;
                }
                this.RetailerID = currReceipt.RetailerID.HasValue ? currReceipt.RetailerID.Value : 0;
                this.Code = currReceipt.Code;
                this.StrTransactionDate = currReceipt.TransactionDate.Value.ToString("dd/MM/yyyy");
                this.StrTransactionTime = currReceipt.TransactionDate.Value.ToString("HH:mm:ss");
                this.Image = currReceipt.ReceiptImage.Any() ? currReceipt.ReceiptImage.Select(x => x.Image).ToList() : new List<string>();
                this.Status = currReceipt.Status;
                var collects = Data.Collect.GetAllByReceiptID(ReceiptID).ToList();
                this.RejectReason = currReceipt.RejectReason;
                this.RejectCount = currReceipt.RejectCount.GetValueOrDefault(0);
                this.IsNeedReuploadData = currReceipt.IsNeedReuploadData;
                this.RetailerAddress = currReceipt.RetailerAddress;
                this.Channel = currReceipt.Channel;
                this.CampaignID = currReceipt.CampaignID;
                this.Campaign = this.CampaignID == 0 || this.CampaignID == null ? string.Empty : Data.Campaign.GetById((int)this.CampaignID).Name;
                this.StrRequestDate = !currReceipt.RequestDate.HasValue ? string.Empty : DateTime.Now.ToString("dd/MM/yyyy");
                this.TotalPrice = currReceipt.TotalPrice.GetValueOrDefault(0);
                this.IsOnline = currReceipt.IsOnline ?? false;
                var screceipt = Data.SnapcartReceipt.GetByReceiptId(currReceipt.ID);
                if (screceipt != null)
                {
                    Guid correlationId = Guid.Empty;
                    if (screceipt.RequestID != null)
                        correlationId = (Guid)screceipt.RequestID;
                    else
                        correlationId = screceipt.ID;

                    var scresult = Data.SnapcartResult.GetByCorrelationId(correlationId);

                    if(scresult != null)
                        this.ChainValue = scresult.ChainValue;
                }

                if (currReceipt.EventCode.GetValueOrDefault(0) != 0)
                {
                    var eventCodeValue = Data.EventCode.GetById((int)currReceipt.EventCode.GetValueOrDefault(0));
                    if (eventCodeValue != null)
                    {
                        this.EventCode = eventCodeValue.Code;
                    }
                }

                if (collects != null)
                {
                    foreach (Data.Collect detail in collects)
                    {
                        UploadReceiptDetailCMSModel model = new UploadReceiptDetailCMSModel();
                        model.ActionID = detail.ActionID;
                        model.Points = detail.Points;
                        model.Quantity = detail.Quantity;
                        model.QuantityApproved = detail.QuantityApproved;
                        model.TotalPoints = detail.TotalPoints;
                        model.ItemPrice = detail.ItemPrice;
                        model.Remarks = detail.Remarks;
                        model.ChildID = detail.ChildID;
                        if (detail.ChildID.HasValue)
                        {
                            var child = DataMember.Child.GetById(detail.ChildID.Value);
                            if (child != null)
                            {
                                model.ChildName = child.Name;
                            }
                        }
                        listdetail.Add(model);
                    }
                }



            }
            this.ReceiptDetails = listdetail;
        }

        public Guid ID { get; set; }
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public int RetailerID { get; set; }
        public string RetailerCode { get; set; }
        public string ReceiptCode { get; set; }
        public string Code { get; set; }
        public string StrTransactionDate { get; set; }
        public string StrTransactionTime { get; set; }
        public int TotalAction { get; set; }
        public int? ActionDummy { get; set; }
        public List<string> Image { get; set; }
        public List<UploadReceiptDetailCMSModel> ReceiptDetails { get; set; }
        public bool IsNeedReuploadData { get; set; }
        public string RejectReason { get; set; }
        public string RetailerAddress { get; set; }
        public string RetailerPostCode { get; set; }
        public string Channel { get; set; }
        public int? CampaignID { get; set; }
        public bool IsError { get; set; }
        public int Status { get; set; }
        public string StrRequestDate { get; set; }
        public string ProductCode { get; set; }
        public string Campaign { get; private set; }
        public string EventCode { get; set; }
        public int TotalPrice { get; set; }
        public string ChainValue { get; set; }
    }
    public class UploadReceiptDetailCMSModel
    {
        public int? ActionID { get; set; }
        public int Quantity { get; set; }
        public int QuantityApproved { get; set; }
        public int Points { get; set; }
        public int TotalPoints { get; set; }
        public int TotalQuantityApprovedPoints { get; set; }
        public int MaxPoints { get; set; }
        public string Remarks { get; set; }
        public string ErrorMessage { get; set; }
        public decimal ItemPrice { get; set; }
        public int? ChildID { get; set; }
        public string ChildName { get; set; }
    }

    public class UploadReceipFromExcelCMSModel
    {
        public string MemberPhone { get; set; }
        public string TransactionCode { get; set; }
        public string RetailerCode { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public string RetailerAddress { get; set; }
        public string Channel { get; set; } //add RX-119
        public int TotalPrice { get; set; }
    }

    public class RejectReceiptModel
    {
        public Guid ID { get; set; }
        public string Reason { get; set; }
        public bool IsNeedReuploadData { get; set; }
    }

    public class UploadReceiptFromExcelResultCMSModel : UploadReceipFromExcelCMSModel
    {
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public int RetailerID { get; set; }
        public Guid ReceiptID { get; set; }
        public string result { get; set; }
        public string message { get; set; }
    }

}
