﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Receipt
{
    public class ParamReceiptPagination:ParamPaginationWithDate
    {
        public int? Status { get; set; }
        public bool? IsReupload { get; set; }
    }
}
