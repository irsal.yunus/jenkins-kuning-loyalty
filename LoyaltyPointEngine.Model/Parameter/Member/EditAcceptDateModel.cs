﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class EditAcceptDateModel
    {
        public DateTime AcceptDate { get; set; }
    }

    public class EditRatingDateModel
    {
        public bool IsRated { get; set; }
    }
}
