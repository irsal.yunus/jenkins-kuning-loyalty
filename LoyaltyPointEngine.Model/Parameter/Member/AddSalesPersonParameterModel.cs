﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class AddSalesPersonParameterModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string OutletName { get; set; }
        public string LeaderName { get; set; }
        public int? DistrictID { get; set; }
    }
}
