﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class MemberParameterModel
    {
        public int? MemberId { get; set; }
        public string ClientID { get; set; }
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "start_Date")]
        public string StartDate { get; set; }
        [JsonProperty(PropertyName = "end_Date")]
        public string EndDate { get; set; }
        public int? IsApprovedKKImage { get; set; }

    }


}
