﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class EditChildParameterModel
    {
        public int ParentID { get; set; }
        public int ID { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Birthdate { get; set; }
        public int ProductBeforeId { get; set; }
    }
}
