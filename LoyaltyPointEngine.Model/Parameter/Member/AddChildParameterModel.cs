﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class AddChildParameterModel
    {
        public int ID { get; set; }
        public int ParentID { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Birthdate { get; set; }
        public int ProductBeforeId { get; set; }
        public DateTime BirthdateChild { get; set; }
    }

    public class AddNewChildParameterModel : AddChildParameterModel
    {
        public int ParentID { get; set; }
    }

    public class AddChildOnlyParameterModel
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Phone { get; set; }
        public List<AddChildParameterModel> Child { get; set; }
    }
}
