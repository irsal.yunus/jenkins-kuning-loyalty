﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class EditMemberParameterModel
    {
        //public int ID { get; set; }
        public string FacebookID { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? StagesID { get; set; }
        public string StagesValue { get; set; }
        public string Address { get; set; }
        public int? SpgID { get; set; }
        public string Birthdate { get; set; }
        public string PlaceOfBirth { get; set; }
        public string ProfilePicture { get; set; }
        public string Password { get; set; }
        public string KTPImage { get; set; }
        public string KTPNumber { get; set; }
        public string KKImage { get; set; }
        public string AkteImage { get; set; }
        public string KKNumber { get; set; }
        public string ZipCode { get; set; }
        public int? ProvinceID { get; set; }
        public int? DistrictID { get; set; }
        public int? SubDistrictID { get; set; }
        public int AgePregnant { get; set; }
        public string ChildName { get; set; }
        public string Channel { get; set; }
        public string ChildBirthdate { get; set; }
        public string Subscriber { get; set; }
        public string UsiaKehamilan { get; set; }
        public List<EditChildParameterModel> Childs { get; set; }
        public bool IsNewsletter { get; set; }
        public int? ProductBeforeID { get; set; }
        public string ESignature { get; set; }
        public bool Consent { get; set; }
    }
    public class EditMemberParameterCMSModel : EditMemberParameterModel
    {
        public int ID { get; set; }
    }
    public class UpdateStatVen
    {
        public string phone { get; set; }
        public string vendor { get; set; }
    }

    public class ChangePasswordModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class ChangeResetPasswordModel
    {
        public string ClientID { get; set; }
        public string Token { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
