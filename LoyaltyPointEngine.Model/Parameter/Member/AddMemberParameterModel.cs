﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class AddMemberParameterModel
    {
        public string ClientID { get; set; }
        //Static aja
        //public string ChannelCode { get; set; }
        public string FacebookID { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? StagesID { get; set; }
        public string StagesValue { get; set; }
        public string Address { get; set; }
        public string ChildName { get; set; }
        public string ChildBirthdate { get; set; }
        public int AgePregnant { get; set; }
        public int? SpgID { get; set; }
        public string Birthdate { get; set; }
        public string PlaceOfBirth { get; set; }
        public string ProfilePicture { get; set; }
        public string Password { get; set; }
        public string KTPImage { get; set; }
        public string KTPNumber { get; set; }
        public string AkteImage { get; set; }
        public string KKImage { get; set; }
        public string KKNumber { get; set; }
        public string ZipCode { get; set; }
        public int? ProvinceID { get; set; }
        public int? DistrictID { get; set; }
        public int? SubDistrictID { get; set; }
        public string EventCode { get; set; }
        public string Remarks { get; set; }
        public string Channel { get; set; }
        public List<AddChildParameterModel> Childs { get; set; }
        public bool IsNewsLetter { get; set; }
        public int? DistrictPointID { get; set; }
        public int? ProductBeforeID { get; set; }
        public string ESignature { get; set; }
        public bool Consent { get; set; }
        public string ReasonRejectKK { get; set; }

    }

    public class AddMemberParameterCMSModel : AddMemberParameterModel
    {
        public int ID { get; set; }
        public string ConfirmPassword { get; set; }
        public string KdNBA { get; set; }
    }

    public class AddMemberSMSParameterModel
    {
        public string ClientID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string ChildDateOfBirth { get; set; }
        public string SMSID { get; set; }
        public string SPGCode { get; set; }
        public string Source { get; set; }
        //add by bry 24022017 for Wet/Dry
        public string ExBrand { get; set; }
        public string SampleType { get; set; }
        public int? DistrictID { get; set; }
    }

    public class EditMemberCMSModel : AddMemberParameterCMSModel
    {
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlackList { get; set; }
    }

    public class SchedulerModel //By Ucup
    {
        public string MemberName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }

    public class ApiScheduler //By Ucup
    {
        public string ClientID { get; set; }
        public List<SchedulerModel> ListMember { get; set; }

    }

    public class PublicAddMemberModel
    {
        public string ClientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Channel { get; set; }
        public string ChildBirthDate { get; set; }
    }

    public class PublicCheckMember
    {
        public string ClientID { get; set; }
        public string PhoneOREmail { get; set; }
    }

    public class ApproveMemberKKImageModel : AddMemberParameterCMSModel
    {
        public bool IsApprovedKKImage { get; set; }
    }
}
