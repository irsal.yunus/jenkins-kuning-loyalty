﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class AddApprovalImageParameterModel
    {
        public int MemberID { get; set; }
        public string KTPImage { get; set; }
        public string AkteImage { get; set; }
        public string KKImage { get; set; }
    }

}
