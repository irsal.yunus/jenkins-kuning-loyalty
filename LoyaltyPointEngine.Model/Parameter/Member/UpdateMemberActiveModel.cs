﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class UpdateMemberActiveModel
    {
        public string ClientID { get; set; }
        public string Key { get; set; }
    }
}
