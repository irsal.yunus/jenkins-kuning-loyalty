﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class EmailByForgotPasswordTokenParamModel
    {
        public string ClientID { get; set; }
        public string Token { get; set; }
    }

    public class SetForgotPasswordTokenByEmailParamModel : EmailByForgotPasswordTokenParamModel
    {
        public string Email { get; set; }
    }

    public class ForgotPasswordByTokenModel
    {
        public string ClientID { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

    }
}
