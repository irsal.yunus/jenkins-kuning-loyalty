﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Member
{
    public class MemberRegisterModel
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string SpgCode { get; set; }
        public string KKPicture { get; set; }
        public int StagesID { get; set; }
        public string ChildName { get; set; }
        public string ChildDOB { get; set; }
        public int AgePregnant { get; set; }
        public int SpgID { get; set; }
        public string FacebookID { get; set; }
        public int? ProductBeforeID { get; set; }
        public string Channel { get; set; }
    }

    public class MemberLongRegisterModel
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string DOB { get; set; }
        public string ChildName { get; set; }
        public string ChildBirthDate { get; set; }
        public string ProfilePicture { get; set; }
        public string Stages { get; set; }
        public string OldEmailBeforeUpdated { get; set; }
        public string subscriber { get; set; }
        public string UsiaKehamilan { get; set; }
        public int? DistrictID { get; set; }
        public int? SubDistrictID { get; set; }
        public string Address { get; set; }
        public int? ProvinceID { get; set; }
    }

    public class MobileRegisterBaseModel
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
    }

    public class MobileRegisterModel : MobileRegisterBaseModel
    {
        public string Phone { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string ChildName { get; set; }
        public string ChildBirthDate { get; set; }
        public string ProfilePicture { get; set; }
        public string SpgCode { get; set; }
        public bool IsSms { get; set; }
        public string ProductBeforeID { get; set; }
        public int StagesID { get; set; }
        public int AgePregnant { get; set; }
        public string OTPCode { get; set; }
    }

    public class MobileRegisterFacebookModel : MobileRegisterBaseModel
    {
        public string FacebookID { get; set; }
        //public string ProfilePicture { get; set; }
    }



    public class ParamSecret
    {
        public string secret { get; set; }

    }
    public class ResultLoginSecret
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class MemberOptOutModel
    {
        public string ClientID { get; set; }
        public string Phone { get; set; }
    }

    public class MemberGetDetailModel
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Phone { get; set; }
    }

    public class MemberRegisterModelWithUtm : MemberRegisterModel
    {
        public string utm_source { get; set; }
        public string utm_medium { get; set; }
        public string utm_campaign { get; set; }
    }
}
