﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartProductPricing : SnapcartField
    {
        [JsonProperty(PropertyName = "quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "base_price")]
        public int? BasePrice { get; set; }

        [JsonProperty(PropertyName = "total_base_price")]
        public int? TotalBasePrice { get; set; }

        [JsonProperty(PropertyName = "total_discount")]
        public int? TotalDiscount { get; set; }
    }
}
