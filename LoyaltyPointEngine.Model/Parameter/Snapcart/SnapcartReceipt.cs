﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartReceipt
    {
        [JsonProperty(PropertyName = "receipt_id")]
        public Guid? ReceiptID { get; set; }

        [JsonProperty(PropertyName = "images")]
        public string[] Images { get; set; }

        [JsonProperty(PropertyName = "total_purchase")]
        public SnapcartTotalPurchase TotalPurchase { get; set; }

        [JsonProperty(PropertyName = "chain")]
        public SnapcartChain Chain { get; set; }        

        [JsonProperty(PropertyName = "purchase_date")]
        public SnapcartPurchaseDate PurchaseDate { get; set; }

        [JsonProperty(PropertyName = "purchase_time")]
        public SnapcartPurchaseTime PurchaseTime { get; set; }

        [JsonProperty(PropertyName = "discounts")]
        public SnapcartDiscount[] Discounts { get; set; }

        [JsonProperty(PropertyName = "products")]
        public SnapcartProduct[] Products { get; set; }

        [JsonProperty(PropertyName = "raw_text")]
        public string RawText { get; set; }    
    }
}
