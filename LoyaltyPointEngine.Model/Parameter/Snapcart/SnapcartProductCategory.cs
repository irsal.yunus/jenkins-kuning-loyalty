﻿using Newtonsoft.Json;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartProductCategory : SnapcartField
    {
        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}
