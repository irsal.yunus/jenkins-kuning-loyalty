﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartInput
    {
        [JsonProperty(PropertyName = "receipt_id")]
        public Guid? ReceiptID { get; set; }

        [JsonProperty(PropertyName = "images")]
        public string[] Images { get; set; }

        [JsonProperty(PropertyName = "http_callback_url")]
        public string HttpCallbackUrl { get; set; }
    }
}
