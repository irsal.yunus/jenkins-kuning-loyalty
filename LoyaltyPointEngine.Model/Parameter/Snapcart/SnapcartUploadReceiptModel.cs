﻿using LoyaltyPointEngine.Model.Parameter.Receipt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartUploadReceiptModel
    {
        public SnapcartUploadReceiptModel()
        {
            this.Products = new List<SnapcartUploadReceiptDetailModel>();
        }

        public Guid CorrelationId { get; set; }
        public Guid ReceiptId { get; set; }
        public Guid ExecutionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public Data.Retailer Retailer { get; set; }
        public string Image { get; set; }
        public List<SnapcartUploadReceiptDetailModel> Products { get; set; }
        public int TotalPrice { get; set; }
    }

    public class SnapcartUploadReceiptDetailModel
    {
        // public Data.Action Action { get; set; }
        public Data.Product Product { get; set; }
        public string Brand { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Variant { get; set; }
        public int? Price { get; set; }
        public int? TotalPrice { get; set; }
        public int? Quantity { get; set; }
        public int? Size { get; set; }
        public string SizeUnit { get; set; }
    }
}
