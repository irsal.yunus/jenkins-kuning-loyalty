﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartDiscount : SnapcartField
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "value")]
        public int Value { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public SnapcartDiscountBrand Brand { get; set; }

        [JsonProperty(PropertyName = "category")]
        public SnapcartDiscountCategory Category { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
    }
}
