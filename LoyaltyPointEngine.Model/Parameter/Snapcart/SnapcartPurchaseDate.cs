﻿using Newtonsoft.Json;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartPurchaseDate : SnapcartField
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}
