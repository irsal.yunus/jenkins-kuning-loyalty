﻿using Newtonsoft.Json;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartProductPackSize : SnapcartField
    {
        [JsonProperty(PropertyName = "size_value")]
        public string SizeValue { get; set; }

        [JsonProperty(PropertyName = "size_unit_id")]
        public string SizeUnitID { get; set; }

        [JsonProperty(PropertyName = "size_unit")]
        public string SizeUnit { get; set; }

        [JsonProperty(PropertyName = "volume")]
        public int? Volume { get; set; }

        [JsonProperty(PropertyName = "volume_unit_id")]
        public string VolumeUnitID { get; set; }

        [JsonProperty(PropertyName = "volume_unit")]
        public string VolumeUnit { get; set; }
    }
}
