﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartField
    {
        [JsonProperty(PropertyName = "score")]
        public double? Score { get; set; }
    }
}
