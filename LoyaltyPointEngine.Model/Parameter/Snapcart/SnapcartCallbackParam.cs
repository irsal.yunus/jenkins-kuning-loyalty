﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartCallbackParam
    {
        [JsonProperty(PropertyName = "receipt")]
        public SnapcartReceipt Receipt { get; set; }

        [JsonProperty(PropertyName = "input")]
        public SnapcartInput Input { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "receipt_id")]
        public Guid? ReceiptID { get; set; }

        [JsonProperty(PropertyName = "request_id")]
        public Guid? RequestID { get; set; }

        [JsonProperty(PropertyName = "request_time")]
        public string RequestTime { get; set; }

        [JsonProperty(PropertyName = "ttl")]
        public string Ttl { get; set; }

        [JsonProperty(PropertyName = "output_url")]
        public string OutputUrl { get; set; }

        [JsonProperty(PropertyName = "failure_reason")]
        public string FailureReason { get; set; }
    }
}
