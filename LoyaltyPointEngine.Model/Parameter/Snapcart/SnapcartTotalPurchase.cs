﻿using Newtonsoft.Json;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartTotalPurchase : SnapcartField
    {
        [JsonProperty(PropertyName = "value")]
        public double? Value { get; set; }
    }
}
