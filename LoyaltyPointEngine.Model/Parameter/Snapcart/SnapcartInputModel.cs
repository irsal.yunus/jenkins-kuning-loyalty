﻿using LoyaltyPointEngine.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartInputModel
    {
        public SnapcartInputModel()
        {
            this.HttpCallbackUrl = SiteSetting.SnapcartCallbackUrl;
        }

        //[JsonProperty(PropertyName = "correlation_id")]
        //public Guid? CorrelationID { get; set; }

        [JsonProperty(PropertyName = "receipt_id")]
        public Guid? ReceiptID { get; set; }

        [JsonProperty(PropertyName = "http_callback_url")]
        public string HttpCallbackUrl { get; set; }

        [JsonProperty(PropertyName = "images")]
        public string[] Images { get; set; }
    }
}
