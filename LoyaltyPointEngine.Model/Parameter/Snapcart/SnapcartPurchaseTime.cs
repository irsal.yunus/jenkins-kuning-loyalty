﻿using Newtonsoft.Json;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartPurchaseTime : SnapcartField
    {
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }
    }
}
