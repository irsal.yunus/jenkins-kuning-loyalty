﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Snapcart
{
    public class SnapcartProduct
    {
        [JsonProperty(PropertyName = "name")]
        public SnapcartProductName Name { get; set; }

        [JsonProperty(PropertyName = "pricing")]
        public SnapcartProductPricing Pricing { get; set; }

        [JsonProperty(PropertyName = "brand")]
        public SnapcartProductBrand Brand { get; set; }

        [JsonProperty(PropertyName = "category")]
        public SnapcartProductCategory Category { get; set; }

        [JsonProperty(PropertyName = "packsize")]
        public SnapcartProductPackSize PackSize { get; set; }

        [JsonProperty(PropertyName = "variant")]
        public SnapcartProductVariant Variant { get; set; }

        [JsonProperty(PropertyName = "sku")]
        public SnapcartProductSKU SKU { get; set; }
    }
}
