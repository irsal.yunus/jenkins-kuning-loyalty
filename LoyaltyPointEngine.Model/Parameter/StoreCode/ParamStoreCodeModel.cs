﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.StoreCode
{
    public class ParamStoreCodeModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public int ClusterId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
