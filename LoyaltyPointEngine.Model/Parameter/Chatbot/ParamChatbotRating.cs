﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Chatbot
{
    public class ParamChatbotRating
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string Phone { get; set; }
        public string Rating { get; set; }
        public string Input { get; set; }
        
    }

    
}
