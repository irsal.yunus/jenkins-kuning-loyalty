﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Redeem
{
    public class SubmitRedeemModel
    {
        public Guid RedemptionID { get; set; }
        public string TransactionCode { get; set; }
        public int MemberID { get; set; }
        public int MemberPoint { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverAddress { get; set; }
        public string UserId { get; set; }
    }
}
