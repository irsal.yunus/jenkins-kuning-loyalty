﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Redeem
{
    public class SubmitRedeemParameterModel
    {
        public string TransactionCode { get; set; }
        public int Point { get; set; }
    }

    public class SubmitRedeemParameterCMSModel : SubmitRedeemParameterModel
    {
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public string StrTransactionDate { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}
