﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Data;

namespace LoyaltyPointEngine.Model.Parameter.BalanceLog
{
    public class BalanceLog
    {
        public int Balance { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public EntityKey EntityKey { get; set; }
        public Guid ID { get; set; }
        public int MemberID { get; set; }
        public int Points { get; set; }
        public Pool Pool { get; set; }
        public int PoolID { get; set; }
        public EntityReference<Pool> PoolReference { get; set; }
        public Guid ReferenceID { get; set; }
        public string Type { get; set; }
    }
}
