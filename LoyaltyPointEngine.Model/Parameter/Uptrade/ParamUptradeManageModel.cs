﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Uptrade
{
    public class ParamUptradeManageModel
    {
        public long ID { get; set; }
        public string Channel { get; set; }
        public long ProductAfter { get; set; }
        public long ProductBefore { get; set; }
        public long Point { get; set; }
        public long FlagID { get; set; }
        public string Remarks { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }
}
