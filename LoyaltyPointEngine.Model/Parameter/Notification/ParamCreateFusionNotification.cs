﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elasticsearch;
using Nest;

namespace LoyaltyPointEngine.Model.Parameter.Notification
{
    [ElasticsearchType(Name = "notification", IdProperty = nameof(NotificationID))]
    public class ParamCreateFusionNotification
    {
        /// <summary>
        /// NotificationID
        /// </summary>
        public string NotificationID { get; set; }
        /// <summary>
        /// MemberID
        /// </summary>
        public long MemberID { get; set; }
        /// <summary>
        /// NotificationDate
        /// </summary>
        public DateTime NotificationDate { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// SubTitle
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// Detail
        /// </summary>
        public string Detail { get; set; }
        /// <summary>
        /// NotificationType
        /// </summary>
        public string NotificationType { get; set; }

        /// <summary>
        /// ReferenceID
        /// </summary>
        public string ReferenceID { get; set; }
        /// <summary>
        /// ReferenceCode
        /// </summary>
        public string ReferenceCode { get; set; }
        /// <summary>
        /// Point
        /// </summary>
        public long Point { get; set; }
        /// <summary>
        /// LastPoint
        /// </summary>
        public long LastPoint { get; set; }
        /// <summary>
        /// IsRead
        /// </summary>
        public bool IsRead { get; set; }
        /// <summary>
        /// LeftIcon
        /// </summary>
        public string LeftIcon { get; set; }
        /// <summary>
        /// RightIcon
        /// </summary>
        public string RightIcon { get; set; }
        /// <summary>
        /// SourceData
        /// </summary>
        public string SourceData { get; set; }
    }
}
