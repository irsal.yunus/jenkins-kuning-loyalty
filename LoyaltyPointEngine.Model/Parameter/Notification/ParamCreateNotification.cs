﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Notification
{
    public class ParamCreateNotification
    {
        public Guid? ID { get; set; }
        // public int MemberID { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string ReferenceCode { get; set; }
        public int Point { get; set; }
        public string LeftIcon { get; set; }
        public string RightIcon { get; set; }

        public string FusionTitle { get; set; }
        public string SubTitle { get; set; }
        public string Detail { get; set; }
        public string FusionReferenceCode { get; set; }
        public string ReferenceId { get; set; }
        public int? LastPoint { get; set; }
        public string SourceData { get; set; }
        public string LinkUrl { get; set; }
    }

    public class ParamSendNotification : ParamCreateNotification
    {
        public string MemberIDs { get; set; }
        public string MemberNames { get; set; }
        public bool ToAllMembers { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class ParamCreateNotificationAPI
    {
        public int MemberID { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string ReferenceCode { get; set; }
        public int Point { get; set; }
        public string LeftIcon { get; set; }
        public string RightIcon { get; set; }
    }
    public class ParamNotifTelcomate : ParamCreateNotificationAPI
    {
        public string MemberPhone { get; set; }
        public string Url { get; set; }
    }
}
