﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Parameter.Consent
{
    public class ConsentModel
    {
        public Guid ID { get; set; }
        public int MemberId { get; set; }
        public bool IsAcceptCookie { get; set; }
    }
}
