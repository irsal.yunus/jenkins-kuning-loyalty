﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Product
{
    public class ProductModel
    {
        public ProductModel()
        {

        }

        public ProductModel(Data.Product data)
        {
            if (data != null)
            {
                this.ID = data.ID;
                this.ProductGroupID = data.ProductGroupID;
                this.Code = data.Code;
                this.Name = data.Name;
                this.Description = data.Description;
                this.ProductImage = data.ProductImage;
                this.Size = data.Size;
                this.Flavour = data.Flavour;
                this.IsDisplay = data.IsDisplay;
                this.Price = data.Price;
                this.IsPregnancyProduct = data.IsPregnancyProduct;
                this.SnapcartSKU = data.SnapcartSKU;
            }

        }

        public int ID { get; set; }
        public int ProductGroupID { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProductImage { get; set; }
        public string Size { get; set; }
        public string Flavour { get; set; }
        public bool IsDisplay { get; set; }
        public int? Price { get; set; }
        public bool IsPregnancyProduct { get; set; }
        public string SnapcartSKU { get; set; }
    }





}
