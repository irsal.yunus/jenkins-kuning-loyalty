﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Product
{
    public enum Size
    {
//        1000 g
//1800 g
//200 g
//400 g
//800 g
        g200 = 0,
        g400 = 1,
        g800= 2,
        g1000 = 4,
        g1800 = 5,
        g160 = 6,
        g360 = 7,
        g700 = 8,
    }
}
