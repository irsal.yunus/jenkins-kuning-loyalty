﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Product
{
    public class ProductGroupModel
    {
        public ProductGroupModel()
        {

        }

        public ProductGroupModel(Data.ProductGroup data)
        {
            if(data != null)
            {
                this.ID = data.ID;
                this.ProductGroupCode = data.ProductGroupCode;
                this.IsActived = data.IsActived;
            }

        }

        public int ID { get; set; }      
        public string ProductGroupCode { get; set; }
        public bool IsActived { get; set; }      
    }
}
