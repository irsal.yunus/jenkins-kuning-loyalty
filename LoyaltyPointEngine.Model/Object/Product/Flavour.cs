﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Product
{
    public enum Flavour
    {

        //        COKLAT
        //MADU
        //VANILLA

        COKLAT = 0,
        VANILLA = 1,
        MADU = 2,
    }
}
