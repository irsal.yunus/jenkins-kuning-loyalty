﻿using LoyaltyPointEngine.Model.Object.Catalogue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.WishList
{
    public class WishListModel
    {
        public WishListModel() { }

        public long ID { get; set; }
        public int CatalogueID { get; set; }
        public int MemberID { get; set; }
        public bool IsRedeemed { get; set; }
        public bool IsDeleted { get; set; }
        public CatalogueModel Catalogue { get; set; }
    }
}
