﻿using LoyaltyPointEngine.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LoyaltyPointEngine.Model.Object.CMSNavigation
{

    public class NavigationModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Controller { get; set; }
        public int sort { get; set; }
        public long? ParentID { get; set; }
        public bool IsHide { get; set; }
        public long[] Previlage { get; set; }
    }

    public class RoleModel
    {
        public RoleModel()
        {

        }

        public RoleModel(long _ID)
        {
          
            List<long> NavigationPrevilage = new List<long>();
            var RoleNagivationPrevilage = CMSRoleNavigationPrivilegeMapping.GetByRole(_ID).ToList();
            if (RoleNagivationPrevilage != null)
            {
                foreach (CMSRoleNavigationPrivilegeMapping _RoleNavigationPrevilage in RoleNagivationPrevilage)
                {
                    NavigationPrevilage.Add(_RoleNavigationPrevilage.CMSNavigationPrivilegeID);
                }

                this.NavigationPrevilage = NavigationPrevilage.ToArray();
            }
        }

        public long ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public long[] NavigationPrevilage { get; set; }
        public int? Level { get; set; }
    }

    public class PrivilegeModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}