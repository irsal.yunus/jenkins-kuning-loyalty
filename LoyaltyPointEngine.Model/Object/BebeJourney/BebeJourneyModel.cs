﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace LoyaltyPointEngine.Model.Object.BebeJourney
{
    public class BebeJourneyModel
    {
        public int ID { get; set; }
        public int MemberID { get; set; }
        public string Question { get; set; }
        public string Value { get; set; }
        public DateTime CreatedDate { get; set; }
        public string KTPImage { get; set; }
        public string AkteImage { get; set; }
    }
}
