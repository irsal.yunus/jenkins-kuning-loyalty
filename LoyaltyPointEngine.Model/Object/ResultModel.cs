﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object
{
    public class ResultModel<T>
    {
        public ResultModel()
        {

        }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public T Value { get; set; }
    }

    public class ResultPaginationModel<T>:ResultModel<T>
    {
        public int TotalRow { get; set; }
    }
}
