﻿using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Consent
{
    public class ConsentReportModel
    {
        [Name("Consent ID")]
        public Guid Id { get; set; }
        [Name("Member Name")]
        public string Name { get; set; }
        [Name("Member Phone")]
        public string Phone { get; set; }
        [Name("Accept Date")]
        public DateTime? AcceptDate { get; set; }
    }
    public sealed class ConsentReportCsvModel : ClassMap<ConsentReportModel>
    {
        public ConsentReportCsvModel()
        {
            AutoMap();
            Map(m => m.AcceptDate).TypeConverterOption.Format("dd/MM/yyyy hh:mm:ss tt");
        }
    }
}
