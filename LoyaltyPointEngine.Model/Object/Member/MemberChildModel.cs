﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class MemberChildModel
    {
        public int ChildID { get; set; }
        public string ChildName { get; set; }
    }
}