﻿using LoyaltyPointEngine.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class BaseMemberModel
    {
        public BaseMemberModel() { }

        public BaseMemberModel(DataMember.Member data)
        {
            if (data != null)
            {
                this.ID = data.ID;


                this.FirstName = data.FirstName;
                this.LastName = data.LastName;
                this.Email = data.Email;
                this.Phone = data.Phone;

            }
        }

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }


    /// <summary>
    /// For API
    /// </summary>
    public class MemberModel : BaseMemberModel
    {
        public MemberModel() { }

        public MemberModel(DataMember.Member data)
            : base(data)
        {
            if (data != null)
            {
                this.Gender = data.Gender;
                this.FacebookID = data.FacebookID;
                this.StagesID = data.StagesID;
                this.StagesValue = data.StagesValue;
                this.Address = data.Address;
                this.SpgID = data.SpgID;
                this.Birthdate = data.Birthdate.HasValue ? data.Birthdate.Value.ToString("dd/MM/yyyy") : "";
                this.PlaceOfBirth = data.PlaceOfBirth;
                this.ProfilePicture = data.ProfilePicture;
                this.IsApproved = data.IsApproved;
                this.IsActive = data.IsActive;
                this.IsBlacklist = data.IsBlacklist;
                this.ChannelId = data.ChannelId;
                this.KTPImage = data.KTPImage;
                this.KTPNumber = data.KTPNumber;
                this.KKImage = data.KKImage;
                this.AkteImage = data.AkteImage;
                this.KKNumber = data.KKNumber;
                this.ProvinceID = data.ProvinceID;
                this.DistrictID = data.DistrictID;
                this.SubDistrictID = data.SubDistrictID;
                this.ZipCode = data.ZipCode;
                this.IsAlfamart = Convert.ToInt32(data.IsAlfamart);
                this.Subscriber = data.subscribe;
                this.UsiaKehamilan = data.UsiaKehamilan;
                this.AcceptDate = data.AcceptDate;
                this.MobileFirstLoginDate = data.MobileFirstLoginDate;
                this.ProductBeforeID = Convert.ToInt32(data.ProductBeforeID);
                this.RatingDate = data.RatingDate != null ? data.RatingDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                this.RejectDate = data.RejectDate;
                this.RejectBy = data.RejectBy;
                this.ReasonRejectMsg = data.ReasonRejectKK;

                var childs = DataMember.Child.GetByParentId(this.ID).ToList();
                if (childs != null && childs.Count > 0)
                {
                    var child = childs.FirstOrDefault();
                    this.ChildBirthDate = child.Birthdate.HasValue ? child.Birthdate.Value.ToString("dd/MM/yyyy") : string.Empty;
                    this.ChildName = child.Name;
                }
            }
        }

        public string Gender { get; set; }
        public int ChannelId { get; set; }
        public string FacebookID { get; set; }
        public int? StagesID { get; set; }
        public string StagesValue { get; set; }
        public string Address { get; set; }
        public int? City { get; set; }
        public int? SpgID { get; set; }
        public string Birthdate { get; set; }
        public string PlaceOfBirth { get; set; }
        public string ProfilePicture { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsBlacklist { get; set; }
        public int IsAlfamart { get; set; }
        public string KTPImage { get; set; }
        public string KTPNumber { get; set; }
        public string KKImage { get; set; }
        public string AkteImage { get; set; }
        public string KKNumber { get; set; }
        public int? ProvinceID { get; set; }
        public int? DistrictID { get; set; }
        public int? SubDistrictID { get; set; }
        public string ZipCode { get; set; }
        public string responsemsg { get; set; }
        public string ChildBirthDate { get; set; }
        public string ChildName { get; set; }
        public string Subscriber { get; set; }
        public string UsiaKehamilan { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? MobileFirstLoginDate { get; set; }
        public int? ProductBeforeID { get; set; }
        public string RatingDate { get; set; }
        public DateTime? RejectDate { get; set; }
        public string RejectBy { get; set; }
        public string ReasonRejectMsg { get; set; }
    }

    /// <summary>
    /// FOR API
    /// </summary>
    public class MemberDetailModel : MemberModel
    {
        public MemberDetailModel() { }

        public MemberDetailModel(DataMember.Member data)
            : base(data)
        {
            if (data != null)
            {
                var childs = DataMember.Child.GetByParentId(this.ID).ToList();
                if (childs != null && childs.Count > 0)
                {
                    this.Childs = childs.Translated();
                }

                if (data.SPG != null)
                {
                    this.SalesPerson = new SalesPersonModel(data.SPG);
                }

                if (data.Stages != null)
                {
                    this.StagesValue = data.Stages.Name;
                    this.Stages = new StagesModel(data.Stages);
                }

                if (data.Channel != null)
                {
                    this.Channel = new ChannelModel(data.Channel);
                }

                if (data.ProductBeforeID != null)
                {
                    var product = DataMember.ProductBefore.GetById(data.ProductBeforeID.Value);
                    if (product != null)
                    {
                        this.Productbefore = new ProductBeforeModel(product);
                    }
                }

                if (data.SubDistrict != null)
                {
                    this.SubDistrict = new SubDistrictModel(data.SubDistrict);
                    var district = DataMember.District.GetByID(data.SubDistrict.DistrictID);
                    if (district != null)
                    {
                        this.District = new DistrictModel(district);
                        var province = DataMember.Province.GetByID(district.ProvinceID);
                        if (province != null)
                        {
                            this.Province = new ProvinceModel(province);
                        }
                    }
                }
                this.Point = Data.BalanceLog.GetTotalPointByMemberId(this.ID);
                this.PointExpireDate = Data.Collect.GetPointsToBeExpiredDateByMemberID(this.ID);
                if (PointExpireDate != new DateTime(DateTime.Now.Year, 12, 31))
                {
                    this.PointToBeExpired = Data.Collect.GetPointsToBeExpiredByMemberID(this.ID);
                }
                this.AgePregnant = data.AgePregnant;
            }
        }

        public ChannelModel Channel { get; set; }
        public List<ChildModel> Childs { get; set; }
        public SalesPersonModel SalesPerson { get; set; }
        public StagesModel Stages { get; set; }
        public SubDistrictModel SubDistrict { get; set; }
        public DistrictModel District { get; set; }
        public ProvinceModel Province { get; set; }
        public ProductBeforeModel Productbefore { get; set; }
        public int Point { get; set; }
        public int PointToBeExpired { get; set; }
        public DateTime PointExpireDate { get; set; }
        public int CountUpload { get; set; }
        public int? AgePregnant { get; set; }

    }

    public class MemberGridModel
    {
        private string _FirstName;
        private string _LastName;

        public int ID { get; set; }
        public string Gender { get; set; }
        public string FirstName { get { return _FirstName; } set { _FirstName = StringHelper.SanitizeFromWhiteSpace(value); } }
        public string LastName { get { return _LastName; } set { _LastName = StringHelper.SanitizeFromWhiteSpace(value); } }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsBlacklist { get; set; }
        public int? Point { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Type { get; set; }
        public int IsAlfamart { get; set; }
        public string SPGCode { get; set; }
        public string Outlet { get; set; }
        public string SPGName { get; set; }
        public string ChildName { get; set; }
        public DateTime? ChildBOD { get; set; }
        public string Channel { get; set; }
        public string Message { get; set; }
        public string EventCode { get; set; }
        public bool? IsApprovedKKImage { get; set; }
        public string IsActive { get; set; }
        public DateTime? ActiveDate { get; set; }
        public bool IsUploadedKTP { get; set; }
        public bool IsUploadedKK { get; set; }
        public bool IsUploadedAkte { get; set; }
       
    }

    public class MemberSMSModel : BaseMemberModel
    {
        public MemberSMSModel(DataMember.Member data, string NewPassword)
            : base(data)
        {
            if (data != null)
            {
                this.Password = NewPassword;
                this.SMSID = data.SMSID;
            }
        }
        public string Password { get; set; }
        public string SMSID { get; set; }
        public string EventType { get; set; }
        public string EventMsg { get; set; }
    }

    public class MemberUploadTemplateModel
    {
        public string fbid { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public string spgCode { get; set; }
        public string spgName { get; set; }
        public string OutletName { get; set; }
        public string spgLeaderName { get; set; }
        public string momBOD { get; set; }
        public string BirthPlace { get; set; }
        public string momRegisterDate { get; set; }
        public string Point { get; set; }
        public string ChildName { get; set; }  //ucup
        public string Placeofbirth { get; set; } //ucup
        public string ChildDOB { get; set; }
        public string Channel { get; set; }
        public string agepregnant { get; set; } //RX-363
        public string EventCode { get; set; }
        public string ProductBefore { get; set; }
    }
}
