﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public static class MemberTranslator
    {
        /// <summary>
        /// transform list object data layer 'Member' to List MemberModel
        /// </summary>
        /// <param name="data">List DataMember.Member</param>
        /// <returns></returns>
        public static List<MemberModel> Translated(this List<DataMember.Member> data)
        {
            List<MemberModel> result = new List<MemberModel>();

            foreach (var item in data)
            {
                result.Add(new MemberModel(item));
            }

            return result;

        }



    }
}
