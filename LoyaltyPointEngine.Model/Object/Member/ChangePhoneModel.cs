﻿using LoyaltyPointEngine.Model.Parameter.AccessToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class ChangePhoneModel : AccessTokenParamModel
    {
        public string OldPhone { get; set; }
        public string NewPhone { get; set; }
        public string ClientID { get; set; }
    }
}
