﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class SalesPersonModel
    {
        public SalesPersonModel()
        {

        }

        public SalesPersonModel(DataMember.SPG data)
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.Name = data.Name;
            this.DistrictID = Convert.ToInt32(!string.IsNullOrEmpty(data.District) ? data.District : "0");
            this.OutletName = data.OutletName;
            this.LeaderName = data.LeaderName;
            this.DistrictID = Convert.ToInt32(!string.IsNullOrEmpty(data.District) ? data.District : "0");
            this.IsActive = data.IsActive.GetValueOrDefault(true);
        }

        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int DistrictID { get; set; }
        public string OutletName { get; set; }
        public string LeaderName { get; set; }
        public bool IsActive { get; set; }
    }
}
