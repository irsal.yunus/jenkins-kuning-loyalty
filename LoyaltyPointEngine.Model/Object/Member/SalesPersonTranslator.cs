﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public static class SalesPersonTranslator
    {
        /// <summary>
        /// transform list object data layer 'SalesPerson' to List SalesPersonModel
        /// </summary>
        /// <param name="data">List DataMember.SPG</param>
        /// <returns></returns>
        public static List<SalesPersonModel> Translated(this List<DataMember.SPG> data)
        {
            List<SalesPersonModel> result = new List<SalesPersonModel>();

            foreach (var item in data)
            {
                result.Add(new SalesPersonModel(item));
            }

            return result;

        }


    }
}
