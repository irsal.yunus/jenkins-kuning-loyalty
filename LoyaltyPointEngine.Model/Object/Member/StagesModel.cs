﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class StagesModel
    {
          public StagesModel()
        {

        }

          public StagesModel(DataMember.Stages data)
        {
            this.ID = data.ID;
            this.Name = data.Name;
            this.AttributeName = data.AttributeName;
            this.IsActive = data.IsActive;

        }
          public int ID { get; set; }
        public string Name { get; set; }
        public string AttributeName { get; set; }
        public bool  IsActive { get; set; }

    }
}
