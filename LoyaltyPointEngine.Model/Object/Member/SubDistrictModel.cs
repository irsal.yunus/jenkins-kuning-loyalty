﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class SubDistrictModel
    {
         public SubDistrictModel()
        {

        }

        public SubDistrictModel(DataMember.SubDistrict data)
        {
            this.ID = data.ID;
            this.SubDistrictCode = data.SubDistrictCode;
            this.SubDistrictName = data.SubDistrictName;
            this.CreatedDate = data.CreatedDate;
            this.DistrictID = data.DistrictID;
          
            if(data.District != null)
            {
                this.District = new DistrictModel(data.District);
                this.ProvinceID = data.District.ProvinceID;
            }

        }
        public int ID { get; set; }

        [Required(ErrorMessage = "SubDistrictCode is required.")]
        [MaxLength(20, ErrorMessage = "SubDistrictCode cannot be longer than 20 characters.")]
        public string SubDistrictCode { get; set; }

        [Required(ErrorMessage = "SubDistrictName is required.")]
        [MaxLength(50, ErrorMessage = "SubDistrictName cannot be longer than 50 characters.")]       
        public string SubDistrictName { get; set; }

        public int DistrictID { get; set; }
        public DistrictModel District { get; set; }
        public DateTime CreatedDate { get; set; }


        public int ProvinceID { get; set; }
    }
}
