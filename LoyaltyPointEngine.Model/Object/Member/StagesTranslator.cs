﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
   public static class StagesTranslator
    {
        /// <summary>
        /// transform list object data layer 'Stages' to List StagesModel
        /// </summary>
        /// <param name="data">List DataMember.Stages</param>
        /// <returns></returns>
       public static List<StagesModel> Translated(this List<DataMember.Stages> data)
        {
            List<StagesModel> result = new List<StagesModel>();

            foreach (var item in data)
            {
                result.Add(new StagesModel(item));
            }

            return result;

        }
    }
}
