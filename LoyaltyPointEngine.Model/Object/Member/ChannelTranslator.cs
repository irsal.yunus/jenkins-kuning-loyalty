﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public static class ChannelTranslator
    {
        /// <summary>
        /// transform list object data layer 'channel' to List StagesModel
        /// </summary>
        /// <param name="data">List DataMember.channel</param>
        /// <returns></returns>
        public static List<ChannelModel> Translated(this List<DataMember.Channel> data)
        {
            List<ChannelModel> result = new List<ChannelModel>();

            foreach (var item in data)
            {
                result.Add(new ChannelModel(item));
            }

            return result;

        }
    }
}
