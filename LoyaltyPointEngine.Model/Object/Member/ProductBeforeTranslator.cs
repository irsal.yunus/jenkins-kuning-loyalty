﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public static class ProductBeforeTranslator
    {
        /// <summary>
        /// transform list object data layer 'Stages' to List StagesModel
        /// </summary>
        /// <param name="data">List DataMember.Stages</param>
        /// <returns></returns>
        public static List<ProductBeforeModel> Translated(this List<DataMember.ProductBefore> data)
        {
            List<ProductBeforeModel> result = new List<ProductBeforeModel>();

            foreach (var item in data)
            {
                result.Add(new ProductBeforeModel(item));
            }

            return result;

        }
    }
}
