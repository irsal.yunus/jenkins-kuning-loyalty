﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class DistrictModel
    {
        public DistrictModel()
        {

        }

        public DistrictModel(DataMember.District data)
        {
            this.ID = data.ID;
            this.DistrictCode = data.DistrictCode;
            this.DistrictName = data.DistrictName;
            this.CreatedDate = data.CreatedDate;
            if (data.Province != null)
            {
                this.Province = new ProvinceModel(data.Province);
            }
            this.ProvinceID = data.ProvinceID;

        }
        public int ID { get; set; }

        [Required(ErrorMessage = "DistrictCode is required.")]
        [MaxLength(20, ErrorMessage = "DistrictCode cannot be longer than 20 characters.")]
        public string DistrictCode { get; set; }

        [Required(ErrorMessage = "DistrictName is required.")]
        [MaxLength(50, ErrorMessage = "DistrictName cannot be longer than 50 characters.")]
        public string DistrictName { get; set; }


        public int ProvinceID { get; set; }

        public ProvinceModel Province { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
