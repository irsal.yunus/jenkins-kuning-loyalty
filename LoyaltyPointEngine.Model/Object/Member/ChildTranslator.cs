﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public static class ChildTranslator
    {
        /// <summary>
        /// transform list object data layer 'Child' to List ChildModel
        /// </summary>
        /// <param name="data">List DataMember.Child</param>
        /// <returns></returns>
        public static List<ChildModel> Translated(this List<DataMember.Child> data)
        {
            List<ChildModel> result = new List<ChildModel>();

            foreach (var item in data)
            {
                result.Add(new ChildModel(item));
            }

            return result;

        }
    }
}
