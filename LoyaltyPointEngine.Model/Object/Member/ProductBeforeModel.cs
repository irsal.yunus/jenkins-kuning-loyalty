﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class ProductBeforeModel
    {
        public ProductBeforeModel()
        {

        }

        public ProductBeforeModel(DataMember.ProductBefore data)
        {
            this.ID = data.ID;
            this.Name = data.Name;
            this.IsDeleted = data.IsDeleted;
            this.IsPregnancyProduct = data.IsPregnancyProduct;

        }
        public long ID { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsPregnancyProduct { get; set; }
    }
}
