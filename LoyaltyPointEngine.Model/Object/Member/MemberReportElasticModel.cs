﻿using Nest;
using System;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;

namespace LoyaltyPointEngine.Model.Object.Member
{
    [ElasticsearchType(Name = "doc")]
    public class MemberReportElasticModel
    {
        [Name("MemberID")]
        public int ID { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsBlackList { get; set; }
        public int? Point { get; set; }
        public string SpgCode { get; set; }
        public string SpgName { get; set; }
        [Text(Name = "spgoutletname")]
        public string OutletName { get; set; }
        [Text(Name = "channelname")]
        public string Channel { get; set; }
        public int ChannelId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ChildName { get; set; }
        [Text(Name = "childdob")]
        public DateTime? ChildBirthDate { get; set; }
        public string Remarks { get; set; }
        public DateTime? RemarksCreatedDate { get; set; }
        public string ProductBeforeIbu { get; set; }
        public string ProductBeforeAnak { get; set; }
        public string EventCode { get; set; }
        public string CodeGimmick { get; set; }
        public string ClusterName { get; set; }
        public string Stages { get; set; }
        public int AgePregnant { get; set; }
        [CsvHelper.Configuration.Attributes.Ignore]
        public bool IsActive { get; set; }
        [Name("IsActive")]
        public string IsActiveString { get; set; }

        public string StoreCode { get; set; }
        public string NamaToko { get; set; }
        public string ClusteridStore { get; set; }
        public string IsActiveStore { get; set; }
        [Text(Name = "KTPUpload")]
        public string KTPNumber { get; set; }
        public string IsApprovedKK { get; set; }
        public string IsUploadedAkte { get; set; }
        public string ReasonRejectKK { get; set; }
        [Text(Name = "Source")]
        public string Source { get; set; }
        [Text(Name = "Touchpoint")]
        public string Medium { get; set; }
        [Text(Name = "ActivityPoint")]
        public string Campaign { get; set; }
    }

    public sealed class MemberReportElasticCsvMapModel : ClassMap<MemberReportElasticModel>
    {
        public MemberReportElasticCsvMapModel()
        {
            AutoMap();
            Map(m => m.CreatedDate).TypeConverterOption.Format("dd/MM/yyyy hh:mm:ss tt");
            Map(m => m.ChildBirthDate).ConvertUsing(x =>
            {
                string ChildBirthDate = x.GetField<DateTime?>("ChildBirthDate").Value.ToString("dd/MM/yyyy");
                if (ChildBirthDate.Contains("01/01/0001"))
                {
                    return null;
                }
                else
                {
                    return x.GetField<DateTime?>("ChildBirthDate");
                }
            }).TypeConverterOption.Format("dd/MM/yyyy HH:mm").TypeConverterOption.NullValues("");
            Map(m => m.RemarksCreatedDate).ConvertUsing(x =>
            {
                string RemarksCreatedDate = x.GetField<DateTime?>("RemarksCreatedDate").Value.ToString("dd/MM/yyyy");
                if (RemarksCreatedDate.Contains("01/01/0001"))
                {
                    return null;
                }
                else
                {
                    return x.GetField<DateTime?>("RemarksCreatedDate");
                }
            }).TypeConverterOption.Format("dd/MM/yyyy HH:mm").TypeConverterOption.NullValues("");
        }
    }
}
