﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class ChildModel
    {
        public ChildModel()
        {

        }

        public ChildModel(DataMember.Child data)
        {
            if (data != null)
            {
                this.ID = data.ID;
                this.ParentID = data.ParentID;
                this.Gender = data.Gender;
                this.Name = data.Name;
                this.PlaceOfBirth = data.PlaceOfBirth;
                this.Birthdate = data.Birthdate.HasValue ? data.Birthdate.Value.ToString("dd/MM/yyyy"):"";
                this.ProductBeforeID = data.ProductBeforeID.HasValue ? data.ProductBeforeID.Value : 0;
                if (data.ProductBeforeID != null)
                {
                    var product = DataMember.ProductBefore.GetById(data.ProductBeforeID.Value);
                    if (product != null)
                    {
                        this.Productbefore = new ProductBeforeModel(product);
                    }
                }
            }
        }
        public int ID { get; set; }
        public int ParentID { get; set; }
        public string Gender { get; set; }
        public string Name { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Birthdate { get; set; }
        public int ProductBeforeID { get; set; }
        public ProductBeforeModel Productbefore { get; set; }

    }
}
