﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class ProvinceModel
    {
        public ProvinceModel()
        {

        }

        public ProvinceModel(DataMember.Province data)
        {
            this.ID = data.ID;
            this.ProvinceCode = data.ProvinceCode;
            this.ProvinceName = data.ProvinceName;
            this.CreatedDate = data.CreatedDate;

        }
        public int ID { get; set; }

        [Required(ErrorMessage = "ProvinceCode is required.")]
        [MaxLength(20, ErrorMessage = "ProvinceCode cannot be longer than 20 characters.")]
        public string ProvinceCode { get; set; }

        [Required(ErrorMessage = "ProvinceName is required.")]
        [MaxLength(50, ErrorMessage = "ProvinceName cannot be longer than 50 characters.")]       
        public string ProvinceName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
