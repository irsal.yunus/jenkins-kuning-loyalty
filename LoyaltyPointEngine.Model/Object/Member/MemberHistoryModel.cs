﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Member
{
    public class MemberHistoryModel
    {
        public MemberHistoryModel() { }

        public string Type { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public int Balance { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
