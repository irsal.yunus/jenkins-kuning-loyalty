﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.OCR
{
    public class OCRTemplateModel
    {
        public OCRTemplateModel()
        {

        }

        public OCRTemplateModel(Data.OCRTemplate data)
        {
    

            this.Id = data.ID;
            this.StoreName = data.StoreName;
            this.IsMultiLine = data.IsMultiLine.HasValue && data.IsMultiLine.Value == 1 ? true : false;
            this.MultiLinePositionPrice = data.MultiLinePositionPrice;
            this.PositionQuantity = data.PositionQuantity;
            this.PositionPrice = data.PositionPrice;
            this.Seperator = data.Seperator;
            this.RefKeyword = data.RefKeyword;
            this.DateKeyword = data.DateKeyword;
            this.TimeKeyword = data.TimeKeyword;
            this.IsMultilineDate = data.IsMultilineDate.HasValue && data.IsMultilineDate.Value == 1 ? true : false;
            this.DatePosition = data.datePosition;
            this.TimePosition = data.timePosition;
            this.SeparatorDate = data.separatorDate;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.UpdatedBy = data.UpdatedBy;
            this.UpdatedDate = data.UpdatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.IsDeleted = data.IsDeleted;
        }
        
        public long Id { get; set; }
        public string StoreName { get; set; }
        public bool IsMultiLine { get; set; }
        public int? MultiLinePositionPrice { get; set; }
        public int? PositionQuantity { get; set; }
        public int? PositionPrice { get; set; }
        public string Seperator { get; set; }
        public string RefKeyword { get; set; }
        public string DateKeyword { get; set; }
        public string TimeKeyword { get; set; }
        public bool IsMultilineDate { get; set; }
        public int? DatePosition { get; set; }
        public int? TimePosition { get; set; }
        public string SeparatorDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public bool IsDeleted { get; set; }
        public List<ProductInTemplate> listProductInTemplate { get; set; }
    }

    public class OCRResult
    {
        public string StoreName { get; set; }        
        public string TransactionDate { get; set; }
        public string TransactionId { get; set; }
        public List<OCRReceipt> ListProduct { get; set; }
        
    }

    public class OCRReceipt
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public string Price { get; set; }
        public double Total { get; set; }
        public int PointEach { get; set; }
        public string ActionCode { get; set; }
        public int ActionId { get; set; }
        public string ProductGroupName { get; set; }
        public string ProductSize { get; set; }

    }

    public class NutrilonTemplate
    {
        public string ID { get; set; }
        public string StoreName { get; set; }

    }

    public class ProductInTemplate
    {
        public string ProductName { get; set; }

    }
}
