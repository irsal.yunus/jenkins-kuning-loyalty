﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.OCR
{
    public class OCRActionTemplateMappingModel
    {
        public OCRActionTemplateMappingModel()
        {

        }

        public OCRActionTemplateMappingModel(Data.OCRActionTemplateMapping data)
        {
    
            this.ID = data.ID;
            this.TemplateId = data.TemplateId;
            this.ActionId = data.ActionId;
            this.ProductNameOnOCR = data.ProductNameOnOCR;
            this.IsActive = data.IsActive;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.UpdatedBy = data.UpdatedBy;
            this.UpdatedDate = data.UpdatedDate.HasValue ? data.UpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "";
            
        }

        public long ID { get; set; }
        public long TemplateId { get; set; }
        public long ActionId { get; set; }
        public string ProductNameOnOCR { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        
    }
}
