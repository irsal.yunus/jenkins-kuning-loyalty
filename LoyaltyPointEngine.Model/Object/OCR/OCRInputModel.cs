﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.OCR
{
    public class OCRInputModel
    {
        public List<string> Images { get; set; }
        public string OCRString { get; set; }
        
    }
}
