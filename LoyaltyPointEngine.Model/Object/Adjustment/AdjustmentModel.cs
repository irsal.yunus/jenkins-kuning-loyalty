﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Adjustment
{
    public class AdjustmentModel
    {
        public AdjustmentModel()
        {

        }

        public AdjustmentModel(Data.Adjustment data)
        {
            this.ID = data.ID;
            this.TransactionDate = data.TransactionDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.PoolID = data.PoolID;
            this.Point = data.Point;
            this.Reason = data.Reason;
            this.CreatedByID = data.CreatedByID;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.MemberID = data.MemberID;
            this.Code = data.Code;

        }

        public string Code { get; set; }
        public Guid ID { get; set; }
        public string TransactionDate { get; set; }
        public int PoolID { get; set; }
        public int Point { get; set; }
        public string Reason { get; set; }
        public long? CreatedByID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int MemberID { get; set; }
    }


    public class AdjustmentCMSModel
    {
        public AdjustmentCMSModel()
        {

        }

        public AdjustmentCMSModel(Data.Adjustment data)
        {
            this.ID = data.ID;
            this.TransactionDate = data.TransactionDate.ToString("dd/MM/yyyy");
            this.ExpiredDate = data.ExpiredDate;
            this.PoolID = data.PoolID;
            this.Point = data.Point;
            this.Reason = data.Reason;
            this.CreatedByID = data.CreatedByID;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate;
            this.Code = data.Code;
            this.MemberID = data.MemberID;
            if (data.MemberID > 0)
            {
                var dataMember = Data.Member.GetById(data.MemberID);
                if (dataMember != null)
                {
                    this.MemberName = dataMember.Name;
                    this.MemberEmail = dataMember.Email;
                }
            }
        }

        public Guid ID { get; set; }
        public string Code { get; set; }
        public string TransactionDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int PoolID { get; set; }
        public int Point { get; set; }
        public string Reason { get; set; }
        public long? CreatedByID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public string MemberEmail { get; set; }
        public DateTime TransactionDateDT { get; set; }
    }
	
	public class AdjustmentReportingModel
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string TransactionDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public int PoolID { get; set; }
        public int Point { get; set; }
        public string Reason { get; set; }
        public long? CreatedByID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public string MemberEmail { get; set; }
        public DateTime TransactionDateDT { get; set; }
    }
}
