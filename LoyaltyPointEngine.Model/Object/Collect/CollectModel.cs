﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Collect
{
    public class CollectModel
    {
        public CollectModel()
        {

        }

        public CollectModel(Data.Collect data)
        {
            this.ID = data.ID;
            this.TransactionDate = data.TransactionDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.MemberID = data.MemberID;
            this.CampaignID = data.CampaignID;
            this.ActionID = data.ActionID;
            this.ReceiptID = data.ReceiptID;
            this.RedeemID = data.RedeemID;
            this.AdjustmentReferenceID = data.AdjustmentReferenceID;
            this.ExpiryDate =data.ExpiryDate.HasValue?  data.ExpiryDate.Value.ToString("dd/MM/yyyy HH:mm:ss"):"";
            this.PoolID = data.PoolID;
            this.Quantity = data.Quantity;
            this.QuantityApproved = data.QuantityApproved;
            this.Points = data.Points;
            this.TotalPoints = data.TotalPoints;
            this.Remarks = data.Remarks;
            this.IsApproved = data.IsApproved;
            this.ParentID = data.ParentID;
            this.IsActive = data.IsActive;
            this.IsUsed = data.IsUsed;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss"); 
        }

        public Guid ID { get; set; }
        public string TransactionDate { get; set; }
        public int MemberID { get; set; }
        public int? CampaignID { get; set; }
        public int? ActionID { get; set; }
        public Guid? ReceiptID { get; set; }
        public Guid? RedeemID { get; set; }
        public Guid? AdjustmentReferenceID { get; set; }
        public string ExpiryDate { get; set; }
        public int PoolID { get; set; }
        public int Quantity { get; set; }
        public int QuantityApproved { get; set; }
        public int Points { get; set; }
        public int TotalPoints { get; set; }
        public string Remarks { get; set; }
        public bool IsApproved { get; set; }
        public Guid? ParentID { get; set; }
        public bool IsActive { get; set; }
        public bool IsUsed { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public Guid? ConvertID { get; set; }
    }

    public class CollectCMSModel : CollectModel
    {
        public string MemberName { get; set; }
        public string TransactionTime { get; set; }
        public decimal ItemPrice { get; set; }
        public bool? IsFirstSubmission { get; set; }
        public int? ChildID { get; set; }
    }

    public class CollectCMSTableModel : CollectCMSModel
    {
        public string CampaignName { get; set; }
        public string ActionName { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? ExpiryDateTime { get; set; }
    }
}