﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Collect
{
    public static class CollectTranslator
    {

        /// <summary>
        /// transform list object data layer 'Collect' to List CollectModel
        /// </summary>
        /// <param name="data">List DataMember.Collect</param>
        /// <returns></returns>
        public static List<CollectModel> Translated(this List<Data.Collect> data)
        {
            List<CollectModel> result = new List<CollectModel>();

            foreach (var item in data)
            {
                result.Add(new CollectModel(item));
            }

            return result;

        }
    }
}
