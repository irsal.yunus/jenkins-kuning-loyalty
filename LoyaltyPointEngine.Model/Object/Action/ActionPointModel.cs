﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Action
{
    public class ActionPointModel
    {
        public int Points { get; set; }
        public string CampaignName{ get; set; }
        public bool IsCampaign { get; set; }
        public int? CampaignID { get; set; }
        public bool IsPregnancyProduct { get; set; }
    }
}
