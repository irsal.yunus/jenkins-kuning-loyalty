﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Action
{
    public static class ActionCategoryTranslator
    {

        /// <summary>
        /// transform list object data layer 'ActionCategory' to List ActionCategoryModel
        /// </summary>
        /// <param name="data">List DataMember.ActionCategory</param>
        /// <returns></returns>
        public static List<ActionCategoryModel> Translated(this List<Data.ActionCategory> data)
        {
            List<ActionCategoryModel> result = new List<ActionCategoryModel>();

            foreach (var item in data)
            {
                result.Add(new ActionCategoryModel(item));
            }

            return result;

        }


    }
}
