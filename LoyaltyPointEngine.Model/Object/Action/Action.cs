﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Action
{
    public class ActionModel
    {
        public ActionModel()
        {

        }

        public ActionModel(Data.Action data)
        {
            this.ID = data.ID;
         
            this.ActionCategoryID = data.ActionCategoryID;
            this.Points = data.Points;
            this.PoolID = data.PoolID;
            this.IsHaveCapGeneration = data.IsHaveCapGeneration;
            this.CapGeneration = data.CapGeneration ?? 0;
            this.IsActive = data.IsActive;
            this.ExpiredDate = data.ExpiredDate;
            this.CapGenerationDays = data.CapGenerationDays;

         
            this.ProductId = data.ProductID;
            if(data.Product != null)
            {
                this.GroupCode = data.Product.ProductGroup.ProductGroupCode;
                this.Size = data.Product.Size;
                this.Name = data.Product.Name;
                this.Code = data.Product.Code;
                this.Flavour = data.Product.Flavour;
                this.IsPregnancyProduct = data.Product.IsPregnancyProduct;
            }

            this.MinimumPrice = data.MinimumPrice.HasValue ? data.MinimumPrice.Value : 0;
        }
        public int ID { get; set; }
        public int ActionCategoryID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Points { get; set; }
        public int PoolID { get; set; }
        public bool IsHaveCapGeneration { get; set; }
        public bool IsActive { get; set; }
        public int CapGeneration { get; set; }
        public int CapGenerationDays { get; set; }
        public int ExpiredDate { get; set; }

        public int? ProductId { get; set; }
        public string GroupCode { get; set; }
        public string Size { get; set; }
        public string Flavour { get; set; }
        public int MinimumPrice { get; set; }
        public int ProductIdVendor { get; set; }
        public bool IsPregnancyProduct { get; set; }
    }
}
