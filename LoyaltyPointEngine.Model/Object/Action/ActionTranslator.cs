﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Action
{
    public static class ActionTranslator
    {

        /// <summary>
        /// transform list object data layer 'Action' to List ActionModel
        /// </summary>
        /// <param name="data">List DataMember.Action</param>
        /// <returns></returns>
        public static List<ActionModel> Translated(this List<Data.Action> data)
        {
            List<ActionModel> result = new List<ActionModel>();

            foreach (var item in data)
            {
                result.Add(new ActionModel(item));
            }

            if (result != null)
            {
                Regex regex = new Regex("[a-zA-Z ]");
                result = result.OrderBy(x => Int64.Parse(regex.Replace(x.Size, ""))).ToList();
            }

            return result;

        }


    }
}
