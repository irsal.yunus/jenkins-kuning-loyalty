﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Action
{
    public class ActionCategoryModel
    {
        
        public ActionCategoryModel()
        {

        }

        public ActionCategoryModel(Data.ActionCategory data)
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.Name = data.Name;
            this.IsActive = data.IsActive;
        }
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

    }
}
