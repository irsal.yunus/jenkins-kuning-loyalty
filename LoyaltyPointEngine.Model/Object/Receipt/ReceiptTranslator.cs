﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Receipt
{
    public static class ReceiptTranslator
    {
        /// <summary>
        /// transform list object data layer 'Receipt' to List ReceiptModel
        /// </summary>
        /// <param name="data">List DataMember.Receipt</param>
        /// <returns></returns>
        public static List<ReceiptModel> Translated(this List<Data.Receipt> data, string AssetURL="")
        {
            List<ReceiptModel> result = new List<ReceiptModel>();
            
            foreach (var item in data)
            {
                result.Add(new ReceiptModel(item, AssetURL));
            }

            return result;

        }


        public static List<ReceiptDetailModel> TranslatedMoreDetail(this List<Data.Receipt> data, string AssetURL = "")
        {
            List<ReceiptDetailModel> result = new List<ReceiptDetailModel>();

            foreach (var item in data)
            {
                result.Add(new ReceiptDetailModel(item, AssetURL));
            }

            return result;

        }
    }
}
