﻿using System;
using CsvHelper.Configuration;

namespace LoyaltyPointEngine.Model.Object.Receipt
{
    public class ReceiptReportingElasticModel
    {
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public string MemberPhone { get; set; }
        public string MemberEmail { get; set; }
        public string Code { get; set; }
        public string ReceiptCode { get; set; }
        public string Channel { get; set; }
        public string SPG { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Product { get; set; }
        public long Quantity { get; set; }
        public long QuantityApproved { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public long Point { get; set; }
        public string Price { get; set; }
        public string CodeGimmick { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? RequestedDate { get; set; }
        public string Reason { get; set; }
        public string ProductBefore { get; set; }
        public string ClusterName { get; set; }
        public string ChildName { get; set; }
        public string StoreCode { get; set; }
        public string NamaToko { get; set; }
        public string ClusteridStore { get; set; }
        public string IsActiveStore { get; set; }
        public string EventCode { get; set; }
    }

    public sealed class ReceiptReportingElasticCsvMapModel : ClassMap<ReceiptReportingElasticModel>
    {
        public ReceiptReportingElasticCsvMapModel()
        {
            AutoMap();
            Map(m => m.TransactionDate).TypeConverterOption.Format("dd/MM/yyyy HH:mm");
            Map(m => m.CreatedDate).TypeConverterOption.Format("dd/MM/yyyy HH:mm");
            Map(m => m.UpdatedDate).ConvertUsing(x =>
            {
                string UpdatedDate = x.GetField<DateTime?>("UpdatedDate").Value.ToString("dd/MM/yyyy");
                if (UpdatedDate.Contains("01/01/0001"))
                {
                    return null;
                }
                else
                {
                    return x.GetField<DateTime?>("UpdatedDate");
                }
            }).TypeConverterOption.Format("dd/MM/yyyy HH:mm").TypeConverterOption.NullValues("");
            Map(m => m.RequestedDate).ConvertUsing(x =>
            {
                string UpdatedDate = x.GetField<DateTime?>("UpdatedDate").Value.ToString("dd/MM/yyyy");
                if (UpdatedDate.Contains("01/01/0001"))
                {
                    return null;
                }
                else
                {
                    return x.GetField<DateTime?>("UpdatedDate");
                }
            }).TypeConverterOption.Format("dd/MM/yyyy HH:mm").TypeConverterOption.NullValues("");
        }
    }
}
