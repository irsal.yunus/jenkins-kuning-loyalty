﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Receipt
{
    public class BottomPriceReceiptReportModel
    {
        public string MemberPhone { get; set; }
        public string MemberEmail { get; set; }
        public string MemberName { get; set; }
        public string ReceiptNumber { get; set; }
        public string SKU { get; set; }
        public string SKUName { get; set; }
        public string Price { get; set; }
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string Channel { get; set; }
        public string TransactionDate { get; set; }
        public string UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
    }
    public class BottomPriceReceiptEmailModel
    {
        public string Evidence { get; set; }
        public string Date { get; set; }
        public string Account { get; set; }
        public string SKU { get; set; }
        public string Price { get; set; }
        public string MinimumPrice { get; set; }
        public string ReceiptID { get; set; }
    }
}
