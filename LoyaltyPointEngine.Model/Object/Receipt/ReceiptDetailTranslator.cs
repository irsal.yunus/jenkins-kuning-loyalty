﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Receipt
{
    public static class ReceiptDetailTranslator
    {
        /// <summary>
        /// transform list object data layer 'Area' to List AreaModel
        /// </summary>
        /// <param name="data">List DataMember.Area</param>
        /// <returns></returns>
        public static List<ReceiptItemModel> Translated(this List<Data.Collect> data)
        {
            List<ReceiptItemModel> result = new List<ReceiptItemModel>();

            foreach (var item in data)
            {
                result.Add(new ReceiptItemModel(item));
            }

            return result;

        }




    }
}
