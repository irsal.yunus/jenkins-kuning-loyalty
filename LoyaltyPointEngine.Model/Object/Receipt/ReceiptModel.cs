﻿using LoyaltyPointEngine.Model.Object.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Receipt
{
    public class ReceiptModel
    {
        public ReceiptModel()
        {

        }

        public ReceiptModel(Data.Receipt data, string AssetUrl = "")
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.TransactionDate = data.TransactionDate.HasValue ? data.TransactionDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "";
            this.RetailerID = data.RetailerID;


            this.Images = new List<string>();

            if(data.ReceiptImage != null)
            {
                foreach(var item in data.ReceiptImage)
                {
                    this.Images.Add(AssetUrl + item.Image);

                }

            }
            this.Status = data.Status;
            this.Reason = data.RejectReason;
            this.IsNeedReuploadData = data.IsNeedReuploadData;
            this.MemberID = data.MemberID;
            this.ProductBefore = data.ProductBefore;
        }
        public Guid ID { get; set; }
        public string ReceiptCode { get; set; }
        public bool IsNeedReuploadData { get; set; }
        public string Code { get; set; }
        public string TransactionDate { get; set; }
        public int? RetailerID { get; set; }
        public List<string> Images { get; set; }
        public int Status { get; set; }
        public string Reason{ get; set; }
        public int MemberID { get; set; }
        public string ProductBefore { get; set; }
    }

    public class ReceiptDetailModel : ReceiptModel
    {
        public ReceiptDetailModel()
        {

        }

        public ReceiptDetailModel(Data.Receipt data, string AssetUrl = "")
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.TransactionDate = data.TransactionDate.HasValue ? data.TransactionDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : "";
            this.RetailerID = data.RetailerID;
            this.IsNeedReuploadData = data.IsNeedReuploadData;
            this.Images = new List<string>();

            if (data.ReceiptImage != null)
            {
                foreach (var item in data.ReceiptImage)
                {
                    this.Images.Add(AssetUrl + item.Image);

                }

            }
            this.Status = data.Status;

            //if (data.Retailer != null)
            //{
            //    this.Retailer = new RetailerModel(data.Retailer);
            //}
            this.Reason = data.RejectReason;

            var collects = Data.Collect.GetAllByReceiptID(data.ID).ToList();

            if (collects != null && collects.Count > 0)
            {
                this.Items = collects.Translated();

            }

            if (data.UpdatedDate.HasValue)
            {
                this.UpdatedDate = data.UpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
            }
            else
            {
                this.UpdatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            }

            this.MemberID = data.MemberID;

        }
        public string UpdatedDate { get; set; }
        public RetailerModel Retailer { get; set; }
        public List<ReceiptItemModel> Items { get; set; }
    }

    public class ReceiptHeaderCMSModel : ReceiptModel
    {
        public string RetailerName { get; set; }
        public string RetailerAddress { get; set; }
        public string Remarks { get; set; }
        public string MemberName { get; set; }
        public string MemberPhone { get; set; }
        public string MemberEmail { get; set; }
        public string Product { get; set; }
        public string Channel { get; set; }
        public int? Quantity { get; set; }
        public string CreatedBy { get; set; }
        public long? CreatedByID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? RequestDate { get; set; }
        public DateTime? TransactionDateDate { get; set; }
    }

    public class ReceiptReportingModel : ReceiptHeaderCMSModel
    {
        public int? Point { get; set; }
        public string IsDeleted { get; set; }
        public string IsApproved { get; set; }
        public string AdminName { get; set; }
        public string SPG { get; set; }
        public string SPGCode { get; set; }
        public decimal Price { get; set; }
        public string CMSUserName { get; set; }
        public string CodeGimmick { get; set; }
        public string ClusterName { get; set; }
    }
}
