﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Receipt
{
    public class ReceiptItemModel
    {
        public ReceiptItemModel()
        {

        }

        public ReceiptItemModel(Data.Collect data)
        {
            this.ID = data.ID;
            if(data.Action != null)
            {
                this.ActionID = data.ActionID.GetValueOrDefault();
                this.ProductCode = data.Action.Code;
                this.ProductName = data.Action.Name;

                if(data.Action.Product != null)
                {
                    this.Size = data.Action.Product.Size;
                    this.ProductCode = data.Action.Product.Code;
                    this.ProductName = data.Action.Product.Name;
                    if (data.Action.Product.ProductGroup != null)
                        this.ProductGroupCode = data.Action.Product.ProductGroup.ProductGroupCode;
                }
               
            }

            this.Quantity = data.Quantity;
            this.QuantityApproved = data.QuantityApproved;
            this.Point = data.Points;
            this.TotalPoints = data.TotalPoints;
            this.Remarks = data.Remarks;
           
        }
        public Guid ID { get; set; }
        public int ActionID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductGroupCode { get; set; }
        public int Quantity { get; set; }
        public int QuantityApproved { get; set; }
        public int Point { get; set; }
        public int TotalPoints { get; set; }
        public string Remarks { get; set; }
        public string Size { get; set; }
    }
}
