﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Catalogue
{
    public class CatalogueModel
    {
        public CatalogueModel() { }

        public int ID { get; set; }
        public int CategoryId { get; set; }
        public string SKU { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Points { get; set; }
        public string Description { get; set; }
        public int? MinQtyEachRedeem { get; set; }
        public bool IsVirtual { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
