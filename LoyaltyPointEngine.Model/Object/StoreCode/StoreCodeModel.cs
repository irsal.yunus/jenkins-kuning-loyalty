﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.StoreCode
{
    public class StoreCodeModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public int ClusterId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
