﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.OTP
{
    public class ResendSMSModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OTPCode { get; set; }
        public DateTime OTPExpiredDate { get; set; }
        public int RequestOTP { get; set; }
        public string StatusOTP { get; set; }

        public int ID { get; set; }
        public int[] listID { get; set; }
    }

    public class MassResendModel
    {
        public string phone;
        public string errCode;
        public string errMsg;
    }
}
