﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.OTP
{
    public class SMSToLixusModel
    {
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "success")]
        public string MessageOTP { get; set; }

        [JsonProperty(PropertyName = "transid")]
        public string TransactionIdOTP { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string MessageNonOTP { get; set; }

        [JsonProperty(PropertyName = "data")]
        public DataNonOTPModel DataNonOTP { get; set; }
    }
    public class DataNonOTPModel
    {
        [JsonProperty(PropertyName = "transaction_id")]
        public string TransactionId { get; set; }

        [JsonProperty(PropertyName = "msisdn")]
        public string Msisdn { get; set; }
    }

    public class CallbackSMSParam
    {

        [JsonProperty(PropertyName = "transid")]
        public string Transid { get; set; }


        [JsonProperty(PropertyName = "delivered_at")]
        public DateTime DeliveredAt { get; set; }


        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

    }
}
