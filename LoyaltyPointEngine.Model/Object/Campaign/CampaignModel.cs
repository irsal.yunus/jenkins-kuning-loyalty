﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Campaign
{
    public class CampaignModel
    {
        public class ActionRule
        {
            public int ID { get; set; }
            public int ActionID { get; set; }
            public string ActionName { get; set; }
            public string Type { get; set; }
            public int Treshold { get; set; }
            public string Operator { get; set; }
        }
        public CampaignModel()
        {

        }

        public CampaignModel(Data.Campaign data)
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.Name = data.Name;
            this.StartDate = data.StartDate;
            this.EndDate = data.EndDate;
            this.StrStartDate = data.StartDate.HasValue ? data.StartDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            this.StrEndDate = data.EndDate.HasValue ? data.EndDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            this.StrStartTime = data.StartDate.HasValue ? data.StartDate.Value.ToString("HH:mm:ss") : string.Empty;
            this.StrEndTime = data.EndDate.HasValue ? data.EndDate.Value.ToString("HH:mm:ss") : string.Empty;
            this.Type = data.Type;
            this.Value = data.Value;
            this.CapGenerationPoint = data.CapGenerationPoint;
            this.IsActive = data.IsActive;
            this.IsFirstSubmit = data.IsFirstSubmit;
            this.IsAutomate = data.IsAutomate;
            this.IsExpiredPerUser = data.IsExpiredPerUser.HasValue? data.IsExpiredPerUser.Value: false;
            this.ExpireIn = data.ExpireIn;
            this.ActionRules = new List<ActionRule>();
            this.CampaignActionList = new int?[] { };
            if (data.CampaignActionDetail.Any())
            {
                List<int?> campaignActionIds = new List<int?>();
                var listCampaignActionID = data.CampaignActionDetail.Where(x => x.IsActive && !x.IsDeleted).Select(x => x.ActionID).ToList();
                foreach (var actionId in listCampaignActionID)
                {
                    campaignActionIds.Add(actionId);
                }
                this.CampaignActionList = campaignActionIds.ToArray();
            }
            if (data.AutomaticCampaignActionRule.Any())
            {
                List<ActionRule> campaignRole = new List<ActionRule>();
                var listCampaignActionID = data.AutomaticCampaignActionRule.Where(x => !x.IsDeleted);

                foreach (var item in listCampaignActionID)
                {
                    campaignRole.Add(new ActionRule { ID = item.ID, ActionID = item.ActionID, Type = item.Type, Treshold = item.Treshold, Operator = item.Operator });
                }
                this.ActionRules = campaignRole;
            }
        }
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string StrStartDate { get; set; }
        public string StrStartTime { get; set; }
        public string StrEndDate { get; set; }
        public string StrEndTime { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Type { get; set; }
        public int Value { get; set; }
        public int CapGenerationPoint { get; set; }
        public bool IsActive { get; set; }
        public int?[] ActionList { get; set; }
        public int?[] CampaignActionList { get; set; }
        public List<string> Channels { get; set; }
        public List<ActionRule> ActionRules { get; set; }
        public bool IsAutomate { get; set; }
        public bool IsExpiredPerUser { get; set; }
        public int? ExpireIn { get; set; }
        public bool IsFirstSubmit { get; set; }
    }
}
