﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Campaign
{
    public class CampaignActionModel
    {
        public CampaignActionModel()
        {

        }

        public CampaignActionModel(Data.CampaignActionDetail data)
        {
            this.ID = data.ID;
            this.ActionID = data.ActionID;
            this.CampaignID = data.CampaignID;
            this.MaximalQty = data.MaximalQty;
            this.MinimalQty = data.MinimalQty;
            this.IsActive = data.IsActive;
        }
        public int ID{ get; set; }
        public int ActionID { get; set; }
        public int CampaignID { get; set; }
        public int Point { get; set; }
        public int MaximalQty { get; set; }
        public int MinimalQty { get; set; }
        public bool IsActive { get; set; }
    }
}
