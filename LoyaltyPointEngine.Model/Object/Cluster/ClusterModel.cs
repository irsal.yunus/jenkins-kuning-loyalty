﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Cluster
{
    public class ClusterModel
    {
        public ClusterModel()
        {

        }

        public ClusterModel(Data.Cluster data)
        {
            ID = data.ID;
            Name = data.Name;
            IsActive = data.IsActive;
            CreatedBy = data.CreatedBy;
            CreatedDate = data.CreatedDate;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
