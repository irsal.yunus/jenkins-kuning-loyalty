﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Notification
{
    public static class NotificationTranslator
    {
        /// <summary>
        /// transform list object data layer 'Notification' to List NotificationModel
        /// </summary>
        /// <param name="data">List DataMember.Notification</param>
        /// <returns></returns>
        public static List<NotificationModel> Translated(this List<DataMember.Notification> data)
        {
            List<NotificationModel> result = new List<NotificationModel>();

            foreach (var item in data)
            {
                result.Add(new NotificationModel(item));
            }

            return result;

        }

        public static List<FusionNotificationModel> Translated(this List<DataMember.GetFusionNotifications_Result> data)
        {
            List<FusionNotificationModel> result = new List<FusionNotificationModel>();

            foreach (var item in data)
            {
                result.Add(new FusionNotificationModel(item));
            }

            return result;

        }
    }
}
