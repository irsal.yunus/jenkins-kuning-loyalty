﻿using Nest;
using Elasticsearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CLSNotifType = LoyaltyPointEngine.Common.Notification;

namespace LoyaltyPointEngine.Model.Object.Notification
{
    public class NotificationModel
    {
        public NotificationModel()
        {

        }

        public NotificationModel(string Message, string _Type, int MemberId, DateTime CreatedDate)
        {
            this.MemberID = MemberID;
            this.Message = Message;
            this.Type = _Type;
            this.CreatedDate = CreatedDate;
            this.IsRead = false;
        }

        public NotificationModel(DataMember.Notification data)
        {
            if (data != null)
            {
                this.ID = data.ID;
                this.MemberID = data.MemberID;
                this.Message = data.Message;
                this.Type = data.Type;
                this.CreatedDate = data.CreatedDate;
                this.IsRead = data.IsRead;
                this.Title = data.Title;
                this.ReferenceCode = data.ReferenceCode;
                this.Point = data.Point != null ? data.Point : 0;
                this.LeftIcon = data.LeftIcon;
                this.RightIcon = data.RightIcon;
                this.LinkUrl = data.LinkUrl;
            }
        }

        public Guid ID { get; set; }
        public int MemberID { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool IsRead { get; set; }
        public string Title { get; set; }
        public string ReferenceCode { get; set; }
        public string LeftIcon { get; set; }
        public string RightIcon { get; set; }
        public int? Point { get; set; }
        public string LinkUrl { get; set; }
    }

    [ElasticsearchType(IdProperty = nameof(NotificationID))]
    public class FusionNotificationModel
    {
        public FusionNotificationModel()
        {

        }

        public FusionNotificationModel(DataMember.GetFusionNotifications_Result data)
        {
            this.NotificationID = data.NotificationID;
            this.NotificationDate = data.NotificationDate;
            this.MemberID = data.MemberID;
            this.Title = data.Title;
            this.SubTitle = data.SubTitle;
            this.Detail = data.Detail;
            CLSNotifType.NotificationType.Type _NotificationType = CLSNotifType.NotificationType.Type.Information;
            Enum.TryParse<CLSNotifType.NotificationType.Type>(data.NotificationType, out _NotificationType);
            this.NotificationType = _NotificationType;
            this.ReferenceID = data.ReferenceID;
            this.ReferenceCode = data.ReferenceCode;
            this.Point = data.Point;
            this.LastPoint = data.LastPoint;
            this.IsRead = data.IsRead;
            this.LeftIcon = data.LeftIcon;
            this.RightIcon = data.RightIcon;
            this.SourceData = data.SourceData;
        }

        /// <summary>
        /// NotificationID
        /// </summary>
        public Guid NotificationID { get; set; }
        /// <summary>
        /// MemberID
        /// </summary>
        public int MemberID { get; set; }
        /// <summary>
        /// NotificationDate
        /// </summary>
        public DateTime? NotificationDate { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// SubTitle
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// Detail
        /// </summary>
        public string Detail { get; set; }
        /// <summary>
        /// NotificationType
        /// </summary>
        public CLSNotifType.NotificationType.Type NotificationType { get; set; }

        /// <summary>
        /// ReferenceID
        /// </summary>
        public string ReferenceID { get; set; }
        /// <summary>
        /// ReferenceCode
        /// </summary>
        public string ReferenceCode { get; set; }
        /// <summary>
        /// Point
        /// </summary>
        public int? Point { get; set; }
        /// <summary>
        /// LastPoint
        /// </summary>
        public int? LastPoint { get; set; }
        /// <summary>
        /// IsRead
        /// </summary>
        public bool IsRead { get; set; }
        /// <summary>
        /// LeftIcon
        /// </summary>
        public string LeftIcon { get; set; }
        /// <summary>
        /// RightIcon
        /// </summary>
        public string RightIcon { get; set; }
        /// <summary>
        /// SourceData
        /// </summary>
        public string SourceData { get; set; }
    }
}
