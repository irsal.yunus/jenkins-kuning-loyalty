﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.ConvertPoint
{
    public class ConvertPointReportModel
    {
        public string MemberName { get; set; }
        public string MemberPhone { get; set; }
        public string PontaId { get; set; }
        public string TransactionDate { get; set; }
        public string PontaPoints { get; set; }
        public string RewardPoints { get; set; }
        public string Balance { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
    }
}
