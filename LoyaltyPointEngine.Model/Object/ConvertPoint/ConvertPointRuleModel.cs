﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.ConvertPoint
{
    public class ConvertPointRuleModel
    {
        public ConvertPointRuleModel()
        {

        }

        public ConvertPointRuleModel(Data.ConvertPointRule data)
        {
            this.ID = data.ID;
            this.StoreId = data.StoreId;
            this.StoreName = data.StoreName;
            this.Point = data.Point;
            this.StorePoint = data.StorePoint;
            this.Promo = data.Promo;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate;
            this.ModifiedBy = data.ModifiedBy;
            this.ModifiedDate = data.ModifiedDate;
            this.LogoImageUrl = data.LogoImageUrl;
            this.IconImageUrl = data.IconImageUrl;
            this.MinConvert = data.MinConvert.HasValue ? data.MinConvert.Value : 3000;
        }

        public long ID { get; set; }
        public long StoreId { get; set; }
        public string StoreName { get; set; }
        public long StorePoint { get; set; }
        public long Point { get; set; }
        public long Promo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string LogoImageUrl { get; set; }
        public string IconImageUrl { get; set; }
        public long MinConvert { get; set; }
    }
}