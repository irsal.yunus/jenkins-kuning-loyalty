﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.ConvertPoint
{
    public class ConvertPointTransactionLogModel
    {
        public ConvertPointTransactionLogModel()
        {

        }

        public ConvertPointTransactionLogModel(Data.ConvertPointTransactionLog data)
        {
            this.ID = data.Id;
            this.MemberId = data.memberId;
            this.TransactionDate = data.TransactionDate;
            this.StoreId = data.StoreId;
            this.StoreName = data.StoreName;
            this.StorePoint = data.StorePoint;
            this.Promo = data.Promo;
            this.Points = data.Points;
            this.Balance = data.Balance;
            this.Description = data.Description;            
        }


        
        public Guid ID { get; set; }
        public int MemberId { get; set; }
        public DateTime TransactionDate { get; set; }
        public long StoreId { get; set; }
        public string StoreName { get; set; }
        public long StorePoint { get; set; }
        public long Promo { get; set; }
        public long Points { get; set; }
        public long Balance { get; set; }
        public string Description { get; set; }

        
    }
}
