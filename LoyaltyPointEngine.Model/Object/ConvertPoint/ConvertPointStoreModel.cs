﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.ConvertPoint
{
    public class ConvertPointStoreModel
    {
        public ConvertPointStoreModel()
        {

        }

        public ConvertPointStoreModel(Data.ConvertPointStore data)
        {
            this.ID = data.ID;
            this.Name = data.StoreName;
        }

        public long ID { get; set; }
        public string Name { get; set; }
        
    }
}
