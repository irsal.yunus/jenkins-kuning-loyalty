﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Redeem
{
    public class PointHistoryModel
    {
        public DateTime TransactionDate { get; set; }
        public string TransactionCode { get; set; }
        //Debit or Credit
        public string TransactionType { get; set; }
        //Debit or Credit
        public bool IsVoid { get; set; }
        public int Point { get; set; }
        public int Balance { get; set; }
    }
}
