﻿using LoyaltyPointEngine.Model.Object.Collect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Redeem
{
    public class RedeemModel
    {
        public RedeemModel()
        {

        }

        public RedeemModel(Data.Redeem data)
        {
            this.ID = data.ID;
            this.MemberID = data.MemberID;
            this.TransactionCode = data.TransactionCode;
            this.TransactionDate = data.TransactionDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.PoolID = data.PoolID;
            this.Point = data.Point;
            this.IsVoid = data.IsVoid;
            this.CreatedBy = data.CreatedBy;
            this.CreatedByID = data.CreatedByID;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");

        }

        public Guid ID { get; set; }
        public int MemberID { get; set; }
        public string TransactionCode { get; set; }
        public string TransactionDate { get; set; }
        public int PoolID { get; set; }
        public int Point { get; set; }
        public bool IsVoid { get; set; }
        public string CreatedBy { get; set; }
        public int? CreatedByID { get; set; }
        public string CreatedDate { get; set; }
    }


    public class RedeemDetailModel : RedeemModel
    {
        public RedeemDetailModel()
        {

        }

        public RedeemDetailModel(Data.Redeem data)
        {
            this.ID = data.ID;
            this.MemberID = data.MemberID;
            this.TransactionCode = data.TransactionCode;
            this.TransactionDate = data.TransactionDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.PoolID = data.PoolID;
            this.Point = data.Point;
            this.IsVoid = data.IsVoid;
            this.CreatedBy = data.CreatedBy;
            this.CreatedByID = data.CreatedByID;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            var collects = Data.Collect.GetByRedeemId(data.ID).ToList();
            if (collects != null && collects.Count > 0)
            {
                this.Collects = collects.Translated();
            }
        }

        public List<CollectModel> Collects { get; set; }
    }

    public class RedeemCMSViewModel : RedeemModel
    {
        public string MemberName { get; set; }
        public string MemberEmail { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime TransactionDateDate { get; set; }
        public string MemberPhone { get; set; }
        public string ElinaCode { get; set; }
        public string CatalogueName { get; set; }
        public int Quantity { get; set; }
        public string Channel { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverAddress { get; set; }
    }
}