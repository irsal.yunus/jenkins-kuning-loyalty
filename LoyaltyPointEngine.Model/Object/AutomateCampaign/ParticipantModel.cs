﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.AutomateCampaign
{
    public class ParticipantModel
    {
        public int MemberID { get; set; }
        public string MemberName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int CampaignID { get; set; }
        public string Code { get; set; }
        public string CampaignName { get; set; }
        public int Point { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ParticipantInsertModel
    {
        public int MemberId { get; set; }
        public int CampaignID { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Point { get; set; }

        public int BonusPoint { get; set; }
        public bool IsAchieve { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
    public class ParticipantTemplateUploadModel
    {
        public string phone { get; set; }
    }
    public class ListCampaignChoose
    {
        public int campaignID { get; set; }
    }
}
