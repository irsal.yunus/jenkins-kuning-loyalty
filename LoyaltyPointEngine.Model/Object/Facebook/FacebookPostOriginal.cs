﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Facebook
{
    public class FacebookPostResponse
    {
        public List<FacebookPostOriginal> posts { get; set; }
        public FacebookPagination pagination { get; set; }
    }
    public class FacebookPostOriginal
    {
        public string message { get; set; }
        public string created_time { get; set; }
        public string id { get; set; }
    }

    public class FacebookPagination
    {
        public string previous { get; set; }
        public string next { get; set; }
    }

    public class FacebookPostModel
    {
        public FacebookPostModel(Data.FacebookPost data)
        {
            if(data != null)
            {
                this.ID = data.ID;
                this.message = data.Content;
                this.PostId = data.PostID;
                this.created_time = data.CreatedDate;
            }

        }
        public Guid ID{ get; set; }
        public string message { get; set; }
        public DateTime created_time { get; set; }
        public string PostId { get; set; }
    }


    public class FacebookLikeResponse
    {
        public List<FacebookMemberModel> data { get; set; }
        public FacebookPagination pagination { get; set; }
    }

    public class FacebookMemberModel
    {
        public string name { get; set; }   
        public string id { get; set; }
    }



    public class FacebookCommentResponse
    {
        public List<FacebookCommentOriginal> data { get; set; }
        public FacebookPagination pagination { get; set; }
    }

    public class FacebookCommentOriginal
    {
        public FacebookMemberModel from { get; set; }
        public string message { get; set; }
        public string created_time { get; set; }
        public string id { get; set; }
    }
}
