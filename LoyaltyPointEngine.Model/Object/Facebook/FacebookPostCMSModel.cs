﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Facebook
{
    public class FacebookPostCMSModel
    {
        public Guid ID { get; set; }
        public string message { get; set; }
        public DateTime created_time { get; set; }
        public string PostId { get; set; }
        public int TotalLike{ get; set; }
        public int TotalComment { get; set; }
    }
}
