﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Facebook
{
    public class FacebookCommentModel
    {
        public Guid ID { get; set; }
        public DateTime CreatedDate { get; set; }

        public string MemberName { get; set; }
        public int Point { get; set; }
        public string FacebookUserID { get; set; }

        public string Comment { get; set; }
        public string CommentID { get; set; }
    }
}
