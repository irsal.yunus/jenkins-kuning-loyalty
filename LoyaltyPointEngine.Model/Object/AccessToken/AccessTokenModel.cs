﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.AccessToken
{
    public class AccessTokenModel
    {
        public AccessTokenModel() { }

        public AccessTokenModel(DataMember.Member member)
        {
            this.FirstName = member.FirstName;
            this.LastName = member.LastName;
            this.Phone = member.Phone;
            this.IsActive = member.IsActive;
            this.Password = member.Password;
            this.IsApprovedKKImage = member.IsApprovedKKImage;
            this.IsUploadedKK = !string.IsNullOrEmpty(member.KKImage) ? true : false;
        }

        /// <summary>
        /// information your access token application name, example: bebelac,nutriclub
        /// </summary>
        public string PoolName { get; set; }

        /// <summary>
        /// item key for using other API
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// time that acccess token will expired. Format date: dd/MM/yyyy HH:mm:ss
        /// </summary>
        public string ExpiredDate { get; set; }

        /// <summary>
        /// item image file for validate login
        /// </summary>
        public bool? IsApprovedKKImage { get; set; }

        public bool? IsUploadedKK { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
    }
}
