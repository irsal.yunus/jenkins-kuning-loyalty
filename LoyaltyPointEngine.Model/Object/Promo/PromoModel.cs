﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Promo
{
    public class PromoModel
    {
        public PromoModel()
        {

        }

        public PromoModel(Data.Promo data)
        {
            this.ID = data.ID;
            this.PromoCode = data.PromoCode;
            this.StartDate = data.StartDate.ToString("dd/MM/yyyy");
            this.EndDate = data.EndDate.ToString("dd/MM/yyyy");
            this.IsActive = data.IsActive.Value;
            this.IsCheck = data.IsCheck.Value;
            this.MsgResponse = data.MsgResponse;
            this.MsgResponseFailedUnikKodeExp = data.MsgResponseFailedUnikKodeExp;
            this.MsgResponseFailedUnikKodeNotFound = data.MsgResponseFailedUnikKodeNotFound;
            this.MsgResponseFailedUnikKodeUsed = data.MsgResponseFailedUnikKodeUsed;
            this.Description = data.Description;
            this.CreatedBy = data.CreatedBy;
            this.CreatedDate = data.CreatedDate.HasValue?data.CreatedDate.Value.ToString("dd/MM/yyyy"):string.Empty;
            this.UpdateBy = data.UpdateBy;
            this.UpdateDate = data.UpdateDate.HasValue ? data.UpdateDate.Value.ToString("dd/MM/yyyy") :string.Empty ;
            this.ReasonUpdate = data.ReasonUpdate;
            this.ChannelId = data.ChannelId.GetValueOrDefault(0);
            this.MsgResponseUnderOneYear = data.MsgResponseUnderOneYear;
            this.IsVoucher = data.IsVoucher.Value;
            this.MsgResponseStatus = data.MsgResponseForStatus;
            this.Status = data.Status;

        }

        public int ID { get; set; }
        [Required]
        public string PromoCode { get; set; }
        public string ReasonUpdate { get; set; }
        [Required]
        public string StartDate { get; set; }
        [Required]
        public string EndDate { get; set; }
        public string CreatedDate { get; set; }
        public string UpdateDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsCheck { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateBy { get; set; }
        public string Description { get; set; }
        [Required]
        public string MsgResponse { get; set; }
        [Required]
        public string MsgResponseFailedUnikKodeExp { get; set; }
        [Required]
        public string MsgResponseFailedUnikKodeNotFound { get; set; }
        [Required]
        public string MsgResponseFailedUnikKodeUsed { get; set; }
        public string MsgResponseUnderOneYear { get; set; }
        public bool IsVoucher { get; set; }
        public string MsgResponseStatus { get; set; }
        public string Status { get; set; }

        [Required]
        public int ChannelId { get; set; }
        public string interval { get; set; }
    }

    public class UploadReceipFromExcelCMSModel
    {
        public string UniqueCode { get; set; }
        public string PromoCode { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public string Domisili { get; set; }
        public string Retailer { get; set; }
        public string ExpiredPromo { get; set; }
    }

    public class UploadReceiptFromExcelResultCMSModel : UploadReceipFromExcelCMSModel
    {
        public string result { get; set; }
        public string message { get; set; }
        public int totSuccess { get; set; }
        public int totFailed { get; set; }
    }
}
