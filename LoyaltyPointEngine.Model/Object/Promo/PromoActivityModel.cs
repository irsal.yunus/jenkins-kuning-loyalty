﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Promo
{
    public class PromoActivityModel
    {
        public PromoActivityModel() { }

        public int ID { get; set; }
        public int MemberID { get; set; }
        public int PromoDetailID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string ReasonUpdate { get; set; }
        public string StatusMsg { get; set; }


    }
    public class PromoActivityReportDetail:PromoActivityModel
    {
        public Data.Member MemberM {get;set;}

        public Data.PromoDetails PromDetM { get; set; }
        public Data.Promo PromM { get; set; }
        
    }

    public class PromoActivityReportModel
    {
        public Data.Member MemberID { get; set; }
        public Data.Promo PromoID { get; set; }
        public int JmlData { get; set; }

    }

    public class PromoParamAPI
    {
        //promokode, nama, dob anak, nohp, kodeunik
        public string PromoCode { get; set; }
        public string Nama { get; set; }
        public string DOBAnak { get; set; }
        public string KodeUnik { get; set; }
        public string Phone { get; set; }
        public int DomisiliID { get; set; }
        public string SpgCode { get; set; }
        public string Channel { get; set; }
        public string MsgRespon2 { get; set; }
    }


}
