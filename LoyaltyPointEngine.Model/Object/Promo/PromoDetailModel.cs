﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Promo
{

    public class PromoDetailModel
    {
        public PromoDetailModel() { }

        public int ID { get; set; }
        public string CodeName { get; set; }
        public int IDPromo { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string ReasonUpdate { get; set; }
        public bool IsUsed { get; set; }
    }

}
