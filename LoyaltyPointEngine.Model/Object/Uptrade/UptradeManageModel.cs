﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Model.Object.Common;

namespace LoyaltyPointEngine.Model.Object
{
    public class UptradeManageModel
    {
        public long ID { get; set; }
        public long ChannelID { get; set; }
        public string Channel { get; set; }
        public string ProductAfter { get; set; }
        public string ProductBefore { get; set; }
        public long Point { get; set; }
        public string Remarks { get; set; }
        public string RemarksMember { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class CollectUptradeModel
    {
        public string ReceiptCode { get; set; }
        public string Channel { get; set; }
        public string ProductAfter { get; set; }
        public string ProductBefore { get; set; }
        public string Remarks { get; set; }
        public string RemarksMember { get; set; }
        public DateTime ProductAfterDate { get; set; }
        public DateTime ProductBeforeDate { get; set; }
    }
}
