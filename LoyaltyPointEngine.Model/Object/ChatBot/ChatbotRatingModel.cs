﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Chatbot
{
    public class ChatbotRatingModel
    {
        public ChatbotRatingModel()
        {

        }

        public ChatbotRatingModel(Data.ChatbotRating data)
        {
            this.ID = data.ID;
            this.Phone = data.NoHP;
            this.Rating = data.Rating;
            this.Inputs = data.Inputs;
            this.CreatedDate = data.CreatedDate;
            this.CreatedBy = data.CreatedBy;
            this.UpdatedDate = data.UpdatedDate;
            this.UpdatedBy = data.UpdatedBy;
            this.IsDeleted = data.IsDeleted;
        }
        
        public long ID { get; set; }
        public string Phone { get; set; }
        public string Rating { get; set; }
        public string Inputs { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool? IsDeleted { get; set; }
    }

}
