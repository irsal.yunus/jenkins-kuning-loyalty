﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Common
{
    public enum Channel
    {
        POS = 2,
        Email = 3,
        Website = 4,
        Facebook = 5,
        Event = 6,
        Alfamart=7,
        ELINA = 8,
        AlfaMidi = 9,
        Careline = 10,
        Mobile = 11,
        ElinaMobile = 12,
        jdid = 13,
        SmsOthers = 14,
        shopee = 15,
        suzuya = 16,
        Chandra = 17
    }


}
