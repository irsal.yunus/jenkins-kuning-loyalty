﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class AreaModel
    {
        public AreaModel()
        {

        }

        public AreaModel(Data.Area data)
        {
            this.ID = data.ID;
            this.DistrictID = data.DistrictID;
            this.Code = data.Code;
            this.Name = data.Name;
        }
        public int ID { get; set; }
        [Required]
        public int DistrictID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }


    }
    public class AreaDetailModel : AreaModel
    {
        public AreaDetailModel()
        {

        }

        public AreaDetailModel(Data.Area data)
        {
            this.ID = data.ID;
            this.DistrictID = data.DistrictID;
            this.Code = data.Code;
            this.Name = data.Name;
            if (data.District != null)
            {
                this.District = new DistrictModel(data.District);
            }
        }
        public DistrictModel District { get; set; }
    }
}
