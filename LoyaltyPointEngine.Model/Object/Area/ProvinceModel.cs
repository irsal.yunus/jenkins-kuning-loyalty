﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class ProvinceModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
