﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class CityModel
    {
        public CityModel()
        {

        }

        public CityModel(Data.City data)
        {
            this.ID = data.ID;
            this.AreaID = data.AreaID;
            this.Code = data.Code;
            this.Name = data.Name;
        }
        public int ID { get; set; }
        [Required]
        public int AreaID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }


    }
    public class CityDetailModel : CityModel
    {
        public CityDetailModel()
        {

        }

        public CityDetailModel(Data.City data)
        {
            this.ID = data.ID;
            this.AreaID = data.AreaID;
            this.Code = data.Code;
            this.Name = data.Name;
            if (data.Area != null)
            {
                this.Area = new AreaModel(data.Area);
            }
        }
        public AreaModel Area { get; set; }
    }
}
