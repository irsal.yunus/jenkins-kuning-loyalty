﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public static class DistrictTranslator
    {
        /// <summary>
        /// transform list object data layer 'District' to List DistrictModel
        /// </summary>
        /// <param name="data">List DataMember.District</param>
        /// <returns></returns>
        public static List<DistrictModel> Translated(this List<Data.District> data)
        {
            List<DistrictModel> result = new List<DistrictModel>();

            foreach (var item in data)
            {
                result.Add(new DistrictModel(item));
            }

            return result;

        }
    }
}
