﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public static class CityTranslator
    {


        /// <summary>
        /// transform list object data layer 'City' to List CityModel
        /// </summary>
        /// <param name="data">List DataMember.City</param>
        /// <returns></returns>
        public static List<CityModel> Translated(this List<Data.City> data)
        {
            List<CityModel> result = new List<CityModel>();

            foreach (var item in data)
            {
                result.Add(new CityModel(item));
            }

            return result;

        }


    }
}
