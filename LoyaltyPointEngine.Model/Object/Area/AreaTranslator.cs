﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public static class AreaTranslator
    {

        /// <summary>
        /// transform list object data layer 'Area' to List AreaModel
        /// </summary>
        /// <param name="data">List DataMember.Area</param>
        /// <returns></returns>
        public static List<AreaModel> Translated(this List<Data.Area> data)
        {
            List<AreaModel> result = new List<AreaModel>();

            foreach (var item in data)
            {
                result.Add(new AreaModel(item));
            }

            return result;

        }
    }
}
