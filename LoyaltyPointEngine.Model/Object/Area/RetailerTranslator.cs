﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public static class RetailerTranslator
    {
        /// <summary>
        /// transform list object data layer 'Retailer' to List RetailerModel
        /// </summary>
        /// <param name="data">List DataMember.Retailer</param>
        /// <returns></returns>
        public static List<RetailerModel> Translated(this List<Data.Retailer> data)
        {
            List<RetailerModel> result = new List<RetailerModel>();

            foreach (var item in data)
            {
                result.Add(new RetailerModel(item));
            }

            return result;

        }
    }
}
