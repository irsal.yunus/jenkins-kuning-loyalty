﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public static class RegionTranslator
    {
        /// <summary>
        /// transform list object data layer 'Region' to List RegionModel
        /// </summary>
        /// <param name="data">List DataMember.Region</param>
        /// <returns></returns>
        public static List<RegionModel> Translated(this List<Data.Region> data)
        {
            List<RegionModel> result = new List<RegionModel>();

            foreach (var item in data)
            {
                result.Add(new RegionModel(item));
            }

            return result;

        }
    }
}
