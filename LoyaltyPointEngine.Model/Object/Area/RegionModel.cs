﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class RegionModel
    {
        public RegionModel()
        {

        }

        public RegionModel(Data.Region data)
        {
            this.ID = data.ID;
            this.ZoneID = data.ZoneID;
            this.Code = data.Code;
            this.Name = data.Name;
        }
        public int ID { get; set; }
        [Required]
        public int ZoneID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }


    }


    public class RegionDetailModel : RegionModel
    {
        public RegionDetailModel()
        {

        }

        public RegionDetailModel(Data.Region data)
        {
            this.ID = data.ID;
            this.ZoneID = data.ZoneID;
            this.Code = data.Code;
            this.Name = data.Name;
            if (data.Zone != null)
            {
                this.Zone = new ZoneModel(data.Zone);
            }
        }
        public ZoneModel Zone { get; set; }
    }
}
