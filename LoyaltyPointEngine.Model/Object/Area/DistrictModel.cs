﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class DistrictModel
    {
        public DistrictModel()
        {

        }

        public DistrictModel(Data.District data)
        {
            this.ID = data.ID;
            this.RegionID = data.RegionID;
            this.Code = data.Code;
            this.Name = data.Name;
        }
        public int ID { get; set; }
        [Required]
        public int RegionID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }


    }
    public class DistrictDetailModel : DistrictModel
    {
        public DistrictDetailModel()
        {

        }

        public DistrictDetailModel(Data.District data)
        {
            this.ID = data.ID;
            this.RegionID = data.RegionID;
            this.Code = data.Code;
            this.Name = data.Name;
            if (data.Region != null)
            {
                this.Region = new RegionModel(data.Region);
            }
        }
        public RegionModel Region { get; set; }
    }
}
