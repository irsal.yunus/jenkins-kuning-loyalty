﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public static class ZoneTranslator
    {
        /// <summary>
        /// transform list object data layer 'Zone' to List ZoneModel
        /// </summary>
        /// <param name="data">List DataMember.Zone</param>
        /// <returns></returns>
        public static List<ZoneModel> Translated(this List<Data.Zone> data)
        {
            List<ZoneModel> result = new List<ZoneModel>();

            foreach (var item in data)
            {
                result.Add(new ZoneModel(item));
            }

            return result;

        }
    }
}
