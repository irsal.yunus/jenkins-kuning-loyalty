﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class RetailerModel
    {
        public RetailerModel()
        {

        }

        public RetailerModel(Data.Retailer data)
        {
            this.ID = data.ID;
            this.CityID = data.CityID.GetValueOrDefault();
            this.Code = data.Code;
            this.Name = data.Name;
            this.RetailerSubAccountID = data.RetailerSubAccountID;
        }
        public int ID { get; set; }
        //[Required]
        public int? CityID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public int? RetailerSubAccountID { get; set; }
     


    }
    public class RetailerDetailModel : RetailerModel
    {
        public RetailerDetailModel()
        {

        }

        public RetailerDetailModel(Data.Retailer data)
        {
            this.ID = data.ID;
            this.CityID = data.CityID.GetValueOrDefault();
            this.Code = data.Code;
            this.Name = data.Name;
            if (data.City != null)
            {
                this.City = new CityModel(data.City);
            }
            this.RetailerSubAccountID = data.RetailerSubAccountID;
        }
        public CityModel City { get; set; }
    }


    public class RetailerCMSModel : RetailerModel
    {
        public RetailerCMSModel()
        {

        }

        public RetailerCMSModel(Data.Retailer data)
        {
            this.ID = data.ID;
            this.CityID = data.CityID.GetValueOrDefault();
            this.Code = data.Code;
            this.Name = data.Name;
            this.IsNoCap = data.IsNoCap.HasValue ? (bool)data.IsNoCap : false;
            if (data.City != null)
            {
                this.City = new CityModel(data.City);
            }
            this.RetailerSubAccountID = data.RetailerSubAccountID;
            if(data.RetailerSubAccount != null)
            {
                var dataAccount = Data.RetailerAccount.GetById(data.RetailerSubAccount.RetailerAccountID);
                if (dataAccount != null)
                {
                    this.RetailerAccountID = dataAccount.ID;

                    var dataSubChannel = Data.RetailerSubChannel.GetById(dataAccount.RetailerSubChannelID);
                    if (dataSubChannel != null)
                    {
                        this.RetailerSubChannelID = dataSubChannel.ID;
                        var dataChannel = Data.RetailerChannel.GetById(dataSubChannel.RetailerChannelID);
                        if (dataChannel != null)
                        {
                            this.RetailerChannelID = dataChannel.ID;
                            var dataMarket = Data.RetailerMarket.GetById(dataChannel.RetailerMarketID);
                            if(dataMarket != null)
                            {
                                this.RetailerMarketID = dataMarket.ID;
                            }
                            
                        }
                    }
                }
            }
          
        }
        public CityModel City { get; set; }
        public int RetailerAccountID { get; set; }
        public int RetailerSubChannelID { get; set; }
        public int RetailerChannelID { get; set; }
        public int RetailerMarketID { get; set; }
        public bool IsNoCap { get; set; }

    }
}
