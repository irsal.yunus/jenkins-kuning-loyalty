﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Area
{
    public class ZoneModel
    {
        public ZoneModel()
        {

        }

        public ZoneModel(Data.Zone data)
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.Name = data.Name;
        }
        public int ID{ get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }


    }
}
