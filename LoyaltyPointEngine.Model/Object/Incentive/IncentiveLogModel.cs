﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Incentive
{
    public class IncentiveLogResultModel
    {
        public int TotalRow { get; set; }
        public List<IncentiveLogModel> IncentiveLog { get; set; }
    }
    public class IncentiveLogModel
    {
        public IncentiveLogModel()
        {

        }

        public IncentiveLogModel(Data.IncentiveSpgLog data)
        {
            DateTime res = DateTime.MinValue;

            this.ID = data.ID;
            this.SpgCode = data.SpgCode;
            this.MemberId = data.MemberId;
            this.MemberName = data.MemberName;
            this.Phone = data.phone;
            this.Type = data.Type;
            this.Incentive = data.Incentive;
            this.Channel = data.channel;
            this.Remark = data.remark;
            this.ApprovedDate = data.CreatedDate.Value.ToString();
            this.TransactionDate = data.receiptDate.HasValue ? data.receiptDate.ToString() : DateTime.MinValue.ToString();
            this.UploadDate = data.uploadDate.HasValue ? data.uploadDate.ToString() : DateTime.MinValue.ToString();
            this.ApprovedBy = data.CreatedBy;
            this.ReceiptCode = data.ReceiptCode;

        }

        public long ID { get; set; }
        public string SpgCode { get; set; }
        public int MemberId { get; set; }
        public string Type { get; set; }
        public long Incentive { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public string MemberName { get; set; }
        public string Phone { get; set; }
        public string TransactionDate { get; set; }
        public string Remark { get; set; }
        public string Channel { get; set; }
        public string UploadDate { get; set; }
        public string ReceiptCode { get; set; }

    }
}
