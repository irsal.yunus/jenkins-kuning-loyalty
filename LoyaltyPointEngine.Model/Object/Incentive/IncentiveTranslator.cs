﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Incentive
{
    public static class IncentiveTranslator
    {

        /// <summary>
        /// transform list object data layer 'Action' to List ActionModel
        /// </summary>
        /// <param name="data">List DataMember.Action</param>
        /// <returns></returns>
        public static List<IncentiveModel> Translated(this List<Data.IncentiveSpg> data)
        {
            List<IncentiveModel> result = new List<IncentiveModel>();

            foreach (var item in data)
            {
                result.Add(new IncentiveModel(item));
            }

            return result;

        }


    }
}
