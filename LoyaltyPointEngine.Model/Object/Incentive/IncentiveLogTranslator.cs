﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Incentive
{
    public static class IncentiveLogTranslator
    {

        /// <summary>
        /// transform list object data layer 'Action' to List ActionModel
        /// </summary>
        /// <param name="data">List DataMember.Action</param>
        /// <returns></returns>
        public static List<IncentiveLogModel> Translated(this List<Data.IncentiveSpgLog> data)
        {
            List<IncentiveLogModel> result = new List<IncentiveLogModel>();

            foreach (var item in data)
            {
                result.Add(new IncentiveLogModel(item));
            }

            return result;

        }


    }
}
