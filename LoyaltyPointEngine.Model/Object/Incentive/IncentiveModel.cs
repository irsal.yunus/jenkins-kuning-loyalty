﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Incentive
{
    public class IncentiveModel
    {
        public IncentiveModel()
        {

        }

        public IncentiveModel(Data.IncentiveSpg data)
        {
            int resFirstSubmit = 0;
            int resIsDisplayed = 0;
            this.ID = data.ID;

            this.SpgCode = data.SpgCode;
            this.SpgId = data.SpgId;
            this.IncentiveThisMonth = int.Parse(data.Incentive.ToString());
            this.Month = data.Month;
            this.MonthName = data.MonthName;
            this.Year = int.Parse(data.year.ToString());
            this.NumberFirstSubmit = int.TryParse(data.NumberFirstSubmit.ToString(), out resFirstSubmit) ? resFirstSubmit : 0;
            int.TryParse(data.IsDisplayed.ToString(), out resIsDisplayed);
            this.IsDisplayed = resIsDisplayed == 1 ? true : false;
        }

        public int ID { get; set; }
        public long SpgId { get; set; }
        public string SpgCode { get; set; }
        public int IncentiveThisMonth { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public int Year { get; set; }
        public int NumberFirstSubmit { get; set; }
        public bool IsDisplayed { get; set; }

    }

    public class IncentiveViewModel
    {
        public List<IncentiveModel> ListIncentiveModel { get; set; }
    }
}
