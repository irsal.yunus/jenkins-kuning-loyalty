﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.Settings
{
    public class SettingsModel
    {
        public SettingsModel()
        {

        }

        public SettingsModel(Data.Settings data)
        {
            this.ID = data.ID;
            this.Code = data.Code;
            this.Key = data.Key;
            this.Value = data.Value;
        }
        public int ID { get; set; }
        public string Code { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }


    }
}
