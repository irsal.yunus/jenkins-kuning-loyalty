﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.BalanceLog
{
    public class UpdatePointModel
    {
        public UpdatePointModel()
        {

        }

        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public int MemberID { get; set; }
        public int PoolID { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string ID { get; set; }
    }



}
