﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.BalanceLog
{
    public static class BalanceLogTranslator
    {
        /// <summary>
        /// transform list object data layer 'BalanceLog' to List BalanceLogModel
        /// </summary>
        /// <param name="data">List DataMember.BalanceLog</param>
        /// <returns></returns>
        public static List<BalanceLogModel> Translated(this List<Data.BalanceLog> data)
        {
            List<BalanceLogModel> result = new List<BalanceLogModel>();

            foreach (var item in data)
            {
                result.Add(new BalanceLogModel(item));
            }

            return result;

        }

        public static List<BalanceLogModel> Translated(this List<Data.GetHistoryBalanceLog_Result> data)
        {
            List<BalanceLogModel> result = new List<BalanceLogModel>();

            foreach (var item in data)
            {
                result.Add(new BalanceLogModel(item));
            }

            return result;

        }
    }
}
