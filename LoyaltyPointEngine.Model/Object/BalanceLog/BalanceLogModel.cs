﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.BalanceLog
{
    public class BalanceLogModel
    {
        public BalanceLogModel()
        {

        }

        public BalanceLogModel(Data.BalanceLog data)
        {
            this.ID = data.ID;
            this.MemberID = data.MemberID;
            this.Points = data.Points;
            this.PoolID = data.PoolID;
            this.Balance = data.Balance;
            this.Description = data.Description;
            this.Type = data.Type;
            this.ReferenceID = data.ReferenceID;
            this.CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss");
            this.CreatedBy = data.CreatedBy;

        }

        public BalanceLogModel(Data.GetHistoryBalanceLog_Result data)
        {
            this.ID = new Guid();
            this.MemberID = data.MemberID;
            this.Points = (data.Points != null) ? (int)data.Points : 0;
            this.PoolID = 0;
            this.Balance = (data.Balance != null) ? (int)data.Balance : 0;
            this.Description = data.Description;
            this.Type = data.Type;
            this.ReferenceID = data.ReferenceID;
            this.CreatedDate = (data.CreatedDate != null) ? data.CreatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty;
            this.CreatedBy = data.CreatedBy;
        }

        public Guid ID { get; set; }
        public int MemberID { get; set; }
        public int Points { get; set; }
        public int PoolID { get; set; }
        public int Balance { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public Guid ReferenceID { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }



    }


}
