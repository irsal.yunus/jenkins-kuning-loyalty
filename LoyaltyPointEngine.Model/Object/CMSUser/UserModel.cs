﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Model.Object.CMSUser
{
    public class UserModel
    {
        public string FullName { get; set; }
        public int Approved { get; set; }
        public int Reject { get; set; }
    }
}
