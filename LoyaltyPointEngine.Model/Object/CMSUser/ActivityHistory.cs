﻿using System;

namespace LoyaltyPointEngine.Model.Object.CMSUser
{
    public class ActivityHistory
    {
        public long CmsUserId { get; set; }
        public string Activity { get; set; }
        public DateTime ActivityDate { get; set; }
        public string UserName { get; set; }
        public string CheckInImage { get; set; }
        public string RetailerName { get; set; }
    }
}
