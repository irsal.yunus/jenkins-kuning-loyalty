﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Extension
{
    public static class ObjectExtension
    {
        public static object GetPropertyValue(this object @object, string property)
        {
            return @object.GetType().GetProperty(property).GetValue(@object, null);
        }

        public static T GetPropertyValue<T>(this object @object, string property, T defaultValue = default(T))
        {
            var value = @object.GetType().GetProperty(property).GetValue(@object, null);
            if (value == null)
            {
                if (defaultValue != null)
                    return defaultValue;
                return default(T);
            }
            return (T)value;
        }
    }
}
