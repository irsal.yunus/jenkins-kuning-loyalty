﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LoyaltyPointEngine.Common.Extension
{
    public class EnumHelper
    {
        public static List<SelectListItem> ToSelectList<T>(bool convertValue = false, string selectedValue = null)
        {
            var selectList = new List<SelectListItem>();
            var enumValues = Enum.GetValues(typeof(T));
            foreach (var value in enumValues)
            {
                var valueString = (convertValue ? Convert.ToInt32(value).ToString() : value.ToString());
                selectList.Add(new SelectListItem() { Selected = (valueString == selectedValue), Text = value.ToString(), Value = valueString });
            }
            return selectList;
        }

        /// <summary>
        /// Convert enum object into multi select list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="convertValue">default is false. if true than the enum will be converted into <int>.</param>
        /// <param name="selectedValue">nullable. if convertValue is true then selectedValue will be converted from <string> into <int></param>
        /// <returns></returns>
        public static MultiSelectList ToMultiSelectList<T>(bool convertValue = false, List<string> selectedValue = null)
        {
            Dictionary<string, string> selectList = new Dictionary<string, string>();
            var enumValues = Enum.GetValues(typeof(T));
            foreach (var value in enumValues)
            {
                selectList.Add(value.ToString(), (convertValue ? Convert.ToInt32(value).ToString() : value.ToString()));
            }

            if (selectedValue != null)
                return new MultiSelectList(selectList, "Value", "Key", selectedValue);
            return new MultiSelectList(selectList, "Value", "Key");
        }
    }
}
