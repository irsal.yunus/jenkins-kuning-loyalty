﻿using LoyaltyPointEngine.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Extension
{
    public static class StringExtension
    {
        public static string FirstCharToUpper(this string input)
        {
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }

        public static bool IsBase64(this string input)
        {
            return StringHelper.IsBase64String(input);
        }

        public static string OnlyAlphaNumeric(this string input)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            return rgx.Replace(input, "");
        }

        public static string OnlyAlpha(this string input)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            return rgx.Replace(input, "");
        }
    }
}
