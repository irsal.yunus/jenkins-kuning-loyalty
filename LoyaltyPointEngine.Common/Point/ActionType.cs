﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Point
{
    public class ActionType
    {
        public enum Code
        {
            /// <summary>
            /// REGSITER MANUAL
            /// </summary>
            REGMANUAL = 1,
            /// <summary>
            /// REGISTER VIA SMS
            /// </summary>
            REGSMS = 2,
            /// <summary>
            /// REGISTRASI VIA FB LOGIN
            /// </summary>
            REGFB = 3,
            /// <summary>
            /// COMMENT FACEBOOK
            /// </summary>
            FBCOMMENT = 4,
            /// <summary>
            /// LIKE FACEBOOK
            /// </summary>
            FBLIKE = 5,
            /// <summary>
            /// REGISTRASI MANUAL OLEH SALES(NBA)
            /// </summary>
            REGNBA = 6,
            /// <summary>
            /// REGSITER MANUAL MELALUI MOBILE APP
            /// </summary>
            REGMANUALMOBILE = 7,
            /// <summary>
            /// REGSITER MANUAL MELALUI MOBILE APP DENGAN NBA CODE
            /// </summary>
            REGNBAMOBILE = 8,
            /// <summary>
            /// REGSITER FACEBOOK MELALUI MOBILE APP
            /// </summary>
            REGFACEBOOKMOBILE = 9
        }
    }
}
