﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common
{
    public class EventLogLogic
    {
        public enum LogType
        {
            Information = 1,
            Warning = 2,
            Error = 3,
            Exception = 4,
            ApiCall = 5,
            ApiResult = 6
        }
        public enum LogSource
        {
            Global = 1,
            Member = 2,
            Child = 3,
            Notification = 4,
            Access = 5,
            SalesPerson = 6,
            Navigation = 7,
            CMSUser = 8,
            AreaAndRegion = 9,
            Settings = 10,
            Action = 11,
            Collect = 12,
            Campaign = 13,
            CapGeneration = 14,
            Receipt = 15,
            FacebookPost = 16,
            PromoActivity = 17,
            Umbraco = 18,
            LogSendSMS = 19,
            StoreCode = 20,
            EventCode = 21
        }

        public enum LogEventCode
        {
            Global = 1,
            View = 2,
            Add = 3,
            Edit = 4,
            Delete = 5,
        }

    }
}
