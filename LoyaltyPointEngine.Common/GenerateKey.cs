﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common
{
    public class GenerateKey
    {
        private static Random random = new Random();
        public static string RandomKey(int length, string library = null, string param = null)
        {
            if (string.IsNullOrEmpty(library))
            {
                library = Guid.NewGuid().ToString();
            }

            return new string(Enumerable.Repeat(library, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
