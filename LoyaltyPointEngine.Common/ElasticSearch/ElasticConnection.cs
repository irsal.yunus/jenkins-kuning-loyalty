﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections.Generic;

using Nest;
using Newtonsoft.Json;

using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using Elasticsearch.Net;

namespace LoyaltyPointEngine.Common.ElasticSearch
{
    public class ElasticConnection
    {
        /// Instantiated client
        public ElasticClient Client { get; set; }

        public IClusterHealthResponse HealthStatus { get; set; }

        /// Constructor
        public ElasticConnection(string index = "")
        {
            var node = new Uri(SiteSetting.ElasticServerUrl);
            var _settings = new ConnectionSettings(node);
            _settings.BasicAuthentication(SiteSetting.ElasticServerAuthUser, SiteSetting.ElasticServerAuthPassword);
            _settings.DefaultIndex(string.IsNullOrEmpty(index) ? SiteSetting.ElasticServerDefaultIndex : index);
            _settings.DisableDirectStreaming();

            _settings.ServerCertificateValidationCallback(CertificateValidations.AllowAll);

            Client = new ElasticClient(_settings);

            HealthStatus = Client.ClusterHealth();
        }
    }

}
