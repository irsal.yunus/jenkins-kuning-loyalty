﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace LoyaltyPointEngine.Common.Helper
{
    public class RESTHelper
    {
        public static T Get<T>(string url, string token = "")
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);
                if (!string.IsNullOrEmpty(token.Trim()))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                HttpResponseMessage responseMessage = client.GetAsync(url).Result;
                return ResultHandler<T>(responseMessage);
            }
        }

        public static T Get<T>(string url, object param, string token = "")
        {
            if (url.Contains("?"))
            {
                return Get<T>(string.Format("{0}{1}{2}", url, "&", RESTHelper.GetQueryString(param)), token);
            }
            else
            {
                return Get<T>(string.Format("{0}{1}{2}", url, "?", RESTHelper.GetQueryString(param)), token);
            }
        }

        public static T Post<T>(string url, object param, string token = "")
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);
                if (!string.IsNullOrEmpty(token.Trim()))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string postBody = JsonConvert.SerializeObject(param);
                HttpResponseMessage responseMessage = client.PostAsync(url, new StringContent(postBody, Encoding.UTF8, "application/json")).Result;
                return ResultHandler<T>(responseMessage);
            }
        }

        public static T PostNotJson<T>(string url, string param, string token = "")
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);
                if (!string.IsNullOrEmpty(token.Trim()))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string postBody = param;
                HttpResponseMessage responseMessage = client.PostAsync(url, new StringContent(postBody, Encoding.UTF8, "application/x-www-form-urlencoded")).Result;
                return ResultHandler<T>(responseMessage);
            }
        }
        public static T ResultHandler<T>(HttpResponseMessage responseMessage)
        {
            string responseString = responseMessage.Content.ReadAsStringAsync().Result;
            if (responseMessage.StatusCode == HttpStatusCode.OK)
            {
                if (JSONHelper.IsValidJsonString(responseString))
                {
                    return JsonConvert.DeserializeObject<T>(responseString);
                }
                else
                {
                    return default(T);
                }
            }
            else if (responseMessage.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new HttpException((int)HttpStatusCode.Unauthorized, responseString);
            }
            else
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, responseString);
            }
        }

        public static string GetQueryString(object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }
    }
}
