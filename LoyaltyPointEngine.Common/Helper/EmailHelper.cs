﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Helper
{
    public class EmailHelper
    {
        public static bool IsValidEmail(string email)
        {
            bool isValid = false;
            if (!string.IsNullOrWhiteSpace(email))
            {
                Match match = Regex.Match(email, "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    isValid = true;
                }
            }

            return isValid;
        }

        public static string GetEmailTemplate(string template_url)
        {
            string text = System.IO.File.ReadAllText(template_url);
            return text;
        }

        public static void SendEmail(string subject, string message, string EmailTo, string SiteEmail, string SMTP_NameDisplay,
            string SMTPHost, bool SMTPSSL, string SMTPUsername, string SMTPPassword, int SMTPPort, 
            List<string> attachmentsPath = null)
        {
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(SiteEmail, SMTP_NameDisplay);
            if (!string.IsNullOrEmpty(EmailTo))
            {
                if (EmailTo.Contains(";"))
                {
                    var arrayEmail = EmailTo.Split(';');
                    foreach (string mail in arrayEmail)
                    {
                        mailMessage.To.Add(new MailAddress(mail));
                    }
                }
                else
                {
                    mailMessage.To.Add(new MailAddress(EmailTo));
                }

            }



            mailMessage.Subject = subject;
            mailMessage.Body = message;
            mailMessage.IsBodyHtml = true;
            if (attachmentsPath != null)
            {
                foreach (string attac in attachmentsPath)
                {
                    mailMessage.Attachments.Add(new Attachment(attac));
                }
            }
            
            //mailMessage.Headers.Add("Message-id", string.Format("{0}{1}@axa-life.co.id", "AXA", DateTime.Now.ToString("ddMMyyyyHHmmss")));
            //mailMessage.Headers.Add("Date", DateTime.Now.ToString("ddMMyyyyHHmmss"));

            SmtpClient client = new SmtpClient();
            if (!string.IsNullOrEmpty(SMTPHost)) client.Host = SMTPHost;
            client.EnableSsl = SMTPSSL;
            if (!string.IsNullOrEmpty(SMTPUsername)) client.Credentials = new NetworkCredential(SMTPUsername, SMTPPassword);
            if (SMTPPort >0)
            {
                client.Port = SMTPPort;
            }

            client.Send(mailMessage);
        }
    }
}
