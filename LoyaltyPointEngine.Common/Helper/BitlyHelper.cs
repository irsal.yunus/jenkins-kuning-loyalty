﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitlyDotNET.Implementations;

namespace LoyaltyPointEngine.Common.Helper
{
    public static class BitlyHelper
    {
        public static string ShortenBitlyUsingAPiKey(string longUrl, out string errorMsg)
        {
            errorMsg = "";
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(longUrl))
            {

                BitlyService bitlyApps = new BitlyService(SiteSetting.BitlyUser, SiteSetting.BitlyApiKey);
                BitlyDotNET.Interfaces.StatusCode resCode = bitlyApps.Shorten(longUrl, out result);
                if (resCode != BitlyDotNET.Interfaces.StatusCode.OK)
                {
                    errorMsg = resCode.ToString();
                    result = string.Empty;
                }
            }
            else
                errorMsg = "Invalid LongUrl";
            return result;
        }
    }
}
