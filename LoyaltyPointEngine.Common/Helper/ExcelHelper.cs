﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace LoyaltyPointEngine.Common.Helper
{
    public static class ExcelHelper
    {
        public static MemoryStream exportToExcel<T>(IList<T> data, string headerstring = "") where T : class
        {

            MemoryStream stream = new MemoryStream();

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                // Add a WorkbookPart to the document.
                WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "mySheet" };
                sheets.Append(sheet);

                // Get the sheetData cell table.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                WorkbookStylesPart stylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                stylesPart.Stylesheet = GenerateStyleSheet();
                stylesPart.Stylesheet.Save();

                //bikin logic perulangan

                var columns = getColumnsFromObj<T>();
                int i = 0,j = 0;

                //jika ada header
                if (headerstring != "")
                {
                    i++;
                    Row row;
                    row = new Row() { RowIndex = System.Convert.ToUInt32(i), StyleIndex = 1 };

                    sheetData.Append(row);
                    string[] splittedStrings = headerstring.ToString().Split(new string[] { "{/*/}" }, StringSplitOptions.None);

                    foreach (string str in splittedStrings)
                    {
                        j++;
                        Cell refCell = null;
                        foreach (Cell cell in row.Elements<Cell>())
                        {
                            if (string.Compare(cell.CellReference.Value, IndexToColumn(j) + i, true) > 0)
                            {
                                refCell = cell;
                                break;
                            }
                        }

                        Cell newCell = new Cell() { CellReference = IndexToColumn(j) + i, StyleIndex = 1 };

                        row.InsertBefore(newCell, refCell);
                        newCell.CellValue = new CellValue(splittedStrings[j-1]);
                        newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                    }


                    j = 0;
                }
                //end of header logic

                foreach (T Trow in data)
                {
                    i++;
                    Row row;
                    row = new Row() { RowIndex = System.Convert.ToUInt32(i) };
                    sheetData.Append(row);

                    foreach (var col in columns)
                    {

                        if (col(Trow).ToString().Contains("{/*/}"))
                        {
                            string[] splittedStrings = col(Trow).ToString().Split(new string[] { "{/*/}" }, StringSplitOptions.None);

                            foreach (string str in splittedStrings) {
                                j++;
                                Cell refCell = null;
                                foreach (Cell cell in row.Elements<Cell>())
                                {
                                    if (string.Compare(cell.CellReference.Value, IndexToColumn(j) + i, true) > 0)
                                    {
                                        refCell = cell;
                                        break;
                                    }
                                }

                                Cell newCell = new Cell() { CellReference = IndexToColumn(j) + i };
                                row.InsertBefore(newCell, refCell);
                                newCell.CellValue = new CellValue(str);
                                newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                            }

                        }
                        else {
                            j++;
                            Cell refCell = null;
                            foreach (Cell cell in row.Elements<Cell>())
                            {
                                if (string.Compare(cell.CellReference.Value, IndexToColumn(j) + i, true) > 0)
                                {
                                    refCell = cell;
                                    break;
                                }
                            }

                            Cell newCell = new Cell() { CellReference = IndexToColumn(j) + i };
                            row.InsertBefore(newCell, refCell);
                            if (col(Trow) != null)
                            {
                                newCell.CellValue = new CellValue(col(Trow).ToString());
                                newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                            }
                            else
                            {
                                newCell.CellValue = new CellValue("");
                                newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                            }
                        }
                    }
                    j = 0;
                }

                //for (int i = 1; i <= numRows;i++ )
                //{
                //    // Add a row to the cell table.
                //    Row row;
                //    row = new Row() { RowIndex = System.Convert.ToUInt32(i)};
                //    sheetData.Append(row);

                //    for (int j = 1; j <= numCols; j++)
                //    {

                //        // In the new row, find the column location to insert a cell in A1.
                //        Cell refCell = null;
                //        foreach (Cell cell in row.Elements<Cell>())
                //        {
                //            if (string.Compare(cell.CellReference.Value, IndexToColumn(j) + i, true) > 0)
                //            {
                //                refCell = cell;
                //                break;
                //            }
                //        }

                //        Cell newCell = new Cell() { CellReference = IndexToColumn(j) + i };
                //        row.InsertBefore(newCell, refCell);
                //        // Set the cell value to be a numeric value of 100.
                //        newCell.CellValue = new CellValue(i.ToString() + j.ToString());
                //        newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                //    }
                //}
                spreadsheetDocument.Close();
                return stream;
            }

        }

        public static MemoryStream ExportToExcelNewVersion(List<List<string>> listData, List<string> listHeader)
        {
            MemoryStream stream = new MemoryStream();
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                // Add a WorkbookPart to the document.
                WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart.
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                // Add Sheets to the Workbook.
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook.
                Sheet sheet = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "mySheet" };
                sheets.Append(sheet);

                // Get the sheetData cell table.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                WorkbookStylesPart stylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                stylesPart.Stylesheet = GenerateStyleSheet();
                stylesPart.Stylesheet.Save();

                int rowCount = 0, colCount = 0;
                if (listHeader.Count > 0)
                {
                    rowCount++;
                    Row row;
                    row = new Row() { RowIndex = System.Convert.ToUInt32(rowCount), StyleIndex = 1 };

                    sheetData.Append(row);
                    foreach (string str in listHeader)
                    {
                        colCount++;
                        Cell refCell = null;
                        foreach (Cell cell in row.Elements<Cell>())
                        {
                            //if (string.Compare(cell.CellReference.Value, IndexToColumn(colCount) + rowCount, true) > 0)
                            //{
                            //    refCell = cell;
                            //    break;
                            //}
                            if (GetColumnIndex(cell.CellReference.Value) > GetColumnIndex(IndexToColumn(colCount) + rowCount))
                            {
                                refCell = cell;
                                break;
                            }
                        }
                        Cell newCell = new Cell() { CellReference = IndexToColumn(colCount) + rowCount, StyleIndex = 1 };
                        row.InsertBefore(newCell, refCell);
                        newCell.CellValue = new CellValue(str);
                        newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                    }

                    colCount = 0;
                }

                if (listData.Count > 0)
                {
                    foreach (var data in listData)
                    {
                        List<string> objData = (List<string>)data;
                        if (objData.Count > 0)
                        {
                            rowCount++;
                            Row row;
                            row = new Row() { RowIndex = System.Convert.ToUInt32(rowCount)};

                            sheetData.Append(row);
                            foreach (string obj in objData)
                            {
                                colCount++;
                                Cell refCell = null;
                                foreach (Cell cell in row.Elements<Cell>())
                                {
                                    //if (string.Compare(cell.CellReference.Value, IndexToColumn(colCount) + rowCount, true) > 0)
                                    //{
                                    //    refCell = cell;
                                    //    break;
                                    //}
                                    if (GetColumnIndex(cell.CellReference.Value) > GetColumnIndex(IndexToColumn(colCount) + rowCount))
                                    {
                                        refCell = cell;
                                        break;
                                    }
                                }
                                Cell newCell = new Cell() { CellReference = IndexToColumn(colCount) + rowCount };
                                row.InsertBefore(newCell, refCell);
                                newCell.CellValue = new CellValue(obj);
                                newCell.DataType = new EnumValue<CellValues>(CellValues.String);
                            }

                            colCount = 0;
                        }
                    }
                }
                spreadsheetDocument.Close();
                return stream;
            }
        }

        private static Stylesheet GenerateStyleSheet()
        {
            return new Stylesheet(
                new Fonts(
                    new Font(                                                               // Index 0 - The default font.
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 1 - The bold font.
                        new Bold(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 2 - The Italic font.
                        new Italic(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 2 - The Times Roman font. with 16 size
                        new FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Times New Roman" })
                ),
                new Fills(
                    new Fill(                                                           // Index 0 - The default fill.
                        new PatternFill() { PatternType = PatternValues.None }),
                    new Fill(                                                           // Index 1 - The default fill of gray 125 (required)
                        new PatternFill() { PatternType = PatternValues.Gray125 }),
                    new Fill(                                                           // Index 2 - The yellow fill.
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                        ) { PatternType = PatternValues.Solid })
                ),
                new Borders(
                    new Border(                                                         // Index 0 - The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new RightBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new TopBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new BottomBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                ),
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                          // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 1 - Bold
                    new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 - Italic
                    new CellFormat() { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 3 - Times Roman
                    new CellFormat() { FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 - Yellow Fill
                    new CellFormat(                                                                   // Index 5 - Alignment
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                    ) { FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true },
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }      // Index 6 - Border
                )
            ); // return
        }


        public static string IndexToColumn(int index)
        {
            const int ColumnBase = 26;
            const int DigitMax = 7; // ceil(log26(Int32.Max))
            const string Digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (index <= 0)
                throw new IndexOutOfRangeException("index must be a positive number");

            if (index <= ColumnBase)
                return Digits[index - 1].ToString();

            var sb = new StringBuilder().Append(' ', DigitMax);
            var current = index;
            var offset = DigitMax;
            while (current > 0)
            {
                sb[--offset] = Digits[--current % ColumnBase];
                current /= ColumnBase;
            }
            return sb.ToString(offset, DigitMax - offset);
        }

        static Func<T, object>[] getColumnsFromObj<T>()
        {
            var propertyAccessors = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(x => x.CanRead)
                        .Select((x) => CreatePropertyAccessor<T>(x));
            return propertyAccessors.Select(e => e).ToArray();
        }

        static Func<T, object> CreatePropertyAccessor<T>(PropertyInfo prop)
        {
            var param = Expression.Parameter(typeof(T), "input");
            var propertyAccess = Expression.Property(param, prop.GetGetMethod());
            var castAsObject = Expression.TypeAs(propertyAccess, typeof(object));
            var lambda = Expression.Lambda<Func<T, object>>(castAsObject, param);
            return lambda.Compile();
        }


        public static ExcelDataHelper ReadExcel(HttpPostedFileBase file, int maxRowAccept = 0)
        {
            var data = new ExcelDataHelper();

            // Check if the file is excel
            if (file.ContentLength <= 0)
            {
                data.Status.Message = "You uploaded an empty file";
                return data;
            }

            if (file.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                data.Status.Message = "Please upload a valid excel file of version 2007 and above";
                return data;
            }

            // Open the excel document
            WorkbookPart workbookPart; List<Row> rows;
            try
            {
                var document = SpreadsheetDocument.Open(file.InputStream, false);
                workbookPart = document.WorkbookPart;

                var sheets = workbookPart.Workbook.Descendants<Sheet>();
                var sheet = sheets.First();
                data.SheetName = sheet.Name;

                var workSheet = ((WorksheetPart)workbookPart.GetPartById(sheet.Id)).Worksheet;
                var columns = workSheet.Descendants<Columns>().FirstOrDefault();
                data.ColumnConfigurations = columns;

                var sheetData = workSheet.Elements<SheetData>().First();
                rows = sheetData.Elements<Row>().ToList();
            }
            catch (Exception e)
            {
                data.Status.Message = "Unable to open the file";
                return data;
            }

            if (maxRowAccept > 0)
            {
                maxRowAccept += 1; //Exclude Row Header
                if (rows != null && rows.Count > maxRowAccept)
                {
                    data.Status.Message = "Limit data row allowed for upload is " + maxRowAccept + " rows.";
                    return data;
                }
            }

            // Read the header
            if (rows != null && rows.Count > 0)
            {
                var row = rows[0];
                var cellEnumerator = GetExcelCellEnumerator(row);
                while (cellEnumerator.MoveNext())
                {
                    var cell = cellEnumerator.Current;
                    var text = ReadExcelCell(cell, workbookPart).Trim();
                    data.Headers.Add(text);
                }
            }

            // Read the sheet data
            if (rows != null && rows.Count > 1)
            {
                for (var i = 1; i < rows.Count; i++)
                {
                    var dataRow = new List<string>();
                    data.DataRows.Add(dataRow);
                    var row = rows[i];
                    var cellEnumerator = GetExcelCellEnumerator(row);
                    while (cellEnumerator.MoveNext())
                    {
                        var cell = cellEnumerator.Current;
                        var text = ReadExcelCell(cell, workbookPart).Trim();
                        dataRow.Add(text);
                    }
                }
            }

            return data;
        }

        private static IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)
                {
                    var emptycell = new Cell() { DataType = null, CellValue = new CellValue(string.Empty) };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private static string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }

        private static string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private static int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                int current = i == 0 ? letter - 65 : letter - 64; // ASCII 'A' = 65
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        public static void CheckData(List<string> data, List<string> header)
        {
            if (data.Count < header.Count)
            {
                data.Add(string.Empty);
                CheckData(data, header);
            }
        }
        public static List<string> ObjectToList(object value)
        {
            List<string> lists = null;
            if (value != null)
            {
                lists = new List<string>();
                foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(value))
                {
                    if (descriptor != null && descriptor.Name != null)
                    {
                        lists.Add(descriptor.Name);
                    }
                }
            }
            return lists;
        }

        public static Dictionary<string, int> ModeltoDictionary(List<string> model, List<string> headers)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            foreach (string key in model)
            {
                string modelkey = headers.Where(x => x.ToLower() == key.ToLower()).FirstOrDefault();
                if (!string.IsNullOrEmpty(modelkey))
                {
                    dictionary.Add(modelkey, headers.IndexOf(modelkey));
                }
            }
            return dictionary;
        }

        private static int? GetColumnIndex(string cellRef)
        {
            if (string.IsNullOrEmpty(cellRef))
                return null;

            cellRef = cellRef.ToUpper();

            int columnIndex = -1;
            int mulitplier = 1;

            foreach (char c in cellRef.ToCharArray().Reverse())
            {
                if (char.IsLetter(c))
                {
                    columnIndex += mulitplier * ((int)c - 64);
                    mulitplier = mulitplier * 26;
                }
            }

            return columnIndex;
        }
    }
}
