﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;

namespace LoyaltyPointEngine.Common
{
    public class SiteSetting
    {
        public static string SITE_URL = "SITE_URL";
        public static string API_URL = "API_URL";
        public static string ASSET_URL = "ASSET_URL";

        public static string MediaLocalImageDirectoryReceipt = "MediaLocalImageDirectoryReceipt";
        public static string RelativePathReceipt = "RelativePathReceipt";
        public static string DefaultControler = "DefaultControler";
        public static string DefaultCapGeneration = "DefaultCapGeneration";
        public static string FacebookApplicationID = "FacebookApplicationID";
        public static string FacebookApplicationSecret = "FacebookApplicationSecret";
        public static string FacebookPageID = "FacebookPageID";
        public static string TresholdNotifCollectExpired = "TresholdNotifCollectExpired";
        public static string MailBillingTemplate = "MailBillingTemplate";
        public static string SMTPHost = "SMTPHost";
        public static string SMTPUsername = "SMTPUsername";
        public static string SMTPPassword = "SMTPPassword";
        public static string SMTPPort = "SMTPPort";
        public static string SMTPSSL = "SMTPSSL";
        public static string SiteEmail = "SiteEmail";
        public static string SMTP_NameDisplay = "SMTP_NameDisplay";
        public static string MediaLocalImageDirectoryMember = "MediaLocalImageDirectoryMember";
        public static string RelativePathMember = "RelativePathMember";
        public static string MediaLocalImageDirectoryProduct = "MediaLocalImageDirectoryProduct";
        public static string RelativePathProduct = "RelativePathProduct";
        public static string CMSUserCodeOfSpg = "CMSUserCodeOfSpg";
        //For action code
        public static string UpdateProfile = "UpdateProfile";
        public static string ListProductSizeSettingKey = "ListProductSizeSetting";
        public static string ListProductFlavourSettingKey = "ListProductFlavourSetting";

        public static string UploadReceiptSuccessTitle = "UploadReceiptSuccessTitle";
        public static string UploadReceiptSuccessMsg = "UploadReceiptSuccessMsg";

        public static string MemberUploadPath = "MemberUploadPath";

        #region Snapcart
        public static string SnapcartBaseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SnapcartBaseUrl"];
            }
        }
        public static string SnapcartEndpoint
        {
            get
            {
                return ConfigurationManager.AppSettings["SnapcartEndpoint"];
            }
        }
        public static string SnapcartApiKey
        {
            get
            {
                return ConfigurationManager.AppSettings["SnapcartApiKey"];
            }
        }
        public static string SnapcartConfidentialLevel = "SnapcartConfidentialLevel";
        public static string DefaultSnapcartConfidentialLevel
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultSnapcartConfidentialLevel"];
            }
        }
        public static string SnapcartCallbackUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SnapcartCallbackUrl"];
            }
        }
        #endregion

        #region "OCR Snapcart"
        public static string SnapcartReceiptNull
        {
            get
            {
                return ConfigurationManager.AppSettings["SnapcartReceiptNull"];
            }
        }
        public static string SnapcartFailedUpdateReceipt
        {
            get
            {
                return ConfigurationManager.AppSettings["SnapcartFailedUpdateReceipt"];
            }
        }
        #endregion

        public static string Rewards_ClientId
        {
            get
            {
                return ConfigurationManager.AppSettings["Rewards_ClientId"];
            }
        }
        public static string Rewards_ClientSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["Rewards_ClientSecret"];
            }
        }
        public static string Rewards_WishListEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["Rewards_WishListEndPoint"];
            }
        }
        public static int Rewards_WishListTreshold
        {
            get
            {
                int result = 0;
                string tresholdString = ConfigurationManager.AppSettings["Rewards_WishListTreshold"];
                if (string.IsNullOrEmpty(tresholdString)) return result;
                if (int.TryParse(tresholdString, out result))
                {
                    return result;
                }
                return result;
            }
        }
        public static string BebelacImageURL
        {
            get
            {
                return ConfigurationManager.AppSettings["BebelacImageURL"];
            }
        }

        #region ALFA PROPERTIES
        public static string FileNameGetCSV
        {
            get
            {
                return ConfigurationManager.AppSettings["FileNameGetCSV"];
            }
        }
        public static string FileNamePutCSV
        {
            get
            {
                return ConfigurationManager.AppSettings["FileNamePutCSV"];
            }
        }
        public static string PathDirSend
        {
            get
            {
                return ConfigurationManager.AppSettings["PathDirSend"];
            }
        }

        public static string FtpDirSend
        {
            get
            {
                return ConfigurationManager.AppSettings["FtpDirSend"];
            }
        }
        public static string DirWinscp
        {
            get
            {
                return ConfigurationManager.AppSettings["DirWinscp"];
            }
        }
        public static string FtpSessionName
        {
            get
            {
                return ConfigurationManager.AppSettings["FtpSessionName"];
            }
        }
        public static string PathDirGet
        {
            get
            {
                return ConfigurationManager.AppSettings["PathDirGet"];
            }
        }
        public static string FtpDirGet
        {
            get
            {
                return ConfigurationManager.AppSettings["FtpDirGet"];
            }
        }
        public static string FileSalesGetCSV
        {
            get
            {
                return ConfigurationManager.AppSettings["FileSalesGetCSV"];
            }
        }
        public static string telcomnetAPIURL
        {
            get
            {
                return ConfigurationManager.AppSettings["telcomnetAPIURL"];
            }
        }
        public static string CidAlfa
        {
            get
            {
                return ConfigurationManager.AppSettings["CidAlfa"];
            }
        }
        public static string msgKePonta
        {
            get
            {
                return ConfigurationManager.AppSettings["msgKePonta"];
            }
        }
        public static string msgKeBebe
        {
            get
            {
                return ConfigurationManager.AppSettings["msgKeBebe"];
            }
        }

        public static string msgToPonta
        {
            get
            {
                return ConfigurationManager.AppSettings["msgToPonta"];
            }
        }
        public static string msgToBB
        {
            get
            {
                return ConfigurationManager.AppSettings["msgToBB"];
            }
        }
        #endregion
        public static string PathLogERRORCsv
        {
            get
            {
                return ConfigurationManager.AppSettings["PathLogERRORCsv"];
            }
        }
        public static string msgGMD
        {
            get
            {
                return ConfigurationManager.AppSettings["msgGMD"];
            }
        }
        public static string msgGMDFailed
        {
            get
            {
                return ConfigurationManager.AppSettings["msgGMDFailed"];
            }
        }
        public static int AlfaRetailId
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["AlfaRetailId"]);
            }
        }

        public static int BebePointUpdateProfile
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["BebePointUpdateProfile"]);
            }
        }


        public static DateTime? OldMemberParameterDate
        {
            get
            {
                var dateTimeStr = ConfigurationManager.AppSettings["OldMemberParameterDate"];
                if (string.IsNullOrEmpty(dateTimeStr)) return null;
                DateTime dateTimeParsed;
                if (!DateTime.TryParseExact(dateTimeStr, "dd-MM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTimeParsed))
                {
                    return null;
                }
                return dateTimeParsed;
            }
        }
        public static Dictionary<int, string> ReceiptRejectReasonList
        {
            get
            {
                return new Dictionary<int, string>() {
                    { 0, "Struk Tidak terbaca dengan Jelas" },
                    { 1, "Struk terpotong (Tidak ada Tgl/Jam Transaksi/No Struk/nama Toko)" },
                    { 2, "Toko Belum terdaftar" },
                    { 3, "Sudah mencapai Max Limit perbulan" },
                    { 4, "Struk sudah pernah diupload" },
                    { 5, "Tgl Transaksi diluar periode Program" },
                    { 6, "Produk tidak ikut serta dalam program Rewards" },
                    { 7, "Format Struk tidak sesuai dengan format resmi dari Toko terlampir" },
                    { 8, "Foto yang di upload bukan berupa Bukti Pembayaran" },
                    { 9, "Struk Dupikat, Copy & Reprint tidak bisa diproses" },
                    { 10, "Tgl Transaksi struk sudah lebih > 3 bulan"},
                    { 11, "Tidak melampirkan struk, silahkan upload ulang"},
                    { 12, "Struk sudah di proses melalui Alfa Ponta"},
                    { 13, "Tanggal Transaksi Struk/Receipt Sudah Lebih dari Satu Bulan"},
                    { 14, "Struk sudah diproses melalui JD.ID" },
                    { 15, "Struk Sudah Diproses Melalui Alfamidi Ponta" },
                    { 16, "1 Kali Upload Hanya Untuk 1 StruK" }
                };
            }
        }
        public static string MsgResponseFormatPromoIscheckTrue
        {
            get
            {
                return ConfigurationManager.AppSettings["MsgResponseFailedFormatPromoIscheckTrue"];

            }
        }

        #region ChildAgeBelow1YearRule
        public static string[] ChildAgeBelow1YearRuleAllowedActionList
        {
            get
            {
                var value = ConfigurationManager.AppSettings["ChildAgeBelow1YearRuleAllowedActionList"];
                if (string.IsNullOrEmpty(value))
                    return new string[] { };
                return value.Split(',');
            }
        }

        public static string ChildAgeBelow1YearRuleValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["ChildAgeBelow1YearRuleValidationMessage"];
            }
        }

        public static string BlankChildBirthDateRuleValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["BlankChildBirthDateRuleValidationMessage"];
            }
        }

        public static string InPregnancyPeriodRuleValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["InPregnancyPeriodRuleValidationMessage"];
            }
        }

        public static string ChildDateValidationSuccessRuleValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["ChildDateValidationSuccessRuleValidationMessage"];
            }
        }
        #endregion

        #region Integrasi ALFA MIDI
        //ADD By Bryant for Alfa integration Needed 20170505
        #region Bebe
        //Bebe Region
        public static string FileNameGetCSVBebe
        {
            get
            {
                return ConfigurationManager.AppSettings["CareLineNumber"];
            }
        }
        #endregion

        //Tambahan By Ucup 
        public static string SuccessRegisterSmsText
        {
            get
            {
                return ConfigurationManager.AppSettings["SuccessRegisterSmsText"];
            }
        }
        public static string TelcomMateCId
        {
            get
            {
                return ConfigurationManager.AppSettings["TelcomMateCId"];
            }
        }
        public static string TelcomMateCToken
        {
            get
            {
                return ConfigurationManager.AppSettings["TelcomMateCToken"];
            }
        }
        public static string BebeURL
        {
            get
            {
                return ConfigurationManager.AppSettings["BebeURL"];
            }
        }
        public static string telcomnetAPIURLBebe
        {
            get
            {
                return ConfigurationManager.AppSettings["telcomnetAPIURLBebe"];
            }
        }
        public static string CidAlfaBebe
        {
            get
            {
                return ConfigurationManager.AppSettings["CidAlfaBebe"];
            }
        }
        public static string msgKePontaBebe
        {
            get
            {
                return ConfigurationManager.AppSettings["msgKePontaBebe"];
            }
        }

        public static string msgToPontaBebe
        {
            get
            {
                return ConfigurationManager.AppSettings["msgToPontaBebe"];
            }
        }


        #region Nutri
        //Bebe Region
        public static string FileNameGetCSVNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["FileNameGetCSVNutri"];
            }
        }
        public static int AlfaRetailIdNutri
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["AlfaRetailIdNutri"]);
            }
        }
        public static int AlfaMidiRetailIdNutri
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["AlfaMidiRetailIdNutri"]);
            }
        }
        public static string FileNamePutCSVNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["FileNamePutCSVNutri"];
            }
        }
        public static string PathDirSendNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["PathDirSendNutri"];
            }
        }

        public static string FtpDirSendNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["FtpDirSendNutri"];
            }
        }
        public static string DirWinscpNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["DirWinscpNutri"];
            }
        }
        public static string FtpSessionNameNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["FtpSessionNameNutri"];
            }
        }
        public static string PathDirGetNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["PathDirGetNutri"];
            }
        }
        public static string FtpDirGetNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["FtpDirGetNutri"];
            }
        }
        public static string FileSalesGetCSVNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["FileSalesGetCSVNutri"];
            }
        }
        public static string telcomnetAPIURLNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["telcomnetAPIURLNutri"];
            }
        }
        public static string CidAlfaNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["CidAlfaNutri"];
            }
        }
        public static string msgKePontaNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["msgKePontaNutri"];
            }
        }
        public static string msgKeNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["msgKeNutri"];
            }
        }

        public static string msgToPontaNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["msgToPontaNutri"];
            }
        }
        public static string msgToNutri
        {
            get
            {
                return ConfigurationManager.AppSettings["msgToNutri"];
            }
        }
        #endregion
        #endregion

        #region MobilePushNotification
        public static string IOSKey
        {
            get
            {
                return ConfigurationManager.AppSettings["IOSKey"];
            }
        }
        public static string IOSPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["IOSPassword"];
            }
        }
        public static string GoogleCloudMessagingKey
        {
            get
            {
                return ConfigurationManager.AppSettings["GoogleCloudMessagingKey"];
            }
        }
        #endregion

        #region Careline
        public static string CareLineNumber
        {
            get
            {
                return ConfigurationManager.AppSettings["CareLineNumber"];
            }
        }

        #endregion

        //Tambahan By Ucup

        public static string TripleDesKey
        {
            get
            {
                return ConfigurationManager.AppSettings["TripleDesKey"];
            }
        }

        public static string BitlyUser
        {
            get
            {
                return ConfigurationManager.AppSettings["BitlyUser"];
            }
        }

        public static string BitlyApiKey
        {
            get
            {
                return ConfigurationManager.AppSettings["BitlyApiKey"];
            }
        }

        public static string ForgotPasswordSmsText
        {
            get
            {
                return ConfigurationManager.AppSettings["ForgotPasswordSmsText"];
            }
        }

        public static int ForgotPasswordLinkExpiredInHour
        {
            get
            {
                int result = 5;
                string tresholdString = ConfigurationManager.AppSettings["ForgotPasswordLinkExpiredInHour"];
                if (string.IsNullOrEmpty(tresholdString)) return result;
                if (int.TryParse(tresholdString, out result))
                {
                    return result;
                }
                return result;
            }
        }

        public static string VerifyForgotPaswordURL
        {
            get
            {
                return ConfigurationManager.AppSettings["VerifyForgotPaswordURL"];
            }
        }

        public static string EmailForgotPasswordTemplateURL
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailForgotPasswordTemplateURL"];
            }
        }

        public static string EmailReceiptFailedProcessTemplateURL
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailReceiptFailedProcessTemplateURL"];
            }
        }

        public static string EmailReceiptFailedProcessReceiver
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailReceiptFailedProcessReceiver"];
            }
        }
        
        public static string EmailForgotPasswordSubjectEmail
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailForgotPasswordSubjectEmail"];
            }
        }

        public static string ReceiptExpiredValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["ReceiptExpiredValidationMessage"];
            }
        }

        #region ElasticSearch
        public static string ElasticServerUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticServerUrl"];
            }
        }

        public static string ElasticServerAuthUser
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticServerAuthUser"];
            }
        }

        public static string ElasticServerAuthPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticServerAuthPassword"];
            }
        }

        public static string ElasticServerDefaultIndex
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticServerDefaultIndex"];
            }
        }

        public static string ElasticReceiptReportIndex
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticReceiptReportIndex"];
            }
        }

        public static string ElasticNotificationIndex
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticNotificationIndex"];
            }
        }

        public static string ElasticMemberReportIndex
        {
            get
            {
                return ConfigurationManager.AppSettings["ElasticMemberReportIndex"];
            }
        }
        #endregion

        #region "Message Error Login"
        public static string WrongEmailPasswordLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["WrongEmailPasswordLoginValidationMessage"];
            }
        }

        public static string LockingLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["LockingLoginValidationMessage"];
            }
        }

        public static string NoActiveLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["NoActiveLoginValidationMessage"];
            }
        }

        public static string UserPassBlankLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["UserPassBlankLoginValidationMessage"];
            }
        }

        public static string IncorrectCSLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["IncorrectCSLoginValidationMessage"];
            }
        }

        public static string IncorrectCILoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["IncorrectCILoginValidationMessage"];
            }
        }

        public static string IncorrectCICSLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["IncorrectCICSLoginValidationMessage"];
            }
        }

        public static string BlankParamLoginValidationMessage
        {
            get
            {
                return ConfigurationManager.AppSettings["BlankParamLoginValidationMessage"];
            }
        }
        #endregion

        #region "Lixus Settings"
        public static string ApiSendOTP
        {
            get
            {
                return ConfigurationManager.AppSettings["ApiSendOTP"];
            }
        }

        public static string FormatMessageLixus
        {
            get
            {
                return ConfigurationManager.AppSettings["FormatMessageLixus"];
            }
        }

        public static string ChannelWeb
        {
            get
            {
                return ConfigurationManager.AppSettings["ChannelWeb"];
            }
        }
        public static string ChannelForgotPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["ChannelForgotPassword"];
            }
        }

        #endregion

        #region "Sentry IO Setting"
        public static string RavenClientDsn
        {
            get
            {
                return ConfigurationManager.AppSettings["RavenClientDsn"];
            }
        }

        public static string RavenClientEnvironment
        {
            get
            {
                return ConfigurationManager.AppSettings["RavenClientEnvironment"];
            }
        }
        #endregion
    }
}
