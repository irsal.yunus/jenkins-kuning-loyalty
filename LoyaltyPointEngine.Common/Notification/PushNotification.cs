﻿using LoyaltyPointEngine.Common.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Notification
{
    /// <summary>
    /// Push notification helper for mobile device.
    /// ps : only available for Android and iOS at the moment.
    /// </summary>
    public class PushNotification
    {
        /// <summary>
        /// Send push notification for single android device only
        /// </summary>
        /// <param name="DeviceID"></param>
        /// <param name="Title"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static string SendAndroid(string DeviceID, string Title, string Message, string leftIcon = "", string rightIcon = "")
        {
            return _SendAndroid(new
            {
                to = DeviceID,
                data = new
                {
                    title = Title,
                    message = Message,
                    leftIcon = leftIcon,
                    rightIcon = rightIcon
                }
            });
        }

        /// <summary>
        /// Send push notification for multiple android devices
        /// </summary>
        /// <param name="DeviceIDs"></param>
        /// <param name="Title"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static void SendAndroid(string[] DeviceIDs, string Title, string Message, string leftIcon = "", string rightIcon = "", string Tag_Url = "")
        {
            if (DeviceIDs.Length > 0)
            {
                int index = 0;
                while (index <= DeviceIDs.Length - 1)
                {
                    _SendAndroid(new
                    {
                        to = DeviceIDs[index],
                        data = new
                        {
                            title = Title,
                            message = Message,
                            leftIcon = leftIcon,
                            rightIcon = rightIcon,
                            LinkUrl = Tag_Url
                        }
                    });
                    index++;
                }
            }
        }

        /// <summary>
        /// Send push notification for topic groups
        /// </summary>
        /// <param name="DeviceIDs"></param>
        /// <param name="Title"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static string SendAndroidTopic(string Topic, string Title, string Message, string leftIcon = "", string rightIcon = "")
        {
            return _SendAndroid(new
            {
                to = Topic,
                data = new
                {
                    title = Title,
                    message = Message,
                    leftIcon = leftIcon,
                    rightIcon = rightIcon
                }
            });
        }

        /// <summary>
        /// Send push notification object to google cloud messaging server
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private static string _SendAndroid(object param)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.Authorization] = "key=" + SiteSetting.GoogleCloudMessagingKey;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    return client.UploadString("https://fcm.googleapis.com/fcm/send", "POST", JsonConvert.SerializeObject(param));
                }
            }
            catch (Exception ex)
            {
                return ex.Message ?? ex.InnerException.Message;
            }
        }

        private static SslStream sslStream;
        /// <summary>
        /// Open connection to apple push gateway
        /// </summary>
        /// <returns></returns>
        private static bool ConnectToAPNS()
        {
            string CerType = ""; //"Production";
            X509Certificate2Collection certs = new X509Certificate2Collection();
            X509Certificate2 certificate = new X509Certificate2(SiteSetting.IOSKey, SiteSetting.IOSPassword, X509KeyStorageFlags.MachineKeySet);
            certs.Add(certificate);
            string apsHost;
            if (CerType == "Production")
                apsHost = "gateway.push.apple.com";
            else
                apsHost = "gateway.sandbox.push.apple.com";
            TcpClient tcpClient = new TcpClient(apsHost, 2195);
            sslStream = new SslStream(tcpClient.GetStream());
            if (CerType == "Production")
                sslStream.AuthenticateAsClient(apsHost, certs, SslProtocols.Tls, false);
            else
                sslStream.AuthenticateAsClient(apsHost, certs, SslProtocols.Default, false);
            return true;
        }

        /// <summary>
        /// Send push notification object to apple push gateway
        /// </summary>
        /// <param name="DeviceID"></param>
        /// <param name="Title"></param>
        /// <param name="Message"></param>
        /// <param name="Badge"></param>
        /// <returns></returns>
        public static bool SendIOS(string DeviceID, string Title, string Message, int Badge, string Tag_Url = "")
        {
            ConnectToAPNS();

            byte[] buf = new byte[256];
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write(new byte[] { 0, 0, 32 });

            byte[] deviceToken = StringHelper.HexToByteArray(DeviceID);
            bw.Write(deviceToken);
            bw.Write((byte)0);

            object param = new
            {
                aps = new
                {
                    alert = Message,
                    badge = Badge,
                    LinkUrl = Tag_Url,
                    sound = "noti.aiff",
                }
            };

            string msg = JsonConvert.SerializeObject(param);

            bw.Write((byte)msg.Length);
            bw.Write(msg.ToCharArray());
            bw.Flush();

            if (sslStream != null)
            {
                sslStream.Write(ms.ToArray());
                return true;
            }
            return false;
        }

        public static void SendIOS(string[] DeviceIDs, string Title, string Message, int Badge, string Tag_Url = "")
        {
            if (DeviceIDs.Length > 0)
            {
                int index = 0;
                while (index <= DeviceIDs.Length - 1)
                {
                    SendIOS(DeviceIDs[index], Title, Message, Badge, Tag_Url);
                    index++;
                }
            }
        }
    }
}
