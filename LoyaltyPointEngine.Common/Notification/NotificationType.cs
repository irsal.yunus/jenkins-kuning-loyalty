﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Notification
{
    public class NotificationType
    {
        public enum Type
        {
            Information = 1,
            Warning = 2,
            //Error = 3,

            //Struck
            OnProgress =4,//--> saat struk pertama kali dibuat
            Reject =5, //--> Struck ditolak admin
            Approved =6, // --> Struck di Approved admin
            RejectReupload = 12, //--> Struck ditolak admin tapi boleh di reupload

            //REDEEM
            CheckOut = 7,// --> Redeem Pertama kali dibuat
            OnShipping = 8,// --> Sedang dikirim
            Delivered = 9, // --> hadiah sudah diterima member
            Void = 10, // --> point expired  Void = 9,// --> hadiah di void

            Expired = 11, // --> point expired
            Event = 13,

            Custom = 100
        }
    }
}
