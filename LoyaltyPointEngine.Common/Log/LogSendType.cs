﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Common.Log
{
    public class LogSendType
    {
        public LogSendType() { }
        private LogSendType(string value) { Value = value; }

        public string Value { get; set; }

        public static LogSendType ForgetPassword { get { return new LogSendType("Forget Password"); } }
        public static LogSendType RegisterMember { get { return new LogSendType("Register Member"); } }
        public static LogSendType ResendSms { get { return new LogSendType("Resend SMS"); } }
        public static LogSendType Scheduler { get { return new LogSendType("Scheduler"); } }
    }
}
