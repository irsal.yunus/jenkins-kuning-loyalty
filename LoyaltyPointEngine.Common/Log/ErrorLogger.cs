﻿using LoyaltyPointEngine.Common.Extension;
using System;
using System.Collections.Generic;
using System.Web;

namespace LoyaltyPointEngine.Common.Log
{
    public class HttpErrorLogger
    {
        public class ErrorModel
        {
            /*RouteDetail*/
            public string ControllerName { get; set; }
            public string ActionName { get; set; }

            /*SessionDetail*/
            public bool? HasSession { get; set; }
            public string Session { get; set; }

            /*RequestDetail*/
            public Dictionary<string, string> RequestHeader { get; set; }
            public string RequestData { get; set; }

            /*ExceptionDetail*/
            public Exception Exception { get; set; }
            public DateTime CreatedDateTime { get; set; }
        }

        public readonly HttpRequestBase RequestBase;

        public HttpErrorLogger(HttpRequestBase requestBase)
        {
            if (requestBase == null)
            {
                throw new NullReferenceException("requestBase is null at ErrorLogUtility.");
            }
            RequestBase = requestBase;
        }

        public HttpErrorLogger(HttpRequest request)
            : this(new HttpRequestWrapper(request))
        {

        }

        public ErrorModel GetErrorModel(Exception exception)
        {
            var errorModel = new ErrorModel();
            SetRouteDetail(ref errorModel);
            SetRequestDetail(ref errorModel);
            SetExceptionDetail(ref errorModel, exception);
            return errorModel;
        }

        public ErrorModel GetErrorModel(Exception exception, object session)
        {
            var errorModel = GetErrorModel(exception);
            SetSessionDetail(ref errorModel, session);
            return errorModel;
        }

        private void SetRequestDetail(ref ErrorModel errorModel)
        {
            errorModel.RequestHeader = RequestBase.ToRaw();
            errorModel.RequestData = RequestBase.ParamsToString();
        }

        private void SetRouteDetail(ref ErrorModel errorModel)
        {
            Func<string, string> routeDataValue = delegate(string indexName)
            {
                bool hasValue = RequestBase.RequestContext.RouteData.Values[indexName] != null;
                return hasValue ? RequestBase.RequestContext.RouteData.Values[indexName].ToString() : "";
            };
            errorModel.ControllerName = routeDataValue("controller");
            errorModel.ActionName = routeDataValue("action");
        }

        private void SetExceptionDetail(ref ErrorModel errorModel, Exception exception)
        {
            errorModel.Exception = exception;
            errorModel.CreatedDateTime = DateTime.Now;
        }

        private void SetSessionDetail(ref ErrorModel errorModel, object session)
        {
            errorModel.HasSession = session != null;
            errorModel.Session = (session != null) ? session.ToString() : "";
        }
    }
}
