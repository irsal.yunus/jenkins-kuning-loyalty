﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace LoyaltyPointEngine.Common.Log
{
    public enum LoggerState
    {
        Idle,
        Writing
    }

    public class Logger
    {
        private string _SystemPath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
        private string _LogNameFormat = "Log_{0}.log";

        public string LogPath { get; private set; }
        public Dictionary<double, string> Queues { get; private set; }
        public LoggerState State { get; private set; }

        public static Logger Log { get; set; }

        public Logger()
        {
            LogPath = Path.Combine(_SystemPath, "Log");
            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }
            Queues = new Dictionary<double, string>();
        }

        /// <summary>
        /// Log everything you want here
        /// </summary>
        /// <param name="logString"></param>
        public void Push(string logString)
        {
            double logTimeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            Queues.Add(logTimeStamp, logString);
            Write();
        }

        /// <summary>
        /// This function will only log your application exception
        /// </summary>
        /// <param name="request"></param>
        public void Push(HttpRequest request)
        {
            double logTimeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            HttpErrorLogger.ErrorModel errorModel = new HttpErrorLogger(request).GetErrorModel(HttpContext.Current.Server.GetLastError());
            var logString = Newtonsoft.Json.JsonConvert.SerializeObject(errorModel);
            Push(logString);
        }

        /// <summary>
        /// This function will only log your application exception plus your session
        /// </summary>
        /// <param name="request"></param>
        /// <param name="session"></param>
        public void Push(HttpRequest request, object session)
        {
            double logTimeStamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            HttpErrorLogger.ErrorModel errorModel = new HttpErrorLogger(request).GetErrorModel(HttpContext.Current.Server.GetLastError(), session);
            var logString = Newtonsoft.Json.JsonConvert.SerializeObject(errorModel);
            Push(logString);
        }

        private void Write()
        {
            if (Queues.Any())
            {
                var nextLog = Queues.OrderBy(x => x.Key).FirstOrDefault();
                StreamWriter fileStream;
                if (!GetCurrentLogFile(out fileStream))
                {
                    try
                    {
                        State = LoggerState.Writing;
                        using (StreamWriter logStream = fileStream)
                        {
                            logStream.WriteLine(nextLog.Value);
                            logStream.WriteLine(Environment.NewLine);
                            logStream.Dispose();
                            logStream.Close();
                            Queues.Remove(nextLog.Key);
                            State = LoggerState.Idle;
                            WriteNext();
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                State = LoggerState.Idle;
            }
        }

        private void WriteNext()
        {
            if (Queues.Any() && State == LoggerState.Idle)
            {
                Write();
            }
        }

        private bool GetCurrentLogFile(out StreamWriter logStream)
        {
            String currentLogFileName = Path.Combine(LogPath, String.Format(_LogNameFormat, DateTime.Today.ToString("yyyyMMdd")));
            if (!File.Exists(currentLogFileName))
            {
                File.Create(currentLogFileName);
            }
            try
            {
                FileStream fileStream = File.Open(currentLogFileName, FileMode.Append, FileAccess.Write);
                logStream = new StreamWriter(fileStream);
                return false;
            }
            catch (IOException ex)
            {
                logStream = null;
                return true;
            }
        }
    }
}
