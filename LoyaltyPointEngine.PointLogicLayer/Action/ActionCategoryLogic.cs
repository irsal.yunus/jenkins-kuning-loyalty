﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;


namespace LoyaltyPointEngine.PointLogicLayer.Action
{
    public class ActionCategoryLogic
    {
        public static ResultPaginationModel<List<ActionCategoryModel>> Search(ParamPagination param)
        {
            ResultPaginationModel<List<ActionCategoryModel>> result = new ResultPaginationModel<List<ActionCategoryModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var ActionCategoryLinq = Data.ActionCategory.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        ActionCategoryLinq = ActionCategoryLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        ActionCategoryLinq = ActionCategoryLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        ActionCategoryLinq = ActionCategoryLinq.Where(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                        total = Data.ActionCategory.GetAll().Count(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                    }
                    else
                    {
                        total = Data.ActionCategory.GetAll().Count();
                    }


                    var ActionCategorys = ActionCategoryLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} ActionCategory(s)", ActionCategorys.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = ActionCategorys.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<ActionCategoryModel> GetActionCategoryModelById(int ID)
        {
            ResultModel<ActionCategoryModel> result = new ResultModel<ActionCategoryModel>();
            var dataStage = Data.ActionCategory.GetById(ID);
            if (dataStage != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get ActionCategory with ID {0}", ID);
                result.Value = new ActionCategoryModel(dataStage);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("ActionCategory with ID {0} not found in system", ID);
            }



            return result;
        }


    }
}
