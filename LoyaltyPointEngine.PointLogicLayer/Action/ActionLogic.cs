﻿using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using LoyaltyPointEngine.Model.Parameter.Action;

namespace LoyaltyPointEngine.PointLogicLayer.Action
{
    public class ActionLogic
    {
        public static ActionPointModel GetPointByAction(Data.Action dataAction, int? campaignId = null)
        {
            ActionPointModel result = new ActionPointModel();
            if (dataAction != null)
            {
                int point = 0;
                int actionPoint = 0;
                //int campaignPoint = 0;
                bool isCampaign = false;
                string campaignName = string.Empty;
                actionPoint = dataAction.Points;
                bool IsPregnancyProduct = false;
                if (dataAction.ProductID.HasValue)
                {
                    var product = Product.GetByID(dataAction.ProductID.Value);
                    if (product != null)
                    {
                        IsPregnancyProduct = product.IsPregnancyProduct;
                    }
                }
                //var currentCampaigns = CampaignActionDetail.GetAllActiveByAction(dataAction.ID).OrderByDescending(x=> x.CreatedDate).ToList();
                //CampaignActionDetail currCampaign = null;
                //if (currentCampaigns != null && currentCampaigns.Count() > 0)
                //    currCampaign = currentCampaigns.Where(x => !x.Campaign.IsDeleted && !x.Action.IsDeleted).FirstOrDefault();

                //if (currCampaign != null)
                //{
                //    campaignPoint = currCampaign.Campaign.Value;
                //    string typePercent = LoyaltyPointEngine.PointLogicLayer.CampaignLogic.CAMPAIGN_TYPE.PERCENTAGE.ToString();
                //    string typeFixNumber = LoyaltyPointEngine.PointLogicLayer.CampaignLogic.CAMPAIGN_TYPE.FIXEDNUMBER.ToString();
                //    if (currCampaign.Campaign.Type == typePercent)
                //    {
                //        decimal percentval = ((decimal)campaignPoint / 100) * actionPoint;
                //        point = actionPoint + (int)percentval;
                //    }
                //    else if (currCampaign.Campaign.Type == typeFixNumber)
                //        point = actionPoint + campaignPoint;
                //    isCampaign = true;
                //    campaignName = currCampaign.Campaign.Name;
                //    result.CampaignID = currCampaign.CampaignID;
                //}
                //else
                //{
                //    point = actionPoint;
                //}

                point = actionPoint;
                result.Points = point;
                result.IsCampaign = isCampaign;
                result.CampaignName = campaignName;
                result.IsPregnancyProduct = IsPregnancyProduct;
            }

            return result;
        }


        public static ResultPaginationModel<List<ActionModel>> Search(ParamActionPagination param)
        {
            ResultPaginationModel<List<ActionModel>> result = new ResultPaginationModel<List<ActionModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var ActionCategoryLinq = param.ActionCategoryID.HasValue?Data.Action.GetByCategoryID(param.ActionCategoryID.Value): Data.Action.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        ActionCategoryLinq = ActionCategoryLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        ActionCategoryLinq = ActionCategoryLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        ActionCategoryLinq = ActionCategoryLinq.Where(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                        total = param.ActionCategoryID.HasValue ?
                            Data.Action.GetByCategoryID(param.ActionCategoryID.Value).Count(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search)) :
                            Data.Action.GetAll().Count(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                    }
                    else
                    {
                        total =param.ActionCategoryID.HasValue ?
                            Data.Action.GetByCategoryID(param.ActionCategoryID.Value).Count(): Data.Action.GetAll().Count();
                    }


                    var ActionCategorys = ActionCategoryLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} ActionCategory(s)", ActionCategorys.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = ActionCategorys.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<string> GetActionByCode(string Code)
        {
            ResultModel<string> result = new ResultModel<string>();


            if (!string.IsNullOrEmpty(Code))
            {
                try
                {

                    var dataAction = Data.Action.GetByCode(Code);
                    if (dataAction != null)
                    {
                        var data = dataAction.Points;

                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("success get Action(s)");

                        //use this exted method to translate from object datalayer to model
                        result.Value = dataAction.Points.ToString();
                    }


                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }


       

        public static ResultModel<List<ActionModel>> GetByCode(string Code)
        {
            ResultModel<List<ActionModel>> result = new ResultModel<List<ActionModel>>();

            if (!string.IsNullOrEmpty(Code))
            {
                try
                {

                    var dataGroup = Data.ProductGroup.GetByCode(Code);
                    if (dataGroup != null)
                    {
                        var data = Data.Action.GetByProductGroupID(dataGroup.ID).ToList();
                       
                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("success get {0} Action(s)", data.Count);

                        //use this exted method to translate from object datalayer to model
                        result.Value = data.Translated();
                    }

                   
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }


        public static ResultModel<ActionModel> GetActionModelById(int ID)
        {
            ResultModel<ActionModel> result = new ResultModel<ActionModel>();
            var dataStage = Data.Action.GetById(ID);
            if (dataStage != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get ActionCategory with ID {0}", ID);
                result.Value = new ActionModel(dataStage);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("ActionCategory with ID {0} not found in system", ID);
            }



            return result;
        }


        public static ResultModel<List<string>> ProductGroup()
        {
            ResultModel<List<string>> result = new ResultModel<List<string>>();


            result.StatusCode = "10";
            result.StatusMessage = string.Format("No data found");
            var datas = Data.ProductGroup.GetAll().Where(x => x.IsActived).ToList();
            if (datas != null && datas.Count > 0)
            {
                result.Value = datas.Where(x => x.ProductGroupCode != null && x.ProductGroupCode != "").Select(x => x.ProductGroupCode).Distinct().ToList();
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get {0} group(s)", result.Value.Count);
            }
            return result;
        }

        public static bool IsExist(int id)
        {
            return Data.Action.IsExist(id);
        }
    }
}
