﻿using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object.CMSUser;
using System.Linq;

namespace LoyaltyPointEngine.PointLogicLayer.CmsUser
{
    public class CmsUserLogic
    {
        public static IQueryable<ActivityHistory> GetCmsUserActivityHistory()
        {
            return (from L in DataRepositoryFactory.CurrentRepository.LoginLog
                    select new ActivityHistory
                    {
                        CmsUserId = L.MemberID,
                        Activity = L.Activity,
                        ActivityDate = L.ActivityDate,
                        UserName = L.UserName,
                        CheckInImage = "",
                        RetailerName = ""
                    })
                    .Union(from C in DataRepositoryFactory.CurrentRepository.CMSUserCheckIns
                           join U in DataRepositoryFactory.CurrentRepository.CMSUser on C.CmsUserID equals U.ID into Us
                           from U in Us.DefaultIfEmpty()
                           join R in DataRepositoryFactory.CurrentRepository.Retailer on C.RetailerID equals R.ID into Rs
                           from R in Rs.DefaultIfEmpty()
                           select new ActivityHistory
                           {
                               CmsUserId = C.CmsUserID,
                               Activity = "Check In",
                               ActivityDate = C.CreatedDate,
                               UserName = U.Username,
                               CheckInImage = C.CheckInImage,
                               RetailerName = C.IsManual ? C.RetailerManualName : R.Name
                           });
        }
    }
}
