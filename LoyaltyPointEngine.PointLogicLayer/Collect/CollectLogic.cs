﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using System;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using LoyaltyPointEngine.PointLogicLayer.Point;
using LoyaltyPointEngine.PointLogicLayer.Action;
using System.Collections.Generic;
using System.Data.Objects;
using log4net;
using Newtonsoft.Json;

namespace LoyaltyPointEngine.PointLogicLayer.Collect
{
    public class CollectLogic : MainLogic
    {
        private static ILog Log = LogManager.GetLogger(typeof(CollectLogic));

        public CollectLogic()
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(CollectLogic));
            }
        }

        /**
         * Note : Please use TransactionScope outside this function
         **/
        public ResultModel<Data.Collect> InsertCollect(CollectCMSModel model, int PoolID, string UserName, long? UserID, string Url, int TimeStamp,
            int totalMemberReceipts = 1, Data.Action dataAction = null, bool SaveWithBalance = true, bool MemberIsIdle = false, bool isNoCapByRetailer = false,
            int lastPoint = 0, DateTime? createdDate = null, bool isModerate = false)
        {
            //initial
            CapActionPoint curCapAction = null;
            CapGeneration curCapMember = null;
            //CapGeneration curCapCampaign = null;
            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();

            Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(model.ActionID.Value);

            //Data.CampaignActionDetail currCampaign = CampaignActionDetail.GetAllActiveByAction(currAction.ID).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
            CampaignActionDetail currCampaign = new CampaignActionDetail();
            if (model.CampaignID.HasValue)
                currCampaign = CampaignActionDetail.GetAll()
                    .Where(x => x.ActionID == currAction.ID)
                    .Where(x => x.CampaignID == model.CampaignID)
                    .FirstOrDefault();
            else
                currCampaign = null;

            DateTime transactionDate = DateTime.MinValue;
            int actionPoint = currAction.Points;
            int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
            int quantityApproved = (model.QuantityApproved != 0) ? model.QuantityApproved : model.Quantity;
            int pointApproved = currentPoint * quantityApproved; // default

            int memberCapPoint = 0; int.TryParse(Settings.GetValueByKey(SiteSetting.DefaultCapGeneration), out memberCapPoint);
            int actionCapPoint = (currAction.IsHaveCapGeneration) ? currAction.CapGeneration.Value : 0;
            int campaignCapPoint = (currCampaign != null && currCampaign.ID > 0) ? currCampaign.Campaign.CapGenerationPoint : 0;

            //int totalMemberReceipts = Receipt.GetByMemberId(model.MemberID).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve && x.ID != model.ReceiptID);

            List<int> listCapPoint = new List<int>() { memberCapPoint, actionCapPoint, campaignCapPoint };

            int maxPoint = listCapPoint.Where(x => x > 0).Min();

            int sisaPoint = 0;

            if (!string.IsNullOrEmpty(model.TransactionDate))
            {

                string datetime = model.TransactionDate + " " + (model.TransactionTime == null ? "00:00:00" : model.TransactionTime);
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    transactionDate = dt;
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Format date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                    return result;
                }

            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Transaction Date and Transaction time are Mandatory");
                return result;
            }


            #region validate Cap Point
            /**
             * maxPoint adalah Point Maksimum yang bisa di dapat member berdasarkan Cap Member, Action, Campaign yang terkecil
             * CapPoint Action = current Action.CapGeneration
             * CapPoint Member = DefaultCapGeneration from Settings
             * CapPoint Campaign = current Campaign.CapGeneration
             **/

            if (!isNoCapByRetailer && !isModerate)
            {
                //PENJAGAAN 1: cap Action
                if (currAction.IsHaveCapGeneration)
                {
                    curCapAction = CapActionPoint.GetByMemberID(model.MemberID)
                        .Where(x => x.ActionID == currAction.ID && EntityFunctions.TruncateTime(transactionDate) <= EntityFunctions.TruncateTime(x.EndDate.Value) &&
                         EntityFunctions.TruncateTime(transactionDate) >= EntityFunctions.TruncateTime(x.StartDate.Value))
                        .OrderByDescending(x => x.CreatedDate)
                        .FirstOrDefault();
                    if (curCapAction != null && curCapAction.ID > 0)
                    {
                        int sisaPointCapAction = curCapAction.CapPoint - curCapAction.GenerationPoint;
                        int selisihPoint = pointApproved - sisaPointCapAction;
                        if (selisihPoint >= currentPoint)
                        {
                            string message = string.Empty;
                            if (sisaPointCapAction <= 0)
                                message = "Unable to Add Collect. You have reached maximum point.";
                            else
                                message = "Your action's point is insufficient, your max point is " + sisaPointCapAction + ". please change quantity!";
                            result.StatusCode = (sisaPointCapAction < 0) ? "0" : sisaPointCapAction.ToString();
                            result.StatusMessage = message;
                            return result;
                        }
                        else
                            sisaPoint = sisaPointCapAction;
                    }
                    else
                    {
                        //penjagaan jikalau belum pernah ada CAP ACTION DI DB
                        int sisaPointCapAction = currAction.CapGeneration.GetValueOrDefault();
                        if (sisaPointCapAction > 0)
                        {
                            int selisihPoint = pointApproved - sisaPointCapAction;
                            if (selisihPoint >= currentPoint)
                            {
                                string message = string.Empty;
                                if (sisaPointCapAction <= 0)
                                    message = "Unable to Add Collect. You have reached maximum point.";
                                else
                                    //message = "Your action's point is insufficient, your max point is " + sisaPointCapAction + ". please change quantity!";
                                    message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapAction + ". mohon mengurangi quantity!";
                                result.StatusCode = (sisaPointCapAction < 0) ? "0" : sisaPointCapAction.ToString();
                                result.StatusMessage = message;
                                return result;
                            }
                            else
                                sisaPoint = sisaPointCapAction;
                        }

                    }


                }
            }
            var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == model.ActionID).FirstOrDefault();
            DateTime transDate = DateTime.ParseExact(model.TransactionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
            bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
            bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
            bool PECENTAGE = currCampaign != null && CampaignActionActive != null ? BOOL_PERCENTAGE : false;
            bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null ? BOOL_FIXEDNUMBER : false;
            var ActionName = Data.Action.GetById((int)model.ActionID).Name;

            if (!isNoCapByRetailer && !isModerate)
            {
                //PENJAGAAN 2 : Cap Member
                if (!PECENTAGE && !FIXEDNUMBER)
                {
                    curCapMember = CapGeneration.GetByMemberID(model.MemberID)
                    .Where(x => x.CampaignID == null && EntityFunctions.TruncateTime(transactionDate) <= EntityFunctions.TruncateTime(x.EndDate.Value) &&
                         EntityFunctions.TruncateTime(transactionDate) >= EntityFunctions.TruncateTime(x.StartDate.Value))
                    .OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                    if (curCapMember != null && curCapMember.ID > 0)
                    {
                        int sisaPointCapMember = curCapMember.CapPoint - curCapMember.GenerationPoint;
                        int selisihPoint = pointApproved - sisaPointCapMember;
                        if (selisihPoint >= currentPoint)
                        {
                            string message = string.Empty;
                            if (sisaPointCapMember == 0)
                                message = "Unable to Add Collect. You have reached maximum point.";
                            else
                                //oint member sudah mencapai maximum point, mohon mengurangi poin sebesar 500 dengan cara mengurangi quantity dari 
                                message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapMember + string.Format(". dengan cara mengurangi quantity dari {0}!", ActionName);
                            result.StatusCode = (sisaPointCapMember < 0) ? "0" : sisaPointCapMember.ToString();
                            result.StatusMessage = message;
                            return result;
                        }
                        else
                        {
                            if (sisaPoint > 0)
                            {
                                sisaPoint = (sisaPoint > sisaPointCapMember) ? sisaPointCapMember : sisaPoint;
                            }
                            else
                            {
                                sisaPoint = sisaPointCapMember;
                            }

                        }
                    }
                    else
                    {
                        //kalo belum pernah punya cap
                        //  Sisah point diambil dari memberCapPoint --> table setting --> CAP GENERAL
                        int sisaPointCapMember = memberCapPoint;
                        if (sisaPointCapMember > 0)
                        {
                            int selisihPoint = pointApproved - sisaPointCapMember;
                            if (selisihPoint >= currentPoint)
                            {
                                string message = string.Empty;
                                if (sisaPointCapMember == 0)
                                    message = "Unable to Add Collect. You have reached maximum point.";
                                else
                                    //oint member sudah mencapai maximum point, mohon mengurangi poin sebesar 500 dengan cara mengurangi quantity dari 
                                    message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapMember + string.Format(". dengan cara mengurangi quantity dari {0}!", ActionName);
                                result.StatusCode = (sisaPointCapMember < 0) ? "0" : sisaPointCapMember.ToString();
                                result.StatusMessage = message;
                                return result;
                            }
                            else
                            {
                                if (sisaPoint > 0)
                                {
                                    sisaPoint = (sisaPoint > sisaPointCapMember) ? sisaPointCapMember : sisaPoint;
                                }
                                else
                                {
                                    sisaPoint = sisaPointCapMember;
                                }

                            }
                        }


                    }
                }

            }

            ////PENJAGAAN 3: Cap Campaign disable untuk fase 2 --> PENJAGAAN KE 3
            //TODO: VALIDASI LAGI untuk cap campaign

            #endregion validate cap point


            //int _TotalPoint = (sisaPoint > 0 && sisaPoint < pointApproved) ? sisaPoint : pointApproved;
            int _TotalPoint = (sisaPoint > 0 && sisaPoint < pointApproved) ? sisaPoint : pointApproved;
            int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;

            Data.Collect collect = new Data.Collect();
            collect.ID = Guid.NewGuid();
            collect.MemberID = model.MemberID;
            collect.ActionID = model.ActionID;
            collect.Quantity = model.Quantity;
            collect.QuantityApproved = quantityApproved;
            collect.Points = model.Points;
            if (FIXEDNUMBER && model.QuantityApproved != 0)
                collect.TotalPoints = _TotalPoint + _PointCamp;//model.TotalPoints;
            else if (PECENTAGE && model.QuantityApproved != 0)
            {
                var total = _TotalPoint * ((decimal)_PointCamp / 100);
                collect.TotalPoints = (int)total;
                //collect.TotalPoints = _TotalPoint + (_TotalPoint * (_PointCamp / 100));//model.TotalPoints;

            }
            else if (model.QuantityApproved == 0)
                collect.TotalPoints = 0;
            else
                collect.TotalPoints = _TotalPoint;
            collect.ItemPrice = model.ItemPrice;
            if (totalMemberReceipts < 1)
            {
                collect.Remarks = "[First Submission] " + model.Remarks;
            }
            else
            {
                collect.Remarks = model.Remarks;
            }
            if (currCampaign != null)
                collect.CampaignID = currCampaign.Campaign.ID;
            collect.TransactionDate = transactionDate;
            collect.ReceiptID = model.ReceiptID.HasValue ? model.ReceiptID : null;
            collect.RedeemID = model.RedeemID.HasValue ? model.RedeemID : null;
            collect.AdjustmentReferenceID = model.AdjustmentReferenceID.HasValue ? model.AdjustmentReferenceID : null;
            collect.ExpiryDate = DateTime.Now.AddDays(currAction.ExpiredDate);
            collect.PoolID = PoolID;
            collect.IsApproved = true;
            collect.IsActive = true;
            collect.IsUsed = false;
            collect.ChildID = model.ChildID;

            if (SaveWithBalance && MemberIsIdle)
            {
                if (string.IsNullOrEmpty(collect.Remarks))
                {
                    collect.Remarks = "[First Submission Idle]";
                }
                else
                {
                    collect.Remarks = "[First Submission Idle] " + collect.Remarks;
                }
            }

            var json = JsonConvert.SerializeObject(collect);
            Log.Info("Data Collect = " + json);

            var res = collect.Insert(UserName, Url, UserID, TimeStamp);
            if (res.Success)
            {
                if (SaveWithBalance)
                {
                    var resbalace = InsertBalanceAndCap(collect, PoolID, Url, UserName, UserID, TimeStamp, FIXEDNUMBER, PECENTAGE, "", lastPoint, createdDate);
                    if (resbalace.StatusCode == "00")
                    {
                        result.StatusCode = resbalace.StatusCode;
                        result.StatusMessage = resbalace.StatusMessage;
                        result.Value = resbalace.Value;
                    }
                    else
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format(resbalace.StatusMessage);
                    }
                }
                else
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Collect Success Inserted";
                }
            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format(res.ErrorMessage == null ? "" : res.ErrorMessage);
            }
            return result;
        }

        public ResultModel<Data.Collect> InsertBalanceAndCap(Data.Collect collect, int PoolID, string Url, string UserName, long? UserID, int timeStamp,
            bool FIXEDNUMBER, bool PECENTAGE, string desc = "", int lastPoint = 0, DateTime? createdDate = null)
        {
            //initial
            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();
            DateTime transactionDate = new DateTime(collect.TransactionDate.Year, collect.TransactionDate.Month, collect.TransactionDate.Day);
            DateTime firstDayOfMonth = new DateTime(transactionDate.Year, transactionDate.Month, 1);
            DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            Data.Action currAction = Data.Action.GetById(collect.ActionID.Value);
            Data.CampaignActionDetail currCampaign = currAction.CampaignActionDetail.Where(
                x => !x.IsDeleted && x.IsActive).FirstOrDefault();

            int memberCapPoint = 0; int.TryParse(Settings.GetValueByKey(SiteSetting.DefaultCapGeneration), out memberCapPoint);
            int actionCapPoint = (currAction.IsHaveCapGeneration) ? currAction.CapGeneration.Value : 0;
            int campaignCapPoint = (currCampaign != null && currCampaign.ID > 0) ? currCampaign.Campaign.CapGenerationPoint : 0;

            CapActionPoint curCapAction = CapActionPoint.GetByMemberID(collect.MemberID)
                    .Where(x => x.ActionID == currAction.ID && EntityFunctions.TruncateTime(transactionDate) <= EntityFunctions.TruncateTime(x.EndDate.Value) &&
                     EntityFunctions.TruncateTime(transactionDate) >= EntityFunctions.TruncateTime(x.StartDate.Value))
                    .OrderBy(x => x.CreatedDate)
                    .FirstOrDefault();
            CapGeneration curCapMember = CapGeneration.GetByMemberID(collect.MemberID)
                .Where(x => x.CampaignID == null && EntityFunctions.TruncateTime(transactionDate) <= EntityFunctions.TruncateTime(x.EndDate.Value) &&
                     EntityFunctions.TruncateTime(transactionDate) >= EntityFunctions.TruncateTime(x.StartDate.Value))
                .OrderBy(x => x.CreatedDate)
                .FirstOrDefault();
            CapGeneration curCapCampaign = null;

            int _TotalPoint = collect.TotalPoints;
            var res = BalanceLogLogic.Credit(_TotalPoint, string.Format("Penambahan Point {0}", collect.Action.Name), collect.MemberID, PoolID, UserName, collect.ID,
                timeStamp, lastPoint, createdDate);
            if (res != null && res.Success)
            {
                #region Insert / Update Cap Generation

                //cap Action
                if (currAction.IsHaveCapGeneration)
                {
                    if (curCapAction != null && curCapAction.ID > 0)
                    {
                        curCapAction.GenerationPoint += _TotalPoint;
                        curCapAction.Update(UserName, Url, UserID);
                    }
                    else
                    {
                        CapActionPoint capAction = new CapActionPoint();
                        capAction.ActionID = currAction.ID;
                        capAction.MemberID = collect.MemberID;
                        capAction.StartDate = transactionDate;
                        capAction.EndDate = transactionDate.AddDays(currAction.CapGenerationDays);
                        capAction.CapPoint = actionCapPoint;
                        capAction.GenerationPoint = _TotalPoint;
                        capAction.Insert(UserName, Url, UserID);
                    }
                }

                //Cap Member
                if (!PECENTAGE && !FIXEDNUMBER)
                {
                    if (curCapMember != null && curCapMember.ID > 0)
                    {
                        curCapMember.GenerationPoint += _TotalPoint;
                        curCapMember.Update(UserName, Url, UserID);
                    }
                    else
                    {

                        CapGeneration capMember = new CapGeneration();
                        capMember.MemberID = collect.MemberID;
                        capMember.StartDate = firstDayOfMonth;
                        capMember.EndDate = lastDayOfMonth;
                        capMember.CapPoint = memberCapPoint;
                        capMember.GenerationPoint = _TotalPoint;
                        capMember.Insert(UserName, Url, UserID);
                    }

                }

                ////Cap Campaign disable untuk fase 2
                #endregion Insert / Update cap generation

                result.StatusCode = "00";
                result.StatusMessage = string.Format("Collect Success Inserted");
                result.Value = collect;
            }
            else
            {

                result.StatusCode = "500";
                result.StatusMessage = string.Format(res.ErrorMessage);
            }
            return result;
        }

        public ResultModel<Data.Collect> ValidateCapPoint(CollectCMSModel model, ref int totalGeneration, Data.Action dataAction = null, bool isNoCapByRetailer = false)
        {
            //initial
            CapActionPoint curCapAction = null;
            CapGeneration curCapMember = null;

            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();

            Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(model.ActionID.Value);

            CampaignActionDetail currCampaign = new CampaignActionDetail();
            if (model.CampaignID.HasValue)
                currCampaign = CampaignActionDetail.GetAll()
                    .Where(x => x.ActionID == currAction.ID)
                    .Where(x => x.CampaignID == model.CampaignID)
                    .FirstOrDefault();
            else
                currCampaign = null;

            DateTime transactionDate = DateTime.MinValue;
            int actionPoint = currAction.Points;
            int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
            int quantityApproved = (model.QuantityApproved != 0) ? model.QuantityApproved : model.Quantity;
            int pointApproved = currentPoint * quantityApproved; // default

            int memberCapPoint = 0; int.TryParse(Settings.GetValueByKey(SiteSetting.DefaultCapGeneration), out memberCapPoint);
            int actionCapPoint = (currAction.IsHaveCapGeneration) ? currAction.CapGeneration.Value : 0;
            int campaignCapPoint = (currCampaign != null && currCampaign.ID > 0) ? currCampaign.Campaign.CapGenerationPoint : 0;

            //int totalMemberReceipts = Receipt.GetByMemberId(model.MemberID).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve && x.ID != model.ReceiptID);

            List<int> listCapPoint = new List<int>() { memberCapPoint, actionCapPoint, campaignCapPoint };

            int maxPoint = listCapPoint.Where(x => x > 0).Min();

            int sisaPoint = 0;

            if (!string.IsNullOrEmpty(model.TransactionDate))
            {

                string datetime = model.TransactionDate + " " + (model.TransactionTime == null ? "00:00:00" : model.TransactionTime);
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    transactionDate = dt;
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Format date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                    return result;
                }

            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Transaction Date and Transaction time are Mandatory");
                return result;
            }


            #region validate Cap Point
            /**
             * maxPoint adalah Point Maksimum yang bisa di dapat member berdasarkan Cap Member, Action, Campaign yang terkecil
             * CapPoint Action = current Action.CapGeneration
             * CapPoint Member = DefaultCapGeneration from Settings
             * CapPoint Campaign = current Campaign.CapGeneration
             **/

            if (!isNoCapByRetailer)
            {
                //PENJAGAAN 1: cap Action
                if (currAction.IsHaveCapGeneration)
                {
                    curCapAction = CapActionPoint.GetByMemberID(model.MemberID)
                        .Where(x => x.ActionID == currAction.ID && EntityFunctions.TruncateTime(transactionDate) <= EntityFunctions.TruncateTime(x.EndDate.Value) &&
                         EntityFunctions.TruncateTime(transactionDate) >= EntityFunctions.TruncateTime(x.StartDate.Value))
                        .OrderByDescending(x => x.CreatedDate)
                        .FirstOrDefault();
                    if (curCapAction != null && curCapAction.ID > 0)
                    {
                        if (totalGeneration == 0)
                        {
                            totalGeneration = curCapAction.GenerationPoint;
                        }
                        int sisaPointCapAction = curCapAction.CapPoint - totalGeneration;
                        int selisihPoint = pointApproved - sisaPointCapAction;
                        if (selisihPoint >= currentPoint)
                        {
                            string message = string.Empty;
                            if (sisaPointCapAction <= 0)
                                message = "Unable to Add Collect. You have reached maximum point.";
                            else
                                //oint member sudah mencapai maximum point, mohon mengurangi poin sebesar 500 dengan cara mengurangi quantity dari 
                                //message = "Your action's point is insufficient, your max point is " + sisaPointCapAction + ". please change quantity!";
                                message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapAction + ". mohon mengurangi quantity!";
                            result.StatusCode = (sisaPointCapAction < 0) ? "0" : sisaPointCapAction.ToString();
                            result.StatusMessage = message;
                            return result;
                        }
                        else
                            sisaPoint = sisaPointCapAction;
                    }
                    else
                    {
                        //penjagaan jikalau belum pernah ada CAP ACTION DI DB
                        int sisaPointCapAction = totalGeneration;
                        if (sisaPointCapAction > 0)
                        {
                            int selisihPoint = pointApproved - sisaPointCapAction;
                            if (selisihPoint >= currentPoint)
                            {
                                string message = string.Empty;
                                if (sisaPointCapAction <= 0)
                                    message = "Unable to Add Collect. You have reached maximum point.";
                                else
                                    //message = "Your action's point is insufficient, your max point is " + sisaPointCapAction + ". please change quantity!";
                                    message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapAction + ". mohon mengurangi quantity!";
                                result.StatusCode = (sisaPointCapAction < 0) ? "0" : sisaPointCapAction.ToString();
                                result.StatusMessage = message;
                                return result;
                            }
                            else
                                sisaPoint = sisaPointCapAction;
                        }

                    }


                }
            }
            var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == model.ActionID).FirstOrDefault();
            DateTime transDate = DateTime.ParseExact(model.TransactionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
            bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
            bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
            bool PECENTAGE = currCampaign != null && CampaignActionActive != null ? BOOL_PERCENTAGE : false;
            bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null ? BOOL_FIXEDNUMBER : false;
            var ActionName = Data.Action.GetById((int)model.ActionID).Name;

            if (!isNoCapByRetailer)
            {
                //PENJAGAAN 2 : Cap Member
                if (!PECENTAGE && !FIXEDNUMBER)
                {
                    curCapMember = CapGeneration.GetByMemberID(model.MemberID)
                    .Where(x => x.CampaignID == null && EntityFunctions.TruncateTime(transactionDate) <= EntityFunctions.TruncateTime(x.EndDate.Value) &&
                         EntityFunctions.TruncateTime(transactionDate) >= EntityFunctions.TruncateTime(x.StartDate.Value))
                    .OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                    if (curCapMember != null && curCapMember.ID > 0)
                    {
                        if (totalGeneration == 0)
                        {
                            totalGeneration = curCapMember.GenerationPoint;
                        }
                        int sisaPointCapMember = curCapMember.CapPoint - totalGeneration;
                        int selisihPoint = pointApproved - sisaPointCapMember;
                        if (selisihPoint >= currentPoint)
                        {
                            string message = string.Empty;
                            if (sisaPointCapMember == 0)
                                message = "Unable to Add Collect. You have reached maximum point.";
                            else
                                //oint member sudah mencapai maximum point, mohon mengurangi poin sebesar 500 dengan cara mengurangi quantity dari 
                                message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapMember + string.Format(". dengan cara mengurangi quantity dari {0}!", ActionName);
                            result.StatusCode = (sisaPointCapMember < 0) ? "0" : sisaPointCapMember.ToString();
                            result.StatusMessage = message;
                            return result;
                        }
                        else
                        {
                            if (sisaPoint > 0)
                            {
                                sisaPoint = (sisaPoint > sisaPointCapMember) ? sisaPointCapMember : sisaPoint;
                            }
                            else
                            {
                                sisaPoint = sisaPointCapMember;
                            }

                        }
                    }
                    else
                    {
                        //kalo belum pernah punya cap
                        //  Sisah point diambil dari memberCapPoint --> table setting --> CAP GENERAL
                        int sisaPointCapMember = memberCapPoint;
                        if (sisaPointCapMember > 0)
                        {
                            int selisihPoint = pointApproved - sisaPointCapMember;
                            if (selisihPoint >= currentPoint)
                            {
                                string message = string.Empty;
                                if (sisaPointCapMember == 0)
                                    message = "Unable to Add Collect. You have reached maximum point.";
                                else
                                    //oint member sudah mencapai maximum point, mohon mengurangi poin sebesar 500 dengan cara mengurangi quantity dari 
                                    message = "Point member sudah mencapai maximum point, mohon mengurangi poin sebesar " + sisaPointCapMember + string.Format(". dengan cara mengurangi quantity dari {0}!", ActionName);
                                result.StatusCode = (sisaPointCapMember < 0) ? "0" : sisaPointCapMember.ToString();
                                result.StatusMessage = message;
                                return result;
                            }
                            else
                            {
                                if (sisaPoint > 0)
                                {
                                    sisaPoint = (sisaPoint > sisaPointCapMember) ? sisaPointCapMember : sisaPoint;
                                }
                                else
                                {
                                    sisaPoint = sisaPointCapMember;
                                }

                            }
                        }


                    }
                }

            }

            int _TotalPoint = (sisaPoint > 0 && sisaPoint < pointApproved) ? sisaPoint : pointApproved;
            totalGeneration = _TotalPoint;

            ////PENJAGAAN 3: Cap Campaign disable untuk fase 2 --> PENJAGAAN KE 3
            //TODO: VALIDASI LAGI untuk cap campaign

            #endregion validate cap point

            result.StatusCode = "00";
            result.StatusMessage = "";
            return result;
        }
    }
}

