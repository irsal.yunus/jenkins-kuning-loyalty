﻿using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.BalanceLog;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using System.Globalization;

namespace LoyaltyPointEngine.PointLogicLayer.Point
{
    public class BalanceLogLogic
    {
        public static ResultModel<int> GetTotalPoint(int MemberId)
        {
            ResultModel<int> result = new ResultModel<int>();
            result.Value = BalanceLog.GetTotalPointByMemberId(MemberId);

            result.StatusCode = "00";
            result.StatusMessage = string.Format("Poin Anda saat ini adalah {0} poin. Tukarkan poin Anda dengan berbagai produk menarik di www.bebeclub.co.id/beberewards/katalog", result.Value);
            return result;
        }


        public static ResultPaginationModel<List<BalanceLogModel>> History(ParamPaginationWithDate param, int MemberId)
        {
            ResultPaginationModel<List<BalanceLogModel>> result = new ResultPaginationModel<List<BalanceLogModel>>();

            if (param != null)
            {
                try
                {

                    DateTime? _startDate = null;
                    DateTime? _endDate = null;

                    if (!string.IsNullOrEmpty(param.StartDate))
                    {
                        DateTime dt = new DateTime(1900, 1, 1);
                        if (DateTime.TryParseExact(param.StartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            _startDate = new DateTime(dt.Year, dt.Month, dt.Day);
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = "the date (StartDate) format does not match, please using format 'dd/MM/yyyy'";
                            return result;
                        }

                    }
                    else
                        _startDate = new DateTime(1900, 1, 1);

                    if (!string.IsNullOrEmpty(param.EndDate))
                    {
                        DateTime dt = new DateTime(1900, 1, 1);
                        if (DateTime.TryParseExact(param.EndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            _endDate = new DateTime(dt.Year, dt.Month, dt.Day).AddDays(1).AddSeconds(-1);
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = "the date (EndDate) format does not match, please using format 'dd/MM/yyyy'";
                            return result;
                        }

                    }
                    else
                        _endDate = new DateTime(1900, 1, 1);

                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    #region Old Logic
                    //var BalanceLogLinq = Data.BalanceLog.GetByMemberIdAndCreatedDate(MemberId, _startDate, _endDate);

                    //if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    //{
                    //    //note: use extended order by for dynamic order by
                    //    string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                    //    BalanceLogLinq = BalanceLogLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    //}
                    //else
                    //{
                    //    BalanceLogLinq = BalanceLogLinq.OrderBy(x => x.ID);
                    //}

                    //int total = 0;
                    //if (!string.IsNullOrEmpty(param.Search))
                    //{
                    //    BalanceLogLinq = BalanceLogLinq.Where(x => x.Description.Contains(param.Search));
                    //    total = Data.BalanceLog.GetByMemberIdAndCreatedDate(MemberId, _startDate, _endDate).Count(x => x.Description.Contains(param.Search));
                    //}
                    //else
                    //{
                    //    total = Data.BalanceLog.GetByMemberIdAndCreatedDate(MemberId, _startDate, _endDate).Count();
                    //}


                    //var BalanceLogs = BalanceLogLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    //result.TotalRow = total;


                    //result.StatusCode = "00";
                    //result.StatusMessage = string.Format("success get {0} BalanceLog(s)", BalanceLogs.Count);

                    ////use this exted method to translate from object datalayer to model
                    //result.Value = BalanceLogs.Translated();
                    #endregion

                    int total = 0;

                    List<GetHistoryBalanceLog_Result> dataHistory = Data.BalanceLog.GetHistoryBalanceLog(MemberId, _startDate, _endDate).ToList();
                    if (dataHistory != null)
                    {
                        total = dataHistory.Count();
                        dataHistory = dataHistory.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();
                    }

                    result.TotalRow = total;
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} BalanceLog(s)", dataHistory.Count);

                    result.Value = dataHistory.Translated();

                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }


            return result;
        }


        /// <summary>
        /// Reduce point in balance. parameter 'point' value must positive (more than zero)
        /// </summary>
        /// <returns></returns>
        public static EFResponse Debit(int point, string remark, int MemberId, int PoolId, string by, Guid ReferenceID, int newTotalBalance, int timeStamp)
        {

            BalanceLog balance = new BalanceLog();
            balance.ID = Guid.NewGuid();
            balance.MemberID = MemberId;
            balance.Points = -point;
            balance.PoolID = PoolId;
            balance.Balance = newTotalBalance;
            balance.Description = remark;
            balance.Type = "D";
            balance.ReferenceID = ReferenceID;
            return balance.Inserted(by, timeStamp);

        }


        /// <summary>
        /// Adding Point in balance
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static EFResponse Credit(int point, string remark, int MemberId, int PoolId, string by, Guid ReferenceID, int timeStamp, int lastPoint = 0,
            DateTime? createdDate = null)
        {


            //add point to balance
            BalanceLog lastBalanceLog = new BalanceLog();
            if (lastPoint == 0)
            {
                lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(MemberId);
                if (lastBalanceLog != null)
                    lastPoint = lastBalanceLog.Balance;
            }

            int newBalance = lastPoint + point;
            BalanceLog balance = new BalanceLog();
            balance.ID = Guid.NewGuid();
            balance.MemberID = MemberId;
            balance.Points = point;
            balance.PoolID = PoolId;
            balance.Balance = newBalance;
            balance.Description = remark;
            balance.Type = "C";
            balance.ReferenceID = ReferenceID;
            return balance.Inserted(by, timeStamp, createdDate);

        }



        public static void UpdateMemberPoint(int MemberId, string by)
        {
            int total = BalanceLog.GetTotalPointByMemberId(MemberId);
            var response = MemberLogicLayer.MemberLogic.UpdateMemberPoint(MemberId, total, by);
        }

        public static ResultModel<bool> FixBalanceLog(List<string> PhoneList, string user)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            List<string> errorList = new List<string>();
            result.Value = false;

            try
            {
                if (PhoneList != null && PhoneList.Count > 0)
                {
                    foreach (var Phone in PhoneList)
                    {
                        var member = DataMember.Member.GetByPhone(Phone);
                        if (member != null)
                        {
                            var balanceLogDatas = Data.BalanceLog.GetByMemberId(member.ID);
                            if (balanceLogDatas.Count() > 1)
                            {
                                var balanceLogList = balanceLogDatas.OrderBy(x => x.CreatedDate).ToList();
                                for (int i = 1; i < balanceLogList.Count; i++)
                                {
                                    if (balanceLogList[i].Balance != balanceLogList[i - 1].Balance + balanceLogList[i].Points)
                                    {
                                        balanceLogList[i].Balance = balanceLogList[i - 1].Balance + balanceLogList[i].Points;
                                        var updateResponse = balanceLogList[i].Updated();
                                        if (!updateResponse.Success)
                                        {
                                            //result.StatusCode = "50";
                                            //result.StatusMessage = "Balance Log update failed";
                                            //return result;
                                            errorList.Add($"Phone number: {Phone}, balance log id: {balanceLogList[i].ID}, update failed");
                                        }
                                    }
                                }
                                BalanceLogLogic.UpdateMemberPoint(member.ID, user);
                            }
                            else
                            {
                                //result.StatusCode = "40";
                                //result.StatusMessage = "No BalanceLog Data found";
                                //return result;
                                errorList.Add($"Phone number: {Phone}, balance log data not found");
                            }
                        }
                        else
                        {
                            //result.StatusCode = "30";
                            //result.StatusMessage = "Member not found";
                            //return result;
                            errorList.Add($"Phone number: {Phone}, member not found");
                        }
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = "No Phone number in list";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.Message;
                return result;
            }
            result.StatusCode = "00";
            result.StatusMessage = "BalanceLog Fix success";
            result.Value = true;
            return result;
        }
    }
}
