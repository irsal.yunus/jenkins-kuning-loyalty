﻿using log4net;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Redeem;
using LoyaltyPointEngine.Model.Parameter.Redeem;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LoyaltyPointEngine.PointLogicLayer.Point
{
    public class RedeemLogic : MainLogic
    {
        private static ILog Log = LogManager.GetLogger(typeof(RedeemLogic));

        public RedeemLogic()
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(RedeemLogic));
            }
        }

        public static ResultModel<RedeemDetailModel> Get(string param)
        {
            ResultModel<RedeemDetailModel> result = new ResultModel<RedeemDetailModel>();
            Guid ID = Guid.Empty;
            if (Guid.TryParse(param, out ID))
            {
                var data = Redeem.GetById(ID);
                result.Value = new RedeemDetailModel(data);
                result.StatusCode = "00";
                result.StatusMessage = "Get Detail Redeem";
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Format ID is wrong";
            }
            return result;
        }


        public static ResultModel<RedeemDetailModel> Create(SubmitRedeemParameterModel param, int MemberId, string MemberName, int PoolId)
        {
            ResultModel<RedeemDetailModel> result = new ResultModel<RedeemDetailModel>();
            if (param != null)
            {
                if (!string.IsNullOrEmpty(param.TransactionCode))
                {
                    if (param.TransactionCode.Length <= 25)
                    {
                        if (param.Point > 0)
                        {

                            var checkRedeem = Redeem.GetByTransactionCode(param.TransactionCode);
                            if (checkRedeem != null)
                            {
                                result.StatusCode = "21";
                                result.StatusMessage = string.Format("Transaction Code already registered in system");
                            }
                            else
                            {
                                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                                {
                                    Guid RedeemId = Guid.NewGuid();
                                    int restPoint = param.Point;
                                    var collects = Data.Collect.GetAllActiveByMember(MemberId).OrderBy(x => x.ExpiryDate).ToList();
                                    int timeStamp = 0;
                                    foreach (var collect in collects)
                                    {
                                        if (restPoint <= 0)
                                        {
                                            //finish
                                            break;
                                        }
                                        else
                                        {
                                            if (restPoint >= collect.TotalPoints)
                                            {
                                                ///1. collect di habisin semua
                                                ///. respoint dikurangin
                                                collect.IsUsed = true;
                                                collect.RedeemID = RedeemId;
                                                collect.Updated(MemberName);
                                                restPoint = restPoint - collect.TotalPoints;
                                            }
                                            else
                                            {
                                                //2. di splits
                                                //collect.Remarks = "inactive due to be separated in the process redeem";
                                                collect.IsActive = false;
                                                collect.Updated(MemberName);
                                                //collect point di pecah
                                                //collect used
                                                var collectUsed = SplitCollectRedeem(restPoint, RedeemId, collect, true, MemberName, 0);
                                                var restCollect = SplitCollectRedeem(collect.TotalPoints - collectUsed.Points, null, collect, false, MemberName, 1);
                                                restPoint = 0;
                                            }
                                        }
                                    }


                                    //restPoint  -->masoh ada sisah tapi collect sudah tidak habis, tapi masih ada sisah point di balaancelog karena migrasi(migrasi gak create collect)
                                    if (restPoint > 0)
                                    {
                                        var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(MemberId);
                                        if (lastBalanceLog != null && lastBalanceLog.Balance > 0)
                                        {
                                            if ((lastBalanceLog.Balance - param.Point) >= 0)
                                            {
                                                restPoint = 0;
                                            }
                                        }
                                    }

                                    if (restPoint <= 0)
                                    {
                                        //create redeeem
                                        var redeemData = new Redeem();
                                        redeemData.ID = RedeemId;
                                        redeemData.MemberID = MemberId;
                                        redeemData.TransactionCode = param.TransactionCode;
                                        redeemData.TransactionDate = DateTime.Now;
                                        redeemData.PoolID = PoolId;
                                        redeemData.Point = param.Point;
                                        redeemData.IsVoid = false;
                                        var responseEF = redeemData.Inserted(MemberName);
                                        if (responseEF != null && responseEF.Success)
                                        {
                                            //Create BalanceLog
                                            var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(MemberId);
                                            if (lastBalanceLog != null)
                                            {
                                                int newBalanceLog = lastBalanceLog.Balance - param.Point;
                                                if (newBalanceLog >= 0)
                                                {
                                                    var respBalanceEF = BalanceLogLogic.Debit(param.Point, "Pengurangan Point No.Transaksi: " + redeemData.TransactionCode, MemberId,
                                                        PoolId, MemberName, RedeemId, newBalanceLog, timeStamp);
                                                    timeStamp++;
                                                    if (responseEF != null && responseEF.Success)
                                                    {
                                                        result.StatusCode = "00";
                                                        result.StatusMessage = string.Format("Redeem Successfully");
                                                        result.Value = new RedeemDetailModel(redeemData);
                                                        transScope.Complete();
                                                        transScope.Dispose();


                                                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.MemberID, MemberName);


                                                    }
                                                    else
                                                    {
                                                        transScope.Dispose();
                                                        result.StatusCode = "500";
                                                        result.StatusMessage = string.Format("Error when inserted data BalanceLog into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                                                    }
                                                }
                                                else
                                                {
                                                    transScope.Dispose();
                                                    result.StatusCode = "40";
                                                    result.StatusMessage = string.Format("Your Balance's point is insufficient");
                                                }
                                            }
                                            else
                                            {
                                                transScope.Dispose();
                                                result.StatusCode = "41";
                                                result.StatusMessage = string.Format("Your Balance's point is insufficient");
                                            }
                                        }
                                        else
                                        {
                                            transScope.Dispose();
                                            result.StatusCode = "500";
                                            result.StatusMessage = string.Format("Error when inserted data Redeem into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                                        }


                                    }
                                    else
                                    {
                                        transScope.Dispose();
                                        result.StatusCode = "20";
                                        result.StatusMessage = string.Format("Your point is insufficient");
                                    }
                                }//end use
                            }
                        }
                        else
                        {
                            result.StatusCode = "12";
                            result.StatusMessage = string.Format("Point must more than zero");
                        }
                    }
                    else
                    {
                        result.StatusCode = "60";
                        result.StatusMessage = string.Format("TransactionCode max 25 character");
                    }


                }
                else
                {
                    result.StatusCode = "11";
                    result.StatusMessage = string.Format("TransactionCode is required");
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Parameter is null");
            }
            return result;
        }

        public static ResultModel<RedeemDetailModel> SubmitRedeem(SubmitRedeemModel param, int MemberId, string MemberName, int PoolId)
        {
            #region "Validation Redeem"
            ResultModel<RedeemDetailModel> result = new ResultModel<RedeemDetailModel>();
            if (param == null)
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Parameter is null");
                return result;
            }

            if (string.IsNullOrEmpty(param.TransactionCode))
            {
                result.StatusCode = "11";
                result.StatusMessage = string.Format("TransactionCode is required");
                return result;
            }

            if (param.TransactionCode.Length > 25)
            {
                result.StatusCode = "60";
                result.StatusMessage = string.Format("TransactionCode max 25 character");
                return result;
            }

            var checkRedeem = Redeem.GetByTransactionCode(param.TransactionCode);

            if (checkRedeem != null)
            {
                result.StatusCode = "21";
                result.StatusMessage = string.Format("Transaction Code already registered in system");
                return result;
            }
            #endregion

            var resultData = Redeem.SubmitRedeem(param.RedemptionID, param.TransactionCode, param.MemberID, param.MemberPoint, param.ReceiverName, param.ReceiverPhone, param.ReceiverAddress, PoolId, param.UserId);

            if (resultData.Success)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("Redeem Successfully");
            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = (resultData.ErrorEntity ?? resultData.ErrorMessage);
            }

            return result;
        }

        public static ResultModel<RedeemDetailModel> CreateFromCMS(SubmitRedeemParameterCMSModel param, int PoolId)
        {
            ResultModel<RedeemDetailModel> result = new ResultModel<RedeemDetailModel>();
            if (param != null)
            {
                if (!string.IsNullOrEmpty(param.TransactionCode))
                {
                    if (param.Point > 0)
                    {

                        var checkRedeem = Redeem.GetByTransactionCode(param.TransactionCode);
                        if (checkRedeem != null)
                        {
                            result.StatusCode = "21";
                            result.StatusMessage = string.Format("Transaction Code already registered in system");
                        }
                        else
                        {
                            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                            {
                                Guid RedeemId = Guid.NewGuid();
                                int restPoint = param.Point;
                                var collects = Data.Collect.GetAllActiveByMember(param.MemberID).OrderBy(x => x.ExpiryDate).ToList();
                                int timeStamp = 0;
                                foreach (var collect in collects)
                                {
                                    if (restPoint <= 0)
                                    {
                                        //finish
                                        break;
                                    }
                                    else
                                    {
                                        if (restPoint >= collect.TotalPoints)
                                        {
                                            ///1. collect di habisin semua
                                            ///. respoint dikurangin
                                            collect.IsUsed = true;
                                            collect.RedeemID = RedeemId;
                                            collect.Updated(param.MemberName);
                                            restPoint = restPoint - collect.TotalPoints;
                                        }
                                        else
                                        {
                                            //2. di splits
                                            //collect.Remarks = "inactive due to be separated in the process redeem";
                                            collect.IsActive = false;
                                            collect.Updated(param.MemberName);
                                            //collect point di pecah
                                            //collect used
                                            var collectUsed = SplitCollectRedeem(restPoint, RedeemId, collect, true, param.MemberName, 0);
                                            var restCollect = SplitCollectRedeem(collect.TotalPoints - collectUsed.Points, null, collect, false, param.MemberName, 1);
                                            restPoint = 0;
                                        }
                                    }
                                }

                                if (restPoint <= 0)
                                {
                                    //create redeeem
                                    var redeemData = new Redeem();
                                    redeemData.ID = RedeemId;
                                    redeemData.MemberID = param.MemberID;
                                    redeemData.TransactionCode = param.TransactionCode;
                                    redeemData.TransactionDate = param.TransactionDate;
                                    redeemData.PoolID = PoolId;
                                    redeemData.Point = param.Point;
                                    redeemData.IsVoid = false;
                                    var responseEF = redeemData.Inserted(param.MemberName);
                                    if (responseEF != null && responseEF.Success)
                                    {
                                        //Create BalanceLog
                                        var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(param.MemberID);
                                        if (lastBalanceLog != null)
                                        {
                                            int newBalanceLog = lastBalanceLog.Balance - param.Point;
                                            if (newBalanceLog >= 0)
                                            {
                                                var respBalanceEF = BalanceLogLogic.Debit(param.Point, "Pengurangan Point No.Transaksi: " + redeemData.TransactionCode, param.MemberID,
                                                    PoolId, param.MemberName, RedeemId, newBalanceLog, timeStamp);
                                                timeStamp++;
                                                if (responseEF != null && responseEF.Success)
                                                {
                                                    result.StatusCode = "00";
                                                    result.StatusMessage = string.Format("Redeem Successfully");
                                                    result.Value = new RedeemDetailModel(redeemData);
                                                    transScope.Complete();
                                                    transScope.Dispose();

                                                    PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(redeemData.MemberID, param.MemberName);
                                                }
                                                else
                                                {
                                                    transScope.Dispose();
                                                    result.StatusCode = "500";
                                                    result.StatusMessage = string.Format("Error when inserted data BalanceLog into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                                                }
                                            }
                                            else
                                            {
                                                transScope.Dispose();
                                                result.StatusCode = "40";
                                                result.StatusMessage = string.Format("Member Balance's point is insufficient");
                                            }
                                        }
                                        else
                                        {
                                            transScope.Dispose();
                                            result.StatusCode = "41";
                                            result.StatusMessage = string.Format("Member Balance's point is insufficient");
                                        }
                                    }
                                    else
                                    {
                                        transScope.Dispose();
                                        result.StatusCode = "500";
                                        result.StatusMessage = string.Format("Error when inserted data Redeem into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                                    }


                                }
                                else
                                {
                                    transScope.Dispose();
                                    result.StatusCode = "20";
                                    result.StatusMessage = string.Format("Member point is insufficient");
                                }
                            }//end use
                        }
                    }
                    else
                    {
                        result.StatusCode = "12";
                        result.StatusMessage = string.Format("Point must more than zero");
                    }
                }
                else
                {
                    result.StatusCode = "11";
                    result.StatusMessage = string.Format("TransactionCode is required");
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Parameter is null");
            }
            return result;
        }

        private static Data.Collect SplitCollectRedeem(int point, Guid? RedeemId, Data.Collect parent, bool IsUsed, string by, int Timestamp)
        {
            var splitCollect = new Data.Collect();
            splitCollect.ID = Guid.NewGuid();
            splitCollect.MemberID = parent.MemberID;
            splitCollect.Points = point;

            splitCollect.IsUsed = true;
            splitCollect.ParentID = parent.ID;
            splitCollect.TransactionDate = DateTime.Now.AddSeconds(Timestamp);
            splitCollect.ExpiryDate = parent.ExpiryDate;
            splitCollect.PoolID = parent.PoolID;
            splitCollect.Quantity = parent.Quantity;
            splitCollect.QuantityApproved = parent.QuantityApproved;
            splitCollect.TotalPoints = point;
            splitCollect.Remarks = "split";
            splitCollect.IsApproved = true;
            splitCollect.IsActive = true;

            splitCollect.IsUsed = IsUsed;

            if (IsUsed)
            {
                splitCollect.RedeemID = RedeemId;
            }

            var json = JsonConvert.SerializeObject(splitCollect);
            Log.Info("Data Collect = " + json);

            splitCollect.Inserted(by, Timestamp);

            return splitCollect;
        }

        public static ResultPaginationModel<List<RedeemCMSViewModel>> GetRedeemReport(string startDate, string endDate, string orderColumn = "CreatedDate", string orderDirection = "Desc", int page = 1, int pageSize = 10)
        {
            ResultPaginationModel<List<RedeemCMSViewModel>> result = new ResultPaginationModel<List<RedeemCMSViewModel>>();
            orderColumn = string.IsNullOrEmpty(orderColumn) ? "CreatedDate" : orderColumn;
            orderDirection = string.IsNullOrEmpty(orderDirection) ? "Desc" : orderDirection;
            var datas = GetAllRedeemModel(orderColumn, orderDirection);

            DateTime tempStartDate = DateTime.MinValue,
                tempEndDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempStartDate))
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(tempStartDate));
                else
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Invalid value of startDate. Unable to parse into DateTime.";
                    return result;
                }
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempEndDate))
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(tempEndDate));
                else
                {
                    result.StatusCode = "42";
                    result.StatusMessage = "Invalid value of endDate. Unable to parse into DateTime.";
                    return result;
                }
            }

            int totalRow = datas.Count();
            int offset = (page - 1) * pageSize;

            datas = datas.Skip(offset).Take(pageSize);

            try
            {
                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.Value = datas.ToList();
                result.TotalRow = totalRow;
            }
            catch (Exception ex)
            {
                result.StatusCode = "50";
                result.StatusMessage = ex.Message;
            }
            return result;
        }

        private static IQueryable<RedeemCMSViewModel> GetAllRedeemModel(string orderColumn, string orderDirection)
        {
            var data = from x in
                           (from x in LoyaltyPointEngine.Data.Redeem.GetAll()
                            join y in LoyaltyPointEngine.Data.viwRedeemReport.GetAll() on x.ID equals y.ID
                            join member in LoyaltyPointEngine.Data.Member.GetAll() on x.MemberID equals member.ID
                            join z in LoyaltyPointEngine.Data.viwReportReceipt.GetAll() on member.ID equals z.ID
                            select new
                            {
                                ID = x.ID,
                                TransactionCode = x.TransactionCode,
                                TransactionDateDate = x.TransactionDate,
                                Point = y.PointDetail,
                                CreatedDate = x.CreatedDate,
                                MemberName = member.Name,
                                MemberPhone = member.Phone,
                                ElinaCode = z.Code,
                                CatalogueName = y.CatalogueName,
                                Quantity = y.Quantity,
                                Channel = y.Channel,
                                ReceiverName = y.ReceiverName,
                                ReceiverPhone = y.ReceiverPhone,
                                ReceiverAddress = y.ReceiverAddress,
                                IsVoid = x.IsVoid
                            }).OrderBy(string.Format("{0} {1}", orderColumn, orderDirection))
                       select new RedeemCMSViewModel
                       {
                           ID = x.ID,
                           TransactionCode = x.TransactionCode,
                           TransactionDateDate = x.TransactionDateDate,
                           Point = x.Point,
                           CreatedDate = x.CreatedDate,
                           MemberName = x.MemberName,
                           MemberPhone = x.MemberPhone,
                           ElinaCode = x.ElinaCode,
                           CatalogueName = x.CatalogueName,
                           Quantity = x.Quantity,
                           Channel = x.Channel,
                           ReceiverName = x.ReceiverName,
                           ReceiverPhone = x.ReceiverPhone,
                           ReceiverAddress = x.ReceiverAddress,
                           IsVoid = x.IsVoid
                       };

            return data;

        }
    }
}
