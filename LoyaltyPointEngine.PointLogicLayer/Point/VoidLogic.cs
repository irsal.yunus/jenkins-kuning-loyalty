﻿    using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Redeem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LoyaltyPointEngine.PointLogicLayer.Point
{
    public class VoidLogic
    {
        public static ResultModel<RedeemModel> Void(string transactionCode, int MemberId, string MemberName)
        {
            ResultModel<RedeemModel> result = new ResultModel<RedeemModel>();
            if (!string.IsNullOrEmpty(transactionCode))
            {
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };

                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    var redeemData = Redeem.GetByTransactionCode(transactionCode);
                    if (redeemData != null)
                    {

                        if (redeemData.IsVoid)
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = string.Format("{0} has been redeem before this transaction", transactionCode);
                        }
                        else
                        {
                            // Author : Tom
                            // Date : 12 Januari 2017
                            // Function : move variable up for paramater need in function check not match collect and redeem point 
                            int collecttotalpoint = 0;
                            int timeStamp = 0;
                            // end of Function

                            redeemData.IsVoid = true;
                            var responseEFReedeem = redeemData.Updated(MemberName);
                            if (responseEFReedeem != null && responseEFReedeem.Success)
                            {
                                var collectDatas = Data.Collect.GetByRedeemId(redeemData.ID).ToList();
                                if (collectDatas != null)
                                {
                                    //int timeStamp = 0;
                                    foreach (var collect in collectDatas)
                                    {

                                        if (collect.IsActive && collect.IsUsed)
                                        {
                                            var respBalanceEF = BalanceLogLogic.Credit(collect.TotalPoints, "Void No. Transaksi : " + redeemData.TransactionCode, MemberId, collect.PoolID,
                                                MemberName, redeemData.ID, timeStamp);
                                            timeStamp++;
                                            if (respBalanceEF != null && respBalanceEF.Success)
                                            {
                                                collecttotalpoint += collect.TotalPoints;
                                                //continue;
                                            }
                                            else
                                            {

                                                result.StatusCode = "500";
                                                result.StatusMessage = string.Format("Error when inserted data BalanceLog into table: {0}", (responseEFReedeem != null ? responseEFReedeem.ErrorMessage : "null"));
                                                transScope.Dispose();

                                                return result;
                                            }

                                        }


                                        collect.IsUsed = false;
                                       // collect.Remarks = "point is activated due to the process void";
                                        collect.RedeemID = null;
                                        collect.Updated(MemberName);


                                    }
                                }

                                // Author : Tom
                                // Date : 12 Januari 2017
                                // Function : Check if redeem and collect total poin is not match ( there is special condition collect don't have action )

                                try
                                {
                                    if (redeemData.Point != collecttotalpoint)
                                    {

                                        var respBalanceEF = BalanceLogLogic.Credit((redeemData.Point - collecttotalpoint), "Void No. Transaksi : " + redeemData.TransactionCode + ".", MemberId, redeemData.PoolID,
                                            MemberName, redeemData.ID, timeStamp + 1);
                                    }
                                }
                                catch { }

                                // end of function

                                result.StatusCode = "00";
                                result.StatusMessage = string.Format("The process of refund for the transaction code '{0}' successfully", transactionCode);
                                result.Value = new RedeemModel(redeemData);

                                transScope.Complete();


                            }
                            else
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = string.Format("Error when updated data Redeem: {0}", (responseEFReedeem != null ? responseEFReedeem.ErrorMessage : "null"));

                            }



                        }
                    }
                    else
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = "code is not found in the system";

                    }

                    transScope.Dispose();

                    if (result.StatusCode == "00")
                    {
                        //NotificationLogic.Create(redeemData.MemberID, Common.Notification.NotificationType.Type.Void, "Pengembalian Hadiah",
                        //           "Void untuk No Transaksi: " + redeemData.TransactionCode, redeemData.TransactionCode, redeemData.Point, MemberName);

                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(redeemData.MemberID, MemberName);
                    }
                }

            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "transactionCode is required";
            }

            return result;
        }

    }
}
