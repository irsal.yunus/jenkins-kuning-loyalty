﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.Model.Object.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Data.Managed;

namespace LoyaltyPointEngine.PointLogicLayer.Point
{
    public class SchedulerLogic
    {
        public static void ExpiredCollect(DateTime dtNow, string by, string urlReference)
        {
            int total = 0;
            var dataCOllects = Data.Collect.GetAllExpired(dtNow).ToList();
            if (dataCOllects != null)
            {
                int timeStamp = 0;
                foreach (var collect in dataCOllects)
                {
                    var result = CollectLogic.Expired(collect, timeStamp);
                    timeStamp++;
                    if (result.StatusCode == "00")
                    {
                        total++;
                        //TODO: Create Notification
                        string title = "Pengurangan Point";
                        string MessageNotif = string.Format("Total {0} Point Anda sudah kadaluarsa.", collect.TotalPoints);
                        int MemberID = collect.MemberID;
                        var responseEF = NotificationLogic.Create(MemberID, Common.Notification.NotificationType.Type.Expired, title,MessageNotif,collect.ID.ToString(),collect.TotalPoints, by);
                        if (responseEF != null && responseEF.StatusCode == "00")
                        {
                            PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(MemberID,by);                            
                            continue;
                        }
                        else
                        {
                            EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Notification,
                               LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add,
                               "Error when inserting Notification: " + result.StatusMessage, new NotificationModel(MessageNotif,
                                   Common.Notification.NotificationType.Type.Expired.ToString(), MemberID, DateTime.Now), urlReference, by);

                        }


                    }
                    else
                    {
                        EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Collect,
                                 LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Edit, "Error when Expired Collect via scheduler: " + result.StatusMessage, new CollectModel(collect), urlReference, by);

                    }
                }
            }

            EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Information, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Collect,
                            LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Global, string.Format("Scheduler Collect Expired is running. Total collect expired: {0} ", total),
                            null, "scheduler collect expired", "scheduler");



        }


        public static void ExpiredNotification(string by, string urlReference)
        {
            int total = 0;

            string TresholdNotifCollectExpired = Settings.GetValueByKey(SiteSetting.TresholdNotifCollectExpired);
            int _TresholdNotifCollectExpired = 7;
            int.TryParse(TresholdNotifCollectExpired, out _TresholdNotifCollectExpired);
            if (_TresholdNotifCollectExpired <= 0)
            {
                _TresholdNotifCollectExpired = 7;
            }

            DateTime startdate = DateTime.Now;
            DateTime enddate = startdate.AddDays(_TresholdNotifCollectExpired);

            var dataCollects = Data.Collect.GetAllActiveByStartDateAndEndDate(startdate, enddate).ToList();
            if (dataCollects != null && dataCollects.Count > 0)
            {
                var MemberIDs = dataCollects.Select(x => x.MemberID).Distinct().ToList();

                foreach (var memberID in MemberIDs)
                {
                    var totalPoint = dataCollects.Sum(x => x.TotalPoints);

                    //create notification
                    
                    string title = string.Format("Point anda akan Expired pada {0} ",  enddate.ToString("dd MMMMM yyyy"));
                    string message = "Segera tukarkan atau tambah lagi point Anda!";
                    NotificationLogic.Create(memberID, Common.Notification.NotificationType.Type.Information,title, message,"",totalPoint ,by);
                }
            }

            EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Information, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Collect,
                            LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Global, string.Format("Scheduler Notif Point Expired is running."),
                            null, "Scheduler Notif Point Expired", "scheduler");



        }


        public static void BillingMonthly(string by, string urlReference)
        {
            var temp = DateTime.Now.AddMonths(-1);

           
            temp = DateTime.Now;
            
            
            string MailBillingTemplate = Settings.GetValueByKey(SiteSetting.MailBillingTemplate);

            string htmlTemplate = EmailHelper.GetEmailTemplate(MailBillingTemplate);
            if (!string.IsNullOrEmpty(htmlTemplate))
            {

                var dataSettingSmtp = Settings.GetAllByCode("smtp").ToList();

                string SMTPHost = dataSettingSmtp.GetValue(SiteSetting.SMTPHost);
                
                  string SMTPUsername = dataSettingSmtp.GetValue(SiteSetting.SMTPUsername);
                  string SMTPPassword = dataSettingSmtp.GetValue(SiteSetting.SMTPPassword);
                  string SMTPPort = dataSettingSmtp.GetValue(SiteSetting.SMTPPort);
                  string SMTPSSL = dataSettingSmtp.GetValue(SiteSetting.SMTPSSL);
                  string SiteEmail = dataSettingSmtp.GetValue(SiteSetting.SiteEmail);
                  string SMTP_NameDisplay = dataSettingSmtp.GetValue(SiteSetting.SMTP_NameDisplay);

                int _SMTPPort = 0;
                int.TryParse(SMTPPort,out _SMTPPort);

                bool _SMTPSSL =false;
                bool.TryParse(SMTPSSL, out _SMTPSSL);


                DateTime startdate = new DateTime(temp.Year, temp.Month, 1);
                DateTime enddate = new DateTime(startdate.Year, startdate.Month, DateTime.DaysInMonth(startdate.Year, startdate.Month));

                var balances = Data.BalanceLog.GetByStartDateAndEndDate(startdate, enddate).ToList();
                if (balances != null && balances.Count > 0)
                {
                    var MemberIDs = balances.Select(x => x.MemberID).Distinct().ToList();

                    foreach (var memberID in MemberIDs)
                    {
                        var listmemberBalance = balances.Where(x => x.MemberID == memberID).OrderBy(x => x.CreatedDate).ToList();
                        var member = Data.Member.GetById(memberID);

                        var memberHtmlTemplate = htmlTemplate;
                        memberHtmlTemplate = memberHtmlTemplate.Replace("[member]", "Dear, " + member.Name);
                        memberHtmlTemplate = memberHtmlTemplate.Replace("[table]", GenerateTableBilling(listmemberBalance));
                        memberHtmlTemplate = memberHtmlTemplate.Replace("[P1]", GenerateHeader(member, startdate,enddate));
                        memberHtmlTemplate = memberHtmlTemplate.Replace("[Footer]", GenerateFooter(member, startdate, enddate,listmemberBalance));


                        string emailTo = member.Email;

                        //todo:comment
                        emailTo = "azadi.dicky@gmail.com";
                        string subject = string.Format("Loyal Point - Billing statement {0}",(startdate.ToString("dd/MM") +" - "+ enddate.ToString("dd/MM/yyyy")));
                        if (EmailHelper.IsValidEmail(emailTo))
                        {
                            try
                            {
                                EmailHelper.SendEmail(subject, memberHtmlTemplate, emailTo, SiteEmail, SMTP_NameDisplay, SMTPHost,
                                    _SMTPSSL, SMTPUsername, SMTPPassword, _SMTPPort
                                    );
                            }
                            catch(Exception ex)
                            {
                                EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Collect,
                                    LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Global, string.Format("Error when send email in process scheduler email billing: {0}", (ex.InnerException != null? ex.InnerException.Message:ex.Message)),
                         memberHtmlTemplate, "Scheduler email billing is Error", "scheduler");
                            }
                        }
                    }
                }


                EventLogLogic.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Information, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Collect,
                           LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Global, string.Format("Scheduler email billing is running."),
                           null, "Scheduler email billing is running", "scheduler");

            }


        }


        private static string GenerateTableBilling(List<Data.BalanceLog> datas)
        {
            string result = "";
            foreach (var item in datas.OrderBy(x=> x.CreatedDate))
            {
                result = result + "<tr>" +
                        "<td>" + item.CreatedDate.ToString("dd/MM/yyyy") + "</td>" +
                        "<td>" + item.Description + "</td>" +
                        "<td>" + item.Points + "</td>" +
                        "<td>" + item.Type + "</td>" +
                        "<td>" + item.Balance + "</td>" +
                "</tr>";
            }
            return result;
        }

        private static string GenerateHeader(Data.Member member, DateTime startdate, DateTime enddate)
        {
            string result = "";

            result = "<table width=\"528\" valign=\"top\" style=\"font-family: Arial,Helvetica,sans-serif; display: block;\" align=\"left\">" +
                                           " <tr>"+
                                               " <td>Name</td>"+
                                               " <td>:</td>"+
                                               " <td>"+member.Name+"</td>"+
                                           " </tr>"+
                                            " <tr>"+
                                             "   <td>Period</td>"+
                                              "  <td>:</td>"+
                                              "  <td>" + startdate.ToString("dd/MM/yyyy") + " - " + enddate.ToString("dd/MM/yyyy") + "</td>" +
                                           " </tr>"+
                                        "</table>";
            return result;
        }

        private static string GenerateFooter(Data.Member member, DateTime startdate, DateTime enddate, List<Data.BalanceLog> datas)
        {
            int startBalance = datas.OrderBy(x => x.CreatedDate).FirstOrDefault().Balance;
            int totalCredit = datas.Where(x => x.Type.ToLower() == "c").Sum(x=> x.Points);
            int totalDebit = datas.Where(x => x.Type.ToLower() == "d").Sum(x => x.Points);

            int endingBalance = datas.OrderByDescending(x => x.CreatedDate).FirstOrDefault().Balance;



            DateTime _startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime _enddate = new DateTime(_startdate.Year, _startdate.Month, DateTime.DaysInMonth(_startdate.Year, _startdate.Month));



            var dataCollects = Data.Collect.GetAllActiveByStartDateAndEndDateByMember(_startdate, _enddate,member.ID).ToList();

            int totalExpired = 0;
            if (dataCollects != null)
            {
                totalExpired = dataCollects.Sum(x => x.TotalPoints);
            }


            string result = "";

            result = "<table width=\"528\" valign=\"top\" style=\"font-family: Arial,Helvetica,sans-serif; display: block;\" align=\"left\">" +
                                           " <tr>" +
                                               " <td>Starting Balance</td>" +
                                               " <td>:</td>" +
                                               " <td>" + startBalance + "</td>" +
                                           " </tr>" +

                                            " <tr>" +
                                             "   <td>Total Credits</td>" +
                                              "  <td>:</td>" +
                                              "  <td>" + totalCredit + "</td>" +
                                           " </tr>" +

                                              " <tr>" +
                                             "   <td>Total Debits</td>" +
                                              "  <td>:</td>" +
                                              "  <td>" + totalDebit + "</td>" +
                                           " </tr>" +


                                              " <tr>" +
                                             "   <td>Ending Balance</td>" +
                                              "  <td>:</td>" +
                                              "  <td>" + endingBalance + "</td>" +
                                           " </tr>" +

                                                " <tr>" +
                                             "   <td>Point will expired in " + _startdate.ToString("dd/MM/yyyy") + " - " + _enddate.ToString("dd/MM/yyyy") + "</td>" +
                                              "  <td>:</td>" +
                                              "  <td>" + totalExpired + "</td>" +
                                           " </tr>" +
                                        "</table>";
            return result;
        }

    }
}
