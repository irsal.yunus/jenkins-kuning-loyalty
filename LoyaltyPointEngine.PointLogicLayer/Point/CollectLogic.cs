﻿using log4net;
using LoyaltyPointEngine.Common.Point;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.PointLogicLayer.Action;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace LoyaltyPointEngine.PointLogicLayer.Point
{
    public class CollectLogic : MainLogic
    {
        private static ILog Log = LogManager.GetLogger(typeof(CollectLogic));

        public CollectLogic()
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(CollectLogic));
            }
        }

        public static ResultModel<CollectModel> Get(string param)
        {
            ResultModel<CollectModel> result = new ResultModel<CollectModel>();
            Guid ID = Guid.Empty;
            if (Guid.TryParse(param, out ID))
            {
                var data = Data.Collect.GetById(ID);
                result.Value = new CollectModel(data);
                result.StatusCode = "00";
                result.StatusMessage = "Get Detail Collect";
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Format ID is wrong";
            }


            return result;
        }

        public static ResultModel<List<object>> PublicGetByReceiptId(Guid receiptId)
        {
            ResultModel<List<object>> result = new ResultModel<List<object>>();
            var collects = Data.Collect.GetAllByReceiptID(receiptId);
            if (collects == null)
            {
                result.StatusCode = "40";
                result.StatusMessage = string.Format("No collects found based on receiptid = '{0}' or no receipt found based on id = '{0}'", receiptId);
            }
            else
            {
                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.Value = collects.Select(x => new
                {
                    x.Action.Product.Code,
                    x.QuantityApproved,
                    x.ItemPrice
                }).ToList<object>();
            }
            return result;
        }

        public static ResultModel<bool> Expired(Data.Collect collect, int timeStamp)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            try
            {
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    if (collect.TotalPoints > 0)
                    {
                        collect.IsActive = false;
                        //  collect.Remarks = "disabled because the collect is already expired";
                        collect.Updated("scheduler");

                        var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(collect.MemberID);
                        if (lastBalanceLog != null)
                        {
                            int newBalanceLog = lastBalanceLog.Balance - collect.TotalPoints;

                            var responseEF = BalanceLogLogic.Debit(collect.TotalPoints, "Point Kadaluarsa",
                                collect.MemberID, collect.PoolID, "scheduler", collect.ID, newBalanceLog, timeStamp);

                            if (responseEF != null && responseEF.Success)
                            {
                                result.StatusCode = "00";
                                result.StatusMessage = string.Format("Expired Successfully");
                                result.Value = true;
                                transScope.Complete();

                            }
                            else
                            {

                                result.StatusCode = "500";
                                result.StatusMessage = string.Format("Error when inserted data BalanceLog into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                            }

                        }
                        else
                        {

                            result.StatusCode = "41";
                            result.StatusMessage = string.Format("Your Balance's point is insufficient");
                        }


                    }
                    transScope.Dispose();
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Error current process to expiration: {0}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            }


            return result;

        }

        public static ResultModel<Data.Collect> InsertPointByActionIdAndMemberId(string actionCode, int memberId, string url = "", bool saveWithBalanceLog = true,
            bool checkCap = true, int timeStamp = 0, int lastPoint = 0, DateTime? createdDate = null)
        {
            var result = new ResultModel<Data.Collect>();
            var member = DataMember.Member.GetById(memberId);
            if (member == null || (member.IsDeleted || !member.IsActive))
            {
                result.StatusCode = "41";
                result.StatusMessage = String.Format("Member with id {0} not found in the system or not active.", actionCode);
                return result;
            }
            if (checkCap)
                result = InsertPointByActionCode(actionCode, member, url, saveWithBalanceLog, timeStamp, lastPoint, createdDate);
            else
                result = InsertPointByActionCodeNoCap(actionCode, member, url, saveWithBalanceLog, timeStamp, lastPoint, createdDate);
            return result;
        }

        public static ResultModel<Data.Collect> InsertPointUpdateProfile(string actionCode, int memberId, string url = "", bool saveWithBalanceLog = true,
            bool checkCap = true, int timeStamp = 0, int lastPoint = 0, DateTime? createdDate = null)
        {
            var result = new ResultModel<Data.Collect>();
            var member = DataMember.Member.GetById(memberId);
            if (member == null || (member.IsDeleted || !member.IsActive))
            {
                result.StatusCode = "41";
                result.StatusMessage = String.Format("Member with id {0} not found in the system or not active.", actionCode);
                return result;
            }

            result = InsertPointByActionCode(actionCode, member, url, saveWithBalanceLog, timeStamp, lastPoint, createdDate);

            if (result.StatusCode == "00")
            {
                LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(memberId, member.FirstName);
            }

            return result;
        }

        public static ResultModel<Data.Collect> InsertPointByCampaignAndMemberId(Campaign campaign, string messageActionCode, int memberId, string url = "", bool saveWithBalanceLog = true)
        {
            var result = new ResultModel<Data.Collect>();
            var member = DataMember.Member.GetById(memberId);
            if (member == null || (member.IsDeleted || !member.IsActive))
            {
                result.StatusCode = "41";
                result.StatusMessage = String.Format("Member with id {0} not found in the system or not active.", memberId);
                return result;
            }

            result = InsertPointCampaign(campaign, member, messageActionCode, url, saveWithBalanceLog);
            return result;
        }

        public static ResultModel<Data.Collect> InsertPointByActionCode(ActionType.Code ActionCode, DataMember.Member DataMember, string url, bool saveWithBalanceLog = true)
        {
            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();

            try
            {
                var dataAction = Data.Action.GetByCode(ActionCode.ToString());
                if (dataAction == null || (!dataAction.IsActive && dataAction.IsDeleted))
                {
                    result.StatusCode = "40";
                    result.StatusMessage = String.Format("Action with code {0} not found in the system or not active.", ActionCode.ToString());
                    return result;
                }

                var paramModel = PopulateModelCollectForAddPoint(dataAction, DataMember);
                Collect.CollectLogic newLogic = new Collect.CollectLogic();
                var responseInsert = newLogic.InsertCollect(paramModel, dataAction.PoolID, string.Format("{0} {1}", DataMember.FirstName, DataMember.LastName), null, url, 0, 1, dataAction, saveWithBalanceLog);
                if (responseInsert.StatusCode == "00")
                {
                    result.Value = responseInsert.Value;
                }

                result.StatusCode = responseInsert.StatusCode;
                result.StatusMessage = responseInsert.StatusMessage;
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            if (result.StatusCode != "00")
            {
                try
                {
                    LoyaltyPointEngine.Data.EventLog.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Member, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add, result.StatusMessage, DataMember, url, DataMember.FirstName);
                }
                catch
                {

                }
            }
            return result;
        }

        public static ResultModel<Data.Collect> InsertPointByActionCode(string ActionCode, DataMember.Member DataMember, string url, bool saveWithBalanceLog = true,
            int timeStamp = 0, int lastPoint = 0, DateTime? createdDate = null)
        {
            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();
            try
            {
                var dataAction = Data.Action.GetByCode(ActionCode);
                if (dataAction == null || (!dataAction.IsActive && dataAction.IsDeleted))
                {
                    result.StatusCode = "40";
                    result.StatusMessage = String.Format("Action with code {0} not found in the system or not active.", ActionCode.ToString());
                    return result;
                }
                var paramModel = PopulateModelCollectForAddPoint(dataAction, DataMember);
                Collect.CollectLogic newLogic = new Collect.CollectLogic();
                var responseInsert = newLogic.InsertCollect(paramModel, dataAction.PoolID, string.Format("{0} {1}", DataMember.FirstName, DataMember.LastName), null, url,
                    timeStamp, 1, dataAction, saveWithBalanceLog, false, false, lastPoint, createdDate);
                if (responseInsert.StatusCode == "00")
                {
                    result.Value = responseInsert.Value;
                }
                result.StatusCode = responseInsert.StatusCode;
                result.StatusMessage = responseInsert.StatusMessage;
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            if (result.StatusCode != "00")
            {
                try
                {
                    LoyaltyPointEngine.Data.EventLog.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Member, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add, result.StatusMessage, DataMember, url, DataMember.FirstName);
                }
                catch
                {

                }
            }
            return result;
        }

        public static ResultModel<Data.Collect> InsertPointByActionCodeNoCap(string ActionCode, DataMember.Member DataMember, string url, bool saveWithBalanceLog = true,
            int timeStamp = 0, int lastPoint = 0, DateTime? createdDate = null)
        {
            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();
            string by = string.Format("{0} {1}", DataMember.FirstName, DataMember.LastName).Trim(),
                code = Number.Generated(Number.Type.ADJ, by),
                message = "Penambahan Point {0}";
            try
            {
                var dataAction = Data.Action.GetByCode(ActionCode);
                if (dataAction != null)
                {
                    bool isSuccess = false;
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        Data.Collect collect = new Data.Collect();
                        collect.ID = Guid.NewGuid();
                        collect.MemberID = DataMember.ID;
                        collect.Quantity = 1;
                        collect.QuantityApproved = 1;
                        collect.Points = dataAction.Points;
                        collect.TotalPoints = dataAction.Points;
                        collect.Remarks = string.Format(message, dataAction.Name);
                        collect.TransactionDate = DateTime.Now;
                        collect.ExpiryDate = collect.TransactionDate.AddYears(1);
                        collect.PoolID = dataAction.PoolID;
                        collect.IsApproved = true;
                        collect.IsActive = true;
                        collect.IsUsed = false;
                        var json = JsonConvert.SerializeObject(collect);
                        Log.Info("Data Collect = " + json);
                        var res = collect.Insert(by, "", null, 0);
                        if (res.Success)
                        {
                            LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Credit(dataAction.Points, collect.Remarks, DataMember.ID, collect.PoolID, by,
                                collect.ID, 0, lastPoint, createdDate);
                            transScope.Complete();
                            isSuccess = true;
                            result.StatusCode = "00";
                            result.StatusMessage = "";
                            result.Value = collect;
                        }
                        else
                        {
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format("{0}\n{1}", res.ErrorMessage, res.ErrorEntity);
                        }
                        transScope.Dispose();
                    }
                    if (isSuccess)
                    {
                        LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(DataMember.ID, by);
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            if (result.StatusCode != "00")
            {
                EventLog.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Member, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add, result.StatusMessage, DataMember, url, DataMember.FirstName);
            }
            return result;
        }

        public static ResultModel<Data.Collect> InsertPointCampaign(Campaign campaign, DataMember.Member DataMember, string MessageActionCode, string url, bool saveWithBalanceLog = true)
        {
            ResultModel<Data.Collect> result = new ResultModel<Data.Collect>();
            string by = string.Format("{0} {1}", DataMember.FirstName, DataMember.LastName).Trim(),
                code = Number.Generated(Number.Type.ADJ, by),
                message = MessageActionCode + " {0}";

            int promoPoints = 0;

            if (campaign.Type == "FIXEDNUMBER")
            {
                promoPoints = campaign.Value;
            }
            else if (campaign.Type == "PERCENTAGE")
            {
                var dataReceipt = Receipt.GetByMemberId(DataMember.ID)
                .Where(a => a.Status == (int)Receipt.ReceiptStatus.Approve && a.CampaignID == campaign.ID)
                .OrderByDescending(a => a.TransactionDate)
                .Take(1).FirstOrDefault();

                var Points = from a in Data.Collect.GetAll()
                             join b in CampaignActionDetail.GetAll() on
                                new { X = a.CampaignID, Y = a.ActionID } equals
                                new { X = (int?)b.CampaignID, Y = (int?)b.ActionID }
                             join c in Receipt.GetByMemberId(DataMember.ID) on a.ReceiptID equals c.ID
                             where a.MemberID == DataMember.ID && a.IsApproved == true && b.IsActive == true && c.ID == dataReceipt.ID
                             group a by a.ID into g
                             select new { Points = g.Sum(x => x.Points) };

                var totalPoints = Points.FirstOrDefault().Points;
                promoPoints = totalPoints * Convert.ToInt32((decimal)campaign.Value / 100);
            }

            try
            {
                bool isSuccess = false;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    Data.Collect collect = new Data.Collect();
                    collect.ID = Guid.NewGuid();
                    collect.MemberID = DataMember.ID;
                    collect.Quantity = 1;
                    collect.QuantityApproved = 1;
                    collect.Points = promoPoints;
                    collect.TotalPoints = promoPoints;
                    collect.Remarks = string.Format(message, campaign.Name);
                    collect.TransactionDate = DateTime.Now;
                    collect.ExpiryDate = collect.TransactionDate.AddYears(1);
                    collect.PoolID = 1;
                    collect.IsApproved = true;
                    collect.IsActive = true;
                    collect.IsUsed = false;
                    var json = JsonConvert.SerializeObject(collect);
                    Log.Info("Data Collect = " + json);
                    var res = collect.Insert(by, "", null, 0);
                    if (res.Success)
                    {
                        LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Credit(promoPoints, collect.Remarks, DataMember.ID, collect.PoolID, by, collect.ID, 0);
                        transScope.Complete();
                        isSuccess = true;
                        result.StatusCode = "00";
                        result.StatusMessage = "";
                        result.Value = collect;
                    }
                    else
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("{0}\n{1}", res.ErrorMessage, res.ErrorEntity);
                    }
                    transScope.Dispose();
                }
                if (isSuccess)
                {
                    LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(DataMember.ID, by);
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            if (result.StatusCode != "00")
            {
                EventLog.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Member, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add, result.StatusMessage, DataMember, url, DataMember.FirstName);
            }
            return result;
        }

        private static CollectCMSModel PopulateModelCollectForAddPoint(Data.Action dataAction, DataMember.Member DataMember)
        {
            var actionPoint = ActionLogic.GetPointByAction(dataAction);
            CollectCMSModel model = new CollectCMSModel();
            model.ActionID = dataAction.ID;
            model.MemberID = DataMember.ID;
            model.Quantity = 1;
            model.QuantityApproved = 1;
            model.Points = actionPoint.Points;
            model.TotalPoints = actionPoint.Points;
            model.Remarks = "add point by action code";
            model.TransactionDate = DateTime.Now.ToString("dd/MM/yyyy");
            model.TransactionTime = DateTime.Now.ToString("HH:mm:ss");
            var json = JsonConvert.SerializeObject(model);
            Log.Info("Data Collect = " + json);
            return model;
        }
    }
}
