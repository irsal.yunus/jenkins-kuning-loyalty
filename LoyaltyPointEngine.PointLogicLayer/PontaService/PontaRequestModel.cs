﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace LoyaltyPointEngine.PointLogicLayer.PontaService
{
    public class BaseRq
    {
        [XmlAttribute("language")]
        public string LanguageAttribute { get; set; }
        [XmlAttribute("currency")]
        public string CurrencyAttribute { get; set; }

        public CustomerIdentifier CustomerIdentifier { get; set; }

        public BaseRq()
        {
            this.LanguageAttribute = "en_US";
            this.CurrencyAttribute = "IDR";

            CustomerIdentifier = new CustomerIdentifier();
        }

        public string ToXmlString()
        {
            XmlSerializerNamespaces xmlNameSpaces = new XmlSerializerNamespaces();
            xmlNameSpaces.Add("", "");
            XmlSerializer xmlSerializer = new XmlSerializer(this.GetType());
            using (StringWriter textWriter = new StringWriter())
            using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true }))
            {
                xmlSerializer.Serialize(xmlWriter, this, xmlNameSpaces);
                return textWriter.ToString();
            }
        }
    }

    [XmlRoot("OperatorLoginRq")]
    public class OperatorLoginRq : BaseRq
    {
        public LoginRqHeader RqHeader { get; set; }
        public Login Login { get; set; }

        public OperatorLoginRq()
            : base()
        {
            RqHeader = new LoginRqHeader();

            Login = new Login();
            Login.Action = "LGN";
            Login.Mode = "O";
            Login.Password = PontaSettings.Settings.Password;

            CustomerIdentifier = new CustomerIdentifier();
        }
    }

    [XmlRoot("OperatorLogoutRq")]
    public class OperatorLogoutRq : BaseRq
    {
        public LoginRqHeader RqHeader { get; set; }
        public Login Login { get; set; }

        public OperatorLogoutRq()
            : base()
        {
            RqHeader = new LoginRqHeader();

            Login = new Login();
            Login.Action = "LGO";
            Login.Mode = "O";

            CustomerIdentifier = new CustomerIdentifier();
        }
    }

    [XmlRoot("EnquiryWithNameRq")]
    public class EnquiryWithNameRq : BaseRq
    {
        public RqHeader RqHeader { get; set; }
        public EnquiryWithNameRq()
            : base()
        {
            RqHeader = new RqHeader();
        }
    }

    [XmlRoot("TransactionRq")]
    public class TransactionRq : BaseRq
    {
        public EnquiryRqHeader RqHeader { get; set; }
        public Transaction Transaction { get; set; }
        public TransactionRq()
            : base()
        {
            RqHeader = new EnquiryRqHeader();
            CustomerIdentifier = new CustomerIdentifier();
            Transaction = new Transaction();
        }
    }

    [XmlRoot("RqHeader")]
    public class RqHeader
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string TimeZone { get; set; }
        public string AccessToken { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public PontaStoreTypes StoreType { get; set; }
        public string TerminalCode { get; set; }
        public string ChannelId { get; set; }

        public RqHeader()
        {
            DateTime now = DateTime.Now;
            Date = now.ToString("yyyyMMdd");
            Time = now.ToString("hhmmss");
            TimeZone = "GMT+07:00";
            CompanyCode = PontaSettings.Settings.CompanyCode;
            CompanyName = PontaSettings.Settings.CompanyName;
            BranchCode = PontaSettings.Settings.BranchCode;
            BranchName = PontaSettings.Settings.BranchName;
            StoreCode = PontaSettings.Settings.StoreCode;
            StoreName = PontaSettings.Settings.StoreName;
            StoreType = PontaSettings.Settings.StoreType;
            TerminalCode = PontaSettings.Settings.TerminalCode;
            ChannelId = "POS";
        }
    }

    [XmlRoot("RqHeader")]
    public class LoginRqHeader : RqHeader
    {
        public string StanNo { get; set; }
        public string LastTxnRefNo { get; set; }
        public string OTP { get; set; }
        public LoginRqHeader()
            : base()
        {

        }
    }

    [XmlRoot("RqHeader")]
    public class EnquiryRqHeader : RqHeader
    {
        public string LastTxnRefNo { get; set; }
        public string InvoiceNumber { get; set; }
        public EnquiryRqHeader()
            : base()
        {

        }
    }

    [XmlRoot("Login")]
    public class Login
    {
        public string Action { get; set; }
        public string Mode { get; set; }
        public string Password { get; set; }
    }

    [XmlRoot("CustomerIdentifier")]
    public class CustomerIdentifier
    {
        public string IDType { get; set; }
        public string ID { get; set; }

        public CustomerIdentifier()
        {
            ID = PontaSettings.Settings.ID;
            IDType = "OPERATOR";
        }
    }

    public class Transaction
    {
        public string TxnType { get; set; }
        public long GrossTransactionAmount { get; set; }
        public long Points { get; set; }
        public int TotalUnitsOfPurchase { get; set; }
        public int TotalNoOfItemsPurchase { get; set; }
        public int TotalGramsOfPurchase { get; set; }

        public Transaction()
        {
            TxnType = "POI";
        }
    }
}
