﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.PointLogicLayer.PontaService
{
    public class PontaSettings : ConfigurationSection
    {
        private static PontaSettings settings = ConfigurationManager.GetSection("PontaSettings") as PontaSettings;

        public static PontaSettings Settings
        {
            get
            {
                return settings;
            }
        }

        [ConfigurationProperty("companyCode", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string CompanyCode
        {
            get { return (string)this["companyCode"]; }
            set { this["companyCode"] = value; }
        }

        [ConfigurationProperty("companyName", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string CompanyName
        {
            get { return (string)this["companyName"]; }
            set { this["companyName"] = value; }
        }

        [ConfigurationProperty("branchCode", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string BranchCode
        {
            get { return (string)this["branchCode"]; }
            set { this["branchCode"] = value; }
        }

        [ConfigurationProperty("branchName", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string BranchName
        {
            get { return (string)this["branchName"]; }
            set { this["branchName"] = value; }
        }

        [ConfigurationProperty("storeCode", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string StoreCode
        {
            get { return (string)this["storeCode"]; }
            set { this["storeCode"] = value; }
        }

        [ConfigurationProperty("storeName", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string StoreName
        {
            get { return (string)this["storeName"]; }
            set { this["storeName"] = value; }
        }

        [ConfigurationProperty("storeType", DefaultValue = PontaStoreTypes.F, IsRequired = true)]
        public PontaStoreTypes StoreType
        {
            get { return (PontaStoreTypes)this["storeType"]; }
            set { this["storeType"] = value; }
        }

        [ConfigurationProperty("terminalCode", IsRequired = true)]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;’\"|\\")]
        public string TerminalCode
        {
            get { return (string)this["terminalCode"]; }
            set { this["terminalCode"] = value; }
        }

        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get { return (string)this["password"]; }
            set { this["password"] = value; }
        }

        [ConfigurationProperty("id", IsRequired = true)]
        public string ID
        {
            get { return (string)this["id"]; }
            set { this["id"] = value; }
        }

        [ConfigurationProperty("Stores", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(PontaStoreCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public PontaStoreCollection Stores
        {
            get { return (PontaStoreCollection)this["Stores"]; }
        }

        [ConfigurationProperty("PontaResponseCodeMessages", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(PontaResponseCodeMessageCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public PontaResponseCodeMessageCollection PontaResponseCodeMessages
        {
            get { return (PontaResponseCodeMessageCollection)this["PontaResponseCodeMessages"]; }
        }
    }

    public class PontaStoreCollection : ConfigurationElementCollection
    {
        public PontaStoreCollection()
        {
        }

        public PontaStore this[int index]
        {
            get { return (PontaStore)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public new PontaStore this[string key]
        {
            get { return (PontaStore)BaseGet(key); }
            set
            {
                var item = BaseGet(key);
                if (item != null)
                {
                    BaseRemoveAt(BaseIndexOf(item));
                }
                BaseAdd(value, true);
            }
        }

        public object[] AllKeys
        {
            get
            {
                return BaseGetAllKeys();
            }
        }

        public void Add(PontaStore serviceConfig)
        {
            BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new PontaStore();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PontaStore)element).Code;
        }

        public void Remove(PontaStore serviceConfig)
        {
            BaseRemove(serviceConfig.Code);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }
    }

    public class PontaStore : ConfigurationElement
    {
        public PontaStore() { }

        public PontaStore(string code, string name)
        {
            Code = code;
            Name = name;
        }

        [ConfigurationProperty("code", IsKey = true, IsRequired = true)]
        public string Code
        {
            get { return (string)this["code"]; }
            set { this["code"] = value; }
        }

        [ConfigurationProperty("name", IsRequired = true, IsKey = false)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }

    public class PontaResponseCodeMessageCollection : ConfigurationElementCollection
    {
        public PontaResponseCodeMessageCollection()
        {
        }

        public PontaResponseCodeMessage this[int index]
        {
            get { return (PontaResponseCodeMessage)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public new PontaResponseCodeMessage this[string key]
        {
            get { return (PontaResponseCodeMessage)BaseGet(key); }
            set
            {
                var item = BaseGet(key);
                if (item != null)
                {
                    BaseRemoveAt(BaseIndexOf(item));
                }
                BaseAdd(value, true);
            }
        }

        public object[] AllKeys
        {
            get
            {
                return BaseGetAllKeys();
            }
        }

        public void Add(PontaResponseCodeMessage serviceConfig)
        {
            BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new PontaResponseCodeMessage();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PontaResponseCodeMessage)element).Code;
        }

        public void Remove(PontaResponseCodeMessage serviceConfig)
        {
            BaseRemove(serviceConfig.Code);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }
    }


    public class PontaResponseCodeMessage : ConfigurationElement
    {
        public PontaResponseCodeMessage() { }

        public PontaResponseCodeMessage(string code, string message)
        {
            Code = code;
            Message = message;
        }

        [ConfigurationProperty("code", IsKey = true, IsRequired = true, Options = ConfigurationPropertyOptions.IsKey)]
        public string Code
        {
            get { return (string)this["code"]; }
            set { this["code"] = value; }
        }

        [ConfigurationProperty("message", IsRequired = true, IsKey = false)]
        public string Message
        {
            get { return (string)this["message"]; }
            set { this["message"] = value; }
        }
    }
}
