﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.PointLogicLayer.PontaService
{
    public enum PontaStoreTypes
    {
        /// <summary>
        /// R stands for Reguler
        /// </summary>
        R,
        /// <summary>
        /// F stands for Franchise
        /// </summary>
        F
    }
}
