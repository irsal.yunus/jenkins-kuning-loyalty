﻿using LoyaltyPointEngine.PointLogicLayer.GlobalPontaService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LoyaltyPointEngine.PointLogicLayer.PontaService
{
    public class PontaService
    {
        public static XmlDocument Login(OperatorLoginRq login = null)
        {
            OperatorLoginRq in0 = login == null ? new OperatorLoginRq() : login;

            return ProcessRequest(in0.ToXmlString());
        }

        public static XmlDocument Logout(OperatorLogoutRq logout = null)
        {
            OperatorLogoutRq in0 = logout == null ? new OperatorLogoutRq() : logout;

            return ProcessRequest(in0.ToXmlString());
        }

        public static XmlDocument Logout(string accessToken)
        {
            OperatorLogoutRq in0 = new OperatorLogoutRq();
            in0.RqHeader.AccessToken = accessToken;

            return ProcessRequest(in0.ToXmlString());
        }

        public static XmlDocument Enquiry(EnquiryWithNameRq enquiry)
        {
            EnquiryWithNameRq in0 = enquiry == null ? new EnquiryWithNameRq() : enquiry;

            return ProcessRequest(in0.ToXmlString());
        }

        public static XmlDocument Enquiry(string accessToken, string id, string idType)
        {
            EnquiryWithNameRq in0 = new EnquiryWithNameRq();
            in0.RqHeader.AccessToken = accessToken;
            in0.CustomerIdentifier.ID = id;
            in0.CustomerIdentifier.IDType = idType;

            return ProcessRequest(in0.ToXmlString());
        }

        public static XmlDocument AwardOnline(TransactionRq enquiry)
        {
            TransactionRq in0 = enquiry == null ? new TransactionRq() : enquiry;

            return ProcessRequest(in0.ToXmlString());
        }

        public static XmlDocument AwardOnline(string accessToken, string id, string idType, string lastTxnRefNo, string invoiceNumber, Transaction transaction)
        {
            TransactionRq in0 = new TransactionRq();
            in0.RqHeader.AccessToken = accessToken;
            in0.RqHeader.LastTxnRefNo = lastTxnRefNo;
            in0.RqHeader.InvoiceNumber = invoiceNumber;
            in0.Transaction = transaction;
            in0.CustomerIdentifier.ID = id;
            in0.CustomerIdentifier.IDType = idType;

            return ProcessRequest(in0.ToXmlString());
        }

        private static XmlDocument ProcessRequest(string in0)
        {
            GlobalPontaServicePortTypeClient client = new GlobalPontaServicePortTypeClient();
            client.Open();
            string result = client.processRequest(in0, "", "", "", "");
            client.Close();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }
    }
}
