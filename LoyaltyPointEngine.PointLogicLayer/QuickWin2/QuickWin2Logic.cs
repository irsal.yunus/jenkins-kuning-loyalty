﻿using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Parameter.QuickWin2;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Xml;

namespace LoyaltyPointEngine.PointLogicLayer
{
    public class QuickWin2Logic
    {
        public static ResultModel<bool> SaveComment(ParamCreateQuickWin2Participation model, int memberId)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            if(model == null)
            {
                result.StatusCode = "01";
                result.StatusMessage = "parameter not sent.";
                result.Value = false;
                return result;
            }

            if(model.MemberID == null || model.ReceiptID == null || string.IsNullOrEmpty(model.Comment))
            {
                result.StatusCode = "02";
                result.StatusMessage = "Parameter not complete.";
                result.Value = false;
                return result;
            }

            if(model.MemberID != memberId)
            {
                result.StatusCode = "04";
                result.StatusMessage = "Member not valid.";
                result.Value = false;
                return result;
            }

            Member member = Member.GetById(model.MemberID);
            if(member == null)
            {
                result.StatusCode = "02";
                result.StatusMessage = "member not exist.";
                result.Value = false;
                return result;
            }

            Receipt receiptObj = Receipt.GetById(model.ReceiptID);

            if(receiptObj == null)
            {
                result.StatusCode = "06";
                result.StatusMessage = "receipt not exist.";
                result.Value = false;
                return result;
            }

            QuickWin2Participation newObj = new QuickWin2Participation() { 
                ReceiptID = model.ReceiptID
                , ReceiptCode = receiptObj.Code
                , MemberID = model.MemberID 
                , MemberPhone = member.Phone
                , Comment = model.Comment  
                , ReceiptStatus = 0
            };

            var resObj = newObj.Insert(member.Name, "", member.ID);

            if(resObj.Success)
            {
                result.StatusCode = "00";
                result.StatusMessage = "success inserting comment.";
                result.Value = true;

            }
            else
            {
                result.StatusCode = "03";
                result.StatusMessage = "fail inserting comment.";
                result.Value = false;
            }
            return result;
        }
    }
}

