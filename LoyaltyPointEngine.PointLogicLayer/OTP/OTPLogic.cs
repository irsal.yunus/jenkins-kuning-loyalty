﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.OTP;
using System.Transactions;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Common.Notification;
using LoyaltyPointEngine.Common.Log;
using System.Globalization;
using LoyaltyPointEngine.PointLogicLayer.OCR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LoyaltyPointEngine.PointLogicLayer.OTP
{
    public class OTPLogic
    {
        /// <summary>
        /// Status OTP
        ///     Expired
        ///     Limited
        ///     Void
        ///     Success
        /// </summary>
        /// <returns></returns>
        public static string GenerateKeyOTP()
        {
            int Length = Convert.ToInt32(Settings.GetValueByKey("LengthOTP"));
            return Common.GenerateKey.RandomKey(Length, Settings.GetValueByKey("GenerateKeyOTP"));
        }

        public static ResultModel<OTPMemberModel> OTPMemberRegister(string Phone, string Url, long UserID, string poolName, bool CMS = false, string Username = null)
        {
            return OTPMemberRegister(Phone, Phone, Url, UserID, poolName, 0, CMS, Username);
        }

        public static ResultModel<OTPMemberModel> OTPMemberRegister(string Phone, string OldPhone, string Url, long UserID, string poolName, int isLogin, bool CMS = false, string Username = null)
        {
            ResultModel<OTPMemberModel> res = new ResultModel<OTPMemberModel>();
            try
            {
                #region "Validation"
                //Validate Number Phone
                if (!PhoneHelper.isPhoneValid(Phone))
                {
                    res.StatusCode = "404";
                    res.StatusMessage = string.Format("Format nomor handphone yang anda masukkan salah");
                    return res;
                }
                if (!OldPhone.Equals(Phone))
                {
                    if (!PhoneHelper.isPhoneValid(OldPhone))
                    {
                        res.StatusCode = "501";
                        res.StatusMessage = string.Format("Format nomor handphone lama salah");
                        return res;
                    }
                }
                //Check Phone in Member
                if (CMS == false) // Jika not from CMS
                {
                    if (!(isLogin == 1 && OldPhone.Equals(Phone)))
                    {
                        DataMember.Member Member = DataMember.Member.GetByPhone(Phone);
                        if (Member != null)
                        {
                            res.StatusCode = "30";
                            res.StatusMessage = "Phone already registered";
                            return res;
                        }
                    }
                }
                //Jika sudah Send SMS 3 dalam waktu 1 Jam
                var LimitSMS = PoolOTPMember.GetAllbyPhoneEx(Phone).Where(x => (x.Status == "Void" || x.Status == "Waiting")).ToList();
                if (LimitSMS.Count >= 3)
                {
                    if (CMS)
                    {
                        res.StatusCode = "30";
                        res.StatusMessage = Settings.GetValueByKey("OTPMemberOverCMS");
                        return res;
                    }
                    res.StatusCode = "30";
                    res.StatusMessage = Settings.GetValueByKey("OTPMemberOver");
                    return res;
                }
                //Check OTP Per 3 Menit 
                var LastRequest = PoolOTPMember.GetPhoneLastRequest(Phone);
                if (LastRequest != null)
                {
                    DateTime RequestDate = LastRequest.CreatedDate.Value.AddMinutes(3);
                    if (RequestDate > DateTime.Now)
                    {
                        res.StatusCode = "30";
                        res.StatusMessage = "Anda belum bisa request OTP.";
                        return res;
                    }
                }
                //Check All Request OTP
                var AllRequest = PoolOTPMember.GetAllbyPhoneRequest(Phone).ToList();
                if (AllRequest.Count >= 3)
                {
                    res.StatusCode = "30";
                    res.StatusMessage = Settings.GetValueByKey("OTPMemberOver");
                    return res;
                }

                //Check Phone Get Code OTP
                var data = PoolOTPMember.GetAllbyPhoneEx(Phone);
                ////If Not Void Is Delete
                if (data != null)
                {
                    foreach (var Dphone in data)
                    {
                        Dphone.Delete(Phone, Url, UserID);
                    }
                }
                #endregion

                //Generate Code OTP
                string OTPCode = GenerateKeyOTP();
                //Expired Code OTP 1 hours
                DateTime Expired = DateTime.Now.AddHours(1);
                //Text Message
                string Message = Settings.GetValueByKey("MessageOTPMember").Replace("[XXXX]", OTPCode);

                #region For CMS
                if (CMS == true) ///Expired BY CMS 1 day
                {
                    //Expired from CMS
                    Expired = DateTime.Now.AddDays(Convert.ToInt32(Settings.GetValueByKey("SetExpiredDaysVerification")));
                    //Encrypt Phone#otpcode
                    var Encryp = string.Format("{0}-{1}", Phone, OTPCode);

                    string LinkLong = string.Format("{0}{1}", Settings.GetValueByKey("LinkOTPMember"), Encryp);
                    //SHortURL 
                    string shortenErrMsg = "";
                    //var LinkShort = BitlyHelper.ShortenBitlyUsingAPiKey(LinkLong, out shortenErrMsg);
                    var LinkShort = LinkLong;
                    //Text Message from CMS
                    Message = Settings.GetValueByKey("MessageLinkOTPMember").Replace("[XXXX]", LinkShort);
                }
                #endregion

                //Insert into PoolOTPMember
                PoolOTPMember param = new PoolOTPMember();
                param.Email = Phone + "@" + Phone + ".com";
                param.Phone = Phone;
                param.RequestOTP = 0;
                param.Status = PoolOTPMember.OTPStatus.Waiting.ToString();
                param.OTPCode = OTPCode;
                param.OTPExpiredDate = Expired;
                param.Insert(CMS ? Username : Phone, Url, UserID);

                //Send SMSNotifOTP                
                //Get settings SMS OTP JPG
                string camp = Settings.GetValueByKey("campaignSMSOTP");
                string key = Settings.GetValueByKey("apikeySMSOTP");
                string user = Settings.GetValueByKey("apiuserSMSOTP");

                string channelToLixus = SiteSetting.ChannelWeb;
                string smsParam = string.Format("message={0}&campaignid={1}&apikey={2}&apiuser={3}&phone={4}&channel={5}", Message, camp, key, user, Phone, channelToLixus);
                //smsParam = string.Format("message={0}&campaignid={1}&apikey={2}&apiuser={3}&phone={4}", "Test OTP", "BEBOTP1921", "b3b91", "bebe", Phone);
                //string smsParam = string.Format("api_id={0}&access_token={1}&phone={2}&message={3}", SiteSetting.TelcomMateCId, SiteSetting.TelcomMateCToken, Phone, Message);

                LogSendType smsType = new LogSendType();
                if (CMS)
                {
                    smsType = LogSendType.ResendSms;
                }
                else
                {
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    smsType.Value = LogSendType.RegisterMember.Value + " " + textInfo.ToTitleCase(poolName);
                }

                var smsResponse = SendSMSNotifOTP(SiteSetting.ApiSendOTP, Phone, smsParam, Username, Url, UserID, smsType, channelToLixus);

                //Response Code OTP
                OTPMemberModel Response = new OTPMemberModel();
                Response.Phone = Phone;
                Response.OTPCode = "XXXXX";
                Response.OTPExpiredDate = Expired;
                Response.StatusOTP = PoolOTPMember.OTPStatus.Waiting.ToString();
                Response.RequestOTP = 0;

                res.StatusCode = "00";
                //res.StatusMessage = smsResponse.message;
                res.StatusMessage = "Kode OTP berhasil dikirim";
                res.Value = Response;
            }
            catch (Exception ex)
            {
                res.StatusCode = "500";
                res.StatusMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }

            return res;
        }

        public static ResultModel<bool> ValidateOTPOnly(string Phone, string OTPCode, string Url, long UserID)
        {
            ResultModel<bool> res = new ResultModel<bool>();
            res.Value = false;
            try
            {
                //Get Code OTP n Phone
                PoolOTPMember data = PoolOTPMember.GetByOTPReady(OTPCode, Phone);
                //If NULL +1 RequestOTP limit
                #region Data Not Found
                if (data == null)
                {
                    PoolOTPMember ret = PoolOTPMember.GetByPhone(Phone);
                    if (ret != null)
                    {
                        ret.RequestOTP = ret.RequestOTP + 1;
                        if (ret.RequestOTP == 1)
                        {
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberFalse1");
                        }
                        if (ret.RequestOTP == 2)
                        {
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberFalse2");
                        }
                        if (ret.RequestOTP >= 3)
                        {
                            ret.Status = PoolOTPMember.OTPStatus.Limited.ToString();
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberFalse3");
                        }
                        ret.Update(Phone, Url, UserID);
                    }
                    else
                    {
                        //Check Code OTP Ex ExpiredDate
                        PoolOTPMember model = PoolOTPMember.GetByPhoneEx(Phone);
                        //If Model not null Expired
                        if (model != null)
                        {
                            model.Status = PoolOTPMember.OTPStatus.Expired.ToString();
                            model.Update(Phone, Url, UserID);

                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberExpired");
                        }
                        else
                        {
                            //Code Not found OTP
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberNotFound");
                        }
                    }
                    return res;
                }
                #endregion
                //If Over Limit
                if (data.RequestOTP >= 3)
                {
                    data.Status = PoolOTPMember.OTPStatus.Limited.ToString();
                    data.Update(Phone, Url, UserID);

                    res.StatusCode = "60";
                    res.StatusMessage = Settings.GetValueByKey("OTPMemberOver");
                    return res;
                }

                //If Status not void
                if (data.Status != PoolOTPMember.OTPStatus.Void.ToString() && data.Status != PoolOTPMember.OTPStatus.Waiting.ToString())
                {
                    res.StatusCode = "55";
                    res.StatusMessage = "Code OTP sudah terpakai";
                    return res;
                }

                data.Status = PoolOTPMember.OTPStatus.Success.ToString();
                data.RequestOTP = data.RequestOTP + 1;
                
                res.StatusCode = "00";
                res.StatusMessage = "OTP Code Valid and Success";
                res.Value = true;
            }
            catch (Exception ex)
            {
                res.StatusCode = "500";
                res.StatusMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            return res;
        }

        public static ResultModel<PoolOTPMember> ValidateOTP(string Phone, string OTPCode, string Url, long UserID)
        {
            ResultModel<PoolOTPMember> res = new ResultModel<PoolOTPMember>();
            PoolOTPMember resValue = new PoolOTPMember();
            try
            {
                //Get Code OTP n Phone
                PoolOTPMember data = PoolOTPMember.GetByOTPReady(OTPCode, Phone);
                //If NULL +1 RequestOTP limit
                #region Data Not Found
                if (data == null)
                {
                    PoolOTPMember ret = PoolOTPMember.GetByPhone(Phone);
                    if (ret != null)
                    {
                        ret.RequestOTP = ret.RequestOTP + 1;
                        if (ret.RequestOTP == 1)
                        {
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberFalse1");
                        }
                        if (ret.RequestOTP == 2)
                        {
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberFalse2");
                        }
                        if (ret.RequestOTP >= 3)
                        {
                            ret.Status = PoolOTPMember.OTPStatus.Limited.ToString();
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberFalse3");
                        }
                        res.Value = ret;
                    }
                    else
                    {
                        //Check Code OTP Ex ExpiredDate
                        PoolOTPMember model = PoolOTPMember.GetByPhoneEx(Phone);
                        //If Model not null Expired
                        if (model != null)
                        {
                            model.Status = PoolOTPMember.OTPStatus.Expired.ToString();
                            res.Value = model;

                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberExpired");
                        }
                        else
                        {
                            //Code Not found OTP
                            res.StatusCode = "50";
                            res.StatusMessage = Settings.GetValueByKey("OTPMemberNotFound");
                        }
                    }
                    return res;
                }
                #endregion
                //If Over Limit
                if (data.RequestOTP >= 3)
                {
                    data.Status = PoolOTPMember.OTPStatus.Limited.ToString();

                    res.Value = data;
                    res.StatusCode = "60";
                    res.StatusMessage = Settings.GetValueByKey("OTPMemberOver");
                    return res;
                }

                //If Status not void
                if (data.Status != PoolOTPMember.OTPStatus.Void.ToString() && data.Status != PoolOTPMember.OTPStatus.Waiting.ToString())
                {
                    res.StatusCode = "55";
                    res.StatusMessage = "Code OTP sudah terpakai";
                    return res;
                }

                data.Status = PoolOTPMember.OTPStatus.Success.ToString();
                data.RequestOTP = data.RequestOTP + 1;
                data.Update(Phone, Url, UserID);

                res.Value = data;
                res.StatusCode = "00";
                res.StatusMessage = "OTP Code Valid and Success";
            }
            catch (Exception ex)
            {
                res.StatusCode = "500";
                res.StatusMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            return res;
        }

        public static SMSToLixusModel SendSMSNotifOTP(string urlApi, string phone, string smsParam, string Username, string Url, long UserID, LogSendType smsType, string channelToLixus)
        {
            SMSToLixusModel result = null;
            LogSendSMS logSendSMS = new LogSendSMS();
            logSendSMS.Url = urlApi;
            logSendSMS.Phone = phone;
            logSendSMS.RequestMessage = smsParam;
            logSendSMS.SmsType = smsType.Value;
            logSendSMS.ChannelToLixus = channelToLixus;

            try
            {
                result = RESTHelper.PostNotJson<SMSToLixusModel>(urlApi, smsParam);
                if (result.Code != null)
                {
                    logSendSMS.ResponseCode = result.Code.ToString();
                }
                logSendSMS.Transid = result.TransactionIdOTP;
                logSendSMS.ResponseMessage = JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {
               logSendSMS.ResponseMessage = ex.Message;
                throw ex;
            }

            logSendSMS.Insert(Username, Url, UserID);

            return result;
        }

        public static ResultModel<bool> SaveActivateMember(DataMember.Member paramMember, List<Receipt> paramReceipt, PoolOTPMember paramOtpMember, string url, int clientId)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = true;
            result.StatusCode = "00";
            result.StatusMessage = "Success Update Active" + " || " + "Success Update Restatus Receipt";
            //buka transaction
            TransactionOptions transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                try
                {
                    //update member jadi aktif
                    paramMember.IsActive = true;
                    paramMember.ActiveDate = DateTime.Now;
                    paramMember.ActiveBy = paramMember.FirstName + " " + paramMember.LastName;
                    var resultMember = paramMember.Updated(paramMember.ActiveBy);

                    if (resultMember == null || !resultMember.Success)
                    {
                        result.StatusCode = "61";
                        result.StatusMessage = "Gagal update member";
                        result.Value = false;
                        transScope.Dispose();
                        return result;
                    }

                    //update otp jadi terpakai
                    var resultOtp = paramOtpMember.Update(paramMember.Phone, url, clientId);
                    if (resultOtp == null || !resultOtp.Success)
                    {
                        result.StatusCode = "62";
                        result.StatusMessage = "Gagal update otp";
                        result.Value = false;
                        transScope.Dispose();
                        return result;
                    }

                    //Search receipt by Member n Status Unverify -2
                    if (paramReceipt != null && paramReceipt.Count() > 0)
                    {
                        foreach (var Restatus in paramReceipt)
                        {
                            if (Restatus.Status == (int)Receipt.ReceiptStatus.WaitingUnverify)
                            {
                                Restatus.Status = (int)Receipt.ReceiptStatus.WaitingVerify;
                            }
                            else
                            {
                                Restatus.Status = (int)Receipt.ReceiptStatus.Waiting;
                            }

                            // Update Status
                            EFResponse resultReceipt = Restatus.UpdateReceipt(paramMember.Phone, url, clientId);

                            if (!resultReceipt.Success)
                            {
                                result.StatusCode = "63";
                                result.StatusMessage = "Gagal update status receipt";
                                result.Value = false;
                                transScope.Dispose();
                                return result;
                            }

                            if (Restatus.Status == (int)Receipt.ReceiptStatus.WaitingVerify)
                            {
                                // send this receipt to snapcart and update the status to OCR Processing
                                var snapcartRes = OCRLogic.SendToSnapcart(Restatus, paramMember.Phone, url, clientId);

                                // Insert Point Notification for WaitingVerify
                                /*
                                ResultModel<Model.Object.Notification.NotificationModel> resultNotif = NotificationLogic.Create(Restatus.MemberID, NotificationType.Type.OnProgress, "Penambahan Point",
                                                         "No Transaksi: " + Restatus.Code, Restatus.Code, Restatus.Collect.Sum(x => x.TotalPoints), paramMember.Phone);

                                if (resultNotif.StatusCode != "00" || resultNotif.Value == null)
                                {
                                    result.StatusCode = "64";
                                    result.StatusMessage = "Gagal create notification";
                                    result.Value = false;
                                    transScope.Dispose();
                                    return result;
                                }*/
                            }
                        }
                    }

                    transScope.Complete();
                }
                catch (Exception ex)
                {
                    transScope.Dispose();
                    throw ex;
                }

            }

            return result;
        }

        public static ResultModel<bool> ValidateCallbackSMS(JObject callbackSMSParam, string url)
        {
            ResultModel<bool> res = new ResultModel<bool>();
           
            try
            {
                var param = callbackSMSParam.ToObject<CallbackSMSParam>();

                 if (param.Transid == null)
                 res.Value = false;
                 res.StatusCode = "55";
                 res.StatusMessage = "Transid not found";
               
                var dataLogSms = LogSendSMS.GetByTransid(param.Transid);
                //cek transid di tabel logsendsms
                if (dataLogSms != null)
                {
                    dataLogSms.DeliveredAt = param.DeliveredAt;
                    dataLogSms.Status = param.Status;
                    var updateLog = dataLogSms.Update(null, url, null);

                    var dataPoolOTP = PoolOTPMember.GetByPhoneWaiting(dataLogSms.Phone);
                    //cek status dari lixus, 000 => success, 100 => failed
                    if (param.Status == "000")
                    {
                        //cek OTP atau NonOTP, null=NonOTP, not null= OTP -> lanjut update table poolOTPmember 
                        if (dataPoolOTP != null)
                        {
                            dataPoolOTP.Status = PoolOTPMember.OTPStatus.Failed.ToString();

                            if (updateLog != null || updateLog.Success)
                            {
                                dataPoolOTP.Status = PoolOTPMember.OTPStatus.Void.ToString();
                                dataPoolOTP.Update("lixus", url, null);
                                res.Value = true;
                                res.StatusCode = "00";
                                res.StatusMessage = "Lixus OTP success";
                            }
                            else
                            {
                                res.StatusMessage = "Failed to Callback";
                            }

                        }
                        else
                        {
                            res.Value = true;
                            res.StatusCode = "00";
                            res.StatusMessage = "Lixus NonOTP success";
                        }
                    }
                    else
                    {
                        res.StatusMessage = "Status from Lixus = "+param.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                res.Value = false;
                res.StatusCode = "500";
                res.StatusMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            }
            return res;
        }
       
    }
}
