﻿using log4net;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.Model.Object.ConvertPoint;
using LoyaltyPointEngine.Model.Parameter.ConvertPoint;
using LoyaltyPointEngine.PointLogicLayer.Point;
using LoyaltyPointEngine.PointLogicLayer.PontaService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Xml;

namespace LoyaltyPointEngine.PointLogicLayer
{
    public class ConvertPointLogic : MainLogic
    {
        private static ILog Log = LogManager.GetLogger(typeof(ConvertPointLogic));

        public ConvertPointLogic()
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(ConvertPointLogic));
            }
        }

        public static ResultModel<List<ConvertPointStoreModel>> GetActiveStores()
        {
            ResultModel<List<ConvertPointStoreModel>> result = new ResultModel<List<ConvertPointStoreModel>>();
            List<ConvertPointStore> store = ConvertPointStore.GetAll().ToList();
            List<ConvertPointStoreModel> storeList = new List<ConvertPointStoreModel>();

            if (store == null || store.Count() <= 0)
            {
                result.StatusCode = "01";
                result.StatusMessage = string.Format("Data not found");
                return result;
            }

            foreach (var item in store)
            {
                storeList.Add(new ConvertPointStoreModel(item));
            }

            result.StatusCode = "00";
            result.StatusMessage = string.Format("Success get stores");
            result.Value = storeList;

            return result;
        }

        public static ResultModel<ConvertPointRuleModel> GetConvertPointRuleByStoreId(int storeId)
        {
            ResultModel<ConvertPointRuleModel> result = new ResultModel<ConvertPointRuleModel>();

            ConvertPointRule rule = ConvertPointRule.GetByStoreId(storeId);

            if (rule == null)
            {
                result.StatusCode = "01";
                result.StatusMessage = string.Format("Data not found");
                return result;
            }

            result.StatusCode = "00";
            result.StatusMessage = string.Format("Success get rule");
            result.Value = new ConvertPointRuleModel(rule);


            return result;
        }

        public static ResultModel<long> CreateConvertOut(int memberId, string memberName, string memberPhone, int ruleId, int points, int PoolID, int timeStamp, string description = "", string destinationNumber = "")
        {
            ResultModel<long> result = new ResultModel<long>();
            long balance = 0;
            string pontaId = string.Empty;
            //check input                                                                                                                                                                                                                                                                                                  
            if (memberId == 0)
            {
                result.StatusCode = "01";
                result.StatusMessage = "Member Id not provide.";
                result.Value = balance;
                return result;
            }

            if (ruleId == 0)
            {
                result.StatusCode = "02";
                result.StatusMessage = "rule not provide.";
                result.Value = balance;
                return result;
            }

            if (points == 0)
            {
                result.StatusCode = "03";
                result.StatusMessage = "Points not provide.";
                result.Value = balance;
                return result;
            }

            //get balance
            balance = long.Parse(Data.BalanceLog.GetTotalPointByMemberId(memberId).ToString());
            if (balance == 0)
            {
                result.StatusCode = "04";
                result.StatusMessage = "Balance is zero.";
                result.Value = balance;
                return result;
            }

            //check balance
            if (balance < points)
            {
                result.StatusCode = "05";
                result.StatusMessage = "Balance is not enough.";
                result.Value = balance;
                return result;
            }

            //get rule
            var rule = ConvertPointRule.GetById(ruleId);
            if (rule == null)
            {
                result.StatusCode = "06";
                result.StatusMessage = "Rule not correct.";
                result.Value = balance;
                return result;
            }

            var code = Number.Generated(Number.Type.RCP, memberName);
            //send request to store
            long storePoints = rule.StorePoint * points;
            var response = false;
            if (rule.StoreName.ToLower().Contains("nutri"))
            {
                var res = SendToNutri(rule, destinationNumber, (int)points, memberPhone);

                if (res != null && res.StatusCode == "00")
                {
                    response = true;
                }
                else if (res == null)
                {
                    result.StatusCode = "40";
                    result.StatusMessage = "Gagal menconvert poin.";
                    result.Value = 0;
                    return result;
                }
                else
                {
                    result.StatusCode = res.StatusCode;
                    result.StatusMessage = res.StatusMessage;
                    result.Value = 0;
                    return result;
                }
            }
            else if (rule.StoreName.ToLower().Contains("ponta"))
            {
                var IsBelowCap = CheckConvertPointCap(memberId, ruleId, points);
                if (IsBelowCap != null && IsBelowCap.StatusCode == "00")
                {
                    var res = ConvertPointToPonta(memberPhone, storePoints, code);

                    if (res != null && res.StatusCode == "00")
                    {
                        response = true;
                        pontaId = res.Value;
                    }
                    else if (res == null)
                    {
                        result.StatusCode = "40";
                        result.StatusMessage = "Gagal menconvert poin.";
                        result.Value = 0;
                        return result;
                    }
                    else
                    {
                        result.StatusCode = res.StatusCode;
                        result.StatusMessage = res.StatusMessage;
                        result.Value = 0;
                        return result;
                    }
                }
                else
                {
                    result.StatusCode = "40";
                    result.StatusMessage = "Point telah melebihi cap convert.";
                    result.Value = 0;
                    return result;
                }
            }
            else
            {
                response = true;
            }

            Guid transId = Guid.NewGuid();
            //if success
            if (response)
            {
                balance = balance - points;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    int restPoint = (int)points;
                    var collects = Data.Collect.GetAllActiveByMember(memberId).OrderBy(x => x.ExpiryDate).ToList();

                    foreach (var collect in collects)
                    {
                        if (restPoint <= 0)
                        {
                            //finish
                            break;
                        }
                        else
                        {
                            if (restPoint >= collect.TotalPoints)
                            {
                                ///1. collect di habisin semua
                                ///. respoint dikurangin
                                collect.IsUsed = true;
                                collect.ConvertID = transId;
                                collect.Updated(memberName);
                                restPoint = restPoint - collect.TotalPoints;
                            }
                            else
                            {
                                //2. di splits
                                //collect.Remarks = "inactive due to be separated in the process redeem";
                                collect.IsActive = false;
                                collect.Updated(memberName);
                                //collect point di pecah
                                //collect used
                                var collectUsed = SplitCollectRedeem(restPoint, transId, collect, true, memberName, 0);
                                var restCollect = SplitCollectRedeem(collect.TotalPoints - collectUsed.Points, null, collect, false, memberName, 1);
                                restPoint = 0;
                            }
                        }
                    }

                    //restPoint  -->masoh ada sisah tapi collect sudah tidak habis, tapi masih ada sisah point di balaancelog karena migrasi(migrasi gak create collect)
                    if (restPoint > 0)
                    {
                        var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(memberId);
                        if (lastBalanceLog != null && lastBalanceLog.Balance > 0)
                        {
                            if ((lastBalanceLog.Balance - points) >= 0)
                            {
                                restPoint = 0;
                            }
                        }
                    }

                    if (restPoint <= 0)
                    {
                        //save to convert pointlog
                        ConvertPointTransactionLog temp = new ConvertPointTransactionLog();
                        temp.Id = transId;
                        temp.memberId = memberId;
                        temp.TransactionDate = DateTime.Now;
                        temp.StoreId = rule.StoreId;
                        temp.StoreName = rule.StoreName;
                        temp.StorePoint = storePoints;
                        temp.Promo = rule.Promo;
                        temp.Points = points;
                        temp.Balance = balance;
                        temp.Description = description;
                        temp.code = code;
                        temp.DestinationNUmber = destinationNumber;
                        temp.Status = 1;
                        temp.Type = "out";
                        temp.Channel = "Website";
                        if (!string.IsNullOrEmpty(pontaId))
                        {
                            temp.PontaId = pontaId;
                        }
                        var responseEF = temp.Insert(memberName, "", null);

                        if (responseEF != null && responseEF.Success)
                        {
                            //Create BalanceLog
                            var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(memberId);
                            if (lastBalanceLog != null)
                            {
                                int newBalanceLog = lastBalanceLog.Balance - (int)points;
                                if (newBalanceLog >= 0)
                                {
                                    var respBalanceEF = BalanceLogLogic.Debit((int)points, string.Format("Convert Point Ke {0} No. Tujuan: {1} No.Transaksi: {2}", rule.StoreName, destinationNumber, code), memberId,
                                        PoolID, memberName, transId, newBalanceLog, timeStamp);
                                    timeStamp++;
                                    if (responseEF != null && responseEF.Success)
                                    {
                                        result.StatusCode = "00";
                                        result.StatusMessage = string.Format("Convert Points Successfully");
                                        result.Value = newBalanceLog;
                                        transScope.Complete();
                                        transScope.Dispose();

                                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(memberId, memberName);
                                    }
                                    else
                                    {
                                        transScope.Dispose();
                                        result.StatusCode = "500";
                                        result.StatusMessage = string.Format("Error when inserted data BalanceLog into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                                    }
                                }
                                else
                                {
                                    transScope.Dispose();
                                    result.StatusCode = "40";
                                    result.StatusMessage = string.Format("Your Balance's point is insufficient");
                                }
                            }
                            else
                            {
                                transScope.Dispose();
                                result.StatusCode = "41";
                                result.StatusMessage = string.Format("Your Balance's point is insufficient");
                            }
                        }
                        else
                        {
                            transScope.Dispose();
                            result.StatusCode = "500";
                            result.StatusMessage = string.Format("Error when inserted data convert point into table: {0}", (responseEF != null ? responseEF.ErrorMessage : "null"));
                        }
                    }
                    else
                    {
                        transScope.Dispose();
                        result.StatusCode = "20";
                        result.StatusMessage = string.Format("Your point is insufficient");
                    }
                }//end use

                ////create redeem
                //SubmitRedeemParameterModel model = new SubmitRedeemParameterModel();
                //model.Point = (int)points;
                //model.TransactionCode = code;
                //ResultModel<RedeemDetailModel> resRedeem = RedeemLogic.Create(model, memberId, memberName, PoolID);
                //if(resRedeem.StatusCode != "00")
                //{
                //    result.StatusCode = resRedeem.StatusCode;
                //    result.StatusMessage = resRedeem.StatusMessage;
                //    result.Value = balance;
                //    return result;
                //}
                //PointLogicLayer.Point.BalanceLogLogic.Debit(int.Parse(points.ToString()), "Convert point to " + rule.StoreName, memberId, PoolID, memberName, transId, (int)balance, timeStamp);
                //PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(memberId, memberName);
                //RESTHelper.Get<ResultModel<ConvertPointRuleModel>>(Data.Settings.GetValueByKey(SiteSettings.LOYAL_API_URL) + string.Format(Data.Settings.GetValueByKey(SiteSettings.EndPoint_ConvertPointGetRuleByStoreId), id), accessToken);
            }
            else
            {
                result.StatusCode = "30";
                result.StatusMessage = string.Format("Proses gagal ketika sedang di proses oleh " + rule.StoreName);
            }
            return result;
        }

        public static ResultModel<long> CreateConvertIn(string phoneNumber, string actionCode, int points, int PoolID, int timeStamp, string description = "", string fromNumber = "")
        {
            ResultModel<long> result = new ResultModel<long>();

            //CollectCMSModel model, int PoolID, string UserName, long? UserID, string Url, int TimeStamp, Data.Action dataAction = null, bool SaveWithBalance = true
            //Data.Collect tempCollect = Data.Collect.GetById
            //public string TransactionDate { get; set; }
            //public int MemberID { get; set; }
            //public int? ActionID { get; set; }
            //public string ExpiryDate { get; set; }
            //public int PoolID { get; set; }
            //public int Quantity { get; set; }
            //public int QuantityApproved { get; set; }
            //public int Points { get; set; }
            //public int TotalPoints { get; set; }
            //public string Remarks { get; set; }
            //public bool IsApproved { get; set; }
            //public bool IsActive { get; set; }
            //public bool IsUsed { get; set; }
            //public string CreatedBy { get; set; }
            //public string CreatedDate { get; set; }


            DataMember.Member objMember = DataMember.Member.GetByPhone(phoneNumber);

            Data.Action objAction = Data.Action.GetByCode(actionCode);
            //cek apakah no telpon tsb merupakan member
            if (objMember == null)
            {
                result.StatusCode = "01";
                result.StatusMessage = "Nomor telepon tidak terdaftar di sistem.";
                return result;
            }

            if (objAction == null)
            {
                result.StatusCode = "02";
                result.StatusMessage = "Code store tidak terdaftar di sistem.";
                return result;
            }

            Guid transId = Guid.NewGuid();
            var code = Number.Generated(Number.Type.RCP, objMember.FirstName);

            //convert to nutripoint
            ConvertPointRule objRule = ConvertPointRule.GetByActionCode(actionCode);
            if (objRule == null)
            {
                result.StatusCode = "03";
                result.StatusMessage = "Code Action tidak terdaftar di sistem.";
                return result;
            }
            points = ((int)objRule.Point * points / (int)objRule.StorePoint);


            //save to convert pointlog
            ConvertPointTransactionLog temp = new ConvertPointTransactionLog();
            temp.Id = transId;
            temp.memberId = objMember.ID;
            temp.TransactionDate = DateTime.Now;
            temp.StoreId = objAction.ID;
            temp.StoreName = objAction.Code;
            temp.StorePoint = objAction.Points;
            temp.Promo = 0;
            temp.Points = points;
            temp.Balance = (long)(objMember.MemberPoint + (objAction.Points * points));
            temp.Description = description;
            temp.code = code;
            temp.DestinationNUmber = phoneNumber;
            temp.Status = 1;
            temp.Type = "in";
            temp.Channel = "Website";
            var responseEF = temp.Insert(objMember.FirstName, "", null);

            if (responseEF != null && responseEF.Success)
            {
                CollectCMSModel collectCMS = new Model.Object.Collect.CollectCMSModel();

                collectCMS.TransactionDate = DateTime.Now.ToString("dd/MM/yyyy");
                collectCMS.MemberID = objMember.ID;
                collectCMS.ActionID = objAction.ID;
                collectCMS.ExpiryDate = DateTime.Now.AddDays(365).ToString("dd/MM/yyyy");
                collectCMS.PoolID = PoolID;
                collectCMS.Quantity = points;
                collectCMS.QuantityApproved = points;
                collectCMS.Points = objAction.Points;
                collectCMS.TotalPoints = objAction.Points * points;
                collectCMS.Remarks = description;
                collectCMS.IsApproved = true;
                collectCMS.IsActive = true;
                collectCMS.IsUsed = false;
                collectCMS.CreatedBy = objMember.FirstName;
                collectCMS.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                collectCMS.ConvertID = transId;

                Collect.CollectLogic obj = new Collect.CollectLogic();

                var resultCollect = obj.InsertCollect(collectCMS, PoolID, objMember.FirstName, 1, string.Format("Penambahan Point {0} dari {1}", objAction.Name, fromNumber), 0);

                if (resultCollect.StatusCode == "00")
                {
                    NotificationLogic.Create(objMember.ID, Common.Notification.NotificationType.Type.Approved, "PENAMBAHAN POINT " + points.ToString(), "No Transaksi: " + code + " dan No Pengirim:" + fromNumber, code, points, objMember.FirstName);
                }

                result.StatusCode = resultCollect.StatusCode;
                result.StatusMessage = resultCollect.StatusMessage;
                result.Value = (long)points;
            }

            return result;
        }

        private static Data.Collect SplitCollectRedeem(int point, Guid? convertId, Data.Collect parent, bool IsUsed, string by, int Timestamp)
        {
            var splitCollect = new Data.Collect();
            splitCollect.ID = Guid.NewGuid();
            splitCollect.MemberID = parent.MemberID;
            splitCollect.Points = point;

            splitCollect.IsUsed = true;
            splitCollect.ParentID = parent.ID;
            splitCollect.TransactionDate = DateTime.Now.AddSeconds(Timestamp);
            splitCollect.ExpiryDate = parent.ExpiryDate;
            splitCollect.PoolID = parent.PoolID;
            splitCollect.Quantity = parent.Quantity;
            splitCollect.QuantityApproved = parent.QuantityApproved;
            splitCollect.TotalPoints = point;
            splitCollect.Remarks = "split";
            splitCollect.IsApproved = true;
            splitCollect.IsActive = true;

            splitCollect.IsUsed = IsUsed;

            if (IsUsed)
            {
                splitCollect.ConvertID = convertId;
            }

            var json = JsonConvert.SerializeObject(splitCollect);
            Log.Info("Data Collect = " + json);

            splitCollect.Inserted(by, Timestamp);

            return splitCollect;
        }

        private static ResultModel<long> SendToNutri(ConvertPointRule rule, string destinationNumber = "", int points = 0, string fromNumber = "")
        {
            string accessToken = "MlvabmFWC3zwR2M8KFcAmnZcBqo=";
            ParamConvertIn param = new ParamConvertIn();
            param.ClientID = "bebelacconvert";
            param.ClientSecret = "1234567890qwerty";
            param.phone = destinationNumber;
            param.actionCode = "NUTRI001";
            param.points = points;
            param.description = "";
            param.FromNumber = fromNumber;

            var res = RESTHelper.Post<ResultModel<long>>(rule.ConvertOutUrl, param, accessToken);
            return res;
        }

        private static ResultModel<string> ConvertPointToPonta(string phone, long point, string transactionCode)
        {
            ResultModel<string> result = new ResultModel<string>();
            result.Value = string.Empty;
            string pontaId = string.Empty;
            XmlDocument loginResponse = PontaService.PontaService.Login();
            XmlNode responseCode = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/ResponseCode"),
                responseErrorCode = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/ErrorCode"),
                responseErrorMessage = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/ErrorMessage");

            if (responseCode.InnerText == "00")
            {
                XmlNode accessToken = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/AccessToken");
                XmlDocument enquiryResponse = PontaService.PontaService.Enquiry(accessToken.InnerText, phone, "MOBILE");
                responseCode = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/RsHeader/ResponseCode");
                responseErrorCode = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/RsHeader/ErrorCode");
                responseErrorMessage = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/RsHeader/ErrorMessage");

                if (responseCode.InnerText == "00")
                {
                    XmlNode cardId = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/MemberAccounts/MemberAccount/AccountNo");
                    pontaId = cardId.InnerText;
                    if (!PontaService.PontaSettings.Settings.Stores.AllKeys.Any(x => cardId.InnerText.StartsWith(x.ToString())))
                    {
                        result.StatusCode = "40";
                        result.StatusMessage = "Ponta Id tidak termasuk dalam program convert point.";
                        return result;
                    }

                    PontaService.Transaction transaction = new PontaService.Transaction();
                    transaction.Points = point;
                    transaction.GrossTransactionAmount = point;
                    transaction.TotalGramsOfPurchase = 0;
                    transaction.TotalUnitsOfPurchase = 1;
                    transaction.TotalNoOfItemsPurchase = 1;
                    XmlDocument awardResponse = PontaService.PontaService.AwardOnline(accessToken.InnerText, cardId.InnerText, "CARD", "", transactionCode, transaction);
                    responseCode = awardResponse.SelectSingleNode("//TransactionRs/RsHeader/ResponseCode");
                    responseErrorCode = awardResponse.SelectSingleNode("//TransactionRs/RsHeader/ErrorCode");
                    responseErrorMessage = awardResponse.SelectSingleNode("//TransactionRs/RsHeader/ErrorMessage");

                    if (responseCode.InnerText == "00")
                    {
                        XmlDocument logoutResponse = PontaService.PontaService.Logout(accessToken.InnerText);
                        responseCode = logoutResponse.SelectSingleNode("//OperatorLogoutRs/RsHeader/ResponseCode");
                        responseErrorCode = logoutResponse.SelectSingleNode("//OperatorLogoutRs/RsHeader/ErrorCode");
                        responseErrorMessage = logoutResponse.SelectSingleNode("//OperatorLogoutRs/RsHeader/ErrorMessage");

                        if (responseCode.InnerText == "00")
                        {
                            result.StatusCode = "00";
                            result.Value = pontaId;
                            return result;
                        }
                    }
                }
            }

            string errorMessage = responseErrorMessage.InnerText;
            var messageSetting = PontaSettings.Settings.PontaResponseCodeMessages[responseErrorCode.InnerText];
            if (messageSetting != null)
            {
                errorMessage = messageSetting.Message;
            }

            result.StatusCode = responseErrorCode.InnerText;
            result.StatusMessage = errorMessage;
            return result;
        }

        public static ResultModel<bool> ValidateStoreMembership(long storeId, string userId)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = false;

            ConvertPointStore store = ConvertPointStore.GetById(storeId);
            if (store == null)
            {
                result.StatusCode = "40";
                result.StatusMessage = string.Format("store with id {0} is not found.", storeId);
                return result;
            }

            if (store.StoreName.ToLower().Contains("ponta"))
            {
                XmlDocument loginResponse = PontaService.PontaService.Login();
                XmlNode responseCode = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/ResponseCode"),
                    responseErrorCode = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/ErrorCode"),
                    responseErrorMessage = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/ErrorMessage");

                if (responseCode.InnerText == "00")
                {
                    XmlNode accessToken = loginResponse.SelectSingleNode("//OperatorLoginRs/RsHeader/AccessToken");
                    XmlDocument enquiryResponse = PontaService.PontaService.Enquiry(accessToken.InnerText, userId, "MOBILE");
                    responseCode = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/RsHeader/ResponseCode");
                    responseErrorCode = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/RsHeader/ErrorCode");
                    responseErrorMessage = enquiryResponse.SelectSingleNode("//EnquiryWithNameRs/RsHeader/ErrorMessage");

                    if (responseCode.InnerText == "00")
                    {
                        XmlDocument logoutResponse = PontaService.PontaService.Logout(accessToken.InnerText);
                        responseCode = logoutResponse.SelectSingleNode("//OperatorLogoutRs/RsHeader/ResponseCode");
                        responseErrorCode = logoutResponse.SelectSingleNode("//OperatorLogoutRs/RsHeader/ErrorCode");
                        responseErrorMessage = logoutResponse.SelectSingleNode("//OperatorLogoutRs/RsHeader/ErrorMessage");

                        if (responseCode.InnerText == "00")
                        {
                            //Tambah validasi upload struk yang sudah di approve
                            var memberId = DataMember.Member.GetByPhone(userId);
                            if (memberId != null)
                            {
                                var ApprovedReceipt = Receipt.GetByMemberId(memberId.ID).Where(x => x.Status == 1);
                                if (ApprovedReceipt.Count() <= 0)
                                {
                                    result.StatusCode = "40";
                                    result.StatusMessage = "Maaf Ibu harus melakukan upload struk terlebih dahulu.";
                                    result.Value = false;
                                    return result;
                                }
                            }

                            result.StatusCode = "00";
                            result.Value = true;
                            return result;
                        }
                    }
                }
                string errorMessage = responseErrorMessage.InnerText;
                var messageSetting = PontaSettings.Settings.PontaResponseCodeMessages[responseErrorCode.InnerText];
                if (messageSetting != null)
                {
                    errorMessage = messageSetting.Message;
                }
                result.StatusCode = responseErrorCode.InnerText;
                result.StatusMessage = errorMessage;
                return result;
            }
            else
            {
                result.StatusCode = "41";
                result.StatusMessage = "store not found or you are not registered as this store member.";
                return result;
            }
        }

        public static ResultModel<bool> CheckConvertPointCap(int memberId, int ruleId, int points)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            long balance = 0;
            if (memberId == 0)
            {
                result.StatusCode = "01";
                result.StatusMessage = "Member Id not provide.";
                result.Value = false;
                return result;
            }

            if (ruleId == 0)
            {
                result.StatusCode = "02";
                result.StatusMessage = "rule not provide.";
                result.Value = false;
                return result;
            }

            if (points == 0)
            {
                result.StatusCode = "03";
                result.StatusMessage = "Points not provide.";
                result.Value = false;
                return result;
            }

            //get balance
            balance = long.Parse(Data.BalanceLog.GetTotalPointByMemberId(memberId).ToString());
            if (balance == 0)
            {
                result.StatusCode = "04";
                result.StatusMessage = "Balance is zero.";
                result.Value = false;
                return result;
            }

            //check balance
            if (balance < points)
            {
                result.StatusCode = "05";
                result.StatusMessage = "Balance is not enough.";
                result.Value = false;
                return result;
            }

            //get rule
            var rule = ConvertPointRule.GetById(ruleId);
            if (rule == null)
            {
                result.StatusCode = "06";
                result.StatusMessage = "Rule not correct.";
                result.Value = false;
                return result;
            }
            else if (rule != null && rule.ConvertCap != null && rule.ConvertCap <= 0)
            {
                result.StatusCode = "00";
                result.StatusCode = "No cap";
                result.Value = true;
                return result;
            }

            var MinConvert = rule.MinConvert.HasValue ? rule.MinConvert.Value : 3000;
            if (points < MinConvert)
            {
                result.StatusCode = "09";
                result.StatusMessage = string.Format("Minimal jumlah penukaran point adalah {0}", MinConvert.ToString());
                result.Value = false;
                return result;
            }

            DateTime date = DateTime.Now;
            var from = new DateTime(date.Year, date.Month, 1);
            var to = from.AddMonths(1).AddDays(-1);

            int ConvertCount = ConvertPointTransactionLog.GetConvertCount(memberId, from, to);
            int convertCountRule = rule.ConvertCount.HasValue ? rule.ConvertCount.Value : 17;
            if (ConvertCount >= convertCountRule)
            {
                result.StatusCode = "08";
                result.StatusMessage = string.Format("Dalam satu bulan Ibu hanya dapat melakukan konversi sebanyak {0} kali", convertCountRule.ToString());
                result.Value = false;
                return result;
            }

            var pointSum = ConvertPointTransactionLog.GetStorePointsByMemberIdTransDate(memberId, from, to) + (points * (rule.StorePoint / rule.Point));
            if (pointSum >= rule.ConvertCap)
            {
                result.StatusCode = "07";
                result.StatusMessage = "Anda telah mencapai batas maksimal convert point";
                result.Value = false;
                return result;
            }

            result.StatusCode = "00";
            result.StatusMessage = "Points conversion is valid";
            result.Value = true;
            return result;
        }
    }
}

