﻿using System;
using System.Collections.Generic;
using System.Linq;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.BebeJourney;

namespace LoyaltyPointEngine.PointLogicLayer.BebeJourney
{
    public class BebeJourneyLogic
    {
        public static ResultModel<BebeJourneyModel> Add(BebeJourneyModel param, int memberID, string memberName)
        {
            ResultModel<BebeJourneyModel> result = new ResultModel<BebeJourneyModel>();

            var model = new Data.BebeJourney();
            model.MemberID = memberID;
            model.Question = param.Question;
            model.Value = param.Value;
            var res = model.Insert(memberName, "BebeJourney", memberID);

            if (res.Success)
            {
                result.StatusCode = "00";
                result.StatusMessage = "Data has been saved.";
                result.Value = param;
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = res.ErrorMessage;
                result.Value = param;
            }

            return result;
        }

        public static ResultPaginationModel<List<BebeJourneyModel>> GetPagination(int page, int size)
        {
            ResultPaginationModel<List<BebeJourneyModel>> result = new ResultPaginationModel<List<BebeJourneyModel>>();
            List<BebeJourneyModel> modelList = new List<BebeJourneyModel>();

            var data = Data.BebeJourney.GetPagination(page, size).ToList();

            foreach (var items in data)
            {
                BebeJourneyModel model = new BebeJourneyModel();
                model.ID = items.ID;
                model.MemberID = items.MemberID;
                model.Question = items.Question;
                model.Value = items.Value;
                model.CreatedDate = items.CreatedDate.HasValue ? items.CreatedDate.Value : DateTime.MinValue;
                modelList.Add(model);
            }

            result.StatusCode = "00";
            result.TotalRow = Data.BebeJourney.CountRow();
            result.StatusMessage = "Success Get Data";
            result.Value = modelList;

            return result;
        }
    }
}
