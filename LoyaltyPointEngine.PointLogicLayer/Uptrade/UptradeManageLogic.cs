﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Parameter.Uptrade;

namespace LoyaltyPointEngine.PointLogicLayer
{
    public partial class UptradeManageLogic
    {
        public static ResultModel<List<CollectUptradeModel>> SyncTradelogic(int UptradeID, string url, long? UserID, string By)
        {
            ResultModel<List<CollectUptradeModel>> ret = new ResultModel<List<CollectUptradeModel>>();
            var list = new List<CollectUptradeModel>();
            UptradeManage model = UptradeManage.GetById(UptradeID);
            if (model == null)
            {
                ret.StatusCode = "20";
                ret.StatusMessage = "Up Trade not found";
                return ret;
            }
            var UptradeAfter = Data.Collect.GetAll()
                                .Where(x => x.Action.ProductID == model.ProductAfter && x.TransactionDate >= model.StartDate
                                    && x.TransactionDate <= model.EndDate && x.IsApproved == true && x.IsActive == true
                                    && x.Receipt.Status == 1);//Cek Product yg dibeli dan rentang waktu
            int total = 0;
            int existing = 0;
            foreach (var data in UptradeAfter.ToList())
            {

                var collect = Data.Collect.GetAll() //Cek Product sebelumnya
                                    .Where(x => x.Action.ProductID == model.ProductBefore && x.ReceiptID != null
                                        && x.MemberID == data.MemberID && x.TransactionDate <= data.TransactionDate
                                        && x.IsApproved == true && x.IsActive == true && x.Receipt.Status == 1).FirstOrDefault(); //Collects product before dibawah transaction date product after
                if (collect != null)
                {
                    if (model.Channel.ToLower() != "all channel") ///Utk Channel tertentu yg udh di set UpTrade kecuali "all channel"
                    {
                        var receipt = Data.Receipt.GetAll() //Cek Channel, MemberID status Approve ReceiptID
                            .Where(x => x.Channel.ToLower() == model.Channel.ToLower() && x.MemberID == data.MemberID && x.Status == 1
                                && x.ID == collect.ReceiptID)
                            .FirstOrDefault();

                        if (receipt != null) //Jika ada 
                        {
                            if (data.Remarks != model.Remarks)
                            {
                                string by = "Sync by " + By;
                                //Insert Log Uprade
                                LogUptradeMember log = new LogUptradeMember();
                                log.FlagUptradeID = model.FlagID;
                                log.MemberID = data.MemberID;
                                log.UptradeManageID = model.ID;
                                log.Insert(by);

                                //Update remarks Collects yg After
                                var rm = Data.Collect.GetById(data.ID);
                                rm.Remarks = model.Remarks;
                                rm.Update(by, url, UserID);

                                var point = InsertPoint(model, data.Quantity, data.MemberID, Convert.ToInt32(model.ProductAfter.Value), by); //Cek Point n Insert Point
                                list.Add(new CollectUptradeModel
                                {
                                    ReceiptCode = data.Receipt.Code,
                                    Channel = receipt.Channel,
                                    ProductAfter = data.Action.Product.Name,
                                    ProductBefore = collect.Action.Product.Name,
                                    ProductAfterDate = data.TransactionDate,
                                    ProductBeforeDate = collect.TransactionDate,
                                    Remarks = model.Remarks
                                });
                                total++;
                            }
                            else
                            {
                                list.Add(new CollectUptradeModel
                                {
                                    ReceiptCode = data.Receipt.Code,
                                    Channel = receipt.Channel,
                                    ProductAfter = data.Action.Product.Name,
                                    ProductBefore = collect.Action.Product.Name,
                                    ProductAfterDate = data.TransactionDate,
                                    ProductBeforeDate = collect.TransactionDate,
                                    Remarks = "Existing"
                                });
                                existing++; //data sudah ada dihitung
                            }
                        }
                    }
                    else
                    {
                        var receipt = Data.Receipt.GetAll() //Cek MemberID status Approve ReceiptID
                            .Where(x => x.MemberID == data.MemberID && x.Status == 1 && x.ID == collect.ReceiptID).FirstOrDefault();

                        if (receipt != null) //Jika ada 
                        {
                            if (data.Remarks != model.Remarks)
                            {
                                string by = "Sync by " + By;
                                //Insert Log Uprade
                                LogUptradeMember log = new LogUptradeMember();
                                log.FlagUptradeID = model.FlagID;
                                log.MemberID = data.MemberID;
                                log.UptradeManageID = model.ID;
                                log.Insert(by);

                                //Update remarks Collects yg After
                                var rm = Data.Collect.GetById(data.ID);
                                rm.Remarks = model.Remarks;
                                rm.Update(by, url, UserID);

                                var point = InsertPoint(model, data.Quantity, data.MemberID, Convert.ToInt32(model.ProductAfter.Value), by); //Cek Point n Insert Point
                                list.Add(new CollectUptradeModel
                                {
                                    ReceiptCode = data.Receipt.Code,
                                    Channel = receipt.Channel,
                                    ProductAfter = data.Action.Product.Name,
                                    ProductBefore = collect.Action.Product.Name,
                                    ProductAfterDate = data.TransactionDate,
                                    ProductBeforeDate = collect.TransactionDate,
                                    Remarks = model.Remarks
                                });
                                total++;
                            }
                            else
                            {
                                list.Add(new CollectUptradeModel
                                {
                                    ReceiptCode = data.Receipt.Code,
                                    Channel = receipt.Channel,
                                    ProductAfter = data.Action.Product.Name,
                                    ProductBefore = collect.Action.Product.Name,
                                    ProductAfterDate = data.TransactionDate,
                                    ProductBeforeDate = collect.TransactionDate,
                                    Remarks = "Existing"
                                });
                                existing++;  //data sudah ada dihitung
                            }
                        }
                    }
                }
            }
            ret.StatusCode = "00";
            ret.StatusMessage = "Data Sync = " + total + " Existing = " + existing;
            ret.Value = list;
            return ret;
        }
        public static ResultModel<UptradeManageModel> Tradelogic(int MemberID, string Channel, int ProductID, string url, int Quantity, string By = null) //For Logic
        {
            ResultModel<UptradeManageModel> ret = new ResultModel<UptradeManageModel>();
            ret.StatusCode = "30";
            var valid = Validateparam(MemberID, Channel, ProductID);
            if (valid.StatusCode != "00")
            {
                return valid;
            }
            DateTime dts = DateTime.Now;
            var Flag = UptradeManage.GetAll() //Cek Product yg dibeli dan rentang waktu
                .Where(x => x.ProductAfter == ProductID && x.StartDate <= dts && x.EndDate >= dts);
            var Uptrade = Flags(Flag, MemberID);  //Cek Product Before n channel

            if (Uptrade.ID == 0)
            {
                ret.StatusMessage = "Uptrade belum ditemukan";
                return ret;
            }

            LogUptradeMember log = new LogUptradeMember();
            log.FlagUptradeID = Uptrade.FlagID;
            log.MemberID = MemberID;
            log.UptradeManageID = Uptrade.ID;
            log.Insert(By);

            var point = InsertPoint(Uptrade, Quantity, MemberID, ProductID, By); //Cek Point n Insert Point

            UptradeManageModel value = new UptradeManageModel();
            value.Remarks = Uptrade.Remarks;

            ret.Value = value;
            ret.StatusCode = "00";
            ret.StatusMessage = "Success Uptrade";
            return ret;
        }
        private static UptradeManage Flags(IQueryable<UptradeManage> models, int MemberID)
        {
            var ret = new UptradeManage();
            foreach (var model in models)
            {
                if (model.Channel.ToLower() != "all channel") ///Utk Channel tertentu yg udh di set UpTrade kecuali "all channel"
                {
                    var receipt = Data.Collect.GetAll() //Cek Product before, channel, MemberID, Approved
                            .Where(x => x.Action.ProductID == model.ProductBefore && x.ReceiptID != null && x.MemberID == MemberID
                                && x.IsApproved == true && x.IsActive == true && x.Receipt.Status == 1 && x.Receipt.Channel.ToLower() == model.Channel.ToLower()).FirstOrDefault();
                    if (receipt != null) //Jika ada hentikan
                    {
                        ret = model;
                        break;
                    }
                }
                else
                {
                    var receipt = Data.Collect.GetAll() //Cek Product before, MemberID, Approved
                            .Where(x => x.Action.ProductID == model.ProductBefore && x.ReceiptID != null && x.MemberID == MemberID
                                && x.IsApproved == true && x.IsActive == true && x.Receipt.Status == 1).FirstOrDefault();
                    if (receipt != null) //Jika ada hentikan
                    {
                        ret = model;
                        break;
                    }
                }
            }
            return ret;
        }
        private static ResultModel<UptradeManageModel> Validateparam(int MemberID, string Channel, long ProductID)
        {
            ResultModel<UptradeManageModel> res = new ResultModel<UptradeManageModel>();
            res.StatusCode = "20";
            if (MemberID == null || MemberID < 1)
            {
                res.StatusMessage = "Param MemberID is required";
                return res;
            }
            if (string.IsNullOrEmpty(Channel))
            {
                res.StatusMessage = "Param MemberID is required";
                return res;
            }
            if (ProductID == null || ProductID < 1)
            {
                res.StatusMessage = "Param MemberID is required";
                return res;
            }
            res.StatusCode = "00";
            return res;
        }
        public static bool InsertPoint(UptradeManage model, int Quantity, int MemberID, int ProductID, string By)
        {
            bool ret = new bool();
            ret = false;
            var act = Data.Action.GetAll().FirstOrDefault(x => x.ProductID == ProductID);
            //var proafter = Product.GetByID(Convert.ToInt32(Uptrade.ProductAfter));
            var probefore = Product.GetByID(Convert.ToInt32(model.ProductBefore));

            //Tambah Point dan remarks in Receipt (Take Out)
            if (model.Point > 1)
            {
                Data.Collect collect = new Data.Collect();
                collect.ID = Guid.NewGuid();
                collect.MemberID = MemberID;
                collect.ActionID = act.ID;
                collect.Quantity = Quantity;
                collect.QuantityApproved = Quantity;
                collect.Points = Convert.ToInt32(model.Point.Value);
                collect.Remarks = model.Remarks;
                string Remarks = string.Format("Penambahan Point from Uptrade Product Before {0}", probefore.Name, act.Product.Name);
                Point.BalanceLogLogic.Credit(Convert.ToInt32(model.Point.Value), Remarks, MemberID, 1, By, collect.ID, 0);
                //Point.BalanceLogLogic.UpdateMemberPoint(MemberID, By);
                ret = true;
            }
            return ret;
        }
    }
}
