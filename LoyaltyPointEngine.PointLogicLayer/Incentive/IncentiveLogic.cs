﻿using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.PointLogicLayer.Point;
using LoyaltyPointEngine.Model.Object.Incentive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Xml;

namespace LoyaltyPointEngine.PointLogicLayer.Incentive
{
    public class IncentiveLogic
    {
        public static void CheckIncentive(Member member, Receipt receipt, string by, bool memberIsIdle = false)
        {
            //check isfirstsubmit
            if (!receipt.IsFirstSubmission)
            {
                if(!memberIsIdle)
                {
                    return;
                }
            }

            if (member == null)
            {
                return;
            }

            if (receipt == null)
            {
                return;
            }

            //check registerby            
            DataMember.Member memberObj = DataMember.Member.GetById(member.ID);

            if (memberObj == null)
            {
                return;
            }

            string registerBy = string.Empty;

            if (memberObj.SpgID != null)
            {
                registerBy = int.Parse(memberObj.SpgID.ToString()).ToString();
            }

            //check uploaded by
            string uploadedBy = string.Empty;
            if (receipt.CreatedByID != null)
            {
                //get to cmsuser
                CMSUser objCmsUser = CMSUser.GetByID(long.Parse(receipt.CreatedByID.ToString()));

                if (objCmsUser != null)
                {
                    var spg = DataMember.SPG.GetByCode(objCmsUser.Username);

                    if (spg != null)
                    {
                        uploadedBy = spg.ID.ToString();
                    }

                }

            }

            //check mapping
            string dbRegisteredBy = string.Empty;
            string dbUploadBy = string.Empty;

            if (string.IsNullOrEmpty(registerBy))
            {
                dbRegisteredBy = "noelina";
            }
            else
            {
                dbRegisteredBy = "elinaa";
            }

            if (string.IsNullOrEmpty(uploadedBy))
            {
                dbUploadBy = "noelina";
            }
            else
            {
                if (registerBy != uploadedBy)
                {
                    dbUploadBy = "elinab";
                }
                else
                {
                    dbUploadBy = "elinaa";
                }

            }

            //set incentive
            var mappingIncentive = IncentiveSpgMapping.GetByCondition(dbRegisteredBy, dbUploadBy);

            if (mappingIncentive == null || string.IsNullOrEmpty(mappingIncentive.IncentiveTo))
            {
                return;
            }

            string incentiveTo = string.Empty;
            switch (mappingIncentive.IncentiveTo)
            {
                case "elinaa": { incentiveTo = registerBy; break; }
                case "elinab": { incentiveTo = uploadedBy; break; }
            }

            var objSpg = DataMember.SPG.GetById(int.Parse(incentiveTo));
            if (objSpg == null)
            {
                return;
            }

            //check incentiveSpg
            var objIncentiveSpg = Data.IncentiveSpg.GetBySpgCode(objSpg.Code, DateTime.Now.Month, DateTime.Now.Year);

            string incentiveValue = Data.Settings.GetValueByKey("IncentiveValue");
            if (string.IsNullOrEmpty(incentiveValue))
            {
                return;
            }

            //jika gak null maka d update
            if (objIncentiveSpg != null)
            {
                int res = 0;
                objIncentiveSpg.Incentive = objIncentiveSpg.Incentive + long.Parse(incentiveValue);
                objIncentiveSpg.NumberFirstSubmit = int.TryParse(objIncentiveSpg.NumberFirstSubmit.ToString(), out res) ? res + 1 : res;
                objIncentiveSpg.Update();
            }
            else//pertama x untuk bulan itu dan taon itu
            {
                Data.IncentiveSpg objIncentive = new IncentiveSpg();
                objIncentive.SpgId = objSpg.ID;
                objIncentive.SpgCode = objSpg.Code;
                objIncentive.Month = DateTime.Now.Month;
                objIncentive.MonthName = DateTime.Now.ToString("MMMM");
                objIncentive.Incentive = long.Parse(incentiveValue);
                objIncentive.year = DateTime.Now.Year;
                objIncentive.NumberFirstSubmit = 1;
                objIncentive.Insert();
            }

            //insert di log
            Data.IncentiveSpgLog objIncentiveSpgLog = new IncentiveSpgLog();
            objIncentiveSpgLog.SpgCode = objSpg.Code;
            objIncentiveSpgLog.MemberId = member.ID;
            objIncentiveSpgLog.Type = "Spg Incentive";
            objIncentiveSpgLog.Incentive = long.Parse(incentiveValue);
            Data.Member objMember = Data.Member.GetById(member.ID);
            if (objMember != null)
            {
                objIncentiveSpgLog.MemberName = objMember.Name;
                objIncentiveSpgLog.phone = objMember.Phone;
            }
            objIncentiveSpgLog.receiptDate = receipt.TransactionDate;
            objIncentiveSpgLog.uploadDate = receipt.CreatedDate;
            objIncentiveSpgLog.remark = "[First Submission]";
            objIncentiveSpgLog.channel = receipt.Channel;
            objIncentiveSpgLog.ReceiptCode = receipt.ReceiptCode;
            objIncentiveSpgLog.Insert(by);


        }


        public static List<IncentiveModel> GetListIncentiveMonthlyBySpgCode(string spgCode)
        {
            try
            {
                List<IncentiveModel> obj = new List<IncentiveModel>();

                var incentiveObj = Data.IncentiveSpg.GetBySpgCode(spgCode);
                if (incentiveObj == null)
                {
                    return obj = null;
                }
                obj = IncentiveTranslator.Translated(incentiveObj.ToList());
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<IncentiveModel> GetListIncentiveMonthlyBySpgCodeOnlyDisplayed(string spgCode)
        {
            try
            {
                List<IncentiveModel> obj = new List<IncentiveModel>();

                var incentiveObj = Data.IncentiveSpg.GetBySpgCodeOnlyDisplayed(spgCode);
                if (incentiveObj == null)
                {
                    return obj = null;
                }
                obj = IncentiveTranslator.Translated(incentiveObj.ToList());
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateIncentiveSpg(List<IncentiveModel> listItem)
        {
            try
            {
                foreach (IncentiveModel item in listItem)
                {
                    IncentiveSpg obj = new IncentiveSpg();
                    obj.ID = item.ID;
                    obj.SpgId = item.SpgId;
                    obj.SpgCode = item.SpgCode;
                    obj.Month = item.Month;
                    obj.MonthName = item.MonthName;
                    obj.NumberFirstSubmit = item.NumberFirstSubmit;
                    obj.IsDisplayed = item.IsDisplayed ? 1 : 0;
                    obj.year = item.Year;
                    obj.Incentive = item.IncentiveThisMonth;
                    obj.Update();

                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DisplayAll()
        {
            try
            {
                List<IncentiveSpg> objAll = IncentiveSpg.GetByYear(DateTime.Now.Year).ToList();


                foreach (IncentiveSpg item in objAll)
                {
                    item.IsDisplayed = 1;
                    item.Update();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

