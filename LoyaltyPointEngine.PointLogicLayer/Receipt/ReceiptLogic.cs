﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Common.Notification;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.Model.Parameter.Notification;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.PointLogicLayer.CampaignLogic;
using LoyaltyPointEngine.PointLogicLayer.Incentive;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using LoyaltyPointEngine.PointLogicLayer.OCR;
using log4net;
using Newtonsoft.Json;

namespace LoyaltyPointEngine.PointLogicLayer
{
    public class ReceiptLogic : MainLogic
    {
        private static ILog Log = LogManager.GetLogger(typeof(ReceiptLogic));

        public ReceiptLogic()
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(ReceiptLogic));
            }
        }

        private static List<AutomaticCampaignParticipant> memberAutomatedCampaigns = new List<AutomaticCampaignParticipant>();
        public static ResultModel<ReceiptModel> Upload(UploadReceiptModel param, string by, int memberId, int poolID, bool IsMobileApp = false)
        {
            ResultModel<ReceiptModel> result = new ResultModel<ReceiptModel>();
            Member member = Member.GetById(memberId);
            DataMember.Member currentMember = DataMember.Member.GetById(memberId);
            int totalMemberReceipts = Receipt.GetByMemberId(memberId)
                .Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved);
            int isBelow1YearOrPregnant = MemberLogic.CheckMemberChildBelow1YearOrPregnant(member.ID);
            bool MemberIsActive = MemberLogic.CheckMemberIsVerify(member.ID);
            bool isContainSpecificAllowedProduct = param.Details.Any(x => SiteSetting.ChildAgeBelow1YearRuleAllowedActionList.Any(y => y == x.ActionID.ToString()));

            if (isBelow1YearOrPregnant == 3 && currentMember.StagesID.HasValue)
            {
                var stage = DataMember.Stages.GetById(currentMember.StagesID.Value);
                if (stage != null && stage.IsPregnant)
                {
                    isBelow1YearOrPregnant = 4;
                }
            }
            if (!MemberIsActive)  ///BLocking receipt Account Member Unverify
            {
                result.StatusCode = "50";
                result.StatusMessage = "Account Member not verify, Please verify account member";
                return result;
            }
            DateTime dt = DateTime.MinValue;
            if (param != null)
            {
                if (string.IsNullOrEmpty(param.TransactionDate))
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Silahkan isi kolom Tanggal Transaksi";
                    return result;
                }
                else
                {
                    if (!DateTime.TryParseExact(param.TransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        result.StatusCode = "41";
                        result.StatusMessage = "Tanggal Transaksi salah";
                        return result;
                    }
                }

                if (param.Details == null || param.Details.Count <= 0)
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Mohon lengkapi detail produk anda";
                    return result;
                }
                else
                {
                    foreach (var detail in param.Details)
                    {
                        if (detail.ActionID <= 0)
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Mohon lengkapi detail produk anda";
                            return result;
                        }

                        if (detail.Quantity <= 0)
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Mohon lengkapi detail produk anda";
                            return result;
                        }
                    }
                }

                if (param.Images != null && param.Images.Count > 0)
                {
                    foreach (var image in param.Images)
                    {
                        if (!image.IsBase64())
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Foto struk tidak valid.";
                            return result;
                        }
                    }

                    //Check if product is pregnancy product
                    foreach (var item in param.Details)
                    {
                        var action = Data.Action.GetById(item.ActionID);
                        if (action != null && action.ProductID.HasValue)
                        {
                            var product = Product.GetByID(action.ProductID.Value);
                            if (product != null)
                            {
                                if (!product.IsPregnancyProduct && !item.ChildID.HasValue)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Harap mengisi nama anak anda terlebih dahulu";
                                    return result;
                                }

                                //Check child birthdate
                                if (item.ChildID.HasValue)
                                {
                                    var child = DataMember.Child.GetById(item.ChildID.Value);
                                    if (child != null && child.Birthdate.HasValue)
                                    {
                                        if (((DateTime.Today.Subtract(child.Birthdate.Value)).TotalDays / 365) < 1)
                                        {
                                            isBelow1YearOrPregnant = 2;
                                            break;
                                        }
                                    }
                                }

                                if (product.IsPregnancyProduct && !currentMember.Stages.IsPregnant)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Kondisi Anda Belum Hamil. Silahkan Edit Profile Terlebih Dahulu";
                                    return result;
                                }
                            }
                        }
                    }

                    var code = Number.Generated(Number.Type.RCP, by);
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    Receipt data = null;
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        Guid NewId = Guid.NewGuid();
                        data = new Receipt();
                        data.ID = NewId;
                        data.MemberID = memberId;
                        data.RetailerAddress = param.RetailerAddress ?? string.Empty;
                        data.Status = param.IsOnline ? (int)Receipt.ReceiptStatus.Waiting : (int)Receipt.ReceiptStatus.OcrProcessing;

                        if (isBelow1YearOrPregnant < 4)
                        {
                            data.IsNeedReuploadData = false;
                            data.Status = (int)Receipt.ReceiptStatus.Reject;
                            data.RejectReason = MemberLogic.ReceiptRejectedMessage(isBelow1YearOrPregnant);
                        }

                        data.Code = code;
                        data.RetailerID = param.RetailerID;
                        if (param.EventCode != null)
                        {
                            data.EventCode = Convert.ToInt32(param.EventCode);
                        }

                        Channel tempChannel = Channel.Website;
                        if (string.IsNullOrEmpty(param.Channel) || Enum.TryParse<Channel>(param.Channel, out tempChannel))
                        {
                            data.Channel = Convert.ToString(Channel.Website);
                        }
                        if (IsMobileApp)
                        {
                            data.Channel = Convert.ToString(Channel.Mobile);
                        }
                        if (SiteSetting.OldMemberParameterDate.HasValue)
                        {
                            if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                            {
                                data.IsFirstSubmission = false;
                                if (totalMemberReceipts < 1)
                                    data.IsFirstSubmission = true;
                            }
                        }
                        DateTime dtTransactionDate = DateTime.Now;
                        if (!string.IsNullOrEmpty(param.TransactionDate))
                        {
                            if (DateTime.TryParseExact(param.TransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            {
                                data.TransactionDate = dt;
                                dtTransactionDate = dt;

                                Settings limitTransDateSetting = Data.Settings.GetByKey("LimitUploadReceiptTransactionDate");
                                if (limitTransDateSetting != null)
                                {
                                    int intLimit = 0; int.TryParse(limitTransDateSetting.Value, out intLimit);
                                    if (intLimit > 0)
                                    {
                                        DateTime limitDateUploadReceipt = DateTime.Now.AddMonths(-(intLimit));
                                        if (dtTransactionDate.Date < limitDateUploadReceipt.Date)
                                        {
                                            result.StatusCode = "41";
                                            result.StatusMessage = string.Format("Transaction date lower than limit date upload receipt");
                                            transScope.Dispose();
                                            return result;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result.StatusCode = "41";
                                result.StatusMessage = string.Format("Format transaction date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                                transScope.Dispose();
                                return result;
                            }
                        }

                        Campaign selectedCampaign = data.CampaignID.HasValue ? Campaign.GetById(data.CampaignID.Value) : null;
                        if (selectedCampaign != null)
                        {
                            if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                            {
                                if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(data.Channel)))
                                {
                                    data.CampaignID = selectedCampaign.ID;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(param.RequestDate))
                        {
                            if (DateTime.TryParseExact(param.RequestDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            {
                                data.RequestDate = dt;
                            }
                            else
                            {
                                result.StatusCode = "41";
                                result.StatusMessage = string.Format("Format request date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                                transScope.Dispose();
                                return result;
                            }
                        }
                        else
                            data.RequestDate = DateTime.Now;

                        #region Validation Fraud / Bot / Spam multiple upload struk
                        List<int> listActionID = (param.Details != null) ? param.Details.OrderBy(x => x.ActionID).Select(x => x.ActionID).ToList() : null;
                        int totalDuplicateData = Receipt.CheckTotalDuplicateReceipt(memberId, data.TransactionDate, null, listActionID);
                        if (totalDuplicateData >= 3)
                        {
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format("Can not save double receipt, your receipt data has been submitted more than 3 times");
                            transScope.Dispose();
                            return result;
                        }
                        #endregion

                        bool MemberIsIdle = false;
                        if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                        {
                            var monthsIdleSetting = Data.Settings.GetByKey("MemberIdle").Value;
                            var lastReceiptDate = Data.Receipt.GetByMemberId(data.MemberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.Status == 1);
                            if (lastReceiptDate != null)
                            {
                                var monthDifference = (DateTime.Now.Month - lastReceiptDate.CreatedDate.Month) + 12 * (DateTime.Now.Year - lastReceiptDate.CreatedDate.Year);
                                if (monthDifference >= Convert.ToInt32(monthsIdleSetting))
                                {
                                    MemberIsIdle = true;
                                }
                            }
                            var dataProductBefore = ProductBeforeLogic.GetLastProductBeforeByMemberId(data.MemberID);
                            data.ProductBefore = dataProductBefore;
                        }
                        data.IsOnline = param.IsOnline;

                        data.Inserted(by);

                        var resultImages = InsertingReceiptImage(param.Images, NewId, by);
                        if (resultImages.StatusCode == "00")
                        {
                            var resultActions = InsertedReceiptDetail(param.Details, NewId, by, member, poolID, dtTransactionDate, param.Remark, totalMemberReceipts, data.IsFirstSubmission, false, MemberIsIdle);
                            if (resultActions.StatusCode == "00")
                            {
                                result.Value = new ReceiptModel(data, Settings.GetValueByKey(SiteSetting.ASSET_URL));
                                result.StatusCode = "00";
                                result.StatusMessage = string.Format("Receipt success uploading");

                                if (SiteSetting.OldMemberParameterDate.HasValue)
                                {
                                    if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                                    {
                                        member.IsFirstSubmit = data.IsFirstSubmission;
                                        var memberDbResponse = member.Update(by, "Api", null);
                                        if (!memberDbResponse.Success)
                                        {
                                            result.StatusCode = "50";
                                            result.StatusMessage = memberDbResponse.ErrorMessage;
                                        }
                                    }
                                }

                                transScope.Complete();
                                transScope.Dispose();

                                if (result.StatusCode == "00")
                                {
                                    if (data.Status == (int)Receipt.ReceiptStatus.Waiting || data.Status == (int)Receipt.ReceiptStatus.OcrProcessing)
                                    {
                                        //NotificationLogic.Create(data.MemberID, Common.Notification.NotificationType.Type.OnProgress, "Upload Struk Berhasil", "No Transaksi: " + data.Code, data.Code, data.Collect.Sum(x => x.TotalPoints) , by);
                                        NotificationLogic.Create(data.MemberID, Common.Notification.NotificationType.Type.OnProgress, Settings.GetValueByKey(SiteSetting.UploadReceiptSuccessTitle), "No Transaksi: " + data.Code + " " + Settings.GetValueByKey(SiteSetting.UploadReceiptSuccessMsg), data.Code, data.Collect.Sum(x => x.TotalPoints), by);
                                        //send this receipt to snapcart
                                        if (!param.IsOnline)
                                        {
                                            OCRLogic.SendToSnapcart(data, by, string.Empty, memberId);
                                        }
                                    }
                                    else if (data.Status == (int)Receipt.ReceiptStatus.Reject)
                                    {
                                        result.StatusCode = "55";
                                        //result.StatusMessage = SiteSetting.ChildAgeBelow1YearRuleValidationMessage;
                                        result.StatusMessage = data.RejectReason;
                                    }
                                }
                            }
                            else
                            {
                                result.StatusCode = resultActions.StatusCode;
                                result.StatusMessage = resultActions.StatusMessage;
                                transScope.Dispose();
                            }
                        }
                        else
                        {
                            result.StatusCode = resultImages.StatusCode;
                            result.StatusMessage = resultImages.StatusMessage;
                            transScope.Dispose();
                        }
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = string.Format("Image is required");
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Parameter is null");
            }
            return result;

        }

        public static ResultModel<ReceiptModel> PublicUpload(PublicUploadReceiptModel param, Pool pool)
        {
            ResultModel<ReceiptModel> result = new ResultModel<ReceiptModel>();
            bool isExpired = false;

            #region TransactionDate Validation
            if (string.IsNullOrEmpty(param.TransactionDate))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Transaction Date is required.";
                return result;
            }

            DateTime _transactionDate = DateTime.MinValue;
            if (!DateTime.TryParseExact(param.TransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out _transactionDate))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Transaction Date is invalid. Use this format instead \"dd/MM/yyyy HH:mm:ss\"";
                return result;
            }

            if (_transactionDate > DateTime.Now)
            {
                result.StatusCode = "41";
                result.StatusMessage = "Transaction Date is invalid. Transaction Date can not be bigger dan today.";
                return result;
            }

            Settings limitTransDateSetting = Data.Settings.GetByKey("LimitUploadReceiptTransactionDate");
            if (limitTransDateSetting != null)
            {
                int intLimit = 0; int.TryParse(limitTransDateSetting.Value, out intLimit);
                if (intLimit > 0)
                {
                    DateTime limitDateUploadReceipt = DateTime.Now.AddMonths(-(intLimit));
                    if (_transactionDate.Date < limitDateUploadReceipt.Date)
                    {
                        //result.StatusCode = "41";
                        //result.StatusMessage = "Transaction Date has exceeded the Transaction Date limit.";
                        //return result;

                        isExpired = true;
                    }
                }
            }
            #endregion

            #region Images Validation
            if (param.Images != null)
            {
                for (var i = 0; i < param.Images.Count; i++)
                {
                    var image = param.Images[i];
                    if (!image.IsBase64())
                    {
                        result.StatusCode = "41";
                        result.StatusMessage = string.Format("Image[{0}] is invalid base64 string.");
                        return result;
                    }
                }
            }
            #endregion

            #region Member Validation
            Member member = null;
            if (string.IsNullOrEmpty(param.MemberPhone))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Member Phone is required.";
                return result;
            }

            member = Member.GetByPhone(param.MemberPhone);

            if (member == null)
            {
                result.StatusCode = "41";
                result.StatusMessage = string.Format("Member with phone number {0} is not exist", param.MemberPhone);
                return result;
            }
            #endregion

            #region Details Validation
            if (param.Details == null || param.Details.Count <= 0)
            {
                result.StatusCode = "41";
                result.StatusMessage = "Details is required.";
                return result;
            }

            for (var i = 0; i < param.Details.Count; i++)
            {
                var detail = param.Details[i];
                if (!Action.ActionLogic.IsExist(detail.ActionID))
                {
                    result.StatusCode = "41";
                    result.StatusMessage = string.Format("Action Id of Detail[{0}] is not exist.", i);
                    return result;
                }

                if (detail.Quantity <= 0)
                {
                    result.StatusCode = "41";
                    result.StatusMessage = string.Format("Quantity of Detail[{0}] is required.", i);
                    return result;
                }
            }
            #endregion

            #region Validation Fraud / Bot / Spam multiple upload struk
            List<int> listActionID = (param.Details != null) ? param.Details.OrderBy(x => x.ActionID).Select(x => x.ActionID).ToList() : null;
            int totalDuplicateData = Receipt.CheckTotalDuplicateReceipt(member.ID, _transactionDate, null, listActionID);
            if (totalDuplicateData >= 3)
            {
                result.StatusCode = "41";
                result.StatusMessage = "Receipt is duplicated. Duplicate receipt limit only to 3.";
                return result;
            }
            #endregion
            bool MemberIsActive = MemberLogic.CheckMemberIsVerify(member.ID);
            LoyaltyPointEngine.DataMember.Member dataMember = DataMember.Member.GetById(member.ID);
            int isBelow1YearOrPregnant = MemberLogic.CheckMemberChildBelow1YearOrPregnant(member.ID);
            bool isContainSpecificAllowedProduct = param.Details.Any(x => SiteSetting.ChildAgeBelow1YearRuleAllowedActionList.Any(y => y == x.ActionID.ToString()));
            int totalMemberReceipts = Receipt.GetByMemberId(member.ID).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved);
            if (!MemberIsActive)  ///BLocking receipt Account Member Unverify
            {
                result.StatusCode = "50";
                result.StatusMessage = "Account Member not verify, Please verify account member";
                return result;
            }

            string assetUrl = Settings.GetValueByKey(SiteSetting.ASSET_URL);

            if (isBelow1YearOrPregnant == 3 && dataMember.StagesID.HasValue)
            {
                var stage = DataMember.Stages.GetById(dataMember.StagesID.Value);
                if (stage != null && stage.IsPregnant)
                {
                    isBelow1YearOrPregnant = 4;
                }
            }

            //Check if product is pregnancy product
            bool detailsInvalid = false;
            foreach (var item in param.Details)
            {
                if (item.ActionID > 0)
                {
                    var action = Data.Action.GetById(item.ActionID);
                    if (action != null && action.ProductID.HasValue)
                    {
                        var product = Product.GetByID(action.ProductID.Value);
                        if (product != null)
                        {
                            if (param.Channel.ToLower() != "suzuya") //SMB-260 (Pengecekan anak untuk channel selain dari Suzuya)
                            {
                                if (!product.IsPregnancyProduct && !item.ChildID.HasValue)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Harap mengisi nama anak anda terlebih dahulu";
                                    return result;
                                }
                            }

                            //Check children birthdate
                            if (item.ChildID.HasValue)
                            {
                                var child = DataMember.Child.GetById(item.ChildID.Value);
                                if (child != null && child.Birthdate.HasValue)
                                {
                                    if (((DateTime.Today.Subtract(child.Birthdate.Value)).TotalDays / 365) < 1)
                                    {
                                        isBelow1YearOrPregnant = 2;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!detailsInvalid)
            {
                isBelow1YearOrPregnant = 4;
            }

            var code = Number.Generated(Number.Type.RCP, pool.PoolName);
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            Receipt data = null;
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                Guid NewId = Guid.NewGuid();
                data = new Receipt();
                data.ID = NewId;
                data.MemberID = member.ID;
                data.RetailerAddress = param.RetailerAddress ?? string.Empty;
                data.ReceiptCode = param.ReceiptCode;
                data.Code = code;
                data.TransactionDate = _transactionDate;
                data.RetailerID = param.RetailerID;
                data.Channel = param.Channel;
                data.RequestDate = DateTime.Now;
                data.IsFirstSubmission = false;
                data.Status = (int)Receipt.ReceiptStatus.Approve;

                if (isBelow1YearOrPregnant < 4 && !isContainSpecificAllowedProduct)
                {
                    data.Status = (int)Receipt.ReceiptStatus.Reject;
                    data.RejectReason = MemberLogic.ReceiptRejectedMessage(isBelow1YearOrPregnant);
                }

                if (isExpired)
                {
                    data.Status = (int)Receipt.ReceiptStatus.Reject;
                    data.RejectReason = SiteSetting.ReceiptExpiredValidationMessage.Replace("[xx]", limitTransDateSetting.Value);
                }

                if (SiteSetting.OldMemberParameterDate.HasValue)
                {
                    if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                    {
                        if (totalMemberReceipts < 1)
                            data.IsFirstSubmission = true;
                    }
                }

                bool MemberIsIdle = false;
                if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                {
                    var monthsIdleSetting = Data.Settings.GetByKey("MemberIdle").Value;
                    var lastReceiptDate = Data.Receipt.GetByMemberId(data.MemberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.Status == 1);
                    if (lastReceiptDate != null)
                    {
                        var monthDifference = (DateTime.Now.Month - lastReceiptDate.CreatedDate.Month) + 12 * (DateTime.Now.Year - lastReceiptDate.CreatedDate.Year);
                        if (monthDifference >= Convert.ToInt32(monthsIdleSetting))
                        {
                            MemberIsIdle = true;
                        }
                    }

                    var lastProductBefore = ProductBeforeLogic.GetLastProductBeforeByMemberId(data.MemberID);
                    data.ProductBefore = lastProductBefore;
                }

                data.Inserted(pool.PoolName);

                var resultImages = InsertingReceiptImage(param.Images, NewId, pool.PoolName);
                if (resultImages.StatusCode == "00")
                {
                    var resultActions = InsertedReceiptDetail(param.Details, NewId, pool.PoolName, member, pool.ID, _transactionDate, param.Remark, totalMemberReceipts, data.IsFirstSubmission, true, MemberIsIdle);
                    if (resultActions.StatusCode == "00")
                    {
                        result.Value = new ReceiptModel(data, assetUrl);
                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("Receipt success uploading");
                        if (SiteSetting.OldMemberParameterDate.HasValue)
                        {
                            if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                            {
                                member.IsFirstSubmit = data.IsFirstSubmission;
                                var memberDbResponse = member.Update(pool.PoolName, "Api", null);
                                if (!memberDbResponse.Success)
                                {
                                    result.StatusCode = "50";
                                    result.StatusMessage = memberDbResponse.ErrorMessage;
                                }
                            }
                        }
                        transScope.Complete();
                        transScope.Dispose();
                    }
                    else
                    {
                        result.StatusCode = resultActions.StatusCode;
                        result.StatusMessage = resultActions.StatusMessage;
                        transScope.Dispose();
                    }
                }
                else
                {
                    result.StatusCode = resultImages.StatusCode;
                    result.StatusMessage = resultImages.StatusMessage;
                    transScope.Dispose();
                }
            }
            if (result.StatusCode == "00")
            {
                if (data.Status == (int)Receipt.ReceiptStatus.Waiting)
                {
                    var receiptTotalPoints = Data.Collect.GetAllByReceiptID(data.ID).Sum(x => x.TotalPoints);
                    NotificationLogic.Create(data.MemberID, Common.Notification.NotificationType.Type.Approved, "Upload Struk Berhasil", "No Transaksi: " + data.Code, data.Code, receiptTotalPoints, pool.PoolName);
                }
                else if (data.Status == (int)Receipt.ReceiptStatus.Reject)
                {
                    if (isExpired)
                    {
                        result.StatusCode = "56";
                        result.StatusMessage = SiteSetting.ReceiptExpiredValidationMessage.Replace("[xx]", limitTransDateSetting.Value);
                    }
                    else
                    {
                        result.StatusCode = "55";
                        result.StatusMessage = SiteSetting.ChildAgeBelow1YearRuleValidationMessage;
                    }
                }
            }
            return result;
        }

        public static ResultModel<ReceiptDetailModel> UploadByOCR(UploadReceiptModel param, string by, int memberId, int poolID)
        {
            ResultModel<ReceiptDetailModel> result = new ResultModel<ReceiptDetailModel>();
            Member member = Member.GetById(memberId);
            int totalMemberReceipts = Receipt.GetByMemberId(memberId).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved);
            int isBelow1YearOrPregnant = MemberLogic.CheckMemberChildBelow1YearOrPregnant(member.ID);
            bool isContainSpecificAllowedProduct = param.Details.Any(x => SiteSetting.ChildAgeBelow1YearRuleAllowedActionList.Any(y => y == x.ActionID.ToString()));
            bool MemberIsActive = MemberLogic.CheckMemberIsVerify(member.ID);
            bool withBalanceLog = true;

            if (!MemberIsActive)  ///BLocking receipt Account Member Unverify
            {
                result.StatusCode = "50";
                result.StatusMessage = "Account Member not verify, Please verify account member";
                return result;
            }

            if (param != null)
            {
                if (!string.IsNullOrEmpty(param.ReceiptCode))
                {
                    Receipt predictedReceipt = Receipt.GetByReceiptCode(param.ReceiptCode).FirstOrDefault();
                    if (predictedReceipt != null)
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("Kode struk {0} sudah terdaftar", param.ReceiptCode);
                        return result;
                    }
                }

                if (param.Images != null && param.Images.Count > 0)
                {
                    var code = Number.Generated(Number.Type.RCP, by);
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        Guid NewId = Guid.NewGuid();
                        Receipt data = new Receipt();
                        data.ID = NewId;
                        data.MemberID = memberId;
                        data.RetailerAddress = param.RetailerAddress ?? string.Empty;
                        data.Status = (int)Receipt.ReceiptStatus.Approve;

                        if (isBelow1YearOrPregnant < 4)
                        {
                            withBalanceLog = false;
                            data.IsNeedReuploadData = false;
                            data.Status = (int)Receipt.ReceiptStatus.Reject;
                            data.RejectReason = MemberLogic.ReceiptRejectedMessage(isBelow1YearOrPregnant);
                        }

                        data.Code = code;
                        data.Channel = Convert.ToString(Channel.Mobile);
                        if (SiteSetting.OldMemberParameterDate.HasValue)
                        {
                            if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                            {
                                data.IsFirstSubmission = false;
                                if (totalMemberReceipts < 1)
                                    data.IsFirstSubmission = true;
                            }
                        }
                        DateTime dtTransactionDate = DateTime.Now;
                        if (!string.IsNullOrEmpty(param.TransactionDate))
                        {
                            DateTime dt = DateTime.MinValue;
                            if (DateTime.TryParseExact(param.TransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            {
                                data.TransactionDate = dt;
                                dtTransactionDate = dt;

                                Settings limitTransDateSetting = Data.Settings.GetByKey("LimitUploadReceiptTransactionDate");
                                if (limitTransDateSetting != null)
                                {
                                    int intLimit = 0; int.TryParse(limitTransDateSetting.Value, out intLimit);
                                    if (intLimit > 0)
                                    {
                                        DateTime limitDateUploadReceipt = DateTime.Now.AddMonths(-(intLimit));
                                        if (dtTransactionDate.Date < limitDateUploadReceipt.Date)
                                        {
                                            result.StatusCode = "41";
                                            result.StatusMessage = string.Format("Transaction date lower than limit date upload receipt");
                                            transScope.Dispose();
                                            return result;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result.StatusCode = "41";
                                result.StatusMessage = string.Format("Format date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                                transScope.Dispose();
                                return result;
                            }
                        }
                        data.RequestDate = DateTime.Now;
                        Campaign selectedCampaign = data.CampaignID.HasValue ? Campaign.GetById(data.CampaignID.Value) : null;
                        if (selectedCampaign != null)
                        {
                            if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(data.Channel)))
                            {
                                data.CampaignID = selectedCampaign.ID;
                            }
                        }
                        bool MemberIsIdle = false;
                        if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                        {
                            var monthsIdleSetting = Data.Settings.GetByKey("MemberIdle").Value;
                            var lastReceiptDate = Data.Receipt.GetByMemberId(data.MemberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.Status == 1);
                            if (lastReceiptDate != null)
                            {
                                var monthDifference = (DateTime.Now.Month - lastReceiptDate.CreatedDate.Month) + 12 * (DateTime.Now.Year - lastReceiptDate.CreatedDate.Year);
                                if (monthDifference >= Convert.ToInt32(monthsIdleSetting))
                                {
                                    MemberIsIdle = true;
                                }
                            }
                        }
                        if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                        {
                            var dataProductBefore = ProductBeforeLogic.GetLastProductBeforeByMemberId(data.MemberID);
                            data.ProductBefore = dataProductBefore;
                        }
                        data.Inserted(by);
                        var resultImages = InsertingReceiptImage(param.Images, NewId, by);
                        if (resultImages.StatusCode == "00")
                        {
                            var resultActions = InsertedReceiptDetail(param.Details, NewId, by, member, poolID, dtTransactionDate, param.Remark, totalMemberReceipts, data.IsFirstSubmission, true, MemberIsIdle);
                            if (resultActions.StatusCode == "00")
                            {
                                result.Value = new ReceiptDetailModel(data, Settings.GetValueByKey(SiteSetting.ASSET_URL));
                                result.StatusCode = "00";
                                result.StatusMessage = string.Format("Receipt success uploading");
                                if (SiteSetting.OldMemberParameterDate.HasValue)
                                {
                                    if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                                    {
                                        member.IsFirstSubmit = data.IsFirstSubmission;
                                        var memberDbResponse = member.Update(by, "Api", null);
                                        if (!memberDbResponse.Success)
                                        {
                                            result.StatusCode = "50";
                                            result.StatusMessage = memberDbResponse.ErrorMessage;
                                        }
                                    }
                                }
                                transScope.Complete();
                                transScope.Dispose();
                                if (result.StatusCode == "00")
                                {
                                    if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                                    {
                                        BonusPointForChannelFirstSubmission(member, data.Channel);
                                        NotificationLogic.Create(data.MemberID, Common.Notification.NotificationType.Type.OnProgress, "Penambahan Point", "No Transaksi: " + data.Code, data.Code, data.Collect.Sum(x => x.TotalPoints), by);

                                        // ambil list automated campaign dimana member ini terdaftar
                                        memberAutomatedCampaigns = (from P in AutomaticCampaignParticipant.GetAll(false)
                                                                    join C in Campaign.GetAll() on P.CampaignID equals C.ID
                                                                    where P.IsActive && C.IsAutomate && P.MemberId == member.ID && (C.StartDate <= data.TransactionDate && data.TransactionDate <= C.EndDate)
                                                                    select P).ToList();

                                        foreach (var collect in data.Collect)
                                        {
                                            CalculateAutomatedCampaign(collect, collect.Action, member, data);
                                        }
                                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(data.MemberID, by);
                                    }
                                    else if (data.Status == (int)Receipt.ReceiptStatus.Reject)
                                    {
                                        result.StatusCode = "55";
                                        result.StatusMessage = SiteSetting.ChildAgeBelow1YearRuleValidationMessage;
                                    }
                                }
                            }
                            else
                            {
                                result.StatusCode = resultActions.StatusCode;
                                result.StatusMessage = resultActions.StatusMessage;
                                transScope.Dispose();
                            }
                        }
                        else
                        {
                            result.StatusCode = resultImages.StatusCode;
                            result.StatusMessage = resultImages.StatusMessage;
                            transScope.Dispose();
                        }
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = string.Format("Image is required");
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Parameter is null");
            }
            return result;
        }

        public static ResultModel<ReceiptModel> ReUpload(ReUploadReceiptModel param, string by, int memberId, int poolID, string url)
        {
            DateTime dt = DateTime.MinValue;
            ResultModel<ReceiptModel> result = new ResultModel<ReceiptModel>();
            if (param != null)
            {
                if (string.IsNullOrEmpty(param.TransactionDate))
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Silahkan isi kolom Tanggal Transaksi";
                    return result;
                }
                else
                {
                    if (!DateTime.TryParseExact(param.TransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        result.StatusCode = "41";
                        result.StatusMessage = "Tanggal Transaksi salah";
                        return result;
                    }
                }

                if (param.Details == null || param.Details.Count <= 0)
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Mohon lengkapi detail produk anda";
                    return result;
                }
                else
                {
                    foreach (var detail in param.Details)
                    {
                        if (detail.ActionID <= 0)
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Mohon lengkapi detail produk anda";
                            return result;
                        }

                        if (detail.Quantity <= 0)
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Mohon lengkapi detail produk anda";
                            return result;
                        }
                    }
                }

                if (param.Images != null && param.Images.Count > 0)
                {
                    foreach (var image in param.Images)
                    {
                        if (!image.IsBase64())
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Foto struk tidak valid.";
                            return result;
                        }
                    }
                    var member = Member.GetById(memberId);
                    DataMember.Member currentMember = DataMember.Member.GetById(memberId);
                    int isBelow1YearOrPregnant = MemberLogic.CheckMemberChildBelow1YearOrPregnant(member.ID);
                    bool isContainSpecificAllowedProduct = param.Details.Any(x => SiteSetting.ChildAgeBelow1YearRuleAllowedActionList.Any(y => y == x.ActionID.ToString()));
                    int totalMemberReceipts = Receipt.GetByMemberId(memberId).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved);
                    bool MemberIsActive = MemberLogic.CheckMemberIsVerify(member.ID);
                    if (!MemberIsActive)  ///BLocking receipt Account Member Unverify
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = "Account Member not verify, Please verify account member";
                        return result;
                    }

                    if (isBelow1YearOrPregnant == 3 && currentMember.StagesID.HasValue)
                    {
                        var stage = DataMember.Stages.GetById(currentMember.StagesID.Value);
                        if (stage != null && stage.IsPregnant)
                        {
                            isBelow1YearOrPregnant = 4;
                        }
                    }

                    //Check if product is pregnancy product
                    foreach (var item in param.Details)
                    {
                        var action = Data.Action.GetById(item.ActionID);
                        if (action != null && action.ProductID.HasValue)
                        {
                            var product = Product.GetByID(action.ProductID.Value);
                            if (product != null)
                            {
                                if (!product.IsPregnancyProduct && !item.ChildID.HasValue)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Harap mengisi nama anak anda terlebih dahulu";
                                    return result;
                                }

                                //Check child birthdate
                                if (item.ChildID.HasValue)
                                {
                                    var child = DataMember.Child.GetById(item.ChildID.Value);
                                    if (child != null && child.Birthdate.HasValue)
                                    {
                                        if (((DateTime.Today.Subtract(child.Birthdate.Value)).TotalDays / 365) < 1)
                                        {
                                            isBelow1YearOrPregnant = 2;
                                            break;
                                        }
                                    }
                                }

                                if (product.IsPregnancyProduct && !currentMember.Stages.IsPregnant)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Kondisi Anda Belum Hamil. Silahkan Edit Profile Terlebih Dahulu";
                                    return result;
                                }
                            }
                        }
                    }

                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        var data = Receipt.GetById(param.ReceiptID);
                        if (data != null)
                        {
                            if (data.IsNeedReuploadData)
                            {
                                //data.ID = NewId;
                                data.MemberID = member.ID;
                                data.Status = (int)Receipt.ReceiptStatus.Waiting;
                                data.IsNeedReuploadData = false;
                                if (isBelow1YearOrPregnant < 4)
                                {
                                    data.IsNeedReuploadData = false;
                                    data.Status = (int)Receipt.ReceiptStatus.Reject;
                                    data.RejectReason = MemberLogic.ReceiptRejectedMessage(isBelow1YearOrPregnant);
                                }
                                //data.Code = code;

                                DateTime dtTransactionDate = DateTime.Now;
                                if (!string.IsNullOrEmpty(param.TransactionDate))
                                {
                                    if (DateTime.TryParseExact(param.TransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                                    {
                                        data.TransactionDate = dt;
                                        dtTransactionDate = dt;

                                        Settings limitTransDateSetting = Data.Settings.GetByKey("LimitUploadReceiptTransactionDate");
                                        if (limitTransDateSetting != null)
                                        {
                                            int intLimit = 0; int.TryParse(limitTransDateSetting.Value, out intLimit);
                                            if (intLimit > 0)
                                            {
                                                DateTime limitDateUploadReceipt = DateTime.Now.AddMonths(-(intLimit));
                                                if (dtTransactionDate.Date < limitDateUploadReceipt.Date)
                                                {
                                                    result.StatusCode = "41";
                                                    result.StatusMessage = string.Format("Transaction date lower than limit date upload receipt");
                                                    transScope.Dispose();
                                                    return result;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result.StatusCode = "41";
                                        result.StatusMessage = string.Format("Format date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                                        transScope.Dispose();
                                        return result;
                                    }
                                }

                                Campaign selectedCampaign = data.CampaignID.HasValue ? Campaign.GetById(data.CampaignID.Value) : null;
                                if (selectedCampaign != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                                    {
                                        if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(data.Channel)))
                                        {
                                            data.CampaignID = selectedCampaign.ID;
                                        }
                                    }
                                }

                                data.Update(by, url, null);

                                var resultDeletingImages = DeletingReceiptImages(data.ID, by, url);
                                if (resultDeletingImages.StatusCode == "00")
                                {
                                    var resultImages = InsertingReceiptImage(param.Images, data.ID, by);
                                    if (resultImages.StatusCode == "00")
                                    {

                                        var resultDeleting = DeletingReceiptDetail(data.ID, by, url);
                                        if (resultDeleting.StatusCode == "00")
                                        {
                                            var resultActions = InsertedReceiptDetail(param.Details, data.ID, by, member, poolID, dtTransactionDate, param.Remark, totalMemberReceipts);
                                            if (resultActions.StatusCode == "00")
                                            {
                                                result.Value = new ReceiptModel(data, Settings.GetValueByKey(SiteSetting.ASSET_URL));
                                                result.StatusCode = "00";
                                                result.StatusMessage = string.Format("Receipt success re-uploading");

                                                transScope.Complete();
                                                transScope.Dispose();

                                                if (data.Status == (int)Receipt.ReceiptStatus.Waiting)
                                                {
                                                    var collects = Data.Collect.GetAllByReceiptID(data.ID).ToList();
                                                    var predictedNotification = NotificationLogic.GetByReferenceCode(data.Code, memberId)
                                                        .OrderByDescending(x => x.CreatedDate)
                                                        .FirstOrDefault(x => x.Type == Convert.ToString(Common.Notification.NotificationType.Type.RejectReupload));
                                                    if (predictedNotification != null)
                                                    {
                                                        predictedNotification.IsDeleted = true;
                                                        predictedNotification.Updated(by);
                                                    }

                                                    NotificationLogic.Create(data.MemberID, Common.Notification.NotificationType.Type.OnProgress, "Upload Struk Berhasil", "No Transaksi: " + data.Code, data.Code, collects != null ? collects.Sum(x => x.TotalPoints) : 0, by);
                                                    //send this receipt to snapcart
                                                    if (!param.IsOnline)
                                                    {
                                                        OCRLogic.SendToSnapcart(data, by, string.Empty, memberId);
                                                    }
                                                }
                                                else if (data.Status == (int)Receipt.ReceiptStatus.Reject)
                                                {
                                                    result.StatusCode = "55";
                                                    result.StatusMessage = SiteSetting.ChildAgeBelow1YearRuleValidationMessage;
                                                }
                                            }
                                            else
                                            {
                                                result.StatusCode = resultActions.StatusCode;
                                                result.StatusMessage = resultActions.StatusMessage;
                                                transScope.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            result.StatusCode = resultDeleting.StatusCode;
                                            result.StatusMessage = resultDeleting.StatusMessage;
                                            transScope.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        result.StatusCode = resultImages.StatusCode;
                                        result.StatusMessage = resultImages.StatusMessage;
                                        transScope.Dispose();
                                    }
                                }
                                else
                                {
                                    result.StatusCode = resultDeletingImages.StatusCode;
                                    result.StatusMessage = resultDeletingImages.StatusMessage;
                                    transScope.Dispose();
                                }
                            }
                            else
                            {
                                result.StatusCode = "80";
                                result.StatusMessage = string.Format("Receipt can not ReUpload, please contact your administrator");
                                transScope.Dispose();
                            }
                        }
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = string.Format("ReceiptID  '{0}' is not found in system", param.ReceiptID);
                            transScope.Dispose();
                        }
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = string.Format("Image is required");
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Parameter is null");
            }
            return result;
        }

        private static ResultModel<bool> InsertingReceiptImage(List<string> images, Guid ReceiptID, string by)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            string RelativePathReceipt = Settings.GetValueByKey(SiteSetting.RelativePathReceipt);
            RelativePathReceipt = RelativePathReceipt + "/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString("00") + "/";
            if (RelativePathReceipt.Substring(RelativePathReceipt.Length - 1, 1).Contains("/"))
            {
                RelativePathReceipt = RelativePathReceipt + ReceiptID + "/";
            }
            else
            {
                RelativePathReceipt = RelativePathReceipt + "/" + ReceiptID + "/";
            }



            foreach (var _image in images)
            {
                if (!string.IsNullOrEmpty(_image))
                {
                    if (StringHelper.IsBase64String(_image))
                    {


                        string MediaLocalImageDirectory = Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryReceipt);
                        MediaLocalImageDirectory = MediaLocalImageDirectory + "\\" + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString("00") + "\\";
                        //generate path with year and month
                        System.IO.Directory.CreateDirectory(MediaLocalImageDirectory);
                        if (MediaLocalImageDirectory.Substring(MediaLocalImageDirectory.Length - 1, 1).Contains("\\"))
                        {
                            MediaLocalImageDirectory = MediaLocalImageDirectory + ReceiptID + "\\";
                        }
                        else
                        {
                            MediaLocalImageDirectory = MediaLocalImageDirectory + "\\" + ReceiptID + "\\";
                        }


                        if (!string.IsNullOrEmpty(MediaLocalImageDirectory))
                        {
                            if (!Directory.Exists(MediaLocalImageDirectory))
                            {
                                DirectoryInfo di = Directory.CreateDirectory(Path.Combine(MediaLocalImageDirectory));
                            }


                            Guid imageID = Guid.NewGuid();
                            string filename = string.Format("{0}.jpg", imageID);
                            string imageFulleName = MediaLocalImageDirectory.Substring(MediaLocalImageDirectory.Length - 1, 1).Contains("\\") ?
                                string.Format("{0}{1}", MediaLocalImageDirectory, filename) :
                                string.Format("{0}\\{1}", MediaLocalImageDirectory, filename);


                            if (File.Exists(imageFulleName))
                            {
                                File.Delete(imageFulleName);
                            }


                            ImageHelper.SaveImageFromBase64(_image, imageFulleName, 50L);

                            //inserted images to db
                            var data = new ReceiptImage();
                            data.ID = imageID;
                            data.ReceiptID = ReceiptID;
                            data.Image = RelativePathReceipt + filename;
                            data.Insert(by, by, null);



                        }
                        else
                        {
                            result.StatusCode = "40";
                            result.StatusMessage = string.Format("configuration 'MediaLocalImageDirectory' is required, please contact your administrator to setup this configuration");
                            return result;
                        }
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.StatusMessage = string.Format("Image is not base64 string");
                        return result;
                    }
                }
                else
                {
                    result.StatusCode = "31";
                    result.StatusMessage = string.Format("Image is can not empty string");
                    return result;
                }


            }//end foreach


            result.StatusCode = "00";
            result.StatusMessage = "Success inserted receipt images";
            result.Value = true;

            return result;
        }

        private static ResultModel<bool> InsertedReceiptDetail(List<UploadReceiptDetail> Details, Guid ReceiptID, string by, Member member, int poolID, DateTime TransactionDate, string remark, int totalMemberReceipts = 1, bool isFirstSubmission = false, bool saveWithBalance = false, bool memberIsIdle = false)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            var receipt = Receipt.GetById(ReceiptID);
            if (Details != null && Details.Count > 0)
            {
                Collect.CollectLogic colLogic = new Collect.CollectLogic();
                int timeStamp = 0;
                foreach (var detail in Details)
                {
                    var data = new Data.Collect();

                    var checkAction = Data.Action.GetById(detail.ActionID);
                    if (checkAction != null)
                    {
                        var calculatePOint = ActionLogic.GetPointByAction(checkAction);
                        data.ID = Guid.NewGuid();
                        data.ActionID = checkAction.ID;
                        data.TransactionDate = TransactionDate;
                        data.MemberID = member.ID;
                        data.ReceiptID = ReceiptID;
                        //data.CampaignID = calculatePOint.IsCampaign ? calculatePOint.CampaignID : null;
                        data.PoolID = poolID;
                        data.Quantity = detail.Quantity;
                        data.QuantityApproved = detail.Quantity;
                        data.Points = calculatePOint != null && calculatePOint.Points > 0 ? calculatePOint.Points : checkAction.Points;
                        data.TotalPoints = data.Points * detail.Quantity;
                        data.IsApproved = false;
                        data.IsActive = true;
                        data.IsUsed = false;
                        data.Remarks = remark;
                        data.ChildID = detail.ChildID;
                        if (calculatePOint.IsCampaign)
                        {
                            Campaign selectedCampaign = calculatePOint.CampaignID.HasValue ? Campaign.GetById(data.CampaignID.Value) : null;
                            if (selectedCampaign != null)
                            {
                                if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                                {
                                    if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(receipt.Channel)))
                                    {
                                        data.CampaignID = selectedCampaign.ID;
                                    }
                                }
                            }
                        }

                        if (saveWithBalance)
                        {
                            CollectCMSModel colModel = new CollectCMSModel();
                            colModel.MemberID = data.MemberID;
                            colModel.MemberName = member.Name;
                            colModel.ActionID = data.ActionID;
                            colModel.Quantity = data.Quantity;
                            colModel.QuantityApproved = data.QuantityApproved;
                            colModel.Points = data.Points;
                            colModel.TotalPoints = data.TotalPoints;
                            colModel.TransactionDate = data.TransactionDate.ToString("dd/MM/yyyy");
                            colModel.TransactionTime = data.TransactionDate.ToString("HH:mm:ss");
                            colModel.ReceiptID = ReceiptID;
                            colModel.CampaignID = data.CampaignID;
                            colModel.Remarks = data.Remarks ?? "Receipt Detail from " + ReceiptID;
                            colModel.ItemPrice = data.ItemPrice;
                            colModel.ChildID = data.ChildID;
                            //if (SiteSetting.OldMemberParameterDate.HasValue)
                            //{
                            //    if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                            //    {
                            //        if (isFirstSubmission)
                            //            colModel.Remarks = "[First Submission] " + colModel.Remarks;
                            //    }
                            //}
                            var json = JsonConvert.SerializeObject(colModel);
                            Log.Info("Data Collect = " + json);
                            colLogic.InsertCollect(colModel, poolID, member.Name, member.ID, "", timeStamp, totalMemberReceipts);
                        }
                        else
                        {
                            //if (SiteSetting.OldMemberParameterDate.HasValue)
                            //{
                            //    if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                            //    {
                            //        if (isFirstSubmission)
                            //            data.Remarks = "[First Submission] " + remark;
                            //    }
                            //}
                            var json = JsonConvert.SerializeObject(data);
                            Log.Info("Data Collect = " + json);
                            data.Inserted(by, timeStamp);
                        }

                        timeStamp++;
                    }
                    else
                    {
                        result.StatusCode = "80";
                        result.StatusMessage = string.Format("ActionID {0} not found in system", detail.ActionID);
                        return result;
                    }
                }
            }

            result.StatusCode = "00";
            result.StatusMessage = "Success inserted receipt details";
            result.Value = true;

            return result;
        }

        private static ResultModel<bool> DeletingReceiptDetail(Guid ReceiptID, string by, string url)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            var datas = Data.Collect.GetAllByReceiptID(ReceiptID).ToList();
            if (datas != null && datas.Count > 0)
            {
                foreach (var item in datas)
                {
                    item.IsDeleted = true;
                    item.IsActive = false;
                    item.IsApproved = false;
                    var response = item.Update(by, url, null);
                    if (!response.Success)
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = "Error when update collect to deleted: " + response.ErrorEntity;
                        result.Value = false;
                        return result;
                    }
                }
            }

            result.StatusCode = "00";
            result.StatusMessage = "Success deleting receipt details";
            result.Value = true;

            return result;
        }

        private static ResultModel<bool> DeletingReceiptImages(Guid ReceiptID, string by, string url)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            var datas = Data.ReceiptImage.GetAllByReceiptID(ReceiptID).ToList();
            if (datas != null && datas.Count > 0)
            {
                foreach (var item in datas)
                {


                    var response = item.HardDeleted();
                    if (!response.Success)
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = "Error when deleting Receipt Images : " + response.ErrorEntity;
                        result.Value = false;
                        return result;
                    }
                    else
                    {

                        #region in Case butuh hapus fisik


                        ////////string MediaLocalImageDirectory = Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryReceipt);
                        ////////if (MediaLocalImageDirectory.Substring(MediaLocalImageDirectory.Length - 1, 1).Contains("\\"))
                        ////////{
                        ////////    MediaLocalImageDirectory = MediaLocalImageDirectory + ReceiptID + "\\";
                        ////////}
                        ////////else
                        ////////{
                        ////////    MediaLocalImageDirectory = MediaLocalImageDirectory + "\\" + ReceiptID + "\\";
                        ////////}


                        ////////if (!string.IsNullOrEmpty(MediaLocalImageDirectory))
                        ////////{
                        ////////    if (!Directory.Exists(MediaLocalImageDirectory))
                        ////////    {
                        ////////        DirectoryInfo di = Directory.CreateDirectory(Path.Combine(MediaLocalImageDirectory));
                        ////////    }



                        ////////    string filename = string.Format("{0}.jpg", item.ID);
                        ////////    string imageFulleName = MediaLocalImageDirectory.Substring(MediaLocalImageDirectory.Length - 1, 1).Contains("\\") ?
                        ////////        string.Format("{0}{1}", MediaLocalImageDirectory, filename) :
                        ////////        string.Format("{0}\\{1}", MediaLocalImageDirectory, filename);


                        ////////    if (File.Exists(imageFulleName))
                        ////////    {
                        ////////        File.Delete(imageFulleName);
                        ////////    }

                        ////////}
                        #endregion

                    }
                }
            }

            result.StatusCode = "00";
            result.StatusMessage = "Success deleting receipt details";
            result.Value = true;

            return result;
        }

        public static ResultPaginationModel<List<ReceiptDetailModel>> History(int MemberId, ParamReceiptPagination param)
        {
            ResultPaginationModel<List<ReceiptDetailModel>> result = new ResultPaginationModel<List<ReceiptDetailModel>>();
            if (param != null)
            {
                DateTime? _startDate = null;
                DateTime? _endDate = null;

                if (!string.IsNullOrEmpty(param.StartDate))
                {
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(param.StartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        _startDate = dt;
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.StatusMessage = "the date (StartDate) format does not match, please using format 'dd/MM/yyyy'";
                        return result;
                    }

                }
                if (!string.IsNullOrEmpty(param.EndDate))
                {
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(param.EndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        _endDate = dt;
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.StatusMessage = "the date (EndDate) format does not match, please using format 'dd/MM/yyyy'";
                        return result;
                    }

                }

                int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                if (CurrentPage <= 0)
                {
                    CurrentPage = 1;
                }



                int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 8;

                if (totalRowPerPage > 8)
                {
                    totalRowPerPage = 8;
                }

                var datas = Receipt.GetByMemberIdAndStatusAndTransDate(MemberId, param.Status, param.IsReupload, _startDate, _endDate);

                if (!string.IsNullOrEmpty(param.OrderByColumnName))
                {
                    //note: use extended order by for dynamic order by
                    string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                    datas = datas.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                }
                else
                {
                    datas = datas.OrderBy(x => x.TransactionDate);
                }



                int total = 0;
                if (!string.IsNullOrEmpty(param.Search))
                {
                    datas = datas.Where(x => x.Code.Contains(param.Search));
                    total = Receipt.GetByMemberIdAndStatusAndTransDate(MemberId, param.Status, param.IsReupload, _startDate, _endDate).Count(x => x.Code.Contains(param.Search));
                }
                else
                {
                    total = Receipt.GetByMemberIdAndStatusAndTransDate(MemberId, param.Status, param.IsReupload, _startDate, _endDate).Count();
                }




                var regions = datas.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                result.TotalRow = total;


                if (datas != null)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} Receipt(s)", total);
                    string assetURL = Settings.GetValueByKey(SiteSetting.ASSET_URL);
                    //use this exted method to translate from object datalayer to model
                    result.Value = regions.TranslatedMoreDetail(assetURL);

                }
                else
                {

                    result.StatusCode = "10";
                    result.StatusMessage = "no data found";
                }
            }
            else
            {
                result.StatusCode = "20";
                result.StatusMessage = "parameter is required";
            }



            return result;

        }

        public static ResultModel<ReceiptDetailModel> GetReceiptModelById(string idS)
        {
            ResultModel<ReceiptDetailModel> result = new ResultModel<ReceiptDetailModel>();

            Guid ID = Guid.Empty;
            if (Guid.TryParse(idS, out ID))
            {




                var data = Receipt.GetById(ID);
                if (data != null)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get Receipt with ID {0}", ID);
                    string assetURL = Settings.GetValueByKey(SiteSetting.ASSET_URL);
                    result.Value = new ReceiptDetailModel(data, assetURL);
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = "Id not found in the system";
                }

            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Id does not match the format";
            }

            return result;
        }

        public static ResultModel<ReceiptDetailModel> GetReceiptModelByCode(string Code)
        {
            ResultModel<ReceiptDetailModel> result = new ResultModel<ReceiptDetailModel>();
            if (!string.IsNullOrEmpty(Code))
            {
                var data = Receipt.GetByCode(Code);
                if (data != null)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get Receipt with Code {0}", Code);
                    string assetURL = Settings.GetValueByKey(SiteSetting.ASSET_URL);
                    result.Value = new ReceiptDetailModel(data, assetURL);
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = "Id not found in the system";
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Code is required";
            }

            return result;
        }

        public static ResultModel<Receipt> InsertFromCMS(UploadReceiptCMSModel model, Guid newId, List<HttpPostedFileBase> images, int poolID, string By, string url, long? userID, string code, bool isSpg = false, bool isExcel = false)
        {
            ResultModel<Receipt> result = new ResultModel<Receipt>();
            try
            {
                DateTime transactiondate = DateTime.MinValue;
                int productBeforeId = DataMember.Member.GetById(model.MemberID).ProductBeforeID.HasValue ? (int)DataMember.Member.GetById(model.MemberID).ProductBeforeID : 0;
                DataMember.ProductBefore objProductBefore = DataMember.ProductBefore.GetById(productBeforeId);

                if (!string.IsNullOrEmpty(model.StrTransactionDate) && !string.IsNullOrEmpty(model.StrTransactionTime))
                {
                    string datetime = model.StrTransactionDate + " " + model.StrTransactionTime;
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        transactiondate = dt;
                        if (dt > DateTime.Now)
                        {
                            result.StatusCode = "41";
                            result.StatusMessage = "Tanggal Transaksi salah, tidak boleh melewati waktu sekarang.";
                            return result;
                        }
                    }
                    else
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("Format date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                        return result;
                    }
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Transaction Date and Transaction time are Mandatory");
                    return result;
                }


                DateTime reqDate = DateTime.MinValue;
                if (!string.IsNullOrEmpty(model.StrRequestDate))
                {
                    string datetime = model.StrRequestDate + " 23:59:59";
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        reqDate = dt;

                        //if (dt > DateTime.Today)
                        //{
                        //    result.StatusCode = "41";
                        //    result.StatusMessage = "Tanggal Request salah, tidak boleh melewati waktu sekarang.";
                        //    return result;
                        //}
                    }
                    else
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("Format Request date is not correct, please using format dd/MM/yyyy");
                        return result;
                    }
                }

                if (!string.IsNullOrEmpty(model.ReceiptCode))
                {
                    Receipt predictedReceipt = Receipt.GetByReceiptCode(model.ReceiptCode).FirstOrDefault();
                    if (predictedReceipt != null)
                    {
                        result.StatusCode = "failed";
                        result.StatusMessage = string.Format("Receipt Code {0} already exist in database", model.ReceiptCode);
                        return result;
                    }
                }
                else
                {
                    var checkFraud = Receipt.GetByMemberRetailerDateAndAddressWithOutReceiptCode(model.MemberID, model.RetailerID, transactiondate, model.RetailerAddress);
                    if (checkFraud != null)
                    {
                        var dataRetailer = Data.Retailer.GetById(model.RetailerID);
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("Receipt with Member {0}, Retailer {1} and transactionDate {2}  and Address '{3}' is already exist in database",
                            model.MemberName, (dataRetailer != null ? dataRetailer.Code : model.RetailerID.ToString()), transactiondate, model.RetailerAddress);
                        return result;
                    }
                }

                Member member = Member.GetById(model.MemberID);
                LoyaltyPointEngine.DataMember.Member dataMember = DataMember.Member.GetById(model.MemberID);
                int totalMemberReceipts = Receipt.GetByMemberId(model.MemberID).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved);
                int isBelow1YearOrPregnant = MemberLogic.CheckMemberChildBelow1YearOrPregnant(member.ID);
                bool isContainSpecificAllowedProduct = model.ReceiptDetails.Any(x => SiteSetting.ChildAgeBelow1YearRuleAllowedActionList.Any(y => y == x.ActionID.ToString()));
                bool MemberIsActive = MemberLogic.CheckMemberIsVerify(member.ID);
                bool withBalanceLog = true;

                if (!MemberIsActive)  ///BLocking receipt Account Member Unverify
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Account Member not verify, Please verify account member";
                    return result;
                }

                if (isBelow1YearOrPregnant == 3 && dataMember.StagesID.HasValue)
                {
                    var stage = DataMember.Stages.GetById(dataMember.StagesID.Value);
                    if (stage != null && stage.IsPregnant)
                    {
                        isBelow1YearOrPregnant = 4;
                    }
                }

                //Check if product is pregnancy product
                foreach (var item in model.ReceiptDetails)
                {
                    if (item.ActionID.HasValue)
                    {
                        var action = Data.Action.GetById(item.ActionID.Value);
                        if (action != null && action.ProductID.HasValue)
                        {
                            var product = Product.GetByID(action.ProductID.Value);
                            if (product != null)
                            {
                                if (!product.IsPregnancyProduct && !item.ChildID.HasValue)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Harap mengisi nama anak anda terlebih dahulu";
                                    return result;
                                }

                                //Check children birthdate
                                if (item.ChildID.HasValue)
                                {
                                    var child = DataMember.Child.GetById(item.ChildID.Value);
                                    if (child != null && child.Birthdate.HasValue)
                                    {
                                        if (((DateTime.Today.Subtract(child.Birthdate.Value)).TotalDays / 365) < 1)
                                        {
                                            isBelow1YearOrPregnant = 2;
                                            break;
                                        }
                                    }
                                }

                                if (product.IsPregnancyProduct && !dataMember.Stages.IsPregnant)
                                {
                                    result.StatusCode = "29";
                                    result.StatusMessage = "Kondisi Anda Belum Hamil. Silahkan Edit Profile Terlebih Dahulu";
                                    return result;
                                }
                            }
                        }
                    }
                }

                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                Receipt data = null;
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    data = new Receipt();
                    data.ID = newId;
                    data.MemberID = model.MemberID;
                    if (model.Code != null)
                    {
                        data.Code = model.Code;
                    }
                    data.ReceiptCode = model.ReceiptCode;
                    data.Status = (int)Receipt.ReceiptStatus.Approve;
                    data.Channel = string.IsNullOrEmpty(model.Channel.Trim()) ? Convert.ToString(Channel.Website) : model.Channel.Trim();
                    if (isSpg)
                    {
                        data.Status = (int)Receipt.ReceiptStatus.Waiting;
                        data.Channel = Convert.ToString(Channel.ELINA);
                    }
                    if (isBelow1YearOrPregnant < 4)
                    {
                        withBalanceLog = false;
                        data.IsNeedReuploadData = false;
                        data.Status = (int)Receipt.ReceiptStatus.Reject;
                        data.RejectReason = MemberLogic.ReceiptRejectedMessage(isBelow1YearOrPregnant);
                    }
                    data.RetailerID = model.RetailerID;
                    data.TransactionDate = transactiondate;
                    data.Code = code;
                    data.RetailerAddress = model.RetailerAddress;
                    data.TotalPrice = model.TotalPrice;
                    if (!string.IsNullOrEmpty(model.StrRequestDate))
                    {
                        data.RequestDate = reqDate;
                    }
                    if (SiteSetting.OldMemberParameterDate.HasValue)
                    {
                        if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                        {
                            data.IsFirstSubmission = false;
                            if (totalMemberReceipts < 1)
                                data.IsFirstSubmission = true;
                        }
                    }

                    Campaign selectedCampaign = model.CampaignID.HasValue ? Campaign.GetById(model.CampaignID.Value) : null;
                    if (selectedCampaign != null)
                    {
                        if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                        {
                            if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(data.Channel)))
                            {
                                data.CampaignID = selectedCampaign.ID;
                            }
                        }
                    }

                    bool MemberIsIdle = false;
                    if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                    {
                        var monthsIdleSetting = Data.Settings.GetByKey("MemberIdle").Value;
                        var lastReceiptDate = Data.Receipt.GetByMemberId(model.MemberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.Status == 1);
                        if (lastReceiptDate != null)
                        {
                            var monthDifference = (DateTime.Now.Month - lastReceiptDate.CreatedDate.Month) + 12 * (DateTime.Now.Year - lastReceiptDate.CreatedDate.Year);
                            if (monthDifference >= Convert.ToInt32(monthsIdleSetting))
                            {
                                MemberIsIdle = true;
                            }
                        }

                        //setting sob receipt
                        if (data.IsFirstSubmission)
                        {

                            if (productBeforeId == 0)
                            {
                                data.ProductBefore = string.Empty;
                            }
                            else
                            {
                                if (objProductBefore == null)
                                {
                                    data.ProductBefore = string.Empty;
                                }
                                else
                                {
                                    data.ProductBefore = objProductBefore.Name;
                                }
                            }
                        }
                        else
                        {
                            if (objProductBefore == null)
                            {
                                data.ProductBefore = string.Empty;
                            }
                            else
                            {
                                data.ProductBefore = objProductBefore.Name;
                            }
                        }
                        //end setting sob receipt
                    }

                    if (data.Status == (int)Receipt.ReceiptStatus.Approve)
                    {
                        var dataProductBefore = ProductBeforeLogic.GetLastProductBeforeByMemberId(data.MemberID);
                        data.ProductBefore = dataProductBefore;
                    }
                    var res = data.Insert(By, url, userID);
                    int timeStamp = 0;
                    int lastPoint = 0;
                    DateTime createdDateInsert = DateTime.Now;
                    var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(model.MemberID);
                    if (lastBalanceLog != null)
                        lastPoint += lastBalanceLog.Balance;
                    if (res.Success)
                    {
                        var resultImages = new ResultModel<bool>();
                        Guid receiptID = new Guid(res.AdditionalInfo);
                        if (isExcel == false)
                        {
                            resultImages = InsertingReceiptImageCMS(images, receiptID, By);
                        }
                        else
                        {
                            resultImages.StatusCode = "00";
                        }
                        if (resultImages.StatusCode == "00")
                        {
                            foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                            {
                                var product = Data.Action.GetById(detail.ActionID.Value);
                                var UptradeRemarks = UptradeManageLogic.Tradelogic(model.MemberID, data.Channel, product.ProductID.Value, url, detail.Quantity, By);

                                Collect.CollectLogic colLogic = new Collect.CollectLogic();

                                CollectCMSModel colModel = new CollectCMSModel();
                                colModel.MemberID = model.MemberID;
                                colModel.MemberName = model.MemberName;
                                colModel.ActionID = detail.ActionID;
                                colModel.Quantity = detail.Quantity;
                                colModel.QuantityApproved = detail.QuantityApproved;
                                colModel.Points = detail.Points;
                                colModel.TotalPoints = (detail.TotalQuantityApprovedPoints == 0 ? (detail.QuantityApproved * detail.Points) : detail.TotalQuantityApprovedPoints);
                                colModel.TransactionDate = transactiondate.ToString("dd/MM/yyyy");
                                colModel.TransactionTime = transactiondate.ToString("HH:mm:ss");
                                colModel.ReceiptID = new Guid(res.AdditionalInfo);
                                if (selectedCampaign != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                                    {
                                        if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(data.Channel)))
                                        {
                                            colModel.CampaignID = selectedCampaign.ID;
                                        }
                                    }
                                }
                                colModel.Remarks = detail.Remarks ?? "Receipt Detail from " + res.AdditionalInfo;
                                if (UptradeRemarks.StatusCode == "00")
                                {
                                    if (UptradeRemarks.Value.Remarks != null)
                                    {
                                        colModel.Remarks = UptradeRemarks.Value.Remarks;
                                    }
                                }
                                colModel.ItemPrice = detail.ItemPrice;
                                if (SiteSetting.OldMemberParameterDate.HasValue)
                                {
                                    if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                                    {
                                        if (data.IsFirstSubmission)
                                        {
                                            if (url.ToLower().Contains("ftp"))
                                            {
                                                colModel.Remarks = "[First Submission FTP " + data.Channel + "] " + colModel.Remarks;
                                                //string actCode = SiteSetting.FTPActionCodeFisrtSubmit;
                                                //LoyaltyPointEngine.PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actCode, dataMember, url);

                                            }
                                            else
                                            {
                                                colModel.Remarks = "[First Submission] " + colModel.Remarks;
                                            }
                                        }
                                    }
                                }
                                colModel.ReceiptID = receiptID;

                                //cek jika retailer bebas cap
                                bool noCapByRetailer = false;
                                //if (data.Retailer.IsNoCap.HasValue && (bool)data.Retailer.IsNoCap)
                                //{
                                //    noCapByRetailer = true;
                                //}
                                //end cek retailer bebas cap
                                colModel.ChildID = detail.ChildID;

                                var json = JsonConvert.SerializeObject(colModel);
                                Log.Info("Data Collect = " + json);

                                var colres = colLogic.InsertCollect(colModel, poolID, By, userID, url, timeStamp, totalMemberReceipts, null, withBalanceLog, MemberIsIdle, noCapByRetailer
                                    , lastPoint, createdDateInsert);
                                timeStamp++;
                                createdDateInsert = createdDateInsert.AddSeconds(1);
                                if (colres.StatusCode == "00")
                                {
                                    if (withBalanceLog && colres.Value != null)
                                        lastPoint += colres.Value.TotalPoints;

                                    result.StatusCode = "00";
                                    result.StatusMessage = string.Format("Receipt And Collect Success Inserted");
                                }
                                else
                                {
                                    result.StatusCode = colres.StatusCode;
                                    result.StatusMessage = colres.StatusMessage;
                                    RemovingReceiptImageCMS(receiptID);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            result.StatusCode = resultImages.StatusCode;
                            result.StatusMessage = resultImages.StatusMessage;
                        }

                        if (SiteSetting.OldMemberParameterDate.HasValue)
                        {
                            if (member.CreatedDate > SiteSetting.OldMemberParameterDate)
                            {
                                member.IsFirstSubmit = data.IsFirstSubmission;
                                var memberDbResponse = member.Update(By, url, userID);
                                if (!memberDbResponse.Success)
                                {
                                    result.StatusCode = "50";
                                    result.StatusMessage = memberDbResponse.ErrorMessage;
                                }
                            }
                        }
                    }
                    else
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("{0} \r\n {1}", res.ErrorMessage, res.ErrorEntity);
                    }

                    if (result.StatusCode == "00")
                    {
                        transScope.Complete();
                    }
                    transScope.Dispose();

                    if (result.StatusCode == "00")
                    {
                        if (data != null && data.Status == (int)Receipt.ReceiptStatus.Approve)
                        {
                            BonusPointForChannelFirstSubmission(member, data.Channel, timeStamp, lastPoint, createdDateInsert);
                            NotificationLogic.Create(data.MemberID, Common.Notification.NotificationType.Type.Approved, "Penambahan Point", "No Transaksi: " + data.Code, data.Code, data.Collect.Sum(x => x.TotalPoints), By);

                            // ambil list automated campaign dimana member ini terdaftar
                            memberAutomatedCampaigns = (from P in AutomaticCampaignParticipant.GetAll(false)
                                                        join C in Campaign.GetAll() on P.CampaignID equals C.ID
                                                        where P.IsActive && C.IsAutomate && P.MemberId == member.ID && (C.StartDate <= data.TransactionDate && data.TransactionDate <= C.EndDate)
                                                        select P).ToList();

                            foreach (var collect in data.Collect)
                            {
                                CalculateAutomatedCampaign(collect, collect.Action, member, data);
                            }
                            PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(data.MemberID, By);


                            //#region 1 Million Participations
                            //var model1million = new Model.Parameter.Participations.ParticipationsModel();
                            //var getmember = MemberLogicLayer.MemberLogic.GetMemberModelById(data.MemberID);
                            //model1million.NameUser = getmember.Value.FirstName + " " + getmember.Value.LastName;
                            //model1million.Phone = getmember.Value.Phone;
                            //model1million.ReceiptID = data.ID.ToString();
                            //model1million.ChildDOB = getmember.Value.ChildBirthDate;
                            //model1million.ChildName = getmember.Value.ChildName;
                            //model1million.Channel = "Receipt";
                            //model1million.UserId = Convert.ToInt32(userID);
                            //model1million.Description = "Insert by Receipt ID" + data.ID.ToString();

                            //var million = RESTHelper.Post<ResultModel<Model.Object.Participations.ResultParticipationsModel>>(Data.Settings.GetValueByKey("RewardsAPI") + "/Participations/AddParticipations", model1million);
                            //#endregion
                            //#region 1 million participations
                            //var model1million = new Model.Parameter.Participations.ParticipationsModel();
                            //var getmember = MemberLogicLayer.MemberLogic.GetMemberModelById(data.MemberID);
                            //model1million.NameUser = getmember.Value.FirstName + " " + getmember.Value.LastName;
                            //model1million.Phone = getmember.Value.Phone;
                            //model1million.ReceiptID = new Guid(res.AdditionalInfo).ToString();
                            //model1million.ChildDOB = getmember.Value.ChildBirthDate;
                            //model1million.ChildName = getmember.Value.ChildName;
                            //model1million.Channel = "Receipt";
                            //model1million.UserId = Convert.ToInt32(userID);
                            //model1million.Caption = "Insert by Receipt ID " + new Guid(res.AdditionalInfo).ToString();

                            //var million = RESTHelper.Post<ResultModel<Model.Object.Participations.ResultParticipationsModel>>(Data.Settings.GetValueByKey("RewardsAPI") + "/Participations/AddParticipationsJson", model1million);
                            //#endregion
                            //check spg incentive
                            IncentiveLogic.CheckIncentive(member, data, By, MemberIsIdle);
                        }
                        else if (data != null && data.Status == (int)Receipt.ReceiptStatus.Reject)
                        {
                            result.StatusCode = "55";
                            result.StatusMessage = SiteSetting.ChildAgeBelow1YearRuleValidationMessage;
                        }
                        result.Value = data;
                    }
                }
            }
            catch (Exception ed)
            {
                result.StatusCode = "099999";
                result.StatusMessage = "[" + ed.StackTrace + "    =    " + ed.Message + "]";
            }
            return result;
        }

        public static ResultModel<UploadReceiptCMSModel> ModerateFromCMS(UploadReceiptCMSModel model, int poolID, string By, string url, long? userID, bool isSpg = false)
        {
            ResultModel<UploadReceiptCMSModel> result = new ResultModel<UploadReceiptCMSModel>();

            // Get Member first product
            int productBeforeId = 0;
            var dataMember = DataMember.Member.GetById(model.MemberID);
            if (!dataMember.ProductBeforeID.HasValue || dataMember.ProductBeforeID == 0)
            {
                if (dataMember.Child != null || dataMember.Child.Count > 0)
                {
                    productBeforeId = dataMember.Child.First(x => x.ProductBeforeID.HasValue).ProductBeforeID.Value;
                }
            }
            else
            {
                productBeforeId = Convert.ToInt32(dataMember.ProductBeforeID);
            }

            DataMember.ProductBefore objProductBefore = DataMember.ProductBefore.GetById(productBeforeId);
            int totalMemberReceipts = Receipt.GetByMemberId(model.MemberID).Count(x => x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved);

            #region "Validasi Moderate"
            //Check if product is pregnancy product
            foreach (var item in model.ReceiptDetails)
            {
                if (item.ActionID.HasValue)
                {
                    var action = Data.Action.GetById(item.ActionID.Value);
                    if (action != null && action.ProductID.HasValue)
                    {
                        var product = Product.GetByID(action.ProductID.Value);
                        if (product != null && !product.IsPregnancyProduct && !item.ChildID.HasValue)
                        {
                            result.StatusCode = "29";
                            result.StatusMessage = "Child ID must not be empty if product is not pregnancy product";
                            return result;
                        }
                    }
                }
            }

            Receipt predictedReceipt = Receipt.GetById(model.ID);
            if (predictedReceipt == null)
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("can't find receipt {0} please check your Receipt ID ", model.ID);
                return result;
            }
            else
            {
                if (predictedReceipt.Status == (int)Receipt.ReceiptStatus.Approve || predictedReceipt.Status == (int)Receipt.ReceiptStatus.OcrApproved)
                {
                    result.StatusCode = "600";
                    result.StatusMessage = string.Format("receipt {0} already Approved, please choose another", predictedReceipt.Code);
                    return result;
                }
            }

            Member member = Member.GetById(model.MemberID);

            DateTime transactiondate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrTransactionDate) && !string.IsNullOrEmpty(model.StrTransactionTime))
            {
                string datetime = model.StrTransactionDate + " " + model.StrTransactionTime;
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    transactiondate = dt;
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Format date is not correct, please using format dd/MM/yyyy HH:mm:ss");
                    return result;
                }
            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Transaction Date and Transaction time are Mandatory");
                return result;
            }

            DateTime reqDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrRequestDate))
            {
                string datetime = model.StrRequestDate + " 23:59:59";
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    reqDate = dt;
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Format Request date is not correct, please using format dd/MM/yyyy");
                    return result;
                }
            }

            if (!string.IsNullOrEmpty(model.ReceiptCode))
            {
                var checkFraud = Receipt.GetByReceiptCodeNotSelf(model.ReceiptCode, predictedReceipt.ID);

                if (checkFraud != null)
                {
                    if (checkFraud.Any(x => x.Status == 0 || x.Status == 1))
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("Receipt Code {0} already exist in database", model.ReceiptCode);
                        return result;
                    }
                }
            }
            else
            {
                var checkFraud = Receipt.GetByMemberRetailerDateAddressWithOutReceiptCodeNotSelf(model.MemberID, model.RetailerID, transactiondate, model.RetailerAddress, predictedReceipt.ID);
                if (checkFraud != null)
                {
                    var dataRetailer = Data.Retailer.GetById(model.RetailerID);
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Receipt with Member {0}, Retailer {1} and transactionDate {2} and Address '{3}' is  already exist in database",
                        model.MemberName, (dataRetailer != null ? dataRetailer.Code : model.RetailerID.ToString()), transactiondate, model.RetailerAddress);
                    return result;
                }
            }

            List<CollectCMSModel> listCollectModel = new List<CollectCMSModel>();
            Campaign selectedCampaign = model.CampaignID.HasValue ? Campaign.GetById(model.CampaignID.Value) : null;
            bool IsBottomPrice = false;

            #region "Set Receipt for Update"
            predictedReceipt.MemberID = model.MemberID;
            //predictedReceipt.Code = model.Code;
            predictedReceipt.ReceiptCode = model.ReceiptCode;
            predictedReceipt.Status = (int)Receipt.ReceiptStatus.Approve;
            if (predictedReceipt.Status == (int)Receipt.ReceiptStatus.Approve)
            {
                predictedReceipt.RejectReason = String.Empty;
            }
            predictedReceipt.RetailerID = model.RetailerID;
            predictedReceipt.TransactionDate = transactiondate;
            predictedReceipt.RetailerAddress = model.RetailerAddress;
            predictedReceipt.Channel = string.IsNullOrEmpty(model.Channel.Trim()) ? Convert.ToString(Channel.Website) : model.Channel.Trim();
            if (!string.IsNullOrEmpty(model.StrRequestDate))
            {
                predictedReceipt.RequestDate = reqDate;
            }
            else
            {
                predictedReceipt.RequestDate = null;
            }
            if (isSpg)
            {
                predictedReceipt.Channel = Convert.ToString(Channel.ELINA);
            }

            if (selectedCampaign != null)
            {
                if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                {
                    if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(predictedReceipt.Channel)))
                    {
                        predictedReceipt.CampaignID = selectedCampaign.ID;
                    }
                }
            }
            int timeStamp = 0;

            if (!string.IsNullOrEmpty(model.RetailerPostCode))
            {
                predictedReceipt.PostCode = model.RetailerPostCode;
            }

            bool MemberIsIdle = false;
            if (predictedReceipt.Status == (int)Receipt.ReceiptStatus.Approve || predictedReceipt.Status == (int)Receipt.ReceiptStatus.OcrApproved)
            {
                var monthsIdleSetting = Data.Settings.GetByKey("MemberIdle").Value;
                var lastReceiptDate = Data.Receipt.GetByMemberId(predictedReceipt.MemberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.Status == 1);
                if (lastReceiptDate != null)
                {
                    var monthDifference = (DateTime.Now.Month - lastReceiptDate.CreatedDate.Month) + 12 * (DateTime.Now.Year - lastReceiptDate.CreatedDate.Year);
                    if (monthDifference >= Convert.ToInt32(monthsIdleSetting))
                    {
                        MemberIsIdle = true;
                    }
                }
                var dataProductBefore = ProductBeforeLogic.GetLastProductBeforeByMemberId(predictedReceipt.MemberID);
                predictedReceipt.ProductBefore = dataProductBefore;
            }

            //setting sob receipt
            List<GetLatestProductApproved_Result> listObj = Data.Receipt.getLatestUploadedProduct(predictedReceipt.MemberID);
            if (listObj.Count() > 0)
            {
                predictedReceipt.ProductBefore = listObj[0].productgroupcode;
            }
            else
            {
                if (productBeforeId == 0)
                {
                    predictedReceipt.ProductBefore = string.Empty;
                }
                else
                {
                    if (objProductBefore == null)
                    {
                        predictedReceipt.ProductBefore = string.Empty;
                    }
                    else
                    {
                        predictedReceipt.ProductBefore = objProductBefore.Name;
                    }
                }
            }
            //end setting sob receipt
            #endregion

            //cek jika retailer bebas cap
            bool noCapByRetailer = false;
            //if (predictedReceipt.Retailer.IsNoCap.HasValue && (bool)predictedReceipt.Retailer.IsNoCap)
            //{
            //    noCapByRetailer = true;
            //}
            //end cek retailer bebas cap

            // Checking Cap Point
            int totalGenerationPoint = 0;

            foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
            {
                Collect.CollectLogic colLogic = new Collect.CollectLogic();

                var dataAction = Data.Action.GetById(detail.ActionID.GetValueOrDefault());

                CollectCMSModel colModel = new CollectCMSModel();
                colModel.MemberID = model.MemberID;
                colModel.MemberName = model.MemberName;
                colModel.ActionID = detail.ActionID;
                colModel.Quantity = detail.Quantity;
                colModel.QuantityApproved = detail.QuantityApproved;
                colModel.Points = detail.Points;
                colModel.TotalPoints = detail.TotalQuantityApprovedPoints == 0 ? (detail.QuantityApproved * detail.Points) : detail.TotalQuantityApprovedPoints;
                colModel.TransactionDate = transactiondate.ToString("dd/MM/yyyy");
                colModel.TransactionTime = transactiondate.ToString("HH:mm:ss");
                colModel.ReceiptID = model.ID;
                colModel.IsFirstSubmission = predictedReceipt.IsFirstSubmission;
                colModel.Remarks = detail.Remarks ?? "Receipt Detail from " + model.ID;
                colModel.ReceiptID = predictedReceipt.ID;
                if (selectedCampaign != null)
                {
                    if (!string.IsNullOrWhiteSpace(selectedCampaign.Channels))
                    {
                        if (selectedCampaign.Channels.Split(',').Any(x => x == Convert.ToString(predictedReceipt.Channel)))
                        {
                            colModel.CampaignID = selectedCampaign.ID;
                        }
                    }
                }
                colModel.ItemPrice = detail.ItemPrice;

                if (detail.ItemPrice < dataAction.MinimumPrice * detail.QuantityApproved)
                {
                    IsBottomPrice = true;
                }

                colModel.ChildID = detail.ChildID;

                ResultModel<Data.Collect> resultCapValidation = colLogic.ValidateCapPoint(colModel, ref totalGenerationPoint, dataAction, noCapByRetailer);

                if (resultCapValidation.StatusCode != "00")
                {
                    result.StatusCode = resultCapValidation.StatusCode;
                    result.StatusMessage = resultCapValidation.StatusMessage;
                    return result;
                }

                var json = JsonConvert.SerializeObject(colModel);
                Log.Info("Data Collect = " + json);

                listCollectModel.Add(colModel);
            }
            #endregion

            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                result.Value = model;

                var res = predictedReceipt.Update(By, url, userID);
                int lastPoint = 0;
                DateTime createdDateInsert = DateTime.Now;
                var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(model.MemberID);
                if (lastBalanceLog != null)
                {
                    lastPoint += lastBalanceLog.Balance;
                }
                if (res.Success)
                {
                    Guid receiptID = predictedReceipt.ID;
                    List<Data.Collect> oldCollects = Data.Collect.GetAllByReceiptID(receiptID).ToList();

                    foreach (CollectCMSModel detail in listCollectModel)
                    {
                        Collect.CollectLogic colLogic = new Collect.CollectLogic();

                        var dataAction = Data.Action.GetById(detail.ActionID.GetValueOrDefault());

                        //For Uptrade
                        var UptradeRemarks = UptradeManageLogic.Tradelogic(model.MemberID, predictedReceipt.Channel, dataAction.ProductID.Value, url, detail.Quantity, By);

                        #region Remarks Uptrade
                        if (UptradeRemarks.StatusCode == "00")
                        {
                            if (UptradeRemarks.Value.Remarks != null)
                            {
                                detail.Remarks = UptradeRemarks.Value.Remarks;
                            }
                        }
                        #endregion

                        var colres = colLogic.InsertCollect(detail, poolID, By, userID, url, timeStamp, totalMemberReceipts, dataAction, true, MemberIsIdle, noCapByRetailer, lastPoint, createdDateInsert, true);
                        createdDateInsert = createdDateInsert.AddSeconds(1);
                        timeStamp++;
                        if (colres.StatusCode == "00")
                        {
                            lastPoint += colres.Value.TotalPoints;
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("Receipt And Collect Success Inserted");
                        }
                        else
                        {
                            result.StatusCode = colres.StatusCode;
                            result.StatusMessage = (dataAction != null ? (" (" + dataAction.Name + " ) ") : "") + colres.StatusMessage;
                            transScope.Dispose();
                            break;
                        }
                    }
                    if (IsBottomPrice)
                    {
                        predictedReceipt.IsBottomPrice = true;
                        predictedReceipt.Update(By, url, userID);
                    }
                    if (result.StatusCode == "00")
                    {
                        foreach (Data.Collect oldCollect in oldCollects)
                        {
                            oldCollect.IsDeleted = true;
                            oldCollect.IsActive = false;
                            oldCollect.IsApproved = false;
                            oldCollect.Update(By, url, userID);
                        }
                        transScope.Complete();
                    }
                }
                transScope.Dispose();
                if (result.StatusCode == "00")
                {
                    //check spg incentive
                    IncentiveLogic.CheckIncentive(member, predictedReceipt, By, MemberIsIdle);

                    if (predictedReceipt != null && predictedReceipt.Status == (int)Receipt.ReceiptStatus.Approve)
                    {
                        BonusPointForChannelFirstSubmission(member, predictedReceipt.Channel, timeStamp + 20, lastPoint, createdDateInsert);
                        var collects = Data.Collect.GetAllByReceiptID(predictedReceipt.ID).ToList();
                        NotificationLogic.Create(predictedReceipt.MemberID, Common.Notification.NotificationType.Type.Approved, "Penambahan Point", "No Transaksi: " + predictedReceipt.Code, predictedReceipt.Code, collects != null ? collects.Sum(x => x.TotalPoints) : 0, By);

                        BonusPointForChannelSubmissionDay(member, predictedReceipt.Channel, collects != null ? collects.Select(x => x.TotalPoints).FirstOrDefault() : 0);

                        // ambil list automated campaign dimana member ini terdaftar
                        memberAutomatedCampaigns = (from P in AutomaticCampaignParticipant.GetAll(false)
                                                    join C in Campaign.GetAll() on P.CampaignID equals C.ID
                                                    where P.IsActive && C.IsAutomate && P.MemberId == member.ID && (C.StartDate <= predictedReceipt.TransactionDate && predictedReceipt.TransactionDate <= C.EndDate)
                                                    select P).ToList();

                        foreach (var collect in predictedReceipt.Collect)
                        {
                            CalculateAutomatedCampaign(collect, collect.Action, member, predictedReceipt);
                        }

                        //update status di quickwin2
                        QuickWin2Participation objQuickWin2 = QuickWin2Participation.GetByReceiptId(predictedReceipt.ID);

                        if (objQuickWin2 != null)
                        {
                            objQuickWin2.ReceiptStatus = predictedReceipt.Status;
                            objQuickWin2.Update(By, "CMS", predictedReceipt.MemberID);
                        }
                    }
                    PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(predictedReceipt.MemberID, By);

                    #region 1 Million Participations
                    var model1million = new Model.Parameter.Participations.ParticipationsModel();
                    var getmember = MemberLogicLayer.MemberLogic.GetMemberModelById(predictedReceipt.MemberID);
                    model1million.NameUser = getmember.Value.FirstName + " " + getmember.Value.LastName;
                    model1million.Phone = getmember.Value.Phone;
                    model1million.ReceiptID = predictedReceipt.ID.ToString();
                    model1million.ChildDOB = getmember.Value.ChildBirthDate;
                    model1million.ChildName = getmember.Value.ChildName;
                    model1million.Channel = "Receipt";
                    model1million.UserId = Convert.ToInt32(userID);
                    model1million.Description = "Insert by Receipt ID" + predictedReceipt.ID.ToString();

                    var million = RESTHelper.Post<ResultModel<Model.Object.Participations.ResultParticipationsModel>>(Data.Settings.GetValueByKey("RewardsAPI") + "/Participations/AddParticipations", model1million);
                    #endregion
                }
            }
            return result;
        }

        public static ResultModel<UploadReceiptCMSModel> RejectFromCMS(RejectReceiptModel model, int poolID, string By, string url, long? userID)
        {
            ResultModel<UploadReceiptCMSModel> result = new ResultModel<UploadReceiptCMSModel>();
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                Receipt predictedReceipt = Receipt.GetById(model.ID);
                if (predictedReceipt == null)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("can't find receipt {0} please check your Receipt ID ", model.ID);
                    transScope.Dispose();
                    return result;
                }
                else
                {
                    if (predictedReceipt.Status == (int)Receipt.ReceiptStatus.Approve)
                    {
                        result.StatusCode = "600";
                        result.StatusMessage = string.Format("receipt {0} already Approved, please choose another", predictedReceipt.Code);
                        transScope.Dispose();
                        return result;
                    }
                }
                if (string.IsNullOrEmpty(model.Reason))
                {
                    result.StatusCode = "700";
                    result.StatusMessage = string.Format("receipt {0} MUST be given REASON for Rejected", predictedReceipt.Code);
                    transScope.Dispose();
                    return result;
                }
                predictedReceipt.RejectReason = model.Reason;
                predictedReceipt.IsNeedReuploadData = model.IsNeedReuploadData;
                predictedReceipt.Status = (int)Receipt.ReceiptStatus.Reject;
                var res = predictedReceipt.Update(By, url, userID);
                if (res.Success)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("Receipt " + predictedReceipt.ID + " successfully rejected!");
                    transScope.Complete();

                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format(res.ErrorMessage);
                    transScope.Dispose();
                    return result;
                }
                transScope.Dispose();

                if (result.StatusCode == "00")
                {
                    var collects = Data.Collect.GetAllByReceiptID(predictedReceipt.ID).ToList();

                    NotificationLogic.Create(predictedReceipt.MemberID, (model.IsNeedReuploadData
                        ? Common.Notification.NotificationType.Type.RejectReupload
                        : Common.Notification.NotificationType.Type.Reject)
                        , "Struk Di Reject"
                        , string.Format("No Transaksi: {0} {1}", predictedReceipt.Code, predictedReceipt.RejectReason)
                        , predictedReceipt.Code
                        , collects != null ? collects.Sum(x => x.TotalPoints) : 0
                        , By);
                }
            }
            return result;
        }

        public static ResultModel<UploadReceiptCMSModel> UpdateReceiptFromCMS(UploadReceiptCMSModel model, string by, string url, long? userid)
        {
            ResultModel<UploadReceiptCMSModel> result = new ResultModel<UploadReceiptCMSModel>();
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            HistoryEditChain data = null;
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                var curreceipt = Receipt.GetById(model.ID);

                if (curreceipt == null)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("can't find receipt {0} please check your Receipt ID ", model.ID);
                    transScope.Dispose();
                    return result;
                }
                else
                {
                    Guid NewId = Guid.NewGuid();
                    data = new HistoryEditChain();
                    data.RetailerIDBefore = curreceipt.RetailerID;
                    data.RetailerAddressBefore = curreceipt.RetailerAddress;
                    data.RetailerIDAfter = model.RetailerID;
                    data.RetailerAddressAfter = model.RetailerAddress;
                    data.ReceiptID = curreceipt.ID;
                    var res = data.Insert(by);
                    var cek_hist = HistoryEditChain.GetByReceiptId(curreceipt.ID);

                    if (cek_hist == null)
                    {
                        res.ErrorMessage = "Failed update Retailer";
                        res.Success = false;
                    }

                    if (res.Success)
                    {
                        curreceipt.RetailerID = model.RetailerID;
                        curreceipt.RetailerAddress = model.RetailerAddress;

                        var ress = curreceipt.UpdateReceipt(by, url, userid);

                        if (ress.Success)
                        {
                            result.StatusCode = "00";
                            result.StatusMessage = "Success Update Retailer";
                            transScope.Complete();
                        }
                        else
                        {
                            result.StatusCode = "500";
                            result.StatusMessage = string.Format("{0} \r\n {1}", ress.ErrorMessage, ress.ErrorEntity);
                        }
                    }
                    else
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("{0} \r\n {1}", res.ErrorMessage, res.ErrorEntity);
                    }
                }
                transScope.Dispose();
            }
            return result;
        }

        private static ResultModel<bool> InsertingReceiptImageCMS(List<HttpPostedFileBase> images, Guid ReceiptID, string by)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            if (images == null || images.Count <= 0 || (images.Count == 1 && images.FirstOrDefault() == null))
            {
                result.StatusCode = "00";
                result.StatusMessage = "There is no receipt images";
                result.Value = true;
                return result;
            }

            string RelativePathReceipt = Settings.GetValueByKey(SiteSetting.RelativePathReceipt);
            RelativePathReceipt = RelativePathReceipt + "/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString("00") + "/";
            if (RelativePathReceipt.Substring(RelativePathReceipt.Length - 1, 1).Contains("/"))
            {
                RelativePathReceipt = RelativePathReceipt + ReceiptID + "/";
            }
            else
            {
                RelativePathReceipt = RelativePathReceipt + "/" + ReceiptID + "/";
            }

            foreach (var _image in images)
            {
                if (_image != null)
                {
                    if (!string.IsNullOrEmpty(_image.FileName))
                    {
                        string MediaLocalImageDirectory = Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryReceipt);
                        MediaLocalImageDirectory = MediaLocalImageDirectory + "\\" + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString("00") + "\\";
                        //generate path with year and month
                        System.IO.Directory.CreateDirectory(MediaLocalImageDirectory);
                        if (MediaLocalImageDirectory.Substring(MediaLocalImageDirectory.Length - 1, 1).Contains("\\"))
                        {
                            MediaLocalImageDirectory = MediaLocalImageDirectory + ReceiptID + "\\";
                        }
                        else
                        {
                            MediaLocalImageDirectory = MediaLocalImageDirectory + "\\" + ReceiptID + "\\";
                        }

                        if (!string.IsNullOrEmpty(MediaLocalImageDirectory))
                        {
                            if (!Directory.Exists(MediaLocalImageDirectory))
                            {
                                DirectoryInfo di = Directory.CreateDirectory(Path.Combine(MediaLocalImageDirectory));
                            }

                            Guid imageID = Guid.NewGuid();
                            string filename = string.Format("{0}.jpg", imageID);
                            string imageFulleName = MediaLocalImageDirectory.Substring(MediaLocalImageDirectory.Length - 1, 1).Contains("\\")
                                ? string.Format("{0}{1}", MediaLocalImageDirectory, filename)
                                : string.Format("{0}\\{1}", MediaLocalImageDirectory, filename);

                            _image.SaveAs(imageFulleName);

                            //inserted images to db
                            var data = new ReceiptImage();
                            data.ID = imageID;
                            data.ReceiptID = ReceiptID;
                            data.Image = RelativePathReceipt + filename;
                            data.Insert(by, by, null);
                        }
                        else
                        {
                            result.StatusCode = "40";
                            result.StatusMessage = string.Format("configuration 'MediaLocalImageDirectory' is required, please contact your administrator to setup this configuration");
                            return result;
                        }
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.StatusMessage = string.Format("Filename is required");
                        return result;
                    }
                }
                else
                {
                    result.StatusCode = "31";
                    result.StatusMessage = string.Format("Image is can not null");
                    return result;
                }
            }//end foreach

            result.StatusCode = "00";
            result.StatusMessage = "Success inserted receipt images";
            result.Value = true;
            return result;
        }

        private static void RemovingReceiptImageCMS(Guid ReceiptID)
        {
            string MediaLocalImageDirectory = Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryReceipt);
            string path = Path.Combine(MediaLocalImageDirectory, ReceiptID.ToString());
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            if (Directory.Exists(path))
            {
                try
                {
                    DirectoryInfo imagedir = new DirectoryInfo(path);
                    foreach (FileInfo file in imagedir.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in imagedir.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    imagedir.Delete(true);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.InnerException.ToString());
                }
            }

        }

        private static void CalculateAutomatedCampaign(Data.Collect collect, Data.Action action, Data.Member member, Receipt receipt)
        {
            var product = Data.Product.GetByCode(action.Code);
            int productSize = 0;
            if (product != null)
            {
                var matchSize = Regex.Match(product.Size, "[0-9]+");
                if (matchSize.Success)
                    int.TryParse(matchSize.Value, out productSize);
            }

            // loop tiap automated campaign
            foreach (var campaign in memberAutomatedCampaigns)
            {
                var _campaign = campaign.Campaign;
                CAMPAIGN_TYPE _campaignType = CAMPAIGN_TYPE.PERCENTAGE;
                Enum.TryParse<CAMPAIGN_TYPE>(_campaign.Type, out _campaignType);

                // ambil list rule yg ada untuk action terpilih berdasarkan campaign
                var actionRules = CampaignLogic.CampaignLogic.GetActionRule(action.ID, campaign.CampaignID).ToArray();

                // jika rules dari action berikut lebih dari 0 maka bonus point yang dipakai adalah dari rules jika rules == 0 maka bonus point yg dipakai adalah dari campaign
                if (actionRules.Count() > 0)
                {
                    // check apakah member sudah pernah dapat bonus poin
                    if (!campaign.IsAchieve)
                    {


                        // hasil akhir dari pencapaian treshold rule dan atau hasil perbandingan operator masing" rule.
                        bool isAchieve = false;
                        Dictionary<int, Tuple<bool, CAMPAIGN_RULE_OPERATOR>> listRuleAchievement = new Dictionary<int, Tuple<bool, CAMPAIGN_RULE_OPERATOR>>();
                        int bonusPoint = 0;

                        // loop tiap rule dari action, jika rule hanya 1 maka yg menentukan bahwa si member sudah layak mendapat bonus poin adalah rule 1 itu aja, kalau rulenya diatas 1 maka rule tersebut akan di compare juga dengan rule berikutnya dengan menggunakan operator yg diset untuk rule tersebut. cth: jika jumlah rule ada 2 & rule 1 memiliki operator OR maka pertama kali kita cek rule 1 & 2 apakah treshold rule 1 & 2 sudah tercapai karena operatornya OR maka jika salah satu rule sudah mencapai treshold maka member berhak dapat bonus poin, jika operatornya AND maka setiap rule harus mencapai treshold agar member berhak mendapat bonus poin
                        for (int ruleIdx = 0; ruleIdx < actionRules.Count(); ruleIdx++)
                        {
                            AutomaticCampaignActionRule rule = actionRules[ruleIdx];

                            CAMPAIGN_RULE_TYPE ruleType = CAMPAIGN_RULE_TYPE.NONE;
                            Enum.TryParse<CAMPAIGN_RULE_TYPE>(rule.Type, out ruleType);

                            CAMPAIGN_RULE_OPERATOR ruleOperator = CAMPAIGN_RULE_OPERATOR.NONE;
                            Enum.TryParse<CAMPAIGN_RULE_OPERATOR>(rule.Operator, out ruleOperator);

                            // hitung total value yg sudah dicapai member dari rule ini
                            AutomaticCampaignValue ruleValue = CampaignLogic.CampaignLogic.GetActionRuleTotalValues(campaign.CampaignID, action.ID, member.ID, rule.ID);
                            bool isNewRuleValue = false;
                            if (ruleValue == null)
                            {
                                isNewRuleValue = true;
                                ruleValue = new AutomaticCampaignValue
                                {
                                    RuleID = rule.ID,
                                    MemberID = member.ID,
                                    ActionID = action.ID,
                                    CampaignID = campaign.CampaignID,
                                    Value = 0
                                };
                            }

                            // bandingkan antara total value dengan tresholdnya
                            bool isRuleTresholdAchieve = ruleValue.Value >= rule.Treshold;
                            // jika blm tercapai insert row value terbaru
                            if (!isRuleTresholdAchieve)
                            {
                                int value = 0;

                                switch (ruleType)
                                {
                                    //case CAMPAIGN_RULE_TYPE.PRICE:
                                    //    value = product.Price.GetValueOrDefault(1) * detail.Quantity;
                                    //    break;
                                    case CAMPAIGN_RULE_TYPE.QUANTITY:
                                        value = collect.QuantityApproved;
                                        break;
                                    case CAMPAIGN_RULE_TYPE.SIZE:
                                        value = productSize > 0 ? productSize * collect.QuantityApproved : 0;
                                        break;
                                    default:
                                        break;
                                }

                                if (ruleType != CAMPAIGN_RULE_TYPE.NONE)
                                {
                                    if (isNewRuleValue)
                                    {
                                        ruleValue.Value = value;
                                        ruleValue.Insert(member.Name, "", null);
                                    }
                                    else
                                    {
                                        ruleValue.Value = ruleValue.Value + value;
                                        ruleValue.Update(member.Name, "", null);
                                    }

                                    CampaignLogic.CampaignLogic.CreateAutomaticCampaignLog(receipt.ID, member.ID, ruleValue.ID, member.Name);

                                    if (campaign.FirstSubmissionDate == null)
                                        campaign.FirstSubmissionDate = receipt.TransactionDate;
                                    campaign.Point = campaign.Point + (action.Points * collect.QuantityApproved);

                                    isRuleTresholdAchieve = ruleValue.Value >= rule.Treshold;
                                }
                            }
                            listRuleAchievement.Add(rule.ID, new Tuple<bool, CAMPAIGN_RULE_OPERATOR>(isRuleTresholdAchieve, ruleOperator));
                        }

                        foreach (var ruleAchievement in listRuleAchievement)
                        {
                            isAchieve = ruleAchievement.Value.Item1;
                            if (ruleAchievement.Value.Item2 == CAMPAIGN_RULE_OPERATOR.AND)
                            {
                                isAchieve = isAchieve && ruleAchievement.Value.Item1;
                            }
                            else if (ruleAchievement.Value.Item2 == CAMPAIGN_RULE_OPERATOR.OR)
                            {
                                isAchieve = isAchieve || ruleAchievement.Value.Item1;
                            }
                        }

                        if (isAchieve)
                        {
                            campaign.IsAchieve = true;
                            if (_campaignType == CAMPAIGN_TYPE.PERCENTAGE)
                            {
                                bonusPoint = (campaign.Point * _campaign.Value) / 100;
                            }
                            else if (_campaignType == CAMPAIGN_TYPE.FIXEDNUMBER)
                            {
                                bonusPoint = _campaign.Value;
                            }
                            campaign.BonusPoint = bonusPoint;

                            var pointResult = AdjustmentLogic.AdjustPoint(bonusPoint, collect.PoolID, member.ID, "Point Automated Campaign" + campaign.Campaign.Name, receipt.TransactionDate.GetValueOrDefault(), member.Name);
                            if (pointResult != null && pointResult.StatusCode == "00")
                            {
                                ParamCreateNotification notificationParam = new ParamCreateNotification
                                {
                                    Type = NotificationType.Type.Information.ToString(),
                                    Message = "Selamat anda mendapatkan bonus point campaign.",
                                    Point = 0,
                                    Title = "Point Automated Campaign " + campaign.Campaign.Name
                                };
                                var notifResult = NotificationLogic.Create(notificationParam, member.ID);
                            }
                        }
                        campaign.Update(member.Name, "", null);
                    }
                }
                //else
                //{
                //    string typePercent = LoyaltyPointEngine.PointLogicLayer.CampaignLogic.CAMPAIGN_TYPE.PERCENTAGE.ToString();
                //    string typeFixNumber = LoyaltyPointEngine.PointLogicLayer.CampaignLogic.CAMPAIGN_TYPE.FIXEDNUMBER.ToString();
                //    int bonusPoint = 0;
                //    if (_campaign.Type == typePercent)
                //    {
                //        decimal percentval = ((decimal)_campaign.Value / 100) * action.Points;
                //        bonusPoint = action.Points + (int)percentval;
                //    }
                //    else if (_campaign.Type == typeFixNumber)
                //        bonusPoint = action.Points + _campaign.Value;
                //    AdjustmentLogic.AdjustPoint(bonusPoint, collect.PoolID, member.ID, string.Format("Bonus point Campaign {0} dengan Action code {1}", _campaign.ID, action.ID), receipt.TransactionDate.GetValueOrDefault(), member.Name);
                //}
            }
        }

        private static void BonusPointForChannelFirstSubmission(Member member, string channel, int timeStamp = 0, int lastPoint = 0, DateTime? createdDate = null)
        {
            bool isChannelFirstSubmission = Receipt.GetByMemberId(member.ID).Where(x => (x.Status == (int)Receipt.ReceiptStatus.Approve || x.Status == (int)Receipt.ReceiptStatus.OcrApproved) && x.Channel.Trim() == channel).Count() <= 1;
            string channelActionCode = "ChannelAction" + channel,
                channelMessageKey = "ChannelNotificationMessage" + channel,
                channelMessage = "Bonus Channel " + channel;

            var channelMessageSetting = Settings.GetByKey(channelMessageKey);
            if (channelMessageSetting != null)
                channelMessage = channelMessageSetting.Value;

            if (isChannelFirstSubmission)
            {
                var channelAction = Data.Action.GetByCode(channelActionCode);
                if (channelAction != null)
                {
                    var pointResult = Point.CollectLogic.InsertPointByActionIdAndMemberId(channelActionCode, member.ID, "", true, false, timeStamp + 10, lastPoint,
                        createdDate);
                    if (pointResult != null && pointResult.StatusCode == "00")
                    {
                        NotificationLogic.Create(member.ID, NotificationType.Type.Information, "Bonus Poin", channelMessage, "", pointResult.Value.Points, member.Name);
                    }
                }
            }
        }

        private static void BonusPointForChannelSubmissionDay(Member member, string channel, int point)
        {
            var SubmissionDayCampaign = new Campaign();
            SubmissionDayCampaign = Campaign.GetAll().Where(x => x.IsFirstSubmit == true && x.IsActive == true && x.IsDeleted == false).FirstOrDefault();
            var MaxReceipt = Convert.ToInt32(Settings.GetByKey("MaximumRedeemSubmission").Value);

            string channelActionCode = string.Empty, channelMessage = string.Empty;

            if (SubmissionDayCampaign != null)
            {
                var receiptSubmitted = Receipt.GetAll().Where(x => x.MemberID == member.ID && x.Status == (int)Receipt.ReceiptStatus.Approve && x.Channel.Trim() == channel && x.CreatedDate <= SubmissionDayCampaign.StartDate).Count();
                if (receiptSubmitted != 0)
                {
                    var ReceiptOnCampaignRange = Receipt.GetByMemberId(member.ID).Where(x => x.Status == (int)Receipt.ReceiptStatus.Approve && x.CampaignID == SubmissionDayCampaign.ID && x.CreatedDate >= SubmissionDayCampaign.StartDate && x.CreatedDate <= SubmissionDayCampaign.EndDate).Count();

                    if (ReceiptOnCampaignRange > 0)
                    {
                        var LastReceipt = Receipt.GetByMemberId(member.ID).Where(x => x.Status == (int)Receipt.ReceiptStatus.Approve).OrderByDescending(x => x.CreatedDate).Take(1).FirstOrDefault();

                        var CollectionInReceipt = from a in Receipt.GetByMemberId(member.ID)
                                                  join b in Data.Collect.GetAll() on a.ID equals b.ReceiptID
                                                  where a.Status == (int)Receipt.ReceiptStatus.Approve && a.Channel.Trim() == channel && a.CampaignID == SubmissionDayCampaign.ID && b.IsApproved == true && a.ID == LastReceipt.ID
                                                  select b;

                        int actionMappedToCampaign = 0;
                        if (CollectionInReceipt != null)
                        {
                            foreach (var item in CollectionInReceipt)
                            {
                                actionMappedToCampaign += Data.CampaignActionDetail.GetAllActiveByAction(item.ActionID ?? 0).Count();
                            }
                        }

                        if (LastReceipt.CampaignID == SubmissionDayCampaign.ID &&
                            LastReceipt.CreatedDate >= SubmissionDayCampaign.StartDate && LastReceipt.CreatedDate <= SubmissionDayCampaign.EndDate &&
                            actionMappedToCampaign > 0)
                        {
                            channelActionCode = "ExtraPoint" + channel;
                            var channelMessageSetting = Settings.GetByKey("SubmissionExtraPoint" + channel);
                            if (channelMessageSetting != null)
                                channelMessage = channelMessageSetting.Value + " " + SubmissionDayCampaign.Name.ToLower() + " channel " + channel;

                            var GetBonusCount = BalanceLog.GetByMemberId(member.ID).Where(x => x.Description.Contains(channelActionCode) && x.CreatedDate >= SubmissionDayCampaign.StartDate && x.CreatedDate <= SubmissionDayCampaign.EndDate).Count();
                            if (GetBonusCount < MaxReceipt)
                            {
                                var channelAction = Data.Action.GetByCode(channelActionCode);
                                if (channelAction != null)
                                {
                                    var pointResult = Point.CollectLogic.InsertPointByCampaignAndMemberId(SubmissionDayCampaign, channelActionCode, member.ID, "", true);
                                    if (pointResult != null && pointResult.StatusCode == "00")
                                    {
                                        NotificationLogic.Create(member.ID, NotificationType.Type.Information, "Bonus Poin", channelMessage, "", pointResult.Value.Points, member.Name);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static ResultPaginationModel<List<SP_GetGridReceipt_Result>> PublicGetAll(string channel, string spgCode, int? eventCode, int? status, string startDate, string endDate, string orderColumn, string orderDirection, string search, int page = 1, int pageSize = 10, int? RetailerId = null, String RetailerHasAddr = null)
        {
            ResultPaginationModel<List<SP_GetGridReceipt_Result>> result = new ResultPaginationModel<List<SP_GetGridReceipt_Result>>();

            DateTime tempStartDate = DateTime.MinValue,
                tempEndDate = DateTime.MinValue;

            DateTime? _startDate = null,
                _endDate = null;

            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempStartDate))
                    _startDate = tempStartDate;
                else
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Invalid value of startDate. Unable to parse into DateTime.";
                    return result;
                }
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempEndDate))
                    _endDate = tempEndDate;
                else
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Invalid value of endDate. Unable to parse into DateTime.";
                    return result;
                }
            }
            try
            {
                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.Value = DataRepositoryFactory.CurrentRepository
                    .SP_GetGridReceipt(null, null, channel, spgCode, status, _startDate, _endDate, page, pageSize, orderColumn, orderDirection, search, eventCode, RetailerId, RetailerHasAddr)
                    .ToList();
                result.TotalRow = DataRepositoryFactory.CurrentRepository.SP_GetGridReceiptTotalRow(null, null, channel, spgCode, status, _startDate, _endDate, page, pageSize, orderColumn, orderDirection, search, eventCode, RetailerId, RetailerHasAddr).FirstOrDefault().Value;
            }
            catch (Exception ex)
            {
                result.StatusCode = "50";
                result.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResultPaginationModel<List<GetReceiptReport_Result>> GetReceiptApprovalReport(string UserID, string StartDate, string EndDate, string orderColumn, string orderDirection, int page = 1, int pageSize = 10)
        {
            ResultPaginationModel<List<GetReceiptReport_Result>> result = new ResultPaginationModel<List<GetReceiptReport_Result>>();

            DateTime stDate = DateTime.MinValue;
            DateTime edDate = DateTime.MaxValue;
            DateTime? _startDate = null, _endDate = null;
            System.Data.Objects.ObjectParameter outPutTotalRow = new System.Data.Objects.ObjectParameter("TotalRow", typeof(Int64));

            long? CMSUserID = null;
            if (!string.IsNullOrEmpty(UserID))
            {
                CMSUserID = Convert.ToInt64(UserID);
            }

            if (!string.IsNullOrEmpty(StartDate))
            {
                if (DateTime.TryParseExact(StartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out stDate))
                {
                    _startDate = stDate;
                }
                else
                {
                    result.StatusCode = "41";
                    result.StatusMessage = "Invalid value of startDate. Unable to parse into DateTime.";
                    return result;
                }
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", CultureInfo.CurrentCulture);
            }

            if (!string.IsNullOrEmpty(EndDate))
            {
                if (DateTime.TryParseExact(EndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out edDate))
                {
                    _endDate = edDate;
                }
                else
                {
                    result.StatusCode = "42";
                    result.StatusMessage = "Invalid value of endDate. Unable to parse into DateTime.";
                    return result;
                }
            }
            else
            {
                _endDate = DateTime.ParseExact("31/12/2199", "dd/MM/yyyy", CultureInfo.CurrentCulture);
            }

            try
            {
                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.Value = DataRepositoryFactory.CurrentRepository
                    .GetReceiptReport(CMSUserID, _startDate, _endDate, page, pageSize, orderColumn, orderDirection, outPutTotalRow).ToList();
                result.TotalRow = Convert.ToInt32(outPutTotalRow.Value);
            }
            catch (Exception ex)
            {
                result.StatusCode = "50";
                result.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResultModel<List<string>> GetInactiveMemberReceipt()
        {
            ResultModel<List<string>> result = new ResultModel<List<string>>();

            var IDList = Receipt.GetInactiveMemberReceipt();
            if (IDList != null && IDList.Count > 0)
            {
                result.StatusCode = "00";
                result.StatusMessage = "success get data";
                result.Value = IDList;
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = "get data failed";
            }
            return result;
        }

        public static ResultModel<List<BottomPriceReceiptEmailModel>> GetBottomPriceReceiptEmail(DateTime? startDate, DateTime? endDate)
        {
            ResultModel<List<BottomPriceReceiptEmailModel>> result = new ResultModel<List<BottomPriceReceiptEmailModel>>();

            if (!startDate.HasValue)
            {
                startDate = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                //startDate = DateTime.ParseExact("16/08/2018 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }
            if (!endDate.HasValue)
            {
                endDate = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                //endDate = DateTime.ParseExact("20/08/2018 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }

            var data = GetBottomPriceReceiptModel(DataRepositoryFactory.CurrentRepository.GetBottomPriceReceiptReport(startDate, endDate, 1, 0, "", "").ToList());

            if (data != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = "success get data";
                result.Value = data;
            }
            else
            {
                result.StatusCode = "30";
                result.StatusMessage = "failed to get data";
            }

            return result;
        }

        public static List<BottomPriceReceiptEmailModel> GetBottomPriceReceiptModel(List<GetBottomPriceReceiptReport_Result> data)
        {
            if (data.Any())
            {
                string linkReceipt = Data.Settings.GetByKey("ReceiptImageLink").Value;
                var datas = from a in data
                            select new BottomPriceReceiptEmailModel
                            {
                                Date = a.TransactionDate.HasValue ? a.TransactionDate.Value.ToString("dd/MM/yyyy") : "",
                                Account = a.RetailerName,
                                SKU = a.SKUName,
                                Price = a.Price.ToString("0"),
                                MinimumPrice = a.MinimumPrice.ToString(),
                                ReceiptID = a.ReceiptID.ToString(),
                                Evidence = linkReceipt + a.Image.ToString()
                            };
                return datas.ToList();
            }
            else
            {
                return null;
            }
        }

        public static ResultModel<bool> ReStatusUnverifyReceipt(string MemberPhone, string By, string url, long UserID)
        {
            var res = new ResultModel<bool>();
            res.StatusCode = "00";
            res.StatusMessage = "Success";
            res.Value = false;

            //Validasi Member
            Member OKMember = new Member();
            try
            {
                OKMember = Member.GetByPhone(MemberPhone);
            }
            catch (Exception ex)
            {
                res.StatusCode = "51";
                res.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            if (OKMember != null)
            {
                string assetBaseUrl = Settings.GetValueByKey(SiteSetting.ASSET_URL);

                //Search receipt by Member n Status Unverify -2
                List<Receipt> receiptUnverify = Receipt.GetByMemberId(OKMember.ID).Where(x => x.Status == (int)Receipt.ReceiptStatus.Unverify || x.Status == (int)Receipt.ReceiptStatus.WaitingUnverify).ToList();
                foreach (var Restatus in receiptUnverify)
                {
                    TransactionOptions transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        try
                        {
                            if (Restatus.Status == (int)Receipt.ReceiptStatus.WaitingUnverify)
                            {
                                Restatus.Status = (int)Receipt.ReceiptStatus.WaitingVerify;
                            }
                            else
                            {
                                Restatus.Status = (int)Receipt.ReceiptStatus.Waiting;
                            }
                            Restatus.UpdateReceipt(By, url, UserID);

                            if (Restatus.Status == (int)Receipt.ReceiptStatus.WaitingVerify)
                            {
                                // Insert Point Notification for WaitingVerify
                                ResultModel<Model.Object.Notification.NotificationModel> resultNotif = NotificationLogic.Create(Restatus.MemberID, NotificationType.Type.OnProgress, "Penambahan Point",
                                                         "No Transaksi: " + Restatus.Code, Restatus.Code, Restatus.Collect.Sum(x => x.TotalPoints), By);
                            }
                        }
                        catch (Exception ex)
                        {
                            transScope.Dispose();
                            res.StatusCode = "51";
                            res.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                        }

                        transScope.Complete();

                        res.StatusCode = "00";
                        res.StatusMessage = "Success Update Restatus Receipt";
                    }

                    res.Value = true;
                }
            }
            else
            {
                res.StatusCode = "50";
                res.StatusMessage = "Member not found";
            }

            return res;
        }
    }
}
