﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Adjustment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Data;
using System.Transactions;

namespace LoyaltyPointEngine.PointLogicLayer
{
    public class AdjustmentLogic
    {
        public static ResultModel<AdjustmentModel> GetAdjustmentModelById(string idS)
        {
            ResultModel<AdjustmentModel> result = new ResultModel<AdjustmentModel>();

            Guid ID = Guid.Empty;
            if (Guid.TryParse(idS, out ID))
            {
                var data = LoyaltyPointEngine.Data.Adjustment.GetById(ID);
                if (data != null)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get Adjustment with ID {0}", ID);
                    result.Value = new AdjustmentModel(data);
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = "Id not found in the system";
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Id does not match the format";
            }
            return result;
        }

        public static ResultModel<AdjustmentModel> AdjustPoint(int point, int poolId, int memberId, string reason, DateTime transactionDate, string by)
        {
            ResultModel<AdjustmentModel> result = new ResultModel<AdjustmentModel>();

            Guid ID = Guid.Empty;
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                var adjustment = new LoyaltyPointEngine.Data.Adjustment
                {
                    ID = Guid.NewGuid(),
                    Code = GeneratedCode(by),
                    ExpiredDate = DateTime.Today.AddMonths(3),
                    Point = point,
                    PoolID = poolId,
                    MemberID = memberId,
                    Reason = reason,
                    TransactionDate = transactionDate
                };
                var ressEF = adjustment.Insert(by, null);
                if (ressEF.Success)
                {
                    Data.Collect collect = new Data.Collect();
                    collect.ID = Guid.NewGuid();
                    collect.MemberID = memberId;
                    collect.Quantity = 1;
                    collect.QuantityApproved = 1;
                    collect.Points = point;
                    collect.TotalPoints = point;
                    collect.Remarks = reason;
                    collect.TransactionDate = transactionDate;
                    collect.AdjustmentReferenceID = adjustment.ID;
                    collect.ExpiryDate = adjustment.ExpiredDate;
                    collect.PoolID = poolId;
                    collect.IsApproved = true;
                    collect.IsActive = true;
                    collect.IsUsed = false;
                    var res = collect.Insert(by, "", null, 0);
                    if (res.Success)
                    {
                        result.StatusCode = "00";
                        LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Credit(adjustment.Point, reason, memberId, collect.PoolID, by, collect.ID, 0);
                        transScope.Complete();
                    }
                    else
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = res.ErrorMessage;
                    }
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = ressEF.ErrorMessage;
                }
            }
            return result;
        }

        private static string GeneratedCode(string by)
        {
            return Data.Number.Generated(Number.Type.ADJ, by);
        }
    }
}
