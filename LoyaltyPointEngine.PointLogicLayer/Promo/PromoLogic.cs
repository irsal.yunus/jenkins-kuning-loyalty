﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Object.Promo;
using LoyaltyPointEngine.Model.Parameter.Member;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LoyaltyPointEngine.PointLogicLayer.Promo
{
    public class PromoLogic
    {
        public static ResultModel<PromoParamAPI> PromoBySMS(PromoParamAPI param)
        {
            ResultModel<PromoParamAPI> result = new ResultModel<PromoParamAPI>();

            Data.PromoDetails pmd = new Data.PromoDetails();

            DataMember.Member member = new DataMember.Member();

            bool isNewMember = false;

            try
            {
                if (string.IsNullOrEmpty(param.Phone))
                {
                    result.StatusCode = "201";
                    result.StatusMessage = string.Format("Phone is required");
                    return result;
                }

                if (!PhoneHelper.isPhoneValid(param.Phone))
                {
                    result.StatusCode = "201";
                    result.StatusMessage = string.Format("Maaf, Format No Handphone yang dimasukan salah, silahkan masukan dengan format 08xxxxxxxxxx");
                    return result;
                }

                if (string.IsNullOrEmpty(param.PromoCode))
                {
                    result.StatusCode = "203";
                    result.StatusMessage = string.Format("Promo Code Not Found");
                    return result;
                }

                bool isPromoNeedDomisili = Data.PromoDetails.GetAll()
                    .Where(x => x.Promo.PromoCode == param.PromoCode)
                    .Any(x => x.DistrictID.HasValue);

                if (isPromoNeedDomisili)
                {
                    bool isDomisiliExist = DataMember.District.GetAll()
                        .Any(x => !x.IsDeleted && x.ID == param.DomisiliID);
                    if (!isDomisiliExist)
                    {
                        result.StatusCode = "204";
                        result.StatusMessage = string.Format("Nama kota tidak ditemukan. Contoh penulisan kota yang benar: SURABAYA");
                        return result;
                    }
                }

                if (!string.IsNullOrEmpty(param.SpgCode))
                {
                    bool isSpgCodeValid = DataMember.SPG.GetAll()
                        .Any(x => x.Code == param.SpgCode && !x.IsDeleted && (x.IsActive.HasValue && x.IsActive.Value));
                    if (!isSpgCodeValid)
                    {
                        result.StatusCode = "205";
                        result.StatusMessage = string.Format("Kode SPG tidak terdaftar");
                        return result;
                    }
                }

                //string currentChannel = "sms",
                //    channelPromoCodeKeyPrefix = "PromoCode";
                //var channelPromoCodeKeyList = EnumHelper.ToArrayString<ChannelType>().Select(x => string.Format("{0}{1}", channelPromoCodeKeyPrefix, x.ToLower().FirstCharToUpper().Trim())).ToArray();
                //var channelPromoCodeSettingList = Data.Settings.GetValuesByKeys(channelPromoCodeKeyList);
                //var channelPromoCodeSettingByPromoCode = channelPromoCodeSettingList.Where(x => x.Value.Trim().ToLower().Contains(param.PromoCode.Trim().ToLower())).FirstOrDefault();
                //if (!channelPromoCodeSettingByPromoCode.Equals(default(KeyValuePair<string, string>)))
                //{
                //    currentChannel = channelPromoCodeSettingByPromoCode.Key.Replace(channelPromoCodeKeyPrefix, string.Empty);
                //}

                string currentChannel = "sms_promo";
                var promo = Data.Promo.GetByCode(param.PromoCode);
                //if (promo != null)
                //{
                //    ChannelType _currentChannel = (ChannelType)promo.ChannelId.GetValueOrDefault(15);
                //    currentChannel = _currentChannel.ToString();
                //}komen_tono

                string cekMember = "Member Existing";
                member = DataMember.Member.GetByPhone(param.Phone);
                if (member == null)
                {
                    isNewMember = true;

                    #region AddNewMember
                    //if (string.IsNullOrEmpty(param.Channel))
                    //{
                    //    param.Channel = currentChannel.ToLower();
                    //}komen_tono

                    param.Channel = ChannelType.sms_promo.ToString();

                    string newPassword = "", tsMSG = "", dtg = DateTime.Now.ToString("yyyyMMddHHmmssss");
                    AddMemberSMSParameterModel par = new AddMemberSMSParameterModel
                    {
                        Name = param.Nama,
                        ChildDateOfBirth = param.DOBAnak,
                        Phone = param.Phone,
                        SMSID = string.Format("PromoSMS{0}", dtg),
                        SPGCode = param.SpgCode,
                        DistrictID = param.DomisiliID
                    };
                    var resultMem = MemberLogic.Sms(par, "PromoSMS", out newPassword, "PromoSMS", param.Channel);
                    if (resultMem.StatusCode != "00")
                    {
                        param.MsgRespon2 = resultMem.StatusMessage;
                        result.StatusCode = resultMem.StatusCode;
                        result.Value = param;
                        return result;

                    }
                    else
                    {
                        tsMSG = string.Format("Success, {0}|{1}|{2}", tsMSG, result.StatusCode, result.StatusMessage);
                        var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                        using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                        {

                            PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.REGSMS, resultMem.Value, "PromoSMS");
                            transScope.Complete();
                        }

                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(resultMem.Value.ID, "PromoSMS");
                        cekMember = "New Member";

                        result.StatusCode = resultMem.StatusCode;
                        param.MsgRespon2 = resultMem.StatusMessage;
                        if (resultMem.StatusCode == "500")
                            param.MsgRespon2 = "Sorry, seems like our system has enocuntered a problem, please try again later";
                        result.Value = param;
                    }
                    #endregion
                }
                member = new DataMember.Member();
                member = DataMember.Member.GetByPhone(param.Phone);
                Guid id = Guid.NewGuid();
                string By = "Null";
                if (member != null)
                {
                    bool isBelow1YearOrPregnant = MemberLogic.IsMemberChildBelow1YearOrPregnant(member.ID);

                    #region Add New Activity
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        var pm = Data.Promo.GetByCode(param.PromoCode);
                        if (pm != null)
                        {
                            if (isBelow1YearOrPregnant)
                            {
                                transScope.Dispose();
                                result.StatusCode = "205";  ////Revisi By Ucup jika usia anak Under 1 Year
                                result.StatusMessage += pm.MsgResponseUnderOneYear;
                                //result.StatusMessage = MemberLogic.ReceiptRejectedMessage(isBelow1YearOrPregnant);
                                return result;
                            }

                            DateTime dts = DateTime.Now;
                            string expiredDate = string.Empty;
                            double interval = 0;
                            if (!string.IsNullOrEmpty(pm.interval) && double.TryParse(pm.interval, out interval))
                            {
                                expiredDate = DateTime.Now.AddDays(interval).ToString("ddMMMM");
                            }
                            if (pm.StartDate <= dts && pm.EndDate >= dts)
                            {
                                if (pm.IsVoucher.Value) //Jika pakai kode voucher
                                {
                                    if (pm.IsCheck.Value)
                                    {
                                        #region Need Cek
                                        //if (string.IsNullOrEmpty(param.KodeUnik))
                                        //{
                                        //    transScope.Dispose();
                                        //    result.StatusCode = "204";
                                        //    result.StatusMessage += SiteSetting.MsgResponseFormatPromoIscheckTrue;
                                        //    return result;
                                        //}
                                        pmd = Data.PromoDetails.GetByCode(param.KodeUnik);
                                        if (pmd != null)
                                        {
                                            if (pmd.ExpiredDate != null && pmd.ExpiredDate < DateTime.Now)
                                            {
                                                result.StatusCode = "206";
                                                result.StatusMessage += pm.MsgResponseFailedUnikKodeExp.Replace("[xxx]", param.KodeUnik).Replace("[ddMMMM]", expiredDate);
                                                return result;
                                            }
                                            if (!pmd.isUsed)
                                            {
                                                Data.PromoActivity pma = new Data.PromoActivity();
                                                pma.MemberID = member.ID;
                                                pma.PromoID = pm.ID;
                                                pma.PromoDetailsID = pmd.ID;
                                                pma.SMSID = "PromoBy" + currentChannel;
                                                pma.UniqueID = id;
                                                pma.StatusMsg = "";
                                                pma.IsGetPromo = false;

                                                if (!pm.Status.Equals("New Member") || isNewMember)
                                                {
                                                    pma.IsGetPromo = true;
                                                }

                                                var rs = pma.Insert("promo", currentChannel, null);
                                                if (rs.Success)
                                                {
                                                    if (!pm.Status.Equals("New Member") || isNewMember)
                                                    {
                                                        pmd.isUsed = true;
                                                        pma.IsGetPromo = true;
                                                    }
                                                    var ret = pmd.Update(currentChannel, "ApiPromo", null);
                                                    if (ret.Success)
                                                    {
                                                        string pmdRetailerName = (pmd.Retailer != null ? pmd.Retailer.Name : "");
                                                        transScope.Complete();
                                                        result.StatusCode = "00";
                                                        if (cekMember == promo.Status) //Msg special member
                                                        {
                                                            result.StatusMessage += pm.MsgResponseForStatus
                                                                .Replace("[xxx]", pmd.CodeName)
                                                                .Replace("[yyy]", pmdRetailerName)
                                                                .Replace("[zzz]", pmd.ExpiredPromo);
                                                        }
                                                        else //Msg 
                                                        {
                                                            result.StatusMessage += pm.MsgResponse.Replace("[xxx]", pmd.CodeName).Replace("[ddMMMM]", expiredDate);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        result.StatusCode = "444";
                                                        result.StatusMessage += "Failed Update Promo Detail";
                                                    }
                                                }
                                                else
                                                {
                                                    result.StatusCode = "444";
                                                    result.StatusMessage += "Failed Update promo";
                                                }

                                            }
                                            else
                                            {
                                                result.StatusCode = "444";
                                                result.StatusMessage += pm.MsgResponseFailedUnikKodeUsed.Replace("[xxx]", param.KodeUnik).Replace("[ddMMMM]", expiredDate);
                                            }

                                        }
                                        else
                                        {
                                            transScope.Dispose();
                                            result.StatusCode = "205";
                                            result.StatusMessage += pm.MsgResponseFailedUnikKodeNotFound.Replace("[xxx]", param.KodeUnik).Replace("[ddMMMM]", expiredDate);
                                            return result;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region not Check
                                        var cmf = Data.PromoActivity.GetAll();
                                        Data.PromoActivity pma = Data.PromoActivity.GetAll().Where(x => x.MemberID == member.ID && x.PromoID == pm.ID).FirstOrDefault();
                                        if (pma != null)
                                        {
                                            transScope.Dispose();
                                            result.StatusCode = "205";   ////Revisi By Ucup jika lebih dari 1 kali Member
                                            result.StatusMessage += pm.MsgResponseFailedUnikKodeUsed.Replace("[xxx]", pmd.CodeName).Replace("[ddMMMM]", expiredDate);
                                            return result;
                                        }

                                        pmd = Data.PromoDetails.GetAll()
                                            .Where(x =>
                                                x.IDPromo == pm.ID
                                                && !x.isUsed
                                                && EntityFunctions.TruncateTime(x.ExpiredDate) >= EntityFunctions.TruncateTime(DateTime.Now)
                                                && (x.DistrictID == param.DomisiliID || param.DomisiliID == 0)
                                             ).FirstOrDefault();

                                        if (pmd != null)
                                        {
                                            pma = new Data.PromoActivity();
                                            pma.MemberID = member.ID;
                                            pma.PromoID = pm.ID;
                                            pma.SMSID = "PromoBy" + currentChannel;
                                            pma.StatusMsg = "";
                                            pma.PromoDetailsID = pmd.ID;
                                            pma.IsGetPromo = false;

                                            if (!pm.Status.Equals("New Member") || isNewMember)
                                            {
                                                pma.IsGetPromo = true;
                                            }

                                            var rs = pma.Insert("promo", currentChannel, null);
                                            if (rs.Success)
                                            {
                                                if (!pm.Status.Equals("New Member") || isNewMember)
                                                {
                                                    pmd.isUsed = true;
                                                }
                                                var ret = pmd.Update(currentChannel, "ApiPromo", null);
                                                if (ret.Success)
                                                {
                                                    string pmdRetailerName = (pmd.Retailer != null ? pmd.Retailer.Name : "");
                                                    transScope.Complete();
                                                    result.StatusCode = "00";
                                                    if (cekMember == promo.Status) //Msg special member
                                                    {
                                                        result.StatusMessage += pm.MsgResponseForStatus
                                                            .Replace("[xxx]", pmd.CodeName)
                                                            .Replace("[yyy]", pmdRetailerName)
                                                            .Replace("[zzz]", pmd.ExpiredPromo);
                                                    }
                                                    else //Msg 
                                                    {
                                                        result.StatusMessage += pm.MsgResponse.Replace("[xxx]", pmd.CodeName).Replace("[ddMMMM]", expiredDate);
                                                    }
                                                }
                                                else
                                                {
                                                    transScope.Dispose();
                                                    result.StatusCode = "205";
                                                    result.StatusMessage += "Failed update Kode Unik";
                                                }
                                            }
                                            else
                                            {
                                                result.StatusCode = "444";
                                                result.StatusMessage += "Failed insert promo";
                                            }
                                        }
                                        else
                                        {
                                            transScope.Dispose();
                                            result.StatusCode = "205";
                                            result.StatusMessage += pm.MsgResponseFailedUnikKodeNotFound.Replace("[xxx]", param.PromoCode).Replace("[ddMMMM]", expiredDate);
                                            return result;
                                        }
                                        #endregion
                                    }
                                }
                                else //Jika tidak pakai kode voucher
                                {
                                    #region not used code voucher
                                    var cmf = Data.PromoActivity.GetAll();
                                    Data.PromoActivity pma = Data.PromoActivity.GetAll().Where(x => x.MemberID == member.ID && x.PromoID == pm.ID).FirstOrDefault();
                                    if (pma != null)
                                    {
                                        transScope.Dispose();
                                        result.StatusCode = "205";   ////Revisi By Ucup jika lebih dari 1 kali Member
                                        result.StatusMessage += pm.MsgResponseFailedUnikKodeUsed.Replace("[xxx]", pmd.CodeName).Replace("[ddMMMM]", expiredDate);
                                        return result;
                                    }

                                    pma = new Data.PromoActivity();
                                    pma.MemberID = member.ID;
                                    pma.PromoID = pm.ID;
                                    if (!string.IsNullOrEmpty(currentChannel))
                                    {
                                        pma.SMSID = "PromoBy" + currentChannel;
                                        By = currentChannel;
                                    }
                                    else
                                    {
                                        pma.SMSID = "PromoBySMS";
                                        By = "sms";
                                    }
                                    pma.StatusMsg = "";
                                    pma.PromoDetailsID = pmd.ID;
                                    pma.IsGetPromo = true;
                                    var rs = pma.Insert(By, By, null);
                                    if (rs.Success)
                                    {
                                        transScope.Complete();
                                        result.StatusCode = "00";
                                        if (cekMember == promo.Status) //Msg special member
                                        {
                                            result.StatusMessage += pm.MsgResponseForStatus.Replace("[xxx]", param.KodeUnik).Replace("[ddMMMM]", expiredDate);
                                        }
                                        else //Msg 
                                        {
                                            result.StatusMessage += pm.MsgResponse.Replace("[xxx]", param.KodeUnik).Replace("[ddMMMM]", expiredDate);
                                        }
                                    }
                                    else
                                    {
                                        result.StatusCode = "444";
                                        result.StatusMessage += "Failed insert promo";
                                    }

                                    #endregion
                                }
                            }
                            else
                            {
                                if (pm.StartDate >= dts)
                                {
                                    transScope.Dispose();
                                    result.StatusCode = "205";  ///revisi By ucup
                                    result.StatusMessage += "Maaf promo " + pmd.CodeName + " belum dimulai. Terus ikuti promo menarik Bebelac, hub Careline Bebelac Indonesia di 08104001233.";
                                }
                                if (pm.EndDate <= dts)
                                {
                                    transScope.Dispose();
                                    result.StatusCode = "205";  ////Revisi By Ucup jika promo telah expired
                                    result.StatusMessage += pm.MsgResponseFailedUnikKodeExp.Replace("[xxx]", pm.PromoCode).Replace("[ddMMMM]", expiredDate);
                                }
                            }


                        }
                        else
                        {
                            transScope.Dispose();
                            result.StatusCode = "205"; ///revisi
                            result.StatusMessage += "Maaf keyword promo tidak ditemukan. Mohon cek kembali atau hub Careline Bebelac Indonesia di 08104001233.";
                        }
                        transScope.Dispose();

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static ResultModel<PromoModel> InsertFromCMS(PromoModel model, int poolID, string By, string url, long? userID)
        {
            DateTime strDate, endDate;
            ResultModel<PromoModel> result = new ResultModel<PromoModel>();
            #region Validasi
            strDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StartDate))
            {
                string datetime = model.StartDate + " 00:00:01";
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    strDate = dt;
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Format Start date is not correct, please using format dd/MM/yyyy");
                    return result;
                }

            }
            endDate = DateTime.MaxValue;
            if (!string.IsNullOrEmpty(model.EndDate))
            {
                string datetime = model.EndDate + " 23:59:59";
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    endDate = dt;
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = string.Format("Format End date is not correct, please using format dd/MM/yyyy");
                    return result;
                }
            }
            #endregion

            Data.Promo promo = Data.Promo.GetByCode(model.PromoCode);
            if (promo != null)
            {
                result.StatusCode = "500";
                result.StatusMessage = string.Format("Promo Code \"" + model.PromoCode + "\" Sudah Ada Pada Data Base.");
                return result;
            }
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                promo = new Data.Promo();
                promo.PromoCode = model.PromoCode;
                promo.StartDate = strDate;
                promo.EndDate = endDate;
                promo.IsActive = model.IsActive;
                promo.IsCheck = model.IsCheck;
                promo.MsgResponse = model.MsgResponse;
                promo.MsgResponseFailedUnikKodeExp = model.MsgResponseFailedUnikKodeExp;
                promo.MsgResponseFailedUnikKodeNotFound = model.MsgResponseFailedUnikKodeNotFound;
                promo.MsgResponseFailedUnikKodeUsed = model.MsgResponseFailedUnikKodeUsed;
                promo.MsgResponseUnderOneYear = model.MsgResponseUnderOneYear;
                promo.UpdateBy = By;
                promo.IsVoucher = model.IsVoucher;
                promo.Status = model.Status;
                promo.MsgResponseForStatus = model.MsgResponseStatus;
                promo.UpdateDate = DateTime.Now;
                promo.CreatedBy = By;
                promo.CreatedDate = DateTime.Now;
                promo.Description = model.Description;
                promo.ChannelId = model.ChannelId;
                var res = promo.Insert(By, url, userID);
                if (res.Success)
                {
                    transScope.Complete();
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("Promo Success Inserted");
                }
                else
                {
                    result.StatusCode = "400";
                    result.StatusMessage = string.Format("Promo Failed Inserted");
                }

                transScope.Dispose();
            }
            return result;
        }
    }
}
