﻿using LoyaltyPointEngine.Data;
using System;
using System.Collections.Generic;

namespace LoyaltyPointEngine.PointLogicLayer.CampaignLogic
{
    public class CampaignLogic
    {
        public static bool IsAutomaticCampaignParticipant(int memberId, int campaignId)
        {
            return Data.AutomaticCampaignParticipant.IsAutomaticCampaignParticipant(memberId, campaignId);
        }

        public static IEnumerable<AutomaticCampaignActionRule> GetActionRule(int actionId, int campaignId)
        {
            return AutomaticCampaignActionRule.GetByCampaignIDAndActionID(campaignId, actionId);
        }

        public static IEnumerable<AutomaticCampaignValue> GetActionRuleValues(int campaignId, int actionId, int memberId)
        {
            return AutomaticCampaignValue.GetAllByActionIDndCampaignIDAndMemberID(actionId, campaignId, memberId);
        }

        public static AutomaticCampaignValue GetActionRuleTotalValues(int campaignId, int actionId, int memberId, int ruleId)
        {
            return AutomaticCampaignValue.GetAllByActionIDAndRuleIDAndCampaignIDAndMemberID(actionId, ruleId, campaignId, memberId);
        }

        public static void CreateAutomaticCampaignLog(Guid receiptId, int memberId, int valueId, string by)
        {
            AutomaticCampaignReceiptLog log = new AutomaticCampaignReceiptLog
            {
                ReceiptId = receiptId,
                MemberID = memberId,
                ValueID = valueId
            };
            log.Insert(by, "", null);
        }
    }

    public enum CAMPAIGN_TYPE
    {
        PERCENTAGE,
        FIXEDNUMBER,
    }

    public enum CAMPAIGN_RULE_OPERATOR
    {
        AND,
        OR,
        NONE
    }

    public enum CAMPAIGN_RULE_TYPE
    {
        SIZE,
        QUANTITY,
        PRICE,
        NONE
    }
}
