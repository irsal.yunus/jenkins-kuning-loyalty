﻿using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Chatbot;
using LoyaltyPointEngine.Model.Parameter.Chatbot;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Xml;

namespace LoyaltyPointEngine.PointLogicLayer.Chatbot
{
    public class ChatbotLogic
    {
        public static ResultModel<bool> ChatbotRatingAdd(ParamChatbotRating param)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                ChatbotRating objChatbotRating = new ChatbotRating();
                if(param == null)
                {
                    result.StatusCode = "01";
                    result.StatusMessage = "param null";
                    result.Value = false;
                    return result;
                  
                }
                if (string.IsNullOrEmpty(param.Phone) || !PhoneHelper.isPhoneValid(param.Phone))
                {
                    result.StatusCode = "01";
                    result.StatusMessage = "param phone null or invalid";
                    result.Value = false;
                    return result;
                }

               
                //check exist                
                objChatbotRating = ChatbotRating.GetByPhone(param.Phone);
                if (objChatbotRating != null)
                {
                    //edit
                    objChatbotRating.Rating = param.Rating;
                    objChatbotRating.Inputs = param.Input;

                    objChatbotRating.Update(param.Phone, "ChatbotLogic/ChatbotRatingAdd", null);
                }
                else
                {
                    //insert
                    ChatbotRating newChatbotRating = new ChatbotRating();
                    newChatbotRating.NoHP = param.Phone;
                    newChatbotRating.Rating = param.Rating;
                    newChatbotRating.Inputs = param.Input;

                    newChatbotRating.Insert(param.Phone, "ChatbotLogic/ChatbotRatingAdd", null);
                }
                result.StatusCode = "00";
                result.StatusMessage = "rating submitted";
                result.Value = true;
                return result;
            }
            catch (Exception ex)
            {
                result.StatusCode = "03";
                result.StatusMessage = "Error while trying to submit";
                result.Value = false;
                return result;
            }
        }        
    }
}

