﻿using log4net;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.OCR;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using LoyaltyPointEngine.PointLogicLayer.Action;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using System.Web;
using System.Text.RegularExpressions;
using LoyaltyPointEngine.Model.Parameter.Snapcart;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Data.Common.EntitySql;
using static LoyaltyPointEngine.Data.Receipt;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.PointLogicLayer.OCR
{
    public class OCRLogic : MainLogic
    {
        static OCRLogic()
        {
            Log = LogManager.GetLogger(typeof(OCRLogic));
        }

        public static ResultModel<List<OCRTemplateModel>> OCRGetStoreName()
        {
            ResultModel<List<OCRTemplateModel>> result = new ResultModel<List<OCRTemplateModel>>();
            List<OCRTemplate> listTemplate = OCRTemplate.GetAll().ToList();
            List<OCRTemplateModel> listTemplateModel = new List<OCRTemplateModel>();
            foreach (OCRTemplate item in listTemplate)
            {
                OCRTemplateModel objModel = new OCRTemplateModel(item);
                List<ProductInTemplate> listProductInTemplate = new List<ProductInTemplate>();
                //get template mapping
                List<OCRActionTemplateMapping> listMapping = OCRActionTemplateMapping.GetByTemplateId((int)item.ID).ToList();

                foreach (OCRActionTemplateMapping itemMapping in listMapping)
                {
                    ProductInTemplate obj = new ProductInTemplate();
                    obj.ProductName = itemMapping.ProductNameOnOCR;
                    listProductInTemplate.Add(obj);
                }
                objModel.listProductInTemplate = listProductInTemplate;
                listTemplateModel.Add(objModel);
            }

            result.StatusCode = "00";
            result.StatusMessage = "Sukses ambil data";
            result.Value = listTemplateModel.OrderBy(x => x.StoreName).ToList();
            return result;
        }

        public static ResultModel<OCRResult> ScanOcr(string inputOCR)
        {
            ResultModel<OCRResult> result = new ResultModel<OCRResult>();
            OCRResult resultOCR = new OCRResult();
            List<OCRReceipt> listProducts = new List<OCRReceipt>();
            List<OCRTemplate> templateList = OCRTemplate.GetAll().ToList(),
                calculatedTemplateList = new List<OCRTemplate>();

            string TextResult = string.Empty;

            //TextResult = inputOCR.Replace("\\n", "\r\n");
            string tempResult = inputOCR.Replace("\n", "#|#|#");
            string[] textArray = tempResult.Split(new string[] { "#|#|#" }, StringSplitOptions.RemoveEmptyEntries);

            OCRTemplate listTemplate = SeekTemplate(textArray, templateList);
            List<OCRReceipt> listScannedProduct = new List<OCRReceipt>();

            resultOCR.StoreName = listTemplate.StoreName;
            resultOCR.TransactionDate = GetDate(textArray, listTemplate);
            //if (string.IsNullOrEmpty(resultOCR.TransactionDate))
            //{
            //    result.StatusCode = "01";
            //    result.StatusMessage = string.Format("Tanggal transaksi tidak di ketemukan");
            //    return result;
            //}

            resultOCR.TransactionId = GetRef(tempResult, listTemplate);
            //if (string.IsNullOrEmpty(resultOCR.TransactionId))
            //{
            //    result.StatusCode = "02";
            //    result.StatusMessage = string.Format("Id transaksi tidak di ketemukan");
            //    return result;
            //}

            listScannedProduct = SeekProduct(textArray, listTemplate);
            //if (listScannedProduct == null || listScannedProduct.Count <= 0)
            //{
            //    result.StatusCode = "03";
            //    result.StatusMessage = string.Format("Produk tidak di ketemukan");
            //    return result;
            //}

            resultOCR.ListProduct = listScannedProduct;

            UploadReceiptModel uploadReceiptObj = new UploadReceiptModel();
            uploadReceiptObj.TransactionDate = resultOCR.TransactionDate;
            uploadReceiptObj.Remark = string.Empty;
            uploadReceiptObj.RetailerAddress = string.Empty;
            List<UploadReceiptDetail> detailObj = new List<UploadReceiptDetail>();

            foreach (OCRReceipt item in listScannedProduct)
            {
                UploadReceiptDetail obj = new UploadReceiptDetail();
                obj.ActionID = item.ActionId;
                obj.Quantity = item.Quantity;
                detailObj.Add(obj);
            }
            uploadReceiptObj.Details = detailObj;

            //ResultModel<ReceiptModel> resultUpload = ReceiptLogic.OCRUpload(uploadReceiptObj, by, memberId, poolID);

            //result.StatusCode = resultUpload.StatusCode;
            //result.StatusMessage = string.Format(resultUpload.StatusMessage);
            result.StatusCode = "00";
            result.StatusMessage = string.Format("Sukses scan");
            result.Value = resultOCR;

            return result;
        }

        private static OCRTemplate SeekTemplate(string[] bodyTextArray, List<OCRTemplate> templateList)
        {
            Dictionary<long, List<Tuple<string, double>>> calculatedTemplateDebug = new Dictionary<long, List<Tuple<string, double>>>();
            Dictionary<long, List<double>> calculatedTemplate = new Dictionary<long, List<double>>();
            foreach (OCRTemplate item in templateList)
            {
                List<Tuple<string, double>> calculatedListDebug = new List<Tuple<string, double>>();
                List<double> calculatedList = new List<double>();

                foreach (string row in bodyTextArray)
                {
                    double calculated = StringHelper.CalculateSimilarity(row.OnlyAlphaNumeric().Trim().ToLower(), item.StoreName.Trim().ToLower());
                    if (row.OnlyAlphaNumeric().Trim().ToLower().Contains(item.StoreName.Trim().ToLower()) || calculated >= 0.7)
                    {
                        calculated = 10;
                    }
                    calculatedListDebug.Add(new Tuple<string, double>(row.OnlyAlphaNumeric(), calculated));
                    calculatedList.Add(calculated);
                }

                calculatedTemplateDebug.Add(item.ID, calculatedListDebug.OrderByDescending(x => x.Item2).ToList());
                calculatedTemplate.Add(item.ID, calculatedList.OrderByDescending(x => x).ToList());
            }

            var predictedTemplate = calculatedTemplate.OrderByDescending(x => x.Value.FirstOrDefault()).FirstOrDefault();
            return templateList.FirstOrDefault(x => x.ID == predictedTemplate.Key);
        }

        private static List<OCRReceipt> SeekProduct(string[] bodyTextArray, OCRTemplate templateChoosen)
        {
            List<OCRReceipt> result = new List<OCRReceipt>();
            int temp = 0;

            //get product mapping from template
            List<OCRActionTemplateMapping> listProductFromTemplate = OCRActionTemplateMapping.GetByTemplateId((int)templateChoosen.ID).ToList();

            foreach (string item in bodyTextArray)
            {
                foreach (OCRActionTemplateMapping itemTemplateMapping in listProductFromTemplate)
                {
                    OCRReceipt tempObj = new OCRReceipt();
                    //get product data from action
                    LoyaltyPointEngine.Data.Action actionObj = LoyaltyPointEngine.Data.Action.GetById((int)itemTemplateMapping.ActionId);
                    double calculated = StringHelper.CalculateSimilarity(itemTemplateMapping.ProductNameOnOCR.ToLower(), item.OnlyAlpha().ToLower());
                    if (item.OnlyAlphaNumeric().Trim().ToLower().Contains(itemTemplateMapping.ProductNameOnOCR.Trim().ToLower()) || calculated >= 0.5)
                    {
                        if (templateChoosen.IsMultiLine == 1)
                        {
                            string tempPrice = bodyTextArray[temp + (int)templateChoosen.MultiLinePositionPrice];
                            string[] tempPriceArray = tempPrice.Split(char.Parse(templateChoosen.Seperator));
                            if (tempPriceArray.Length > templateChoosen.PositionPrice.GetValueOrDefault(tempPriceArray.Length))
                            {
                                tempObj.Price = tempPriceArray[(int)templateChoosen.PositionPrice].Trim().Replace(" ", "").Replace(",", "");
                            }

                            if (tempPriceArray.Length > templateChoosen.PositionQuantity.GetValueOrDefault(tempPriceArray.Length))
                            {
                                string qty = tempPriceArray[(int)templateChoosen.PositionQuantity];
                                if (qty.ToLower() == "l" || qty == "|" || qty == "I")
                                {
                                    tempObj.Quantity = 1;
                                }
                                else
                                {
                                    int qtyTemp = 0;
                                    tempObj.Quantity = int.TryParse(qty, out qtyTemp) ? qtyTemp : 0;
                                }
                            }
                        }
                        else
                        {
                            string tempProduct = item.Replace(itemTemplateMapping.ProductNameOnOCR, "");
                            string tempDate = tempProduct.Trim();
                            string[] tempProductArray = tempDate.Split(char.Parse(templateChoosen.Seperator));
                            if (tempProductArray.Count() > 1)
                            {
                                tempObj.Price = tempProductArray[(int)templateChoosen.PositionPrice].Trim().Replace(" ", "").Replace(",", "");
                            }
                            if (tempProductArray.Length > templateChoosen.PositionQuantity.GetValueOrDefault(tempProductArray.Length))
                            {
                                string qty = tempProductArray[(int)templateChoosen.PositionQuantity];
                                if (qty.ToLower() == "l" || qty == "|" || qty == "I")
                                {
                                    tempObj.Quantity = 1;
                                }
                                else
                                {
                                    int qtyTemp = 0;
                                    tempObj.Quantity = int.TryParse(qty, out qtyTemp) ? qtyTemp : 0;
                                }
                            }
                        }
                        tempObj.ProductName = actionObj.Name;
                        tempObj.Total = (double)actionObj.Points * double.Parse(tempObj.Quantity.ToString());
                        tempObj.PointEach = actionObj.Points;
                        tempObj.ActionCode = actionObj.Code;
                        tempObj.ActionId = actionObj.ID;
                        //get product
                        Product objProduct = Product.GetByCode(actionObj.Code);
                        tempObj.ProductGroupName = objProduct.ProductGroup.ProductGroupCode;
                        //get productgroup
                        if (objProduct != null)
                        {
                            ProductGroup objProductGroup = ProductGroup.GetByID(objProduct.ProductGroupID);
                        }
                        int parsedPrice = int.TryParse(tempObj.Price, out parsedPrice) ? parsedPrice : 0;
                        if (itemTemplateMapping.UpperPrice.HasValue && itemTemplateMapping.LowerPrice.HasValue)
                        {
                            if (parsedPrice <= itemTemplateMapping.UpperPrice && parsedPrice >= itemTemplateMapping.LowerPrice)
                            {
                                result.Add(tempObj);
                            }
                        }
                        //else
                        //{
                        //    result.Add(tempObj);
                        //}
                    }
                }
                temp++;
            }
            return result;
        }

        private static string GetDate(string[] bodyTextArray, OCRTemplate templateChoosen)
        {
            string resultDate = string.Empty;
            string resultTime = string.Empty;

            foreach (string item in bodyTextArray)
            {
                string[] tempDateArray = item.Split(' ');
                Regex checkDate = new Regex(templateChoosen.DateKeyword);
                Regex checkTime = new Regex(templateChoosen.TimeKeyword);
                //get date
                foreach (string tempRow in tempDateArray)
                {
                    if (checkDate.IsMatch(tempRow))
                    {
                        resultDate = tempRow;
                        break;
                    }
                }

                //get time
                foreach (string tempRow in tempDateArray)
                {
                    string timeResult = string.Empty;
                    if (checkTime.IsMatch(tempRow))
                    {
                        timeResult = tempRow;
                        if (tempRow.Count(x => x == ':') == 1)
                        {
                            timeResult = timeResult + ":00";
                        }
                        resultTime = timeResult;
                        break;
                    }
                }

                if (!string.IsNullOrEmpty(resultDate) && !string.IsNullOrEmpty(resultTime))
                {
                    break;
                }
            }

            DateTime parsedDateTime = DateTime.MinValue;
            if (DateTime.TryParseExact(resultDate, templateChoosen.DateTimeFormat, CultureInfo.CurrentCulture, DateTimeStyles.None, out parsedDateTime))
            {
                //return parsedDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                return parsedDateTime.ToString("dd/MM/yyyy");
            }
            //return resultDate + " " + resultTime;
            return string.Empty;
        }

        private static string GetRef(string bodyText, OCRTemplate templateChoosen)
        {
            Regex refRegex = new Regex(templateChoosen.RefKeyword, RegexOptions.IgnoreCase);
            var matchValue = refRegex.Match(bodyText);
            if (matchValue.Success)
                return matchValue.Value;
            return string.Empty;
        }

        public static ResultModel<bool> SendToSnapcart(Receipt receipt, string by, string url, long? userId)
        {
            string assetBaseUrl = Settings.GetValueByKey(SiteSetting.ASSET_URL);
            if (assetBaseUrl.EndsWith("/"))
            {
                assetBaseUrl = assetBaseUrl.Substring(0, assetBaseUrl.Length - 1);
            }

            TransactionOptions transOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            };

            ResultModel<bool> result = new ResultModel<bool> { Value = false };

            string receiptImage = receipt.ReceiptImage
                .Select(x => assetBaseUrl + x.Image.Replace("//", "/"))
                .FirstOrDefault();

            //string receiptImage = "https://bebelac.staging.salt.id/Receipt/2019/11/4352b80c-32fe-4169-91e4-2d6d4cd44cd5/125696fb-89ab-4c66-90a7-23b38da7482b.jpg";

            SnapcartInputModel model = new SnapcartInputModel();
            Guid snapcartReceiptId = Guid.NewGuid();
            Data.SnapcartReceipt ocrReceipt = new Data.SnapcartReceipt
            {
                ID = snapcartReceiptId,
                ReceiptID = receipt.ID,
                ImagePath = receiptImage,
            };

            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                try
                {
                    ocrReceipt.Status = (int)Data.SnapcartReceipt.State.Queueing;
                    var ocrReceiptRes = ocrReceipt.Insert("Snapcart", "", null);

                    if (!ocrReceiptRes.Success)
                        throw new Exception(JsonConvert.SerializeObject(ocrReceiptRes));

                    receipt.Status = (int)Receipt.ReceiptStatus.OcrProcessing;
                    var recRes = receipt.UpdateReceipt(by, url, userId);
                    if (!recRes.Success)
                        throw new Exception(JsonConvert.SerializeObject(recRes));

                    //model.CorrelationID = ocrReceipt.ID;
                    model.ReceiptID = receipt.ID;
                    model.Images = new string[] { receiptImage };
                    transScope.Complete();
                }
                catch (Exception ex)
                {
                    //ocrReceipt.Status = (int)Data.SnapcartReceipt.State.FailedSend;
                    ocrReceipt.StatusMessage = JsonConvert.SerializeObject(new
                    {
                        StatusMessage = String.Format("InnerException {0}", ex.InnerException.ToString() ?? ex.Message),
                        Value = model
                    });
                    ocrReceipt.Update(by, url, userId);
                    transScope.Complete();
                    result.StatusCode = "500";
                    result.StatusMessage = ex.Message ?? ex.InnerException.Message;
                }
                transScope.Dispose();
            }

            bool isSuccess = false;
            int incLoop = 0;
            const string urlCallback = "http://loyalapi.bebeclub.co.id/ocr/SnapcartCallback";

            do
            {
                incLoop++;

                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, SiteSetting.SnapcartBaseUrl + SiteSetting.SnapcartEndpoint);
                        requestMessage.Headers.Add("x-api-key", SiteSetting.SnapcartApiKey);

                        if (model.HttpCallbackUrl == null)
                        {
                            model.HttpCallbackUrl = SiteSetting.SnapcartCallbackUrl;
                        }

                        if (model.HttpCallbackUrl == null)
                        {
                            model.HttpCallbackUrl = urlCallback;
                        }

                        var json = JsonConvert.SerializeObject(model);
                        Log.Info("Request Param " + incLoop.ToString() + " : " + json);
                        using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                        {
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                            requestMessage.Content = stringContent;
                            var response = client
                                .SendAsync(requestMessage, HttpCompletionOption.ResponseHeadersRead)
                                .Result;

                            var responseJson = JsonConvert.SerializeObject(response);
                            Log.Info("Response Param : " + responseJson);

                            //Convert To Object
                            var responseString = response.Content.ReadAsStringAsync();
                            var responseObject = JObject.Parse(responseString.Result);
                            var snapcartResult = responseObject.ToObject<SnapcartCallbackParam>();

                            if (response.StatusCode != HttpStatusCode.OK)
                            {
                                Log.Info("StatusCode Response " + incLoop.ToString() + " : " + response.StatusCode.ToString());
                                Log.Error("Error Status " + incLoop.ToString() + " : " + response.Content.ReadAsStringAsync().Result);
                                throw new HttpRequestException(response.Content.ReadAsStringAsync().Result);
                            }
                            else
                            {
                                ocrReceipt.StatusMessage = JsonConvert.SerializeObject(new
                                {
                                    Content = response.Content,
                                    StatusCode = response.StatusCode,
                                    ReasonPhrase = response.ReasonPhrase,
                                    IsSuccessStatusCode = response.IsSuccessStatusCode
                                });

                                ocrReceipt.RequestID = Guid.Parse(snapcartResult.RequestID.Value.ToString());
                                ocrReceipt.StatusSnapcart = snapcartResult.Status;
                                ocrReceipt.SnapcartUrl = snapcartResult.OutputUrl;

                                var respUpdate = ocrReceipt.Update(by, url, userId);
                                isSuccess = true;
                                if (!respUpdate.Success)
                                    throw new Exception(JsonConvert.SerializeObject(respUpdate));
                            }
                        }
                    }

                    result.Value = true;
                    result.StatusCode = "00";
                    result.StatusMessage = "Success";
                }
                catch (Exception ex)
                {
                    ocrReceipt.Status = (int)Data.SnapcartReceipt.State.FailedSend;
                    ocrReceipt.StatusMessage = JsonConvert.SerializeObject(new
                    {
                        StatusMessage = String.Format("InnerException {0}", ex.InnerException.ToString() ?? ex.Message),
                        Value = model
                    });
                    ocrReceipt.Update(by, url, userId);
                    Log.Error("Error Exception " + incLoop.ToString() + " from " + ocrReceipt.ReceiptID.ToString() + " : " + ex.StackTrace);
                    if (ex.InnerException != null)
                    {
                        Log.Error("Error InnerException " + incLoop.ToString() + " from " + ocrReceipt.ReceiptID.ToString() + " : " + ex.InnerException.StackTrace);
                    }
                    result.StatusCode = "500";
                    result.StatusMessage = ex.Message ?? ex.InnerException.Message;
                }
            } while (!isSuccess && incLoop < 3);

            //send email if 50 receipt failed 
            if (!isSuccess)
            {
                SendEmailReceiptFailedProcess(receipt, by, url, userId);

            }
            return result;
        }

        public static void SendEmailReceiptFailedProcess(Receipt receipt, string by, string url, long? userId)
        {
            var getSettingCounter = Data.Settings.GetByKey("ReceiptFailProcessCount");
            var get = Data.Settings.GetAll().Where(x => x.Key == "ReceiptFailProcessCount").FirstOrDefault();
            var getSett = Data.Settings.GetAll().OrderByDescending(x => x.CreatedDate).Skip(0).Take(10).ToList();
            int failCount = Convert.ToInt32(getSettingCounter.Value);

            if (getSettingCounter.UpdatedDate.Value.Date != DateTime.Now.Date)
            {
                getSettingCounter.Value = "1";
                failCount = 1;
            }
            else
            {
                failCount++;
                getSettingCounter.Value = failCount.ToString();
            }
            if (failCount >= 50)
            {
                
                    //send email
                    //Task.Run(() =>
                    //{
                        bool isSent = true;
                        try
                        {
                            string emailMsg = EmailHelper.GetEmailTemplate(SiteSetting.EmailReceiptFailedProcessTemplateURL);
                            //emailMsg = emailMsg.Replace("{Code}", receipt.ReceiptCode);
                            //emailMsg = emailMsg.Replace("{RejectCount}", receipt.RejectCount.Value.ToString());
                            //emailMsg = emailMsg.Replace("{Channel}", receipt.Channel);
                            string siteEmail = Data.Settings.GetValueByKey(SiteSetting.SiteEmail);
                            string smtpNameDisp = Data.Settings.GetValueByKey(SiteSetting.SMTP_NameDisplay);
                            string smtpHost = Data.Settings.GetValueByKey(SiteSetting.SMTPHost);
                            bool smtpSSL = false; bool.TryParse(Data.Settings.GetValueByKey(SiteSetting.SMTPSSL), out smtpSSL);
                            string smtpUserName = Data.Settings.GetValueByKey(SiteSetting.SMTPUsername);
                            string smtpPassword = Data.Settings.GetValueByKey(SiteSetting.SMTPPassword);
                            int smtpPort = 0; int.TryParse(Data.Settings.GetValueByKey(SiteSetting.SMTPPort), out smtpPort);

                            string receiver = Data.Settings.GetValueByKey(SiteSetting.EmailReceiptFailedProcessReceiver);
                            receiver = new Regex("\\s").Replace(receiver, "");

                            //var receivers = receiver.Split(';').ToList();
                            //receivers.Where(x => string.IsNullOrEmpty(x) || string.IsNullOrWhiteSpace(x)).ToList().ForEach(x => {
                            //    receivers.Remove(x);
                            //});
                            //receivers.ForEach(to =>
                                //{
                                    EmailHelper.SendEmail("Failed Request to Snapcart ", emailMsg, receiver, siteEmail, smtpNameDisp, smtpHost, smtpSSL, smtpUserName, smtpPassword, smtpPort);
                                //});
                        }
                        catch (Exception ex)
                        {
                            isSent = false;
                        }
                        //insert history
                        var ocrhHistoryEmail = new OCRHistorySendEmail();
                        ocrhHistoryEmail.Type = "Notif Ocr Down";
                        ocrhHistoryEmail.IsSent = isSent;
                        ocrhHistoryEmail.TotalReceipt = failCount;
                        ocrhHistoryEmail.Insert(by, url, userId);
                    //});


            }
            //if (failCount > 50)
            //{
            //    failCount = failCount - 50;
            //    getSettingCounter.Value = failCount.ToString();
            //}
            getSettingCounter.Update(by, url, userId);
        }

        public static ResultModel<bool> SnapcartCallback(JObject snapcartCallback)
        {
            try
            {
                if (snapcartCallback["status"].Value<string>().Equals("FAILED"))
                {
                    Guid requestId = Guid.Parse(snapcartCallback["request_id"].Value<string>());
                    string status = snapcartCallback["status"].Value<string>();
                    string result = JsonConvert.SerializeObject(snapcartCallback);

                    // Update SnapcartReceipt
                    var resultUpdateSnapcart = UpdateSnapcartReceipt(requestId, status, result);

                    if (!resultUpdateSnapcart.Value) return resultUpdateSnapcart;

                    // Update Receipt Status
                    Guid receiptId = Guid.Parse(snapcartCallback["receipt_id"].Value<string>());
                    var receipt = Receipt.GetById(receiptId);
                    receipt.Status = (int)ReceiptStatus.OcrReject;
                    receipt.RejectReason = SiteSetting.SnapcartReceiptNull;
                    var resultUpdateReceipt = receipt.Update("SnapcartCallback", "", null);

                    if (!resultUpdateReceipt.Success)
                    {
                        return new ResultModel<bool>
                        {
                            StatusCode = "500",
                            StatusMessage = SiteSetting.SnapcartFailedUpdateReceipt,
                            Value = false
                        };
                    }
                    else
                    {
                        var collects = Data.Collect.GetAllByReceiptID(receipt.ID).ToList();
                        NotificationLogic.Create(
                            receipt.MemberID,
                            Common.Notification.NotificationType.Type.RejectReupload,
                            "",
                            string.Format("No Transaksi: {0} {1}", receipt.Code, receipt.RejectReason), receipt.Code, collects != null ? collects.Sum(x => x.TotalPoints) : 0,
                            "SnapcartCallback");
                    }
                }
                else
                {
                    var param = snapcartCallback.ToObject<SnapcartCallbackParam>();

                    if (param.Receipt == null)
                        return new ResultModel<bool>
                        {
                            StatusCode = "500",
                            StatusMessage = "Invalid callback result from snapcart",
                            Value = false
                        };

                    var resultUpdateSnapcart = UpdateSnapcartReceipt(param.RequestID.GetValueOrDefault(), param.Status, JsonConvert.SerializeObject(param));

                    if (!resultUpdateSnapcart.Value) return resultUpdateSnapcart;

                    foreach (var StringImage in param.Receipt.Images)
                    {
                        Data.SnapcartResult snapcartResult = new SnapcartResult();

                        snapcartResult.ID = Guid.NewGuid();
                        snapcartResult.CorrelationID = param.RequestID;
                        snapcartResult.ChainValue = param.Receipt.Chain.Value;
                        snapcartResult.ChainScore = Convert.ToDecimal(param.Receipt.Chain.Score);
                        snapcartResult.ImagesLink = StringImage;

                        if (param.Receipt.PurchaseDate != null)
                        {
                            snapcartResult.PurchaseDateValue = !string.IsNullOrWhiteSpace(param.Receipt.PurchaseDate.Value) ? param.Receipt.PurchaseDate.Value : string.Empty;
                            snapcartResult.PurchaseDateScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(param.Receipt.PurchaseDate.Score.ToString()) ? param.Receipt.PurchaseDate.Score : 0);
                        }

                        if (param.Receipt.PurchaseTime != null)
                        {
                            snapcartResult.PurchaseTimeValue = !string.IsNullOrWhiteSpace(param.Receipt.PurchaseTime.Value) ? param.Receipt.PurchaseTime.Value : string.Empty;
                            snapcartResult.PurchaseTimeScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(param.Receipt.PurchaseTime.Score.ToString()) ? param.Receipt.PurchaseTime.Score : 0);
                        }

                        if (param.Receipt.TotalPurchase.Value != null)
                        {
                            snapcartResult.TotalPurchaseValue = !string.IsNullOrWhiteSpace(param.Receipt.TotalPurchase.Value.ToString()) ? param.Receipt.TotalPurchase.Value.ToString() : string.Empty;
                            snapcartResult.TotalPurchaseScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(param.Receipt.TotalPurchase.Score.ToString()) ? param.Receipt.TotalPurchase.Score : 0);
                        }

                        var res = snapcartResult.Insert("SnapcartResult", "", null);
                        if (res.Success)
                        {
                            ResultModel<SnapcartResult> resultModel = new ResultModel<SnapcartResult>();
                            resultModel.Value = snapcartResult;
                            foreach (var productItem in param.Receipt.Products)
                            {
                                Data.SnapcartProductResult snapcartProductResult = new SnapcartProductResult();

                                snapcartProductResult.ID = Guid.NewGuid();
                                snapcartProductResult.SnapcartResultID = snapcartResult.ID;

                            if (productItem.Name.Value != null)
                            {
                                snapcartProductResult.NameValue = !string.IsNullOrWhiteSpace(productItem.Name.Value) ? productItem.Name.Value : string.Empty;
                                snapcartProductResult.NameScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(productItem.Name.Score.ToString()) ? productItem.Name.Score : 0);
                                snapcartProductResult.BrandValue = !string.IsNullOrWhiteSpace(productItem.Brand.Value) ? productItem.Brand.Value : string.Empty;
                                snapcartProductResult.BrandScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(productItem.Brand.Score.ToString()) ? productItem.Brand.Score : 0);
                                snapcartProductResult.CategoryValue = !string.IsNullOrWhiteSpace(productItem.Category.Value) ? productItem.Category.Value : string.Empty;
                                snapcartProductResult.CategoryScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(productItem.Category.Score.ToString()) ? productItem.Category.Score : 0);
                                snapcartProductResult.PacksizeValue = !string.IsNullOrWhiteSpace(productItem.PackSize.SizeValue) ? productItem.PackSize.SizeValue : string.Empty;
                                snapcartProductResult.PacksizeScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(productItem.PackSize.Score.ToString()) ? productItem.PackSize.Score : 0);
                                snapcartProductResult.VariantValue = !string.IsNullOrWhiteSpace(productItem.Variant.Value) ? productItem.Variant.Value : string.Empty;
                                snapcartProductResult.VariantScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(productItem.Variant.Score.ToString()) ? productItem.Variant.Score : 0);
                                snapcartProductResult.SKUValue = !string.IsNullOrWhiteSpace(productItem.SKU.Value.ToString()) ? productItem.SKU.Value.ToString() : string.Empty;
                                snapcartProductResult.SKUScore = Convert.ToDecimal(!string.IsNullOrWhiteSpace(productItem.SKU.Score.ToString()) ? productItem.SKU.Score : 0);
                                snapcartProductResult.Quantity = productItem.Pricing.Quantity.GetValueOrDefault();
                            }

                                var resProductResult = snapcartProductResult.Insert("SnapcartProductResult", "", null);
                                if (!resProductResult.Success)
                                    return new ResultModel<bool>
                                    {
                                        StatusCode = "500",
                                        StatusMessage = "Can not insert result to snapcart product result",
                                        Value = false
                                    };
                            }
                        }
                        else
                        {
                            return new ResultModel<bool>
                            {
                                StatusCode = "500",
                                StatusMessage = "Can not insert result to snapcart result",
                                Value = false
                            };
                        }
                    }
                }

                return new ResultModel<bool>
                {
                    StatusCode = "00",
                    StatusMessage = "Success",
                    Value = true
                };
            }
            catch (Exception ex)
            {
                return new ResultModel<bool>
                {
                    StatusCode = "200",
                    StatusMessage = ex.Message ?? ex.InnerException.Message
                };
            }
        }



        public static bool ValidateSnapcartConfidential(SnapcartCallbackParam param, out SnapcartUploadReceiptModel snapcartReceiptModel)
        {
            int inValidCount = 0;
            snapcartReceiptModel = new SnapcartUploadReceiptModel
            {
                CorrelationId = param.RequestID.GetValueOrDefault(),
                //ExecutionId = param.ExecutionID.GetValueOrDefault()
            };

            if (double.TryParse(SiteSetting.DefaultSnapcartConfidentialLevel, out double snapcartConfidentialLvl))
            {
                var dbSnapcartConfidentialLvl = Settings.GetValueByKey(SiteSetting.SnapcartConfidentialLevel);
                if (!string.IsNullOrEmpty(dbSnapcartConfidentialLvl))
                {
                    double.TryParse(dbSnapcartConfidentialLvl, out snapcartConfidentialLvl);
                }
            }

            if (param.Receipt == null)
                inValidCount++;
            else
                snapcartReceiptModel.ReceiptId = param.Receipt.ReceiptID.GetValueOrDefault();

            if (param.Receipt.TotalPurchase == null
                || param.Receipt.TotalPurchase.Score < snapcartConfidentialLvl)
                inValidCount++;
            else
                snapcartReceiptModel.TotalPrice = (int)param.Receipt.TotalPurchase.Value;

            if (param.Receipt.Chain == null)
                inValidCount++;
            else
            {
                if (param.Receipt.Chain.Score < snapcartConfidentialLvl)
                    inValidCount++;
                else
                {
                    Retailer retailer = Retailer.GetAll()
                        .FirstOrDefault(x => x.Name == param.Receipt.Chain.Value);

                    if (retailer == null)
                        inValidCount++;
                    else
                        snapcartReceiptModel.Retailer = retailer;
                }
            }

            DateTime transactionDate = DateTime.MinValue;
            if (param.Receipt.PurchaseDate == null)
                inValidCount++;
            else
            {
                if (param.Receipt.PurchaseDate.Score < snapcartConfidentialLvl)
                    inValidCount++;

                if (!DateTime.TryParseExact(param.Receipt.PurchaseDate.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out transactionDate))
                    inValidCount++;
            }

            if (param.Receipt.PurchaseTime == null)
                inValidCount++;
            else
            {
                if (param.Receipt.PurchaseTime.Score < snapcartConfidentialLvl)
                    inValidCount++;

                if (!TimeSpan.TryParseExact(param.Receipt.PurchaseTime.Value, "HH:mm:ss", CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan transactionTime))
                    inValidCount++;

                transactionDate.Add(transactionTime);
            }

            if (transactionDate.GetMonthDifference(DateTime.Now) > 1)
                inValidCount++;

            if (param.Receipt.Products == null || param.Receipt.Products.Length == 0)
                inValidCount++;
            else
            {
                foreach (var ocrProduct in param.Receipt.Products)
                {
                    Product product = null;
                    if (ocrProduct.Name.Score >= snapcartConfidentialLvl)
                        product = Product.GetAll().FirstOrDefault(x => x.SnapcartSKU == ocrProduct.Name.Value);

                    string brand = string.Empty,
                        category = string.Empty,
                        name = string.Empty,
                        variant = string.Empty,
                        sizeUnit = string.Empty;

                    int price = 0,
                        quantity = 0,
                        totalPrice = 0,
                        size = 0;

                    if (ocrProduct.Brand != null)
                        brand = ocrProduct.Brand.Value;

                    if (ocrProduct.Category != null)
                        category = ocrProduct.Category.Value;

                    if (ocrProduct.Name != null)
                        name = ocrProduct.Name.Value;

                    if (ocrProduct.Variant != null)
                        variant = ocrProduct.Variant.Value;

                    if (ocrProduct.Pricing != null)
                    {
                        price = ocrProduct.Pricing.BasePrice.GetValueOrDefault();
                        quantity = ocrProduct.Pricing.Quantity.GetValueOrDefault();
                        totalPrice = ocrProduct.Pricing.TotalBasePrice.GetValueOrDefault();
                    }

                    if (ocrProduct.PackSize != null)
                    {
                        size = ocrProduct.PackSize.Volume.GetValueOrDefault();
                        sizeUnit = ocrProduct.PackSize.VolumeUnit;
                    }

                    snapcartReceiptModel.Products.Add(new SnapcartUploadReceiptDetailModel
                    {
                        Product = product,
                        Brand = brand,
                        Category = category,
                        Name = name,
                        Variant = variant,
                        Price = price,
                        Quantity = quantity,
                        TotalPrice = totalPrice,
                        Size = size,
                        SizeUnit = sizeUnit
                    });
                }

                if (snapcartReceiptModel.Products.Count(x => x.Product == null || x.Price == 0 || x.Quantity == 0) > 0)
                    inValidCount++;
            }
            snapcartReceiptModel.TransactionDate = transactionDate;
            return inValidCount == 0;
        }

        public static void SaveNonDanoneProducts(SnapcartUploadReceiptModel receipt)
        {
            var products = receipt.Products.Where(x => x.Product == null);
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                try
                {
                    foreach (var product in products)
                    {
                        Data.SnapcartReceiptItem receiptItem = new SnapcartReceiptItem
                        {
                            SnapcartReceiptID = receipt.CorrelationId,
                            Brand = product.Brand,
                            Category = product.Category,
                            Variant = product.Variant,
                            Name = product.Name,
                            Price = product.Price,
                            Quantity = product.Quantity,
                            Retailer = receipt.Retailer.Name ?? "",
                            Size = product.Size,
                            SizeUnit = product.SizeUnit,
                            TotalPrice = product.TotalPrice
                        };

                        var resp = receiptItem.Insert("Snapcart", "", null);

                        if (!resp.Success)
                            throw new Exception(JsonConvert.SerializeObject(resp));
                    }
                    transScope.Complete();
                }
                catch (Exception ex)
                {
                    transScope.Dispose();
                }
            }
        }

        public static ResultModel<bool> UpdateSnapcartReceipt(Guid requestId, string statusSnapcart, string result)
        {
            Data.SnapcartReceipt ocrReceipt = Data.SnapcartReceipt.GetByStatusAndRequestId("new", requestId);

            if (ocrReceipt == null)
                return new ResultModel<bool>
                {
                    StatusCode = "500",
                    StatusMessage = "Invalid receipt id from snapcart result",
                    Value = false
                };

            if (ocrReceipt.Status != (int)Data.SnapcartReceipt.State.Queueing)
                return new ResultModel<bool>
                {
                    StatusCode = "500",
                    StatusMessage = "Receipt already processed",
                    Value = false
                };

            ocrReceipt.ReceivedDate = DateTime.Now;
            ocrReceipt.RequestID = requestId;
            ocrReceipt.StatusSnapcart = statusSnapcart;
            ocrReceipt.Result = result;
            var ocrRecResp = ocrReceipt.Update("Snapcart", "", null);
            if (!ocrRecResp.Success)
                return new ResultModel<bool>
                {
                    StatusCode = "500",
                    StatusMessage = "Can not update ocr receipt from snapcart result",
                    Value = false
                };

            return new ResultModel<bool>
            {
                StatusCode = "00",
                StatusMessage = "Success",
                Value = true
            };
        }
    }
}
