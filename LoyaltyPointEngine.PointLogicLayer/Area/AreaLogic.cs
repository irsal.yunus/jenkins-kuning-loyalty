﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace LoyaltyPointEngine.PointLogicLayer.Area
{
    public class AreaLogic
    {
        public static ResultPaginationModel<List<AreaModel>> Search(ParamPagination param)
        {
            ResultPaginationModel<List<AreaModel>> result = new ResultPaginationModel<List<AreaModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var AreaLinq = Data.Area.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        AreaLinq = AreaLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        AreaLinq = AreaLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        AreaLinq = AreaLinq.Where(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                        total = Data.Area.GetAll().Count(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                    }
                    else
                    {
                        total = Data.Area.GetAll().Count();
                    }


                    var Areas = AreaLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} Area(s)", Areas.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = Areas.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<AreaDetailModel> GetAreaModelById(int ID)
        {
            ResultModel<AreaDetailModel> result = new ResultModel<AreaDetailModel>();
            var dataStage = Data.Area.GetById(ID);
            if (dataStage != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Area with ID {0}", ID);
                result.Value = new AreaDetailModel(dataStage);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Area with ID {0} not found in system", ID);
            }



            return result;
        }

        public static ResultModel<List<AreaModel>> GetAreaModelByDistrictId(int DistrictID)
        {
            ResultModel<List<AreaModel>> result = new ResultModel<List<AreaModel>>();
            var dataAreas = Data.Area.GetByDistrictId(DistrictID).ToList();
            if (dataAreas != null && dataAreas.Count > 0)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Area with DistrictID {0}", DistrictID);
                result.Value = dataAreas.Translated();
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Area with DistrictID {0} not found in system", DistrictID);
            }



            return result;
        }
    }
}
