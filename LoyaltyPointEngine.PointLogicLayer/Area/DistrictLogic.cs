﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace LoyaltyPointEngine.PointLogicLayer.Area
{
    public class DistrictLogic
    {
        public static ResultPaginationModel<List<DistrictModel>> Search(ParamPagination param)
        {
            ResultPaginationModel<List<DistrictModel>> result = new ResultPaginationModel<List<DistrictModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var DistrictLinq = Data.District.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        DistrictLinq = DistrictLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        DistrictLinq = DistrictLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        DistrictLinq = DistrictLinq.Where(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                        total = Data.District.GetAll().Count(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                    }
                    else
                    {
                        total = Data.District.GetAll().Count();
                    }


                    var Districts = DistrictLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} District(s)", Districts.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = Districts.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<DistrictDetailModel> GetDistrictModelById(int ID)
        {
            ResultModel<DistrictDetailModel> result = new ResultModel<DistrictDetailModel>();
            var dataStage = Data.District.GetById(ID);
            if (dataStage != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get District with ID {0}", ID);
                result.Value = new DistrictDetailModel(dataStage);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("District with ID {0} not found in system", ID);
            }



            return result;
        }

        public static ResultModel<List<DistrictModel>> GetDistrictModelByRegionId(int RegionID)
        {
            ResultModel<List<DistrictModel>> result = new ResultModel<List<DistrictModel>>();
            var dataDistricts = Data.District.GetByRegionId(RegionID).ToList();
            if (dataDistricts != null && dataDistricts.Count > 0)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get District with RegionID {0}", RegionID);
                result.Value = dataDistricts.Translated();
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("District with RegionID {0} not found in system", RegionID);
            }



            return result;
        }

    }
}
