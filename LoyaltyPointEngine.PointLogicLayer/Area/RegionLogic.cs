﻿using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace LoyaltyPointEngine.PointLogicLayer.Area
{
    public class RegionLogic
    {
        public static ResultPaginationModel<List<RegionModel>> Search(ParamPagination param)
        {
            ResultPaginationModel<List<RegionModel>> result = new ResultPaginationModel<List<RegionModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var RegionLinq = Data.Region.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        RegionLinq = RegionLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        RegionLinq = RegionLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        RegionLinq = RegionLinq.Where(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                        total = Data.Region.GetAll().Count(x => x.Name.Contains(param.Search) || x.Code.Contains(param.Search));
                    }
                    else
                    {
                        total = Data.Region.GetAll().Count();
                    }


                    var regions = RegionLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} region(s)", regions.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = regions.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<RegionDetailModel> GetRegionModelById(int ID)
        {
            ResultModel<RegionDetailModel> result = new ResultModel<RegionDetailModel>();
            var dataStage = Data.Region.GetById(ID);
            if (dataStage != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Region with ID {0}", ID);
                result.Value = new RegionDetailModel(dataStage);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Region with ID {0} not found in system", ID);
            }



            return result;
        }

        public static ResultModel<List<RegionModel>> GetRegionModelByZoneId(int ZoneID)
        {
            ResultModel<List<RegionModel>> result = new ResultModel<List<RegionModel>>();
            var dataRegions = Data.Region.GetByZoneId(ZoneID).ToList();
            if (dataRegions != null && dataRegions.Count > 0)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Region with ZoneID {0}", ZoneID);
                result.Value = dataRegions.Translated();
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Region with ZoneID {0} not found in system", ZoneID);
            }



            return result;
        }

    }
}
