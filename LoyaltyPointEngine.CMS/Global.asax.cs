﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Data;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace LoyaltyPointEngine.CMS
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {

        private class HttpDataPointContextDataStore : LoyaltyPointEngine.Data.IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        private class HttpDataMemberContextDataStore : LoyaltyPointEngine.DataMember.IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

        protected void Application_Start()
        {

            LoyaltyPointEngine.Data.DataRepositoryStore.CurrentDataStore = new HttpDataPointContextDataStore();
            LoyaltyPointEngine.DataMember.DataRepositoryStore.CurrentDataStore = new HttpDataMemberContextDataStore();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            log4net.Config.XmlConfigurator.Configure();
        }

        // Global error catcher
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                IRavenClient _ravenClient = new RavenClient(SiteSetting.RavenClientDsn);
                _ravenClient.Environment = SiteSetting.RavenClientEnvironment;
                _ravenClient.Capture(new SentryEvent(exception));
            }
        }
    }
}