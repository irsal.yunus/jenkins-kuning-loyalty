/*
 * custom.js
 *
 * Place your code here that you need on all your pages.
 */

"use strict";

$(document).ready(function () {

	$("#ZipCode").keydown(function (e) {


		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl+A, Command+A
			(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right, down, up
			(e.keyCode >= 35 && e.keyCode <= 40)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});




	//===== Sidebar Search (Demo Only) =====//
	$('.sidebar-search').submit(function (e) {
		//e.preventDefault(); // Prevent form submitting (browser redirect)

		$('.sidebar-search-results').slideDown(200);
		return false;
	});

	$('.sidebar-search-results .close').click(function () {
		$('.sidebar-search-results').slideUp(200);
	});

	//===== .row .row-bg Toggler =====//
	$('.row-bg-toggle').click(function (e) {
		e.preventDefault(); // prevent redirect to #

		$('.row.row-bg').each(function () {
			$(this).slideToggle(200);
		});
	});

	//===== Sparklines =====//
	/*
	$("#sparkline-bar").sparkline('html', {
		type: 'bar',
		height: '35px',
		zeroAxis: false,
		barColor: App.getLayoutColorCode('red')
	});

	$("#sparkline-bar2").sparkline('html', {
		type: 'bar',
		height: '35px',
		zeroAxis: false,
		barColor: App.getLayoutColorCode('green')
	});
	*/
	//===== Refresh-Button on Widgets =====//

	$('.widget .toolbar .widget-refresh').click(function () {
		var el = $(this).parents('.widget');

		App.blockUI(el);
		window.setTimeout(function () {
			App.unblockUI(el);
			noty({
				text: '<strong>Widget updated.</strong>',
				type: 'success',
				timeout: 1000
			});
		}, 1000);
	});

	//===== Fade In Notification (Demo Only) =====//
	setTimeout(function () {
		$('#sidebar .notifications.demo-slide-in > li:eq(1)').slideDown(500);
	}, 3500);

	setTimeout(function () {
		$('#sidebar .notifications.demo-slide-in > li:eq(0)').slideDown(500);
	}, 7000);

	if ($(".ipt-number").length > 0) {
		$(document).on('keypress', '.ipt-number', function (evt) {
		    var theEvent = evt || window.event;
		    var key = theEvent.keyCode || theEvent.which;

		    if (!CheckValidKeyPressForRegex(key)) {
		        key = String.fromCharCode(key);
		        var regex = /^[0-9]+$/;
		        if (!regex.test(key)) {
		            theEvent.returnValue = false;
		            if (theEvent.preventDefault) theEvent.preventDefault();
		        }
		    }
		});
	}
});

function ShowLoader() {
	$('.submit-loader').show();
	$(".submit-loader").height($(document).height());
} function HideLoader() {
	$('.submit-loader').hide();
}


function updateTotalAction(val) {
	$("#TotalAction").val(val);
}
function getTotalAction() {
	return $("#TotalAction").val();
}

function getTotalImage() {
	return $("#TotalImage").val();
}

function removeAction(index) {

	var wrapper = $("#action-box");
	var mtrIdx = (wrapper.find(".action-obj").length + 1);

	$("#action-box .action-obj-" + index)
	.animate({
		"height": 0,
		"overflow": "hidden",
		"opacity": 0,
		"filter": "alpha(opacity=0)"
	}, 200, function () {
		$(this).remove();
		var total = 0;
		$('.TotalQuantityApproved').each(function () {
			total += parseInt(this.value);
		});
		$('#TotalAllPoint').val(total);
	});

	//updateTotalAction(mtrIdx);
}
function addAction() {

	var wrapper = $("#action-box");
	var lastactionIndex = parseInt(getTotalAction());
	var mtrIdx = (lastactionIndex + 1);

	/* naming */
	var ActionID = "ActionID-" + mtrIdx;
	var ChildID = "ChildID-" + mtrIdx;
	var Quantity = "Quantity-" + mtrIdx;
	var Points = "Points-" + mtrIdx;
	var TotalPoints = "TotalPoints-" + mtrIdx;
	var QuantityApproved = "QuantityApproved-" + mtrIdx;
	var TotalQuantityApprovedPoints = "TotalQuantityApprovedPoints-" + mtrIdx;
	var ItemPrice = "ItemPrice-" + mtrIdx;
	var Remarks = "Remarks-" + mtrIdx;

	var addProjectTemplate = '<div class="row action-obj-' + mtrIdx + '"><div class="col-md-12"><div class="widget box"><div class="widget-header"><h4><i class="icon-reorder"></i>Receipt Detail</h4><div class="toolbar no-padding"><div class="btn-group" onclick="removeAction(' + mtrIdx + ')"><span class="btn btn-xs"><i class="icon-minus"></i> Remove</span></div></div></div><div class="widget-content"><div class="form-group"><label class="col-md-3 control-label">Action <span class="required">*</span></label><div class="col-md-9"><select id="' + ActionID + '" name="' + ActionID + '" class="select2-select-00 col-md-12 full-width-fix actionId-check" onchange="GetActionValue(this.value,' + mtrIdx + ')" required></select></div></div><div class="form-group" style="display: none;"><label class="col-md-3 control-label">Child</label><div class="col-md-9"><select id="' + ChildID + '" name="' + ChildID + '" class="select2-select-00 col-md-12 full-width-fix"><option value="" selected></option></select></div></div><div class="form-group"><label class="col-md-3 control-label">Quantity <span class="required">*</span></label><div class="col-md-9"><input class="form-control ipt-number" id="' + Quantity + '" name="' + Quantity + '" onchange="SetTotalPoint(this.value,' + mtrIdx + ')" required type="tel"></div></div><div class="form-group"><label class="col-md-3 control-label">Points</label><div class="col-md-9"><input class="form-control ipt-number" id="' + Points + '" name="' + Points + '" readonly="" type="tel"></div></div><div class="form-group"><label class="col-md-3 control-label">Quantity Approved</label><div class="col-md-9"><input class="form-control ipt-number quantity-check" id="' + QuantityApproved + '" name="' + QuantityApproved + '" onchange="SetTotalQuantityPoint(this.value,' + mtrIdx + ')" type="tel" required ></div></div><div class="form-group"><label class="col-md-3 control-label">Total Quantity Approved Points</label><div class="col-md-9"><input class="form-control TotalQuantityApproved ipt-number" id="' + TotalQuantityApprovedPoints + '" name="' + TotalQuantityApprovedPoints + '" readonly="" type="tel"></div></div><div class="form-group"><label class="col-md-3 control-label">Item Price</label><div class="col-md-9"><input class="form-control ipt-number price-check" id="' + ItemPrice + '" name="' + ItemPrice + '" type="tel"></div></div><div class="form-group"><label class="col-md-3 control-label">Remarks</label><div class="col-md-9"><textarea class="form-control" id="' + Remarks + '" name="' + Remarks + '"></textarea></div></div></div></div></div></div>';
	wrapper.append(addProjectTemplate);


	var first = document.getElementById('ActionDummy');
	var options = first.innerHTML;

	var second = document.getElementById(ActionID);
	var options = second.innerHTML + options;
	second.innerHTML = options;

	// Initialize default select2 boxes
	$('.select2, .select2-select-00').each(function () {
		var self = $(this);
		$(self).select2(self.data());
	});

	updateTotalAction(mtrIdx);
}

function addChild() {
    var idxChild = $('#CountChild').val();
    var Parent = $('#ParentID').val();
    idxChild++;
    $('#CountChild').val(idxChild);

    $('.datepicker').datepicker('destroy');
    var child = $($('.child-obj-1')[0]).clone();
    child.find("#Childs-0-ID").attr("name", "Childs[" + (idxChild - 1) + "].ID").attr("id", "Childs-" + (idxChild - 1) + "-ID").val('0');
    child.find("#Childs-0-Name").attr("name", "Childs[" + (idxChild - 1) + "].Name").attr("id", "Childs-" + (idxChild - 1) + "-Name").val('');
    child.find("#Childs-0-Birthdate").attr("name", "Childs[" + (idxChild - 1) + "].Birthdate").attr("id", "Childs-" + (idxChild - 1) + "-Birthdate").val('');
    child.find("#Childs-0-ParentID").attr("name", "Childs[" + (idxChild - 1) + "].ParentID", Parent).attr("id", "Childs-" + (idxChild - 1) + "-ParentID", Parent);
    child.find("#Childs-0-ProductBeforeID").attr("name", "Childs[" + (idxChild - 1) + "].ProductBeforeID").attr("id", "Childs-" + (idxChild - 1) + "-ProductBeforeID");
    //var clone = $('#child-box').clone('.child-obj-1');
    child.find("#Childs-0-ChildButtonBar").remove();
    $('#child-box').append(child);

    $('.datepicker').datepicker({
        defaultDate: +7,
        showOtherMonths: true,
        autoSize: true,
        appendText: '<span class="help-block">(dd/mm/yyyy)</span>',
        dateFormat: 'dd/mm/yy',
        maxDate: '0'
    });
    $('#Childs-' + (idxChild - 1) + '-ProductBeforeID').val('0');
}

function ReceiptRemoveImage(index) {
	var objImage = $("#image-box .image-obj-" + index);
	objImage.animate({
		"height": 0,
		"overflow": "hidden",
		"opacity": 0,
		"filter": "alpha(opacity=0)"
	}, 200, function () {
		$(this).remove();
	});
}

function ReceiptAddImage() {
	var wrapper = $("#image-box");
	var lastactionIndex = parseInt(getTotalImage());
	var mtrIdx = (lastactionIndex + 1);

	/* naming */
	var Image = "Image-" + mtrIdx;

	var addImageTemplate = '<div class="form-group image-obj-' + mtrIdx + '"> ' +
		'<label class="col-md-3 control-label">Receipt Image</label>' +
		'<div class="col-md-4"><input type="file" id="' + Image + '" class="ImageReceipt form-control" name="Image"/></div><div class="col-md-5" style="padding-top: 7px;">' +
		'<button  id="view-' + Image + '" type="button" class="btn btn-primary view-image" onclick="readURL(document.getElementById(\'' + Image + '\'))" data-toggle="modal" data-target="#imageViewer" style="margin-right: 3px;display:none;">View</button>' +

	   //'<button type="button" style="margin-right: 3px;" class="btn btn-primary" onclick="readURL(document.getElementById(\'' + Image + '\'))" data-toggle="modal" data-target="#imageViewer">View</button>' +
		'<button type="button" style="margin-right: 3px;" class="btn btn-success" onclick="ReceiptAddImage()">Add</button>' +
		'<button type="button" class="btn btn-danger" onclick="ReceiptRemoveImage(' + mtrIdx + ')">Remove</button></div></div>';
	wrapper.append(addImageTemplate).fadeIn('slow');

	$('#TotalImage').val(mtrIdx);
	$("#formAddReceipt").validate();


	$('.ImageReceipt').change(function () {

		ValidateImageRecieipt(this);

	});
}


function maxLengthCheck(object) {

	if (object.value.length > object.maxLength)
		object.value = object.value.slice(0, object.maxLength)
}

function ParseDom(source) {
	var result = '';
	var parser = new DOMParser;
	var dom = parser.parseFromString(source, 'text/html');
	if (dom != undefined) {
		result = dom.body.textContent;
	}
	return result;
}

function CheckValidKeyPressForRegex(key) {

	var arrKeyValid = [8, 9, 35, 36, 37, 38, 39, 40, 46];
	var result = $.inArray(parseInt(key), arrKeyValid);
	if (result < 0) {
		return false;
	} else {
		return true;
	}
}