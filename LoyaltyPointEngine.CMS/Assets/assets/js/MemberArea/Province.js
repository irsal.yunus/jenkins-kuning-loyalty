﻿/*
* created by    : azadi dicky

* created on    : 25 Austus 2016
*
* Copyright ©2016 azadi.dicky@gmail.com
*/


function ProvinceController(model) {
    var thisObject = this;

    this.url_api_populate_district = '';
   
    this.ddl_province = $('.ddl-province');
    this.div_widget_distric = $('.div-widget-district');



    this.url_api_populate_subdistrict = '';

    this.ddl_district = $('.ddl-district');
    this.ddl_sub_district = $('.ddl-sub-district');
    this.div_widget_sub_distric = $('.div-widget-sub-district');



    this.Initialize = function () {
        thisObject.MainFunction();
        thisObject.MainEvent();
    };
    this.MainFunction = function () {      
        this.PopulateDistrict = function (id) {

            ShowLoader();
            $.ajax({
                url: thisObject.url_api_populate_district+'/'+id,
                type: "GET",
                contentType: "application/json",               
                async: true,
                success: function (res) {

                   
                    if (res != null) {
                       
                        HideLoader();
                        thisObject.RenderDistrictToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    alert("error : " + detail);
                    HideLoader();
                }
            });


        }

        this.PopulateSubDistrict = function (id) {


            var _id = id;

            if (id == null || id == '' || id == undefined) {
                _id = 0;
            }
            ShowLoader();
            $.ajax({
                url: thisObject.url_api_populate_subdistrict + '/' + _id,
                type: "GET",
                contentType: "application/json",
                async: true,
                success: function (res) {

                 
                    if (res != null) {

                        HideLoader();
                        thisObject.RenderSubDistrictToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    alert("error : " + detail);
                    HideLoader();
                }
            });


        }


        this.RenderDistrictToOption = function (ress) {
           
            var appendData = '';
            if (ress != null) {
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
            }

            thisObject.ddl_district.html(appendData);
        };
        this.RenderSubDistrictToOption = function (ress) {

            var appendData = '';
            if (ress != null) {
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
            }

            thisObject.ddl_sub_district.html(appendData);
        };
    };
    this.MainEvent = function () {
        thisObject.ddl_province.change(function () {
          
            var value = $(this).val();          
            thisObject.PopulateDistrict(value);
           
            thisObject.ddl_district.prop('selectedIndex', 0);
            thisObject.ddl_district.change();
        });

        thisObject.ddl_district.change(function () {
            var value = $(this).val();           
            thisObject.PopulateSubDistrict(value);
         
            thisObject.ddl_sub_district.prop('selectedIndex', 0);
            thisObject.ddl_sub_district.change();
        });
    };
  
}