﻿/*
* created by    : azadi dicky

* created on    : 29 July 2016
*
* Copyright ©2016 azadi.dicky@gmail.com
*/


function PostController(model) {
    var thisObject = this;
    this.url_api = '';
    this.btn_synch_post = $('#btn-synch-fb-post');
    this.Initialize = function () {
        thisObject.MainFunction();
        thisObject.MainEvent();
    };
    this.MainFunction = function () {      
        this.PopulatePost = function(token) {
            ShowLoader();
           

            $.ajax({
                url: thisObject.url_api,
                type: "POST",
                contentType: "application/json",
                data: '{"token":"' + token + '"}',
                async: true,
                success: function (res) {
                  
                    console.log(res);
                    if (res != null)
                    {
                        $(".grid-mvc").gridmvc().refreshFullGrid();
                        HideLoader();

                    }
                   
                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    alert("error : "+detail);
                    HideLoader();
                }
            });


        }
    };
    this.MainEvent = function () {
        thisObject.btn_synch_post.click(function () {

            FB.getLoginStatus(function (response) {
                console.log('getLoginStatus');
                console.log(response);
                if (response.status != 'connected') {
                    FB.login(function (loginResponse) {
                        var token = loginResponse.authResponse.accessToken;
                        thisObject.PopulatePost(token);
                    }, { scope: 'email,public_profile,user_friends' });
                } else {

                    var objAuth = FB.getAuthResponse();
                    if (objAuth != null) {
                        var token = objAuth.accessToken;
                        thisObject.PopulatePost(token);
                    } else {
                        console.log(objAuth);
                        alert('Login failed');
                    }
                }

            });


        });
    };
  
}