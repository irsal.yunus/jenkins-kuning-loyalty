﻿/*
* created by    : azadi dicky

* created on    : 31 July 2016
*
* Copyright ©2016 azadi.dicky@gmail.com
*/


function CommentController(model) {
    var thisObject = this;
    this.url_api = '';
    this.PostId = '';
    this.btn_synch_comment = $('#btn-synch-fb-comment');
    this.Initialize = function () {
        thisObject.MainFunction();
        thisObject.MainEvent();
    };
    this.MainFunction = function () {      
        this.PopulateComment = function (token) {
            console.log(token);
            ShowLoader();
            $.ajax({
                url: thisObject.url_api,
                type: "POST",
                contentType: "application/json",
                data: '{"token":"' + token + '","PostID":"' + thisObject.PostId + '"}',
                async: true,
                success: function (res) {
                  
                    console.log(res);
                    if (res != null)
                    {
                        $(".grid-mvc[data-gridname='AllFacebookComment']").gridmvc().refreshFullGrid();
                        HideLoader();

                    }
                   
                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    alert("error : "+detail);
                    HideLoader();
                }
            });


        }
    };
    this.MainEvent = function () {
        thisObject.btn_synch_comment.click(function () {

            FB.getLoginStatus(function (response) {
                console.log(response);
                if (response.status != 'connected') {
                    FB.login(function (loginResponse) {
                        var token = loginResponse.authResponse.accessToken;
                        thisObject.PopulateComment(token);
                    }, { scope: 'email,public_profile,user_friends' });
                } else {

                    var objAuth = FB.getAuthResponse();
                    if (objAuth != null) {
                        var token = objAuth.accessToken;
                        thisObject.PopulateComment(token);
                    } else {
                        console.log(objAuth);
                        alert('Login failed');
                    }
                }

            });


        });
    };
  
}