﻿/*
* created by    : azadi dicky

* created on    : 31 July 2016
*
* Copyright ©2016 azadi.dicky@gmail.com
*/


function LikeController(model) {
    var thisObject = this;
    this.url_api = '';
    this.url_api_both = '';
    this.PostId = '';
    this.btn_synch_like = $('#btn-synch-fb-like');
    this.btn_synch_comment = $('#btn-synch-fb-comment');
    this.btn_synch_like_and_comment = $('#btn-synch-fb-like-and-comment');

    this.btn_trigger = $('#btn-trigger-synch-fb');
    this.ddl_fb_type = $('#ddl-synch-fb-type');

    this.Initialize = function () {
        thisObject.MainFunction();
        thisObject.MainEvent();
    };
    this.MainFunction = function () {      
        this.PopulateLike = function(token) {
            ShowLoader();
            console.log(token);
           

            $.ajax({
                url: thisObject.url_api,
                type: "POST",
                contentType: "application/json",
                data: '{"token":"' + token + '","PostID":"' + thisObject.PostId + '"}',
                async: true,
                success: function (res) {
                  
                    console.log(res);
                    if (res != null)
                    {
                        $(".grid-mvc[data-gridname='AllFacebookLike']").gridmvc().refreshFullGrid();
                        HideLoader();

                    }
                   
                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    alert("error : "+detail);
                    HideLoader();
                }
            });

        }
        this.PopulateLikeAndComment = function (token) {
            ShowLoader();
            console.log(token);


            $.ajax({
                url: thisObject.url_api_both,
                type: "POST",
                contentType: "application/json",
                data: '{"token":"' + token + '","PostID":"' + thisObject.PostId + '"}',
                async: true,
                success: function (res) {

                    console.log(res);
                    if (res != null) {
                        $(".grid-mvc[data-gridname='AllFacebookLike']").gridmvc().refreshFullGrid();
                        $(".grid-mvc[data-gridname='AllFacebookComment']").gridmvc().refreshFullGrid();
                        HideLoader();

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    alert("error : " + detail);
                    HideLoader();
                }
            });


        }
    };
    this.MainEvent = function () {
        thisObject.btn_synch_like.click(function () {
            console.log('btn_synch_like');
            FB.getLoginStatus(function (response) {
                console.log('getLoginStatus');
                console.log(response);
                if (response.status != 'connected') {
                    FB.login(function (loginResponse) {
                        var token = loginResponse.authResponse.accessToken;
                        thisObject.PopulateLike(token);
                    }, { scope: 'email,public_profile,user_friends' });
                } else {

                    var objAuth = FB.getAuthResponse();
                    if (objAuth != null) {
                        var token = objAuth.accessToken;
                        thisObject.PopulateLike(token);
                    } else {
                        console.log(objAuth);
                        alert('Login failed');
                    }
                }

            });


        });

        thisObject.btn_synch_like_and_comment.click(function () {
            console.log('btn_synch_like_and_comment');
            FB.getLoginStatus(function (response) {
                console.log('getLoginStatus');
                console.log(response);
                if (response.status != 'connected') {
                    FB.login(function (loginResponse) {
                        var token = loginResponse.authResponse.accessToken;
                        thisObject.PopulateLikeAndComment(token);
                    }, { scope: 'email,public_profile,user_friends' });
                } else {

                    var objAuth = FB.getAuthResponse();
                    if (objAuth != null) {
                        var token = objAuth.accessToken;
                        thisObject.PopulateLikeAndComment(token);
                    } else {
                        alert('Login failed');
                    }
                }

            });


        });

        thisObject.btn_trigger.click(function () {

            var ddl_value = thisObject.ddl_fb_type.val();
            console.log(ddl_value);
            if (ddl_value == 'like') {
                thisObject.btn_synch_like.click();
            } else if (ddl_value == 'comment') {
                thisObject.btn_synch_comment.click();
            } else {
                thisObject.btn_synch_like_and_comment.click();
            }

        });
    };
  
}