﻿/*
* created by    : azadi dicky

* created on    : 25 Austus 2016
*
* Copyright ©2016 azadi.dicky@gmail.com
*/


function MarketController(model) {
    var thisObject = this;

    this.url_api_populate_channel = '';
    this.url_api_populate_sub_channel = '';
    this.url_api_populate_account = '';
    this.url_api_populate_sub_account = '';

    this.ddl_market = $('.ddl-market');
    this.ddl_channel = $('.ddl-chanel');
    this.ddl_sub_channel = $('.ddl-sub-chanel');
    this.ddl_account = $('.ddl-account');
    this.ddl_sub_account = $('.ddl-sub-account');


    this.Initialize = function () {
        thisObject.MainFunction();
        thisObject.MainEvent();
    };


    this.MainFunction = function () {
        this.PopulateChannel = function (id) {

            var url = thisObject.url_api_populate_channel + '?id=' + id;
            console.log(url);
            ShowLoader();
            $.ajax({
                url: url,
                type: "GET",
                contentType: "application/json",
                async: true,
                success: function (res) {

                    if (res != null) {

                        HideLoader();
                        thisObject.renderChannelToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    // alert("error : " + detail);
                    HideLoader();
                }
            });


        }

        this.PopulateSubChannel = function (id) {

            ShowLoader();
            $.ajax({
                url: thisObject.url_api_populate_sub_channel + '/' + id,
                type: "GET",
                contentType: "application/json",
                async: true,
                success: function (res) {


                    if (res != null) {

                        HideLoader();
                        thisObject.renderSubChannelToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    //alert("error : " + detail);
                    HideLoader();
                }
            });


        }


        this.renderChannelToOption = function (ress) {
            console.log('chanel');
            var appendData = '';
            if (ress != null) {
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
            }

            thisObject.ddl_channel.html(appendData);
        };

        this.renderSubChannelToOption = function (ress) {
            console.log('sub');
            var appendData = '';
            if (ress != null) {
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
            }

            thisObject.ddl_sub_channel.html(appendData);
        };


        this.PopulateAccount = function (id) {

            ShowLoader();
            $.ajax({
                url: thisObject.url_api_populate_account + '/' + id,
                type: "GET",
                contentType: "application/json",
                async: true,
                success: function (res) {


                    if (res != null) {

                        HideLoader();
                        thisObject.renderAccountToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    //alert("error : " + detail);
                    HideLoader();
                }
            });


        };

        this.renderAccountToOption = function (ress) {
            console.log('account');
            var appendData = '';
            if (ress != null) {
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
            }

            thisObject.ddl_account.html(appendData);
        };


        this.PopulateSubAccount = function (id) {

            ShowLoader();
            $.ajax({
                url: thisObject.url_api_populate_sub_account + '/' + id,
                type: "GET",
                contentType: "application/json",
                async: true,
                success: function (res) {


                    if (res != null) {

                        HideLoader();
                        thisObject.renderSubAccountToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    //alert("error : " + detail);
                    HideLoader();
                }
            });


        };

        this.renderSubAccountToOption = function (ress) {
            console.log('account');
            var appendData = '';
            if (ress != null) {
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
            }

            thisObject.ddl_sub_account.html(appendData);
        };


    };
    this.MainEvent = function () {
        thisObject.ddl_market.change(function () {
            thisObject.ddl_channel.html('');
            var value = $(this).val();
            thisObject.PopulateChannel(value);
            thisObject.ddl_channel.change();


        });


        thisObject.ddl_channel.change(function () {
            var value = $(this).val();

            thisObject.PopulateSubChannel(value);

            thisObject.ddl_sub_channel.change();


        });


        thisObject.ddl_sub_channel.change(function () {
            var value = $(this).val();
            thisObject.PopulateAccount(value);
            thisObject.ddl_account.change();

        });

        thisObject.ddl_account.change(function () {
            var value = $(this).val();
            thisObject.PopulateSubAccount(value);
            thisObject.ddl_sub_account.change();

        });
    };

}