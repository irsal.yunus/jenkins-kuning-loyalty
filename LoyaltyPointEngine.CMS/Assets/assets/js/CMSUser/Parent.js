﻿

function CMSUserController(model) {
    var thisObject = this;

    this.url_populate_parent = '';

    this.ddl_cmsrole = $('.ddl-cmsrole');
    this.ddl_parent = $('.ddl-parent');
    this.list_parent = $('.list-parent');
    this.parent = $('.parent');
    this.Initialize = function () {
        thisObject.MainFunction();
        thisObject.MainEvent();
    };
    this.MainFunction = function () {
        this.PopulateParent = function (id) {

            ShowLoader();
            $.ajax({
                url: thisObject.url_populate_parent + '/' + id,
                type: "GET",
                contentType: "application/json",
                async: true,
                success: function (res) {

                    if (res != null) {

                        HideLoader();
                        thisObject.RenderParentToOption(res);

                    }

                },
                error: function (ex, msg, detail) {
                    console.log(ex);
                    // alert("error : " + detail);
                    HideLoader();
                }
            });
        };
    };

    this.RenderParentToOption = function (ress) {

        var appendData = '';
        if (ress != null) {
            if (ress.length > 1)
            {
                this.parent.removeClass("hidden");
                $.each(ress, function (index, value) {
                    appendData = appendData + '<option value="' + value.Value + '">' + value.Text + '</option>';
                });
                thisObject.ddl_parent.html(appendData);
            }
            else
            {
                this.list_parent.removeAttr('required');
                this.parent.addClass("hidden");
            }
        }
        

        
    };

    this.MainEvent = function () {
        thisObject.ddl_cmsrole.change(function () {

            var value = $(this).val();
            thisObject.PopulateParent(value);

            thisObject.ddl_parent.prop('selectedIndex', 0);
            thisObject.ddl_parent.change();
        });
    };

}