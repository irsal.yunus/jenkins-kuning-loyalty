// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class QuickWin2Controller
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public QuickWin2Controller() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected QuickWin2Controller(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Index()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult GridGetAllQuickWin2Pager()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllQuickWin2Pager);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public QuickWin2Controller Actions { get { return MVC.QuickWin2; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "QuickWin2";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "QuickWin2";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string GridGetAllQuickWin2 = "GridGetAllQuickWin2";
            public readonly string GridGetAllQuickWin2Pager = "GridGetAllQuickWin2Pager";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string GridGetAllQuickWin2 = "GridGetAllQuickWin2";
            public const string GridGetAllQuickWin2Pager = "GridGetAllQuickWin2Pager";
        }


        static readonly ActionParamsClass_Index s_params_Index = new ActionParamsClass_Index();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Index IndexParams { get { return s_params_Index; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Index
        {
            public readonly string status = "status";
            public readonly string phone = "phone";
            public readonly string tglAwal = "tglAwal";
            public readonly string tglAkhir = "tglAkhir";
        }
        static readonly ActionParamsClass_GridGetAllQuickWin2Pager s_params_GridGetAllQuickWin2Pager = new ActionParamsClass_GridGetAllQuickWin2Pager();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GridGetAllQuickWin2Pager GridGetAllQuickWin2PagerParams { get { return s_params_GridGetAllQuickWin2Pager; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GridGetAllQuickWin2Pager
        {
            public readonly string hiddenUsername = "hiddenUsername";
            public readonly string hiddenAwal = "hiddenAwal";
            public readonly string hiddenAkhir = "hiddenAkhir";
            public readonly string page = "page";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string GridGetAllQuickWin2 = "GridGetAllQuickWin2";
                public readonly string Index = "Index";
            }
            public readonly string GridGetAllQuickWin2 = "~/Views/QuickWin2/GridGetAllQuickWin2.cshtml";
            public readonly string Index = "~/Views/QuickWin2/Index.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_QuickWin2Controller : LoyaltyPointEngine.CMS.Controllers.QuickWin2Controller
    {
        public T4MVC_QuickWin2Controller() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string status);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(string status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "status", status);
            IndexOverride(callInfo, status);
            return callInfo;
        }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string phone, string tglAwal, string tglAkhir);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(string phone, string tglAwal, string tglAkhir)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "phone", phone);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "tglAwal", tglAwal);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "tglAkhir", tglAkhir);
            IndexOverride(callInfo, phone, tglAwal, tglAkhir);
            return callInfo;
        }

        [NonAction]
        partial void GridGetAllQuickWin2Override(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult GridGetAllQuickWin2()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllQuickWin2);
            GridGetAllQuickWin2Override(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void GridGetAllQuickWin2PagerOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string hiddenUsername, string hiddenAwal, string hiddenAkhir, int? page);

        [NonAction]
        public override System.Web.Mvc.ActionResult GridGetAllQuickWin2Pager(string hiddenUsername, string hiddenAwal, string hiddenAkhir, int? page)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllQuickWin2Pager);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "hiddenUsername", hiddenUsername);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "hiddenAwal", hiddenAwal);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "hiddenAkhir", hiddenAkhir);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            GridGetAllQuickWin2PagerOverride(callInfo, hiddenUsername, hiddenAwal, hiddenAkhir, page);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
