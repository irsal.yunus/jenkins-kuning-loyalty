// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberParticipantACBController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public MemberParticipantACBController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected MemberParticipantACBController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Index()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult GridGetAllPager()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllPager);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Delete()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Delete);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public MemberParticipantACBController Actions { get { return MVC.MemberParticipantACB; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "MemberParticipantACB";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "MemberParticipantACB";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string GridGetAllPager = "GridGetAllPager";
            public readonly string DownloadExcelTemplate = "DownloadExcelTemplate";
            public readonly string AddMassParticipant = "AddMassParticipant";
            public readonly string AddSingleParticipant = "AddSingleParticipant";
            public readonly string Delete = "Delete";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string GridGetAllPager = "GridGetAllPager";
            public const string DownloadExcelTemplate = "DownloadExcelTemplate";
            public const string AddMassParticipant = "AddMassParticipant";
            public const string AddSingleParticipant = "AddSingleParticipant";
            public const string Delete = "Delete";
        }


        static readonly ActionParamsClass_Index s_params_Index = new ActionParamsClass_Index();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Index IndexParams { get { return s_params_Index; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Index
        {
            public readonly string status = "status";
        }
        static readonly ActionParamsClass_GridGetAllPager s_params_GridGetAllPager = new ActionParamsClass_GridGetAllPager();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GridGetAllPager GridGetAllPagerParams { get { return s_params_GridGetAllPager; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GridGetAllPager
        {
            public readonly string CampaignID = "CampaignID";
            public readonly string campaignID = "campaignID";
            public readonly string search = "search";
            public readonly string orderBy = "orderBy";
            public readonly string orderDirection = "orderDirection";
            public readonly string page = "page";
        }
        static readonly ActionParamsClass_AddMassParticipant s_params_AddMassParticipant = new ActionParamsClass_AddMassParticipant();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AddMassParticipant AddMassParticipantParams { get { return s_params_AddMassParticipant; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AddMassParticipant
        {
            public readonly string excelFile = "excelFile";
            public readonly string campaignID = "campaignID";
        }
        static readonly ActionParamsClass_AddSingleParticipant s_params_AddSingleParticipant = new ActionParamsClass_AddSingleParticipant();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AddSingleParticipant AddSingleParticipantParams { get { return s_params_AddSingleParticipant; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AddSingleParticipant
        {
            public readonly string memberID = "memberID";
            public readonly string campaignID = "campaignID";
        }
        static readonly ActionParamsClass_Delete s_params_Delete = new ActionParamsClass_Delete();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Delete DeleteParams { get { return s_params_Delete; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Delete
        {
            public readonly string ID = "ID";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string AddMassParticipant = "AddMassParticipant";
                public readonly string AddSingleParticipant = "AddSingleParticipant";
                public readonly string GridGetAllPager = "GridGetAllPager";
                public readonly string Index = "Index";
            }
            public readonly string AddMassParticipant = "~/Views/MemberParticipantACB/AddMassParticipant.cshtml";
            public readonly string AddSingleParticipant = "~/Views/MemberParticipantACB/AddSingleParticipant.cshtml";
            public readonly string GridGetAllPager = "~/Views/MemberParticipantACB/GridGetAllPager.cshtml";
            public readonly string Index = "~/Views/MemberParticipantACB/Index.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_MemberParticipantACBController : LoyaltyPointEngine.CMS.Controllers.MemberParticipantACBController
    {
        public T4MVC_MemberParticipantACBController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string status);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(string status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "status", status);
            IndexOverride(callInfo, status);
            return callInfo;
        }

        [NonAction]
        partial void GridGetAllPagerOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int? CampaignID);

        [NonAction]
        public override System.Web.Mvc.ActionResult GridGetAllPager(int? CampaignID)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllPager);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "CampaignID", CampaignID);
            GridGetAllPagerOverride(callInfo, CampaignID);
            return callInfo;
        }

        [NonAction]
        partial void DownloadExcelTemplateOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult DownloadExcelTemplate()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.DownloadExcelTemplate);
            DownloadExcelTemplateOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void AddMassParticipantOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult AddMassParticipant()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddMassParticipant);
            AddMassParticipantOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void AddMassParticipantOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Web.HttpPostedFileBase excelFile, int campaignID);

        [NonAction]
        public override System.Web.Mvc.ActionResult AddMassParticipant(System.Web.HttpPostedFileBase excelFile, int campaignID)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddMassParticipant);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "excelFile", excelFile);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "campaignID", campaignID);
            AddMassParticipantOverride(callInfo, excelFile, campaignID);
            return callInfo;
        }

        [NonAction]
        partial void AddSingleParticipantOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult AddSingleParticipant()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddSingleParticipant);
            AddSingleParticipantOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void AddSingleParticipantOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int memberID, int campaignID);

        [NonAction]
        public override System.Web.Mvc.ActionResult AddSingleParticipant(int memberID, int campaignID)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddSingleParticipant);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "memberID", memberID);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "campaignID", campaignID);
            AddSingleParticipantOverride(callInfo, memberID, campaignID);
            return callInfo;
        }

        [NonAction]
        partial void GridGetAllPagerOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int? campaignID, string search, string orderBy, string orderDirection, int? page);

        [NonAction]
        public override System.Web.Mvc.ActionResult GridGetAllPager(int? campaignID, string search, string orderBy, string orderDirection, int? page)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllPager);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "campaignID", campaignID);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "search", search);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "orderBy", orderBy);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "orderDirection", orderDirection);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            GridGetAllPagerOverride(callInfo, campaignID, search, orderBy, orderDirection, page);
            return callInfo;
        }

        [NonAction]
        partial void DeleteOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int ID);

        [NonAction]
        public override System.Web.Mvc.ActionResult Delete(int ID)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Delete);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "ID", ID);
            DeleteOverride(callInfo, ID);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
