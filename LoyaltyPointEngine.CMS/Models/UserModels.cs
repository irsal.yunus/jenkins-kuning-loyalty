﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web;
using System.Web.Security;
using LoyaltyPointEngine.Data;

namespace LoyaltyPointEngine.CMS.Models
{
    public class UserModel
    {
        public long ID { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ReEnterPassword { get; set; }
        public string Email { get; set; }
        public long CMSRoleID { get; set; }
        public long? ParentID { get; set; }
        public CMSRole Role { get; set; }
    }

    public class UserLoginModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class NavigationPrivilegeModel
    {
        public long ID { get; set; }
        [Required]
        public long CMSNavigationID { get; set; }
        [Required]
        public long CMSPrivilegeID { get; set; }
    }

    public class RoleNavigationPrivilegeModel
    {
        public long ID { get; set; }
        [Required]
        public long CMSRoleID { get; set; }
        [Required]
        public long CMSNavigationPrivilegeID { get; set; }
    }

    public class UserAccessModel
    {
        public long ID { get; set; }
        public string Controler { get; set; }
        public Guid Action { get; set; }
    }
}