﻿using log4net;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    [UserAccessFilter]
    public partial class BaseController : Controller
    {
        public static string BebeUmbracoAPISecretCode = "B3beS3CRE7C0D3";
        protected static ILog Log;

        protected void SaveLogError(Exception ex)
        {
            // Saving error to log
            string errorMessage = string.Format("Internal server error: {0}", (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            SaveLogError(errorMessage);
        }

        protected void SaveLogError(string errorMessage)
        {
            if (Log == null)
            {
                Log = LogManager.GetLogger(typeof(BaseController));
            }

            // Saving error to log
            Log.Error(errorMessage);
        }
        //
        // GET: /Base/
        public CMSUser CurrentUser
        {
            get
            {
                return CMSUserInformation.CurrentUser;
            }
        }

        public CMSRole CurrentRole
        {
            get
            {
                return CMSUserInformation.CurrentRole;
            }
        }

        public List<CMSNavigationPrivilegeMapping> CurrentNavigationPrivilegeMapping
        {
            get
            {
                return CMSUserInformation.CurrentNavigationPrivilegeMapping;
            }
        }
        public List<CMSNavigation> CurrentNavigation
        {
            get
            {
                return CMSUserInformation.CurrentNavigation;
            }
        }

        public Pool CurrentPool
        {
            get
            {
                return CMSUserInformation.CurrentPool;
            }
        }

        public List<CMSNavigation> SetCurrentNavigationCache()
        {
            List<CMSNavigation> navigations = new List<CMSNavigation>();
            string currNavigationKey = CurrentRole.Name + CurrentRole.ID;
            List<CMSNavigation> cachedsharingskey = new LoyaltyPointEngine.Common.Helper.InMemoryCache().GET<List<CMSNavigation>>(currNavigationKey);
            if (cachedsharingskey != null)
                navigations = cachedsharingskey;
            else
            {
                navigations = CurrentNavigation;
                new LoyaltyPointEngine.Common.Helper.InMemoryCache().SET<dynamic>(currNavigationKey, () => navigations);
            }
            return navigations;
        }

        public void SetRemoveCurrentNavigationCache()
        {
            LoyaltyPointEngine.Common.Helper.InMemoryCache cacheFunc = new LoyaltyPointEngine.Common.Helper.InMemoryCache();
            string currNavigationKey = CurrentRole.Name + CurrentRole.ID;
            List<CMSNavigation> cachedsharingskey = cacheFunc.GET<List<CMSNavigation>>(currNavigationKey);
            if (cachedsharingskey != null)
                cacheFunc.REMOVE(currNavigationKey);
            SetCurrentNavigationCache();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            if (CookieHelper.CookieExist("Administrator_Username") == false)
                filterContext.Result = new RedirectResult(Url.Action(MVC.Account.Login()));
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        private bool? _IsSpgRole;
        public bool IsSpgRole(out string roleCode)
        {
            string spgRoleCode = roleCode = string.Empty;
            if (!_IsSpgRole.HasValue)
            {
                var cmsUserCodeOfSpg = Data.Settings.GetByKey(SiteSetting.CMSUserCodeOfSpg);
                if (cmsUserCodeOfSpg != null)
                    roleCode = spgRoleCode = cmsUserCodeOfSpg.Value;
                if (!string.IsNullOrEmpty(spgRoleCode))
                {
                    Guid spgId;
                    if (Guid.TryParse(spgRoleCode, out spgId))
                    {
                        ViewBag.IsSpg = this._IsSpgRole = CurrentRole.Code == Guid.Parse(spgRoleCode);
                        return this._IsSpgRole.Value;
                    }
                }
            }
            return ViewBag.IsSpg = this._IsSpgRole.GetValueOrDefault(false);
        }
    }
}
