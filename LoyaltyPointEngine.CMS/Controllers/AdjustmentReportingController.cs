﻿using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object.Adjustment;
using LoyaltyPointEngine.Model.Object.Adjustment;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{

    public partial class AdjustmentReportingController : Controller
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Reporting - Adjustment")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Adjustment")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string startDate, string endDate)
        {
            DateTime _from, _to;
            DateTime? from = null, to = null;

            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _from)) from = _from;
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(endDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _to)) to = _to;
            }

            var datas = GetAllAdjustmentModel(from, to);
            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            if (datas != null)
            {
                report = GetAdjustmentReporting(datas.OrderByDescending(x => x.CreatedDate).ToList());
            }
            fileName = string.Format("AdjustmentReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Code", "ExpiredDate", "TransactionDate", "Point", "CreatedDate", "MemberName", "Reason", "MemberEmail" });
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        private List<List<string>> GetAdjustmentReporting(List<AdjustmentReportingModel> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                data.Add(item.Code.ToString());
                data.Add(item.ExpiredDate.ToString("dd/MM/yyy  HH:mm:ss"));
                data.Add(item.TransactionDateDT.ToString("dd/MM/yyy  HH:mm:ss"));
                data.Add(item.Point.ToString());
                data.Add(item.CreatedDate.ToString("dd/MM/yyy  HH:mm:ss"));
                data.Add(item.MemberName);
                data.Add(item.Reason);
                data.Add(item.MemberEmail);
                datas.Add(data);
            }
            return datas;
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAll(DateTime? hiddenAwal, DateTime? hiddenAkhir)
        {
            gridMvcHelper = new GridMvcHelper();
            // IOrderedQueryable<Member> allMember = Member.GetAll().OrderBy(x => x.FirstName);

            var datas = GetAllAdjustmentModel(hiddenAwal, hiddenAkhir);

            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllPager(string hiddenAwal, string hiddenAkhir, int? page)
        {
            gridMvcHelper = new GridMvcHelper();

            DateTime _from, _to;
            DateTime? from = null, to = null;

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _from)) from = _from;
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _to)) to = _to;
            }

            var datas = GetAllAdjustmentModel(from, to);

            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.AdjustmentReporting.Views.GridGetAll, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        private IQueryable<AdjustmentReportingModel> GetAllAdjustmentModel(DateTime? from, DateTime? to)
        {
            IQueryable<Adjustment> adjustments = LoyaltyPointEngine.Data.Adjustment.GetAll();

            if (from.HasValue) adjustments = adjustments.Where(x => EntityFunctions.TruncateTime(from) <= EntityFunctions.TruncateTime(x.TransactionDate));
            if (to.HasValue) adjustments = adjustments.Where(x => EntityFunctions.TruncateTime(to) >= EntityFunctions.TruncateTime(x.TransactionDate));

            var query = from a in adjustments
                        join b in Data.Member.GetAll() on a.MemberID equals b.ID
                        into memberTemp
                        from b_temp in memberTemp.DefaultIfEmpty()
                        select new AdjustmentReportingModel
                        {
                            ID = a.ID,
                            Code = a.Code,
                            ExpiredDate = a.ExpiredDate,
                            TransactionDateDT = a.TransactionDate,
                            Point = a.Point,
                            CreatedDate = a.CreatedDate,
                            MemberName = b_temp.Name,
                            Reason = a.Reason,
                            MemberEmail = b_temp.Email
                        };
            return query;
        }

    }

}