﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.Model.Object;
using System.Transactions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RetailerUploadController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Upload Retailer Account")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Index(HttpPostedFileBase excelFile)
        {
            ResultModel<string> result = new ResultModel<string>();
            if (excelFile != null)
            {
                ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
                List<string> headers = listData.Headers;
                List<List<string>> datas = listData.DataRows;
                List<string> errs = new List<string>();
                result.Value = UploadMasterAccount(datas, CurrentUser.FullName,out errs);
                result.StatusCode = "00";
                result.StatusMessage = "Success";
                if(errs != null && errs.Count >0)
                {
                    result.StatusMessage = "Detail Error: <br/>" + PopulateErrorTable(errs);
                }

               
            }

            return View(result);
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/RetailerAccountMarketTemplate.xlsx"));
            string fileName = "UploadAccountMarketTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


        private string UploadMasterAccount(List<List<string>> datas, string by, out List<string> errs)
        {
            string result = "";
            errs = new List<string>();
            int indexRow = 1;
            int total = 0;
            int totalSuccess = 0;
            int totalFailed = 0;
            int notinsert = 0;
            int insert = 0;
            foreach (var row in datas)
            {

                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    string MarketCd, MarketDesc, ChannelCd, ChannelDesc, SubChannelCd, SubChannelDesc, AccountCd, AccountDesc, SubAccountCd, 
                        SubAccountDesc, retailercode, retailername, DistrictID;

                    MarketCd = row != null && row.Count >= 1 ? row[0] : "";
                    MarketDesc = row != null && row.Count >= 2 ? row[1] : "";
                    ChannelCd = row != null && row.Count >= 3 ? row[2] : "";
                    ChannelDesc = row != null && row.Count >= 4 ? row[3] : "";
                    SubChannelCd = row != null && row.Count >= 5 ? row[4] : "";
                    SubChannelDesc = row != null && row.Count >= 6 ? row[5] : "";
                    AccountCd = row != null && row.Count >= 7 ? row[6] : "";
                    AccountDesc = row != null && row.Count >= 8 ? row[7] : "";
                    SubAccountCd = row != null && row.Count >= 9 ? row[8] : "";
                    SubAccountDesc = row != null && row.Count >= 10 ? row[9] : "";
                    SubAccountDesc = row != null && row.Count >= 10 ? row[9] : "";
                    //ucup
                    retailercode = row != null && row.Count >= 11 ? row[10] : "";
                    retailername = row != null && row.Count >= 12 ? row[11] : "";
                    DistrictID = row != null && row.Count >= 13 ? row[12] : "";
                    if (row != null && row.Count >= 1)
                    {
                        var dataretail = Retailer.GetByCode(retailercode);
                        if (dataretail != null)
                        {
                            notinsert++;
                        }
                        else
                        {
                            insert++;
                        }
                        string errRow = "";
                        var returnRow = ProcessingRowExcel(MarketCd, MarketDesc, ChannelCd, ChannelDesc, SubChannelCd, SubChannelDesc,
                            retailercode, retailername, DistrictID, AccountCd, AccountDesc, SubAccountCd, SubAccountDesc,
                              by, out errRow);
                        total++;
                        if (returnRow == 1)
                        {
                            transScope.Complete();
                            totalSuccess++;
                        }
                        else
                        {
                            totalFailed++;
                            errs.Add(string.Format("Error Row '{0}'  --> {1}", indexRow, errRow));
                        }
                        transScope.Dispose();
                    }
                }

                if(  indexRow ==100)
                {

                }
                if (indexRow == 200)
                {

                }

                indexRow++;
            }


            result = string.Format("total processed data: {0}<br/>Success: {1}<br/>Retailer success: {4}<br/>Retailer failed: {3}<br/>Failed: {2}",
                   total, totalSuccess, totalFailed, notinsert, insert);  //ucup
            return result;
        }

        private string PopulateErrorTable(List<string> errs)
        {
            string result = "";

            if(errs != null && errs.Count >0)
            {
                result = "<ol>";

                int index = 1;
                foreach(var item in errs)
                {

                    result += string.Format("<li>{1}</li>", index, item);
                    index++;
                }

                result += "</ol>";

            }

            return result;

        }


        /// <summary>

        /// -1 == failed
        /// 1 == Success
        /// </summary>
        /// <param name="MarketCd"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        private int ProcessingRowExcel(string MarketCd, string MarketDesc, string ChannelCd,
            string ChannelDesc, string SubChannelCd, string SubChannelDesc, string retailercode, string retailername, string DistrictID,
            string AccountCd, string AccountDesc, string SubAccountCd, string SubAccountDesc, string userCMSName, out string err)
        {
            int result = 1;
            err = "";

            if (string.IsNullOrEmpty(MarketCd))
            {
                result = -1;
                err = string.Format("MarketCd is required");
                return result;
            }

            if (string.IsNullOrEmpty(ChannelCd))
            {
                result = -1;
                err = string.Format("ChannelCd is required");
                return result;
            }

            if (string.IsNullOrEmpty(SubChannelCd))
            {
                result = -1;
                err = string.Format("SubChannelCd is required");
                return result;
            }

            if (string.IsNullOrEmpty(AccountCd))
            {
                result = -1;
                err = string.Format("AccountCd is required");
                return result;
            }

            if (string.IsNullOrEmpty(SubAccountCd))
            {
                result = -1;
                err = string.Format("SubAccountCd is required");
                return result;
            }
            //ucup validasi
            if (string.IsNullOrEmpty(retailercode))
            {
                result = -1;
                err = string.Format("RetailerCode is required");
                return result;
            }

            if (string.IsNullOrEmpty(retailername))
            {
                result = -1;
                err = string.Format("RetailerName is required");
                return result;
            }

            var dataMarket = RetailerMarket.GetByCode(MarketCd);
            if (dataMarket == null)
            {
                dataMarket = new RetailerMarket();
                dataMarket.Code = MarketCd;
                dataMarket.MarketDesc = MarketDesc;
                var response = dataMarket.Insert(userCMSName);
                if (!response.Success)
                {
                    result = -1;
                    err = string.Format("error when insert RetailerMarket with code {0}. Error Detail: {1} ", MarketCd, response.ErrorMessage);
                    return result;
                }
            }

            if (dataMarket != null)
            {
                var dataChannel = RetailerChannel.GetByCode(ChannelCd);
                if (dataChannel == null)
                {
                    dataChannel = new RetailerChannel();
                    dataChannel.Code = ChannelCd;
                    dataChannel.ChannelDesc = ChannelDesc;
                    dataChannel.RetailerMarketID = dataMarket.ID;
                    var response = dataChannel.Insert(userCMSName);
                    if (!response.Success)
                    {
                        result = -1;
                        err = string.Format("error when insert RetailerChannel with code {0}. Error Detail: {1} ", ChannelCd, response.ErrorMessage);
                        return result;
                    }
                    else
                    {

                    }
                }



                if (dataChannel != null)
                {
                    var dataSubChannel = RetailerSubChannel.GetByCode(SubChannelCd);
                    if (dataSubChannel == null)
                    {
                        dataSubChannel = new RetailerSubChannel();
                        dataSubChannel.Code = SubChannelCd;
                        dataSubChannel.SubChannelDesc = SubChannelDesc;
                        dataSubChannel.RetailerChannelID = dataChannel.ID;

                        var response = dataSubChannel.Insert(userCMSName);
                        if (!response.Success)
                        {
                            result = -1;
                            err = string.Format("error when insert RetailerSubChannel with code {0}. Error Detail: {1} ", SubChannelCd, response.ErrorMessage);
                            return result;
                        }
                    }


                    if (dataSubChannel != null)
                    {
                        var dataAccount = RetailerAccount.GetByCode(AccountCd);
                        if (dataAccount == null)
                        {
                            dataAccount = new RetailerAccount();
                            dataAccount.Code = AccountCd;
                            dataAccount.AccountDesc = AccountDesc;
                            dataAccount.RetailerSubChannelID = dataSubChannel.ID;

                            var response = dataAccount.Insert(userCMSName);
                            if (!response.Success)
                            {
                                result = -1;
                                err = string.Format("error when insert RetailerAccount with code {0}. Error Detail: {1} ", AccountCd, response.ErrorMessage);
                                return result;
                            }
                        }


                        if (dataAccount != null)
                        {

                            var dataSubAccount = RetailerSubAccount.GetByCode(SubAccountCd);
                            if (dataSubAccount == null)
                            {
                                dataSubAccount = new RetailerSubAccount();
                                dataSubAccount.Code = SubAccountCd;
                                dataSubAccount.SubAccountDesc = SubAccountDesc;
                                dataSubAccount.RetailerAccountID = dataAccount.ID;

                                var response = dataSubAccount.Insert(userCMSName);
                                if (!response.Success)
                                {
                                    result = -1;
                                    err = string.Format("error when insert RetailerSubAccount with code {0}. Error Detail: {1} ", SubAccountCd, response.ErrorMessage);
                                    return result;
                                }
                            }

                            ///ucup
                            if (dataSubAccount != null)
                            {
                                var dataretail = Retailer.GetByCode(retailercode);
                                if (dataretail == null)
                                {
                                    dataretail = new Retailer();
                                    dataretail.RetailerSubAccountID = dataSubAccount.ID;
                                    dataretail.Code = retailercode;
                                    dataretail.Name = retailername;
                                    
                                    if (DistrictID != "")
                                    {
                                        var city = City.GetByCode(DistrictID); //search cityCode di City
                                        if (city != null)
                                        {
                                        dataretail.CityID = city.ID;
                                        }
                                    }
                                    dataretail.InsertMigrate(userCMSName); //insert tabel retailer

                                }
                            }

                        }



                    }

                }
            }

            return result;
        }
    }
}