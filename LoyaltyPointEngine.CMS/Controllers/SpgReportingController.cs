﻿using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object.Member;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class SpgReportingController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        // GET: SpgReporting
        [BreadCrumb(Clear = true, Label = "Spg Reporting")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index()
        {
            var currentSpg = GetCurrentSpg();
            if (currentSpg == null) return Redirect("/Error");
            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Spg Reporting")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Index(string tglAwal, string tglAkhir)
        {
            var currentSpg = GetCurrentSpg();
            if (currentSpg == null)
                return Redirect("/Error");

            var dataRaw = GetMemberRegisteredBySpgId(currentSpg.ID);
            if (!string.IsNullOrEmpty(tglAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    dataRaw = dataRaw.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(tglAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    dataRaw = dataRaw.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }

            var datas = dataRaw.Select(a => new MemberGridModel
                        {
                            ID = a.ID,
                            Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            Email = a.Email,
                            Phone = a.Phone,
                            IsBlacklist = a.IsBlacklist,
                            Point = a.MemberPoint,
                            CreatedDate = a.CreatedDate,
                            Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                            Channel = a.Channel.Name
                        }).ToList();

            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();

            foreach (var data in datas)
            {
                List<string> row = new List<string>();
                row.Add(data.Type.ToString());
                row.Add(data.ID.ToString());
                row.Add(data.Gender.ToString());
                row.Add(data.FirstName.ToString());
                row.Add(data.LastName.ToString());
                row.Add(data.Email.ToString());
                row.Add(data.Phone.ToString());
                row.Add(data.CreatedDate.ToString());
                row.Add(data.Channel);
                report.Add(row);
            }

            fileName = string.Format("SpgReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Type", "ID", "Gender", "FirstName", "LastName", "Email", "Phone", "CreatedDate", "Channel" });
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllMemberBySpg()
        {
            var currentSpg = GetCurrentSpg();
            if (currentSpg == null)
                return Redirect("/Error");

            gridMvcHelper = new GridMvcHelper();
            var datas = from a in GetMemberRegisteredBySpgId(currentSpg.ID)
                        select new MemberGridModel
                        {
                            ID = a.ID,
                            Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            Email = a.Email,
                            Phone = a.Phone,
                            IsBlacklist = a.IsBlacklist,
                            Point = a.MemberPoint,
                            CreatedDate = a.CreatedDate,
                            Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                            Channel = a.Channel.Name
                        };

            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(MVC.SpgReporting.Views.GridGetAllMemberBySpg, grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllMemberBySpgPager(string hiddenAwal, string hiddenAkhir, string search, string orderBy, string orderDirection, int? page)
        {
            var currentSpg = GetCurrentSpg();
            if (currentSpg == null)
                return Redirect("/Error");

            gridMvcHelper = new GridMvcHelper();
            var datas = from a in GetMemberRegisteredBySpgId(currentSpg.ID)
                        select new MemberGridModel
                        {
                            ID = a.ID,
                            Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            Email = a.Email,
                            Phone = a.Phone,
                            IsBlacklist = a.IsBlacklist,
                            Point = a.MemberPoint,
                            CreatedDate = a.CreatedDate,
                            Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                            Channel = a.Channel.Name
                        };

            if (!string.IsNullOrEmpty(search))
            {
                datas = datas.Where(x => x.FirstName.Contains(search) || x.LastName.Contains(search) || x.Email.Contains(search) || x.Phone.Contains(search));
            }

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }

            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.SpgReporting.Views.GridGetAllMemberBySpg, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<Member> GetMemberRegisteredBySpgId(int Id)
        {
            return Member.GetAll().Where(x => x.IsApproved && x.IsActive && !x.IsBlacklist && !x.IsDeleted && x.SpgID == Id);
        }

        private SPG GetCurrentSpg()
        {
            string spgRoleCode;
            if (this.IsSpgRole(out spgRoleCode))
            {
                if (CurrentRole.Code == Guid.Parse(spgRoleCode))
                    return SPG.GetByCode(CurrentUser.Username);
            }
            return null;
        }
    }
}