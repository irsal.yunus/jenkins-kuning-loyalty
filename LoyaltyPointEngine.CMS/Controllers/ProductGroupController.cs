﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Product;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ProductGroupController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "ProductGroup successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "ProductGroup successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "ProductGroup successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }




        [BreadCrumb(Label = "Detail Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int id)
        {
            var data = ProductGroup.GetByID(id);
            ProductGroupModel model = new ProductGroupModel(data);
            return View(model);
        }


        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ProductGroupModel model)
        {
            ValidateEdit(model);

            if (ModelState.IsValid)
            {
                string by = CurrentUser.FullName;
                ProductGroup data = ProductGroup.GetByID(model.ID);
                if (data != null)
                {
                    data.ProductGroupCode = model.ProductGroupCode;
                    data.IsActived = model.IsActived;
                    var ressEF = data.Update(by);
                    if (ressEF.Success)
                    {
                        return RedirectToAction(MVC.ProductGroup.Index("Edit_Success"));
                    }
                    else
                    {
                        ModelState.AddModelError("", ressEF.ErrorMessage);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "ID not founded");
                }
               
            }


            return View(model);
        }

        private void ValidateEdit(ProductGroupModel model)
        {
            if (string.IsNullOrEmpty(model.ProductGroupCode))
            {
                ModelState.AddModelError("ProductGroupCode", "ProductGroupCode is required");
            }
            else
            {
                var predictedData = Data.ProductGroup.GetByCodeNotSelf(model.ProductGroupCode, model.ID);
                if (predictedData != null)
                {
                    ModelState.AddModelError("ProductGroupCode", "The code has already been used by other data");
                }
            }
        }


        [BreadCrumb(Label = "Add Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ProductGroupModel model)
        {
            ValidateAdd(model);

            if (ModelState.IsValid)
            {
                string by = CurrentUser.FullName;
                ProductGroup data = new ProductGroup();
                data.ProductGroupCode = model.ProductGroupCode;
                data.IsActived = model.IsActived;
                var ressEF = data.Insert(by);
                if (ressEF.Success)
                {
                    return RedirectToAction(MVC.ProductGroup.Index("Create_Success"));
                }
                else
                {
                    ModelState.AddModelError("", ressEF.ErrorMessage);
                }

            }


            return View(model);
        }

        private void ValidateAdd(ProductGroupModel model)
        {
            if (string.IsNullOrEmpty(model.ProductGroupCode))
            {
                ModelState.AddModelError("ProductGroupCode", "ProductGroupCode is required");
            }
            else
            {
                var predictedData = Data.ProductGroup.GetByCode(model.ProductGroupCode);
                if (predictedData != null)
                {
                    ModelState.AddModelError("ProductGroupCode", "The code has already been used by other data");
                }
            }
        }


        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllProductGroup()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<LoyaltyPointEngine.Data.ProductGroup> datas = LoyaltyPointEngine.Data.ProductGroup.GetAll().OrderBy(x => x.ProductGroupCode);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllProductGroupPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<LoyaltyPointEngine.Data.ProductGroup> datas = LoyaltyPointEngine.Data.ProductGroup.GetAll().OrderBy(x => x.ProductGroupCode);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.ProductGroupCode.Contains(txtSearch)).OrderBy(x => x.ProductGroupCode);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.ProductGroup.Views.GridGetAllProductGroup, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}