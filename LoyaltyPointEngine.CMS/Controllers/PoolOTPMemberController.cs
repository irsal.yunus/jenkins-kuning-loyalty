﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Parameter.OTP;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.PointLogicLayer.OTP;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Helper;
using Grid.Mvc.Ajax.GridExtensions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class PoolOTPMemberController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Pool OTP Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "OTP Code successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "OTP Code successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "OTP successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(string ID)
        {
            List<PoolOTPMember> OTPlist = PoolOTPMember.GetAll().Where(x => x.Phone == ID).ToList();
            foreach (var OTP in OTPlist)
            {
                OTP.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            }
            return RedirectToAction(MVC.PoolOTPMember.Index("Delete_Success"));
        }
        #region List OTP Code Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllOTP()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<PoolOTPMember> allOTP = PoolOTPMember.GetAll().OrderBy(x => x.CreatedDate);
            AjaxGrid<PoolOTPMember> grid = this.gridMvcHelper.GetAjaxGrid(allOTP);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllOTPPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<PoolOTPMember> allData = PoolOTPMember.GetAll().OrderBy(x => x.CreatedDate)
                .OrderBy(x => x.CreatedDate);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.OTPCode.Contains(txtSearch) || x.Phone.Contains(txtSearch) || x.Status.Contains(txtSearch) &&
                        x.OTPExpiredDate > DateTime.Now).OrderBy(x => x.CreatedDate);
            }
            AjaxGrid<PoolOTPMember> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.PoolOTPMember.Views.GridGetAllOTP, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}