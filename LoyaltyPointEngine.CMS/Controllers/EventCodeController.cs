﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.EventCode;
using LoyaltyPointEngine.Model.Parameter.EventCode;
using MvcBreadCrumbs;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class EventCodeController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Kode Event")]

        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Kode Event successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Kode Event successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Kode Event successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Kode Event")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            EventCodeParam model = new EventCodeParam();
            return View(model);
        }

        [BreadCrumb(Label = "Add Kode Event")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        [HttpPost]
        public virtual ActionResult Add(EventCodeParam model)
        {
            ValidateEventCode(model);
            if (ModelState.IsValid)
            {
                try
                {
                    //var Dist = Data.District.GetById(model.DistrictID.Value);
                    EventCodeModel data = new EventCodeModel();
                    data.Code = model.Code;
                    data.StartDate = model.StartDate;
                    data.EndDate = model.EndDate;
                    EventCodeLogic.AddEventCode(data, Convert.ToInt32(CurrentUser.ID), CurrentUser.FullName, Request.Url.AbsoluteUri);
                    return RedirectToAction(MVC.EventCode.Index("Create_Success"));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Ini error : " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
                }
                model.StartDateStr = model.StartDate.ToString("dd'/'MM'/'yyyy");
                model.EndDateStr = model.EndDate.ToString("dd'/'MM'/'yyyy");
            }
            
            return View(model);
        }

        [BreadCrumb(Label = "Edit Kode Event")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int id)
        {
            var data = EventCodeLogic.GetEventCodeById(id);
            EventCodeParam model = new EventCodeParam();
            model.ID = data.Value.ID;
            model.Code = data.Value.Code;
            model.StartDateStr = data.Value.StartDate.ToString("dd'/'MM'/'yyyy");
            model.EndDateStr = data.Value.EndDate.ToString("dd'/'MM'/'yyyy");
            return View(model);
        }

        [BreadCrumb(Label = "Edit Kode Event")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        [HttpPost]
        public virtual ActionResult Edit(EventCodeParam model)
        {
            ValidateEventCode(model);
            if (ModelState.IsValid)
            {
                var data = EventCodeLogic.GetEventCodeById(model.ID);
                if (data != null)
                {
                    //var Dist = Data.District.GetById(model.DistrictID);
                    EventCodeModel _model = new EventCodeModel();
                    _model.ID = model.ID;
                    _model.Code = model.Code;
                    _model.StartDate = model.StartDate;
                    _model.EndDate = model.EndDate;
                    EventCodeLogic.UpdateEventCode(_model, Convert.ToInt32(CurrentUser.ID), CurrentUser.FullName, Request.Url.AbsoluteUri);
                    return RedirectToAction(MVC.EventCode.Index("Edit_Success"));
                }
                else
                {
                    ModelState.AddModelError("", "ID not founded in system");
                }
                model.StartDateStr = model.StartDate.ToString("dd'/'MM'/'yyyy");
                model.EndDateStr = model.EndDate.ToString("dd'/'MM'/'yyyy");
            }
            
            return View(model);
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            var data = EventCodeLogic.GetEventCodeById(ID);
            EventCodeModel model = new EventCodeModel();
            model.ID = data.Value.ID;
            model.Code = data.Value.Code;
            model.StartDate = data.Value.StartDate;
            model.EndDate = data.Value.EndDate;
            EventCodeLogic.DeleteEventCode(model, Convert.ToInt32(CurrentUser.ID), CurrentUser.FullName, Request.Url.AbsoluteUri);
            return RedirectToAction(MVC.EventCode.Index("Delete_Success"));
        }

        #region List Event Code Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllEventCode()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<EventCode> allEvnetCode = EventCode.GetAll().OrderBy(x => x.Code);
            AjaxGrid<EventCode> grid = this.gridMvcHelper.GetAjaxGrid(allEvnetCode);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllEventPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<EventCode> allData = EventCode.GetAll().OrderBy(x => x.Code);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Code.Contains(txtSearch))
                    .OrderBy(x => x.Code);
            }
            AjaxGrid<EventCode> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.EventCode.Views.GridGetAllEventCode, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        private void ValidateEventCode(EventCodeParam model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Code))
                {
                    ModelState.AddModelError("Code", "Code harus di isi");
                }
                else
                {
                    var checkAvaibility = EventCodeLogic.GetEventCodeByCode(model.Code);
                    if (checkAvaibility.StatusCode == "00" && checkAvaibility.Value.ID != model.ID)
                    {
                        ModelState.AddModelError("Code", "Kode Event sudah terdaftar di sistem");
                    }
                }

                if (model.StartDateStr != null || model.EndDateStr != null)
                {
                    DateTime dtStartDateTime = DateTime.MinValue;
                    DateTime dtEndDateTime = DateTime.MinValue;
                    if (DateTime.TryParseExact(model.StartDateStr.Replace("-", "/"), "dd/MM/yyyy", CultureInfo.CurrentCulture,
                        DateTimeStyles.None, out dtStartDateTime))
                    {
                        if (dtStartDateTime <= DateTime.MinValue)
                        {
                            ModelState.AddModelError("StartDate", "Format tanggal tidak sesuai");
                        }
                        else
                        {
                            model.StartDate = dtStartDateTime;
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("StartDate", "Format tanggal tidak sesuai");
                    }

                    if (DateTime.TryParseExact(model.EndDateStr.Replace("-", "/"), "dd/MM/yyyy", CultureInfo.CurrentCulture,
                        DateTimeStyles.None, out dtEndDateTime))
                    {
                        if (dtEndDateTime <= DateTime.MinValue)
                        {
                            ModelState.AddModelError("EndDate", "Format tanggal tidak sesuai");
                        }
                        else
                        {
                            model.EndDate = dtEndDateTime;
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("EndDate", "Format tanggal tidak sesuai");
                    }

                    if (model.StartDate > DateTime.MinValue && model.EndDate > DateTime.MinValue)
                    {
                        if (model.StartDate >= model.EndDate)
                        {
                            ModelState.AddModelError("Datetime", "End Date tidak boleh lebih besar dari Start Date");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("Datetime", "Tanggal tidak boleh kosong");
                }


            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Ini error : " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            }
        }
    }
}