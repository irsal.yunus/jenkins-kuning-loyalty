﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Cluster;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ClusterController : BaseController
    {
        // GET: Cluster
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Cluster")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Setting successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Setting successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Setting successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Cluster")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ClusterModel model = new ClusterModel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ClusterModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {

                Cluster Cluster = new Cluster();
                Cluster.Name = model.Name;
                Cluster.IsActive = model.IsActive;
                var result = Cluster.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Cluster.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        private void ValidateAddModel(ClusterModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
        }

        private void ValidateEditModel(ClusterModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
        }
        [BreadCrumb(Label = "Edit Cluster")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int ClusterId)
        {
            Cluster currentCluster = LoyaltyPointEngine.Data.Cluster.GetById(ClusterId);
            ClusterModel model = new ClusterModel();
            model.InjectFrom(currentCluster);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ClusterModel model)
        {
            ValidateEditModel(model);
            Cluster Cluster = LoyaltyPointEngine.Data.Cluster.GetById(model.ID);
            if (Cluster == null)
            {
                ModelState.AddModelError("ID", "ID is not registered in system");
            }


            if (ModelState.IsValid)
            {
                Cluster.Name = model.Name;
                Cluster.IsActive = model.IsActive;
                var result = Cluster.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Cluster.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Cluster Cluster = Cluster.GetById(ID);
            Cluster.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Cluster.Index("Delete_Success"));
        }

        #region List Cluster Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllCluster()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Cluster> allCluster = LoyaltyPointEngine.Data.Cluster.GetAll().OrderBy(x => x.Name);
            AjaxGrid<Cluster> grid = this.gridMvcHelper.GetAjaxGrid(allCluster);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllClusterPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Cluster> allData = LoyaltyPointEngine.Data.Cluster.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<Cluster> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Cluster.Views.GridGetAllCluster, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}