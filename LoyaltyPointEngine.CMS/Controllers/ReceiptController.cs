﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System.Globalization;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using System.IO;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.PointLogicLayer;
using System.Transactions;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Model.Object.Common;
using System.Data.Objects;
using Newtonsoft.Json.Linq;
using System.Text;
using log4net;
using Newtonsoft.Json;
using System.Data.Entity.Infrastructure;
using Nest;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ReceiptController : BaseController
    {
        public ReceiptController()
        {
            Log = LogManager.GetLogger(typeof(ReceiptController));
        }
        // GET: Receipt
        private IGridMvcHelper gridMvcHelper;
        private string spgRoleCode;

        [BreadCrumb(Clear = true, Label = "Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Receipt successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Receipt successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Receipt successfully deleted";
                    break;
                case "Ocr_Reject":
                    ViewBag.Success = "Can not moderate receipt because it is still on ocr processing.";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            ViewBag.ReceiptStatusList = EnumHelper.ToSelectList<Receipt.ReceiptStatus>(true);
            ViewBag.ChannelList = EnumHelper.ToSelectList<Channel>();
            var spg = DataMember.SPG.GetAll();
            SelectList spgList = new SelectList(spg, "Code", "Code");
            ViewBag.SPGCodeList = spgList.ToList();
            var eventCode = EventCode.GetAll().Where(x => DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate); ;
            SelectList eventCodeList = new SelectList(eventCode, "ID", "Code");
            ViewBag.EventCodeList = eventCodeList.ToList();
            GetListRetailer();
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string hiddenAwal, string hiddenAkhir, Channel? channelHidden, string spgCodeHidden, int? eventCodeHidden, Receipt.ReceiptStatus? statusHidden, string memberIDHidden, int? retailerHidden, string retailerHasHidden)
        {
            DateTime _from, _to;
            DateTime? from = null, to = null;

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _from)) from = _from;
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _to)) to = _to;
            }

            int tempMemberId,
                totalRow = 0;
            int? paramMemberId = null,
                paramStatus = null;

            if (int.TryParse(memberIDHidden, out tempMemberId))
                paramMemberId = tempMemberId;

            if (statusHidden.HasValue)
                paramStatus = Convert.ToInt32(statusHidden);

            var datas = GetAllReceiptModel(paramMemberId, Convert.ToString(channelHidden), spgCodeHidden, eventCodeHidden, paramStatus, from, to, null, null, "CreatedDate", "Desc", null, retailerHidden, retailerHasHidden, out totalRow);

            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetReceiptReporting(datas.ToList());
            fileName = string.Format("SubReceiptReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "MemberName", "MemberPhone", "Code", "ReceiptCode", "TransactionDateDate", "RetailerName", "Status", "Channel", "CreatedDate", "EventCode" });
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        [BreadCrumb(Label = "Add Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {

            UploadReceiptCMSModel model = new UploadReceiptCMSModel();
            GetListRetailer();
            GetListCampaign();
            GetListAction();
            GetListEventCode();
            ViewBag.ChannelList = EnumHelper.ToSelectList<Channel>();
            if (this.IsSpgRole(out spgRoleCode))
            {
                model.Channel = Convert.ToString(Channel.ELINA);
            }
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddJson(IEnumerable<HttpPostedFileBase> Image)
        {
            bool isSuccess = false, isSpg = false;
            string message = string.Empty, error = string.Empty;
            isSpg = this.IsSpgRole(out spgRoleCode);
            try
            {
                UploadReceiptCMSModel model = GenerateReceiptObjectFromRequest(out error);
                if (string.IsNullOrEmpty(error))
                {
                    error = ValidateAddReceipt(model, Image.ToList());
                }
                if (string.IsNullOrEmpty(error))
                {
                    string createdby = CurrentUser.FullName;
                    var code = Number.Generated(Number.Type.RCP, createdby);
                    var listImage = Image.ToList();
                    Guid newID = Guid.NewGuid();
                    var result = ReceiptLogic.InsertFromCMS(model, newID, listImage, CurrentPool.ID, createdby, Request.Url.AbsoluteUri, CurrentUser.ID, code, isSpg);
                    if (result.StatusCode == "00")
                    {
                        //transScope.Complete();
                        //transScope.Dispose();
                        isSuccess = true;
                        message = result.StatusMessage;
                    }
                    else
                    {
                        // transScope.Dispose();
                        message = result.StatusMessage;
                    }
                }
                else
                {
                    message = error;
                }
                return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message, IsNeedRetry = true }, JsonRequestBehavior.AllowGet);
            }
        }

        private string ValidateAddReceipt(UploadReceiptCMSModel model, List<HttpPostedFileBase> images)
        {
            bool isError = false;
            string error = "<ul>";

            if (model.MemberID <= 0)
            {
                error += "<li>Member is required</li>";
                isError = true;
            }

            //This Validate For Image upload not required for LoyaltyCMS
            //if (images == null || images.Count <= 0)
            //{
            //    error += "<li>Image is required</li>";
            //    isError = true;
            //}
            //else
            //{
            //    int i = 1;
            //    foreach (var item in images)
            //    {
            //        if (item == null || item.InputStream == null && images[0] == null)
            //        {
            //            error += "<li>Image is required</li>";
            //            isError = true;
            //        }
            //        else if (item == null || item.InputStream == null)
            //        {
            //            error += string.Format("<li>Image no.{0} not selected</li>", i);
            //            isError = true;
            //        }
            //        i++;
            //    }
            //}

            DateTime modelTransactionDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrTransactionDate) && !string.IsNullOrEmpty(model.StrTransactionTime))
            {
                string datetime = model.StrTransactionDate + " " + model.StrTransactionTime;
                if (!DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out modelTransactionDate))
                {
                    error += "<li>Format Transaction date or time is not correct, please using format dd/MM/yyyy HH:mm:ss</li>";
                    isError = true;
                }
                else
                {
                    int intLimitTransDateSetting = 0;
                    string strLimitTransDateSetting = Data.Settings.GetValueByKey("LimitUploadReceiptTransactionDate");
                    int.TryParse(strLimitTransDateSetting, out intLimitTransDateSetting);
                    DateTime limitTransDate = DateTime.Now.AddMonths(-(intLimitTransDateSetting)).Date;

                    if (intLimitTransDateSetting > 0 && modelTransactionDate < limitTransDate)
                    {
                        error += "<li>Transaction date can not lower than limit date upload receipt</li>";
                        isError = true;
                    }
                }
            }
            else
            {
                error += "<li>Transaction Date and Transaction time are Mandatory</li>";
                isError = true;
            }

            DateTime modelRequestDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrRequestDate))
            {
                string datetime = model.StrRequestDate + " 23:59:59";
                if (!DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out modelRequestDate))
                {
                    error += "<li>Format Request date is not correct, please using format dd/MM/yyyy</li>";
                    isError = true;
                }
                else
                {

                    if (modelRequestDate < modelTransactionDate)
                    {
                        error += "<li>Request date can not lower than transaction date upload receipt</li>";
                        isError = true;
                    }
                }
            }

            #region Validation Fraud / Bot / Spam multiple upload struk
            List<int> listActionID = (model.Details != null) ? model.Details.Select(x => x.ActionID).ToList() : null;
            int totalDuplicateData = Receipt.CheckTotalDuplicateReceipt(model.MemberID, modelTransactionDate, null, listActionID);
            if (totalDuplicateData >= 3)
            {
                error += "<li>Can not insert double receipt, your receipt data has been submitted more than 3 times </li>";
                isError = true;
            }
            else
            {
                if (string.IsNullOrEmpty(model.ReceiptCode))
                {
                    int totalPoint = model.ReceiptDetails.Sum(z => z.Quantity * z.Points);
                    var possibleDuplicateReceipt = Receipt.GetByMemberId(model.MemberID).Where(x => x.TransactionDate == modelTransactionDate);
                    possibleDuplicateReceipt = possibleDuplicateReceipt.Where(x => x.RetailerID == model.RetailerID);
                    possibleDuplicateReceipt = possibleDuplicateReceipt.Where(x => x.Collect.Where(y => !y.IsDeleted).Sum(y => y.TotalPoints) == totalPoint);
                    if (possibleDuplicateReceipt.Any())
                    {
                        error += "<li>Can not insert duplicate receipt</li>";
                        isError = true;
                    }
                }
                else
                {
                    var possibleDuplicateReceipt = Receipt.GetByMemberId(model.MemberID).Where(x => x.ReceiptCode.Trim().ToLower() == model.ReceiptCode.Trim().ToLower());
                    if (possibleDuplicateReceipt.Any())
                    {
                        error += "<li>Can not insert duplicate receipt</li>";
                        isError = true;
                    }
                }
            }
            #endregion

            if (string.IsNullOrEmpty(model.Channel))
            {
                error += "<li>Channel are Mandatory</li>";
                isError = true;
            }

            if (!new int?[] { null, 0 }.Contains(model.CampaignID))
            {
                var capCampaign = Campaign.GetById((int)model.CampaignID).CapGenerationPoint;


                List<string> ListActionName = new List<string>();
                DateTime transDate = DateTime.ParseExact(model.StrTransactionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Data.Action dataAction = null;
                int collTotal = 0;
                var collIsExist = Collect.GetAll().Where(x => x.MemberID == model.MemberID && x.CampaignID == model.CampaignID).ToList();
                List<int> collPoint = new List<int>();
                if (collIsExist.Count() > 0)
                {
                    foreach (var data in collIsExist)
                    {
                        var point = BalanceLog.GetByMemberId(data.MemberID).Where(x => x.ReferenceID == data.ID).FirstOrDefault().Points;
                        collPoint.Add(point);
                    }
                    collTotal = collPoint.Sum();
                }
                foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                {
                    var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == detail.ActionID).FirstOrDefault();
                    var TotalPoints = new List<Data.Collect>();
                    Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(detail.ActionID.Value);
                    CampaignActionDetail currCampaign = new CampaignActionDetail();
                    if (model.CampaignID.HasValue)
                        currCampaign = CampaignActionDetail.GetAll()
                            .Where(x => x.ActionID == currAction.ID)
                            .Where(x => x.CampaignID == model.CampaignID)
                            .FirstOrDefault();
                    else
                        currCampaign = null;

                    if (currCampaign != null && !string.IsNullOrEmpty(model.StrTransactionDate))
                    {
                        int actionPoint = currAction.Points;
                        int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
                        int quantityApproved = (detail.QuantityApproved != 0) ? detail.QuantityApproved : detail.Quantity;
                        int pointApproved = currentPoint * quantityApproved;


                        int _TotalPoint = pointApproved;
                        var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
                        bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
                        bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
                        int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;
                        bool PECENTAGE = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_PERCENTAGE : false;
                        bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_FIXEDNUMBER : false;

                        Data.Collect collect = new Data.Collect();

                        if (FIXEDNUMBER)
                        {


                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            collTotal += Total;

                            if (collTotal > capCampaign)
                            {
                                error += String.Format("<li>Cap Generation Point is {0}. </li> <ul>", capCampaign);
                                break;
                            }

                        }

                        else if (PECENTAGE)
                        {

                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            collTotal += Total;

                            if (collTotal > capCampaign)
                            {
                                error += String.Format("<li>Cap Generation Point is {0}. </li> <ul>", capCampaign);
                                break;
                            }

                        }
                    }
                }

                int colTotal = 0;
                var colIsExist = Collect.GetAll().Where(x => x.MemberID == model.MemberID && x.CampaignID == model.CampaignID).ToList();
                List<int> colPoint = new List<int>();
                if (colIsExist.Count() > 0)
                {
                    foreach (var data in colIsExist)
                    {
                        var points = BalanceLog.GetByMemberId(data.MemberID).Where(x => x.ReferenceID == data.ID).FirstOrDefault().Points;
                        colPoint.Add(points);
                    }
                    colTotal = colPoint.Sum();
                }
                foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                {
                    var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == detail.ActionID).FirstOrDefault();
                    var TotalPoints = new List<Data.Collect>();
                    Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(detail.ActionID.Value);
                    CampaignActionDetail currCampaign = new CampaignActionDetail();
                    if (model.CampaignID.HasValue)
                        currCampaign = CampaignActionDetail.GetAll()
                            .Where(x => x.ActionID == currAction.ID)
                            .Where(x => x.CampaignID == model.CampaignID)
                            .FirstOrDefault();
                    else
                        currCampaign = null;

                    if (currCampaign != null && !string.IsNullOrEmpty(model.StrTransactionDate))
                    {
                        int actionPoint = currAction.Points;
                        int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
                        int quantityApproved = (detail.QuantityApproved != 0) ? detail.QuantityApproved : detail.Quantity;
                        int pointApproved = currentPoint * quantityApproved;


                        int _TotalPoint = pointApproved;
                        var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
                        bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
                        bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
                        int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;
                        bool PECENTAGE = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_PERCENTAGE : false;
                        bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_FIXEDNUMBER : false;

                        Data.Collect collect = new Data.Collect();

                        if (FIXEDNUMBER)
                        {

                            collect.TotalPoints = _TotalPoint + _PointCamp;//model.TotalPoints;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colTotal += Total;

                            if (colTotal > capCampaign)
                            {

                                error += String.Format("<li>Please change Quantity for Action {0}.</li>", ActionName);

                            }
                        }

                        else if (PECENTAGE)
                        {
                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colTotal += Total;

                            if (colTotal > capCampaign)
                            {
                                error += String.Format("<li>Please change Quantity for Action {0}.</li>", ActionName);

                            }
                        }
                    }
                }


                int colleTotal = 0;
                var colleIsExist = Collect.GetAll().Where(x => x.MemberID == model.MemberID && x.CampaignID == model.CampaignID).ToList();
                List<int> collePoint = new List<int>();
                if (colleIsExist.Count() > 0)
                {
                    foreach (var data in colIsExist)
                    {
                        var points = BalanceLog.GetByMemberId(data.MemberID).Where(x => x.ReferenceID == data.ID).FirstOrDefault().Points;
                        collePoint.Add(points);
                    }
                    colleTotal = collePoint.Sum();
                }
                foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                {
                    var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == detail.ActionID).FirstOrDefault();
                    var TotalPoints = new List<Data.Collect>();
                    Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(detail.ActionID.Value);
                    CampaignActionDetail currCampaign = new CampaignActionDetail();
                    if (model.CampaignID.HasValue)
                        currCampaign = CampaignActionDetail.GetAll()
                            .Where(x => x.ActionID == currAction.ID)
                            .Where(x => x.CampaignID == model.CampaignID)
                            .FirstOrDefault();
                    else
                        currCampaign = null;

                    if (currCampaign != null && !string.IsNullOrEmpty(model.StrTransactionDate))
                    {
                        int actionPoint = currAction.Points;
                        int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
                        int quantityApproved = (detail.QuantityApproved != 0) ? detail.QuantityApproved : detail.Quantity;
                        int pointApproved = currentPoint * quantityApproved;


                        int _TotalPoint = pointApproved;
                        var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
                        bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
                        bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
                        int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;
                        bool PECENTAGE = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_PERCENTAGE : false;
                        bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_FIXEDNUMBER : false;

                        Data.Collect collect = new Data.Collect();

                        if (FIXEDNUMBER)
                        {

                            collect.TotalPoints = _TotalPoint + _PointCamp;//model.TotalPoints;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colleTotal += Total;

                            if (colleTotal > capCampaign)
                            {

                                error += "</ul>";
                                isError = true;
                                break;

                            }
                        }

                        else if (PECENTAGE)
                        {
                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colleTotal += Total;

                            if (colleTotal > capCampaign)
                            {
                                error += "</ul>";
                                isError = true;
                                break;
                            }
                        }
                    }
                }

                //if (colTotal > capCampaign)
                //{
                //    error += "</ul>";
                //    isError = true;
                //}

            }

            error += "</ul>";
            if (!isError) error = "";
            return error;
        }

        [BreadCrumb(Label = "Moderate Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Moderate(Guid ReceiptID)
        {
            UploadReceiptCMSModel model = new UploadReceiptCMSModel(ReceiptID);
            if (
                model.Status == (int)Receipt.ReceiptStatus.OcrProcessing
                || model.Status == (int)Receipt.ReceiptStatus.OcrApproved
                )
                return RedirectToAction(MVC.Receipt.Detail(ReceiptID));

            GetListRetailer();
            GetListCampaign();
            GetListAction();
            ViewBag.ChannelList = EnumHelper.ToSelectList<Channel>();
            if (this.IsSpgRole(out spgRoleCode))
            {
                if (string.IsNullOrEmpty(model.Channel))
                {
                    model.Channel = Convert.ToString(Channel.ELINA);
                }
            }
            if (string.IsNullOrEmpty(model.Channel))
            {
                model.Channel = Convert.ToString(Channel.Website);
            }
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Moderate()
        {
            bool isSuccess = false, isSpg = false;
            string message = string.Empty, error = string.Empty;
            isSpg = this.IsSpgRole(out spgRoleCode);

            try
            {
                UploadReceiptCMSModel model = GenerateReceiptObjectFromRequest(out error);

                if (string.IsNullOrEmpty(error))
                {
                    error = ValidateModerateReceipt(model);
                }
                if (string.IsNullOrEmpty(error))
                {
                    //string createdby = CurrentUser.FullName;
                    //var code = Number.Generated(Number.Type.RCP, createdby);
                    //var listImage = Image.ToList();
                    //Guid newID = Guid.NewGuid();
                    var result = ReceiptLogic.ModerateFromCMS(model, CurrentPool.ID, CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID, isSpg);
                    if (result.StatusCode == "00")
                    {
                        //transScope.Complete();
                        //transScope.Dispose();
                        isSuccess = true;
                        message = result.StatusMessage;
                    }
                    else
                    {
                        // transScope.Dispose();
                        message = result.StatusMessage;
                    }
                }
                else
                {
                    message = error;
                }
                return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message + ", inner exception : " + ex.InnerException, IsNeedRetry = true }, JsonRequestBehavior.AllowGet);
            }
            //if (ModelState.IsValid)
            //{
            //    UploadReceiptCMSModel model = GenerateReceiptObjectFromRequest(out error);

            //    try
            //    {
            //        var result = ReceiptLogic.ModerateFromCMS(model, CurrentPool.ID, CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID, isSpg);
            //        if (result.StatusCode == "00")
            //        {

            //            isSuccess = true;
            //            message = result.StatusMessage;
            //        }
            //        else
            //        {

            //            message = result.StatusMessage;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        isSuccess = false;
            //        message = string.Format("Internal server error, Please refresh and try again. Detail Error: {0}", ex.Message);

            //    }

            //}
            //return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
        }

        private string ValidateModerateReceipt(UploadReceiptCMSModel model)
        {
            bool isError = false;
            string error = "";
            if (model.TotalPrice <= 0)
            {
                error += "Total price can not be 0 or below.";
            }

            if (!new int?[] { null, 0 }.Contains(model.CampaignID))
            {
                var capCampaign = Campaign.GetById((int)model.CampaignID).CapGenerationPoint;


                List<string> ListActionName = new List<string>();
                DateTime transDate = DateTime.ParseExact(model.StrTransactionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Data.Action dataAction = null;
                int collTotal = 0;
                var collIsExist = Collect.GetAll().Where(x => x.MemberID == model.MemberID && x.CampaignID == model.CampaignID).ToList();
                List<int> collPoint = new List<int>();
                if (collIsExist.Count() > 0)
                {
                    foreach (var data in collIsExist)
                    {
                        var balanceLogData = BalanceLog.GetByMemberId(data.MemberID).Where(x => x.ReferenceID == data.ID).FirstOrDefault();
                        int point = 0;
                        if (balanceLogData != null)
                        {
                            point = balanceLogData.Points;
                        }
                        else
                        {
                            point = data.Points;
                        }
                        collPoint.Add(point);
                    }
                    collTotal = collPoint.Sum();
                }
                foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                {
                    var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == detail.ActionID).FirstOrDefault();
                    var TotalPoints = new List<Data.Collect>();
                    Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(detail.ActionID.Value);
                    CampaignActionDetail currCampaign = new CampaignActionDetail();
                    if (model.CampaignID.HasValue)
                        currCampaign = CampaignActionDetail.GetAll()
                            .Where(x => x.ActionID == currAction.ID)
                            .Where(x => x.CampaignID == model.CampaignID)
                            .FirstOrDefault();
                    else
                        currCampaign = null;

                    if (currCampaign != null && !string.IsNullOrEmpty(model.StrTransactionDate))
                    {
                        int actionPoint = currAction.Points;
                        int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
                        int quantityApproved = (detail.QuantityApproved != 0) ? detail.QuantityApproved : detail.Quantity;
                        int pointApproved = currentPoint * quantityApproved;


                        int _TotalPoint = pointApproved;
                        var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
                        bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
                        bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
                        int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;
                        bool PECENTAGE = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_PERCENTAGE : false;
                        bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_FIXEDNUMBER : false;

                        Data.Collect collect = new Data.Collect();

                        if (FIXEDNUMBER)
                        {


                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            collTotal += Total;

                            if (collTotal > capCampaign)
                            {
                                error += String.Format("Cap Generation Point is {0}.", capCampaign);
                                break;
                            }

                        }

                        else if (PECENTAGE)
                        {

                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            collTotal += Total;

                            if (collTotal > capCampaign)
                            {
                                error += String.Format("Cap Generation Point is {0}.", capCampaign);
                                break;
                            }

                        }
                    }
                }

                int colTotal = 0;
                var colIsExist = Collect.GetAll().Where(x => x.MemberID == model.MemberID && x.CampaignID == model.CampaignID).ToList();
                List<int> colPoint = new List<int>();
                if (colIsExist.Count() > 0)
                {
                    foreach (var data in colIsExist)
                    {
                        var balanceLogData = BalanceLog.GetByMemberId(data.MemberID).Where(x => x.ReferenceID == data.ID).FirstOrDefault();
                        int point = 0;
                        if (balanceLogData != null)
                        {
                            point = balanceLogData.Points;
                        }
                        else
                        {
                            point = data.Points;
                        }
                        collPoint.Add(point);
                    }
                    colTotal = colPoint.Sum();
                }
                foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                {
                    var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == detail.ActionID).FirstOrDefault();
                    var TotalPoints = new List<Data.Collect>();
                    Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(detail.ActionID.Value);
                    CampaignActionDetail currCampaign = new CampaignActionDetail();
                    if (model.CampaignID.HasValue)
                        currCampaign = CampaignActionDetail.GetAll()
                            .Where(x => x.ActionID == currAction.ID)
                            .Where(x => x.CampaignID == model.CampaignID)
                            .FirstOrDefault();
                    else
                        currCampaign = null;

                    if (currCampaign != null && !string.IsNullOrEmpty(model.StrTransactionDate))
                    {
                        int actionPoint = currAction.Points;
                        int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
                        int quantityApproved = (detail.QuantityApproved != 0) ? detail.QuantityApproved : detail.Quantity;
                        int pointApproved = currentPoint * quantityApproved;


                        int _TotalPoint = pointApproved;
                        var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
                        bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
                        bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
                        int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;
                        bool PECENTAGE = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_PERCENTAGE : false;
                        bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_FIXEDNUMBER : false;

                        Data.Collect collect = new Data.Collect();

                        if (FIXEDNUMBER)
                        {

                            collect.TotalPoints = _TotalPoint + _PointCamp;//model.TotalPoints;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colTotal += Total;

                            if (colTotal > capCampaign)
                            {

                                error += String.Format(" Please change Quantity for Action {0}.", ActionName);

                            }
                        }

                        else if (PECENTAGE)
                        {
                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colTotal += Total;

                            if (colTotal > capCampaign)
                            {
                                error += String.Format(" Please change Quantity for Action {0}.", ActionName);

                            }
                        }
                    }
                }


                int colleTotal = 0;
                var colleIsExist = Collect.GetAll().Where(x => x.MemberID == model.MemberID && x.CampaignID == model.CampaignID).ToList();
                List<int> collePoint = new List<int>();
                if (colIsExist.Count() > 0)
                {
                    foreach (var data in colIsExist)
                    {
                        var balanceLogData = BalanceLog.GetByMemberId(data.MemberID).Where(x => x.ReferenceID == data.ID).FirstOrDefault();
                        int point = 0;
                        if (balanceLogData != null)
                        {
                            point = balanceLogData.Points;
                        }
                        else
                        {
                            point = data.Points;
                        }
                        collPoint.Add(point);
                    }
                    colleTotal = colPoint.Sum();
                }
                foreach (UploadReceiptDetailCMSModel detail in model.ReceiptDetails)
                {
                    var CampaignActionActive = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == model.CampaignID && x.ActionID == detail.ActionID).FirstOrDefault();
                    var TotalPoints = new List<Data.Collect>();
                    Data.Action currAction = dataAction != null ? dataAction : Data.Action.GetById(detail.ActionID.Value);
                    CampaignActionDetail currCampaign = new CampaignActionDetail();
                    if (model.CampaignID.HasValue)
                        currCampaign = CampaignActionDetail.GetAll()
                            .Where(x => x.ActionID == currAction.ID)
                            .Where(x => x.CampaignID == model.CampaignID)
                            .FirstOrDefault();
                    else
                        currCampaign = null;

                    if (currCampaign != null && !string.IsNullOrEmpty(model.StrTransactionDate))
                    {
                        int actionPoint = currAction.Points;
                        int currentPoint = ActionLogic.GetPointByAction(currAction).Points;
                        int quantityApproved = (detail.QuantityApproved != 0) ? detail.QuantityApproved : detail.Quantity;
                        int pointApproved = currentPoint * quantityApproved;


                        int _TotalPoint = pointApproved;
                        var GET_RANGEDATE = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID && x.StartDate <= transDate && x.EndDate >= transDate).FirstOrDefault() : null;
                        bool BOOL_PERCENTAGE = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "PERCENTAGE" : false;
                        bool BOOL_FIXEDNUMBER = GET_RANGEDATE != null ? GET_RANGEDATE.Type == "FIXEDNUMBER" : false;
                        int _PointCamp = currCampaign != null ? Campaign.GetAll().Where(x => x.ID == currCampaign.CampaignID).FirstOrDefault().Value : 0;
                        bool PECENTAGE = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_PERCENTAGE : false;
                        bool FIXEDNUMBER = currCampaign != null && CampaignActionActive != null && detail.QuantityApproved != 0 ? BOOL_FIXEDNUMBER : false;

                        Data.Collect collect = new Data.Collect();

                        if (FIXEDNUMBER)
                        {

                            collect.TotalPoints = _TotalPoint + _PointCamp;//model.TotalPoints;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colleTotal += Total;

                            if (colleTotal > capCampaign)
                            {

                                error += "";
                                isError = true;
                                break;

                            }
                        }

                        else if (PECENTAGE)
                        {
                            var total = _TotalPoint * ((decimal)_PointCamp / 100);
                            collect.TotalPoints = (int)total;
                            TotalPoints.Add(new Collect() { TotalPoints = collect.TotalPoints });
                            var ActionName = Data.Action.GetById((int)detail.ActionID).Name;
                            //ListActionName.Add(ActionName);
                            var Total = TotalPoints.Sum(x => x.TotalPoints);
                            colleTotal += Total;

                            if (colleTotal > capCampaign)
                            {
                                error += "";
                                isError = true;
                                break;
                            }
                        }
                        if (detail.ItemPrice == null || detail.ItemPrice < 1)
                        {
                            error += String.Format("Please fill item price.");
                            isError = true;
                        }
                    }
                }

                //if (colTotal > capCampaign)
                //{
                //    error += "</ul>";
                //    isError = true;
                //}

            }
            error += "";
            if (!isError) error = "";
            return error;
        }

        [BreadCrumb(Label = "Detail Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Detail(Guid ReceiptID)
        {
            string spgRoleCode;
            if (this.IsSpgRole(out spgRoleCode)) return RedirectToAction(MVC.Error.Index());

            UploadReceiptCMSModel model = new UploadReceiptCMSModel(ReceiptID);
            if (
                model.Status != (int)Receipt.ReceiptStatus.Approve &&
                model.Status != (int)Receipt.ReceiptStatus.Reject &&
                model.Status != (int)Receipt.ReceiptStatus.WaitingUnverify &&
                model.Status != (int)Receipt.ReceiptStatus.OcrApproved &&
                model.Status != (int)Receipt.ReceiptStatus.OcrProcessing &&
                (model.Status == (int)Receipt.ReceiptStatus.OcrReject && model.RejectCount >= 3)
                )
                return RedirectToAction(MVC.Receipt.Index());
            GetListRetailer();
            GetListCampaign();
            GetListAction();
            return View(model);
        }

        [BreadCrumb(Label = "Edit Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(Guid ReceiptID)
        {
            string spgRoleCode;
            string GetOtherChainValues = System.Web.Configuration.WebConfigurationManager.AppSettings["OtherChainValues"];
            if (this.IsSpgRole(out spgRoleCode)) return RedirectToAction(MVC.Error.Index());

            UploadReceiptCMSModel model = new UploadReceiptCMSModel(ReceiptID);
            model.ChainValue = model.ChainValue ?? string.Empty;

            var hist = HistoryEditChain.GetByReceiptId(ReceiptID);

            if ( !(model.Status == (int)Receipt.ReceiptStatus.OcrApproved 
                    && GetOtherChainValues.Contains(model.ChainValue) 
                    && !string.IsNullOrEmpty(model.ChainValue)
                    && hist == null
                   )
                )
                return RedirectToAction(MVC.Receipt.Index());

            GetListRetailer();
            GetListCampaign();
            GetListAction();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit()
        {
            bool isSuccess = false, isSpg = false;
            string message = string.Empty, error = string.Empty;
            isSpg = this.IsSpgRole(out spgRoleCode);

            try
            {
                UploadReceiptCMSModel model = GenerateReceiptObjectFromRequest(out error);

                var result = ReceiptLogic.UpdateReceiptFromCMS(model, CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.StatusCode == "00")
                {
                    isSuccess = true;
                    message = result.StatusMessage;
                }
                else
                {
                    message = result.StatusMessage;
                }

                return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { IsSuccess = false, Message = ex.Message + ", inner exception : " + ex.InnerException, IsNeedRetry = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Detil()
        {
            bool isSuccess = false, isSpg = false;
            string message = string.Empty, error = string.Empty;
            isSpg = this.IsSpgRole(out spgRoleCode);
            if (ModelState.IsValid)
            {
                UploadReceiptCMSModel model = GenerateReceiptObjectFromRequest(out error);

                try
                {
                    var result = ReceiptLogic.ModerateFromCMS(model, CurrentPool.ID, CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID, isSpg);
                    if (result.StatusCode == "00")
                    {

                        isSuccess = true;
                        message = result.StatusMessage;
                    }
                    else
                    {

                        message = result.StatusMessage;
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    message = string.Format("Internal server error, Please refresh and try again. Detail Error: {0}", ex.Message);

                }

            }
            return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
        }

        [BreadCrumb(Label = "Detil Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Detil(Guid ReceiptID)
        {
            UploadReceiptCMSModel model = new UploadReceiptCMSModel(ReceiptID);
            GetListRetailer();
            GetListCampaign();
            GetListAction();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Reject(RejectReceiptModel model)
        {
            bool isSuccess = false;
            string message = string.Empty;
            // UploadReceiptCMSModel modelx = GenerateReceiptObjectFromRequest(out error);
            //if (string.IsNullOrEmpty(Request.Form["channelList"]))
            //{
            //    message = "Channel Harus Diisi";
            //}
            //else
            //{
            var result = ReceiptLogic.RejectFromCMS(model, CurrentPool.ID, CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            if (result.StatusCode == "00")
            {
                //transScope.Complete();
                //transScope.Dispose();
                isSuccess = true;
                message = result.StatusMessage;
            }
            else
            {
                // transScope.Dispose();
                message = result.StatusMessage;
            }
            //}
            return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
        }

        [BreadCrumb(Clear = true, Label = "Mass Upload")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult UploadFromExel(string Status)
        {
            if (!string.IsNullOrEmpty(Status))
            {
                ModelState.AddModelError(Status, Status);
            }
            List<UploadReceiptFromExcelResultCMSModel> listReceipt = new List<UploadReceiptFromExcelResultCMSModel>();
            return View(listReceipt);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult UploadFromExel(HttpPostedFileBase excelFile)
        {

            string currentUserName = CurrentUser.FullName;
            long CurrentUserID = CurrentUser.ID;
            var rawDataFromExel = PopulateFromExcelRawDataToModel(excelFile);
            List<UploadReceiptFromExcelResultCMSModel> listReceiptResultModel = rawDataFromExelToUploadReceiptFromExcelResultCMSModel(rawDataFromExel);
            var listReceipt = PopulateFromExcelToListUploadReceiptCMSModelHaveTransactionCode(listReceiptResultModel);

            var reciptWithOutCode = PopulateFromExcelToListUploadReceiptCMSModelDontHaveTransactionCode(listReceiptResultModel);
            if (reciptWithOutCode != null && reciptWithOutCode.Count > 0)
            {
                listReceipt.AddRange(reciptWithOutCode);
            }


            if (listReceipt != null)
            {
                var dataToPRocess = listReceipt.Where(x => x.ReceiptDetails != null && x.ReceiptDetails.Count > 0
                    && x.MemberID > 0 && x.RetailerID > 0 && !x.IsError).ToList();


                foreach (UploadReceiptCMSModel receipt in dataToPRocess)
                {
                    var code = Number.Generated(Number.Type.RCP, currentUserName);
                    Guid newID = Guid.NewGuid();
                    #region Value in Param
                    var Act = Data.Action.GetByCode(receipt.ProductCode);

                    var Camp = Campaign.GetAll().Where(x => x.Channels == receipt.Channel && x.CampaignActionDetail.Any(i => i.ActionID == Act.ID)).FirstOrDefault();

                    if (Camp != null)
                    {
                        receipt.CampaignID = Camp.ID;
                    }
                    receipt.RetailerAddress = string.Empty;
                    #endregion
                    var result = ReceiptLogic.InsertFromCMS(receipt, newID, null, CurrentPool.ID, currentUserName, Request.Url.AbsoluteUri, CurrentUserID, code, false, true);

                    ResultModel<UploadReceiptCMSModel> res = new ResultModel<UploadReceiptCMSModel>();
                    res.Value = receipt;
                    res.StatusCode = result.StatusCode;
                    res.StatusMessage = result.StatusMessage;

                    listReceiptResultModel = TranslateResultInsertReceiptFromExcel(res, listReceiptResultModel, receipt, newID);
                }
            }

            listReceiptResultModel = FinalizeMEssageError(listReceiptResultModel);
            return View(listReceiptResultModel);
        }

        private List<UploadReceiptFromExcelResultCMSModel> FinalizeMEssageError(List<UploadReceiptFromExcelResultCMSModel> param)
        {
            var itemTemp = param;
            param.Where(x => string.IsNullOrEmpty(x.result)).ToList().
                    ForEach(x =>
                    {
                        x.result = "failed";
                        x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " please validate all row in this Receipt";

                    });


            return param;
        }

        private List<UploadReceiptFromExcelResultCMSModel> rawDataFromExelToUploadReceiptFromExcelResultCMSModel(List<UploadReceipFromExcelCMSModel> rawDataFromExel)
        {
            List<UploadReceiptFromExcelResultCMSModel> result = new List<UploadReceiptFromExcelResultCMSModel>();

            foreach (var item in rawDataFromExel)
            {

                UploadReceiptFromExcelResultCMSModel itemToShow = new UploadReceiptFromExcelResultCMSModel()
                {
                    MemberPhone = item.MemberPhone,
                    TransactionCode = item.TransactionCode,
                    RetailerCode = item.RetailerCode,
                    TransactionDate = item.TransactionDate,
                    TransactionTime = item.TransactionTime,
                    ProductCode = item.ProductCode,
                    Quantity = item.Quantity,
                    RetailerAddress = item.RetailerAddress,
                    TotalPrice = item.TotalPrice
                };
                var Ch = DataMember.Channel.GetByCode(item.Channel);
                if (Ch != null)
                {
                    itemToShow.Channel = Ch.Code;
                }
                else
                {
                    itemToShow.Channel = "mass upload";
                }
                var memberData = DataMember.Member.GetByPhone(item.MemberPhone);

                if (memberData != null)
                {
                    itemToShow.MemberID = memberData.ID;
                    itemToShow.MemberName = memberData.FirstName + "" + memberData.LastName;
                }
                else
                {
                    itemToShow.result = "failed";
                    itemToShow.message = "Member Phone is not found in system";
                }


                Retailer currRetailer = Retailer.GetByCode(item.RetailerCode);
                if (currRetailer != null)
                {
                    itemToShow.RetailerCode = currRetailer.Code;
                    itemToShow.RetailerID = currRetailer.ID;
                }
                else
                {
                    itemToShow.result = "failed";
                    itemToShow.message = "RetailerCode is not found in system";
                }

                /* Mass Upload - Retailer Address harusnya tidak mandatory By Rendhi */
                //if (string.IsNullOrEmpty(item.RetailerAddress))
                //{
                //    itemToShow.result = "failed";
                //    itemToShow.message = "RetailerAddress is required";
                //}




                if (!string.IsNullOrEmpty(item.TransactionDate))
                {
                    if (!string.IsNullOrEmpty(item.TransactionTime))
                    {
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(item.TransactionDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            string datetime = item.TransactionDate + " " + item.TransactionTime;
                            if (DateTime.TryParseExact(datetime, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            {
                                // transactionDate = dt;
                            }
                            else
                            {
                                itemToShow.result = "failed";
                                itemToShow.message = string.Format("Format time is not correct, please using format HH:mm:ss");

                            }
                        }
                        else
                        {


                            itemToShow.result = "failed";
                            itemToShow.message = string.Format("Format Date is not correct, please using format dd/MM/yyyy");

                        }
                    }
                    else
                    {

                        itemToShow.result = "failed";
                        itemToShow.message = "TransactionTime is required";
                    }
                }
                else
                {

                    itemToShow.result = "failed";
                    itemToShow.message = "TransactionDate is required";
                }

                result.Add(itemToShow);

            }


            return result;
        }

        /// <summary>
        ///  Convert RawData from Excel to Object UploadReceiptCMSModel And Result Model where data have ReceiptCode
        /// </summary>
        /// <returns></returns>
        private List<UploadReceiptCMSModel> PopulateFromExcelToListUploadReceiptCMSModelHaveTransactionCode(List<UploadReceiptFromExcelResultCMSModel> listReceiptResultModel)
        {

            List<UploadReceiptCMSModel> listReceipt = new List<UploadReceiptCMSModel>();
            List<string> listReceiptCode = listReceiptResultModel.Where(x => !string.IsNullOrEmpty(x.TransactionCode)).Select(x => x.TransactionCode).Distinct().ToList();
            if (listReceiptCode.Count() > 0)
            {
                foreach (string receiptCode in listReceiptCode)
                {

                    List<UploadReceiptDetailCMSModel> receiptDetails = new List<UploadReceiptDetailCMSModel>();
                    var rawReceipt = listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList();

                    if (rawReceipt.Count != null && rawReceipt.Count > 0)
                    {
                        UploadReceiptCMSModel receipt = new UploadReceiptCMSModel();
                        //generate Header
                        receipt.MemberID = rawReceipt.FirstOrDefault().MemberID;
                        receipt.ReceiptCode = rawReceipt.FirstOrDefault().TransactionCode;
                        receipt.MemberName = rawReceipt.FirstOrDefault().MemberName;
                        receipt.RetailerID = rawReceipt.FirstOrDefault().RetailerID;
                        receipt.RetailerCode = rawReceipt.FirstOrDefault().RetailerCode;
                        receipt.StrTransactionDate = rawReceipt.FirstOrDefault().TransactionDate;
                        receipt.StrTransactionTime = rawReceipt.FirstOrDefault().TransactionTime;
                        receipt.Channel = rawReceipt.FirstOrDefault().Channel;
                        receipt.ProductCode = rawReceipt.FirstOrDefault().ProductCode;
                        receipt.TotalPrice = rawReceipt.FirstOrDefault().TotalPrice;
                        if (rawReceipt.FirstOrDefault(x => x.result == "failed") != null)
                        {
                            receipt.IsError = true;
                        }

                        var checkDuplicateRetailer = rawReceipt.Where(x => !string.IsNullOrEmpty(x.RetailerCode)).Select(x => x.RetailerCode == null ? "" : x.RetailerCode.Trim()).Distinct();
                        if (checkDuplicateRetailer != null && checkDuplicateRetailer.Count() > 1)
                        {
                            listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList().
                                 ForEach(x =>
                                 {
                                     //  x.TransactionCode = receipt.ReceiptCode;
                                     x.result = "failed";

                                     x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " in one Transaction Code can only be 1 Retailer";
                                 });

                            //make sure tidak diprosess tahap selanjutnya

                            receipt.IsError = true;
                        }



                        var checkDuplicateMember = rawReceipt.Where(x => !string.IsNullOrEmpty(x.MemberPhone)).Select(x => x.MemberPhone.Trim()).Distinct();
                        if (checkDuplicateMember != null && checkDuplicateMember.Count() > 1)
                        {
                            listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList().
                                 ForEach(x =>
                                 {
                                     // x.TransactionCode = receipt.ReceiptCode;
                                     x.result = "failed";

                                     x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " in 1 Transaction Code can be used only by one member phone";
                                 });

                            //make sure tidak diprosess tahap selanjutnya

                            receipt.IsError = true;
                        }



                        var checkDuplicateDate = rawReceipt.Where(x => !string.IsNullOrEmpty(x.TransactionDate)).Select(x => x.TransactionDate.Trim()).Distinct();
                        if (checkDuplicateDate != null && checkDuplicateDate.Count() > 1)
                        {
                            listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList().
                                 ForEach(x =>
                                 {
                                     // x.TransactionCode = receipt.ReceiptCode;
                                     x.result = "failed";

                                     x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " in 1 Transaction Code can only be 1 Date";
                                 });

                            //make sure tidak diprosess tahap selanjutnya

                            receipt.IsError = true;
                        }



                        var checkDuplicateTime = rawReceipt.Where(x => !string.IsNullOrEmpty(x.TransactionTime)).Select(x => x.TransactionTime.Trim()).Distinct();
                        if (checkDuplicateTime != null && checkDuplicateTime.Count() > 1)
                        {
                            listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList().
                                 ForEach(x =>
                                 {
                                     //x.TransactionCode = receipt.ReceiptCode;
                                     x.result = "failed";

                                     x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " in 1 Transaction Code can only be in a same time (date,Hour,minutes,second)";
                                 });

                            //make sure tidak diprosess tahap selanjutnya

                            receipt.IsError = true;
                        }


                        var checkDuplicateProduct = rawReceipt.Where(x => !string.IsNullOrEmpty(x.ProductCode)).Select(x => x.ProductCode.Trim());
                        if (checkDuplicateProduct != null)
                        {
                            var query = checkDuplicateProduct.GroupBy(x => x)
                                 .Where(g => g.Count() > 1)
                                 .Select(y => new checkDuplicateModel { Key = y.Key, Count = y.Count() })
                                 .ToList();

                            if (query != null && query.FirstOrDefault(x => x.Count > 1) != null)
                            {
                                foreach (var itemDuplicated in query.Where(x => x.Count > 1))
                                {
                                    listReceiptResultModel.Where(x => x.TransactionCode == receiptCode
                                        && x.ProductCode == itemDuplicated.Key
                                        ).ToList().
                                   ForEach(x =>
                                   {
                                       //x.TransactionCode = receipt.ReceiptCode;
                                       x.result = "failed";

                                       x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + string.Format(" in 1 Transaction Code can only be 1 Product Code '{0}'", itemDuplicated.Key);
                                   });
                                }


                                //make sure tidak diprosess tahap selanjutnya

                                receipt.IsError = true;
                            }
                        }



                        var checkDuplicateAddress = rawReceipt.Where(x => !string.IsNullOrEmpty(x.RetailerCode)).Select(x => x.RetailerAddress == null ? "" : x.RetailerAddress.Trim()).Distinct();
                        if (checkDuplicateAddress != null && checkDuplicateAddress.Count() > 1)
                        {
                            listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList().
                                 ForEach(x =>
                                 {
                                     //  x.TransactionCode = receipt.ReceiptCode;
                                     x.result = "failed";

                                     x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " in one Transaction Code can only be 1 Retailer Address";
                                 });

                            //make sure tidak diprosess tahap selanjutnya

                            receipt.IsError = true;
                        }

                        var child = DataMember.Child.GetByParentId(receipt.MemberID).FirstOrDefault();
                        bool IsProductError = false;
                        if (child != null)
                        {
                            foreach (UploadReceipFromExcelCMSModel rawDetail in rawReceipt)
                            {
                                Data.Action currAction = Data.Action.GetByCode(rawDetail.ProductCode);
                                if (currAction != null)
                                {
                                    var actionPoint = ActionLogic.GetPointByAction(currAction);
                                    UploadReceiptDetailCMSModel detail = new UploadReceiptDetailCMSModel();
                                    detail.ActionID = currAction.ID;
                                    detail.Quantity = rawDetail.Quantity;
                                    detail.Points = actionPoint.Points;
                                    detail.QuantityApproved = rawDetail.Quantity;
                                    detail.TotalQuantityApprovedPoints = detail.QuantityApproved * detail.Points;
                                    detail.ChildID = child.ID;
                                    receiptDetails.Add(detail);
                                }
                                else
                                {
                                    string err = string.Format(" Product Code {0} not found in system", rawDetail.ProductCode);

                                    listReceiptResultModel.Where(x => x.TransactionCode == receiptCode &&
                                        x.ProductCode == rawDetail.ProductCode
                                        ).ToList().
                                         ForEach(x =>
                                         {
                                             if (x.message == null || !x.message.Contains(err))
                                             {

                                                 x.result = "failed";

                                                 x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + err;
                                             }

                                         });

                                    IsProductError = true;
                                }
                            }
                        }
                        else
                        {
                            string err = "Saat ini kondisi Ibu belum memiliki anak";

                            listReceiptResultModel.Where(x => x.TransactionCode == receiptCode).ToList().
                                        ForEach(x =>
                                        {
                                            if (x.message == null || !x.message.Contains(err))
                                            {

                                                x.result = "failed";

                                                x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + err;
                                            }

                                        });
                            IsProductError = true;
                        }

                        if (!IsProductError)
                        {
                            receipt.ReceiptDetails = receiptDetails;
                        }
                        listReceipt.Add(receipt);

                    }

                }
            }


            return listReceipt;
        }

        private List<UploadReceiptCMSModel> PopulateFromExcelToListUploadReceiptCMSModelDontHaveTransactionCode(List<UploadReceiptFromExcelResultCMSModel> listReceiptResultModel)
        {

            List<UploadReceiptCMSModel> listReceipt = new List<UploadReceiptCMSModel>();
            //List<string> listReceiptCode = listReceiptResultModel.Where(x => !string.IsNullOrEmpty(x.TransactionCode)).Select(x => x.TransactionCode).Distinct().ToList();


            var dataNotHaveTransationCode = listReceiptResultModel.Where(x => string.IsNullOrEmpty(x.TransactionCode) && x.MemberID > 0 && x.RetailerID > 0).ToList();

            if (dataNotHaveTransationCode != null && dataNotHaveTransationCode.Count > 0)
            {
                var group = (from t in dataNotHaveTransationCode
                             group t by new { t.MemberID, t.RetailerID, t.TransactionDate, t.TransactionTime }
                                 into grp
                             select new
                             {
                                 grp.Key.MemberID,
                                 grp.Key.RetailerID,
                                 grp.Key.TransactionDate,
                                 grp.Key.TransactionTime,

                             }).Distinct().ToList();

                if (group != null && group.Count() > 0)
                {
                    foreach (var itemUnique in group)
                    {

                        List<UploadReceiptDetailCMSModel> receiptDetails = new List<UploadReceiptDetailCMSModel>();
                        var rawReceipt = dataNotHaveTransationCode.Where(x =>
                            x.MemberID == itemUnique.MemberID &&
                            x.RetailerID == itemUnique.RetailerID &&
                            x.TransactionDate == itemUnique.TransactionDate &&
                            x.TransactionTime == itemUnique.TransactionTime
                            ).ToList();


                        if (rawReceipt.Count != null && rawReceipt.Count > 0)
                        {
                            UploadReceiptCMSModel receipt = new UploadReceiptCMSModel();
                            //generate Header
                            receipt.MemberID = rawReceipt.FirstOrDefault().MemberID;
                            receipt.ReceiptCode = null;
                            receipt.MemberName = rawReceipt.FirstOrDefault().MemberName;
                            receipt.RetailerID = rawReceipt.FirstOrDefault().RetailerID;
                            receipt.RetailerCode = rawReceipt.FirstOrDefault().RetailerCode;
                            receipt.StrTransactionDate = rawReceipt.FirstOrDefault().TransactionDate;
                            receipt.StrTransactionTime = rawReceipt.FirstOrDefault().TransactionTime;
                            receipt.Channel = rawReceipt.FirstOrDefault().Channel;
                            receipt.RetailerAddress = rawReceipt.FirstOrDefault().RetailerAddress;
                            receipt.ProductCode = rawReceipt.FirstOrDefault().ProductCode;


                            if (rawReceipt.FirstOrDefault(x => x.result == "failed") != null)
                            {
                                receipt.IsError = true;
                            }

                            //changes nambah adress

                            var checkDuplicateAddress = rawReceipt.Where(x => !string.IsNullOrEmpty(x.RetailerAddress)).Select(x => x.RetailerAddress.Trim()).Distinct();
                            if (checkDuplicateAddress != null && checkDuplicateAddress.Count() > 1)
                            {
                                listReceiptResultModel.Where(x =>
                                    string.IsNullOrEmpty(x.TransactionCode) &&
                                        x.MemberID == itemUnique.MemberID &&
                            x.RetailerID == itemUnique.RetailerID &&
                            x.TransactionDate == itemUnique.TransactionDate &&
                            x.TransactionTime == itemUnique.TransactionTime
                                    ).ToList().
                                     ForEach(x =>
                                     {
                                         //  x.TransactionCode = receipt.ReceiptCode;
                                         x.result = "failed";

                                         x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + " in one Transaction Code can only be 1 Retailer Address";
                                     });

                                //make sure tidak diprosess tahap selanjutnya

                                receipt.IsError = true;
                            }



                            var checkDuplicateProduct = rawReceipt.Where(x => !string.IsNullOrEmpty(x.ProductCode)).Select(x => x.ProductCode.Trim());
                            if (checkDuplicateProduct != null)
                            {

                                var query = checkDuplicateProduct.GroupBy(x => x)
                                     .Where(g => g.Count() > 1)
                                     .Select(y => new checkDuplicateModel { Key = y.Key, Count = y.Count() })
                                     .ToList();
                                if (query != null)
                                {
                                    if (query != null && query.FirstOrDefault(x => x.Count > 1) != null)
                                    {

                                        foreach (var itemDuplicated in query.Where(x => x.Count > 1))
                                        {
                                            listReceiptResultModel.Where(x =>
                                                    string.IsNullOrEmpty(x.TransactionCode) &&
                                                          x.MemberID == receipt.MemberID &&
                                                            x.RetailerID == receipt.RetailerID &&
                                                            x.TransactionDate == receipt.StrTransactionDate &&
                                                            x.TransactionTime == receipt.StrTransactionTime
                                                           && x.ProductCode == itemDuplicated.Key
                                                    ).ToList().
                                                        ForEach(x =>
                                                        {
                                                            x.result = "failed";
                                                            x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + string.Format(" in 1 Receipt can only be 1 row for Product with same Code '{0}'", itemDuplicated.Key);
                                                        });
                                        }

                                        //make sure tidak diprosess tahap selanjutnya

                                        receipt.IsError = true;

                                    }
                                }
                            }








                            bool IsProductError = false;
                            foreach (UploadReceipFromExcelCMSModel rawDetail in rawReceipt)
                            {
                                Data.Action currAction = Data.Action.GetByCode(rawDetail.ProductCode);
                                if (currAction != null)
                                {
                                    var actionPoint = ActionLogic.GetPointByAction(currAction);
                                    UploadReceiptDetailCMSModel detail = new UploadReceiptDetailCMSModel();
                                    detail.ActionID = currAction.ID;
                                    detail.Quantity = rawDetail.Quantity;
                                    detail.Points = actionPoint.Points;
                                    detail.QuantityApproved = rawDetail.Quantity;
                                    detail.TotalQuantityApprovedPoints = detail.QuantityApproved * detail.Points;
                                    receiptDetails.Add(detail);
                                }
                                else
                                {
                                    string err = string.Format(" Product Code {0} not found in system", rawDetail.ProductCode);

                                    var emang = listReceiptResultModel.Where(x =>
                                         string.IsNullOrEmpty(x.TransactionCode) &&
                                               x.MemberID == receipt.MemberID &&
                                                 x.RetailerID == receipt.RetailerID &&
                                                 x.TransactionDate == receipt.StrTransactionDate &&
                                                 x.TransactionTime == receipt.StrTransactionTime &&
                                                 x.ProductCode == rawDetail.ProductCode

                                         ).ToList();


                                    listReceiptResultModel.Where(x =>
                                         string.IsNullOrEmpty(x.TransactionCode) &&
                                               x.MemberID == receipt.MemberID &&
                                                 x.RetailerID == receipt.RetailerID &&
                                                 x.TransactionDate == receipt.StrTransactionDate &&
                                                 x.TransactionTime == receipt.StrTransactionTime &&
                                                 x.ProductCode == rawDetail.ProductCode

                                         ).ToList().
                                           ForEach(x =>
                                           {


                                               if (x.message == null)
                                               {
                                                   x.message = "";
                                               }
                                               if (!x.message.Contains(err))
                                               {

                                                   x.result = "failed";

                                                   x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + err;
                                               }

                                           });

                                    IsProductError = true;

                                }
                            }


                            if (!IsProductError)
                            {
                                receipt.ReceiptDetails = receiptDetails;
                            }
                            listReceipt.Add(receipt);

                        }

                    }
                }
            }



            return listReceipt;
        }

        private List<UploadReceipFromExcelCMSModel> PopulateFromExcelRawDataToModel(HttpPostedFileBase excelFile)
        {
            List<UploadReceipFromExcelCMSModel> rawDataFromExel = new List<UploadReceipFromExcelCMSModel>();
            ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
            List<string> headers = listData.Headers;
            List<List<string>> datas = listData.DataRows.Where(x => !string.IsNullOrEmpty(string.Join("", x.ToArray()).Trim())).ToList();

            UploadReceipFromExcelCMSModel excelModel = new UploadReceipFromExcelCMSModel();
            List<string> listHeader = null;
            Dictionary<string, int> HeaderDict = null;

            listHeader = ExcelHelper.ObjectToList(excelModel);
            if (listHeader != null && headers != null)
                HeaderDict = ExcelHelper.ModeltoDictionary(listHeader, headers);

            foreach (List<string> data in datas)
            {
                ExcelHelper.CheckData(data, headers);
                UploadReceipFromExcelCMSModel newData = null;
                List<string> rawdata = data;
                newData = ConvertRawDatatoObject(rawdata, HeaderDict);
                if (newData != null)
                {
                    rawDataFromExel.Add(newData);
                }
            }
            return rawDataFromExel;
        }

        private List<UploadReceiptFromExcelResultCMSModel> TranslateResultInsertReceiptFromExcel(ResultModel<UploadReceiptCMSModel> result, List<UploadReceiptFromExcelResultCMSModel> listReceiptResultModel, UploadReceiptCMSModel receipt, Guid ReceiptID)
        {
            if (string.IsNullOrEmpty(receipt.ReceiptCode))
            {
                listReceiptResultModel.Where(x =>
                    string.IsNullOrEmpty(x.TransactionCode) &&
                          x.MemberID == receipt.MemberID &&
                            x.RetailerID == receipt.RetailerID &&
                            x.TransactionDate == receipt.StrTransactionDate &&
                            x.TransactionTime == receipt.StrTransactionTime

                    ).ToList().
                      ForEach(x =>
                      {
                          x.ReceiptID = ReceiptID;
                          if (result.StatusCode == "00")
                          {
                              x.result = "Success";
                          }
                          else if (result.StatusCode == "failed")
                          {
                              x.result = "failed";
                              x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + result.StatusMessage;
                          }
                          else
                          {
                              x.result = "Need Moderate";
                              x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + result.StatusMessage;
                          }

                      });


            }
            else
            {
                listReceiptResultModel.Where(x => x.TransactionCode == receipt.ReceiptCode).ToList().
                     ForEach(x =>
                     {
                         x.ReceiptID = ReceiptID;
                         if (result.StatusCode == "00")
                         {
                             x.result = "Success";
                         }
                         else if (result.StatusCode == "failed")
                         {
                             x.result = "failed";
                             x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + result.StatusMessage;
                         }
                         else
                         {
                             x.result = "Need Moderate";
                             x.message += (!string.IsNullOrEmpty(x.message) ? "<br/><br/>" : "") + result.StatusMessage;
                         }

                     });
            }

            return listReceiptResultModel;
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/ReceiptExcelFormat.xlsx"));
            string fileName = "ReceiptExcelFormat.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        private UploadReceipFromExcelCMSModel ConvertRawDatatoObject(List<string> rawdata, Dictionary<string, int> dataKey)
        {
            UploadReceipFromExcelCMSModel model = new UploadReceipFromExcelCMSModel();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(model))
            {
                if (descriptor != null && descriptor.Name != null)
                {
                    if (dataKey.ContainsKey(descriptor.Name))
                    {
                        int index = dataKey[descriptor.Name];
                        switch (descriptor.PropertyType.Name)
                        {
                            case "Int32":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt32(rawdata[index]) : 0);
                                break;
                            case "Int64":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt64(rawdata[index]) : 0);
                                break;
                            case "Decimal":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToDecimal(rawdata[index]) : 0);
                                break;
                            default: //string
                                descriptor.SetValue(model, rawdata[index]);
                                break;

                        }
                    }
                }
            }
            return model;
        }

        private UploadReceiptCMSModel GenerateReceiptObjectFromRequest(out string error)
        {
            bool isError = true;
            error = "<ul>";

            UploadReceiptCMSModel model = new UploadReceiptCMSModel();
            List<UploadReceiptDetailCMSModel> listdetail = new List<UploadReceiptDetailCMSModel>();
            int totalAction = Convert.ToInt32(Request.Form["TotalAction"]);
            if (!string.IsNullOrWhiteSpace(Request.Form["ID"]))
            {
                model.ID = new Guid(Request.Form["ID"]);
            }
            model.MemberID = Convert.ToInt32(Request.Form["MemberID"]);
            model.MemberName = Request.Form["MemberName"];
            model.ReceiptCode = Request.Form["ReceiptCode"];
            model.Code = Guid.NewGuid().ToString();//sementara pake GUID dlu
            model.RetailerID = Convert.ToInt32(Request.Form["RetailerID"]);
            model.StrTransactionDate = Request.Form["StrTransactionDate"];
            model.StrTransactionTime = Request.Form["StrTransactionTime"];
            model.Channel = string.IsNullOrEmpty(Request.Form["hdnChannel"]) ? Request.Form["Channel"] : Request.Form["hdnChannel"];
            model.StrRequestDate = Request.Form["StrRequestDate"];
            string campaign = Request.Form["CampaignID"];
            model.CampaignID = campaign == "" ? null : (int?)Convert.ToInt32(campaign);
            model.RetailerAddress = Request.Form["RetailerAddress"];
            model.RetailerPostCode = Request.Form["RetailerPostCode"];
            if (Request.Form["EventCode"] != "")
            {
                model.EventCode = Request.Form["EventCode"];
            }
            //model.Image = Request.Form["Image"];
            //Generate Receipt Detail

            var child = DataMember.Child.GetByParentId(model.MemberID).OrderBy(x => x.Birthdate).Take(1).Select(x => x.ID).FirstOrDefault();

            for (int i = 1; i <= totalAction; i++)
            {
                int temp = 0;

                UploadReceiptDetailCMSModel detail = new UploadReceiptDetailCMSModel();
                if (!string.IsNullOrWhiteSpace(Request.Form["Quantity-" + i]))
                {
                    if (int.TryParse(Request.Form["Quantity-" + i], out temp))
                    {
                        detail.Quantity = temp;
                        temp = 0;
                    }
                    else
                    {
                        isError = false;
                        error += string.Format("<li>Wrong Quantity Format Input, Must Be Numeric</li>");
                        detail.Quantity = temp;
                        temp = 0;
                    }
                }


                if (!string.IsNullOrWhiteSpace(Request.Form["QuantityApproved-" + i]))
                {
                    if (int.TryParse(Request.Form["QuantityApproved-" + i], out temp))
                    {
                        detail.QuantityApproved = temp;
                        temp = 0;
                    }
                    else
                    {
                        isError = false;
                        error += string.Format("<li>Wrong QuantityApproved Format Input, Must Be Numeric</li>");
                        detail.QuantityApproved = temp;
                        temp = 0;
                    }
                }


                if (!string.IsNullOrWhiteSpace(Request.Form["ItemPrice-" + i]))
                {
                    if (int.TryParse(Request.Form["ItemPrice-" + i], out temp))
                    {
                        if (temp <= 0)
                        {
                            isError = false;
                            error += string.Format("<li>Wrong Item Price is Required</li>");
                            detail.ItemPrice = temp;
                            temp = 0;
                        }
                        else
                        {
                            detail.ItemPrice = temp;
                            temp = 0;
                        }
                    }
                    else
                    {
                        isError = false;
                        error += string.Format("<li>Wrong Item Price Format Input, Must Be Numeric</li>");
                        detail.ItemPrice = temp;
                        temp = 0;
                    }
                }

                if (!string.IsNullOrWhiteSpace(Request.Form["TotalQuantityApprovedPoints-" + i]))
                {
                    if (int.TryParse(Request.Form["TotalQuantityApprovedPoints-" + i], out temp))
                    {
                        detail.TotalQuantityApprovedPoints = temp;
                        temp = 0;
                    }
                    else
                    {
                        isError = false;
                        error += string.Format("<li>Wrong Total Quantity Approved Points Format Input, Must Be Numeric</li>");
                        detail.TotalQuantityApprovedPoints = temp;
                        temp = 0;
                    }
                }

                detail.ActionID = !string.IsNullOrWhiteSpace(Request.Form["ActionID-" + i]) ?
                                                    Convert.ToInt32(Request.Form["ActionID-" + i]) : 0;
                //detail.ChildID = !string.IsNullOrWhiteSpace(Request.Form["ChildID-" + i]) ?
                //                                    Convert.ToInt32(Request.Form["ChildID-" + i]) : (int?)null;
                detail.ChildID = !string.IsNullOrWhiteSpace(Request.Form["ChildID-" + i]) ?
                                                    Convert.ToInt32(Request.Form["ChildID-" + i]) : child;
                //detail.Quantity = !string.IsNullOrWhiteSpace(Request.Form["Quantity-" + i]) ?
                //                                    Convert.ToInt32(Request.Form["Quantity-" + i]) : 0;
                detail.Points = !string.IsNullOrWhiteSpace(Request.Form["Points-" + i]) ?
                                                    Convert.ToInt32(Request.Form["Points-" + i]) : 0;
                detail.TotalPoints = !string.IsNullOrWhiteSpace(Request.Form["TotalPoints-" + i]) ?
                                                    Convert.ToInt32(Request.Form["TotalPoints-" + i]) : 0;
                //detail.QuantityApproved = !string.IsNullOrWhiteSpace(Request.Form["QuantityApproved-" + i]) ?
                //                                    Convert.ToInt32(Request.Form["QuantityApproved-" + i]) : 0;
                //detail.TotalQuantityApprovedPoints = !string.IsNullOrWhiteSpace(Request.Form["TotalQuantityApprovedPoints-" + i]) ?
                //Convert.ToInt32(Request.Form["TotalQuantityApprovedPoints-" + i]) : 0;
                detail.Remarks = !string.IsNullOrWhiteSpace(Request.Form["Remarks-" + i]) ?
                                                    Request.Form["Remarks-" + i] : string.Empty;
                //detail.ItemPrice = !string.IsNullOrWhiteSpace(Request.Form["ItemPrice-" + i]) ?pr:1;
                if (detail.ActionID > 0)
                    listdetail.Add(detail);

            }
            model.ReceiptDetails = listdetail;
            error += "</ul>";
            if (isError) error = "";
            return model;
        }

        [HttpGet]
        public virtual ActionResult GetCampaignValueById(int ID)
        {
            int point = 0;
            var campAction = CampaignActionDetail.GetAllByCampaignID(ID);
            if (campAction != null)
                point = Campaign.GetById(ID).Value;
            return Json(new { value = point }, JsonRequestBehavior.AllowGet);
        }

        private void GetListAction()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var CatGroup = Data.ActionCategory.GetByCode(Data.ActionCategory.CategoryType.PRODUCT.ToString());
            if (CatGroup != null)
            {
                IQueryable<LoyaltyPointEngine.Data.Action> listdata = LoyaltyPointEngine.Data.Action.GetByCategoryID(CatGroup.ID);
                foreach (LoyaltyPointEngine.Data.Action data in listdata)
                {

                    items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
                }
            }

            ViewBag.ListAction = items;
        }
        private void GetListCampaign()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Campaign> listdata = Campaign.GetAll().Where(x => x.IsActive == true).OrderByDescending(y => y.CreatedDate);
            foreach (Campaign data in listdata)
            {
                string label = string.Format("{0} (no campaign period)", data.Name);
                if (data.StartDate != null && data.EndDate != null)
                {
                    label = string.Format("{0} ({1} sd {2})", data.Name, data.StartDate.Value.ToString("dd/MM/yyy"), data.EndDate.Value.ToString("dd/MM/yyy"));
                }
                items.Add(new SelectListItem { Text = label, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListCampaign = items;
        }
        private void GetListRetailer()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Retailer> listdata = Retailer.GetAll();
            foreach (Retailer data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListRetailer = items;
        }
        private void GetListEventCode()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = EventCode.GetAll().Where(x => DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate);
            if (listdata != null)
            {
                foreach (EventCode data in listdata)
                {

                    items.Add(new SelectListItem { Text = data.Code, Value = data.ID.ToString(), Selected = false });
                }
            }

            ViewBag.ListEventCode = items;
        }

        #region List Receipt Grid
        //[ChildActionOnly]
        //public virtual ActionResult GridGetAllReceipt()
        //{
        //    gridMvcHelper = new GridMvcHelper();
        //    IOrderedQueryable<ReceiptHeaderCMSModel> allReceipt = GetAllReceiptModel();
        //    AjaxGrid<ReceiptHeaderCMSModel> grid = this.gridMvcHelper.GetAjaxGrid(allReceipt);
        //    return PartialView(grid);
        //}

        [HttpGet]
        public virtual ActionResult GridGetAllReceiptPager(string txtSearch, int? page, string memberIDHidden, string hiddenAwal, string hiddenAkhir, int? statusHidden, string channelHidden, string spgCodeHidden, int? eventCodeHidden, int? retailerHidden, string retailerHasHidden, string orderColumn = "CreatedDate", string orderDirection = "Desc")//Modify by Ucup
        {
            int tempMemberId = 0,
                totalRow = 0,
                limit = 5;

            List<SP_GetGridReceipt_Result> gridReceipt = new List<SP_GetGridReceipt_Result>();

            // Validation if click button search
            if (txtSearch != null)
            {
                if (string.IsNullOrEmpty(hiddenAwal) || string.IsNullOrEmpty(hiddenAkhir))
                {
                    ViewBag.ErrorValidation = "Tgl Awal & Tgl Akhir is required";
                }
            }

            int? paramMemberId = null,
                paramPage = page.GetValueOrDefault(1) < 1 ? 1 : page.GetValueOrDefault(1);

            if (int.TryParse(memberIDHidden, out tempMemberId))
                paramMemberId = tempMemberId;

            ViewBag.MemberId = memberIDHidden;
            ViewBag.TglAwal = hiddenAwal;
            ViewBag.TglAkhir = hiddenAkhir;
            ViewBag.Status = statusHidden;
            ViewBag.Channel = channelHidden;
            ViewBag.SPGCode = spgCodeHidden;
            ViewBag.EventCode = eventCodeHidden;
            ViewBag.Retailer = retailerHidden;
            ViewBag.RetailerAddr = retailerHasHidden;

            DateTime tempStartDate = DateTime.MinValue,
                tempEndDate = DateTime.MinValue;

            DateTime? startDate, endDate;

            if (
                string.IsNullOrEmpty(txtSearch) &&
                string.IsNullOrEmpty(memberIDHidden) &&
                string.IsNullOrEmpty(hiddenAwal) &&
                string.IsNullOrEmpty(hiddenAkhir) &&
                !statusHidden.HasValue &&
                string.IsNullOrEmpty(channelHidden) &&
                string.IsNullOrEmpty(spgCodeHidden) &&
                !eventCodeHidden.HasValue &&
                !retailerHidden.HasValue &&
                string.IsNullOrEmpty(retailerHasHidden)
                )
            {
                startDate = DateTime.Now.AddDays(-7);
                endDate = DateTime.Now;
                ViewBag.Search = txtSearch;
                ViewBag.CurrentPage = paramPage;
                ViewBag.TotalPage = (int)Math.Ceiling((double)totalRow / limit);
                ViewBag.OrderColumn = orderColumn ?? "CreatedDate";
                ViewBag.OrderDirection = orderDirection ?? "Desc";
                ViewBag.TotalRow = totalRow;
                return PartialView(MVC.Receipt.Views.GridGetAllReceipt, gridReceipt);
            }
            else
            {
                startDate = null;
                endDate = null;
            }

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempStartDate))
                    startDate = tempStartDate;
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempEndDate))
                    endDate = tempEndDate;
            }

            gridReceipt = GetAllReceiptModel(paramMemberId, channelHidden, spgCodeHidden, eventCodeHidden, statusHidden, startDate, endDate, paramPage, limit, orderColumn, orderDirection, txtSearch, retailerHidden, retailerHasHidden, out totalRow).ToList();

            ViewBag.Search = txtSearch;
            ViewBag.CurrentPage = paramPage;
            ViewBag.TotalPage = (int)Math.Ceiling((double)totalRow / limit);
            ViewBag.OrderColumn = orderColumn ?? "CreatedDate";
            ViewBag.OrderDirection = orderDirection ?? "Desc";
            ViewBag.TotalRow = totalRow;
            return PartialView(MVC.Receipt.Views.GridGetAllReceipt, gridReceipt);
        }

        private IQueryable<SP_GetGridReceipt_Result> GetAllReceiptModel(int? memberId, string channel, string spgCodeId, int? eventCode, int? status, DateTime? startDate, DateTime? endDate, int? page, int? pageSize, string orderColumn, string orderDirection, string search, int? retailerHidden, string retailerHasHidden, out int totalRow)
        {
            string spgCode = "";
            bool isSpg = this.IsSpgRole(out spgCode);

            if (isSpg)
            {
                totalRow = DataRepositoryFactory.CurrentRepository
                    .SP_GetGridReceiptTotalRow(CurrentUser.ID, memberId, channel, spgCodeId, status, startDate, endDate, page, pageSize, orderColumn, orderDirection, search, eventCode, retailerHidden, retailerHasHidden).FirstOrDefault().Value;
                IQueryable<SP_GetGridReceipt_Result> result = DataRepositoryFactory.CurrentRepository
                    .SP_GetGridReceipt(CurrentUser.ID, memberId, channel, spgCodeId, status, startDate, endDate, page, pageSize, orderColumn, orderDirection, search, eventCode, retailerHidden, retailerHasHidden)
                    .AsQueryable();

                return result;
            }
            else
            {
                totalRow = DataRepositoryFactory.CurrentRepository
                    .SP_GetGridReceiptTotalRow(null, memberId, channel, spgCodeId, status, startDate, endDate, page, pageSize, orderColumn, orderDirection, search, eventCode, retailerHidden, retailerHasHidden)
                    .FirstOrDefault().Value;
                IQueryable<SP_GetGridReceipt_Result> result = DataRepositoryFactory.CurrentRepository
                    .SP_GetGridReceipt(null, memberId, channel, spgCodeId, status, startDate, endDate, page, pageSize, orderColumn, orderDirection, search, eventCode, retailerHidden, retailerHasHidden)
                    .AsQueryable();

                return result;
            }
        }
        #endregion

        public class checkDuplicateModel
        {
            public string Key { get; set; }
            public int Count { get; set; }
        }

        [HttpPost]
        public virtual JsonResult CheckDuplicateReceiptCode(string ReceiptCode)
        {
            string output = "";
            try
            {
                //var duplicatereceipt = Receipt.GetByReceiptCode(ReceiptCode);
                if (Receipt.GetByReceiptCode(ReceiptCode).Count() > 0)
                {
                    output = string.Format("Receipt Code {0} already exist in database", ReceiptCode);
                }
            }
            catch { }

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        // add code by bryant
        private IQueryable<ReceiptHeaderCMSModel> GetAllReceiptModel(DateTime? from = null, DateTime? to = null)
        {

            IQueryable<Receipt> receipts = LoyaltyPointEngine.Data.Receipt.GetAll();
            IQueryable<ReceiptHeaderCMSModel> data;
            if (from.HasValue) receipts = receipts.Where(x => EntityFunctions.TruncateTime(from) <= EntityFunctions.TruncateTime(x.CreatedDate));
            if (to.HasValue) receipts = receipts.Where(x => EntityFunctions.TruncateTime(to) >= EntityFunctions.TruncateTime(x.CreatedDate));

            string s;
            bool isSpg = this.IsSpgRole(out s);
            if (isSpg)
            {
                data = from a in receipts
                       join b in Data.Member.GetAll() on a.MemberID equals b.ID
                       where a.CreatedByID == CurrentUser.ID
                       select new ReceiptHeaderCMSModel
                       {
                           ID = a.ID,
                           ReceiptCode = a.ReceiptCode,
                           Code = a.Code,
                           TransactionDateDate = a.TransactionDate,
                           //RetailerID = a.RetailerID,
                           //RetailerName = a.Retailer.Name,
                           CreatedDate = a.CreatedDate,
                           MemberName = b.Name,
                           MemberID = a.MemberID,
                           Status = a.Status,
                           Channel = a.Channel,
                           MemberEmail = b.Email,
                           MemberPhone = b.Phone
                       };
            }
            else
            {
                data = from a in receipts
                       join b in Data.Member.GetAll() on a.MemberID equals b.ID
                       select new ReceiptHeaderCMSModel
                       {
                           ID = a.ID,
                           ReceiptCode = a.ReceiptCode,
                           Code = a.Code,
                           TransactionDateDate = a.TransactionDate,
                           //RetailerID = a.RetailerID,
                           //RetailerName = a.Retailer.Name,
                           CreatedDate = a.CreatedDate,
                           MemberName = b.Name,
                           MemberID = a.MemberID,
                           Status = a.Status,
                           Channel = a.Channel,
                           MemberEmail = b.Email,
                           MemberPhone = b.Phone
                       };
            }


            return data.OrderByDescending(x => x.CreatedDate);
        }

        private List<List<string>> GetReceiptReporting(List<SP_GetGridReceipt_Result> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                data.Add(item.MemberName);
                data.Add(item.MemberPhone);
                data.Add(item.Code);
                data.Add(item.ReceiptCode);
                data.Add(item.ReceiptTransactionDate.HasValue ? item.ReceiptTransactionDate.Value.ToString("dd/MM/yyyy") : "");
                //data.Add(item.Product);
                //data.Add(item.Quantity.ToString());
                data.Add(item.RetailerName);
                string status = Enum.GetName(typeof(Receipt.ReceiptStatus), item.ReceiptStatus);
                data.Add(status);
                data.Add(item.ReceiptChannel);
                data.Add(item.CreatedDate.ToString("dd/MM/yyyy"));
                data.Add(item.EventCode);
                datas.Add(data);
            }
            return datas;
        }

        //private IOrderedQueryable<ReceiptHeaderCMSModel> GetAllReceiptModel()
        //{
        //    IQueryable<ReceiptHeaderCMSModel> data;
        //    bool isSpg = ViewBag.IsSpg != null ? (bool)ViewBag.IsSpg : false;

        //    if (isSpg)
        //    {
        //        data = from a in LoyaltyPointEngine.Data.Receipt.GetAll()
        //               join b in Data.Member.GetAll() on a.MemberID equals b.ID
        //               where a.CreatedByID == CurrentUser.ID
        //               select new ReceiptHeaderCMSModel
        //               {
        //                   ID = a.ID,
        //                   ReceiptCode = a.ReceiptCode,
        //                   Code = a.Code,
        //                   TransactionDateDate = a.TransactionDate,
        //                   RetailerID = a.RetailerID,
        //                   RetailerName = a.Retailer.Name,
        //                   CreatedDate = a.CreatedDate,
        //                   MemberName = b.Name,
        //                   MemberID = a.MemberID,
        //                   Status = a.Status,
        //                   Channel = a.Channel
        //               };
        //    }
        //    else
        //    {
        //        data = from a in LoyaltyPointEngine.Data.Receipt.GetAll()
        //               join b in Data.Member.GetAll() on a.MemberID equals b.ID
        //               select new ReceiptHeaderCMSModel
        //               {
        //                   ID = a.ID,
        //                   ReceiptCode = a.ReceiptCode,
        //                   Code = a.Code,
        //                   TransactionDateDate = a.TransactionDate,
        //                   RetailerID = a.RetailerID,
        //                   RetailerName = a.Retailer.Name,
        //                   CreatedDate = a.CreatedDate,
        //                   MemberName = b.Name,
        //                   MemberID = a.MemberID,
        //                   Status = a.Status,
        //                   Channel = a.Channel
        //               };
        //    }
        //    return data.OrderByDescending(x => x.CreatedDate);
        //}

        [HttpGet]
        public virtual JsonResult CheckMinimumPrice(int Price, int Quantity, int ActionID)
        {
            string message = string.Empty;
            bool IsBottomPrice = false;
            var dataAction = Data.Action.GetById(ActionID);
            if (dataAction != null)
            {
                if (Price < dataAction.MinimumPrice * Quantity)
                {
                    IsBottomPrice = true;
                    message = "Price is below minimum price.";
                }
            }
            return Json(new { IsBottomPrice = IsBottomPrice, Message = message }, JsonRequestBehavior.AllowGet);
        }


        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadJson(Guid ReceiptID)
        {
            var dataSnapcartReceipt = SnapcartReceipt.GetByReceiptIdTopOne(ReceiptID);
            var dataReceipt = Receipt.GetById(ReceiptID);
            string receiptcode = dataReceipt.Code;
            string dataLog = string.Empty;

            string json = JsonConvert.SerializeObject(new { Result = "No data Result" });

            if (!string.IsNullOrEmpty(dataSnapcartReceipt.Result))
            {
                JObject obj = JObject.Parse(dataSnapcartReceipt.Result);
                var jsonModel = obj.ToObject<LoyaltyPointEngine.Model.Parameter.Snapcart.SnapcartCallbackParam>();
                json = JsonConvert.SerializeObject(jsonModel);
            }
            dataLog = JsonConvert.SerializeObject(new { Param = ReceiptID, ReceiptCode = receiptcode, Result = dataSnapcartReceipt.Result });
            Log.Info("Data log = " + dataLog);
            string fileName = receiptcode + ".json";

            byte[] buffer;
            using (var memoryStream = new MemoryStream())
            {
                buffer = Encoding.Default.GetBytes(json);

                memoryStream.Write(buffer, 0, buffer.Length);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.AddHeader("Content-Length", memoryStream.Length.ToString());
                Response.ContentType = "application/json"; //This is MIME type
                memoryStream.WriteTo(Response.OutputStream);
            }
            Response.End();
            return File(buffer, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        class emptyDataModel
        {
            public string Result { get; set; }
        }
    }
}