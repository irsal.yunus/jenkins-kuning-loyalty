﻿using LoyaltyPointCMS.Controllers;
using LoyaltyPointEngine.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class WidgetController : BaseController
    {
        //
        // GET: /Widget/

        public virtual ActionResult TopNavigationBar()
        {
            ViewBag.CurrentUser = CurrentUser.FullName;
            return PartialView();
        }

        public virtual ActionResult Navigation()
        {
            List<CMSNavigation> navigations = new List<CMSNavigation>();
            navigations = SetCurrentNavigationCache();
            ViewBag.ListNavigation = navigations;
            return PartialView();
        }


    }
}
