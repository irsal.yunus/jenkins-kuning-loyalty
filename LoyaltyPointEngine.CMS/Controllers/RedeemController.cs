﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.Model.Parameter.Redeem;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System.Globalization;
using LoyaltyPointEngine.Model.Object.Redeem;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RedeemController : BaseController
    {
        // GET: Redeem
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Redeem")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Redeem successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Redeem successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Redeem successfully deleted";
                    break;
                case "Void_Success":
                    ViewBag.Success = "Redeem successfully voided";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Redeem")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            SubmitRedeemParameterCMSModel model = new SubmitRedeemParameterCMSModel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(SubmitRedeemParameterCMSModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime transactionDate = DateTime.MinValue;
                if (!string.IsNullOrEmpty(model.StrTransactionDate))
                {
                    if (DateTime.TryParseExact(model.StrTransactionDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out transactionDate))
                    {
                        model.TransactionDate = transactionDate;
                    }
                }
                else
                {
                    ModelState.AddModelError("Error", "Transaction Date is required");
                    return View(model);
                }

                var result = RedeemLogic.CreateFromCMS(model, CurrentPool.ID);
                if (result.StatusCode == "00")
                    return RedirectToAction(MVC.Redeem.Index("Create_Success"));
                else
                    ModelState.AddModelError("Error", result.StatusMessage);
            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Void)]
        public virtual ActionResult Void(string transactionCode)
        {
            bool isSuccess = false;
            string message = string.Empty;
            if (!string.IsNullOrEmpty(transactionCode))
            {
                Redeem redeemData = Redeem.GetByTransactionCode(transactionCode);
                if (redeemData != null)
                {
                    var res = VoidLogic.Void(transactionCode, redeemData.MemberID, redeemData.CurrentMember.Name);
                    if (res.StatusCode == "00")
                    {
                        isSuccess = true;
                    }
                    else
                    {
                        message = res.StatusMessage;
                    }
                }

            }
            return Json(new { Issuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
        }

        #region List Redeem Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllRedeem()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<RedeemCMSViewModel> allRedeem = GetAllRedeemModel();
            AjaxGrid<RedeemCMSViewModel> grid = this.gridMvcHelper.GetAjaxGrid(allRedeem);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllRedeemPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<RedeemCMSViewModel> allData = GetAllRedeemModel();
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData
                    .Where(x => x.MemberName.Contains(txtSearch) || x.MemberEmail.Contains(txtSearch) || x.TransactionCode.Contains(txtSearch))
                    .OrderByDescending(x => x.CreatedDate);
            }
            AjaxGrid<RedeemCMSViewModel> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Redeem.Views.GridGetAllRedeem, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private IOrderedQueryable<RedeemCMSViewModel> GetAllRedeemModel()
        {
            var data = from x in LoyaltyPointEngine.Data.Redeem.GetAll()
                       join member in LoyaltyPointEngine.Data.Member.GetAll() on x.MemberID equals member.ID
                       select new RedeemCMSViewModel
                       {
                           ID = x.ID,
                           TransactionCode = x.TransactionCode,
                           TransactionDateDate = x.TransactionDate,
                           Point = x.Point,
                           CreatedDate = x.CreatedDate,
                           MemberName = member.Name,
                           MemberEmail = member.Email,
                           IsVoid = x.IsVoid
                       };

            return data.OrderByDescending(x => x.CreatedDate);
        }
        #endregion
    }
}