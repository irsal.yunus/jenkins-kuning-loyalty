﻿using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Data;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omu.ValueInjecter;
using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CMSNavigationPrivilegeMappingController : BaseController
    {

        private IGridMvcHelper gridMvcHelper;
        // GET: CMSNavigationPrivilegeMapping
        [BreadCrumb(Label = "Manage Navigation Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Privilege successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Privilege successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Privilege successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Label = "Add Navigation Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            NavigationPrivilegeModel model = new NavigationPrivilegeModel();
            GetListNavigation();
            GetListPrivilege();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(NavigationPrivilegeModel model)
        {
            if (ModelState.IsValid)
            {
                CMSNavigationPrivilegeMapping nav = new CMSNavigationPrivilegeMapping();
                nav.InjectFrom(model);
                var result = nav.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.CMSNavigationPrivilegeMapping.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }

            GetListRole();
            return View(model);
        }

        private void GetListRole()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSRole> listRole = CMSRole.GetAll();
            foreach (CMSRole role in listRole)
            {

                items.Add(new SelectListItem { Text = role.Name, Value = role.ID.ToString(), Selected = false });
            }
            ViewBag.ListRole = items;
        }

        private void GetListNavigation()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSNavigation> listall = CMSNavigation.GetAll();
            foreach (CMSNavigation data in listall)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListNavigation = items;
        }

        private void GetListPrivilege()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSPrivilege> listdata = CMSPrivilege.GetAll();
            foreach (CMSPrivilege data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListPrivilege = items;
        }

        #region List Navigation Privilege Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllNavigationPrivilegeMapping()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSNavigationPrivilegeMapping> allData = CMSNavigationPrivilegeMapping.GetAll().OrderBy(x => x.CMSNavigation.Name);
            AjaxGrid<CMSNavigationPrivilegeMapping> grid = this.gridMvcHelper.GetAjaxGrid(allData);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllNavigationPrivilegeMappingPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSNavigationPrivilegeMapping> allData = CMSNavigationPrivilegeMapping.GetAll().OrderBy(x => x.CMSNavigation.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.CMSNavigation.Name.Contains(txtSearch) || x.CMSPrivilege.Name.Contains(txtSearch)).OrderBy(x => x.CMSNavigation.Name);
            }
            AjaxGrid<CMSNavigationPrivilegeMapping> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.CMSNavigationPrivilegeMapping.Views.GridGetAllNavigationPrivilegeMapping, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}