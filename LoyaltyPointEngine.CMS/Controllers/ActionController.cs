﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ActionController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Action")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Label = "Add Action")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ActionModel model = new ActionModel();
            GetListActionCategory("");
            PopulateListProduct(model.ProductId);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ActionModel model)
        {
            bool IsProduct = false;
            ValidateAddModel(model, out IsProduct);

            var dataProduct = IsProduct ? Data.Product.GetByID(model.ProductId.GetValueOrDefault()) : null;


            if (ModelState.IsValid)
            {

                LoyaltyPointEngine.Data.Action _Action = new LoyaltyPointEngine.Data.Action();
                if (IsProduct && dataProduct != null)
                {
                    model.Code = dataProduct.Code;
                    model.Name = dataProduct.Name ?? string.Empty;
                    _Action.ProductID = dataProduct.ID;
                    _Action.MinimumPrice = model.MinimumPrice;
                    _Action.ProductIdVendor = model.ProductIdVendor;
                }
                else
                {
                    _Action.ProductID = null;
                }
                _Action.InjectFrom(model);
                _Action.PoolID = CurrentPool.ID;
                _Action.CapGeneration = model.CapGeneration;



                var result = _Action.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Action.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListActionCategory(model.ActionCategoryID.ToString());
            PopulateListProduct(model.ProductId);
            return View(model);
        }

        private void ValidateAddModel(ActionModel model, out bool isProduct)
        {
            isProduct = false;
            if (model.ActionCategoryID <= 0)
            {
                ModelState.AddModelError("ActionCategoryID", "ActionCategory is required");
            }
            else
            {
                var dataCat = ActionCategory.GetById(model.ActionCategoryID);
                if (dataCat != null && dataCat.Code.ToLower() == "Product".ToLower())
                {
                    isProduct = true;
                }
            }

            if (!isProduct)
            {
                if (string.IsNullOrEmpty(model.Code))
                {
                    ModelState.AddModelError("Code", "Code is required");
                }
                else
                {
                    var data = Data.Action.GetByCode(model.Code);
                    if (data != null)
                    {
                        ModelState.AddModelError("Code", "Code is already registered in system");
                    }
                }


                if (string.IsNullOrEmpty(model.Name))
                {
                    ModelState.AddModelError("Name", "Name is required");
                }
            }
            else
            {
                if (isProduct)
                {
                    if (model.ProductId == null)
                    {
                        ModelState.AddModelError("ProductId", "Product is required");
                    }
                    else
                    {
                        var checkAction = Data.Action.GetByProductID(model.ProductId.GetValueOrDefault());
                        if (checkAction != null)
                        {
                            ModelState.AddModelError("ProductId", string.Format("Product '{0}' is already in Data Action", checkAction.Product.Code));
                        }
                    }
                }
            }




            if (model.IsHaveCapGeneration)
            {
                if (model.CapGeneration < model.Points)
                {
                    ModelState.AddModelError("CapGeneration", "CapGeneration value must more than point or equal");
                }
            }
        }

        [BreadCrumb(Label = "Edit Action")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int ActionId)
        {

            LoyaltyPointEngine.Data.Action currentAction = LoyaltyPointEngine.Data.Action.GetById(ActionId);
            ActionModel model = new ActionModel();
            model.InjectFrom(currentAction);
            model.CapGeneration = (currentAction.CapGeneration.HasValue) ? currentAction.CapGeneration.Value : 0;
            model.ProductId = currentAction.ProductID;
            model.MinimumPrice = currentAction.MinimumPrice.HasValue ? currentAction.MinimumPrice.Value : 0;
            GetListActionCategory(model.ActionCategoryID.ToString());
            PopulateListProduct(model.ProductId);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ActionModel model)
        {
            bool IsProduct = false;
            ValidateEditModel(model, out IsProduct);
            var _Action = Data.Action.GetById(model.ID);
            if (_Action == null)
            {
                ModelState.AddModelError("ID", "ID is not found");
            }

            var dataProduct = IsProduct ? Data.Product.GetByID(model.ProductId.GetValueOrDefault()) : null;

            if (ModelState.IsValid)
            {

                _Action.InjectFrom(model);
                _Action.PoolID = CurrentPool.ID;
                _Action.CapGeneration = model.CapGeneration;
                if (IsProduct && dataProduct != null)
                {
                    _Action.Code = dataProduct.Code;
                    _Action.Name = dataProduct.Name;
                    _Action.ProductID = dataProduct.ID;
                    _Action.MinimumPrice = model.MinimumPrice;
                    _Action.ProductIdVendor = model.ProductIdVendor;
                }
                else
                {
                    _Action.ProductID = null;
                }


                var result = _Action.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Action.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListActionCategory(model.ActionCategoryID.ToString());
            PopulateListProduct(model.ProductId);
            return View(model);
        }

        private void ValidateEditModel(ActionModel model, out bool isProduct)
        {
            isProduct = false;
            if (model.ActionCategoryID <= 0)
            {
                ModelState.AddModelError("ActionCategoryID", "ActionCategory is required");
            }
            else
            {
                var dataCat = ActionCategory.GetById(model.ActionCategoryID);
                if (dataCat != null && dataCat.Code.ToLower() == "Product".ToLower())
                {
                    isProduct = true;
                }
            }

            if (!isProduct)
            {
                if (string.IsNullOrEmpty(model.Code))
                {
                    ModelState.AddModelError("Code", "Code is required");
                }
                else
                {
                    var data = Data.Action.GetByCodeNotSelf(model.Code, model.ID);
                    if (data != null)
                    {
                        ModelState.AddModelError("Code", "Code is already registered in system");
                    }
                }

                if (string.IsNullOrEmpty(model.Name))
                {
                    ModelState.AddModelError("Name", "Name is required");
                }
            }
            else
            {
                if (isProduct)
                {
                    if (model.ProductId == null)
                    {
                        ModelState.AddModelError("ProductId", "Product is required");
                    }
                    else
                    {
                        var checkAction = Data.Action.GetByProductIDNotSelf(model.ProductId.GetValueOrDefault(), model.ID);
                        if (checkAction == null)
                        {
                            ModelState.AddModelError("ProductId", string.Format("Product '{0}' Not Found in Data Action", checkAction.Product.Code));
                        }
                    }
                }
            }

            if (model.IsHaveCapGeneration)
            {
                if (model.CapGeneration < model.Points)
                {
                    ModelState.AddModelError("CapGeneration", "CapGeneration value must more than point or equal");
                }
            }
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            LoyaltyPointEngine.Data.Action _Action = LoyaltyPointEngine.Data.Action.GetById(ID);
            _Action.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Action.Index("Delete_Success"));
        }

        private void GetListActionCategory(string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<ActionCategory> listdata = ActionCategory.GetAll().Where(x => x.IsActive);
            foreach (ActionCategory data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListActionCategory = items;
        }

        private void PopulateListProduct(int? productID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = Product.GetAll().Where(x => x.IsDisplay);
            foreach (var data in listdata)
            {
                string displayName = string.Format("({0}) Code: {1} - Name: {2} - Size: {3}  ", data.ProductGroup.ProductGroupCode, data.Code, data.Name, data.Size);
                items.Add(new SelectListItem { Text = displayName, Value = data.ID.ToString(), Selected = data.ID == productID });
            }
            ViewBag.ListProduct = items;
        }

        [HttpGet]
        public virtual ActionResult GetActionValueById(int ID)
        {
            var dataAction = Data.Action.GetById(ID);
            var result = ActionLogic.GetPointByAction(dataAction);
            return Json(new { value = result.Points, isCampaign = result.IsCampaign, campaignName = result.CampaignName, isPregnancyProduct = result.IsPregnancyProduct }, JsonRequestBehavior.AllowGet);
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllAction()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<LoyaltyPointEngine.Data.Action> allAction = LoyaltyPointEngine.Data.Action.GetAll().OrderBy(x => x.Name);
            AjaxGrid<LoyaltyPointEngine.Data.Action> grid = this.gridMvcHelper.GetAjaxGrid(allAction);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllActionPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<LoyaltyPointEngine.Data.Action> allData = LoyaltyPointEngine.Data.Action.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<LoyaltyPointEngine.Data.Action> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Action.Views.GridGetAllAction, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}