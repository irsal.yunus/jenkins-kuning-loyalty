﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Parameter.OTP;
using LoyaltyPointEngine.PointLogicLayer.OTP;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using LoyaltyPointEngine.Model.Object.OTP;
using LoyaltyPointEngine.Common.Helper;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ResendSMSController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Resend SMS")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]

        //
        // GET: /ResendSMS/
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "00":
                    ViewBag.Success = "OTP Code successfully sent";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "OTP Code successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "OTP successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();

        }

        [BreadCrumb(Clear = true, Label = "Get All Image Publish")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult ResendView(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Item successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Item successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Item successfully deleted";
                    break;
                case "Resend_Success":
                    ViewBag.Success = "Item successfully Resend";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllResendSMS()
        {

            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<PoolOTPMember> allResendSMS = PoolOTPMember.GetByNonPhoneAndNonSuccess().OrderBy(x => x.CreatedDate);
            AjaxGrid<PoolOTPMember> grid = this.gridMvcHelper.GetAjaxGrid(allResendSMS);
            return PartialView(grid);

        }

        [HttpGet]
        public virtual ActionResult GridGetAllResendSMSPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<PoolOTPMember> allData = PoolOTPMember.GetByNonPhoneAndNonSuccess().OrderBy(x => x.CreatedDate);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.OTPCode.Contains(txtSearch) || x.Phone.Contains(txtSearch) || x.Status.Contains(txtSearch) &&
                        x.OTPExpiredDate > DateTime.Now).OrderBy(x => x.CreatedDate);
            }
            AjaxGrid<PoolOTPMember> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.ResendSMS.Views.GridGetAllResendSMS, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Resend(ResendSMSModel param)
        {
            ResultModel<LoyaltyPointEngine.Model.Object.OTP.OTPMemberModel> res = new ResultModel<LoyaltyPointEngine.Model.Object.OTP.OTPMemberModel>();

            try
            {
                var listId = Request.Form["listID[]"].Split(',');

                foreach (var item in listId)
                {
                    res = OTPLogic.OTPMemberRegister(item, Request.Url.ToString(), CurrentUser.ID, "Loyal CMS", true, CurrentUser.FullName);
                }

            }
            catch (Exception e)
            {
                res.StatusMessage = e.InnerException.Message;
            }

            return Json(new { IsSuccess = res.StatusCode, Message = res.StatusMessage }, JsonRequestBehavior.AllowGet);

        }

        [BreadCrumb(Clear = true, Label = "Upload Member")]
        public virtual ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Upload Member")]
        public virtual ActionResult Upload(HttpPostedFileBase excelFile)
        {
            List<MassResendModel> res = new List<MassResendModel>();

            string url = Request.Url.AbsoluteUri;
            List<string> ListError = new List<string>();
            List<ResendSMSModel> Listdata = new List<ResendSMSModel>();
            ResendSMSModel resModel = new ResendSMSModel();
            string resultFailed = string.Empty;
            if (excelFile != null)
            {
                ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
                if (listData != null)
                {
                    foreach (var item in listData.DataRows)
                    {
                        string phone = item[0].ToString();
                        if (phone.Trim().Length > 0)
                        {
                            if (phone[0] != '0')
                            {
                                phone = "0" + phone;
                            }
                            LoyaltyPointEngine.DataMember.Member member = LoyaltyPointEngine.DataMember.Member.GetByPhone(phone);
                            MassResendModel massResendModel = new MassResendModel();
                            if (member == null)
                            {
                                massResendModel.errCode = "100";
                                massResendModel.errMsg = "nomor tidak ada d sistem kita";
                                massResendModel.phone = phone;
                                res.Add(massResendModel);
                                continue;
                            }

                            if (member.IsActive && !member.IsDeleted)
                            {
                                massResendModel.errCode = "200";
                                massResendModel.errMsg = "nomor sudah aktif tidak perlu dikirim ulang";
                                massResendModel.phone = phone;
                                res.Add(massResendModel);
                                continue;
                            }
                            var result = OTPLogic.OTPMemberRegister(phone, Request.Url.ToString(), CurrentUser.ID, "Loyal CMS", true, CurrentUser.FullName);

                            if (result == null)
                            {
                                massResendModel.errCode = "500";
                                massResendModel.errMsg = "result null";
                                massResendModel.phone = phone;
                                res.Add(massResendModel);
                            }
                            else
                            {
                                massResendModel.errCode = result.StatusCode;
                                massResendModel.errMsg = result.StatusMessage;
                                massResendModel.phone = phone;
                                res.Add(massResendModel);
                            }
                        }
                    }
                }
            }

            return View(res);
        }

        private List<MassResendModel> PopulateUploadModelFromExcelRawDataToModel(HttpPostedFileBase excelFile)
        {
            List<MassResendModel> rawDataFromExel = new List<MassResendModel>();
            ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
            List<string> headers = listData.Headers;
            List<List<string>> datas = listData.DataRows;

            MassResendModel excelModel = new MassResendModel();
            List<string> listHeader = null;
            Dictionary<string, int> HeaderDict = null;

            listHeader = ExcelHelper.ObjectToList(excelModel);
            if (listHeader != null && headers != null)
                HeaderDict = ExcelHelper.ModeltoDictionary(listHeader, headers);


            foreach (List<string> data in datas)
            {
                ExcelHelper.CheckData(data, headers);
                MassResendModel newData = null;
                List<string> rawdata = data;
                newData = ConvertRawDatatoObject(rawdata, HeaderDict);
                if (newData != null)
                {
                    rawDataFromExel.Add(newData);
                }
            }


            return rawDataFromExel;

        }

        private MassResendModel ConvertRawDatatoObject(List<string> rawdata, Dictionary<string, int> dataKey)
        {
            MassResendModel model = new MassResendModel();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(model))
            {
                if (descriptor != null && descriptor.Name != null)
                {
                    if (dataKey.ContainsKey(descriptor.Name))
                    {
                        int index = dataKey[descriptor.Name];
                        switch (descriptor.PropertyType.Name)
                        {
                            case "Int32":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt32(rawdata[index]) : 0);
                                break;
                            case "Int64":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt64(rawdata[index]) : 0);
                                break;
                            case "Decimal":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToDecimal(rawdata[index]) : 0);
                                break;
                            default: //string
                                descriptor.SetValue(model, rawdata[index]);
                                break;

                        }
                    }
                }
            }
            return model;
        }

    }
}