﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Campaign;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using System.Globalization;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CampaignActionController : BaseController
    {

        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Label = "CampaignAction")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status, int? CampaignID)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "CampaignAction successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "CampaignAction successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "CampaignAction successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            if (CampaignID != null && CampaignID > 0)
                ViewBag.Campaign = Campaign.GetById(CampaignID.Value);
            return View();
        }

        [BreadCrumb(Label = "Add CampaignAction")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(int? CampaignID)
        {
            CampaignActionModel model = new CampaignActionModel();
            if (CampaignID.HasValue)
                model.CampaignID = CampaignID.Value;
            GetListAction();
            GetListCampaign();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(CampaignActionModel model)
        {
            if (ModelState.IsValid)
            {

                CampaignActionDetail _CampaignAction = new CampaignActionDetail();
                IQueryable<CampaignActionDetail> lastCampaignAction = CampaignActionDetail.GetAllActive()
                    .Where(x => x.CampaignID == model.CampaignID && x.ActionID == model.ActionID);
                if (lastCampaignAction != null && lastCampaignAction.Count() > 0)
                    ModelState.AddModelError("Error", "action already in database, please choose another action");
                else
                {
                    _CampaignAction.InjectFrom(model);
                    var result = _CampaignAction.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    if (result.Success)
                        return RedirectToAction(MVC.CampaignAction.Index("Create_Success", model.CampaignID));
                    else
                        ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
                }
            }
            GetListAction();
            GetListCampaign();
            return View(model);
        }

        [BreadCrumb(Label = "Edit CampaignAction")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int CampaignActionId, int? CampaignID)
        {
            CampaignActionDetail currentCampaignAction = LoyaltyPointEngine.Data.CampaignActionDetail.GetById(CampaignActionId);
            CampaignActionModel model = new CampaignActionModel(currentCampaignAction);
            if (CampaignID.HasValue)
                model.CampaignID = CampaignID.Value;
            GetListAction();
            GetListCampaign();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(CampaignActionModel model)
        {
            if (ModelState.IsValid)
            {
                CampaignActionDetail _CampaignAction = LoyaltyPointEngine.Data.CampaignActionDetail.GetById(model.ID);
                IQueryable<CampaignActionDetail> lastCampaignAction = CampaignActionDetail.GetAllActive()
                    .Where(x => x.CampaignID == model.CampaignID && x.ActionID == model.ActionID);
                if (_CampaignAction.ActionID != model.ActionID && lastCampaignAction != null && lastCampaignAction.Count() > 0)
                    ModelState.AddModelError("Error", "action already in database, please choose another action");
                else
                {
                    _CampaignAction.InjectFrom(model);
                    var result = _CampaignAction.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    if (result.Success)
                        return RedirectToAction(MVC.CampaignAction.Index("Edit_Success", model.CampaignID));
                    else
                        ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
                }
                
            }
            GetListAction();
            GetListCampaign();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            CampaignActionDetail CampaignAction = CampaignActionDetail.GetById(ID);
            CampaignAction.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.CampaignAction.Index("Delete_Success", null));
        }

        private void GetListAction()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoyaltyPointEngine.Data.Action> listdata = LoyaltyPointEngine.Data.Action.GetAll();
            foreach (LoyaltyPointEngine.Data.Action data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListAction = items;
        }
        private void GetListCampaign()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoyaltyPointEngine.Data.Campaign> listdata = LoyaltyPointEngine.Data.Campaign.GetAll();
            foreach (LoyaltyPointEngine.Data.Campaign data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListCampaign = items;
        }

        #region List CampaignAction Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllCampaignAction()
        {
            string cID = Request.QueryString["CampaignID"];
            int campaignID = 0;
            int.TryParse(cID, out campaignID);
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CampaignActionDetail> allCampaignAction = GetAllCampaignAction(campaignID);
            AjaxGrid<CampaignActionDetail> grid = this.gridMvcHelper.GetAjaxGrid(allCampaignAction);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllCampaignActionPager(string txtSearch, int? page)
        {
            string cID = Request.QueryString["CampaignID"];
            int campaignID = 0;
            int.TryParse(cID, out campaignID);
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CampaignActionDetail> allData = GetAllCampaignAction(campaignID);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Action.Name.Contains(txtSearch)).OrderBy(x => x.ID);
            }
            AjaxGrid<CampaignActionDetail> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.CampaignAction.Views.GridGetAllCampaignAction, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private IOrderedQueryable<CampaignActionDetail> GetAllCampaignAction(int? CampaignID)
        {
            IOrderedQueryable<CampaignActionDetail> allCampaignAction = null;
            if(CampaignID != null && CampaignID > 0)
                allCampaignAction = LoyaltyPointEngine.Data.CampaignActionDetail.GetAllByCampaignID(CampaignID.Value).OrderBy(x => x.ID);
            else
                allCampaignAction = LoyaltyPointEngine.Data.CampaignActionDetail.GetAll().OrderBy(x => x.ID);
            return allCampaignAction;
        }

        #endregion
    }
}
