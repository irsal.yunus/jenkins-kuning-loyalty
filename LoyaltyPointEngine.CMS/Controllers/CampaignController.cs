﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Model.Object.Campaign;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using System.Globalization;
using LoyaltyPointEngine.PointLogicLayer.CampaignLogic;
using System.Transactions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CampaignController : BaseController
    {
        // GET: Campaign
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Campaign")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Campaign successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Campaign successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Campaign successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Campaign")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            CampaignModel model = new CampaignModel();
            GetListCampaignType(model.Type);
            GetListAction(model.CampaignActionList);
            GetListCampaignAction(null, model.CampaignActionList);
            GetListOfChannel();
            GetListOfType();
            GetListOfOperator();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(CampaignModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {
                int transFailedCount = 0;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    Campaign Campaign = new Campaign();
                    Campaign.InjectFrom(model);
                    Campaign.Channels = String.Join(",", model.Channels);
                    if (!string.IsNullOrEmpty(model.StrStartDate) && !string.IsNullOrEmpty(model.StrStartTime))
                    {
                        string DateTimeStart = model.StrStartDate + " " + model.StrStartTime;
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(DateTimeStart, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            Campaign.StartDate = dt;
                        }
                    }
                    if (!string.IsNullOrEmpty(model.StrEndDate) && !string.IsNullOrEmpty(model.StrEndTime))
                    {
                        string DateTimeEnd = model.StrEndDate + " " + model.StrEndTime;
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(DateTimeEnd, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            Campaign.EndDate = dt;
                        }
                    }
                    var result = Campaign.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    if (result.Success)
                    {
                        if (model.CampaignActionList != null)
                        {
                            foreach (int actionID in model.CampaignActionList)
                            {
                                LoyaltyPointEngine.Data.Action action = LoyaltyPointEngine.Data.Action.GetById(actionID);
                                IQueryable<CampaignActionDetail> lastCampaignAction = CampaignActionDetail.GetAllActive().Where(x => x.CampaignID == Campaign.ID && x.ActionID == actionID);
                                if (lastCampaignAction != null && lastCampaignAction.Count() > 0)
                                {
                                    ModelState.AddModelError("Error", string.Format("action {0} already assigned, please choose another action", action.Name));
                                    transFailedCount++;
                                }
                                else
                                {
                                    CampaignActionDetail _CampaignAction = new CampaignActionDetail();
                                    _CampaignAction.ActionID = actionID;
                                    _CampaignAction.CampaignID = Campaign.ID;
                                    _CampaignAction.MaximalQty = 0;
                                    _CampaignAction.MinimalQty = 0;
                                    _CampaignAction.IsActive = true;
                                    _CampaignAction.IsDeleted = false;
                                    result = _CampaignAction.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        transFailedCount++;
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                    }
                                }
                            }
                        }
                        if (model.ActionRules != null)
                        {
                            foreach (var item in model.ActionRules)
                            {
                                IEnumerable<AutomaticCampaignActionRule> lastCampaignRule = AutomaticCampaignActionRule.GetByCampaignIDAndActionID(Campaign.ID, item.ActionID);
                                if (lastCampaignRule != null && lastCampaignRule.Count() > 0)
                                {
                                    ModelState.AddModelError("Error", string.Format("action {0} already assigned, please choose another action", item.ActionID));
                                    transFailedCount++;
                                }
                                else
                                {
                                    AutomaticCampaignActionRule _CampaignRule = new AutomaticCampaignActionRule();
                                    _CampaignRule.ActionID = item.ActionID;
                                    _CampaignRule.CampaignID = Campaign.ID;
                                    _CampaignRule.CreatedDate = DateTime.Now;
                                    _CampaignRule.CreatedBy = CurrentUser.FullName;
                                    _CampaignRule.Treshold = item.Treshold;
                                    _CampaignRule.Operator = item.Operator;
                                    _CampaignRule.Type = item.Type;
                                    _CampaignRule.IsDeleted = false;
                                    result = _CampaignRule.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        transFailedCount++;
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                    }
                                }
                            }
                        }
                        if (transFailedCount > 0)
                        {
                            transScope.Dispose();
                        }
                        else
                        {
                            transScope.Complete();
                            transScope.Dispose();
                            return RedirectToAction(MVC.Campaign.Index("Create_Success"));
                        }
                    }
                    else
                    {
                        transScope.Dispose();
                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                    }
                }

            }
            GetListCampaignType(model.Type);
            GetListAction(model.CampaignActionList);
            GetListCampaignAction(null, model.CampaignActionList);
            GetListOfChannel(model.Channels);
            GetListOfType();
            GetListOfOperator();
            return View(model);
        }


        private void ValidateAddModel(CampaignModel model)
        {

            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
            else
            {
                if (model.Code.Length > 50)
                {
                    ModelState.AddModelError("Code", "Code length can not be more than 50 characters");
                }
                var checkData = Campaign.GetByCode(model.Code);
                if (checkData != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }

            if (string.IsNullOrEmpty(model.Type))
            {
                ModelState.AddModelError("Type", "Type is required");
            }
            else
            {
                if (model.Type.ToLower() == "PERCENTAGE".ToLower())
                {
                    if (model.Value < 0)
                    {
                        ModelState.AddModelError("Value", "Value minimum 0 (zero)");
                    }
                    else if (model.Value < 100)
                    {
                        ModelState.AddModelError("Value", "Value minimum 100");
                    }
                }
            }


            DateTime startdate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrStartDate))
            {

                if (!DateTime.TryParseExact(model.StrStartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out startdate))
                {
                    ModelState.AddModelError("StrStartDate", "Format start Date is wrong");
                }
            }
            else
            {
                ModelState.AddModelError("StrStartDate", "start date is required");
            }

            DateTime enddate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrEndDate))
            {

                if (!DateTime.TryParseExact(model.StrEndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out enddate))
                {
                    ModelState.AddModelError("StrEndDate", "Format end Date is wrong");
                }
            }
            else
            {
                ModelState.AddModelError("StrEndDate", "end date is required");
            }

            if (!string.IsNullOrEmpty(model.StrStartDate) && !string.IsNullOrEmpty(model.StrEndDate))
            {
                if (startdate > DateTime.MinValue && enddate > DateTime.MinValue)
                {
                    if (enddate < startdate)
                    {
                        ModelState.AddModelError("StrEndDate", "end date must more or equal than start date");
                    }
                }
            }

            if (model.Channels == null || model.Channels.Count <= 0)
            {
                ModelState.AddModelError("Channels", "select at least 1 channel");
            }
        }

        [BreadCrumb(Label = "Edit Campaign")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int CampaignId)
        {
            Campaign currentCampaign = LoyaltyPointEngine.Data.Campaign.GetById(CampaignId);
            GetListCampaignType("");
            CampaignModel model = new CampaignModel(currentCampaign);
            GetListAction(model.CampaignActionList);
            GetListCampaignAction(CampaignId, model.CampaignActionList);
            List<int> campaignActionDetails = CampaignActionDetail.GetAllByCampaignID(CampaignId).Where(x => !x.IsDeleted && x.IsActive).Select(x => x.ActionID).ToList();
            if (campaignActionDetails != null)
            {
                List<int?> actionIdArray = new List<int?>();
                foreach (var actionID in campaignActionDetails)
                {
                    actionIdArray.Add(actionID);
                }
                model.CampaignActionList = actionIdArray.ToArray();
            }
            IEnumerable<AutomaticCampaignActionRule> campaignRules = AutomaticCampaignActionRule.GetByCampaignID(CampaignId).Where(x => !x.IsDeleted);
            if (campaignRules != null)
            {
                //ActionRule actionRuleList = new ActionRule();
                foreach (var item in model.ActionRules)
                {
                    Data.Action GetCampaignName = Data.Action.GetById(item.ActionID);
                    item.ActionName = GetCampaignName.Name;
                    //model.ActionRules = actionRuleList;
                };
            }

            model.Channels = string.IsNullOrEmpty(currentCampaign.Channels)
                ? new List<string>()
                : currentCampaign.Channels.Split(',').Select(x => x).ToList();
            GetListOfChannel(model.Channels);
            GetListOfType();
            GetListOfOperator();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(CampaignModel model)
        {
            ValidateEditModel(model);
            Campaign Campaign = LoyaltyPointEngine.Data.Campaign.GetById(model.ID);
            if (Campaign == null)
            {
                ModelState.AddModelError("ID", "ID not found in system");
            }
            if (ModelState.IsValid)
            {

                Campaign.InjectFrom(model);
                if (!string.IsNullOrEmpty(model.StrStartDate) && !string.IsNullOrEmpty(model.StrStartTime))
                {
                    string DateTimeStart = model.StrStartDate + " " + model.StrStartTime;
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(DateTimeStart, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        Campaign.StartDate = dt;
                    }
                }
                if (!string.IsNullOrEmpty(model.StrEndDate) && !string.IsNullOrEmpty(model.StrEndTime))
                {
                    string DateTimeEnd = model.StrEndDate + " " + model.StrEndTime;
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(DateTimeEnd, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        Campaign.EndDate = dt;
                    }
                }

                int transFailedCount = 0;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    Campaign.Channels = String.Join(",", model.Channels);
                    var result = Campaign.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    if (result.Success)
                    {
                        if (model.CampaignActionList != null)
                        {
                            var removedAction = CampaignActionDetail.GetAllByCampaignID(Campaign.ID).Where(x => model.CampaignActionList.Contains(x.ActionID) == false);
                            foreach (var action in removedAction)
                            {
                                if (action.IsActive)
                                {
                                    action.IsActive = false;
                                    result = action.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                        transFailedCount++;
                                    }
                                }
                            }

                            foreach (int actionID in model.CampaignActionList)
                            {
                                var action = CampaignActionDetail.GetAllByCampaignID(Campaign.ID).FirstOrDefault(x => x.ActionID == actionID);
                                if (action != null)
                                {
                                    action.IsActive = true;
                                    action.IsDeleted = false;
                                    result = action.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                        transFailedCount++;
                                    }
                                }
                                else
                                {
                                    CampaignActionDetail _CampaignAction = new CampaignActionDetail();
                                    _CampaignAction.ActionID = actionID;
                                    _CampaignAction.CampaignID = Campaign.ID;
                                    _CampaignAction.MaximalQty = 0;
                                    _CampaignAction.MinimalQty = 0;
                                    _CampaignAction.IsActive = true;
                                    _CampaignAction.IsDeleted = false;
                                    result = _CampaignAction.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        transFailedCount++;
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                    }
                                }
                            }
                        }
                        //Add by Ardhi
                        if (model.ActionRules != null)
                        {
                            var removedAction = AutomaticCampaignActionRule.GetByCampaignID(Campaign.ID).Where(x => !model.ActionRules.Any(y => y.ID == x.ID));
                            foreach (var action in removedAction)
                            {
                                if (action.IsDeleted == false)
                                {
                                    action.IsDeleted = true;
                                    result = action.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                        transFailedCount++;
                                    }
                                }
                            }

                            foreach (var item in model.ActionRules)
                            {
                                var action = AutomaticCampaignActionRule.GetByCampaignID(Campaign.ID).FirstOrDefault(x => x.ActionID == item.ActionID);
                                if (action != null)
                                {
                                    action.ActionID = item.ActionID;
                                    action.CampaignID = Campaign.ID;
                                    action.CreatedDate = DateTime.Now;
                                    action.CreatedBy = CurrentUser.FullName;
                                    action.Treshold = item.Treshold;
                                    action.Operator = item.Operator;
                                    action.Type = item.Type;
                                    action.IsDeleted = false;
                                    result = action.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                        transFailedCount++;
                                    }
                                }
                                else
                                {
                                    AutomaticCampaignActionRule _CampaignRule = new AutomaticCampaignActionRule();
                                    _CampaignRule.ActionID = item.ActionID;
                                    _CampaignRule.CampaignID = Campaign.ID;
                                    _CampaignRule.CreatedDate = DateTime.Now;
                                    _CampaignRule.CreatedBy = CurrentUser.FullName;
                                    _CampaignRule.Treshold = item.Treshold;
                                    _CampaignRule.Operator = item.Operator;
                                    _CampaignRule.Type = item.Type;
                                    _CampaignRule.IsDeleted = false;
                                    result = _CampaignRule.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                                    if (!result.Success)
                                    {
                                        transFailedCount++;
                                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                                    }
                                }
                            }
                        }
                        if (transFailedCount > 0)
                        {
                            transScope.Dispose();
                        }
                        else
                        {
                            transScope.Complete();
                            transScope.Dispose();
                            return RedirectToAction(MVC.Campaign.Index("Edit_Success"));
                        }
                    }
                    else
                    {
                        transScope.Dispose();
                        ModelState.AddModelError("Error", result.ErrorEntity + " " + result.ErrorMessage);
                    }
                }

            }
            GetListCampaignType(model.Type);
            GetListAction(model.CampaignActionList);
            GetListCampaignAction(model.ID, model.CampaignActionList);
            GetListOfChannel(model.Channels);
            GetListOfType();
            GetListOfOperator();
            return View(model);
        }

        private void ValidateEditModel(CampaignModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
            else
            {
                if (model.Code.Length > 50)
                {
                    ModelState.AddModelError("Code", "Code length can not be more than 50 characters");
                }
                var checkData = Campaign.GetByCodeNotSelf(model.Code, model.ID);
                if (checkData != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }

            if (string.IsNullOrEmpty(model.Type))
            {
                ModelState.AddModelError("Type", "Type is required");
            }
            else
            {
                if (model.Type.ToLower() == "PERCENTAGE".ToLower())
                {
                    if (model.Value < 0)
                    {
                        ModelState.AddModelError("Value", "Value minimum 0 (zero)");
                    }
                    else if (model.Value < 100)
                    {
                        ModelState.AddModelError("Value", "Value minimum 100");
                    }
                }
            }

            DateTime startdate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrStartDate))
            {

                if (!DateTime.TryParseExact(model.StrStartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out startdate))
                {
                    ModelState.AddModelError("StrStartDate", "Format start Date is wrong");
                }
            }
            else
            {
                ModelState.AddModelError("StrStartDate", "start date is required");
            }

            DateTime enddate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(model.StrEndDate))
            {

                if (!DateTime.TryParseExact(model.StrEndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out enddate))
                {
                    ModelState.AddModelError("StrEndDate", "Format end Date is wrong");
                }
            }
            else
            {
                ModelState.AddModelError("StrEndDate", "end date is required");
            }

            if (!string.IsNullOrEmpty(model.StrStartDate) && !string.IsNullOrEmpty(model.StrEndDate))
            {
                if (startdate > DateTime.MinValue && enddate > DateTime.MinValue)
                {
                    if (enddate < startdate)
                    {
                        ModelState.AddModelError("StrEndDate", "end date must more or equal than start date");
                    }
                }
            }

            if (model.Channels == null || model.Channels.Count <= 0)
            {
                ModelState.AddModelError("Channels", "select at least 1 channel");
            }
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Campaign Campaign = Campaign.GetById(ID);
            Campaign.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            IEnumerable<AutomaticCampaignActionRule> CampaignRule = AutomaticCampaignActionRule.GetByCampaignID(ID);
            foreach (var item in CampaignRule)
            {
                AutomaticCampaignActionRule CampaignRuleDel = AutomaticCampaignActionRule.GetByID(item.ID);
                CampaignRuleDel.IsDeleted = true;
                CampaignRuleDel.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            }
            return RedirectToAction(MVC.Campaign.Index("Delete_Success"));
        }

        private void GetListCampaignType(string currentTYpe)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (CAMPAIGN_TYPE type in Enum.GetValues(typeof(CAMPAIGN_TYPE)))
            {

                items.Add(new SelectListItem { Text = type.ToString(), Value = type.ToString(), Selected = type.ToString() == currentTYpe });
            }
            ViewBag.ListCampaignType = items;
        }

        private void GetListOfChannel(List<string> selectedChannels = null)
        {
            ViewBag.ListChannel = EnumHelper.ToMultiSelectList<Channel>(false, selectedChannels);
        }

        private void GetListOfType()
        {
            ViewBag.ListType = EnumHelper.ToSelectList<CAMPAIGN_RULE_TYPE>(false);
        }

        private void GetListOfOperator()
        {
            ViewBag.ListOperator = EnumHelper.ToSelectList<CAMPAIGN_RULE_OPERATOR>(false);
        }

        #region List Campaign Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllCampaign()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Campaign> allCampaign = LoyaltyPointEngine.Data.Campaign.GetAll().OrderByDescending(x => x.CreatedDate);
            AjaxGrid<Campaign> grid = this.gridMvcHelper.GetAjaxGrid(allCampaign);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllCampaignPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Campaign> allData = LoyaltyPointEngine.Data.Campaign.GetAll().OrderByDescending(x => x.CreatedDate);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<Campaign> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Campaign.Views.GridGetAllCampaign, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private void GetListAction(int?[] campaignActionList)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoyaltyPointEngine.Data.Action> listdata = LoyaltyPointEngine.Data.Action.GetByCategoryID(1).Where(x => x.IsActive && !x.IsDeleted);
            if (campaignActionList != null)
            {
                listdata = listdata.Where(x => !campaignActionList.Contains(x.ID));
            }
            if (listdata != null)
            {
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
                }
            }
            ViewBag.ListAction = items;
        }
        private void GetListCampaignAction(int? campaginID, int?[] campaignActionList)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (campaignActionList != null)
            {
                IQueryable<LoyaltyPointEngine.Data.Action> listdata = LoyaltyPointEngine.Data.Action.GetAll().Where(x => x.IsActive && !x.IsDeleted).Where(x => campaignActionList.Contains(x.ID));
                if (campaginID != null)
                {
                    listdata = CampaignActionDetail.GetAllByCampaignID(campaginID.Value).Where(x => x.IsActive && !x.IsDeleted).Select(x => x.Action);
                }
                if (listdata != null)
                {
                    foreach (var data in listdata)
                    {
                        items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
                    }
                }
            }
            ViewBag.ListCampaignAction = items;
        }
        #endregion
    }
}