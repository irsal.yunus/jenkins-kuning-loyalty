﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RegionController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Region")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Region successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Region successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Region successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Region")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            RegionModel model = new RegionModel();
            GetListZone();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(RegionModel model)
        {
            this.IsValidRegion(model, false);
            if (ModelState.IsValid)
            {

                Region Region = new Region();
                Region.InjectFrom(model);
                var result = Region.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Region.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListZone();
            return View(model);
        }

        [BreadCrumb(Label = "Edit Region")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int RegionId)
        {
            Region currentRegion = LoyaltyPointEngine.Data.Region.GetById(RegionId);
            RegionModel model = new RegionModel();
            model.InjectFrom(currentRegion);
            GetListZone();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(RegionModel model)
        {
            this.IsValidRegion(model, true);
            if (ModelState.IsValid)
            {
                Region Region = LoyaltyPointEngine.Data.Region.GetById(model.ID);
                Region.InjectFrom(model);
                var result = Region.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Region.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListZone();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Region Region = Region.GetById(ID);
            Region.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Region.Index("Delete_Success"));
        }

        private void IsValidRegion(RegionModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.Code))
            {
                Data.Region predictedData = Data.Region.GetByCode(Model.Code);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("Region Code '{0}'  has been used by other Region", Model.Code));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("Region Code  '{0}'  has been used by other Region", Model.Code));
            }
        }
        private void GetListZone()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Zone> listdata = LoyaltyPointEngine.Data.Zone.GetAll();
            foreach (Zone data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListZone = items;
        }

        #region List Region Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllRegion()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Region> allRegion = LoyaltyPointEngine.Data.Region.GetAll().OrderBy(x => x.Name);
            AjaxGrid<Region> grid = this.gridMvcHelper.GetAjaxGrid(allRegion);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllRegionPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Region> allData = LoyaltyPointEngine.Data.Region.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<Region> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Region.Views.GridGetAllRegion, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}