﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object.CMSUser;
using LoyaltyPointEngine.PointLogicLayer;

using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
//Create This File by Ucup
namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class QuickWin2Controller : BaseController
    {
        // GET: LoginLog
        // GET: LoginLog
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "QuickWin2")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            QuickWin2Participation model = new QuickWin2Participation();
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Product successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Product successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Product successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            //GetListUsername();
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Report QUickWin2")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string phone, string tglAwal, string tglAkhir)
        {
            var datas = QuickWin2Participation.GetAll().Where(x => x.ReceiptStatus == 1).OrderBy(x => x.CreatedDate).AsQueryable();

            if (!string.IsNullOrEmpty(tglAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(tglAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }
            var export = datas.ToList();
            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetExportQuickWin2(export);
            fileName = string.Format("ReportQuickWin2_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Phone", "ReceiptCode",
                "ReceiptStatus", "CreatedDate", "Comment" });
            excelStream.Position = 0;

            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllQuickWin2()
        {
            gridMvcHelper = new GridMvcHelper();

            //IOrderedQueryable<LoginLog> allLoginLog = LoginLog.GetAll().OrderBy(x => x.ID);
            IOrderedQueryable<QuickWin2Participation> allParticipation = QuickWin2Participation.GetAll().Where(x => x.ReceiptStatus == 1).OrderBy(x => x.CreatedDate);
            var grid = this.gridMvcHelper.GetAjaxGrid(allParticipation);
            return PartialView(grid);
        }       

        [HttpGet]
        public virtual ActionResult GridGetAllQuickWin2Pager(string hiddenUsername, string hiddenAwal, string hiddenAkhir, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            //IQueryable<LoginLog> datas = LoginLog.GetAll();
            IQueryable<QuickWin2Participation> datas = QuickWin2Participation.GetAll().Where(x => x.ReceiptStatus == 1);
            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }

            AjaxGrid<QuickWin2Participation> grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.QuickWin2.Views.GridGetAllQuickWin2, this);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private List<List<string>> GetExportQuickWin2(List<QuickWin2Participation> param)
        {
            var assetMainUrl = Data.Settings.GetValueByKey(SiteSetting.ASSET_URL);
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                data.Add(item.MemberPhone);
                data.Add(item.ReceiptCode);
                data.Add(item.ReceiptStatus == 1? "Approve": item.ReceiptStatus == 0 ? "Waiting" : "Reject");
                data.Add(item.CreatedDate.ToString("dd/MM/yyyy HH:mm"));
                data.Add(item.Comment);
                datas.Add(data);
            }
            return datas;
        }
    }
}