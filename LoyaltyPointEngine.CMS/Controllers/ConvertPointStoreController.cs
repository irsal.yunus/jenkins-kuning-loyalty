﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Parameter.ConvertPoint;
using MvcBreadCrumbs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ConvertPointStoreController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        #region Store
        [BreadCrumb(Clear = true, Label = "Store")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [HttpGet]
        [BreadCrumb(Clear = true, Label = "Add Store")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Add()
        {
            return View(new StoreParamModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [BreadCrumb(Clear = true, Label = "Add Store")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(StoreParamModel model)
        {
            ConvertPointStore store;
            ValidateStoreModel(model, out store);
            if (ModelState.IsValid)
            {
                store = new ConvertPointStore
                {
                    StoreName = model.Name,
                    IsDeleted = model.IsDeleted
                };
                var dbResponse = store.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (!dbResponse.Success)
                {
                    ModelState.AddModelError("Model", dbResponse.ErrorMessage);
                    ModelState.AddModelError("Model", dbResponse.ErrorEntity);
                    ModelState.AddModelError("Model", dbResponse.AdditionalInfo);
                }
                else
                    return RedirectToAction(MVC.ConvertPointStore.Edit(store.ID));
            }
            return View(model);
        }

        [HttpGet]
        [BreadCrumb(Clear = true, Label = "Edit Store")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Edit(long Id)
        {
            var predictedStore = ConvertPointStore.GetById(Id);
            if (predictedStore == null)
                return RedirectToAction(MVC.ConvertPointStore.Index());
            return View(new StoreParamModel
            {
                ID = predictedStore.ID,
                Name = predictedStore.StoreName,
                IsDeleted = predictedStore.IsDeleted
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [BreadCrumb(Clear = true, Label = "Edit Store")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(StoreParamModel model)
        {
            ConvertPointStore predictedStore = null;
            ValidateStoreModel(model, out predictedStore);
            if (ModelState.IsValid)
            {
                predictedStore.StoreName = model.Name;
                predictedStore.IsDeleted = model.IsDeleted;
                var dbResponse = predictedStore.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (!dbResponse.Success)
                {
                    ModelState.AddModelError("Model", dbResponse.ErrorMessage);
                    ModelState.AddModelError("Model", dbResponse.ErrorEntity);
                    ModelState.AddModelError("Model", dbResponse.AdditionalInfo);
                }
                else
                    return RedirectToAction(MVC.ConvertPointStore.Index("Edit_Success"));
            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(long ID)
        {
            ConvertPointStore store = ConvertPointStore.GetById(ID);
            store.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.ConvertPointStore.Index("Delete_Success"));
        }
        #endregion

        #region Rule
        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult AddRule(long storeId)
        {
            return PartialView(new StoreRuleParamModel
            {
                StoreId = storeId
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddRule(StoreRuleParamModel model)
        {
            ConvertPointRule rule;
            var predictedStore = ConvertPointStore.GetById(model.StoreId);
            if (predictedStore == null)
                ModelState.AddModelError("ID", "Invalid StoreId. StoreId does not exist.");
            ValidateRuleModel(model);
            if (ModelState.IsValid)
            {
                rule = new ConvertPointRule
                {
                    StoreId = predictedStore.ID,
                    StoreName = predictedStore.StoreName,
                    IsDeleted = model.IsDeleted,
                    Point = model.Point,
                    StorePoint = model.StorePoint,
                    ConvertOutUrl = model.ConvertOutUrl,
                    IconImageUrl = model.IconImageUrl,
                    LogoImageUrl = model.LogoImageUrl,
                    VoidOutUrl = model.VoidOutUrl,
                    ConvertCap = model.ConvertCap,
                    MinConvert = model.MinConvert,
                    ConvertCount = model.ConvertCount
                };
                var dbResponse = rule.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (!dbResponse.Success)
                {
                    ModelState.AddModelError("Model", dbResponse.ErrorMessage);
                    ModelState.AddModelError("Model", dbResponse.ErrorEntity);
                    ModelState.AddModelError("Model", dbResponse.AdditionalInfo);
                }
                else
                    return PartialView(new StoreRuleParamModel
                    {
                        StoreId = model.StoreId,
                        Point = 0,
                        Promo = 0,
                        StorePoint = 0,
                        ConvertOutUrl = string.Empty,
                        IconImageUrl = string.Empty,
                        LogoImageUrl = string.Empty,
                        VoidOutUrl = string.Empty,
                        ConvertCap = 1,
                        MinConvert = 1,
                        ConvertCount = 1
                    });
            }
            return PartialView(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult DeleteRule(long ruleId)
        {
            ConvertPointRule rule;
            var predictedRule = ConvertPointRule.GetById(ruleId);
            if (predictedRule == null)
                return Json(new
                {
                    Success = false,
                    Message = "rule not exist"
                }, JsonRequestBehavior.AllowGet);
            var dbResponse = predictedRule.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return Json(new
            {
                Success = dbResponse.Success,
                Message = JsonConvert.SerializeObject(dbResponse)
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Helper
        private void ValidateStoreModel(StoreParamModel model, out ConvertPointStore editedStore)
        {
            editedStore = null;
            if (model.ID != null)
            {
                editedStore = ConvertPointStore.GetById(model.ID.Value);
                if (editedStore == null)
                    ModelState.AddModelError("ID", "Invalid Id. Id does not exist.");
            }
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Please fill the store name.");
            }
        }

        private void ValidateRuleModel(StoreRuleParamModel model)
        {
            if (model.Point <= 0)
                ModelState.AddModelError("Point", "Point must be bigger than 0.");

            if (model.StorePoint <= 0)
                ModelState.AddModelError("StorePoint", "Store Point must be bigger than 0.");

            if (model.Promo < 0)
                ModelState.AddModelError("Promo", "Promo point can not be less than 0.");
            if (model.ConvertCap < 1)
                ModelState.AddModelError("ConvertCap", "Convert Point Cap can not be less than 1.");
            if (model.MinConvert < 1)
                ModelState.AddModelError("MinConvert", "Minimum Convert Point can not be less than 1.");
            if (model.ConvertCount < 1)
                ModelState.AddModelError("ConvertCount", "Convert Count Limit can not be less than 1.");
        }

        public virtual ActionResult GridGetAllStore()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<ConvertPointStore> allStore = ConvertPointStore.GetAll(true).OrderBy(x => x.StoreName);
            AjaxGrid<ConvertPointStore> grid = this.gridMvcHelper.GetAjaxGrid(allStore);
            return PartialView(grid);
        }

        public virtual ActionResult GridGetAllStorePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<ConvertPointStore> allStore = ConvertPointStore.GetAll(true).OrderBy(x => x.StoreName);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allStore = allStore.Where(x => x.StoreName.Contains(txtSearch)).OrderBy(x => x.StoreName);
            }
            AjaxGrid<ConvertPointStore> grid = this.gridMvcHelper.GetAjaxGrid(allStore, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.ConvertPointStore.Views.GridGetAllStore, this);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GridGetAllStoreRule(long storeId)
        {
            var predictedStore = ConvertPointStore.GetById(storeId);
            if (predictedStore == null)
            {
                return RedirectToAction(MVC.Error.Index());
            }
            var storeRules = ConvertPointRule.GetAllByStoreId(storeId);
            return PartialView(storeRules.ToList());
        }
        #endregion
    }
}