﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter.Member;
using MvcBreadCrumbs;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Model.Object;
using System.Data.Objects;
using LoyaltyPointEngine.Model.Parameter.Notification;
using System.Reflection;
using Newtonsoft.Json;
using log4net;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberKKApprovalController : BaseController
    {
        /*
         * Data Lengkap Member disimpan di Database Loyalty Member
         *
         */
        public MemberKKApprovalController()
        {
            Log = LogManager.GetLogger(typeof(MemberKKApprovalController));
        }

        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Member KK Approval")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Member successfully created";
                    break;
                case "Approve_Success":
                    ViewBag.Success = "Member KK successfully approved ";
                    break;
                case "Reject_Success":
                    ViewBag.Success = "Member KK successfully rejected ";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Member successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            // PopulateChannel();
            GetChannelTypeList();
            return View();
        }

        [BreadCrumb(Label = "Approve Member KK")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int MemberId, string code = "", string status = "")
        {
            Member currentMember = Member.GetById(MemberId);

            if (currentMember.IsApprovedKKImage != null)
            {
                return RedirectToAction(MVC.MemberKKApproval.Index());
            }

            ApproveMemberKKImageModel model = new ApproveMemberKKImageModel();
            model.InjectFrom(currentMember);
            model.IsApprovedKKImage = currentMember.IsApprovedKKImage ?? false;
            model.Birthdate = currentMember.Birthdate.HasValue ? currentMember.Birthdate.Value.ToString("dd/MM/yyyy") : "";
            model.Password = "";
            model.ConfirmPassword = "";
            model.ChildBirthdate = currentMember.Child.Count > 0 ? currentMember.Child.FirstOrDefault().Birthdate.Value.ToString("dd/MM/yyyy") : "";
            //if (currentMember.Child.Any())
            //{
            //    var firstChild = currentMember.Child.FirstOrDefault();
            //    model.ChildBirthdate = firstChild.Birthdate.HasValue ? firstChild.Birthdate.Value.ToString("dd/MM/yyyy") : "";
            //    model.ChildName = firstChild.Name;
            //}

            if (currentMember.Child.Any())
            {
                model.Childs = (from a in currentMember.Child.Where(x => !x.IsDeleted)
                                select new AddChildParameterModel
                                {
                                    ID = a.ID,
                                    ParentID = a.ParentID,
                                    Name = a.Name,
                                    Gender = a.Gender,
                                    PlaceOfBirth = a.PlaceOfBirth,
                                    Birthdate = a.Birthdate.HasValue ? a.Birthdate.Value.ToString("dd/MM/yyyy") : DateTime.MinValue.ToString("dd/MM/yyyy"),
                                    ProductBeforeId = a.ProductBeforeID.HasValue ? a.ProductBeforeID.Value : 0
                                }).ToList();
                var firstChild = currentMember.Child.FirstOrDefault();
                model.ChildBirthdate = firstChild.Birthdate.HasValue ? firstChild.Birthdate.Value.ToString("dd/MM/yyyy") : "";
                model.ChildName = firstChild.Name;
            }

            if (status != "")
            {
                ViewBag.Code = code;
                ViewBag.StatusDeleteChild = status;
            }

            PopulateReasonReject();

            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ApproveMemberKKImageModel model, EditMemberParameterModel param, string mode="approve")
        {
            EFResponse result = new EFResponse();
            var currMember = Member.GetById(model.ID);

            bool isApprove = string.IsNullOrEmpty(Request.Form["reject"]);

            if (currMember == null)
            {
                ModelState.AddModelError("", "Id not found");
            }
            else
            {
                var IsSuccessful = new ResultModel<bool>();
                if (model.Childs != null)
                {
                    // Validate childs
                    var resultValidation = MemberLogic.ValidateUpdateMultipleChild(model.Childs);

                    if (resultValidation.StatusCode == "00")
                    {
                        foreach (var item in model.Childs)
                        {
                            IsSuccessful = MemberLogic.UpdateChildNameAndChildBirthDate(model.ID, item.ID,
                                        item.BirthdateChild,
                                        item.Name, CurrentUser.Username);

                            if (!IsSuccessful.Value)
                            {
                                ModelState.AddModelError("", IsSuccessful.StatusMessage);
                                return View(model);
                            }
                        }
                    }
                }

                if (IsSuccessful.Value == true)
                {

                    currMember.IsApprovedKKImage = true;
                    currMember.ReasonRejectKK = null;

                    if (!isApprove)
                    {
                        currMember.IsApprovedKKImage = false;
                        currMember.ReasonRejectKK = model.ReasonRejectKK;
                        currMember.RejectDate = DateTime.Now;
                        currMember.RejectBy = CurrentUser.Username;
                    }
                    
                    result = currMember.Updated(CurrentUser.Username);

                    if (result.Success)
                    {
                        
                        string succesAction = isApprove ? "Approve_Success" : "Reject_Success";
                        string title;
                        string message;

                        var approvalImage = DataMember.ApprovalImage.GetByMemberIdTop1(currMember.ID);

                        if (approvalImage != null)
                        {
                            approvalImage.Status = isApprove ? 2 : 1;
                            approvalImage.ReasonRejectKK = isApprove ? null : model.ReasonRejectKK;

                            approvalImage.Updated(CurrentUser.FullName);

                        }

                        if (isApprove)
                        {
                            title = Data.Settings.GetByKey("KKApproveSuccessTitle").Key;
                            message = Data.Settings.GetValueByKey("KKApproveSuccessMsg");
                        }
                        else
                        {
                            title = Data.Settings.GetByKey("KKRejectSuccessTitle").Key;
                            message = String.Format("{0}.{1}", model.ReasonRejectKK, Data.Settings.GetValueByKey("ReuploadKKBirthCertificateMsg"));
                        }

                        ParamCreateNotification paramNotif = new ParamCreateNotification();
                        paramNotif.Message = message;
                        paramNotif.Title = title;
                        paramNotif.Type = Common.Notification.NotificationType.Type.Information.ToString();
                        NotificationLogic.Create(paramNotif, currMember.ID);

                        EditMemberParameterModel editModel = new EditMemberParameterModel();
                        editModel.InjectFrom(model);

                        return RedirectToAction(MVC.MemberKKApproval.Index(succesAction));

                    }

                }

                
                //if (ModelState.IsValid)
                //{
                //    EditMemberParameterModel editModel = new EditMemberParameterModel();
                //    editModel.InjectFrom(model);
                //    if (IsChangePassword)
                //    {
                //        editModel.Password = Common.Encrypt.LoyalEncript(model.Password);

                //    }
                //    editModel.IsNewsletter = model.IsNewsLetter;
                //    if (string.IsNullOrEmpty(model.Gender))
                //    {
                //        model.Gender = "f";
                //    }
                //    var result = MemberLogic.UpdateMemberByMemberModel(editModel, CurrentUser.FullName, model.ID, true, model);
                //    if (result.StatusCode == "00")
                //    {
                //        string provinceName = (model.ProvinceID.HasValue) ? DataMember.Province.GetByID(model.ProvinceID.Value).ProvinceName : string.Empty;
                //        string districtName = (model.DistrictID.HasValue) ? DataMember.District.GetByID(model.DistrictID.Value).DistrictName : string.Empty;
                //        string StagesValue = (model.StagesID.HasValue) ? DataMember.Stages.GetById(model.StagesID.Value).Name : string.Empty;
                //        var dataMember = DataMember.Member.GetById(model.ID);
                //        return RedirectToAction(MVC.MemberKKApproval.Index("Edit_Success"));
                //    }
                //    else
                //        ModelState.AddModelError("Error", result.StatusMessage);
                //}
            }
            PopulateReasonReject();
            return View(model);
        }

        #region List Member Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllMember()
        {
            gridMvcHelper = new GridMvcHelper();
            // IOrderedQueryable<Member> allMember = Member.GetAll().OrderBy(x => x.FirstName);

            IQueryable<Member> rawData = Member.GetAll();

            string spgRoleCode;
            if (this.IsSpgRole(out spgRoleCode))
            {
                var spgUser = SPG.GetByCode(CurrentUser.Username);
                rawData = rawData.Where(x => x.SpgID == spgUser.ID);
            }

            int _totalRow = rawData.Count(),
                _limit = 5;

            IQueryable<MemberGridModel> datas = from a in rawData
                                                select new MemberGridModel
                                                {
                                                    ID = a.ID,
                                                    Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                                                    FirstName = a.FirstName,
                                                    LastName = a.LastName,
                                                    Email = a.Email,
                                                    Phone = a.Phone,
                                                    IsBlacklist = a.IsBlacklist,
                                                    IsApprovedKKImage = a.IsApprovedKKImage ?? false, 
                                                    //IsApprovedKKImage = a.IsApprovedKKImage,
                                                    Point = a.MemberPoint,
                                                    CreatedDate = a.CreatedDate,
                                                    Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                                                    Channel = a.Channel.Name,
                                                    IsUploadedKK = !string.IsNullOrEmpty(a.KKImage) ? true : false
                                                };

            ViewBag.TotalPage = (int)Math.Ceiling((double)_totalRow / _limit);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllMemberPager(string search, string orderBy, string orderDirection, int? page, string hiddenAwal, string hiddenAkhir, string channel, bool isUploadedKTP = false, bool isUploadedKK = false, bool isUploadedAkte = false)
        {
            //gridMvcHelper = new GridMvcHelper();
            Channel dataChannel = null;
           
            int _page = page.GetValueOrDefault(1),
                _limit = 5,
                _totalRow = 0;
            string _orderBy = orderBy ?? "FirstName",
                _orderDirection = orderDirection ?? "Asc";
            if (_page < 1) _page = 1;

            ViewBag.TglAwal = hiddenAwal;
            ViewBag.TglAkhir = hiddenAkhir;
            ViewBag.isUploadedKTP = isUploadedKTP;
            ViewBag.isUploadedKK = isUploadedKK;
            ViewBag.isUploadedAkte = isUploadedAkte;
            ViewBag.Channel = channel;
            DateTime tempStartDate = DateTime.MinValue,
                    tempEndDate = DateTime.MinValue;
            DateTime? startDate = null, endDate = null;

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempStartDate))
                    startDate = tempStartDate;
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempEndDate))
                    endDate = tempEndDate;
            }

            List<MemberGridModel> members = new List<MemberGridModel>();
            if (!string.IsNullOrEmpty(search) )
            {
                var rawMember = Member.GetAll()
                    .Where(x => x.FirstName.Contains(search) || x.LastName.Contains(search) || x.Email.Contains(search) || x.Phone.Contains(search) || (!string.IsNullOrEmpty(x.SMSID) && x.SMSID.Contains(search)) || x.Channel.Name.Contains(search))
                    .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));


                if (startDate != null && endDate != null)
                {
                    rawMember = rawMember
                    .Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(startDate) &&
                                 EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDate));
                }

                if (isUploadedKTP)
                {
                    rawMember = rawMember
                    .Where(x => !string.IsNullOrEmpty(x.KTPImage));
                }

                if (isUploadedKK)
                {
                    rawMember = rawMember
                    .Where(x => !string.IsNullOrEmpty(x.KKImage));
                }

                if (isUploadedAkte)
                {
                    rawMember = rawMember
                    .Where(x => !string.IsNullOrEmpty(x.AkteImage));
                }

                if (!string.IsNullOrEmpty(channel))
                {
                    dataChannel = Channel.GetById(Convert.ToInt32(channel));
                    rawMember = rawMember
                    .Where(x => x.ChannelId == dataChannel.ID);
                }

                string spgRoleCode;
                if (this.IsSpgRole(out spgRoleCode))
                {
                    var spgUser = SPG.GetByCode(CurrentUser.Username);
                    rawMember = rawMember.Where(x => x.SpgID == spgUser.ID);
                }

                _totalRow = rawMember.Count();
                members = rawMember
                    .Skip((_page - 1) * _limit)
                    .Take(_limit)
                    .Select(x => new MemberGridModel
                    {
                        ID = x.ID,
                        Gender = x.Gender.ToLower() == "m" ? "Male" : "Female",
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Phone = x.Phone,
                        IsBlacklist = x.IsBlacklist,
                        // IsApprovedKKImage = x.IsApprovedKKImage ?? false, 
                        IsUploadedKTP = !string.IsNullOrEmpty(x.KTPImage) ? true : false,
                        IsUploadedAkte = !string.IsNullOrEmpty(x.AkteImage) ? true : false,
                        IsUploadedKK = !string.IsNullOrEmpty(x.KKImage) ? true : false,
                        IsApprovedKKImage = x.IsApprovedKKImage,
                        Point = x.MemberPoint,
                        CreatedDate = x.CreatedDate,
                        Type = !string.IsNullOrEmpty(x.SMSID) ? "Sms" : "Web",
                        Channel = x.Channel.Name,
                        EventCode = x.EventCode
                    })
                    .ToList();
            }
            else
            {
                var rawMember = Member.GetAll();

                if (startDate != null && endDate != null)
                {
                    rawMember = rawMember
                    .Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(startDate) &&
                                 EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDate));
                }

                if (isUploadedKTP)
                {
                    rawMember = rawMember
                    .Where(x => !string.IsNullOrEmpty(x.KTPImage));
                }

                if (isUploadedKK)
                {
                    rawMember = rawMember
                    .Where(x => !string.IsNullOrEmpty(x.KKImage));
                }

                if (isUploadedAkte)
                {
                    rawMember = rawMember
                    .Where(x => !string.IsNullOrEmpty(x.AkteImage));
                }

                if (!string.IsNullOrEmpty(channel))
                {
                    dataChannel = Channel.GetById(Convert.ToInt32(channel));
                    rawMember = rawMember
                    .Where(x => x.ChannelId == dataChannel.ID);
                }

                string spgRoleCode;
                if (this.IsSpgRole(out spgRoleCode))
                {
                    var spgUser = SPG.GetByCode(CurrentUser.Username);
                    rawMember = rawMember.Where(x => x.SpgID == spgUser.ID);
                }

                members = rawMember.OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection))
                    .Skip((_page - 1) * _limit)
                    .Take(_limit)
                    .Select(x => new MemberGridModel
                    {
                        ID = x.ID,
                        Gender = x.Gender.ToLower() == "m" ? "Male" : "Female",
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Phone = x.Phone,
                        IsBlacklist = x.IsBlacklist,
                        // IsApprovedKKImage = x.IsApprovedKKImage ?? false, 
                        IsUploadedKTP = !string.IsNullOrEmpty(x.KTPImage) ? true : false,
                        IsUploadedAkte = !string.IsNullOrEmpty(x.AkteImage) ? true : false,
                        IsUploadedKK = !string.IsNullOrEmpty(x.KKImage) ? true : false,
                        IsApprovedKKImage = x.IsApprovedKKImage ,
                        Point = x.MemberPoint,
                        CreatedDate = x.CreatedDate,
                        Type = !string.IsNullOrEmpty(x.SMSID) ? "Sms" : "Web",
                        Channel = x.Channel.Name,
                        EventCode = x.EventCode
                    })
                    .ToList();

                _totalRow = rawMember.Count();
            }
            ViewBag.Search = search;
            ViewBag.CurrentPage = _page;
            ViewBag.TotalPage = (int)Math.Ceiling((double)_totalRow / _limit);
            ViewBag.OrderColumn = _orderBy;
            ViewBag.OrderDirection = _orderDirection;
            ViewBag.TotalRow = _totalRow;
            return PartialView(MVC.MemberKKApproval.Views.GridGetAllMember, members);
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult DeleteChild(int ID, int MemberID)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();
            string currentUserName = CurrentUser.FullName;
            var childsCount = ChildLogic.ChildsCountByMemberID(MemberID);
            if (childsCount.StatusCode == "00")
            {
                if (childsCount.Value <= 1)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = "Cannot delete the only one child.";
                }
                else
                {
                    result = ChildLogic.DeleteByCollect(ID, currentUserName);
                }

            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = childsCount.StatusMessage;
            }

            return RedirectToAction(MVC.MemberKKApproval.Edit(MemberID, result.StatusCode, result.StatusMessage));
        }

        private void PopulateChannel()
        {
            ViewBag.ChannelList = EnumHelper.ToSelectList<ChannelType>();
        }

        private void GetChannelTypeList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "==ALL==", Value = " ", Selected = true });
            IQueryable<Channel> listdata = Channel.GetAll();
            foreach (Channel data in listdata)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ChannelList = items;
        }

        private void PopulateReasonReject()
        {
            //List<string> reasonRejectList = new List<string>() {
            // "Foto KTP tidak jelas",
            // "KTP yang di upload tidak sesuai dengan yang terdaftar / di list",
            // "Foto Akte tidak jelas",
            // "Akte yang di upload tidak sesuai dengan yang terdaftar / di list"
            //};
            var reasonRejectList = Data.Settings.GetAllByCode("ReasonReject").OrderBy(x => x.ID).ToList();

            ViewBag.ReasonRejectList = reasonRejectList;
        }

        #endregion

        #region smb-1345 1st Birthday Campaign - KK & Akte Enhancement

        public virtual ActionResult MoveImage(int MemberId)
        {
            PopulateMoveImage();
            ViewBag.memberId = MemberId;
            return PartialView(MVC.MemberKKApproval.Views.ChangeImage);
        }

      
        [HttpPost]
        public virtual ActionResult MoveImage(int MemberId,string sourceImage, string destinationImage)
        {
            Log.Info("Before process param = {MemberId = "+ MemberId + ", sourceImage = "+ sourceImage + ", destinationImage = "+ destinationImage+"}");
            var result = ActionToMoveImage(MemberId,sourceImage,destinationImage, CurrentUser.Username);
            Log.Info("After process result = "+ JsonConvert.SerializeObject(result));
            return RedirectToAction(MVC.MemberKKApproval.Edit(MemberId, result.StatusCode, result.StatusMessage));
        }

        private static ResultModel<Member> ActionToMoveImage(int memberId, string sourceImage, string destinationImage, string by)
        {
            ResultModel<Member> result = new ResultModel<Member>();
         
            if (sourceImage == " ")
            {
                result.StatusCode = "57";
                result.StatusMessage = "source image belum dipilih";
                return result;
            }
            else
            {
                if (destinationImage == " ")
                {
                    result.StatusCode = "57";
                    result.StatusMessage = "destination image belum dipilih";
                    return result;
                }
                else
                {
                    if (sourceImage.ToLower() == destinationImage.ToLower())
                    {
                        result.StatusCode = "22";
                        result.StatusMessage = "source dan destination tidak boleh sama";
                        return result;
                    }
                }

            }

            var member = Member.GetById(memberId);
            var _ktp = member.KTPImage;
            var _kk = member.KKImage;
            var _akte = member.AkteImage;

            switch (sourceImage.ToLower())
            {
                case "kkimage":
                    if (destinationImage.ToLower() == "akteimage")
                    {
                        member.KKImage = _akte;
                        member.AkteImage = _kk;
                        
                    }
                    if (destinationImage.ToLower() == "ktpimage")
                    {
                        member.KTPImage = _kk;
                        member.KKImage = _ktp;
                    }
                    break;
                case "akteimage":
                    if (destinationImage.ToLower() == "kkimage")
                    {
                        member.KKImage = _akte;
                        member.AkteImage = _kk;
                    }
                    if (destinationImage.ToLower() == "ktpimage")
                    {
                        member.KTPImage = _akte;
                        member.AkteImage = _ktp;
                    }
                    break;
                case "ktpimage":
                    if (destinationImage.ToLower() == "kkimage")
                    {
                        member.KKImage = _ktp;
                        member.KTPImage = _kk;
                    }
                    if (destinationImage.ToLower() == "akteimage")
                    {
                        member.AkteImage = _ktp;
                        member.KTPImage = _akte;
                    }
                    break;
            }
            
            var data = member.Updated(by);
            result.StatusCode = "00";
            result.StatusMessage = "success move image";
            return result;
        }
        private void PopulateMoveImage()
        {
            var moveImageList = Data.Settings.GetAllByCode("MoveImage").OrderBy(x => x.ID).ToList();

            ViewBag.MoveImageList = moveImageList;
        }
        #endregion
    }

}