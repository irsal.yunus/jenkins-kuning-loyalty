﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Model;
using System.Web.Mvc;
using System.Globalization;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Helper;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CodegimmickController : BaseController
    {
        // GET: Codegimmick
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Code Gimmick")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status, int? ID)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Code Gimmick successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Code Gimmick  successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Code Gimmick  successfully deleted";
                    break;
                case "Is_Already":
                    ViewBag.Success = "Code Gimmick is already Name";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            if (ID != null)
            {
                ViewBag.Action = Url.Action(MVC.Codegimmick.Edit());
                CodeGimmick code = CodeGimmick.GetByID(ID.Value);
                ViewBag.CodeName = code.Name;
            }
            return View();
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllCode()
        {
            gridMvcHelper = new GridMvcHelper();

            IOrderedQueryable<CodeGimmick> allCode = CodeGimmick.GetAll().OrderByDescending(x => x.CreatedDate);
            var grid = this.gridMvcHelper.GetAjaxGrid(allCode);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllCodePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CodeGimmick> datas = CodeGimmick.GetAll().OrderByDescending(x => x.CreatedDate);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.Name.Contains(txtSearch))
                    .OrderByDescending(x => x.CreatedDate);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Codegimmick.Views.GridGetAllCode, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(CodeGimmick model)
        {
            var checkData = CodeGimmick.GetByCode(model.Name);
            if (checkData != null)
            {
                return RedirectToAction(MVC.Codegimmick.Index("Is_Already", null));
            }
            if (ModelState.IsValid)
            {
                var result = model.Insert(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.Codegimmick.Index("Create_Success", null));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(CodeGimmick model)
        {
            var checkData = CodeGimmick.GetByCodeNotSelf(model.Name, model.ID);
            if (checkData != null)
            {
                return RedirectToAction(MVC.Codegimmick.Index("Is_Already", Convert.ToInt32(model.ID)));
            }
            if (ModelState.IsValid)
            {
                var result = model.Update(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.Codegimmick.Index("Edit_Success", null));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            CodeGimmick Code = CodeGimmick.GetByID(ID);
            Code.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Codegimmick.Index("Delete_Success", null));
        }
    }
}