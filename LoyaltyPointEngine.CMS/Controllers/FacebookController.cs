﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using Facebook;
using LoyaltyPointEngine.Model.Object.Facebook;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System.Transactions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class FacebookController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Facebook")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Post successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Post successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Post successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Clear = false, Label = "Post Detail")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Detail(Guid ID)
        {
            var data = FacebookPost.GetByID(ID);
            return View(new FacebookPostModel(data));
        }


        #region List  Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllPost()
        {
            gridMvcHelper = new GridMvcHelper();
            //IOrderedQueryable<LoyaltyPointEngine.Data.FacebookPost> allAction = LoyaltyPointEngine.Data.FacebookPost.GetAll().OrderByDescending(x => x.CreatedDate);
            var datas = from a in LoyaltyPointEngine.Data.FacebookPost.GetAll()
                        select new FacebookPostCMSModel
                        {
                            ID = a.ID,
                            created_time = a.CreatedDate,
                            message = a.Content,
                            PostId = a.PostID,
                            TotalComment = a.FacebookComment.Count(x => x.CollectID != null),
                            TotalLike = a.FacebookLike.Count(x => x.CollectID != null)
                        };
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.created_time));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllPostPager(string txtSearch, int? page)
        {

            gridMvcHelper = new GridMvcHelper();
            var datas = from a in LoyaltyPointEngine.Data.FacebookPost.GetAll()
                        select new FacebookPostCMSModel
                        {
                            ID = a.ID,
                            created_time = a.CreatedDate,
                            message = a.Content,
                            PostId = a.PostID,
                            TotalComment = a.FacebookComment.Count(x => x.CollectID != null),
                            TotalLike = a.FacebookLike.Count(x => x.CollectID != null)
                        };
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.message.Contains(txtSearch));
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.created_time), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Facebook.Views.GridGetAllPost, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [ChildActionOnly]
        public virtual ActionResult GridGetAllComment(Guid ID)
        {
            gridMvcHelper = new GridMvcHelper();
            //  IOrderedQueryable<LoyaltyPointEngine.Data.FacebookComment> allAction = LoyaltyPointEngine.Data.FacebookComment.GetByPostID(ID).OrderByDescending(x => x.CreatedDate);
            var datas = from a in LoyaltyPointEngine.Data.FacebookComment.GetByPostID(ID)
                        select new FacebookCommentModel
                        {
                            ID = a.ID,
                            CreatedDate = a.CreatedDate,
                            MemberName = a.Member != null ? a.Member.Name : "",
                            Point = a.Collect != null ? a.Collect.Points : 0,
                            FacebookUserID = a.FacebookUserID,
                            Comment = a.Comment,
                            CommentID = a.CommentID

                        };

            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllCommentPager(Guid PostID, string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = from a in LoyaltyPointEngine.Data.FacebookComment.GetByPostID(PostID)
                        select new FacebookCommentModel
                        {
                            ID = a.ID,
                            CreatedDate = a.CreatedDate,
                            MemberName = a.Member != null ? a.Member.Name : "",
                            Point = a.Collect != null ? a.Collect.Points : 0,
                            FacebookUserID = a.FacebookUserID,
                            Comment = a.Comment,
                            CommentID = a.CommentID

                        };
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.MemberName.Contains(txtSearch) || x.FacebookUserID.Contains(txtSearch) ||
                     x.FacebookUserID.Contains(txtSearch)
                    ).OrderBy(x => x.CreatedDate);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Facebook.Views.GridGetAllComment, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [ChildActionOnly]
        public virtual ActionResult GridGetAllLike(Guid ID)
        {
            gridMvcHelper = new GridMvcHelper();
            // IOrderedQueryable<LoyaltyPointEngine.Data.FacebookLike> allAction = LoyaltyPointEngine.Data.FacebookLike.GetByPostID(ID).OrderByDescending(x => x.CreatedDate);
            var datas = from a in LoyaltyPointEngine.Data.FacebookLike.GetByPostID(ID)
                        select new FacebookLikeModel
                        {
                            ID = a.ID,
                            CreatedDate = a.CreatedDate,
                            MemberName = a.Member != null ? a.Member.Name : "",
                            Point = a.Collect != null ? a.Collect.Points : 0,
                            FacebookUserID = a.FacebookUserID


                        };

            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllLikePager(Guid PostID, string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = from a in LoyaltyPointEngine.Data.FacebookLike.GetByPostID(PostID)
                        select new FacebookLikeModel
                        {
                            ID = a.ID,
                            CreatedDate = a.CreatedDate,
                            MemberName = a.Member != null ? a.Member.Name : "",
                            Point = a.Collect != null ? a.Collect.Points : 0,
                            FacebookUserID = a.FacebookUserID


                        };

            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.MemberName.Contains(txtSearch) || x.FacebookUserID.Contains(txtSearch)).OrderBy(x => x.CreatedDate);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Facebook.Views.GridGetAllLike, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Facebook Post

        [HttpPost]
        public virtual ActionResult PopulatePostFromFacebook(string token)
        {
            try
            {

                var LastPost = Data.FacebookPost.GetNewestPost();
                FacebookClient client = new FacebookClient(token);
                client.AppId = Settings.GetValueByKey(SiteSetting.FacebookApplicationID);// "275134142859426";
                client.AppSecret = Settings.GetValueByKey(SiteSetting.FacebookApplicationSecret);//"fdfca89b956b7476b9a5290cde141aa9";
                string path = string.Format("{0}/posts", Settings.GetValueByKey(SiteSetting.FacebookPageID));//177458975683906
                GetPostAll(client, path, LastPost);
                return Json(new { Success = true, Message = "success synch: " + token }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {

                return Json(new { Success = false, Message ="a: "+ (ex.InnerException !=null? ex.InnerException.Message: ex.Message) }, JsonRequestBehavior.DenyGet);
            }

        }
        private void GetPostAll(FacebookClient client, string path, FacebookPost LastPost)
        {
            string _path = path;
            bool isStop = false;
            string by = CurrentUser.FullName;
            string url = Request.Url.AbsoluteUri;
            long UserID = CurrentUser.ID;



            while (isStop == false)
            {
                dynamic feeds = client.Get(_path);
                if (feeds != null)
                {

                    var data = feeds["data"];
                    Facebook.JsonArray objData = data as dynamic;
                    var pagination = feeds["paging"];
                    FacebookPostResponse objModels = TranslateFromFBResponse(objData, pagination);

                    isStop = InsertPostToDB(objModels, LastPost, by, url, UserID);

                    if (objModels != null && objModels.pagination != null && !string.IsNullOrEmpty(objModels.pagination.next))
                    {
                        _path = objModels.pagination.next;
                    }
                    else
                    {
                        isStop = true;
                    }

                }
                else
                {
                    isStop = true;
                }
            }




        }
        private FacebookPostResponse TranslateFromFBResponse(Facebook.JsonArray objData, Facebook.JsonObject pagination)
        {
            FacebookPostResponse result = new FacebookPostResponse();
            result.pagination = new FacebookPagination();
            result.posts = new List<FacebookPostOriginal>();


            foreach (var item in objData)
            {
                FacebookPostOriginal model = new FacebookPostOriginal();
                if (((Facebook.JsonObject)(item)).ContainsKey("id") && ((Facebook.JsonObject)(item)).ContainsKey("message") && ((Facebook.JsonObject)(item)).ContainsKey("created_time"))
                {
                    model.id = ((Facebook.JsonObject)(item))["id"] as string;
                    model.message = ((Facebook.JsonObject)(item))["message"] as string;
                    model.created_time = ((Facebook.JsonObject)(item))["created_time"] as string;
                    result.posts.Add(model);
                }




            }


            if (pagination != null)
            {
                result.pagination.next = ((Facebook.JsonObject)pagination)["next"] as string;
                result.pagination.previous = ((Facebook.JsonObject)pagination)["previous"] as string;
            }
            return result;
        }
        private bool InsertPostToDB(FacebookPostResponse param, FacebookPost LastPost, string by, string url, long userID)
        {
            bool isStop = false;

            if (param != null && param.posts != null && param.posts.Count > 0)
            {
                foreach (var item in param.posts)
                {
                    var createdDate = Facebook.DateTimeConvertor.FromIso8601FormattedDateTime(item.created_time);
                    if (LastPost != null)
                    {
                        if (LastPost.CreatedDate >= createdDate)
                        {
                            isStop = true;
                            return isStop;
                        }
                    }


                    DateTime OldestDate = DateTime.Now.AddMonths(-1);
                    if (createdDate < OldestDate)
                    {
                        isStop = true;
                        return isStop;
                    }


                    FacebookPost post = new FacebookPost();
                    post.ID = Guid.NewGuid();
                    post.Content = item.message;
                    post.PostID = item.id;
                    post.CreatedDate = createdDate;
                    post.Insert(by, url, userID);

                }
            }
            else
            {
                isStop = true;
            }


            return isStop;

        }

        #endregion


        #region FB LIKE
        [HttpPost]
        public virtual ActionResult PopulateLikeFromFacebook(string token, Guid PostID)
        {
            try
            {
                var dataLikes = FacebookLike.GetByPostID(PostID).ToList();
                var dataPost = FacebookPost.GetByID(PostID);

                FacebookClient client = new FacebookClient(token);
                client.AppId = Settings.GetValueByKey(SiteSetting.FacebookApplicationID);// "275134142859426";
                client.AppSecret = Settings.GetValueByKey(SiteSetting.FacebookApplicationSecret);//"fdfca89b956b7476b9a5290cde141aa9";
                string path = string.Format("{0}/likes", dataPost.PostID);
                GetLikeAll(client, path, dataLikes, PostID);
                return Json(new { Success = true, Message = "success synch: " + token }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {

                return Json(new { Success = false, Message = "a: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message) }, JsonRequestBehavior.DenyGet);
            }
        }

        private void GetLikeAll(FacebookClient client, string path, List<FacebookLike> likes, Guid PostId)
        {
            string _path = path;
            bool isStop = false;
            string by = CurrentUser.FullName;
            string url = Request.Url.AbsoluteUri;
            long UserID = CurrentUser.ID;
            while (isStop == false)
            {
                dynamic feeds = client.Get(_path);
                if (feeds != null)
                {
                    var data = feeds["data"];
                    Facebook.JsonArray objData = data as dynamic;
                    var pagination = feeds["paging"];
                    FacebookLikeResponse objModels = TranslateLIKEFromFBResponse(objData, pagination);
                    isStop = InsertLikeToDB(objModels, by, url, UserID, likes, PostId);
                    if (objModels != null && objModels.pagination != null && !string.IsNullOrEmpty(objModels.pagination.next))
                    {
                        _path = objModels.pagination.next;
                    }
                    else
                    {
                        isStop = true;
                    }
                }
                else
                {
                    isStop = true;
                }
            }
        }
        private FacebookLikeResponse TranslateLIKEFromFBResponse(Facebook.JsonArray objData, Facebook.JsonObject pagination)
        {
            FacebookLikeResponse result = new FacebookLikeResponse();
            result.pagination = new FacebookPagination();
            result.data = new List<FacebookMemberModel>();


            foreach (var item in objData)
            {
                FacebookMemberModel model = new FacebookMemberModel();
                if (((Facebook.JsonObject)(item)).ContainsKey("id") && ((Facebook.JsonObject)(item)).ContainsKey("name"))
                {
                    model.id = ((Facebook.JsonObject)(item))["id"] as string;
                    model.name = ((Facebook.JsonObject)(item))["name"] as string;
                    result.data.Add(model);
                }
            }


            if (pagination != null)
            {
                if (((Facebook.JsonObject)(pagination)).ContainsKey("next"))
                {
                    result.pagination.next = ((Facebook.JsonObject)pagination)["next"] as string;
                }
                if (((Facebook.JsonObject)(pagination)).ContainsKey("previous"))
                {
                    result.pagination.previous = ((Facebook.JsonObject)pagination)["previous"] as string;
                }

            }
            return result;
        }
        private bool InsertLikeToDB(FacebookLikeResponse param, string by, string url, long userID, List<FacebookLike> likes, Guid PostID)
        {
            bool isStop = false;

            if (param != null && param.data != null && param.data.Count > 0)
            {
                foreach (var item in param.data)
                {
                    var dataMember = DataMember.Member.GetByFacebookId(item.id);
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        FacebookLike likeRecord = null;
                        if (likes != null && likes.Count > 0)
                        {
                            likeRecord = likes.FirstOrDefault(x => x.FacebookUserID == item.id);
                        }

                        bool isupdate = false;
                        bool IsDataUpdated = false;
                        if (likeRecord == null)
                        {
                            //insert
                            likeRecord = new FacebookLike();
                            likeRecord.ID = Guid.NewGuid();
                            likeRecord.FacebookPostID = PostID;
                            likeRecord.FacebookUserID = item.id;

                        }
                        else
                        {
                            //udpdate
                            isupdate = true;

                        }

                        bool IsUpdateMemberPoint = false;

                        bool IsLogUPdate = false;
                        if (!likeRecord.MemberID.HasValue && dataMember != null)
                        {

                            likeRecord.MemberID = dataMember.ID;
                            if (isupdate)
                            {
                                IsDataUpdated = true;
                            }

                        }

                        if (!likeRecord.CollectID.HasValue && dataMember != null)
                        {
                            var resultCollect = CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.FBLIKE, dataMember, url);
                            if (resultCollect != null && resultCollect.StatusCode == "00" && resultCollect.Value != null)
                            {
                                if (isupdate)
                                {
                                    IsDataUpdated = true;
                                   
                                }
                                IsUpdateMemberPoint = true;
                                likeRecord.CollectID = resultCollect.Value.ID;
                            }
                            else
                            {
                                IsLogUPdate = true;
                            }
                        }




                        if (isupdate)
                        {
                            if (IsDataUpdated)
                            {
                                likeRecord.Updated(by, url, userID);
                                transScope.Complete();
                            }
                            else
                            {
                                if (IsLogUPdate)
                                {
                                    transScope.Complete();
                                }

                            }

                        }
                        else
                        {
                            likeRecord.Inserted(by, url, userID);
                            transScope.Complete();
                        }


                        transScope.Dispose();

                        if(dataMember != null && IsUpdateMemberPoint)
                        {
                            PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(dataMember.ID, by);
                        }

                    }





                }
            }
            else
            {
                isStop = true;
            }


            return isStop;

        }


        [HttpPost]
        public virtual ActionResult PopulateLikeAndCommentFromFacebook(string token, Guid PostID)
        {
            var dataLikes = FacebookLike.GetByPostID(PostID).ToList();
            var dataPost = FacebookPost.GetByID(PostID);

            FacebookClient client = new FacebookClient(token);
            client.AppId = Settings.GetValueByKey(SiteSetting.FacebookApplicationID);// "275134142859426";
            client.AppSecret = Settings.GetValueByKey(SiteSetting.FacebookApplicationSecret);//"fdfca89b956b7476b9a5290cde141aa9";
            string pathLike = string.Format("{0}/likes", dataPost.PostID);
            GetLikeAll(client, pathLike, dataLikes, PostID);

            string pathComment = string.Format("{0}/comments", dataPost.PostID);
            var dataComments = FacebookComment.GetByPostID(PostID).ToList();
            GetCommentAll(client, pathComment, dataComments, PostID);

            return Json(new { Success = true, Message = "success synch: " + token }, JsonRequestBehavior.DenyGet);
        }

        #endregion

        #region FB COMMENT
        [HttpPost]
        public virtual ActionResult PopulateCommentFromFacebook(string token, Guid PostID)
        {
            try
            {
                var dataComments = FacebookComment.GetByPostID(PostID).ToList();
                var dataPost = FacebookPost.GetByID(PostID);

                FacebookClient client = new FacebookClient(token);
                client.AppId = Settings.GetValueByKey(SiteSetting.FacebookApplicationID);// "275134142859426";
                client.AppSecret = Settings.GetValueByKey(SiteSetting.FacebookApplicationSecret);//"fdfca89b956b7476b9a5290cde141aa9";
                string path = string.Format("{0}/comments", dataPost.PostID);
                GetCommentAll(client, path, dataComments, PostID);
                return Json(new { Success = true, Message = "success synch: " + token }, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {

                return Json(new { Success = false, Message = "a: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message) }, JsonRequestBehavior.DenyGet);
            }
        }

        private void GetCommentAll(FacebookClient client, string path, List<FacebookComment> comments, Guid PostId)
        {
            string _path = path;
            bool isStop = false;
            string by = CurrentUser.FullName;
            string url = Request.Url.AbsoluteUri;
            long UserID = CurrentUser.ID;
            while (isStop == false)
            {
                dynamic feeds = client.Get(_path);
                if (feeds != null)
                {
                    var data = feeds["data"];
                    Facebook.JsonArray objData = data as dynamic;
                    var pagination = feeds["paging"];
                    FacebookCommentResponse objModels = TranslateCommentFromFBResponse(objData, pagination);
                    isStop = InsertCommentToDB(objModels, by, url, UserID, comments, PostId);
                    if (objModels != null && objModels.pagination != null && !string.IsNullOrEmpty(objModels.pagination.next))
                    {
                        _path = objModels.pagination.next;
                    }
                    else
                    {
                        isStop = true;
                    }
                }
                else
                {
                    isStop = true;
                }
            }
        }
        private FacebookCommentResponse TranslateCommentFromFBResponse(Facebook.JsonArray objData, Facebook.JsonObject pagination)
        {
            FacebookCommentResponse result = new FacebookCommentResponse();
            result.pagination = new FacebookPagination();
            result.data = new List<FacebookCommentOriginal>();
            foreach (var item in objData)
            {
                FacebookCommentOriginal model = new FacebookCommentOriginal();
                if (((Facebook.JsonObject)(item)).ContainsKey("id") && ((Facebook.JsonObject)(item)).ContainsKey("created_time") &&
                    ((Facebook.JsonObject)(item)).ContainsKey("message") && ((Facebook.JsonObject)(item)).ContainsKey("from"))
                {
                    model.id = ((Facebook.JsonObject)(item))["id"] as string;
                    model.message = ((Facebook.JsonObject)(item))["message"] as string;
                    model.created_time = ((Facebook.JsonObject)(item))["created_time"] as string;

                    var dataDynmaic = item as dynamic;
                    var fromObj = dataDynmaic["from"];
                    if (fromObj != null)
                    {
                        if (((Facebook.JsonObject)(fromObj)).ContainsKey("id") && ((Facebook.JsonObject)(fromObj)).ContainsKey("name"))
                        {
                            model.from = new FacebookMemberModel();
                            model.from.id = ((Facebook.JsonObject)(fromObj))["id"] as string;
                            model.from.name = ((Facebook.JsonObject)(fromObj))["name"] as string;
                        }
                    }

                    result.data.Add(model);
                }
            }


            if (pagination != null)
            {
                if (((Facebook.JsonObject)(pagination)).ContainsKey("next"))
                {
                    result.pagination.next = ((Facebook.JsonObject)pagination)["next"] as string;
                }
                if (((Facebook.JsonObject)(pagination)).ContainsKey("previous"))
                {
                    result.pagination.previous = ((Facebook.JsonObject)pagination)["previous"] as string;
                }

            }
            return result;
        }
        private bool InsertCommentToDB(FacebookCommentResponse param, string by, string url, long userID, List<FacebookComment> comments, Guid PostID)
        {
            bool isStop = false;

            if (param != null && param.data != null && param.data.Count > 0)
            {
                foreach (var item in param.data)
                {
                    var dataMember = DataMember.Member.GetByFacebookId(item.from.id);

                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        FacebookComment commentRecord = null;
                        if (comments != null && comments.Count > 0)
                        {
                            commentRecord = comments.FirstOrDefault(x => x.CommentID == item.id);
                        }


                        bool isupdate = false;
                        bool IsDataUpdated = false;
                        if (commentRecord == null)
                        {
                            var createdDate = Facebook.DateTimeConvertor.FromIso8601FormattedDateTime(item.created_time).ToLocalTime();

                            //insert
                            commentRecord = new FacebookComment();
                            commentRecord.ID = Guid.NewGuid();
                            commentRecord.FacebookPostID = PostID;
                            commentRecord.FacebookUserID = item.from.id;
                            commentRecord.CommentID = item.id;
                            commentRecord.Comment = item.message;
                            commentRecord.CreatedDate = createdDate;
                        }
                        else
                        {
                            //udpdate
                            isupdate = true;

                        }


                        bool IsLogUPdate = false;
                        bool IsUpdateMemberPoint = false;
                        if (!commentRecord.MemberID.HasValue && dataMember != null)
                        {

                            commentRecord.MemberID = dataMember.ID;
                            if (isupdate)
                            {
                                IsDataUpdated = true;
                            }
                          
                        }

                        if(!commentRecord.CollectID.HasValue && dataMember != null)
                        {
                            var resultCollect = CollectLogic.InsertPointByActionCode(Common.Point.ActionType.Code.FBCOMMENT, dataMember, url);
                            if (resultCollect != null && resultCollect.StatusCode == "00" && resultCollect.Value != null)
                            {
                                if (isupdate)
                                {
                                    IsDataUpdated = true;
                                   
                                }

                                IsUpdateMemberPoint = true;
                                commentRecord.CollectID = resultCollect.Value.ID;
                            }
                            else
                            {
                                IsLogUPdate = true;
                            }
                        }
                       




                        if (isupdate)
                        {
                            if (IsDataUpdated)
                            {
                                commentRecord.Updated(by, url, userID);
                                transScope.Complete();
                            }
                            else
                            {
                                if (IsLogUPdate)
                                {
                                    transScope.Complete();
                                }
                                
                            }

                        }
                        else
                        {
                            commentRecord.Inserted(by, url, userID);
                            transScope.Complete();
                        }

                        transScope.Dispose();


                        if (dataMember != null && IsUpdateMemberPoint)
                        {
                            PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(dataMember.ID, by);
                        }


                    }
                }
            }
            else
            {
                isStop = true;
            }


            return isStop;

        }


        #endregion
    }
}