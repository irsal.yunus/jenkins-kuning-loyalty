﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Settings;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class SettingsController : BaseController
    {
        // GET: Settings
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Settings")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Setting successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Setting successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Setting successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Settings")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            SettingsModel model = new SettingsModel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(SettingsModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {

                Settings Settings = new Settings();
                Settings.InjectFrom(model);
                var result = Settings.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Settings.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        private void ValidateAddModel(SettingsModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }

            if (string.IsNullOrEmpty(model.Key))
            {
                ModelState.AddModelError("Key", "key is required");
            }
            else
            {
                var checkData = Data.Settings.GetByKey(model.Key);
                if (checkData != null)
                {
                    ModelState.AddModelError("Key", "Key is already registered");
                }
            }

            if (string.IsNullOrEmpty(model.Value))
            {
                ModelState.AddModelError("Value", "Value is required");
            }
        }

        private void ValidateEditModel(SettingsModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }

            if (string.IsNullOrEmpty(model.Key))
            {
                ModelState.AddModelError("Key", "key is required");
            }
            else
            {
                var checkData = Data.Settings.GetByKeyNotSelf(model.Key, model.ID);
                if (checkData != null)
                {
                    ModelState.AddModelError("Key", "Key is already registered");
                }
            }

            if (string.IsNullOrEmpty(model.Value))
            {
                ModelState.AddModelError("Value", "Value is required");
            }
        }
        [BreadCrumb(Label = "Edit Settings")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int SettingsId)
        {
            Settings currentSettings = LoyaltyPointEngine.Data.Settings.GetById(SettingsId);
            SettingsModel model = new SettingsModel();
            model.InjectFrom(currentSettings);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(SettingsModel model)
        {
            ValidateEditModel(model);
            Settings Settings = LoyaltyPointEngine.Data.Settings.GetById(model.ID);
            if (Settings == null)
            {
                ModelState.AddModelError("ID", "ID is not registered in system");
            }


            if (ModelState.IsValid)
            {
                Settings.InjectFrom(model);
                var result = Settings.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Settings.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Settings Settings = Settings.GetById(ID);
            Settings.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Settings.Index("Delete_Success"));
        }

        #region List Settings Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllSettings()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Settings> allSettings = LoyaltyPointEngine.Data.Settings.GetAll().OrderBy(x => x.Key);
            AjaxGrid<Settings> grid = this.gridMvcHelper.GetAjaxGrid(allSettings);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllSettingsPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Settings> allData = LoyaltyPointEngine.Data.Settings.GetAll().OrderBy(x => x.Key);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Key.Contains(txtSearch)).OrderBy(x => x.Key);
            }
            AjaxGrid<Settings> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Settings.Views.GridGetAllSettings, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}