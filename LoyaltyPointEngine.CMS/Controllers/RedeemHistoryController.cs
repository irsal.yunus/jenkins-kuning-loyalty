﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System.Globalization;
using LoyaltyPointEngine.Model.Object.Redeem;
using System.Data.Objects;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RedeemHistoryController : BaseController
    {
        // GET: RedeemHistory
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Label = "Redeem History")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Redeem successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Redeem successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Redeem successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }
        [BreadCrumb(Label = "View History")]
        public virtual ActionResult History(int MemberID, string StartDate, string EndDate)
        {
            DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime endDate = startDate.AddMonths(1).AddDays(-1);

            if (!string.IsNullOrEmpty(StartDate))
            {
                DateTime.TryParseExact(StartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out startDate);
            
            }

            if (!string.IsNullOrEmpty(EndDate))
            {
                DateTime.TryParseExact(EndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDate);

            }

            var datas = from a in Redeem.GetByMemberID(MemberID)
                        join b in Data.BalanceLog.GetByMemberId(MemberID) on a.ID equals b.ReferenceID
                        where EntityFunctions.TruncateTime( a.TransactionDate) >= EntityFunctions.TruncateTime(startDate)
                            && EntityFunctions.TruncateTime(a.TransactionDate) <= EntityFunctions.TruncateTime(endDate)
                            && b.Type == "D"
                        select new PointHistoryModel
                       {
                           TransactionDate = a.TransactionDate,
                           TransactionCode = a.TransactionCode ?? string.Empty,
                           TransactionType = b.Type =="D"? "Debit":"Credit",
                           Point = a.Point,
                           IsVoid = a.IsVoid,
                           Balance = b.Balance
                       };


            ViewBag.Member = Data.Member.GetById(MemberID);
            ViewBag.StartDate = startDate.ToString("dd/MM/yyyy") ;
            ViewBag.EndDate = endDate.ToString("dd/MM/yyyy");
            return View(datas.OrderByDescending(x=> x.TransactionDate));
        }
    }
}