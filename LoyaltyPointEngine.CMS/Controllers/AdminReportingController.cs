﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Helper;
using System.Globalization;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class AdminReportingController : BaseController
    {
        //
        // GET: /AdminReporting/
        [BreadCrumb(Clear = true, Label = "Reporting - Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index()
        {
            DateTime? startdate = null;
            try
            {
                startdate = DateTime.ParseExact(Request.Form["tglAwal"], "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None);
            }
            catch { }
            DateTime? enddate = null;
            try
            {
                enddate = DateTime.ParseExact(Request.Form["tglAkhir"], "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None);
            }
            catch { }
            var data = LoyaltyPointEngine.Data.ReportAdmin.getReportAdmin(startdate,enddate);
            List<LoyaltyPointEngine.Model.Object.CMSUser.UserModel> model = new List<LoyaltyPointEngine.Model.Object.CMSUser.UserModel>();
                foreach(var _data in data)
                {
                    LoyaltyPointEngine.Model.Object.CMSUser.UserModel _model = new LoyaltyPointEngine.Model.Object.CMSUser.UserModel();
                    _model.FullName = _data.createdby;
                    _model.Approved = (int) _data.approved;
                    _model.Reject = (int) _data.reject;
                    model.Add(_model);
                }

                return View(model);
        }

	}
}