﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CityController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "City")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "City successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "City successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "City successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add City")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            CityModel model = new CityModel();
            GetListArea();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(CityModel model)
        {
            this.IsValidCity(model, false);
            if (ModelState.IsValid)
            {

                City city = new City();
                city.InjectFrom(model);
                var result = city.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.City.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListArea();
            return View(model);
        }

        [BreadCrumb(Label = "Edit City")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int CityId)
        {
            City currentCity = LoyaltyPointEngine.Data.City.GetById(CityId);
            CityModel model = new CityModel();
            model.InjectFrom(currentCity);
            GetListArea();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(CityModel model)
        {
            this.IsValidCity(model, true);
            if (ModelState.IsValid)
            {
                City city = LoyaltyPointEngine.Data.City.GetById(model.ID);
                city.InjectFrom(model);
                var result = city.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.City.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListArea();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            City city = City.GetById(ID);
            city.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.City.Index("Delete_Success"));
        }

        private void IsValidCity(CityModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.Code))
            {
                Data.City predictedData = Data.City.GetByCode(Model.Code);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("City Code '{0}'  has been used by other City", Model.Code));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("City Code  '{0}'  has been used by other City", Model.Code));
            }
        }
        private void GetListArea()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Area> listdata = LoyaltyPointEngine.Data.Area.GetAll();
            foreach (Area data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListArea = items;
        }

        #region List City Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllCity()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<City> allCity = LoyaltyPointEngine.Data.City.GetAll().OrderBy(x => x.Name);
            AjaxGrid<City> grid = this.gridMvcHelper.GetAjaxGrid(allCity);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllCityPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<City> allData = LoyaltyPointEngine.Data.City.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<City> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.City.Views.GridGetAllCity, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}