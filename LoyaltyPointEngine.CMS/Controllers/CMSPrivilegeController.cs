﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.CMSNavigation;
using LoyaltyPointEngine.CMS.Helper;

namespace LoyaltyPointEngine.CMS.Controllers    
{
    public partial class CMSPrivilegeController : BaseController
    {
        // GET: CMSPrivilege
        private IGridMvcHelper gridMvcHelper;
        //
        // GET: /CMSNavigation/
        [BreadCrumb(Clear = true, Label = "Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Privilege successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Privilege successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Privilege successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            PrivilegeModel model = new PrivilegeModel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(PrivilegeModel model)
        {
            if (ModelState.IsValid)
            {

                CMSPrivilege Privilege = new CMSPrivilege();
                Privilege.Name = model.Name;
                var result = Privilege.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.CMSPrivilege.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View();
        }

        [BreadCrumb(Label = "Edit Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(long PrivilegeId)
        {
            CMSPrivilege currentPrivilege = CMSPrivilege.GetByID(PrivilegeId);
            PrivilegeModel model = new PrivilegeModel();
            model.ID = currentPrivilege.ID;
            model.Name = currentPrivilege.Name;
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(PrivilegeModel model)
        {
            if (ModelState.IsValid)
            {
                CMSPrivilege Privilege = CMSPrivilege.GetByID(model.ID);
                Privilege.Name = model.Name;
                var result = Privilege.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.CMSPrivilege.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View();
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(long ID)
        {
            CMSPrivilege Privilege = CMSPrivilege.GetByID(ID);
            Privilege.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.CMSPrivilege.Index("Delete_Success"));
        }


        #region List Privilege Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllPrivilege()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSPrivilege> allPrivilege = CMSPrivilege.GetAll().OrderBy(x => x.Name);
            AjaxGrid<CMSPrivilege> grid = this.gridMvcHelper.GetAjaxGrid(allPrivilege);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllPrivilegePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSPrivilege> allData = CMSPrivilege.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<CMSPrivilege> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.CMSPrivilege.Views.GridGetAllPrivilege, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}