﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Model.Parameter.Uptrade;
using LoyaltyPointEngine.PointLogicLayer;
using MvcBreadCrumbs;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class FlagUptradeController : BaseController
    {
        // GET: FlagUptrade Manage
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Manage Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Uptrade successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Uptrade successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Uptrade successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Clear = true, Label = "Edit Manage Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Edit(int ID)
        {
            FlagUptradeManage model = FlagUptradeManage.GetById(ID);
            if (model == null)
            {
                return RedirectToAction(MVC.UptradeManage.Index());
            }

            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(FlagUptradeManage param)
        {
            ValidateEditModel(param);
            if (ModelState.IsValid)
            {
                var model = FlagUptradeManage.GetById(param.ID);
                model.StatusFlag = param.StatusFlag;
                model.Code = param.Code;
                model.Update(CurrentUser.FullName);
                return RedirectToAction(MVC.FlagUptrade.Index("Edit_Success"));
            }
            return View(param);
        }

        [BreadCrumb(Clear = true, Label = "Add Manage Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Add(FlagUptradeManage param)
        {
            ValidateAddModel(param);
            if (ModelState.IsValid)
            {
                param.Insert(CurrentUser.FullName);
                return RedirectToAction(MVC.FlagUptrade.Index("Create_Success"));
            }
            return View(param);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            FlagUptradeManage data = FlagUptradeManage.GetById(ID);
            data.Delete(CurrentUser.FullName);
            return RedirectToAction(MVC.FlagUptrade.Index("Delete_Success"));
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllFlagUptradeManage()
        {
            gridMvcHelper = new GridMvcHelper();
            ParamUptradeManageModel param = new ParamUptradeManageModel();
            IOrderedQueryable<FlagUptradeManage> allUptradeManage = FlagUptradeManage.GetAll().OrderByDescending(x => x.CreatedDate);
            AjaxGrid<FlagUptradeManage> grid = this.gridMvcHelper.GetAjaxGrid(allUptradeManage);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllFlagUptradeManagePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            ParamUptradeManageModel param = new ParamUptradeManageModel();
            IOrderedQueryable<FlagUptradeManage> allData = FlagUptradeManage.GetAll().OrderByDescending(x => x.CreatedDate);

            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Code.Contains(txtSearch) || x.StatusFlag.Contains(txtSearch)).OrderBy(x => x.CreatedDate);
            }
            AjaxGrid<FlagUptradeManage> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.FlagUptrade.Views.GridGetAllFlagUptradeManage, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private void ValidateAddModel(FlagUptradeManage model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }

            if (string.IsNullOrEmpty(model.StatusFlag))
            {
                ModelState.AddModelError("Status", "Status is required");
            }
            else
            {
                var checkData = FlagUptradeManage.GetByCode(model.Code);
                if (checkData != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered");
                }
            }

        }

        private void ValidateEditModel(FlagUptradeManage model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }

            if (string.IsNullOrEmpty(model.StatusFlag))
            {
                ModelState.AddModelError("Status", "Status is required");
            }
            else
            {
                var checkData = FlagUptradeManage.GetByCodeNotSelf(model.Code, model.ID);
                if (checkData != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered");
                }
            }

        }
    }
}