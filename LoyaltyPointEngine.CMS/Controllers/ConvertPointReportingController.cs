﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.Common.Extension;
using MvcBreadCrumbs;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Model.Object.ConvertPoint;
using System.Globalization;
using System.IO;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ConvertPointReportingController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Reporting - Convert Point")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Convert Point")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string startDate, string endDate)
        {
            DateTime? _startDate = null, _endDate = null;
            DateTime tempDate = DateTime.Today;
            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _startDate = tempDate;
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/1901", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _endDate = tempDate;
            }
            else
            {
                _endDate = DateTime.ParseExact("31/12/2199", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            var datas = DataRepositoryFactory.CurrentRepository.GetConvertPointReport(_startDate, _endDate, null, null, null, null).AsEnumerable();
            var model = GetConvertPointModel(datas);

            Type type = typeof(ConvertPointReportModel);
            List<string> excelHeader = new List<string>();
            foreach (var prop in type.GetProperties())
            {
                excelHeader.Add(prop.Name);
            }
            List<List<string>> excelRows = new List<List<string>>();
            foreach (var data in model)
            {
                List<string> row = new List<string>();
                foreach (var prop in type.GetProperties())
                {
                    object value = prop.GetValue(data);
                    row.Add(value != null ? value.ToString() : "");
                }
                excelRows.Add(row);
            }

            string fileName = string.Format("ConvertPointReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            MemoryStream excelStream = ExcelHelper.ExportToExcelNewVersion(excelRows, excelHeader);
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAll()
        {
            var data = DataRepositoryFactory.CurrentRepository.GetConvertPointReport(null, null, 1, 5, null, null).AsEnumerable();
            return PartialView();
        }

        [HttpGet]
        public virtual ActionResult GridGetAllPager(string startDate, string endDate, string orderColumn, string orderDirection, int page)
        {
            orderColumn = orderColumn ?? "a.TransactionDate";
            orderDirection = orderDirection ?? "DESC";
            //orderDirection = orderDirection.ToUpper() == "DESC" ? "ASC" : "DESC";
            page = (page <= 0 ? 1 : page);

            int pageSize = 5,
                totalRow = 0;

            DateTime? _startDate = null, _endDate = null;
            DateTime tempDate = DateTime.Today;
            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _startDate = tempDate;
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/1901", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _endDate = tempDate;
            }
            else
            {
                _endDate = DateTime.ParseExact("31/12/2199", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.CurrentPage = page;
            ViewBag.OrderColumn = orderColumn;
            ViewBag.OrderDirection = orderDirection;
            ViewBag.PageSize = pageSize;
            totalRow = DataRepositoryFactory.CurrentRepository.GetConvertPointReportTotalRow(_startDate, _endDate)
                .FirstOrDefault()
                .GetValueOrDefault(0);
            ViewBag.TotalRow = totalRow;
            ViewBag.TotalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            var data = DataRepositoryFactory.CurrentRepository.GetConvertPointReport(_startDate, _endDate, page, pageSize, orderColumn, orderDirection).AsEnumerable();
            var model = GetConvertPointModel(data);
            return PartialView("GridGetAll", model);
        }

        private static List<ConvertPointReportModel> GetConvertPointModel(IEnumerable<GetConvertPointReport_Result> report)
        {
            var data = from a in report
                       select new ConvertPointReportModel
                       {
                           MemberName = a.MemberName,
                           MemberPhone = a.MemberPhone,
                           PontaId = a.PontaId,
                           TransactionDate = a.TransactionDate.ToString("dd/MM/yyyy hh:mm:ss"),
                           PontaPoints = a.StorePoint.ToString(),
                           RewardPoints = a.Points.ToString(),
                           Balance = a.Balance.ToString(),
                           Type = a.Type,
                           Status = a.Status.HasValue && a.Status.Value == 1 ? "Approved" : "Rejected" 
                       };
            return data.ToList();
        }

        #endregion
    }
}