﻿using CsvHelper;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.ElasticSearch;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Model.Object.Receipt;
using MvcBreadCrumbs;
using Nest;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ReceiptReportingController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Reporting - Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            ViewBag.ChannelList = EnumHelper.ToSelectList<Channel>();
            var spg = DataMember.SPG.GetAll();
            SelectList spgList = new SelectList(spg, "Code", "Code");
            ViewBag.SPGCodeList = spgList.ToList();
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string tglAwal, string tglAkhir, Channel? channel, string spgCode)
        {
            DateTime _from, _to;
            DateTime? from = null, to = null;

            if (!string.IsNullOrEmpty(tglAwal))
            {
                if (DateTime.TryParseExact(tglAwal + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out _from))
                    from = _from;
            }
            else
            {
                DateTime.TryParseExact(DateTime.Now.ToString("dd/MM/yyyy") + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out _from);
                from = _from;
            }

            if (!string.IsNullOrEmpty(tglAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAkhir + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out _to))
                    to = _to;
            }
            else
            {
                DateTime.TryParseExact(DateTime.Now.ToString("dd/MM/yyyy") + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out _to);
                to = _to;
            }

            //var datas = GetAllReceiptModel(from, to, channel, spgCode);
            string channelString = channel.HasValue ? channel.ToString() : "";
            //var datas = DataRepositoryFactory.CurrentRepository.GetReceiptReportDownload(from, to, channelString, spgCode);
            var datas = GetReceiptElastic(from.Value, to.Value);

            if (channel.HasValue)
            {
                datas = datas.Where(x => !string.IsNullOrEmpty(x.Channel) && x.Channel.Equals(channelString)).ToList();
            }

            if(!string.IsNullOrEmpty(spgCode))
            {
                datas = datas.Where(x => !string.IsNullOrEmpty(x.SPG) && x.SPG.Equals(spgCode)).ToList();
            }

            datas = datas.OrderByDescending(x => x.CreatedDate).ToList();

            foreach (var item in datas)
            {
                if (!string.IsNullOrEmpty(item.Status) && item.Status.ToLower() == "unverified")
                {
                    datas.Remove(item);
                    continue;
                }
                item.CreatedDate = item.CreatedDate.ToLocalTime();
                item.TransactionDate = item.TransactionDate.ToLocalTime();
                item.UpdatedDate = item.UpdatedDate.HasValue ? item.UpdatedDate.Value.ToLocalTime() : item.UpdatedDate;
                item.RequestedDate = item.RequestedDate.HasValue ? item.RequestedDate.Value.ToLocalTime() : item.RequestedDate;
            }
            
            //write ke file csv
            string filename = string.Format("ReceiptReport_{0}.csv", DateTime.Now.ToString("ddMMyyyy"));
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            var csv = new CsvWriter(writer);
            csv.Configuration.Delimiter = ";";
            csv.Configuration.RegisterClassMap<ReceiptReportingElasticCsvMapModel>();
            csv.WriteRecords(datas);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", filename);
        }

        //private List<List<string>> GetReceiptReporting(List<ReceiptReportingModel> param)
        private List<List<string>> GetReceiptReporting(IQueryable<ReceiptReportingModel> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                var itemPrice = item.Price.ToString();
                data.Add(item.MemberName);
                data.Add(item.MemberPhone);
                data.Add(item.MemberEmail);
                data.Add(item.Code);
                data.Add(item.ReceiptCode);
                data.Add(item.Channel);
                data.Add(item.SPG);
                data.Add(item.TransactionDateDate.HasValue ? item.TransactionDateDate.Value.ToString("dd/MM/yyyy  HH:mm:ss") : "");
                data.Add(item.Product);
                data.Add(item.Quantity.ToString());
                data.Add(item.RetailerName);
                data.Add(item.RetailerAddress);
                string status = Enum.GetName(typeof(Receipt.ReceiptStatus), item.Status);
                data.Add(status);
                data.Add(item.Remarks);
                data.Add(item.Point.ToString());
                if (itemPrice != "0")
                {
                    data.Add(itemPrice);
                }
                else
                {
                    data.Add("");
                }

                data.Add(item.CodeGimmick);
                data.Add(item.CMSUserName);
                data.Add(item.CreatedDate.ToString("dd/MM/yyyy HH:mm:ss"));
                data.Add(item.UpdatedBy);
                data.Add(item.UpdatedDate.HasValue ? item.UpdatedDate.Value.ToString("dd/MM/yyyy  HH:mm:ss") : string.Empty);
                data.Add(item.RequestDate.HasValue ? item.RequestDate.Value.ToString("dd/MM/yyyy  HH:mm:ss") : string.Empty);
                data.Add(item.Reason);
                data.Add(item.ProductBefore);
                data.Add(item.ClusterName);
                datas.Add(data);
            }
            return datas;
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAll(DateTime? from, DateTime? to, Channel? channel, string spgCode)
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = GetAllReceiptModel(from, to, channel, spgCode);
            ViewBag.TotalDataRow = datas.Count();
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllPager(string hiddenAwal, string hiddenAkhir, Channel? hiddenChannel, string hiddenspgCode, int? page)
        {
            gridMvcHelper = new GridMvcHelper();

            DateTime _from, _to;
            DateTime? from = null, to = null;

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _from)) from = _from;
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _to)) to = _to;
            }

            var datas = GetAllReceiptModel(from, to, hiddenChannel, hiddenspgCode);
            if (hiddenChannel.HasValue)
            {
                var channelName = Enum.GetName(typeof(Channel), hiddenChannel.Value);
                datas = datas.Where(x => x.Channel == channelName);
            }

            ViewBag.TotalDataRow = datas.Count();
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.ReceiptReporting.Views.GridGetAll, this);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        private IQueryable<ReceiptReportingModel> GetAllReceiptModel(DateTime? from, DateTime? to, Channel? channel, string spgCode)
        {
            IQueryable<Collect> collects;
            IQueryable<Receipt> receipts = LoyaltyPointEngine.Data.Receipt.GetAllPerMonth();


            if (from.HasValue) receipts = receipts.Where(x => EntityFunctions.TruncateTime(from) <= EntityFunctions.TruncateTime(x.CreatedDate));
            if (to.HasValue)
            {
                to = to.Value.AddDays(1);
                receipts = receipts.Where(x => EntityFunctions.TruncateTime(to) > EntityFunctions.TruncateTime(x.CreatedDate));
            }

            collects = receipts.SelectMany(x => x.Collect).Where(x => x.ParentID == null & x.IsDeleted == false);
            var vchannel = channel.ToString();
            var view = viwReportReceipt.GetAll().OrderByDescending(x => x.CreatedDate);
            var data = from a in collects
                       join b in view
                       on a.MemberID equals b.ID into tempMember
                       from b in tempMember.DefaultIfEmpty()
                       join c in CMSUser.GetAll() on a.Receipt.CreatedByID equals c.ID into tempCMSUser
                       from c in tempCMSUser.DefaultIfEmpty()
                       select new ReceiptReportingModel
                       {
                           ID = a.ID,
                           ReceiptCode = (a.Receipt == null ? string.Empty : a.Receipt.ReceiptCode),
                           Code = (a.Receipt == null ? string.Empty : a.Receipt.Code),
                           TransactionDateDate = a.TransactionDate,
                           //RetailerID = (a.Receipt == null ? null : a.Receipt.RetailerID),
                           //RetailerName = (a.Receipt == null ? string.Empty : a.Receipt.Retailer.Name),
                           //RetailerAddress = (a.Receipt == null ? string.Empty : a.Receipt.RetailerAddress),
                           MemberName = b.Name,
                           MemberPhone = b.Phone,
                           MemberEmail = b.Email,
                           Channel = (a.Receipt == null ? string.Empty : a.Receipt.Channel),
                           SPG = b.Code,
                           Status = a.Receipt.Status,
                           Point = (a.Points * a.QuantityApproved),
                           Remarks = (a.Receipt == null ? string.Empty : a.Remarks),
                           Product = a.Action.Product.Name,
                           Quantity = a.QuantityApproved,
                           Price = a.ItemPrice,
                           CreatedBy = a.Receipt.CreatedBy,
                           CreatedByID = a.Receipt.CreatedByID,
                           CreatedDate = a.Receipt.CreatedDate,
                           UpdatedBy = a.Receipt.UpdatedBy ?? string.Empty,
                           UpdatedDate = a.Receipt.UpdatedDate,
                           RequestDate = a.Receipt.RequestDate,
                           CodeGimmick = a.Receipt.CodeGimmick,
                           CMSUserName = (c != null) ? c.Username : "",
                           IsDeleted = (a.IsDeleted ? "YES" : "NO"),
                           IsApproved = (a.IsApproved ? "YES" : "NO"),
                           Reason = receipts.Where(v => v.ID == a.ReceiptID).FirstOrDefault().RejectReason,
                           ProductBefore = receipts.Where(v => v.ID == a.ReceiptID).FirstOrDefault().ProductBefore,
                           //ClusterName = b.ClusterName

                       };

            if (vchannel != "")
            {
                data = data.Where(x => x.Channel == vchannel);
            }

            if (spgCode != "")
            {
                data = data.Where(x => x.CMSUserName == spgCode);
            }

            if (from.HasValue) data = data.Where(x => EntityFunctions.TruncateTime(from) <= EntityFunctions.TruncateTime(x.CreatedDate));
            if (to.HasValue)
            {
                to = to.Value.AddDays(1);
                data = data.Where(x => EntityFunctions.TruncateTime(to) > EntityFunctions.TruncateTime(x.CreatedDate));
            }
            return data;
        }

        //private IQueryable<ReceiptReportingModel> GetAllReceiptModel(DateTime? from, DateTime? to, Channel? channel, string spgCode)
        //{
        //    IQueryable<Collect> collects;
        //    IQueryable<Receipt> receipts = LoyaltyPointEngine.Data.Receipt.GetAll();


        //    if (from.HasValue) receipts = receipts.Where(x => EntityFunctions.TruncateTime(from) <= EntityFunctions.TruncateTime(x.CreatedDate));
        //    if (to.HasValue)
        //    {
        //        to = to.Value.AddDays(1);
        //        receipts = receipts.Where(x => EntityFunctions.TruncateTime(to) > EntityFunctions.TruncateTime(x.CreatedDate));
        //    }

        //    collects = receipts.SelectMany(x => x.Collect).Where(x => x.ParentID == null & x.IsDeleted == false);
        //    var vchannel = channel.ToString();
        //    var view = viwReportReceipt.GetAll().OrderByDescending(x => x.CreatedDate);
        //    var data = from a in collects
        //               join b in view
        //               on a.MemberID equals b.ID into tempMember
        //               from b in tempMember.DefaultIfEmpty()
        //               join c in CMSUser.GetAll() on a.Receipt.CreatedByID equals c.ID into tempCMSUser
        //               from c in tempCMSUser.DefaultIfEmpty()
        //               select new ReceiptReportingModel
        //               {
        //                   ID = a.ID,
        //                   ReceiptCode = (a.Receipt == null ? string.Empty : a.Receipt.ReceiptCode),
        //                   Code = (a.Receipt == null ? string.Empty : a.Receipt.Code),
        //                   TransactionDateDate = a.TransactionDate,
        //                   RetailerID = (a.Receipt == null ? null : a.Receipt.RetailerID),
        //                   RetailerName = (a.Receipt == null ? string.Empty : a.Receipt.Retailer.Name),
        //                   RetailerAddress = (a.Receipt == null ? string.Empty : a.Receipt.RetailerAddress),
        //                   MemberName = b.Name,
        //                   MemberPhone = b.Phone,
        //                   MemberEmail = b.Email,
        //                   Channel = (a.Receipt == null ? string.Empty : a.Receipt.Channel),
        //                   SPG = b.Code,
        //                   Status = a.Receipt.Status,
        //                   Point = (a.Points * a.QuantityApproved),
        //                   Remarks = (a.Receipt == null ? string.Empty : a.Remarks),
        //                   Product = a.Action.Product.Name,
        //                   Quantity = a.QuantityApproved,
        //                   Price = a.ItemPrice,
        //                   CreatedBy = a.Receipt.CreatedBy,
        //                   CreatedByID = a.Receipt.CreatedByID,
        //                   CreatedDate = a.Receipt.CreatedDate,
        //                   UpdatedBy = a.Receipt.UpdatedBy ?? string.Empty,
        //                   UpdatedDate = a.Receipt.UpdatedDate,
        //                   RequestDate = a.Receipt.RequestDate,
        //                   CodeGimmick = a.Receipt.CodeGimmick,
        //                   CMSUserName = (c != null) ? c.Username : "",
        //                   IsDeleted = (a.IsDeleted ? "YES" : "NO"),
        //                   IsApproved = (a.IsApproved ? "YES" : "NO"),
        //                   Reason = receipts.Where(v => v.ID == a.ReceiptID).FirstOrDefault().RejectReason,
        //                   ProductBefore = receipts.Where(v => v.ID == a.ReceiptID).FirstOrDefault().ProductBefore
        //               };

        //    if (vchannel != "")
        //    {
        //        data = data.Where(x => x.Channel == vchannel);
        //    }

        //    if (spgCode != "")
        //    {
        //        data = data.Where(x => x.CMSUserName == spgCode);
        //    }

        //    return data;
        //}

        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/ReceiptExcelFormat.xlsx"));
            string fileName = "ReceiptExcelFormat.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        private List<ReceiptReportingElasticModel> GetReceiptElastic(DateTime from, DateTime to)
        {
            ElasticConnection connection = new ElasticConnection();

            List<ReceiptReportingElasticModel> result = new List<ReceiptReportingElasticModel>();
            var documents = new ConcurrentBag<IReadOnlyCollection<ReceiptReportingElasticModel>>();

            var initialResponse = connection.Client.Search<ReceiptReportingElasticModel>(s => s
                .From(0)
                .Size(10000)
                .AllTypes()
                .Index(Common.SiteSetting.ElasticReceiptReportIndex)
                .Query(q => q
                    .Bool(bq => bq
                        .Filter(
                            f => f.DateRange(d => d.Field("createddate").GreaterThanOrEquals(from).LessThanOrEquals(to).TimeZone("+07:00")),
                            f => f.Term(t=>t.Field("isdeleted").Value(false))
                            )
                        )
                    )
                .Scroll("3s")
            );

            if (!initialResponse.IsValid || string.IsNullOrEmpty(initialResponse.ScrollId))
                throw new Exception(initialResponse.ServerError.Error.Reason);
            if (initialResponse.Documents.Any())
                result.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<ReceiptReportingElasticModel> loopingResponse = connection.Client.Scroll<ReceiptReportingElasticModel>("2m", scrollid);
                if (loopingResponse.IsValid)
                {
                    result.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            connection.Client.ClearScroll(new ClearScrollRequest(scrollid));
            
            return result;
        }
    }
}