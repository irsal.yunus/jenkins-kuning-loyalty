﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.CMSNavigation;
using LoyaltyPointEngine.CMS.Helper;
using System.Transactions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RoleController : BaseController
    {
        // GET: Role
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Role")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Role successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Role successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Role successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Role")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            RoleModel model = new RoleModel();
            GetNavigationAndPrevillage();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(RoleModel model)
        {
            if (ModelState.IsValid)
            {

                string CMSUserFullName = CurrentUser.FullName;
                long CMSUserID = CurrentUser.ID;
                  var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                  using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                  {
                      CMSRole role = new CMSRole();
                      role.Name = model.Name;
                      role.Level = model.Level;
                      role.Code = Guid.NewGuid();
                      var result = role.Insert(CMSUserFullName, Request.Url.AbsoluteUri, CMSUserID);
                      if (result.Success)
                      {
                          AddRoleNavigationMapping(model.NavigationPrevilage, role.ID, CMSUserFullName, CMSUserID);

                          transScope.Complete();
                          transScope.Dispose();

                          CMSUserInformation _CMSInformation = new CMSUserInformation();
                          SetRemoveCurrentNavigationCache();
                          _CMSInformation.SetRemoveCurrentAllowedControllerActionCache();

                       

                          return RedirectToAction(MVC.Role.Index("Create_Success"));
                      }
                      else
                      {
                          ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
                      }
                      transScope.Dispose();
                  }

            }

            GetNavigationAndPrevillage();
            return View(model);
        }

        [BreadCrumb(Label = "Edit Role")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(long RoleId)
        {
            CMSRole currentRole = CMSRole.GetByID(RoleId);
            RoleModel model = new RoleModel(RoleId);
            model.ID = currentRole.ID;
            model.Name = currentRole.Name;
            model.Level = currentRole.Level;
            GetNavigationAndPrevillage();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                string CMSUserFullName = CurrentUser.FullName;
                long CMSUserID = CurrentUser.ID;

                  var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                  using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                  {
                      CMSRole role = CMSRole.GetByID(model.ID);
                      role.Name = model.Name;
                      role.Level = model.Level;
                      var result = role.Update(CMSUserFullName, Request.Url.AbsoluteUri, CMSUserID);
                      if (result.Success)
                      {
                          EditRoleNavigationMapping(model.NavigationPrevilage, role.ID, CMSUserFullName, CMSUserID);

                          transScope.Complete();
                          transScope.Dispose();

                          CMSUserInformation _CMSInformation = new CMSUserInformation();
                          SetRemoveCurrentNavigationCache();
                          _CMSInformation.SetRemoveCurrentAllowedControllerActionCache();

                          return RedirectToAction(MVC.Role.Index("Edit_Success"));
                      }
                      else
                      {
                          ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
                      }
                      transScope.Dispose();

                  }
            }

            GetNavigationAndPrevillage();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(long ID)
        {
            CMSRole role = CMSRole.GetByID(ID);
            role.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Role.Index("Delete_Success"));
        }


        private void GetNavigationAndPrevillage()
        {
            ViewBag.CMSNavigation = CMSNavigation.GetAll().ToList();
            ViewBag.CMSNavigationPrevilage = CMSNavigationPrivilegeMapping.GetAll().Where(x => x.IsActive).ToList();
        }

        private void AddRoleNavigationMapping(long[] NavigationPrevilage, long RoleID, string UserFullName, long UserCmSID)
        {
            foreach (long NavigationPrevilageID in NavigationPrevilage)
            {
                CMSRoleNavigationPrivilegeMapping nav = new CMSRoleNavigationPrivilegeMapping();
                nav.CMSRoleID = RoleID;
                nav.CMSNavigationPrivilegeID = NavigationPrevilageID;
                nav.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            }
        }
        private void EditRoleNavigationMapping(long[] NavigationPrevilage, long RoleID, string UserFullName, long UserCmSID)
        {
            List<CMSRoleNavigationPrivilegeMapping> RoleNagivationPrevilage = CMSRoleNavigationPrivilegeMapping.GetByRole(RoleID).ToList();
            //set inactive first
            if (RoleNagivationPrevilage != null)
            {
                foreach (CMSRoleNavigationPrivilegeMapping _RoleNagivationPrevilage in RoleNagivationPrevilage)
                {
                    _RoleNagivationPrevilage.IsActive = false;
                    _RoleNagivationPrevilage.Update(UserFullName, Request.Url.AbsoluteUri, UserCmSID);

                }
            }

            foreach (long NavigationPrevilageID in NavigationPrevilage)
            {
                CMSRoleNavigationPrivilegeMapping nav = CMSRoleNavigationPrivilegeMapping.GetByRoleAndNavigationPrevilage(RoleID, NavigationPrevilageID);
                if (nav != null)
                {
                    nav.IsActive = true;
                    var result = nav.Update(UserFullName, Request.Url.AbsoluteUri, UserCmSID);
                   var a = result;
                }
                else
                {
                    CMSRoleNavigationPrivilegeMapping navnew = new CMSRoleNavigationPrivilegeMapping();
                    navnew.CMSRoleID = RoleID;
                    navnew.CMSNavigationPrivilegeID = NavigationPrevilageID;
                    navnew.Insert(UserFullName, Request.Url.AbsoluteUri, UserCmSID);
                }
            }

        }
        #region List Role Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllRole()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSRole> allRole = CMSRole.GetAll().OrderBy(x => x.Name);
            AjaxGrid<CMSRole> grid = this.gridMvcHelper.GetAjaxGrid(allRole);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllRolePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSRole> allData = CMSRole.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<CMSRole> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Role.Views.GridGetAllRole, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}