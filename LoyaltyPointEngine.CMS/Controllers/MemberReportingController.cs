﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LoyaltyPointEngine.DataMember;
using MvcBreadCrumbs;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using System.Globalization;
using System.IO;
using LoyaltyPointEngine.Common.ElasticSearch;
using Nest;
using CsvHelper;
using System.Collections.Concurrent;
using LoyaltyPointEngine.Model.Object.Member;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberReportingController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Reporting - Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            bool unikKonsumen = false;
            ViewBag.IsNotActive = unikKonsumen;
            GetChannelTypeList();
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string startDate, string endDate, int? channelId, bool? memberWithReceipt)
        {
            DateTime? _startDate = null, _endDate = null;
            DateTime tempDate = DateTime.Today;
            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _startDate = tempDate;
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/2016 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _endDate = tempDate;
            }
            else
            {
                _endDate = DateTime.Today.AddDays(1);
            }

            //var datas = DataRepositoryFactory.CurrentRepository.SP_GetGridMemberReporting(channelId, _startDate, _endDate, memberWithReceipt, null, null, null, null).AsEnumerable();
            var datas = GetMemberReportElastic(_startDate.Value, _endDate.Value);
            
            string channel = "";
            if (channelId.HasValue)
            {
                channel = Channel.GetById(channelId.Value).Name;
                datas = datas.Where(x => !string.IsNullOrEmpty(x.Channel) && x.Channel == channel).ToList();
            }

            foreach (var item in datas)
            {
                item.CreatedDate = item.CreatedDate.ToLocalTime();
                item.ChildBirthDate = item.ChildBirthDate.HasValue ? item.ChildBirthDate.Value.ToLocalTime() : (DateTime?)null;
                item.IsActiveString = item.IsActive ? "Yes" : "No";
            }

            string filename = string.Format("MemberReport_{0}.csv", DateTime.Now.ToString("ddMMyyyy"));
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            var csv = new CsvWriter(writer);
            csv.Configuration.Delimiter = ";";
            csv.Configuration.RegisterClassMap<MemberReportElasticCsvMapModel>();
            csv.WriteRecords(datas);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", filename);
            //Type type = typeof(SP_GetGridMemberReporting_Result);
            //List<string> excelHeader = new List<string>();
            //foreach (var prop in type.GetProperties())
            //            {
            //    excelHeader.Add(prop.Name);
            //    }
            //List<List<string>> excelRows = new List<List<string>>();
            //foreach (var data in datas)
            //{
            //    List<string> row = new List<string>();
            //    foreach (var prop in type.GetProperties())
            //    {
            //        string valueStr = "";
            //        object valueObj = prop.GetValue(data);
            //        if (valueObj != null)
            //        {
            //            if (prop.PropertyType == typeof(DateTime?))
            //            {
            //                valueStr = ((DateTime)valueObj).ToString("dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
            //            }
            //            else
            //            {
            //                valueStr = valueObj.ToString();
            //            }
            //        }
            //        row.Add(valueStr);
            //    }
            //    excelRows.Add(row);
            //}

            //string fileName = string.Format("MemberRegisterReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            //MemoryStream excelStream = ExcelHelper.ExportToExcelNewVersion(excelRows, excelHeader);
            //excelStream.Position = 0;
            //return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        #region List Action Grid
        //[ChildActionOnly]
        //public virtual ActionResult GridGetAll()
        //{
        //    var data = DataRepositoryFactory.CurrentRepository.SP_GetGridMemberReporting(null, null, null, false, 1, 5, null, null).AsEnumerable();
        //    return PartialView();
        //}

        [HttpGet]
        public virtual ActionResult GridGetAllPager(string startDate, string endDate, int? channelId, bool? memberWithReceipt, string orderColumn, string orderDirection, int page)
                {
            orderColumn = orderColumn ?? "CreatedDate";
            orderDirection = orderDirection ?? "DESC";
            //orderDirection = orderDirection.ToUpper() == "DESC" ? "ASC" : "DESC";
            page = (page <= 0 ? 1 : page);

            int pageSize = 5,
                totalRow = 0;

            DateTime? _startDate = null, _endDate = null;
            DateTime tempDate = DateTime.Today;
            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _startDate = tempDate;
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/2016 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _endDate = tempDate;
            }
            else
            {
                _endDate = DateTime.Today.AddDays(1);
            }

            string channel = "";
            if (channelId.HasValue)
            {
                channel = Channel.GetById(channelId.Value).Name;
            }

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.CurrentPage = page;
            ViewBag.ChannelId = channelId;
            ViewBag.OrderColumn = orderColumn;
            ViewBag.OrderDirection = orderDirection;
            ViewBag.PageSize = pageSize;
            ViewBag.MemberWithReceipt = memberWithReceipt;
            //totalRow = DataRepositoryFactory.CurrentRepository.SP_GetGridMemberReportingTotalRow(channelId, _startDate, _endDate, memberWithReceipt)
            //    .FirstOrDefault()
            //    .GetValueOrDefault(0);
            totalRow = (int)GetMemberReportGridElasticCount(channel, _startDate.Value, _endDate.Value);
            ViewBag.TotalRow = totalRow;
            ViewBag.TotalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            //var data = DataRepositoryFactory.CurrentRepository.SP_GetGridMemberReporting(channelId, _startDate, _endDate, memberWithReceipt, page, pageSize, orderColumn, orderDirection).AsEnumerable();
            var data = GetMemberReportGridElastic(channel, _startDate.Value, _endDate.Value, page, pageSize, orderColumn, orderDirection);
            return PartialView("GridGetAll", data);
        }

        private void GetChannelTypeList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Channel> listdata = Channel.GetAll();
            foreach (Channel data in listdata)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ChannelList = items;
        }
        #endregion

        #region Elastic
        private List<MemberReportElasticModel> GetMemberReportElastic(DateTime from, DateTime to)
        {
            ElasticConnection connection = new ElasticConnection();

            List<MemberReportElasticModel> result = new List<MemberReportElasticModel>();
            var documents = new ConcurrentBag<IReadOnlyCollection<MemberReportElasticModel>>();

            var initialResponse = connection.Client.Search<MemberReportElasticModel>(s => s
                .From(0)
                .Size(10000)
                .AllTypes()
                .Index(Common.SiteSetting.ElasticMemberReportIndex)
                .Query(q => q
                    .Bool(bq => bq
                        .Filter(
                            f => f
                                .DateRange(d => d
                                    .Field("createddate")
                                    .GreaterThanOrEquals(from)
                                    .LessThanOrEquals(to)
                                    .TimeZone("+07:00")),
                            f => f.Term(t => t.Field("isdeleted").Value(false))
                            )
                        )
                    )
                .Scroll("3s")
            );

            if (!initialResponse.IsValid || string.IsNullOrEmpty(initialResponse.ScrollId))
                throw new Exception(initialResponse.ServerError.Error.Reason);
            if (initialResponse.Documents.Any())
                result.AddRange(initialResponse.Documents);
            string scrollid = initialResponse.ScrollId;
            bool isScrollSetHasData = true;
            while (isScrollSetHasData)
            {
                ISearchResponse<MemberReportElasticModel> loopingResponse = connection.Client.Scroll<MemberReportElasticModel>("2m", scrollid);
                if (loopingResponse.IsValid)
                {
                    result.AddRange(loopingResponse.Documents);
                    scrollid = loopingResponse.ScrollId;
                }
                isScrollSetHasData = loopingResponse.Documents.Any();
            }

            connection.Client.ClearScroll(new ClearScrollRequest(scrollid));

            return result;
        }

        private static List<MemberReportElasticModel> GetMemberReportGridElastic(string channel, DateTime startDate, DateTime endDate, int currentPage, int totalRowPerPage, string orderColumn, string orderDirection)
        {
            if (currentPage > 0)
            {
                currentPage = currentPage - 1;
            }
            ElasticConnection elasticConn = new ElasticConnection();

            if (orderColumn == "SpgCode")
                orderColumn = "spg code";
            else if (orderColumn == "SpgName")
                orderColumn = "spg name";
            else if (orderColumn == "OutletName")
                orderColumn = "spg outletname";
            else if (orderColumn == "Channel")
                orderColumn = "channel name";
            else if (orderColumn == "ProductBefore")
                orderColumn = "product before";

            string[] notStringProps = new string[]
            {
                "createddate",
                "childdob",
                "agepregnant",
                "point",
                "isblacklist"
            };
            if (!notStringProps.Contains(orderColumn.ToLower()))
            {
                orderColumn = orderColumn + ".keyword";
            }

            ISearchResponse<MemberReportElasticModel> data = null;
            if (orderDirection.ToLower() == "asc")
            {
                data = elasticConn.Client.Search<MemberReportElasticModel>(s => s
                    .Index(Common.SiteSetting.ElasticMemberReportIndex)
                    .AllTypes()
                    .From(currentPage * totalRowPerPage)
                    .Size(totalRowPerPage)
                    .Sort(ss => ss
                        .Ascending(orderColumn.ToLower())
                    )
                    .Query(q => q
                        .Bool(b => b
                            .Filter(
                                f => f.Term(t => t.Field("channelname.keyword").Value(channel)),
                                f => f.DateRange(dr => dr
                                      .Field("createddate")
                                      .GreaterThanOrEquals(startDate)
                                      .LessThanOrEquals(endDate)
                                      .TimeZone("+07:00")
                                ),
                                f => f.Term(t => t.Field("isdeleted").Value(false))
                            )
                        )
                    )
                );
            }
            else
            {
                data = elasticConn.Client.Search<MemberReportElasticModel>(s => s
                    .Index(Common.SiteSetting.ElasticMemberReportIndex)
                    .AllTypes()
                    .From(currentPage * totalRowPerPage)
                    .Size(totalRowPerPage)
                    .Sort(ss => ss
                        .Descending(orderColumn.ToLower())
                    )
                    .Query(q => q
                        .Bool(b => b
                            .Filter(
                                f => f.Term(t => t.Field("channelname.keyword").Value(channel)),
                                f => f.DateRange(dr => dr
                                      .Field("createddate")
                                      .GreaterThanOrEquals(startDate)
                                      .LessThanOrEquals(endDate)
                                      .TimeZone("+07:00")
                                ),
                                f => f.Term(t => t.Field("isdeleted").Value(false))
                            )
                        )
                    )
                );
            }

            var result = data.Hits.Select(x => x.Source).ToList();

            foreach (var item in result)
            {
                item.CreatedDate = item.CreatedDate.ToLocalTime();
                item.ChildBirthDate = item.ChildBirthDate.HasValue ? item.ChildBirthDate.Value.ToLocalTime() : (DateTime?)null;
                item.IsActiveString = item.IsActive ? "Yes" : "No";
            }

            return result;
        }

        private static long GetMemberReportGridElasticCount(string channel, DateTime startDate, DateTime endDate)
        {
            ElasticConnection elasticConn = new ElasticConnection();

            var data = elasticConn.Client.Count<MemberReportElasticModel>(c => c
                .Index(Common.SiteSetting.ElasticMemberReportIndex)
                .AllTypes()
                .Query(q => q
                    .Bool(b => b
                        .Filter(
                            f => f.Term(t => t.Field("channelname.keyword").Value(channel)),
                            f => f.DateRange(dr => dr
                                  .Field("createddate")
                                  .GreaterThanOrEquals(startDate)
                                  .LessThanOrEquals(endDate)
                                  .TimeZone("+07:00")
                            ),
                            f => f.Term(t => t.Field("isdeleted").Value(false))
                        )
                    )
                )
            );

            return data.Count;
        }
        #endregion
    }
}