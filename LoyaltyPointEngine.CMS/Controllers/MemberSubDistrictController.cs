﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.DataMember;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;

using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.Model.Object.Member;
using System.Security.Cryptography;
using System.Text;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberSubDistrictController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "SubDistrict")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "SubDistrict successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "SubDistrict successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "SubDistrict successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add SubDistrict")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            SubDistrictModel model = new SubDistrictModel();
            PopulateProvinces(model.ProvinceID.ToString());
            PopulateDistrict(model.ProvinceID,model.DistrictID.ToString());
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(SubDistrictModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {
                var data = new SubDistrict();
                data.DistrictID = model.DistrictID;
                data.SubDistrictCode = model.SubDistrictCode;
                data.SubDistrictName = model.SubDistrictName;
                var result = data.Inserted(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.MemberSubDistrict.Index("Create_Success"));

            }
            PopulateProvinces(model.ProvinceID.ToString());
            PopulateDistrict(model.ProvinceID, model.DistrictID.ToString());
            return View(model);
        }

        private void ValidateAddModel(SubDistrictModel model)
        {

            if (string.IsNullOrEmpty(model.SubDistrictCode))
            {
                ModelState.AddModelError("SubDistrictCode", "Code is required");
            }
            else
            {
                var data = SubDistrict.GetByCode(model.SubDistrictCode);
                if (data != null)
                {
                    ModelState.AddModelError("SubDistrictCode", "Code is already registered in system");
                }
            }


            if (string.IsNullOrEmpty(model.SubDistrictName))
            {
                ModelState.AddModelError("SubDistrictCode", "Name is required");
            }
        }

        [BreadCrumb(Label = "Edit SubDistrict")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Edit(int ID)
        {
            var data = SubDistrict.GetByID(ID);
            SubDistrictModel model = new SubDistrictModel(data);
            PopulateProvinces(model.ProvinceID.ToString());
            PopulateDistrict(model.ProvinceID, model.DistrictID.ToString());
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Edit(SubDistrictModel model)
        {
            ValidateEditModel(model);
            var data = SubDistrict.GetByID(model.ID);
            if (data == null)
            {
                ModelState.AddModelError("ID", "ID not found in system");
            }
            if (ModelState.IsValid)
            {
                data.DistrictID = model.DistrictID;
                data.SubDistrictCode = model.SubDistrictCode;
                data.SubDistrictName = model.SubDistrictName;
                var result = data.Updated(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.MemberSubDistrict.Index("Edit_Success"));

            }
            PopulateProvinces(model.ProvinceID.ToString());
            PopulateDistrict(model.ProvinceID, model.DistrictID.ToString());
            return View(model);
        }


        private void ValidateEditModel(SubDistrictModel model)
        {

            if (string.IsNullOrEmpty(model.SubDistrictCode))
            {
                ModelState.AddModelError("SubDistrictCode", "Code is required");
            }
            else
            {
                var data = SubDistrict.GetByCodeNotSelf(model.SubDistrictCode, model.ID);
                if (data != null)
                {
                    ModelState.AddModelError("SubDistrictCode", "Code is already registered in system");
                }
            }


            if (string.IsNullOrEmpty(model.SubDistrictName))
            {
                ModelState.AddModelError("SubDistrictCode", "Name is required");
            }
        }

        #region List District Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllMemberSubDistrict()
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = SubDistrict.GetAll().OrderBy(x => x.SubDistrictCode);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllSubDistrictPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var allData = SubDistrict.GetAll().OrderBy(x => x.SubDistrictName);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.SubDistrictName.Contains(txtSearch) || x.SubDistrictCode.Contains(txtSearch)).OrderBy(x => x.SubDistrictCode);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.MemberSubDistrict.Views.GridGetAllMemberSubDistrict, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion


        public void PopulateProvinces(string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = Province.GetAll().OrderBy(x => x.ProvinceName); ;
            foreach (var data in listdata)
            {

                items.Add(new SelectListItem { Text = data.ProvinceName, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListProvince = items;
        }
        public void PopulateDistrict(int ProvinceID, string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = District.GetAll().Where(x => x.ProvinceID == ProvinceID).OrderBy(x => x.DistrictName);
            foreach (var data in listdata)
            {
                items.Add(new SelectListItem { Text = data.DistrictName, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListDistrict = items;
        }

        [HttpGet]
        public virtual JsonResult GeneratedDistrict(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            if (id.HasValue)
            {
                var listdata = District.GetAll().Where(x => x.ProvinceID == id).OrderBy(x => x.DistrictName);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = data.DistrictName, Value = data.ID.ToString() });
                }
            }
        

            return Json(items, JsonRequestBehavior.AllowGet);
           // return PartialView(MVC.MemberSubDistrict.Views._ListDistrict, items);

        }


        [HttpGet]
        public virtual JsonResult GeneratedSubDistrict(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            if (id.HasValue)
            {
                var listdata = SubDistrict.GetAll().Where(x => x.DistrictID == id).OrderBy(x => x.SubDistrictName);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = data.SubDistrictName, Value = data.ID.ToString() });
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);

        }


    }
}