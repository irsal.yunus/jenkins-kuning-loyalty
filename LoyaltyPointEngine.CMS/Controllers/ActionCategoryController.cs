﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ActionCategoryController : BaseController
    {
        // GET: ActionCategory
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "ActionCategory")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action Category successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action Category successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action Category successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add ActionCategory")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ActionCategoryModel model = new ActionCategoryModel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ActionCategoryModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {
                ActionCategory ActionCategory = new ActionCategory();
                ActionCategory.InjectFrom(model);
                var result = ActionCategory.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.ActionCategory.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }


        private void ValidateAddModel(ActionCategoryModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
            else
            {
                var checkData = Data.ActionCategory.GetByCode(model.Code);
                if (checkData != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }
        }

        [BreadCrumb(Label = "Edit ActionCategory")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int ActionCategoryId)
        {
            ActionCategory currentActionCategory = LoyaltyPointEngine.Data.ActionCategory.GetById(ActionCategoryId);
            ActionCategoryModel model = new ActionCategoryModel();
            model.InjectFrom(currentActionCategory);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ActionCategoryModel model)
        {
            ValidateEditModel(model);
            ActionCategory ActionCategory = LoyaltyPointEngine.Data.ActionCategory.GetById(model.ID);
            if (ActionCategory == null)
            {
                ModelState.AddModelError("ID", "ID is not registered in system");
            }

            if (ModelState.IsValid)
            {
                ActionCategory.InjectFrom(model);
                var result = ActionCategory.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.ActionCategory.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View();
        }

        private void ValidateEditModel(ActionCategoryModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
            else
            {
                var checkData = Data.ActionCategory.GetByCodeNotSelf(model.Code, model.ID);
                if (checkData != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }
        }


        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            ActionCategory ActionCategory = ActionCategory.GetById(ID);
            ActionCategory.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.ActionCategory.Index("Delete_Success"));
        }

        #region List ActionCategory Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllActionCategory()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<ActionCategory> allActionCategory = LoyaltyPointEngine.Data.ActionCategory.GetAll().OrderBy(x => x.Name);
            AjaxGrid<ActionCategory> grid = this.gridMvcHelper.GetAjaxGrid(allActionCategory);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllActionCategoryPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<ActionCategory> allData = LoyaltyPointEngine.Data.ActionCategory.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<ActionCategory> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.ActionCategory.Views.GridGetAllActionCategory, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}