﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using System.Globalization;
using LoyaltyPointEngine.PointLogicLayer.Collect;
using System.Transactions;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.Model.Object.Incentive;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.PointLogicLayer.Incentive;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Parameter.Member;
using LoyaltyPointEngine.Model.Object.Member;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class IncentiveController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Incentive")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "SPG successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "SPG successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "SPG successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Clear = true, Label = "Incentive")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Edit(string code)
        {
            //get data total perbulan by code
            List<IncentiveModel> listIncentiveSpg = IncentiveLogic.GetListIncentiveMonthlyBySpgCode(code);

            IncentiveViewModel model = new IncentiveViewModel();
            model.ListIncentiveModel = listIncentiveSpg;
            return View(model);
        }

        [BreadCrumb(Clear = true, Label = "Incentive")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        [HttpPost]
        public virtual ActionResult Edit(IncentiveViewModel model)
        {
            if (model == null || model.ListIncentiveModel.Count() <= 0)
            {
                return View(model);
            }

            if (IncentiveLogic.UpdateIncentiveSpg(model.ListIncentiveModel))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [BreadCrumb(Clear = true, Label = "Incentive")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult DisplayAll()
        {
            //get data total perbulan by code
            IncentiveLogic.DisplayAll();

            return RedirectToAction("Index");
        }

        #region List SPG Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllSPG()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<SPG> allSPG = SPG.GetAll().OrderBy(x => x.Code);
            AjaxGrid<SPG> grid = this.gridMvcHelper.GetAjaxGrid(allSPG);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllSPGPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<SPG> allData = DataMember.SPG.GetAll().OrderBy(x => x.Code);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Code.Contains(txtSearch)
                    || x.Name.Contains(txtSearch)
                    || x.OutletName.Contains(txtSearch)
                    )
                    .OrderBy(x => x.Code);
            }
            AjaxGrid<SPG> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Incentive.Views.GridGetAllSPG, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}