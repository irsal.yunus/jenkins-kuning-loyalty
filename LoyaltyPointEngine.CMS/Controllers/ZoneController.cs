﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ZoneController : BaseController
    {
        // GET: Zone
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Zone")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Zone successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Zone successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Zone successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Zone")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ZoneModel model = new ZoneModel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ZoneModel model)
        {
            this.IsValidZone(model, false);
            if (ModelState.IsValid)
            {

                Zone Zone = new Zone();
                Zone.InjectFrom(model);
                var result = Zone.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Zone.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        [BreadCrumb(Label = "Edit Zone")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int ZoneId)
        {
            Zone currentZone = LoyaltyPointEngine.Data.Zone.GetById(ZoneId);
            ZoneModel model = new ZoneModel();
            model.InjectFrom(currentZone);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ZoneModel model)
        {
            this.IsValidZone(model, true);
            if (ModelState.IsValid)
            {
                Zone Zone = LoyaltyPointEngine.Data.Zone.GetById(model.ID);
                Zone.InjectFrom(model);
                var result = Zone.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Zone.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Zone Zone = Zone.GetById(ID);
            Zone.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Zone.Index("Delete_Success"));
        }

        private void IsValidZone(ZoneModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.Code))
            {
                Data.Zone predictedData = Data.Zone.GetByCode(Model.Code);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("Zone Code '{0}'  has been used by other Zone", Model.Code));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("Zone Code  '{0}'  has been used by other Zone", Model.Code));
            }
        }
        #region List Zone Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllZone()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Zone> allZone = LoyaltyPointEngine.Data.Zone.GetAll().OrderBy(x => x.Name);
            AjaxGrid<Zone> grid = this.gridMvcHelper.GetAjaxGrid(allZone);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllZonePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Zone> allData = LoyaltyPointEngine.Data.Zone.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<Zone> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Zone.Views.GridGetAllZone, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}