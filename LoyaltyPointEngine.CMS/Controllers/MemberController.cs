﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter.Member;
using LoyaltyPointEngine.Model.Parameter;
using MvcBreadCrumbs;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Model.Object;
using System.Configuration;
using System.Text;
using LoyaltyPointEngine.Common.Log;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberController : BaseController
    {
        /*
         * Data Lengkap Member disimpan di Database Loyalty Member
         *
         */

        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Member successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Member successfully updated ";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Member successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            string spgRoleCode;
            ViewBag.IsSpg = this.IsSpgRole(out spgRoleCode);
            return View();
        }

        [BreadCrumb(Label = "Add Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            AddMemberParameterCMSModel model = new AddMemberParameterCMSModel();
            //GetListStages();
            _getListStages();
            GetListCity();
            GetListSPG();
            //GetListProductBefore();
            PopulateProvinces(model.ProvinceID.GetValueOrDefault().ToString());
            PopulateDistrict(model.ProvinceID.GetValueOrDefault(), model.DistrictID.GetValueOrDefault().ToString());
            PopulateSubDistrict(model.DistrictID.GetValueOrDefault(), model.SubDistrictID.GetValueOrDefault().ToString());
            PopulateChannel();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(AddMemberParameterCMSModel model, HttpPostedFileBase ProfilePicture, HttpPostedFileBase KTPImage, HttpPostedFileBase KKImage, HttpPostedFileBase AkteImage, HttpPostedFileBase ESignature)
        {
            string assetURL = Data.Settings.GetValueByKey(SiteSetting.ASSET_URL);
            string url = Request.Url.AbsoluteUri;
            _IsValidMember(model);
            if (ModelState.IsValid)
            {
                string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);

                string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);

                string memberImageCode = Data.Number.Generated(Data.Number.Type.MEMB, CurrentUser.FullName);

                if (RelativePathMember.Substring(RelativePathMember.Length - 1, 1).Contains("/"))
                {
                    RelativePathMember = RelativePathMember + memberImageCode + "/";
                }
                else
                {
                    RelativePathMember = RelativePathMember + "/" + memberImageCode + "/";
                }

                if (MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\"))
                {
                    MediaLocalImageDirectoryMember = MediaLocalImageDirectoryMember + memberImageCode + "\\";
                }
                else
                {
                    MediaLocalImageDirectoryMember = MediaLocalImageDirectoryMember + "\\" + memberImageCode + "\\";
                }

                if (!Directory.Exists(MediaLocalImageDirectoryMember))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Path.Combine(MediaLocalImageDirectoryMember));
                }

                //profile pic
                if (ProfilePicture != null)
                {
                    string filename = ProfilePicture.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    ProfilePicture.SaveAs(imageFulleName);

                    model.ProfilePicture = RelativePathMember + filename;
                }

                //Ktp Image
                if (KTPImage != null)
                {
                    string filename = KTPImage.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    KTPImage.SaveAs(imageFulleName);

                    model.KTPImage = RelativePathMember + filename;
                }

                //KK Image
                if (KKImage != null)
                {
                    string filename = KKImage.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    KKImage.SaveAs(imageFulleName);

                    model.KKImage = RelativePathMember + filename;
                }

                //Akte Image
                if (AkteImage != null)
                {
                    string filename = AkteImage.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    AkteImage.SaveAs(imageFulleName);

                    model.AkteImage = RelativePathMember + filename;
                }

                //E-Signature Image
                if (ESignature != null)
                {
                    string filename = ESignature.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    ESignature.SaveAs(imageFulleName);

                    model.ESignature = RelativePathMember + filename;
                }

                string notEncriptPassword = model.Password;
                model.Password = LoyaltyPointEngine.Common.Encrypt.LoyalEncript(model.Password);

                if (string.IsNullOrEmpty(model.Gender))
                {
                    model.Gender = "f";
                }

                if (string.IsNullOrEmpty(model.Email))
                {
                    model.Email = string.Format("{0}@{0}.com", model.Phone);
                }

                // if current user role is nba user (spg) then assign his/her id to member model for registration
                string spgRoleCode;
                bool isSpg = false;
                if (this.IsSpgRole(out spgRoleCode))
                {
                    if (CurrentRole.Code == Guid.Parse(spgRoleCode))
                    {
                        var spgUser = SPG.GetByCode(CurrentUser.Username);
                        if (spgUser != null)
                        {
                            isSpg = true;
                            model.SpgID = spgUser.ID;
                        }
                    }
                }

                var result = MemberLogic.Add(model, CurrentUser.FullName, url, true);
                if (result.StatusCode == "00")
                {
                    int totalPoint = 0;
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        var memberPoint = new LoyaltyPointEngine.Data.Member();
                        memberPoint.ID = result.Value.ID;
                        memberPoint.Name = string.Format("{0} {1}", result.Value.FirstName, result.Value.LastName).Trim();
                        memberPoint.Email = result.Value.Email;
                        memberPoint.Phone = result.Value.Phone;
                        var responseMemberPoint = memberPoint.Insert(CurrentUser.FullName, url, CurrentUser.ID);
                        if (responseMemberPoint.Success)
                        {
                            var actionCode = Common.Point.ActionType.Code.REGMANUAL;
                            if (isSpg || model.SpgID.HasValue)
                            {
                                actionCode = Common.Point.ActionType.Code.REGNBA;
                            }
                            var ressInsertPOint = PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actionCode, result.Value, url);
                            if (ressInsertPOint != null && ressInsertPOint.StatusCode == "00")
                            {
                                totalPoint = ressInsertPOint.Value.TotalPoints;
                                transScope.Complete();
                            }
                            else
                            {
                                transScope.Dispose();
                            }
                        }
                        else
                        {
                            transScope.Dispose();
                        }
                    }

                    PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(result.Value.ID, CurrentUser.FullName);

                    if (totalPoint > 0)
                    {
                        string title = "Selamat Datang di Bebeclub";
                        string message = "Anda Mendapatkan point Registrasi";
                        var responseNotif = NotificationLogic.Create(result.Value.ID, Common.Notification.NotificationType.Type.Information, title, message, "", totalPoint, result.Value.FirstName);
                    }
                    string provinceName = (model.ProvinceID.HasValue) ? DataMember.Province.GetByID(model.ProvinceID.Value).ProvinceName : string.Empty;
                    string districtName = (model.DistrictID.HasValue) ? DataMember.District.GetByID(model.DistrictID.Value).DistrictName : string.Empty;
                    string StagesValue = (model.StagesID.HasValue) ? DataMember.Stages.GetById(model.StagesID.Value).Name : string.Empty;

                    return RedirectToAction(MVC.Member.Index("Create_Success"));
                }
                else
                {
                    ModelState.AddModelError("Error", result.StatusMessage);
                }

                //RemoveTempDirAndFile(originalPath);
            }

            _getListStages();
            //GetListStages();
            GetListCity();
            GetListSPG();
            //GetListProductBefore();
            PopulateProvinces(model.ProvinceID.GetValueOrDefault().ToString());
            PopulateDistrict(model.ProvinceID.GetValueOrDefault(), model.DistrictID.GetValueOrDefault().ToString());
            PopulateSubDistrict(model.DistrictID.GetValueOrDefault(), model.SubDistrictID.GetValueOrDefault().ToString());
            PopulateChannel();
            return View(model);
        }

        [BreadCrumb(Label = "Edit Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int MemberId, string code = "", string status = "")
        {
            Member currentMember = Member.GetById(MemberId);
            EditMemberCMSModel model = new EditMemberCMSModel();

            //GetListStages();
            _getListStages();
            GetListCity();
            GetListSPG();
            model.InjectFrom(currentMember);
            model.IsBlackList = currentMember.IsBlacklist;
            model.Birthdate = currentMember.Birthdate.HasValue ? currentMember.Birthdate.Value.ToString("dd/MM/yyyy") : "";
            model.Password = "";
            model.ConfirmPassword = "";
            model.ChildBirthdate = currentMember.Child.Count > 0 ? currentMember.Child.FirstOrDefault().Birthdate.Value.ToString("dd/MM/yyyy") : "";
            model.ProductBeforeID = currentMember.ProductBeforeID.HasValue ? Convert.ToInt32(currentMember.ProductBeforeID.Value) : 0;
            model.Consent = currentMember.AcceptDate.HasValue ? true : false;
            model.AgePregnant = currentMember.AgePregnant.HasValue ? currentMember.AgePregnant.Value : 0;
            if (currentMember.Channel != null)
                model.Channel = currentMember.Channel.Code;
            else
                model.Channel = Convert.ToString(ChannelType.web);
            if (currentMember.Child.Any())
            {
                model.Childs = (from a in currentMember.Child
                                where a.IsDeleted == false
                                select new AddChildParameterModel
                                {
                                    ID = a.ID,
                                    ParentID = a.ParentID,
                                    Name = a.Name,
                                    Gender = a.Gender,
                                    PlaceOfBirth = a.PlaceOfBirth,
                                    Birthdate = a.Birthdate.HasValue ? a.Birthdate.Value.ToString("dd/MM/yyyy") : DateTime.MinValue.ToString("dd/MM/yyyy"),
                                    ProductBeforeId = a.ProductBeforeID.HasValue ? a.ProductBeforeID.Value : 0
                                }).ToList();
                var firstChild = currentMember.Child.FirstOrDefault();
                model.ChildBirthdate = firstChild.Birthdate.HasValue ? firstChild.Birthdate.Value.ToString("dd/MM/yyyy") : "";
                model.ChildName = firstChild.Name;
            }
            model.IsNewsLetter = (currentMember.IsNewsLetter != null) ? (bool)currentMember.IsNewsLetter : false;
            //GetListProductBefore(model.ProductBeforeID.GetValueOrDefault().ToString());
            PopulateProvinces(model.ProvinceID.GetValueOrDefault().ToString());
            PopulateDistrict(model.ProvinceID.GetValueOrDefault(), model.DistrictID.GetValueOrDefault().ToString());
            PopulateSubDistrict(model.DistrictID.GetValueOrDefault(), model.SubDistrictID.GetValueOrDefault().ToString());
            Stages stages = new Stages();
            if (model.StagesID.HasValue)
            {
                stages = Stages.GetById(model.StagesID.Value);
            }
            //if (stages != null && stages.IsPregnant)
            //{
            //    _getListProductBefore(true, null);
            //}
            //if(model.Childs != null)
            //{
            //    _getListProductBeforeChild(false, null);
            //}
            _getListProductBefore(true, null);
            _getListProductBeforeChild(false, null);

            PopulateChannel();

            if (status != "")
            {
                ViewBag.Code = code;
                ViewBag.StatusDeleteChild = status;
            }

            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(EditMemberCMSModel model, HttpPostedFileBase ProfilePicture, HttpPostedFileBase KTPImage, HttpPostedFileBase KKImage, HttpPostedFileBase AkteImage, HttpPostedFileBase ESignature)
        {
            string assetURL = Data.Settings.GetValueByKey(SiteSetting.ASSET_URL);
            _IsValidMember(model, true);
            var currMember = Member.GetById(model.ID);
            if (currMember == null)
            {
                ModelState.AddModelError("", "Id not found");
            }
            bool IsChangePassword = false;
            if (!string.IsNullOrEmpty(model.Password))
            {
                if (string.IsNullOrEmpty(model.ConfirmPassword))
                {
                    ModelState.AddModelError("ConfirmPassword", "ConfirmPassword is required");
                }
                else
                {
                    if (model.Password != model.ConfirmPassword)
                    {
                        ModelState.AddModelError("ConfirmPassword", "ConfirmPassword  and Password is not match");
                    }
                    else
                    {
                        if (model.Password.Length < 6)
                        {
                            ModelState.AddModelError("Password", "Password minimum 6 character");
                        }
                        else
                        {
                            IsChangePassword = true;
                        }
                    }
                }
            }
            if (ModelState.IsValid)
            {
                string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
                string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);
                string memberImageCode = "";
                if (!string.IsNullOrEmpty(currMember.ProfilePicture) && currMember.ProfilePicture.Contains("/"))
                {
                    var arr = currMember.ProfilePicture.Split('/');
                    if (arr != null && arr.Count() - 2 >= 0)
                    {
                        memberImageCode = arr[arr.Count() - 2];
                    }
                }
                else if (!string.IsNullOrEmpty(currMember.KTPImage) && currMember.KTPImage.Contains("/"))
                {
                    var arr = currMember.KTPImage.Split('/');
                    if (arr != null && arr.Count() - 2 >= 0)
                    {
                        memberImageCode = arr[arr.Count() - 2];
                    }
                }
                else if (!string.IsNullOrEmpty(currMember.KKImage) && currMember.KKImage.Contains("/"))
                {
                    var arr = currMember.KKImage.Split('/');
                    if (arr != null && arr.Count() - 2 >= 0)
                    {
                        memberImageCode = arr[arr.Count() - 2];
                    }
                }
                else if (!string.IsNullOrEmpty(currMember.AkteImage) && currMember.AkteImage.Contains("/"))
                {
                    var arr = currMember.AkteImage.Split('/');
                    if (arr != null && arr.Count() - 2 >= 0)
                    {
                        memberImageCode = arr[arr.Count() - 2];
                    }
                }
                else
                {
                    memberImageCode = Data.Number.Generated(Data.Number.Type.MEMB, CurrentUser.FullName);
                }
                if (RelativePathMember.Substring(RelativePathMember.Length - 1, 1).Contains("/"))
                {
                    RelativePathMember = RelativePathMember + memberImageCode + "/";
                }
                else
                {
                    RelativePathMember = RelativePathMember + "/" + memberImageCode + "/";
                }
                if (MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\"))
                {
                    MediaLocalImageDirectoryMember = MediaLocalImageDirectoryMember + memberImageCode + "\\";
                }
                else
                {
                    MediaLocalImageDirectoryMember = MediaLocalImageDirectoryMember + "\\" + memberImageCode + "\\";
                }
                if (!Directory.Exists(MediaLocalImageDirectoryMember))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Path.Combine(MediaLocalImageDirectoryMember));
                }
                //profile pic
                if (ProfilePicture != null)
                {
                    string filename = ProfilePicture.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    ProfilePicture.SaveAs(imageFulleName);

                    model.ProfilePicture = RelativePathMember + filename;
                }
                //Ktp Image
                if (KTPImage != null)
                {
                    string filename = KTPImage.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    KTPImage.SaveAs(imageFulleName);

                    model.KTPImage = RelativePathMember + filename;
                }
                //KK Image
                if (KKImage != null)
                {
                    string filename = KKImage.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    KKImage.SaveAs(imageFulleName);

                    model.KKImage = RelativePathMember + filename;
                }

                //Akte Image
                if (AkteImage != null)
                {
                    string filename = AkteImage.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                            string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                            string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    AkteImage.SaveAs(imageFulleName);

                    model.AkteImage = RelativePathMember + filename;
                }

                //E-Signature Image
                if (ESignature != null)
                {
                    string filename = ESignature.FileName;
                    filename = StringHelper.SanitizePath(filename, '_');
                    string imageFullName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                        string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                        string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);
                    ESignature.SaveAs(imageFullName);

                    model.ESignature = RelativePathMember + filename;
                }
                EditMemberParameterModel editModel = new EditMemberParameterModel();
                editModel.InjectFrom(model);
                #region Child
                if (model.Childs != null)
                {
                    editModel.Childs = new List<EditChildParameterModel>();
                    foreach (var item in model.Childs)
                    {
                        var child = new EditChildParameterModel();
                        child.ID = item.ID;
                        child.Name = item.Name;
                        child.ProductBeforeId = item.ProductBeforeId;
                        child.Birthdate = item.Birthdate;
                        child.ParentID = model.ID;
                        editModel.Childs.Add(child);
                    }
                }
                #endregion
                if (IsChangePassword)
                {
                    editModel.Password = Common.Encrypt.LoyalEncript(model.Password);

                }
                editModel.IsNewsletter = model.IsNewsLetter;
                if (string.IsNullOrEmpty(model.Gender))
                {
                    model.Gender = "f";
                }
                var result = MemberLogic.UpdateMemberByMemberModel(editModel, CurrentUser.FullName, model.ID, true, model);
                if (result.StatusCode == "00")
                {
                    string provinceName = (model.ProvinceID.HasValue) ? DataMember.Province.GetByID(model.ProvinceID.Value).ProvinceName : string.Empty;
                    string districtName = (model.DistrictID.HasValue) ? DataMember.District.GetByID(model.DistrictID.Value).DistrictName : string.Empty;
                    string StagesValue = (model.StagesID.HasValue) ? DataMember.Stages.GetById(model.StagesID.Value).Name : string.Empty;
                    var dataMember = DataMember.Member.GetById(model.ID);

                    return RedirectToAction(MVC.Member.Index("Edit_Success"));
                }
                else
                    ModelState.AddModelError("Error", result.StatusMessage);
            }
            //GetListStages();
            GetListCity();
            GetListSPG();
            _getListStages();
            _getListProductBefore(true, null);
            _getListProductBeforeChild(false, null);
            //GetListProductBefore(model.ProductBeforeID.GetValueOrDefault().ToString());
            PopulateProvinces(model.ProvinceID.GetValueOrDefault().ToString());
            PopulateDistrict(model.ProvinceID.GetValueOrDefault(), model.DistrictID.GetValueOrDefault().ToString());
            PopulateSubDistrict(model.DistrictID.GetValueOrDefault(), model.SubDistrictID.GetValueOrDefault().ToString());
            PopulateChannel();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            ResultModel<bool> result = MemberLogic.DeleteMember(ID, BebeUmbracoAPISecretCode, Request.Url.AbsoluteUri, CurrentUser);

            if (result.Value)
            {
                return RedirectToAction(MVC.Member.Index("Delete_Success"));
            }
            else
            {
                return RedirectToAction(MVC.Member.Index("Delete_Failed"));
            }
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult DeleteChild(int ID, int MemberID)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();
            string currentUserName = CurrentUser.FullName;
            var childsCount = ChildLogic.ChildsCountByMemberID(MemberID);
            if (childsCount.StatusCode == "00")
            {
                if (childsCount.Value <= 1)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = "Cannot delete the only one child.";
                }
                else
                {
                    result = ChildLogic.DeleteByCollect(ID, currentUserName);
                }

            }
            else
            {
                result.StatusCode = "500";
                result.StatusMessage = childsCount.StatusMessage;
            }

            return RedirectToAction(MVC.Member.Edit(MemberID, result.StatusCode, result.StatusMessage));
        }

        [HttpGet]
        public virtual ActionResult SyncMember()
        {
            string currentUserName = CurrentUser.FullName;
            long currentUserID = CurrentUser.ID;
            List<Member> listMember = Member.GetAll().Where(x => EntityFunctions.TruncateTime(x.CreatedDate) == EntityFunctions.TruncateTime(DateTime.Now)
                || (x.UpdatedDate != null && EntityFunctions.TruncateTime(x.UpdatedDate) == EntityFunctions.TruncateTime(DateTime.Now))).ToList();
            int updatedDataCount = 0;
            int insertedDataCount = 0;
            int errorDataCount = 0;
            foreach (Member member in listMember)
            {
                Data.EFResponse res = new Data.EFResponse();
                Data.Member predictedMemberData = Data.Member.GetById(member.ID);
                if (predictedMemberData != null)
                {
                    //update
                    predictedMemberData.Name = member.FirstName + " " + member.LastName;
                    predictedMemberData.Email = member.Email;
                    predictedMemberData.Phone = member.Phone;
                    res = predictedMemberData.Update(currentUserName, Request.Url.AbsoluteUri, currentUserID);
                    if (res.Success)
                        updatedDataCount++;
                    else
                        errorDataCount++;
                }
                else
                {
                    //add
                    Data.Member memberData = new Data.Member();
                    memberData.ID = member.ID;
                    memberData.Name = member.FirstName + " " + member.LastName;
                    memberData.Email = member.Email;
                    memberData.Phone = member.Phone;
                    res = memberData.Insert(currentUserName, Request.Url.AbsoluteUri, currentUserID);
                    if (res.Success)
                        insertedDataCount++;
                    else
                        errorDataCount++;
                }
            }
            return Json(new { Update = insertedDataCount, Insert = insertedDataCount, Error = errorDataCount }, JsonRequestBehavior.AllowGet);
        }

        private void _IsValidMember(AddMemberParameterCMSModel model, bool isEdit = false)
        {
            //if (string.IsNullOrEmpty(model.Gender))
            //    ModelState.AddModelError("Error", "Gender is mandatory");
            if (string.IsNullOrEmpty(model.FirstName))
                ModelState.AddModelError("Error", "First Name is mandatory");
            if (string.IsNullOrEmpty(model.LastName))
                ModelState.AddModelError("Error", "Last Name is mandatory");
            if (model.ProductBeforeID < 1)
                ModelState.AddModelError("Error", "Product Before is mandatory");
            if (string.IsNullOrEmpty(model.Email))
            {
                //if (model.ID > 0)
                //{
                //    ModelState.AddModelError("Error", "Email is mandatory");
                //}
            }
            else
            {
                if (!EmailHelper.IsValidEmail(model.Email))
                    ModelState.AddModelError("Error", "Format email is wrong");
                else
                {
                    var checkEmail = Member.GetByEmail(model.Email);
                    if (checkEmail != null && checkEmail.ID != model.ID)
                        ModelState.AddModelError("Error", "Email has been used by other user");
                }
            }
            if (string.IsNullOrEmpty(model.Phone))
                ModelState.AddModelError("Error", "Phone is mandatory");
            else
            {
                if (!PhoneHelper.isPhoneValid(model.Phone))
                    ModelState.AddModelError("Error", "Format Phone is wrong");
                else
                {
                    var checkPhone = Member.GetByPhone(model.Phone);
                    if (checkPhone != null && checkPhone.ID != model.ID)
                        ModelState.AddModelError("Error", "Phone has been used by other user");
                }
            }

            if (!isEdit)
            {
                if (string.IsNullOrEmpty(model.Password))
                    ModelState.AddModelError("Error", "Password is mandatory");
                else if (model.Password.Length < 6)
                    ModelState.AddModelError("Error", "Password should not less than 6 characters");
                if (string.IsNullOrEmpty(model.ConfirmPassword))
                    ModelState.AddModelError("Error", "Confirm Password is mandatory");
                if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.ConfirmPassword) && model.Password != model.ConfirmPassword)
                    ModelState.AddModelError("Error", "Password and confirm password not match");

            }
            if (!string.IsNullOrEmpty(model.Gender))
            {
                if (model.Gender.ToLower() != "f" && model.Gender.ToLower() != "m")
                    ModelState.AddModelError("Error", "Gender format is wrong. Please use 'F' for Female and 'M' for Male");
            }
            if (!string.IsNullOrEmpty(model.KTPNumber))
            {
                if (!model.KTPNumber.All(char.IsDigit))
                    ModelState.AddModelError("Error", "KTP Number must be numeric");
            }
            if (!string.IsNullOrEmpty(model.FacebookID))
            {
                if (!model.FacebookID.All(char.IsDigit))
                    ModelState.AddModelError("Error", "Facebook ID must be numeric");

                DataMember.Member predictedMember = DataMember.Member.GetByFacebookId(model.FacebookID);
                if (isEdit)
                {
                    if (predictedMember != null && predictedMember.ID != model.ID)
                        ModelState.AddModelError("Error", string.Format("Facebook ID '{0}'  has been used by other user", model.FacebookID));
                }
                else if (predictedMember != null && predictedMember.ID > 0)
                    ModelState.AddModelError("Error", string.Format("Facebook ID '{0}'  has been used by other user", model.FacebookID));
            }
            if (!string.IsNullOrEmpty(model.KKNumber))
            {
                if (!model.KKNumber.All(char.IsDigit))
                    ModelState.AddModelError("Error", "KK number must be numeric");
            }
            if (string.IsNullOrEmpty(model.ZipCode))
            {
                // ModelState.AddModelError("ZipCode", "ZipCode is required");
            }
            else
            {
                if (model.ZipCode.Length < 5)
                {
                    ModelState.AddModelError("ZipCode", "ZipCode must 5 character");
                }
            }
            if (model.Childs != null && model.Childs.Count > 0)
            {

            }
            else
            {
                if (!string.IsNullOrEmpty(model.ChildBirthdate))
                {
                    DateTime dt = DateTime.MinValue;
                    if (!DateTime.TryParseExact(model.ChildBirthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        ModelState.AddModelError("Error", "Format Child's Birthdate is wrong");
                    }
                }
            }

            if (!string.IsNullOrEmpty(model.Channel))
            {
                Channel dataChannel = Channel.GetByCode(model.Channel.ToLower());
                if (dataChannel == null)
                {
                    ModelState.AddModelError("Channel", string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.web.ToString()));
                }
            }
        }

        private void RemoveTempDirAndFile(string Path)
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            string path = Path;
            if (Directory.Exists(path))
            {
                try
                {
                    DirectoryInfo tempdir = new DirectoryInfo(path);
                    foreach (FileInfo file in tempdir.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in tempdir.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    tempdir.Delete(true);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.InnerException.ToString());
                }
            }
        }

        private void CreateThumbnail(string OriginalFile, string FileName)
        {

            Image img = Image.FromFile(OriginalFile);
            int imgHeight = 100;
            int imgWidth = 100;
            if (img.Width < img.Height)
            {
                //portrait image
                imgHeight = 100;
                var imgRatio = (float)imgHeight / (float)img.Height;
                imgWidth = Convert.ToInt32(img.Height * imgRatio);
            }
            else if (img.Height < img.Width)
            {
                //landscape image
                imgWidth = 100;
                var imgRatio = (float)imgWidth / (float)img.Width;
                imgHeight = Convert.ToInt32(img.Height * imgRatio);
            }
            Image thumb = img.GetThumbnailImage(imgWidth, imgHeight, () => false, IntPtr.Zero);
            thumb.Save(FileName);
        }

        private void GetListStages()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Stages> listdata = Stages.GetAll();
            foreach (Stages data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListStages = items;
        }

        private void _getListProductBefore(bool? IDPregnancy, int? ID)
        {
            List<ProductBefore> listdata = ProductBefore.GetAll().OrderBy(x => x.ID).ToList();
            if (IDPregnancy.HasValue)
            {
                listdata = listdata.Where(x => x.IsPregnancyProduct == IDPregnancy.Value).ToList();
            }
            ViewBag.ListProductBefore = listdata;
        }

        private void _getListProductBeforeChild(bool? IDPregnancy, int? ID)
        {
            List<ProductBefore> listdata = ProductBefore.GetAll().OrderBy(x => x.ID).ToList();
            if (IDPregnancy.HasValue)
            {
                listdata = listdata.Where(x => x.IsPregnancyProduct == IDPregnancy.Value).ToList();
            }
            ViewBag.ListProductBeforeChild = listdata;
        }

        private void _getListStages()
        {
            List<Stages> listdata = Stages.GetAll().Where(x => x.IsActive).OrderBy(x => x.Name).ToList();

            ViewBag.ListStages = listdata;
        }

        /*
         * City Dari Database LoyaltyPointEngine.Data
         *
         */
        private void GetListCity()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoyaltyPointEngine.Data.City> listdata = LoyaltyPointEngine.Data.City.GetAll();
            foreach (LoyaltyPointEngine.Data.City data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListCity = items;
        }

        private void GetListSPG()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<SPG> listdata = SPG.GetAll().OrderBy(x => x.Code);
            string spgRoleCode;
            SPG thisSPG = null;
            if (this.IsSpgRole(out spgRoleCode))
            {
                thisSPG = SPG.GetByCode(CurrentUser.Username);
            }

            foreach (SPG data in listdata)
            {
                string text = data.Code;

                if (!string.IsNullOrEmpty(data.Name))
                {
                    text = text + " (" + data.Name + ")";
                }

                bool selected = false;
                if (thisSPG != null) if (data.ID == thisSPG.ID) selected = true;
                items.Add(new SelectListItem { Text = text, Value = data.ID.ToString(), Selected = selected });
            }
            ViewBag.ListSPG = items;
        }

        private void GetListChannel()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Channel> listdata = Channel.GetAll();
            foreach (Channel data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListChannel = items;
        }

        private void GetListProductBefore(string ID = "")
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<ProductBefore> listdata = ProductBefore.GetAll().OrderBy(x => x.ID);
            foreach (ProductBefore data in listdata)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = string.IsNullOrEmpty(ID) ? false : data.ID.ToString() == ID });
            }
            ViewBag.ListProductBefore = items;
        }

        public virtual ActionResult GetListProductBefore(bool? IsPregnancy, int? ID)
        {
            List<ProductBefore> listdata = ProductBefore.GetAll().OrderBy(x => x.ID).ToList();
            if (IsPregnancy.HasValue)
            {
                listdata = listdata.Where(x => x.IsPregnancyProduct == IsPregnancy.Value).ToList();
            }
            return Json(listdata, JsonRequestBehavior.AllowGet);
        }

        #region List Member Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllMember()
        {
            gridMvcHelper = new GridMvcHelper();
            // IOrderedQueryable<Member> allMember = Member.GetAll().OrderBy(x => x.FirstName);

            IQueryable<Member> rawData = Member.GetAll();

            string spgRoleCode;
            if (this.IsSpgRole(out spgRoleCode))
            {
                var spgUser = SPG.GetByCode(CurrentUser.Username);
                rawData = rawData.Where(x => x.SpgID == spgUser.ID);
            }

            int _totalRow = rawData.Count(),
                _limit = 5;

            IQueryable<MemberGridModel> datas = from a in rawData
                                                select new MemberGridModel
                                                {
                                                    ID = a.ID,
                                                    Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                                                    FirstName = a.FirstName,
                                                    LastName = a.LastName,
                                                    Email = a.Email,
                                                    Phone = a.Phone,
                                                    IsBlacklist = a.IsBlacklist,
                                                    Point = a.MemberPoint,
                                                    CreatedDate = a.CreatedDate,
                                                    Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                                                    Channel = a.Channel.Name
                                                };

            ViewBag.TotalPage = (int)Math.Ceiling((double)_totalRow / _limit);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllMemberPager(string search, string orderBy, string orderDirection, int? page)
        {
            //gridMvcHelper = new GridMvcHelper();
            int _page = page.GetValueOrDefault(1),
                _limit = 5,
                _totalRow = 0;
            string _orderBy = orderBy ?? "FirstName",
                _orderDirection = orderDirection ?? "Asc";
            if (_page < 1) _page = 1;


            List<MemberGridModel> members = new List<MemberGridModel>();
            if (!string.IsNullOrEmpty(search))
            {
                //var rawMember = Member.GetAll()
                //    .Where(x => x.FirstName.Contains(search) || x.LastName.Contains(search) || x.Email.Contains(search) || x.Phone.Contains(search) || (!string.IsNullOrEmpty(x.SMSID) && x.SMSID.Contains(search)) || x.Channel.Name.Contains(search))
                //    .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                IQueryable<Member> rawMember = Member.GetAll();
                if (PhoneHelper.isPhoneValid(search))
                {
                    rawMember = rawMember
                        .Where(x => x.Phone.Contains(search))
                        .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                }
                else if (EmailHelper.IsValidEmail(search))
                {
                    rawMember = rawMember
                        .Where(x => x.Email.Contains(search))
                        .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                }
                else
                {
                    rawMember = rawMember
                    .Where(x => x.FirstName.Contains(search) || x.LastName.Contains(search) || x.Channel.Name.Contains(search))
                    .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                }

                string spgRoleCode;
                if (this.IsSpgRole(out spgRoleCode))
                {
                    var spgUser = SPG.GetByCode(CurrentUser.Username);
                    rawMember = rawMember.Where(x => x.SpgID == spgUser.ID);
                }

                _totalRow = rawMember.Count();
                members = rawMember
                    .Skip((_page - 1) * _limit)
                    .Take(_limit)
                    .Select(x => new MemberGridModel
                    {
                        ID = x.ID,
                        Gender = x.Gender.ToLower() == "m" ? "Male" : "Female",
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Phone = x.Phone,
                        IsBlacklist = x.IsBlacklist,
                        Point = x.MemberPoint,
                        CreatedDate = x.CreatedDate,
                        Type = !string.IsNullOrEmpty(x.SMSID) ? "Sms" : "Web",
                        Channel = x.Channel.Name,
                        EventCode = x.EventCode,
                        IsActive = x.IsActive == true ? "YES" : "NO",
                        ActiveDate = x.ActiveDate
                    })
                    .ToList();
            }
            else
            {
                //var rawMember = Member.GetAll();
                var today = DateTime.Now;
                var startDate = today.AddDays(-7).Date;
                var endDate = today.Date;
                var rawMember = Member.GetMemberPastAWeek(startDate, endDate);

                string spgRoleCode;
                if (this.IsSpgRole(out spgRoleCode))
                {
                    var spgUser = SPG.GetByCode(CurrentUser.Username);
                    rawMember = rawMember.Where(x => x.SpgID == spgUser.ID);
                }

                var s = rawMember.ToList().Count;

                members = rawMember.OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection))
                    .Skip((_page - 1) * _limit)
                    .Take(_limit)
                    .Select(x => new MemberGridModel
                    {
                        ID = x.ID,
                        Gender = x.Gender.ToLower() == "m" ? "Male" : "Female",
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Phone = x.Phone,
                        IsBlacklist = x.IsBlacklist,
                        Point = x.MemberPoint,
                        CreatedDate = x.CreatedDate,
                        Type = !string.IsNullOrEmpty(x.SMSID) ? "Sms" : "Web",
                        Channel = x.Channel.Name,
                        EventCode = x.EventCode,
                        IsActive = x.IsActive == true ? "YES" : "NO",
                        ActiveDate = x.ActiveDate
                    })
                    .ToList();

                _totalRow = rawMember.Count();
            }
            ViewBag.Search = search;
            ViewBag.CurrentPage = _page;
            ViewBag.TotalPage = (int)Math.Ceiling((double)_totalRow / _limit);
            ViewBag.OrderColumn = _orderBy;
            ViewBag.OrderDirection = _orderDirection;
            ViewBag.TotalRow = _totalRow;
            return PartialView(MVC.Member.Views.GridGetAllMember, members);
        }

        /*
         * digunakan untuk partial popup select Member
         **/
        [ChildActionOnly]
        public virtual ActionResult GridGetSelectMember()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Member> allMember = Member.GetAll().OrderBy(x => x.FirstName);
            AjaxGrid<Member> grid = this.gridMvcHelper.GetAjaxGrid(allMember);
            return PartialView(grid);
        }

        //[HttpGet]
        public virtual ActionResult GridGetSelectMemberPager(string search, string orderBy, string orderDirection, int? page)
        {
            //gridMvcHelper = new GridMvcHelper();
            int _page = page.GetValueOrDefault(1),
                _limit = 5,
                _totalRow = 0;
            string _orderBy = orderBy ?? "FirstName",
                _orderDirection = orderDirection ?? "Asc";
            if (_page < 1) _page = 1;


            List<MemberGridModel> members = new List<MemberGridModel>();
            if (!string.IsNullOrEmpty(search))
            {
                IQueryable<Member> rawMember = Member.GetAll();
                if (PhoneHelper.isPhoneValid(search))
                {
                    rawMember = rawMember
                        .Where(x => x.Phone.Contains(search))
                        .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                }
                else if (EmailHelper.IsValidEmail(search))
                {
                    rawMember = rawMember
                        .Where(x => x.Email.Contains(search))
                        .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                }
                else
                {
                    rawMember = rawMember
                    .Where(x => x.FirstName.Contains(search) || x.LastName.Contains(search) || x.Channel.Name.Contains(search))
                    .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection));
                }

                members = rawMember
                    .Skip((_page - 1) * _limit)
                    .Take(_limit)
                    .Select(x => new MemberGridModel
                    {
                        ID = x.ID,
                        Gender = x.Gender.ToLower() == "m" ? "Male" : "Female",
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Phone = x.Phone,
                        IsBlacklist = x.IsBlacklist,
                        Point = x.MemberPoint,
                        CreatedDate = x.CreatedDate,
                        Type = !string.IsNullOrEmpty(x.SMSID) ? "Sms" : "Web",
                        Channel = x.Channel.Name,
                        IsActive = x.IsActive == true ? "YES" : "NO",
                        ActiveDate = x.ActiveDate
                    }).ToList();
                _totalRow = rawMember.Count();
                //members = rawMember
                //    .Skip((_page - 1) * _limit)
                //    .Take(_limit)
                //    .ToList();
            }
            else
            {
                members = null;
                _totalRow = 0;
            }
            ViewBag.Search = search;
            ViewBag.CurrentPage = _page;
            ViewBag.TotalPage = (int)Math.Ceiling((double)_totalRow / _limit);
            ViewBag.OrderColumn = _orderBy;
            ViewBag.OrderDirection = _orderDirection;
            return PartialView(MVC.Member.Views.GridGetSelectMember, members);
        }
        #endregion

        [HttpPost]
        public virtual ActionResult CheckavailabilityMember(int? ID, string KeyType, string Value)
        {

            bool IsAvailable = false;
            string Message = "";

            if (!string.IsNullOrEmpty(KeyType))
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    if (KeyType.ToLower() == "email")
                    {
                        if (EmailHelper.IsValidEmail(Value))
                        {
                            var data = !ID.HasValue ? Member.GetByEmail(Value) : Member.GetByEmailExceptID(ID.Value, Value);
                            IsAvailable = data == null ? true : false;
                        }
                        else
                        {
                            Message = "Value is not an Email format";
                        }


                    }
                    else
                    {
                        if (PhoneHelper.isPhoneValid(Value))
                        {
                            var data = !ID.HasValue ? Member.GetByPhone(Value) : Member.GetByPhoneExceptID(ID.Value, Value);
                            IsAvailable = data == null ? true : false;
                        }
                        else
                        {
                            Message = "Value is not an Phone format";
                        }

                    }

                    if (IsAvailable)
                    {
                        Message = "Value is available to used";
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Message))
                        {
                            Message = Message = (KeyType.ToLower() == "email") ? "email has been used by other user" : "phone has been used by other user";
                        }
                    }


                }
                else
                {
                    Message = "Value is required";
                }


            }
            else
            {
                Message = "KeyType is required";
            }





            return Json(new { IsAvailable = IsAvailable, Message = Message }, JsonRequestBehavior.AllowGet);
        }

        public void PopulateProvinces(string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = Province.GetAll().OrderBy(x => x.ProvinceName); ;
            foreach (var data in listdata)
            {

                items.Add(new SelectListItem { Text = data.ProvinceName, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListProvince = items;
        }

        public void PopulateDistrict(int ProvinceID, string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = District.GetAll().Where(x => x.ProvinceID == ProvinceID).OrderBy(x => x.DistrictName);
            foreach (var data in listdata)
            {
                items.Add(new SelectListItem { Text = data.DistrictName, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListDistrict = items;
        }

        public void PopulateSubDistrict(int DistrictID, string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = SubDistrict.GetAll().Where(x => x.DistrictID == DistrictID).OrderBy(x => x.SubDistrictName);
            foreach (var data in listdata)
            {
                items.Add(new SelectListItem { Text = data.SubDistrictName, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListSubDistrict = items;
        }

        [BreadCrumb(Clear = false, Label = "History")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult History(int MemberID)
        {
            string spgRoleCode;
            if (this.IsSpgRole(out spgRoleCode)) return RedirectToAction(MVC.Error.Index());

            var dataMember = DataMember.Member.GetById(MemberID);
            return View(dataMember);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = false, Label = "History")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult History(int memberID, string tglAwal, string tglAkhir)
        {
            var datas = Data.BalanceLog.GetByMemberId(memberID);

            if (!string.IsNullOrEmpty(tglAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(tglAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }
            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetHistoryPoint(datas.OrderByDescending(x => x.CreatedDate).ToList());
            fileName = string.Format("HistoryPoint_{0}.xlsx", Member.GetById(memberID).Phone);
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Type", "Description", "Points", "Balance",
                "CreatedDate"});
            excelStream.Position = 0;

            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        private List<List<string>> GetHistoryPoint(List<Data.BalanceLog> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                data.Add(item.Type);
                data.Add(item.Description);
                data.Add(item.Points.ToString());
                data.Add(item.Balance.ToString());
                data.Add(item.CreatedDate.ToString("dd/MM/yyyy"));

                datas.Add(data);
            }
            return datas;
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllMemberHistory(int MemberID)
        {
            gridMvcHelper = new GridMvcHelper();
            // IOrderedQueryable<Member> allMember = Member.GetAll().OrderBy(x => x.FirstName);

            var balanceLogs = Data.BalanceLog.GetByMemberId(MemberID);


            var distictRef = balanceLogs.Select(x => x.ReferenceID).Distinct();
            var distictType = balanceLogs.Select(x => x.Type).Distinct();

            //var collects = Data.Collect.GetAll();
            //var adjustments = Data.Adjustment.GetAll();



            var datas = Data.BalanceLog.GetByMemberId(MemberID)
                .GroupBy(x => new { Type = x.Type, ReferenceID = x.ReferenceID })
                .Select(x => new Model.Parameter.BalanceLog.BalanceLog
                {
                    Type = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().Type,
                    Description = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().Description,
                    Balance = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().Balance,
                    CreatedDate = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().CreatedDate,
                    Points = x.Sum(y => y.Points)
                })
                .OrderByDescending(x => x.CreatedDate);

            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllMemberHistoryPager(string txtSearch, int HiddenMemberID, int? page)
        {

            gridMvcHelper = new GridMvcHelper();

            var datas = Data.BalanceLog.GetByMemberId(HiddenMemberID)
                .GroupBy(x => new { Type = x.Type, ReferenceID = x.ReferenceID })
                .Select(x => new Model.Parameter.BalanceLog.BalanceLog
                {
                    Type = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().Type,
                    Description = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().Description,
                    Balance = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().Balance,
                    CreatedDate = x.OrderByDescending(z => z.CreatedDate).FirstOrDefault().CreatedDate,
                    Points = x.Sum(y => y.Points)
                })
                .OrderByDescending(x => x.CreatedDate);

            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.Description.Contains(txtSearch) || x.Type.Contains(txtSearch)).OrderByDescending(x => x.CreatedDate);
                //.OrderBy(x => x.FirstName);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Member.Views.GridGetAllMemberHistory, this);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [BreadCrumb(Clear = true, Label = "Upload Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Upload Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Upload(HttpPostedFileBase excelFile)
        {
            string url = Request.Url.AbsoluteUri;
            List<string> ListError = new List<string>();
            List<MemberGridModel> Listdata = new List<MemberGridModel>();  //ucup
            MemberGridModel resModel = new MemberGridModel();
            if (excelFile != null)
            {
                var rawDataFromExel = PopulateUploadModelFromExcelRawDataToModel(excelFile);
                if (rawDataFromExel != null && rawDataFromExel.Count > 0)
                {

                    var userLogin = CurrentUser;
                    string AppsName = userLogin.FullName;

                    int sukses = 0; //ucup
                    int gagal = 0; //ucup
                    string errorMessage = "";

                    foreach (var item in rawDataFromExel)
                    {
                        //Validasi data kosong
                        bool IsEmpty = false;
                        errorMessage = _IsMemberUploadDataEmpty(item, out IsEmpty);
                        if (!IsEmpty)
                        {
                            var dataMember = DataMember.Member.GetByPhoneOrEmail(item.phone, item.email);
                            if (dataMember == null)
                            {
                                int totalPoint = 0;
                                string notEncriptPassword = string.IsNullOrEmpty(item.password) ? StringHelper.GeneratedPassword() : item.password;
                                DateTime childDOB = DateTime.MinValue;//ucup

                                DateTime.TryParseExact(item.ChildDOB, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out childDOB);

                                DateTime bod = DateTime.MinValue;

                                DateTime.TryParseExact(item.momBOD, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out bod);

                                DateTime registerDate = DateTime.Now;

                                item.email = string.IsNullOrEmpty(item.email) ? item.phone + "@" + item.phone + ".com" : item.email;

                                dataMember = InsertNewMemberViaExcel(item, notEncriptPassword, bod, registerDate, userLogin.FullName, childDOB);

                                if (dataMember.ID > 0)
                                {
                                    sukses = sukses + 1;
                                    resModel = new MemberGridModel();
                                    resModel.FirstName = item.firstname;
                                    resModel.Email = item.email;
                                    resModel.Phone = item.phone;
                                    resModel.Message = "Insert member berhasil";
                                    Listdata.Add(resModel);

                                    var actionCode = Common.Point.ActionType.Code.REGMANUAL;
                                    if (!string.IsNullOrEmpty(item.spgCode))
                                    {
                                        DataMember.SPG spg = DataMember.SPG.GetByCode(item.spgCode);
                                        if (spg != null)
                                        {
                                            actionCode = Common.Point.ActionType.Code.REGNBA;
                                        }
                                    }
                                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                                    {
                                        var memberPoint = new Data.Member();
                                        memberPoint.ID = dataMember.ID;
                                        memberPoint.Name = string.Format("{0} {1}", dataMember.FirstName, dataMember.LastName).Trim();
                                        memberPoint.Email = dataMember.Email;
                                        memberPoint.Phone = dataMember.Phone;
                                        var responseMemberPoint = memberPoint.Insert(CurrentUser.FullName, url, CurrentUser.ID);

                                        if (responseMemberPoint.Success)
                                        {
                                            var ressInsertPOint = PointLogicLayer.Point.CollectLogic.InsertPointByActionCode(actionCode, dataMember, url);
                                            if (ressInsertPOint != null && ressInsertPOint.StatusCode == "00")
                                            {
                                                totalPoint = ressInsertPOint.Value.TotalPoints;
                                                transScope.Complete();
                                            }
                                            else
                                            {
                                                transScope.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            transScope.Dispose();
                                        }
                                    }
                                    string _point = item.Point + totalPoint;
                                    int point = 0;
                                    int.TryParse(_point, out point);
                                    if (point > 0)
                                    {

                                        //int MemberID = dataMember.ID;
                                        //var dataPool = Data.Pool.GetAll().FirstOrDefault(x => x.IsMainProcess);
                                        //int poolID = dataPool != null ? dataPool.ID : 1;

                                        ////insert balance log
                                        //var resultEF = LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Credit(point, "Upload By Admin", MemberID, poolID, AppsName, Guid.Empty, 0);
                                        //if (resultEF.Success == false)
                                        //{

                                        //    throw new System.ArgumentException("Parameter cannot be null", "original");
                                        //}

                                        //update member
                                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(dataMember.ID, AppsName);
                                    }

                                    var sendSMS = LoyaltyPointEngine.PointLogicLayer.OTP.OTPLogic.OTPMemberRegister(dataMember.Phone, url, dataMember.ID, "", true);
                                    if (sendSMS.StatusCode != "00")
                                    {
                                        resModel.Message = sendSMS.StatusMessage;
                                    }

                                    //string smsText = ConfigurationManager.AppSettings["SuccessRegisterSmsText"];
                                    //if (string.IsNullOrEmpty(smsText))
                                    //    smsText = "Kunjungi Bebeclub www.bebeclub.co.id/beberewards ID:{0} Password:{1}. SdanK:bit.ly/BebeSK Ketik:Bebeclub TIDAK#no.HP, kirim ke 2000 jika tdk setuju";

                                    //#region "Send SMS"
                                    //ResultModel<bool> resultSms = new ResultModel<bool>();
                                    //bool isSuccess = false;
                                    //string templateText = SiteSetting.ForgotPasswordSmsText;
                                    //string channelToLixus = SiteSetting.ChannelWeb;
                                    ////string paramMessage = string.Format(SiteSetting.SuccessRegisterSmsText, dataMember.Phone, string.Format(smsText, dataMember.Phone, notEncriptPassword));
                                    //string paramMessage = string.Format(SiteSetting.SuccessRegisterSmsText, dataMember.Phone, notEncriptPassword);
                                    //string smsParam = string.Format(SiteSetting.FormatMessageLixus, paramMessage, dataMember.Phone, channelToLixus);
                                    //string errSMSMsg = "";
                                    //LogSendType smsType = new LogSendType();
                                    //TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                                    //smsType.Value = LogSendType.RegisterMember.Value + " Upload LoyalCMS";

                                    //dynamic smsResponse = BlastSMS.SendSMSNotifOTP(smsType.Value, SiteSetting.ApiSendOTP, dataMember.Phone, smsParam, dataMember.Phone, SiteSetting.ApiSendOTP, 0, channelToLixus);
                                    //if (smsResponse != null)
                                    //{
                                    //    if (smsResponse.code != null)
                                    //    {
                                    //        if (smsResponse.code.ToString() == "7702")
                                    //            isSuccess = true;
                                    //        else
                                    //            errSMSMsg = SiteSetting.telcomnetAPIURLBebe + " " + smsResponse.code.ToString();
                                    //    }
                                    //    else
                                    //    {
                                    //        errSMSMsg = smsResponse.ToString();

                                    //        if (errSMSMsg.Contains("success"))
                                    //        {
                                    //            isSuccess = true;
                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //    errSMSMsg = "No response from sms API";
                                    //#endregion
                                }
                                else
                                {
                                    gagal++;
                                    resModel = new MemberGridModel();
                                    resModel.FirstName = item.firstname;
                                    resModel.Email = item.email;
                                    resModel.Phone = item.phone;
                                    resModel.Message = "Insert member gagal";
                                    Listdata.Add(resModel);
                                    continue;
                                }
                            }
                            else
                            {
                                gagal = gagal + 1; //ucup
                                resModel = new MemberGridModel();
                                resModel.FirstName = item.firstname;
                                resModel.Email = item.email;
                                resModel.Phone = item.phone;
                                resModel.Message = "Member sudah ada";
                                Listdata.Add(resModel);
                                //////Line By Ucup////////
                                //if (dataMember.MemberPoint.GetValueOrDefault() == 0)
                                //{
                                //    int point = 0;
                                //    string _point = item.Point;
                                //    int.TryParse(_point, out point);
                                //    if (point > 0)
                                //    {

                                //        int MemberID = dataMember.ID;
                                //        var dataPool = Data.Pool.GetAll().FirstOrDefault(x => x.IsMainProcess);
                                //        int poolID = dataPool != null ? dataPool.ID : 1;

                                //        //insert balance log
                                //        LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Credit(point, "Upload By Admin", MemberID, poolID, AppsName, Guid.Empty, 0);

                                //        //update member
                                //        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(MemberID, AppsName);
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            gagal++;
                            MemberGridModel errorModel = new MemberGridModel();
                            errorModel.FirstName = string.IsNullOrEmpty(item.firstname) ? "" : item.firstname;
                            errorModel.Email = item.email;
                            errorModel.Phone = item.phone;
                            errorModel.Message = errorMessage;
                            //if (string.IsNullOrEmpty(item.firstname))
                            //{
                            //    errorModel.Message += "firstname member harus diisi";
                            //}
                            //if (string.IsNullOrEmpty(item.phone))
                            //{
                            //    errorModel.Message += (!string.IsNullOrEmpty(errorModel.Message) ? "<br/>" : "") + "phone member harus diisi";
                            //}
                            //if (string.IsNullOrEmpty(item.ChildDOB))
                            //{
                            //    errorModel.Message += (!string.IsNullOrEmpty(errorModel.Message) ? "<br/>" : "") + "ChildDOB member harus diisi";
                            //}
                            Listdata.Add(errorModel);
                        }
                    }
                    //end foreach
                    //ucup
                    ViewBag.success = "Sukses, Member success = " + sukses + " and Member failed = " + gagal;
                    ViewBag.TotalData = sukses + gagal;
                    return View(Listdata);
                }

            }
            ViewBag.errors = ListError;
            return View();
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/UploadMemberExcelFormat.xlsx"));
            string fileName = "MemberTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult GetImage(string url)
        {
            var dir = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MemberUploadPath);
            var path = dir + url;
            var exists = ImageHelper.IsExists(path);
            if (!exists)
            {
                var dir2 = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
                if (url.Substring(0, 7).ToLower().Equals("/member"))
                {
                    url = url.Substring(7);
                }
                path = dir2 + url;
            }
            return base.File(path, "image/jpeg");
        }

        private List<MemberUploadTemplateModel> PopulateUploadModelFromExcelRawDataToModel(HttpPostedFileBase excelFile)
        {
            List<MemberUploadTemplateModel> rawDataFromExel = new List<MemberUploadTemplateModel>();
            ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
            List<string> headers = listData.Headers;
            List<List<string>> datas = listData.DataRows;

            MemberUploadTemplateModel excelModel = new MemberUploadTemplateModel();
            List<string> listHeader = null;
            Dictionary<string, int> HeaderDict = null;

            listHeader = ExcelHelper.ObjectToList(excelModel);
            if (listHeader != null && headers != null)
                HeaderDict = ExcelHelper.ModeltoDictionary(listHeader, headers);


            foreach (List<string> data in datas)
            {
                ExcelHelper.CheckData(data, headers);
                MemberUploadTemplateModel newData = null;
                List<string> rawdata = data;
                newData = ConvertRawDatatoObject(rawdata, HeaderDict);
                if (newData != null)
                {
                    rawDataFromExel.Add(newData);
                }
            }


            return rawDataFromExel;

        }

        private MemberUploadTemplateModel ConvertRawDatatoObject(List<string> rawdata, Dictionary<string, int> dataKey)
        {
            MemberUploadTemplateModel model = new MemberUploadTemplateModel();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(model))
            {
                if (descriptor != null && descriptor.Name != null)
                {
                    if (dataKey.ContainsKey(descriptor.Name))
                    {
                        int index = dataKey[descriptor.Name];
                        switch (descriptor.PropertyType.Name)
                        {
                            case "Int32":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt32(rawdata[index]) : 0);
                                break;
                            case "Int64":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt64(rawdata[index]) : 0);
                                break;
                            case "Decimal":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToDecimal(rawdata[index]) : 0);
                                break;
                            default: //string
                                descriptor.SetValue(model, rawdata[index]);
                                break;

                        }
                    }
                }
            }
            return model;
        }

        private DataMember.Member InsertNewMemberViaExcel(MemberUploadTemplateModel data, string password, DateTime momBOD, DateTime momRegisterDate, string AppsName, DateTime childDOB)
        {
            var userLogin = CurrentUser;
            ///Modify By Ucup
            //var datachannel = DataMember.Channel.GetByCode(ChannelType.web.ToString());
            //string channel = "mass upload";
            var IDchannel = DataMember.Channel.GetByCode(string.IsNullOrEmpty(data.Channel) ? "mass upload" : data.Channel);
            if (IDchannel == null)
            {
                IDchannel = DataMember.Channel.GetByCode("mass upload");
            }

            DataMember.Member dataMember = new DataMember.Member();
            //dataMember.ChannelId = datachannel != null ? datachannel.ID : DataMember.Channel.GetAll().FirstOrDefault().ID;
            dataMember.ChannelId = IDchannel.ID;
            //dataMember.FacebookID = fbid;
            dataMember.Gender = "F";
            dataMember.FirstName = data.firstname ?? "";
            dataMember.LastName = data.lastname ?? "";
            dataMember.Email = data.email;
            dataMember.StagesID = 8;
            //if (string.IsNullOrEmpty(data.email))
            //{
            //    if (!string.IsNullOrEmpty(data.phone))
            //    {
            //        dataMember.Email = string.Format("{0}@{0}.com", data.phone);
            //    }
            //    //else
            //    //{
            //    //    if (!string.IsNullOrEmpty(fbid))
            //    //    {
            //    //        dataMember.Email = string.Format("{0}@facebook.com", fbid);
            //    //    }
            //    //}
            //}

            if (!string.IsNullOrEmpty(data.agepregnant) && childDOB != DateTime.MinValue)
            {
                dataMember.AgePregnant = Convert.ToInt32(data.agepregnant);
                dataMember.StagesID = 9;
            }

            if (string.IsNullOrEmpty(data.agepregnant) && childDOB != DateTime.MinValue)
            {
                dataMember.StagesID = 10;
            }

            if (!string.IsNullOrEmpty(data.agepregnant) && childDOB == DateTime.MinValue)
            {
                dataMember.AgePregnant = Convert.ToInt32(data.agepregnant);
                dataMember.StagesID = 7;
            }
            dataMember.Phone = data.phone;
            dataMember.Password = !string.IsNullOrEmpty(password) ? Common.Encrypt.LoyalEncript(password) : "";
            dataMember.Address = data.address ?? "";

            if (!string.IsNullOrEmpty(data.spgCode))
            {
                DataMember.SPG spg = DataMember.SPG.GetByCode(data.spgCode);
                if (spg != null)
                {
                    dataMember.SpgID = spg.ID;
                }
                //else
                //{
                //    //insert SPG dulu
                //    spg = new DataMember.SPG();
                //    spg.Code = spgCode;

                //    spg.Name = spgName;
                //    spg.OutletName = OutletName;
                //    spg.LeaderName = spgLeaderName;

                //    spg.Inserted(AppsName);
                //    dataMember.SpgID = spg.ID;
                //}
            }

            //AREA IGNORE AJA


            dataMember.Birthdate = momBOD;
            dataMember.PlaceOfBirth = data.BirthPlace ?? "";
            dataMember.EventCode = data.EventCode;

            var isPregnancyProduct = false;
            int productBeforeId = 0;
            if (!string.IsNullOrWhiteSpace(data.ProductBefore))
            {
                if (int.TryParse(data.ProductBefore, out productBeforeId))
                {
                    var productBefore = ProductBefore.GetById(productBeforeId);
                    if (productBefore != null)
                    {
                        isPregnancyProduct = productBefore.IsPregnancyProduct;
                    }
                }

            }

            if ((dataMember.StagesID == 7 || dataMember.StagesID == 9) && isPregnancyProduct)
            {
                dataMember.ProductBeforeID = productBeforeId;
            }
            dataMember.MigrationInserted(AppsName, momRegisterDate);

            //get memberid if success created
            DataMember.Member memberObj = DataMember.Member.GetByPhone(dataMember.Phone);
            //create child if exist
            if (memberObj != null && (dataMember.StagesID == 9 || dataMember.StagesID == 10))
            {  //ucup
                DataMember.Child childObj = new DataMember.Child();
                childObj.ParentID = memberObj.ID;
                childObj.Name = data.ChildName ?? "";
                childObj.PlaceOfBirth = data.Placeofbirth ?? "";  //ucup
                childObj.Birthdate = childDOB;
                childObj.Gender = "F";
                childObj.IsDeleted = false;
                //childObj.ProductBeforeID = Convert.ToInt32(data.ProductBefore);
                if (!isPregnancyProduct)
                {
                    childObj.ProductBeforeID = productBeforeId;
                }
                childObj.Inserted(CurrentUser.Username);
            }


            return dataMember;
        }

        private void PopulateChannel()
        {
            ViewBag.ChannelList = EnumHelper.ToSelectList<ChannelType>();
        }

        public virtual ActionResult GetAllChild(int parentId)
        {
            var result = MemberLogic.GetAllChild(parentId);
            return Json(new { list = result.Value, statusCode = result.StatusCode }, JsonRequestBehavior.AllowGet);
        }

        private string _IsMemberUploadDataEmpty(MemberUploadTemplateModel data, out bool IsEmpty)
        {
            StringBuilder errorMessage = new StringBuilder();
            IsEmpty = false;

            if (string.IsNullOrEmpty(data.firstname))
            {
                IsEmpty = true;
                errorMessage.Append("firstname member harus diisi <br />");
            }

            if (string.IsNullOrEmpty(data.lastname))
            {
                IsEmpty = true;
                errorMessage.Append("lastname member harus diisi <br />");
            }

            if (string.IsNullOrEmpty(data.email))
            {
                IsEmpty = true;
                errorMessage.Append("email member harus diisi <br />");
            }
            else
            {
                if (!EmailHelper.IsValidEmail(data.email))
                {
                    IsEmpty = true;
                    errorMessage.Append("format email member tidak valid <br />");
                }
            }

            if (string.IsNullOrEmpty(data.phone))
            {
                IsEmpty = true;
                errorMessage.Append("phone member harus diisi <br />");
            }
            else
            {
                if (!PhoneHelper.isPhoneValid(data.phone))
                {
                    IsEmpty = true;
                    errorMessage.Append("format phone member tidak valid <br />");
                }
            }

            if (!string.IsNullOrEmpty(data.ChildName) && string.IsNullOrEmpty(data.ChildDOB))
            {
                IsEmpty = true;
                errorMessage.Append("jika childname diisi, childDOB harus diisi <br />");
            }

            if (string.IsNullOrEmpty(data.ChildName) && !string.IsNullOrEmpty(data.ChildDOB))
            {
                IsEmpty = true;
                errorMessage.Append("jika childDOB diisi, childname harus diisi <br />");
            }

            long productBeforeID = 0;
            bool productBeforeIsANumber = Int64.TryParse(data.ProductBefore, out productBeforeID);
            if (!string.IsNullOrEmpty(data.ChildDOB) || !string.IsNullOrEmpty(data.agepregnant))
            {
                if (string.IsNullOrEmpty(data.ProductBefore) && !productBeforeIsANumber)
                {
                    IsEmpty = true;
                    errorMessage.Append("productbefore member harus diisi <br />");
                }
                else
                {
                    var productBefore = DataMember.ProductBefore.GetById(productBeforeID);
                    if (productBefore == null || productBefore.ID == 0)
                    {
                        IsEmpty = true;
                        errorMessage.Append("productbefore member tidak valid <br />");
                    }
                    else
                    {
                        if (productBefore.IsPregnancyProduct && string.IsNullOrEmpty(data.agepregnant) && string.IsNullOrEmpty(data.ChildDOB))
                        {
                            IsEmpty = true;
                            errorMessage.Append(string.Format("productbefore {0} untuk ibu hamil <br />", productBefore.Name));
                        }
                        else if (productBefore.IsPregnancyProduct && string.IsNullOrEmpty(data.agepregnant) && !string.IsNullOrEmpty(data.ChildDOB))
                        {
                            IsEmpty = true;
                            errorMessage.Append(string.Format("productbefore {0} untuk ibu hamil <br />", productBefore.Name));
                        }
                        else if (!productBefore.IsPregnancyProduct && !string.IsNullOrEmpty(data.agepregnant) && string.IsNullOrEmpty(data.ChildDOB))
                        {
                            IsEmpty = true;
                            errorMessage.Append(string.Format("productbefore {0} untuk ibu yang memiliki anak <br />", productBefore.Name));
                        }
                        else if (!productBefore.IsPregnancyProduct && string.IsNullOrEmpty(data.agepregnant) && string.IsNullOrEmpty(data.ChildDOB))
                        {
                            IsEmpty = true;
                            errorMessage.Append(string.Format("productbefore {0} untuk ibu yang memiliki anak <br />", productBefore.Name));
                        }
                    }
                }
            }

            return errorMessage.ToString();
        }

        [UserActionFilter]
        public virtual ActionResult VerifyChildCollect(int ID, int MemberID)
        {
            ResultModel<ChildModel> result = ChildLogic.CollectCount(ID);
            return RedirectToAction(MVC.Member.Edit(MemberID, result.StatusCode, result.StatusMessage));
        }
    }
}