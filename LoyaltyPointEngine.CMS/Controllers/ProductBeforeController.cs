﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.DataMember;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Common;
using System.IO;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ProductBeforeController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Product Before")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Product successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Product successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Product successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }




        [BreadCrumb(Label = "Detail Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(long id)
        {
            var data = ProductBefore.GetById(id);
            ProductBeforeModel model = new ProductBeforeModel(data);

            return View(model);
        }


        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ProductBeforeModel model)
        {
            ValidateEdit(model);

            if (ModelState.IsValid)
            {


                string by = CurrentUser.FullName;
                ProductBefore data = ProductBefore.GetById(model.ID);
                if (data != null)
                {
                    data.Name = model.Name;
                    data.IsPregnancyProduct = model.IsPregnancyProduct;

                    var ressEF = data.Updated(by);
                    if (ressEF.Success)
                    {
                        return RedirectToAction(MVC.ProductBefore.Index("Edit_Success"));
                    }
                    else
                    {
                        ModelState.AddModelError("", ressEF.ErrorMessage);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "ID not founded");
                }


            }

            return View(model);
        }

        private void ValidateEdit(ProductBeforeModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }
        }


        [BreadCrumb(Label = "Add Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ProductBeforeModel model = new ProductBeforeModel();
            return View();
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ProductBeforeModel model)
        {
            ValidateAdd(model);

            if (ModelState.IsValid)
            {
                string by = CurrentUser.FullName;
                ProductBefore data = new ProductBefore();
                data.Name = model.Name;
                data.IsPregnancyProduct = model.IsPregnancyProduct;
                var ressEF = data.Inserted(by);
                if (ressEF.Success)
                {
                    return RedirectToAction(MVC.ProductBefore.Index("Create_Success"));
                }
                else
                {
                    ModelState.AddModelError("", ressEF.ErrorMessage);
                }

            }

            return View(model);
        }

        private void ValidateAdd(ProductBeforeModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }

        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllProduct()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<LoyaltyPointEngine.DataMember.ProductBefore> datas = LoyaltyPointEngine.DataMember.ProductBefore.GetAll().OrderBy(x => x.ID);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView("GridGetAllProductBefore", grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllProductPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = LoyaltyPointEngine.DataMember.ProductBefore.GetAll().OrderBy(x => x.ID);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.Name.Contains(txtSearch)
                    ).OrderBy(x => x.ID);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.ProductBefore.Views.GridGetAllProductBefore, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}