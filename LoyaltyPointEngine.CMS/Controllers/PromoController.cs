﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.Promo;
using LoyaltyPointEngine.PointLogicLayer.Promo;
using MvcBreadCrumbs;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class PromoController : BaseController
    {
        // GET: Promo
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Promo")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Promo successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Promo successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Promo successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        public virtual ActionResult Add()
        {
            PromoModel model = new PromoModel();
            GetListPromo();
            ViewBag.HeadTitle = "Add Promo";
            ViewBag.Channel = EnumHelper.ToSelectList<ChannelType>(true, "0");
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddJson()
        {
            bool isSuccess = false;
            string message = string.Empty, error = string.Empty;
            try
            {
                PromoModel model = GeneratePromoObjectFromRequest(out error);
                if (string.IsNullOrEmpty(error))
                {
                    string createdby = CurrentUser.FullName;
                    var code = Number.Generated(Number.Type.RCP, createdby);
                    Guid newID = Guid.NewGuid();
                    var result = PromoLogic.InsertFromCMS(model, CurrentPool.ID, createdby, Request.Url.AbsoluteUri,CurrentUser.ID);
                    if (result.StatusCode == "00")
                    {
                        isSuccess = true;
                        message = result.StatusMessage;
                    }
                    else
                    {
                        message = result.StatusMessage;
                    }
                }
                else
                {
                    message = error;
                }
                return Json(new { IsSuccess = isSuccess, Message = message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message, IsNeedRetry = true }, JsonRequestBehavior.AllowGet);
            }
        }


        [BreadCrumb(Label = "Detail")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Detail(int id, string status="")

        {
            ViewBag.Success = status;
            IQueryable<PromoDetails> pmd = Data.PromoDetails.GetAll().Where(x=>x.IDPromo==id);
            PromoDetailModel pmdm = new PromoDetailModel();
            pmd.InjectFrom(pmdm);
            ViewBag.titleHead = string.Format("Detail Promo");
            ViewBag.IdPromo = id;
            return View(pmdm);

        }

        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/PromoExcelFormat.xlsx"));
            string fileName = "PromoExcelFormat.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public virtual ActionResult Edit(int id)
        {
            Promo currentPromo = LoyaltyPointEngine.Data.Promo.GetById(id);
            PromoModel model = new PromoModel();
            model.InjectFrom(currentPromo);
            model.StartDate = currentPromo.StartDate.ToString("dd/MM/yyyy");
            model.EndDate = currentPromo.EndDate.ToString("dd/MM/yyyy");
            model.CreatedDate = currentPromo.CreatedDate.HasValue? currentPromo.CreatedDate.Value.ToString("dd/MM/yyyy"):string.Empty;
            model.UpdateDate = currentPromo.UpdateDate.HasValue ? currentPromo.UpdateDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            model.IsActive = currentPromo.IsActive.Value;
            model.IsCheck = currentPromo.IsCheck.Value;
            model.ReasonUpdate = string.IsNullOrEmpty(currentPromo.ReasonUpdate) ? string.Empty : currentPromo.ReasonUpdate;
            model.ChannelId = currentPromo.ChannelId.GetValueOrDefault(0);
            model.Status = currentPromo.Status;
            model.MsgResponseStatus = currentPromo.MsgResponseForStatus;
            model.IsVoucher = currentPromo.IsVoucher.HasValue ? currentPromo.IsVoucher.Value : false;
            ViewBag.HeadTitle = "Edit Promo";
            ViewBag.Channel = EnumHelper.ToSelectList<ChannelType>(true, model.ChannelId.ToString());
            return View(model);
        }

        public virtual ActionResult EditDetail(int id)
        {
            PromoDetails currentPromo = LoyaltyPointEngine.Data.PromoDetails.GetById(id);
            PromoDetailModel model = new PromoDetailModel();
            model.InjectFrom(currentPromo);

            model.CreatedDate = currentPromo.CreatedDate.HasValue ? currentPromo.CreatedDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            model.UpdatedDate = currentPromo.UpdateDate.HasValue ? currentPromo.UpdateDate.Value.ToString("dd/MM/yyyy") : string.Empty;
            model.IsUsed = currentPromo.isUsed;
            ViewBag.HeadTitle = "Edit Promo Detali";
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(PromoModel model)
        {
            this.IsValidPromo(model, true);
            if (ModelState.IsValid)
            {
                Promo promo = LoyaltyPointEngine.Data.Promo.GetById(model.ID);
                promo.InjectFrom(model);
                promo.ChannelId = model.ChannelId;
                promo.MsgResponseUnderOneYear = model.MsgResponseUnderOneYear;
                promo.MsgResponseForStatus = model.MsgResponseStatus;
                promo.IsVoucher = model.IsVoucher;
                promo.IsActive = model.IsActive;
                promo.IsCheck = model.IsCheck;


                DateTime dt = DateTime.Now;
                if (model.EndDate != null && DateTime.TryParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    promo.StartDate = dt;
                }

                dt = DateTime.Now;
                if (model.EndDate != null && DateTime.TryParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    promo.EndDate = dt;
                }

                var result = promo.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Promo.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListPromo();
            return View(model);
        }

        private void GetListPromo()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Promo> listdata = LoyaltyPointEngine.Data.Promo.GetAll();
            foreach (Promo data in listdata)
            {

                items.Add(new SelectListItem { Text = data.PromoCode, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListArea = items;
        }

        public virtual ActionResult GridGetAllPromo()
        {
            string spgCode = "";
            bool isSpg = this.IsSpgRole(out spgCode);

            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Promo> allPromo = LoyaltyPointEngine.Data.Promo.GetAll().OrderByDescending(x => x.ID);
            AjaxGrid<Promo> grid = this.gridMvcHelper.GetAjaxGrid(allPromo);
            return PartialView(grid);
        }

        public virtual ActionResult GridGetAllPromoDetail(int idp)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<PromoDetails> allPromoDetail = LoyaltyPointEngine.Data.PromoDetails.GetAll().Where(x=>x.IDPromo==idp).OrderByDescending(x => x.ID);
            AjaxGrid<PromoDetails> grid = this.gridMvcHelper.GetAjaxGrid(allPromoDetail);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllPromoPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Promo> allData = LoyaltyPointEngine.Data.Promo.GetAll().OrderBy(x => x.PromoCode);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.PromoCode.Contains(txtSearch)).OrderBy(x => x.PromoCode);
            }
            AjaxGrid<Promo> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Promo.Views.GridGetAllPromo, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GridGetAllPromoDetailPager(string txtSearch,int idp, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<PromoDetails> allData = LoyaltyPointEngine.Data.PromoDetails.GetAll().Where(x => x.IDPromo == idp).OrderBy(x => x.CodeName);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.CodeName.Contains(txtSearch)).OrderBy(x => x.CodeName);
            }
            AjaxGrid<PromoDetails> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Promo.Views.GridGetAllPromoDetail, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private void IsValidPromo(PromoModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.PromoCode))
            {
                Data.Promo predictedData = Data.Promo.GetByCode(Model.PromoCode);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("Promo Code '{0}'  has been used by other Promo", Model.PromoCode));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("Promo Code  '{0}'  has been used by other Promo", Model.PromoCode));
            }
        }

        public virtual JsonResult CheckDuplicatePromoCode(string promoCode)
        {
            string output = "";
            try
            {
                //var duplicatereceipt = Receipt.GetByReceiptCode(ReceiptCode);
                if (Promo.GetByCode(promoCode)!=null)
                {
                    output = string.Format("Receipt Code {0} already exist in database", promoCode);
                }
            }
            catch { }

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        private PromoModel GeneratePromoObjectFromRequest(out string error)
        {
            bool isError = true;
            error = "<ul>";

            PromoModel model = new PromoModel();
            model.PromoCode = Request.Form["PromoCode"];
            model.IsCheck = Request.Form["IsCheck"].ToString().ToLower() == "true" ? true : false;
            model.IsActive = true;
            model.Description = Request.Form["Description"];
            model.EndDate = Request.Form["EndDate"];
            model.StartDate = Request.Form["StartDate"];
            model.MsgResponse = Request.Form["MsgResponse"];
            model.MsgResponseFailedUnikKodeExp = Request.Form["MsgResponseFailedUnikKodeExp"];
            model.MsgResponseFailedUnikKodeNotFound = Request.Form["MsgResponseFailedUnikKodeNotFound"];
            model.MsgResponseFailedUnikKodeUsed = Request.Form["MsgResponseFailedUnikKodeUsed"];
            model.interval = Request.Form["Interval"];
            model.Status = Request.Form["Status"];
            model.IsVoucher = Request.Form["IsVoucher"].ToString().ToLower() == "true" ? true : false;
            model.MsgResponseStatus = Request.Form["MsgResponseStatus"];

            int channelId = 0;
            model.ChannelId = int.TryParse(Request.Form["ChannelId"], out channelId) ? channelId : channelId;

            if (string.IsNullOrWhiteSpace(model.PromoCode))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, Promo Code Must Not Empty</li>");
            }
            if (string.IsNullOrWhiteSpace(model.EndDate))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, Promo Code Must Not Empty</li>");
            }
            if (string.IsNullOrWhiteSpace(model.StartDate))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, Promo Code Must Not Empty</li>");
            }
            if (string.IsNullOrWhiteSpace(model.MsgResponse))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, Msg Response Success Must Not Empty</li>");
            }
            if (string.IsNullOrWhiteSpace(model.MsgResponseFailedUnikKodeExp))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, Msg Response Failed Unik Kode Exp Must Not Empty</li>");
            }
            if (string.IsNullOrWhiteSpace(model.MsgResponseFailedUnikKodeNotFound))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, Msg Response Failed Unik Kode Not Found Must Not Empty</li>");
            }
            if (string.IsNullOrWhiteSpace(model.MsgResponseFailedUnikKodeUsed))
            {
                isError = false;
                error += string.Format("<li>Wrong Format Input, MsgResponse Failed Unik Kode Already Used  Must Not Empty</li>");
            }
            double temp = 0;
            if (!string.IsNullOrEmpty(model.interval) && !double.TryParse(model.interval, out temp))
            {
                isError = false;
                error += string.Format("<li>Interval must be number</li>");
            }
            if (model.Status != "All")
            {
                if (string.IsNullOrEmpty(model.MsgResponseStatus))
                {
                    error += string.Format("<li>Wrong Format Input, Msg Response {0} Must Not Empty</li>", model.Status);
                }
            }


            error += "</ul>";
            if (isError) error = "";
            return model;
        }

        [BreadCrumb(Label = "Upload Detail")]
        public virtual ActionResult UploadFromExel(HttpPostedFileBase excelFile, int id)
        {
            string prmCode = Data.Promo.GetById(id).PromoCode;
            int succ = 0, fail = 0, err = 0;
            string currentUserName = CurrentUser.FullName;
            long CurrentUserID = CurrentUser.ID;
            var rawDataFromExel = PopulateFromExcelRawDataToModel(excelFile);
            List<UploadReceiptFromExcelResultCMSModel> listReceiptResultModel = rawDataFromExelToUploadReceiptFromExcelResultCMSModel(rawDataFromExel);
            //upload promo detail
            //listReceiptResultModel = listReceiptResultModel.Where(x => x.PromoCode == prmCode).ToList();
            foreach (UploadReceiptFromExcelResultCMSModel pmDetail in listReceiptResultModel)
            {
                //.InsertFromExcelCMS(receipt, newID, CurrentPool.ID, currentUserName, Request.Url.AbsoluteUri, CurrentUserID, code);
                try
                {
                    PromoDetails prom = new PromoDetails();
                    DataMember.District district = null;
                    Retailer retailer = null;

                    if (string.IsNullOrEmpty(pmDetail.UniqueCode))
                    {
                        pmDetail.result = "Failed";
                        pmDetail.message = "Kolom Kode Unik Tidak Boleh Kosong";
                    }
                    else
                    {
                        prom = PromoDetails.GetByCode(pmDetail.UniqueCode);
                    }

                    if (prom != null)
                    {
                        //promo sudah ada
                        pmDetail.result = "Failed";
                        pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Kode Unik Sudah Terdaftar" : ", Kode Unik Sudah Terdaftar";
                    }

                    if (pmDetail.ExpiredDate == null)
                    {
                        //Expired Date null
                        pmDetail.result = "Failed";
                        pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Kolom Expired Date Tidak Boleh Kosong" : ", Kolom Expired Date Tidak Boleh Kosong";
                    }

                    if (!string.IsNullOrEmpty(pmDetail.PromoCode))
                    {
                        if (pmDetail.PromoCode != prmCode)
                        {
                            //promo Code Berbeda
                            pmDetail.result = "Failed";
                            pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Promo Code Salah" : ", Promo Code Salah";
                        }
                    }
                    else
                    {
                        //promo Code Berbeda
                        pmDetail.result = "Failed";
                        pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Promo Code Tidak Boleh Kosong" : ", Promo Code Tidak Boleh Kosong";
                    }

                    
                    // Domisili check
                    if (!string.IsNullOrEmpty(pmDetail.Domisili))
                    {
                        district = DataMember.District
                            .GetAll()
                            .FirstOrDefault(x => x.DistrictName == pmDetail.Domisili);

                        if (district == null)
                        {
                            pmDetail.result = "Failed";
                            pmDetail.message += string.IsNullOrEmpty(pmDetail.message)
                                ? "Domisili Tidak Valid"
                                : ", Domisili Tidak Valid";
                        }
                    }

                    // Retailer check
                    if (!string.IsNullOrEmpty(pmDetail.Retailer))
                    {
                        retailer = Retailer
                            .GetAll()
                            .FirstOrDefault(x => x.Name == pmDetail.Retailer);

                        if (retailer == null)
                        {
                            pmDetail.result = "Failed";
                            pmDetail.message += string.IsNullOrEmpty(pmDetail.message)
                                ? "Retailer Tidak Valid"
                                : ", Retailer Tidak Valid";
                        }
                    }

                    // ExpiredPromo check
                    if (string.IsNullOrEmpty(pmDetail.ExpiredPromo))
                    {
                        pmDetail.result = "Failed";
                        pmDetail.message += string.IsNullOrEmpty(pmDetail.message)
                            ? "Kolom ExpiredPromo Tidak Boleh Kosong"
                            : ", Kolom ExpiredPromo Tidak Boleh Kosong";
                    }

                    if (string.IsNullOrEmpty(pmDetail.result))
                    {
                        prom = new PromoDetails();
                        prom.CodeName = pmDetail.UniqueCode;
                        prom.IDPromo = id;
                        prom.ExpiredDate = pmDetail.ExpiredDate;
                        if(district != null)
                        {
                            prom.DistrictID = district.ID;
                            prom.DistrictName = district.DistrictName;
                        }
                        if(retailer != null)
                        {
                            prom.RetailerID = retailer.ID;
                        }
                        prom.ExpiredPromo = pmDetail.ExpiredPromo;

                        var res = prom.Insert(currentUserName, Request.Url.AbsoluteUri, CurrentUserID);
                        if (res.Success)
                        {
                            succ++;
                            pmDetail.result = "Success";
                            pmDetail.message = "Kode Unik Berhasil Diinput";
                        }
                        else
                        {
                            fail++;
                            pmDetail.result = "Success";
                            pmDetail.message = "Kode Unik Berhasil Diinput";
                        }
                    }

                }
                catch (Exception ed)
                {
                    err++;
                }

            }
            //RedirectToAction(MVC.Promo.Detail(id, string.Format("Upload data Promo Detail Succes: {0}, Failed: {1}, Error: {3}, total: {2} data.", succ, fail, listReceiptResultModel.Count(),err)));
            ViewBag.IdProm = id;
            return View(listReceiptResultModel);
        }

        private List<UploadReceipFromExcelCMSModel> PopulateFromExcelRawDataToModel(HttpPostedFileBase excelFile)
        {
            List<UploadReceipFromExcelCMSModel> rawDataFromExel = new List<UploadReceipFromExcelCMSModel>();
            ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile, 10000);
            List<string> headers = listData.Headers;
            List<List<string>> datas = listData.DataRows;

            UploadReceipFromExcelCMSModel excelModel = new UploadReceipFromExcelCMSModel();
            List<string> listHeader = null;
            Dictionary<string, int> HeaderDict = null;

            listHeader = ExcelHelper.ObjectToList(excelModel);
            if (listHeader != null && headers != null)
                HeaderDict = ExcelHelper.ModeltoDictionary(listHeader, headers);


            foreach (List<string> data in datas)
            {
                ExcelHelper.CheckData(data, headers);
                UploadReceipFromExcelCMSModel newData = null;
                List<string> rawdata = data;
                newData = ConvertRawDatatoObject(rawdata, HeaderDict);
                if (newData != null)
                {
                    rawDataFromExel.Add(newData);
                }
            }


            return rawDataFromExel;

        }

        private UploadReceipFromExcelCMSModel ConvertRawDatatoObject(List<string> rawdata, Dictionary<string, int> dataKey)
        {
            UploadReceipFromExcelCMSModel model = new UploadReceipFromExcelCMSModel();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(model))
            {
                if (descriptor != null && descriptor.Name != null)
                {
                    if (dataKey.ContainsKey(descriptor.Name))
                    {
                        int index = dataKey[descriptor.Name];
                        if (descriptor.Name.ToLower() == "expireddate")
                        {
                            DateTime dt = new DateTime();

                            if (DateTime.TryParseExact(rawdata[index], "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                                descriptor.SetValue(model, dt);
                        }
                        else
                        {
                            switch (descriptor.PropertyType.Name)
                            {
                                case "Int32":
                                    descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt32(rawdata[index]) : 0);
                                    break;
                                case "Int64":
                                    descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt64(rawdata[index]) : 0);
                                    break;
                                case "Decimal":
                                    descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToDecimal(rawdata[index]) : 0);
                                    break;
                                case "Datetime":
                                    descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToDecimal(rawdata[index]) : 0);
                                    break;
                                default: //string
                                    descriptor.SetValue(model, rawdata[index]);
                                    break;

                            }
                        }
                    }
                }
            }
            return model;
        }

        private List<UploadReceiptFromExcelResultCMSModel> rawDataFromExelToUploadReceiptFromExcelResultCMSModel(List<UploadReceipFromExcelCMSModel> rawDataFromExel)
        {
            List<UploadReceiptFromExcelResultCMSModel> result = new List<UploadReceiptFromExcelResultCMSModel>();

            foreach (var item in rawDataFromExel)
            {

                UploadReceiptFromExcelResultCMSModel itemToShow = new UploadReceiptFromExcelResultCMSModel()
                {
                    ExpiredDate = item.ExpiredDate,
                    UniqueCode = item.UniqueCode,
                    PromoCode = item.PromoCode,
                    Domisili = item.Domisili,
                    Retailer = item.Retailer,
                    ExpiredPromo = item.ExpiredPromo
                };

                result.Add(itemToShow);

            }


            return result;
        }

        //public virtual ActionResult UploadFromExel(HttpPostedFileBase FileUpload, int id)
        //{
        //    string prmCode = Data.Promo.GetById(id).PromoCode;
        //    List<UploadReceiptFromExcelResultCMSModel> listReceiptResultModel = new List<UploadReceiptFromExcelResultCMSModel>();
        //    int succ = 0, fail = 0, err = 0;
        //    string currentUserName = CurrentUser.FullName;
        //    long CurrentUserID = CurrentUser.ID;

        //        string fileName = Path.GetFileName(FileUpload.FileName);
        //        string path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
        //        string errMsg = "";

        //    IQueryable<PromoDetails> prom = PromoDetails.GetAll();

        //    try
        //    {
        //        FileUpload.SaveAs(path);
        //        listReceiptResultModel = ProcessCSV(path, out errMsg);

        //        Parallel.ForEach(listReceiptResultModel,(pmDetail) =>
        //        {
        //            //.InsertFromExcelCMS(receipt, newID, CurrentPool.ID, currentUserName, Request.Url.AbsoluteUri, CurrentUserID, code);
        //            try
        //            {
        //                if (string.IsNullOrEmpty(pmDetail.UniqueCode))
        //                {
        //                    pmDetail.result = "Failed";
        //                    pmDetail.message = "Kolom Kode Unik Tidak Boleh Kosong";
        //                }
        //                else {
        //                    if (prom.Where(x => x.CodeName == pmDetail.UniqueCode).ToList().Count > 0) {
        //                        pmDetail.result = "Failed";
        //                        pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Kode Unik Sudah Terdaftar" : ", Kode Unik Sudah Terdaftar";
        //                    }
        //                }

        //                if (pmDetail.ExpiredDate == null)
        //                {
        //                    //Expired Date null
        //                    pmDetail.result = "Failed";
        //                    pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Kolom Expired Date Tidak Boleh Kosong" : ", Kolom Expired Date Tidak Boleh Kosong";
        //                }
        //                if (!string.IsNullOrEmpty(pmDetail.PromoCode))
        //                {
        //                    if (pmDetail.PromoCode != prmCode)
        //                    {
        //                        //promo Code Berbeda
        //                        pmDetail.result = "Failed";
        //                        pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Promo Code Salah" : ", Promo Code Salah";
        //                    }
        //                }
        //                else
        //                {
        //                    //promo Code Berbeda
        //                    pmDetail.result = "Failed";
        //                    pmDetail.message += string.IsNullOrEmpty(pmDetail.message) ? "Promo Code Tidak Boleh Kosong" : ", Promo Code Tidak Boleh Kosong";
        //                }


        //                if (string.IsNullOrEmpty(pmDetail.result))
        //                {
        //                    PromoDetails promS = new PromoDetails();
        //                    promS.CodeName = pmDetail.UniqueCode;
        //                    promS.IDPromo = id;
        //                    promS.ExpiredDate = pmDetail.ExpiredDate;
        //                    var res = promS.Insert(currentUserName, Request.Url.AbsoluteUri, CurrentUserID);
        //                    if (res.Success)
        //                    {
        //                        succ++;
        //                        pmDetail.result = "Success";
        //                        pmDetail.message = "Kode Unik Berhasil Diinput";
        //                    }
        //                    else
        //                    {
        //                        fail++;
        //                        pmDetail.result = "Success";
        //                        pmDetail.message = "Kode Unik Berhasil Diinput";
        //                    }
        //                }

        //            }
        //            catch (Exception ed)
        //            {
        //                err++;
        //            }

        //        });

        //    }
        //    catch (Exception ex)
        //    {

        //        ViewData["Feedback"] = ex.Message;
        //    }

        //    ViewBag.IdProm = id;
        //    return View(listReceiptResultModel);
        //}
        private static List<UploadReceiptFromExcelResultCMSModel> ProcessCSV(string fileName, out string errMsg)
        {
            List<UploadReceiptFromExcelResultCMSModel> lsSTring = new List<UploadReceiptFromExcelResultCMSModel>();
            string Feedback = string.Empty;
            string line = string.Empty;
            string[] strArray;
            StreamReader sr = new StreamReader(fileName);
            errMsg = "";
            while ((line = sr.ReadLine()) != null)
            {
                line = line.Trim().Replace("\"", "").Replace("\t", "").Replace(" ","");
                strArray = null;

                if (line.ToLower().Contains("promocode") || line.ToLower().Contains("uniquecode"))
                {
                    strArray = line.Split('|');
                    if (strArray[0].ToLower() != "UniqueCode")
                        errMsg = "";
                }
                else
                {

                    strArray = line.Split('|');
                    DateTime dt = new DateTime();

                    UploadReceiptFromExcelResultCMSModel itemToShows = new UploadReceiptFromExcelResultCMSModel()
                    {
                        UniqueCode = string.IsNullOrEmpty(strArray[0].ToString()) ? string.Empty : strArray[0].ToString(),
                        PromoCode = string.IsNullOrEmpty(strArray[1].ToString()) ? string.Empty : strArray[1].ToString()
                    };
                    if (DateTime.TryParseExact(strArray[2], "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        itemToShows.ExpiredDate = dt;

                    lsSTring.Add(itemToShows);
                }
            }
            sr.Close();
            return lsSTring;
        }

    }
}