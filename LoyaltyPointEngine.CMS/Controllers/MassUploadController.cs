﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Data;
using System.Security.Cryptography;

namespace LoyaltyPointCMS.Controllers
{
    public partial class MassUploadController : Controller
    {


        [HttpGet]
        public virtual ActionResult Index()
        {
            return RedirectToAction(MVC.Receipt.UploadFromExel());

        }


    }
}
