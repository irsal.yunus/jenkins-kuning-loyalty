﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class AreaController : BaseController
    {
        // GET: Area
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Area")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Area successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Area successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Area successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Area")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            AreaModel model = new AreaModel();
            GetListDistrict();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(AreaModel model)
        {
            this.IsValidArea(model, false);
            if (ModelState.IsValid)
            {
                Area area = new Area();
                area.InjectFrom(model);
                var result = area.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Area.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListDistrict();
            return View(model);
        }

        [BreadCrumb(Label = "Edit Area")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int AreaId)
        {
            Area currentArea = LoyaltyPointEngine.Data.Area.GetById(AreaId);
            AreaModel model = new AreaModel();
            model.InjectFrom(currentArea);
            GetListDistrict();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(AreaModel model)
        {
            this.IsValidArea(model, true);
            if (ModelState.IsValid)
            {
                Area area = LoyaltyPointEngine.Data.Area.GetById(model.ID);
                area.InjectFrom(model);
                var result = area.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Area.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListDistrict();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Area Area = Area.GetById(ID);
            Area.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Area.Index("Delete_Success"));
        }

        private void IsValidArea(AreaModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.Code))
            {
                Data.Area predictedData = Data.Area.GetByCode(Model.Code);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("Area Code '{0}'  has been used by other area", Model.Code));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("Area Code  '{0}'  has been used by other area", Model.Code));
            }
        }
        private void GetListDistrict()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<District> listdata = District.GetAll();
            foreach (District data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListDistrict = items;
        }

        #region List Area Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllArea()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Area> allArea = LoyaltyPointEngine.Data.Area.GetAll().OrderBy(x => x.Name);
            AjaxGrid<Area> grid = this.gridMvcHelper.GetAjaxGrid(allArea);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllAreaPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Area> allData = LoyaltyPointEngine.Data.Area.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<Area> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Area.Views.GridGetAllArea, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}