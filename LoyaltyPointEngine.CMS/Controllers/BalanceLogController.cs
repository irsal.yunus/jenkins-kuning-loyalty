﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Point;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class BalanceLogController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Balance")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "BalanceLog successfully created";
                    break;
                case "Fix_Success":
                    ViewBag.Success = "BalanceLog successfully fixed";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "BalanceLog successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "BalanceLog successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            ViewBag.User = CurrentUser.Username;
            return View();
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult FixBalanceLog(HttpPostedFileBase excelFile)
        {
            List<string> phoneList = new List<string>();
            ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
            foreach (var item in listData.DataRows)
            {
                if (PhoneHelper.isPhoneValid(item[0]))
                    phoneList.Add(item[0]);
            }

            var isSuccess = BalanceLogLogic.FixBalanceLog(phoneList, CurrentUser.Username);
            if (isSuccess.StatusCode == "00")
            {
                return RedirectToAction(MVC.BalanceLog.Index("Fix_Success"));
            }
            else
            {
                return RedirectToAction(MVC.BalanceLog.Index(isSuccess.StatusMessage));
            }

            //if (PhoneHelper.isPhoneValid((Phone)))
            //{
            //    phoneList.Add(Phone);
            //    var isSuccess = BalanceLogLogic.FixBalanceLog(phoneList, CurrentUser.Username);
            //    if (isSuccess.Value)
            //    {
            //        return RedirectToAction(MVC.BalanceLog.Index("Create_Success"));
            //    }
            //    else
            //    {
            //        return RedirectToAction(MVC.BalanceLog.Index(isSuccess.StatusMessage));
            //    }
            //}
            //else
            //{
            //    return RedirectToAction(MVC.BalanceLog.Index("Phone is invalid"));
            //}
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/FixBalanceLogTemplate.xlsx"));
            string fileName = "FixBalanceLogTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}