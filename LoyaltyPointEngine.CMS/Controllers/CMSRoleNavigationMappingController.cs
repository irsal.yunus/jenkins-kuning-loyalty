﻿using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Data;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omu.ValueInjecter;
using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CMSRoleNavigationMappingController : BaseController
    {
        // GET: CMSUser
        private IGridMvcHelper gridMvcHelper;
        // GET: CMSRoleNavigationMapping
        [BreadCrumb(Label = "Manage Role Navigation Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Role Mapping successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Role Mapping  successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Role Mapping  successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Label = "Add Role Navigation Privilege")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            RoleNavigationPrivilegeModel model = new RoleNavigationPrivilegeModel();
            GetRoleList();
            GetNavigationPrivilegeList();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(RoleNavigationPrivilegeModel model)
        {
            CMSUserInformation _CMSInformation = new CMSUserInformation();
            if (ModelState.IsValid)
            {
                CMSRoleNavigationPrivilegeMapping nav = new CMSRoleNavigationPrivilegeMapping();
                nav.InjectFrom(model);
                var result = nav.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                {
                    SetRemoveCurrentNavigationCache();
                    _CMSInformation.SetRemoveCurrentAllowedControllerActionCache();
                    return RedirectToAction(MVC.CMSRoleNavigationMapping.Index("Create_Success"));
                }
                    
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }

            GetRoleList();
            return View(model);
        }

        #region List Role Navigation Privilege Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllRoleNavigationPrivilegeMapping()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSRoleNavigationPrivilegeMapping> allData = CMSRoleNavigationPrivilegeMapping.GetAll().OrderBy(x => x.CMSRole.Name).ThenBy(x => x.CMSNavigationPrivilegeMapping.CMSNavigation.Name);
            AjaxGrid<CMSRoleNavigationPrivilegeMapping> grid = this.gridMvcHelper.GetAjaxGrid(allData);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllRoleNavigationPrivilegeMappingPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSRoleNavigationPrivilegeMapping> allData = CMSRoleNavigationPrivilegeMapping.GetAll().OrderBy(x => x.CMSRole.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.CMSRole.Name.Contains(txtSearch) || x.CMSNavigationPrivilegeMapping.CMSNavigation.Name.Contains(txtSearch)).OrderBy(x => x.CMSRole.Name).ThenBy(x => x.CMSNavigationPrivilegeMapping.CMSNavigation.Name);
            }
            AjaxGrid<CMSRoleNavigationPrivilegeMapping> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.CMSRoleNavigationMapping.Views.GridGetAllRoleNavigationPrivilegeMapping, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        private void GetRoleList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSRole> listRole = CMSRole.GetAll();
            foreach (CMSRole role in listRole)
            {

                items.Add(new SelectListItem { Text = role.Name, Value = role.ID.ToString(), Selected = false });
            }
            ViewBag.ListRole = items;
        }

        private void GetNavigationPrivilegeList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSNavigationPrivilegeMapping> listdata = CMSNavigationPrivilegeMapping.GetAll();
            foreach (CMSNavigationPrivilegeMapping data in listdata)
            {

                items.Add(new SelectListItem { Text = data.CMSNavigation.Name + " - " + data.CMSPrivilege.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListNavigationPrivilege = items;
        }
    }
}