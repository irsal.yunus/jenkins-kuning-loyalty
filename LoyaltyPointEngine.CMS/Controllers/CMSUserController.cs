﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using Omu.ValueInjecter;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using LoyaltyPointEngine.CMS.Helper;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CMSUserController : BaseController
    {
        // GET: CMSUser
        private IGridMvcHelper gridMvcHelper;

        //
        // GET: /CMSUser/
        [BreadCrumb(Clear = true, Label = "User")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "User successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "User successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "User successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add User")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            UserModel model = new UserModel();
            GetRoleList();
            GetParentList(model.CMSRoleID);
            bool IsParentHidden = false;
            ViewBag.IsHidden = IsParentHidden;
            return View(model);
        }

        private void GetParentList(long CMSRoleID)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            //List<SelectListItem> items = new List<SelectListItem>();

            IQueryable<CMSRole> cmsRole = Enumerable.Empty<CMSRole>().AsQueryable();

            var GetLevel = CMSRoleID != 0 ? CMSRole.GetByID(CMSRoleID).Level : null;

            if (GetLevel != null)
            {
                for (var i = 10; i <= 40; i++)
                {
                    if (GetLevel == i)
                    {
                        for (var j = i; j <= 40; j++)
                        {
                            if (i < j)
                            {
                                cmsRole = CMSRole.GetAll().Where(x => x.Level == j);
                                if (cmsRole.FirstOrDefault() != null)
                                    break;
                            }

                        }
                        break;
                    }
                }
            }


            items.Add(new SelectListItem { Text = "", Value = "" });



            var listdata = cmsRole.FirstOrDefault() != null ? CMSUser.GetAll().Where(x => x.CMSRoleID == cmsRole.FirstOrDefault().ID).OrderBy(x => x.FullName) : null;

            //var listParent = CMSUser.GetAll();

            if (listdata != null)
            {
                foreach (CMSUser parent in listdata)
                {
                    items.Add(new SelectListItem { Text = parent.FullName, Value = parent.ID.ToString(), Selected = parent.ID.ToString() == CMSRoleID.ToString() });
                }
            }

            ViewBag.ListParent = items;
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(UserModel model)
        {
            this.ValidUserModel(model, false);
            if (ModelState.IsValid)
            {
                CMSUser user = new CMSUser();
                if (string.IsNullOrEmpty(model.Email))
                {
                    model.Email = "";
                }
                user.InjectFrom(model);
                user.UserPrivateSecret = Guid.NewGuid();
                user.Password = GetEncriptedPassword(model.Password, user.UserPrivateSecret);
                user.PoolID = CurrentPool.ID;
                user.ParentID = model.ParentID != null || model.ParentID != 0 ? model.ParentID : null;
                user.IsActive = true;
                var result = user.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.CMSUser.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }

            GetRoleList();
            GetParentList(model.CMSRoleID);
            bool IsParentHidden = false;
            if (model.ParentID == null && model.CMSRoleID != null)
                IsParentHidden = true;
            ViewBag.IsHidden = IsParentHidden;
            return View(model);
        }

        [BreadCrumb(Label = "Edit User")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(long UserId)
        {
            GetRoleList();
            CMSUser currentUser = CMSUser.GetByID(UserId);
            UserModel model = new UserModel();
            GetParentList(currentUser.CMSRoleID);
            model.InjectFrom(currentUser);
            bool IsParentHidden = false;
            if (model.ParentID == null)
                IsParentHidden = true;
            ViewBag.IsHidden = IsParentHidden;
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(UserModel model)
        {
            this.ValidUserModel(model, true);
            if (ModelState.IsValid)
            {
                CMSUser user = CMSUser.GetByID(model.ID);
                if (string.IsNullOrEmpty(model.Email))
                {
                    model.Email = "";
                }
                model.Password = user.Password;
                user.InjectFrom(model);

                var result = user.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.CMSUser.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetRoleList();
            return View(model);
        }

        [BreadCrumb(Label = "Reset Password")]
        public virtual ActionResult ChangePassword(long UserId)
        {
            CMSUser currentUser = CMSUser.GetByID(UserId);
            UserModel model = new UserModel();
            model.InjectFrom(currentUser);
            model.Password = string.Empty;
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult ChangePassword(UserModel model)
        {
            this.ValidResetPassword(model);
            if(ModelState.IsValid)
            {
                CMSUser user = CMSUser.GetByID(model.ID);
                user.Password = GetEncriptedPassword(model.Password, user.UserPrivateSecret);

                var result = user.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.CMSUser.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);

            }
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(long ID)
        {
            CMSUser User = CMSUser.GetByID(ID);
            User.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.CMSUser.Index("Delete_Success"));
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Activated(long ID)
        {
            CMSUser User = CMSUser.GetByID(ID);

            if (User != null)
            {
                User.IsActive = true;
                User.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            }

            return RedirectToAction(MVC.CMSUser.Index("Edit_Success"));
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Deactivated(long ID)
        {
            CMSUser User = CMSUser.GetByID(ID);

            if (User != null)
            {
                User.IsActive = false;
                User.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            }

            return RedirectToAction(MVC.CMSUser.Index("Edit_Success"));
        }

        [HttpGet]
        public virtual JsonResult GeneratedParent(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IQueryable<CMSRole> cmsRole = Enumerable.Empty<CMSRole>().AsQueryable();

            var GetLevel = CMSRole.GetByID((long)id).Level;

            for (var i = 10; i <= 40; i++)
            {
                if (GetLevel == i)
                {
                    for (var j = i; j <= 40; j++)
                    {
                        if (i < j)
                        {
                            cmsRole = CMSRole.GetAll().Where(x => x.Level == j);
                            if (cmsRole.FirstOrDefault() != null)
                                break;
                        }

                    }
                    break;
                }
            }

            items.Add(new SelectListItem { Text = "", Value = "" });
            if (id.HasValue)
            {
                var listdata = CMSUser.GetAll().Where(x => x.CMSRoleID == cmsRole.FirstOrDefault().ID).OrderBy(x => x.FullName);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = data.FullName, Value = data.ID.ToString() });
                }
            }
            //ViewBag.ListParent = items;
            return Json(items, JsonRequestBehavior.AllowGet);

        }

        private void GetRoleList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSRole> listRole = CMSRole.GetAll();
            foreach (CMSRole role in listRole)
            {

                items.Add(new SelectListItem { Text = role.Name, Value = role.ID.ToString(), Selected = false });
            }
            ViewBag.ListRole = items;
        }

        private string GetEncriptedPassword(string PlainPassword, Guid Salts)
        {
            string raw = PlainPassword + Salts;
            byte[] bytes = Encoding.Unicode.GetBytes(PlainPassword + Salts.ToString());
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);
            return BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();
        }

        #region validate data
        private void ValidUserModel(UserModel model, Boolean isEdit)
        {
            /*
            var checkEMail = new EmailAddressAttribute();
            if (string.IsNullOrEmpty(model.Email))
                ModelState.AddModelError("Error", string.Format("{0}", "Email Wajib di isi"));
            else
                if (checkEMail.IsValid(model.Email))
                {
                    CMSUser predictedUser = CMSUser.GetByEmail(model.Email).FirstOrDefault();
                    if (isEdit)
                    {
                        if (predictedUser != null && predictedUser.ID != model.ID)
                            ModelState.AddModelError("Error", string.Format("Email '{0}' sudah digunakan, silahkan gunakan email lain", model.Email));
                    } else if (predictedUser != null && predictedUser.ID > 0)
                        ModelState.AddModelError("Error", string.Format("Email '{0}' sudah digunakan, silahkan gunakan email lain", model.Email));
                }
                else
                    ModelState.AddModelError("Error", string.Format("Format Email '{0}' tidak valid", model.Email));
             */
            if(!isEdit)
                this.ValidResetPassword(model);
        }

        private void ValidResetPassword(UserModel model)
        {
            if (string.IsNullOrEmpty(model.Password))
                ModelState.AddModelError("Error", string.Format("{0}", "Password wajib di isi"));
            if (string.IsNullOrEmpty(model.ReEnterPassword))
                ModelState.AddModelError("Error", string.Format("{0}", "RePassword wajib di isi"));
            if (model.Password != model.ReEnterPassword)
                ModelState.AddModelError("Error", string.Format("{0}", "Password dan Confirm Password harus sama"));
            if (!string.IsNullOrEmpty(model.Password) && model.Password.Length < 6)
                ModelState.AddModelError("Password", string.Format("{0}", "Password tidak boleh kurang dari 6 karakter"));
        }
        #endregion

        #region List User Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllUser()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSUser> allUser = CMSUser.GetAll().OrderBy(x => x.FullName);
            AjaxGrid<CMSUser> grid = this.gridMvcHelper.GetAjaxGrid(allUser);
            return PartialView(grid);
        }

        [HttpPost]
        public virtual ActionResult CheckavailabilityUser(int? ID, string KeyType, string Value)
        {

            bool IsAvailable = false;
            string Message = "";

            if (!string.IsNullOrEmpty(KeyType))
            {
                if (!string.IsNullOrEmpty(Value))
                {
                    if (KeyType.ToLower() == "email")
                    {
                        if (EmailHelper.IsValidEmail(Value))
                        {
                            var data = !ID.HasValue ? CMSUser.Get_ByEmail(Value) : CMSUser.GetByEmailExceptID(ID.Value, Value);
                            IsAvailable = data == null ? true : false;
                        }
                        else
                        {
                            Message = "Value is not an Email format";
                        }


                    }
                    //else
                    //{
                    //    if (PhoneHelper.isPhoneValid(Value))
                    //    {
                    //        var data = !ID.HasValue ? CMSUser.GetByPhone(Value) : CMSUser.GetByPhoneExceptID(ID.Value, Value);
                    //        IsAvailable = data == null ? true : false;
                    //    }
                    //    else
                    //    {
                    //        Message = "Value is not an Phone format";
                    //    }

                    //}

                    if (IsAvailable)
                    {
                        Message = "Value is available to used";
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Message))
                        {
                            Message = Message = (KeyType.ToLower() == "email") ? "email has been used by other user" : "phone has been used by other user";
                        }
                    }


                }
                //else
                //{
                //    Message = "Value is required";
                //}


            }
            //else
            //{
            //    Message = "KeyType is required";
            //}
            return Json(new { IsAvailable = IsAvailable, Message = Message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllUserPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSUser> allData = CMSUser.GetAll().OrderBy(x => x.FullName);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.FullName.ToLower().Contains(txtSearch.ToLower()) || x.Email.ToLower().Contains(txtSearch.ToLower())||x.Username.ToLower().Contains(txtSearch.ToLower())).OrderBy(x => x.FullName);
            }
            AjaxGrid<CMSUser> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.CMSUser.Views.GridGetAllUser, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}