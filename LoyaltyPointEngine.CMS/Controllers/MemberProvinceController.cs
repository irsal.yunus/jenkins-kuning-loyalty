﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.DataMember;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;

using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.Model.Object.Member;
using System.Security.Cryptography;
using System.Text;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberProvinceController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Province")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Province successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Province successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Province successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Province")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            //string passwordlama = "123456";
            //HMACSHA1 hash = new HMACSHA1();
            //hash.Key = Encoding.Unicode.GetBytes(passwordlama);
            //string encodedPassword =
            //  Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(passwordlama)));

            ProvinceModel model = new ProvinceModel();
           
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ProvinceModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {
                var data = new Province();
                data.ProvinceCode = model.ProvinceCode;
                data.ProvinceName = model.ProvinceName;
                var result = data.Inserted(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.MemberProvince.Index("Create_Success"));

            }

            return View(model);
        }

        private void ValidateAddModel(ProvinceModel model)
        {

            if (string.IsNullOrEmpty(model.ProvinceCode))
            {
                ModelState.AddModelError("ProvinceCode", "Code is required");
            }
            else
            {
                var data = Province.GetByCode(model.ProvinceCode);
                if (data != null)
                {
                    ModelState.AddModelError("ProvinceCode", "Code is already registered in system");
                }
            }


            if (string.IsNullOrEmpty(model.ProvinceName))
            {
                ModelState.AddModelError("ProvinceCode", "Name is required");
            }
        }

        [BreadCrumb(Label = "Edit Province")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Edit(int ID)
        {
            var data = Province.GetByID(ID);
            ProvinceModel model = new ProvinceModel(data);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Edit(ProvinceModel model)
        {
            ValidateEditModel(model);
            var data = Province.GetByID(model.ID);
            if(data ==null)
            {
                ModelState.AddModelError("ID", "ID not found in system");
            }
            if (ModelState.IsValid)
            {
              
                data.ProvinceCode = model.ProvinceCode;
                data.ProvinceName = model.ProvinceName;
                var result = data.Updated(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.MemberProvince.Index("Edit_Success"));

            }

            return View(model);
        }


        private void ValidateEditModel(ProvinceModel model)
        {

            if (string.IsNullOrEmpty(model.ProvinceCode))
            {
                ModelState.AddModelError("ProvinceCode", "Code is required");
            }
            else
            {
                var data = Province.GetByCodeNotSelf(model.ProvinceCode, model.ID);
                if (data != null)
                {
                    ModelState.AddModelError("ProvinceCode", "Code is already registered in system");
                }
            }


            if (string.IsNullOrEmpty(model.ProvinceName))
            {
                ModelState.AddModelError("ProvinceCode", "Name is required");
            }
        }

        #region List District Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllMemberProvince()
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = Province.GetAll().OrderBy(x => x.ProvinceCode);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllProvincePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var allData = Province.GetAll().OrderBy(x => x.ProvinceName);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.ProvinceName.Contains(txtSearch) || x.ProvinceCode.Contains(txtSearch)).OrderBy(x => x.ProvinceCode);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.MemberProvince.Views.GridGetAllMemberProvince, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}