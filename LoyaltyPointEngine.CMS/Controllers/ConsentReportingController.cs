﻿using CsvHelper;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object.Consent;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ConsentReportingController : BaseController
    {
        // GET: ConsentReporting
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Reporting - Consent")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Consent")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string startDate, string endDate)
        {
            DateTime? _startDate = null, _endDate = null;
            DateTime tempDate = DateTime.Today;
            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _startDate = tempDate;
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/2016 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _endDate = tempDate;
            }
            else
            {
                _endDate = DateTime.Today.AddDays(1);
            }

            var datas = GetMemberConsentReport(_startDate.Value, _endDate.Value);

            string filename = string.Format("MemberConsentReport_{0}.csv", DateTime.Now.ToString("ddMMyyyy"));
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            var csv = new CsvWriter(writer);
            csv.Configuration.Delimiter = ";";
            csv.Configuration.RegisterClassMap<ConsentReportCsvModel>();
            csv.WriteRecords(datas);
            writer.Flush();
            stream.Position = 0;

            return File(stream, "text/csv", filename);

        }

        [HttpGet]
        public virtual ActionResult GridGetAllPager(string startDate, string endDate, string orderColumn, string orderDirection, int page)
        {
            orderColumn = orderColumn ?? "CreatedDate";
            orderDirection = orderDirection ?? "DESC";
            page = (page <= 0 ? 1 : page);

            int pageSize = 5,
                totalRow = 0;

            DateTime? _startDate = null, _endDate = null;
            DateTime tempDate = DateTime.Today;
            if (!string.IsNullOrEmpty(startDate))
            {
                if (DateTime.TryParseExact(startDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _startDate = tempDate;
            }
            else
            {
                _startDate = DateTime.ParseExact("01/01/2016 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                if (DateTime.TryParseExact(endDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempDate))
                    _endDate = tempDate;
            }
            else
            {
                _endDate = DateTime.Today.AddDays(1);
            }

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.CurrentPage = page;
            ViewBag.OrderColumn = orderColumn;
            ViewBag.OrderDirection = orderDirection;
            ViewBag.PageSize = pageSize;

            totalRow = (int)GetMemberConsentReportGridCount( _startDate.Value, _endDate.Value);
            ViewBag.TotalRow = totalRow;
            ViewBag.TotalPage = (int)Math.Ceiling((double)totalRow / pageSize);
            var data = GetMemberConsentReportGrid(_startDate.Value, _endDate.Value, page, pageSize, orderColumn, orderDirection);
            return PartialView("GridGetAll", data);
        }

        private static long GetMemberConsentReportGridCount( DateTime startDate, DateTime endDate)
        {
            var data = (from c in ConsentCookieMember.GetAll()
                        join m in Member.GetAll() on c.MemberID equals m.ID into leftJoin
                        from m in leftJoin.DefaultIfEmpty()
                        where c.CreatedDate >= startDate && c.CreatedDate <= endDate
                        select c).ToList();

            return data.Count;
        }

        private static List<ConsentReportModel> GetMemberConsentReportGrid(DateTime startDate, DateTime endDate, int currentPage, int totalRowPerPage, string orderColumn, string orderDirection)
        {
            List<ConsentReportModel> result = new List<ConsentReportModel>();

            string direction = orderDirection.ToLower() == "desc" ? "Desc" : "Asc";

            var rawMember = from c in ConsentCookieMember.GetAll()
                            join m in Member.GetAll() on c.MemberID equals m.ID into leftJoin
                            from m in leftJoin.DefaultIfEmpty()
                            where c.CreatedDate >= startDate && c.CreatedDate <= endDate
                            select new {
                                ID=c.ID,
                                FirstName = (m == null ? "-" : m.FirstName),
                                Phone = (m == null ? "-" : m.Phone),
                                CreatedDate = c.CreatedDate
                            };

            result = rawMember
                .OrderBy(string.Format("{0} {1}", orderColumn, direction))
                .Skip((currentPage - 1) * totalRowPerPage)
                .Take(totalRowPerPage)
                .Select(d => new ConsentReportModel {
                    Id = d.ID,
                    Name = d.FirstName,
                    Phone = d.Phone,
                    AcceptDate = d.CreatedDate
                })
                .ToList();

            return result;
        }

        private List<ConsentReportModel> GetMemberConsentReport(DateTime _from, DateTime _to)
        {
            List<ConsentReportModel> result = new List<ConsentReportModel>();
            result = (from c in ConsentCookieMember.GetAll()
                      join m in Member.GetAll() on c.MemberID equals m.ID into leftJoin
                      from m in leftJoin.DefaultIfEmpty()
                      where c.CreatedDate >= _from && c.CreatedDate <= _to
                      select new ConsentReportModel {
                          Id = c.ID,
                          Name = (m == null ? string.Empty : m.FirstName),
                          Phone = (m == null ? string.Empty : m.Phone),
                          AcceptDate = c.CreatedDate
                    }).ToList();
            
            return result;
        }
    }
}