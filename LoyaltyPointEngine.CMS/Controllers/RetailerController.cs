﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RetailerController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Retailer")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Retailer successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Retailer successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Retailer successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Retailer")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            RetailerCMSModel model = new RetailerCMSModel();
            GetListCity(model.CityID.GetValueOrDefault(0));
            GetListMarket(model.RetailerMarketID);
            GetListRetailerChannel(model.RetailerMarketID, model.RetailerChannelID);
            GetListRetailerSubChannel(model.RetailerChannelID, model.RetailerSubChannelID);

            GetListRetailerAccount(model.RetailerSubChannelID, model.RetailerAccountID);
            GetListRetailerSubAccount(model.RetailerAccountID, model.RetailerSubAccountID.GetValueOrDefault());

            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(RetailerCMSModel model)
        {
            this.IsValidRetailer(model, false);
            if(model.Name != null && model.Code != null)
            {
                Retailer retailer = new Retailer();
                retailer.InjectFrom(model);
                retailer.RetailerSubAccountID = model.RetailerSubAccountID;
                retailer.IsNoCap = model.IsNoCap;
                var result = retailer.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Retailer.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListCity(model.CityID.GetValueOrDefault(0));
            GetListMarket(model.RetailerMarketID);
            GetListRetailerChannel(model.RetailerMarketID, model.RetailerChannelID);
            GetListRetailerSubChannel(model.RetailerChannelID, model.RetailerSubChannelID);
            GetListRetailerAccount(model.RetailerSubChannelID, model.RetailerAccountID);
            GetListRetailerSubAccount(model.RetailerAccountID, model.RetailerSubAccountID.GetValueOrDefault());
            return View(model);
        }

        [BreadCrumb(Label = "Edit Retailer")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int RetailerId)
        {
            Retailer currentRetailer = LoyaltyPointEngine.Data.Retailer.GetById(RetailerId);
            RetailerCMSModel model = new RetailerCMSModel(currentRetailer);
            GetListCity(model.CityID.GetValueOrDefault(0));
            GetListMarket(model.RetailerMarketID);
            GetListRetailerChannel(model.RetailerMarketID, model.RetailerChannelID);
            GetListRetailerSubChannel(model.RetailerChannelID, model.RetailerSubChannelID);
            GetListRetailerAccount(model.RetailerSubChannelID, model.RetailerAccountID);
            GetListRetailerSubAccount(model.RetailerAccountID, model.RetailerSubAccountID.GetValueOrDefault());
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(RetailerCMSModel model)
        {
            this.IsValidRetailer(model, true);
            //if (ModelState.IsValid)
            if(model.Name != null && model.Code != null)
            {
                Retailer retailer = LoyaltyPointEngine.Data.Retailer.GetById(model.ID);
                retailer.InjectFrom(model);
                retailer.IsNoCap = model.IsNoCap;
                var result = retailer.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Retailer.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListCity(model.CityID.GetValueOrDefault(0));
            GetListMarket(model.RetailerMarketID);
            GetListRetailerChannel(model.RetailerMarketID, model.RetailerChannelID);
            GetListRetailerSubChannel(model.RetailerChannelID, model.RetailerSubChannelID);
            GetListRetailerAccount(model.RetailerSubChannelID, model.RetailerAccountID);
            GetListRetailerSubAccount(model.RetailerAccountID, model.RetailerSubAccountID.GetValueOrDefault());
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            Retailer retailer = Retailer.GetById(ID);
            retailer.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Retailer.Index("Delete_Success"));
        }

        private void IsValidRetailer(RetailerCMSModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.Code))
            {
                Data.Retailer predictedData = Data.Retailer.GetByCode(Model.Code);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("Retailer Code '{0}'  has been used by other Retailer", Model.Code));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("Retailer Code  '{0}'  has been used by other Retailer", Model.Code));
            }
            if(string.IsNullOrEmpty(Model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }
            //if (Model.CityID < 0)
            //{
            //    ModelState.AddModelError("CityID", "City is required");
            //}
            //if (Model.RetailerSubAccountID <= 0 || Model.RetailerSubAccountID.HasValue ==false)
            //{
            //    ModelState.AddModelError("RetailerSubAccountID", "Retailer SubAccount is required");
            //}
            //if (Model.RetailerMarketID <= 0 )
            //{
            //    ModelState.AddModelError("RetailerMarketID", "Retailer Market is required");
            //}
            //if (Model.RetailerAccountID <= 0)
            //{
            //    ModelState.AddModelError("RetailerAccountID", "RetailerAccount is required");
            //}
            //if (Model.RetailerChannelID <= 0)
            //{
            //    ModelState.AddModelError("RetailerChannelID", "RetailerChannel is required");
            //}
            //if (Model.RetailerSubChannelID <= 0)
            //{
            //    ModelState.AddModelError("RetailerSubChannelID", "RetailerSubChannel is required");
            //}

        }
        private void GetListCity(int cityID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<City> listdata = City.GetAll();
            foreach (City data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = data.ID == cityID });
            }
            ViewBag.ListCity = items;
        }


        private void GetListMarket(int marketID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = RetailerMarket.GetAll();
            foreach (var data in listdata)
            {

                items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.MarketDesc), Value = data.ID.ToString(), Selected = data.ID == marketID });
            }
            ViewBag.ListMarket = items;
        }

        private void GetListRetailerChannel(int parentID,int ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = RetailerChannel.GetAll().Where(x => x.RetailerMarketID == parentID);
            if(listdata != null)
            {
                foreach (var data in listdata)
                {

                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.ChannelDesc), Value = data.ID.ToString(), Selected = data.ID == ID });
                }
            }

            ViewBag.ListChanel = items;
        }


        private void GetListRetailerSubChannel(int parentID, int ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = RetailerSubChannel.GetAll().Where(x => x.RetailerChannelID == parentID);
            if (listdata != null)
            {
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.SubChannelDesc), Value = data.ID.ToString(), Selected = data.ID == ID });
                }
            }

            ViewBag.ListSubChanel = items;
        }


        private void GetListRetailerAccount(int parentID, int ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = RetailerAccount.GetAll().Where(x => x.RetailerSubChannelID == parentID);
            if (listdata != null)
            {
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.AccountDesc), Value = data.ID.ToString(), Selected = data.ID == ID });
                }
            }

            ViewBag.ListAccount = items;
        }


        private void GetListRetailerSubAccount(int parentID, int ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = RetailerSubAccount.GetAll().Where(x => x.RetailerAccountID == parentID);
            if (listdata != null)
            {
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.SubAccountDesc), Value = data.ID.ToString(), Selected = data.ID == ID });
                }
            }

            ViewBag.ListSubAccount = items;
        }




        #region List Retailer Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllRetailer()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Retailer> allRetailer = LoyaltyPointEngine.Data.Retailer.GetAll().OrderBy(x => x.Name);
            AjaxGrid<Retailer> grid = this.gridMvcHelper.GetAjaxGrid(allRetailer);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllRetailerPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<Retailer> allData = LoyaltyPointEngine.Data.Retailer.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<Retailer> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Retailer.Views.GridGetAllRetailer, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion



        [HttpGet]
        public virtual JsonResult GeneratedChannel(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            if(id.HasValue)
            {
                var listdata = RetailerChannel.GetAll().Where(x => x.RetailerMarketID == id).OrderBy(x => x.Code);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.ChannelDesc), Value = data.ID.ToString() });
                }
            }


            return Json(items, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public virtual JsonResult GeneratedSubChannel(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = ""});
            if (id.HasValue)
            {
                var listdata = RetailerSubChannel.GetAll().Where(x => x.RetailerChannelID == id).OrderBy(x => x.Code);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.SubChannelDesc), Value = data.ID.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public virtual JsonResult GeneratedAccount(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            if (id.HasValue)
            {
                var listdata = RetailerAccount.GetAll().Where(x => x.RetailerSubChannelID == id).OrderBy(x => x.Code);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.AccountDesc), Value = data.ID.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public virtual JsonResult GeneratedSubAccount(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            if (id.HasValue)
            {
                var listdata = RetailerSubAccount.GetAll().Where(x => x.RetailerAccountID == id).OrderBy(x => x.Code);
                foreach (var data in listdata)
                {
                    items.Add(new SelectListItem { Text = string.Format("{0} - {1}", data.Code, data.SubAccountDesc), Value = data.ID.ToString() });
                }
            }

            return Json(items, JsonRequestBehavior.AllowGet);

        }

    }
}