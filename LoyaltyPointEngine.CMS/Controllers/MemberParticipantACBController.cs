﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Object.AutomateCampaign;
using LoyaltyPointEngine.Model.Parameter.Member;
using MvcBreadCrumbs;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberParticipantACBController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Member Participant")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Member Participant successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Member Participant successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Member Participant successfully deleted";
                    break;
                case "Download_Success":
                    ViewBag.Success = "Member Participant successfully Download";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            GetCampaignList();
            string spgRoleCode;
            ViewBag.IsSpg = this.IsSpgRole(out spgRoleCode);
            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Member Participant")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult GridGetAllPager(int? CampaignID)
        {
            if (CampaignID == null)
            {
                return RedirectToAction(MVC.MemberParticipantACB.Index("Download_Failed"));
            }
            IEnumerable<AutomaticCampaignParticipant> filteredCampaign = AutomaticCampaignParticipant.GetByCampaignID(CampaignID.Value);
            var datas = from a in Member.GetAll()
                        join c in filteredCampaign on a.ID equals c.MemberId
                        select new ParticipantModel
                        {
                            MemberID = a.ID,
                            MemberName = a.Name,
                            Email = a.Email,
                            Phone = a.Phone,
                            Point = c.Point,
                            CampaignID = c.CampaignID
                        };

            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetMemberParticipant(datas.ToList());
            fileName = string.Format("ParticipantACBReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Name", "Email", "Phone", "Point" });
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
        private List<List<string>> GetMemberParticipant(List<ParticipantModel> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                data.Add(item.MemberName);
                data.Add(item.Email);
                data.Add(item.Phone);
                data.Add(item.Point.ToString());
                datas.Add(data);
            }
            return datas;
        }
        #region MassUpload
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult DownloadExcelTemplate()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/UploadMemberParticipantACBFormat.xlsx"));
            string fileName = "MemberParticipantACBTemplate.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        [BreadCrumb(Clear = true, Label = "Mass Member Participant")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddMassParticipant()
        {
            GetCampaignList();
            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Mass Member Participant")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddMassParticipant(HttpPostedFileBase excelFile, int campaignID)
        {
            List<string> ListError = new List<string>();
            List<ParticipantTemplateUploadModel> Listdata = new List<ParticipantTemplateUploadModel>();  //ucup

            if (excelFile != null)
            {
                var rawDataFromExel = PopulateUploadModelFromExcelRawDataToModel(excelFile);
                if (rawDataFromExel != null && rawDataFromExel.Count > 0)
                {

                    int sukses = 0;
                    int gagal = 0;

                    foreach (var item in rawDataFromExel)
                    {
                        var dataMember = DataMember.Member.GetByPhone(item.phone);
                        if (dataMember != null)
                        {
                            //cek member di campaign yang sama
                            var cekParticipantSameCampaign = AutomaticCampaignParticipant.GetAll(false).Where(x => x.MemberId == dataMember.ID && x.IsActive == true && x.CampaignID == campaignID);
                            if (cekParticipantSameCampaign != null && cekParticipantSameCampaign.Count() > 0)
                            {
                                gagal = gagal + 1;
                                ParticipantTemplateUploadModel model = new ParticipantTemplateUploadModel();
                                model.phone = item.phone;
                                Listdata.Add(model);
                                continue;
                            }
                            IQueryable<AutomaticCampaignParticipant> cekParticipantACB = AutomaticCampaignParticipant.GetAll(false).Where(x => x.MemberId == dataMember.ID && x.IsActive == true);
                            if (cekParticipantACB != null && cekParticipantACB.Count() > 0)
                            {
                                AutomaticCampaignParticipant participantACBDelete = AutomaticCampaignParticipant.GetById(Convert.ToInt32(cekParticipantACB.Select(x => x.ID).FirstOrDefault()));
                                participantACBDelete.IsActive = false;
                                var resultDelete = participantACBDelete.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                            }
                            AutomaticCampaignParticipant participantACB = new AutomaticCampaignParticipant();
                            ParticipantInsertModel participantModel = new ParticipantInsertModel();
                            participantModel.CampaignID = campaignID;
                            participantModel.MemberId = dataMember.ID;
                            participantModel.CreatedBy = CurrentUser.FullName;
                            participantModel.CreatedDate = DateTime.Now;
                            participantModel.BonusPoint = 0;
                            participantModel.Point = 0;
                            participantModel.IsAchieve = false;
                            participantModel.IsDeleted = false;
                            participantModel.IsActive = true;
                            participantACB.InjectFrom(participantModel);
                            var result = participantACB.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                            sukses += 1;
                        }
                        else
                        {
                            gagal = gagal + 1;
                            ParticipantTemplateUploadModel model = new ParticipantTemplateUploadModel();
                            model.phone = item.phone;
                            Listdata.Add(model);
                        }
                    }
                    ViewBag.success = "Sukses, Member success = " + sukses + " and Member same = " + gagal;
                    GetCampaignList();
                    ViewBag.TotalData = gagal;
                    return View(Listdata);
                }

            }
            ViewBag.errors = ListError;
            return View();
        }
        private List<ParticipantTemplateUploadModel> PopulateUploadModelFromExcelRawDataToModel(HttpPostedFileBase excelFile)
        {
            List<ParticipantTemplateUploadModel> rawDataFromExel = new List<ParticipantTemplateUploadModel>();
            ExcelDataHelper listData = ExcelHelper.ReadExcel(excelFile);
            List<string> headers = listData.Headers;
            List<List<string>> datas = listData.DataRows;

            ParticipantTemplateUploadModel excelModel = new ParticipantTemplateUploadModel();
            List<string> listHeader = null;
            Dictionary<string, int> HeaderDict = null;

            listHeader = ExcelHelper.ObjectToList(excelModel);
            if (listHeader != null && headers != null)
                HeaderDict = ExcelHelper.ModeltoDictionary(listHeader, headers);


            foreach (List<string> data in datas)
            {
                ExcelHelper.CheckData(data, headers);
                ParticipantTemplateUploadModel newData = null;
                List<string> rawdata = data;
                newData = ConvertRawDatatoObject(rawdata, HeaderDict);
                if (newData != null)
                {
                    rawDataFromExel.Add(newData);
                }
            }


            return rawDataFromExel;

        }

        private ParticipantTemplateUploadModel ConvertRawDatatoObject(List<string> rawdata, Dictionary<string, int> dataKey)
        {
            ParticipantTemplateUploadModel model = new ParticipantTemplateUploadModel();
            foreach (System.ComponentModel.PropertyDescriptor descriptor in System.ComponentModel.TypeDescriptor.GetProperties(model))
            {
                if (descriptor != null && descriptor.Name != null)
                {
                    if (dataKey.ContainsKey(descriptor.Name))
                    {
                        int index = dataKey[descriptor.Name];
                        switch (descriptor.PropertyType.Name)
                        {
                            case "Int32":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt32(rawdata[index]) : 0);
                                break;
                            case "Int64":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToInt64(rawdata[index]) : 0);
                                break;
                            case "Decimal":
                                descriptor.SetValue(model, !string.IsNullOrWhiteSpace(rawdata[index]) ? Convert.ToDecimal(rawdata[index]) : 0);
                                break;
                            default: //string
                                descriptor.SetValue(model, rawdata[index]);
                                break;

                        }
                    }
                }
            }
            return model;
        }

        #endregion

        #region SingleUpload
        [BreadCrumb(Clear = true, Label = "Single Member Participant")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddSingleParticipant()
        {
            GetCampaignList();
            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Single Member Participant")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult AddSingleParticipant(int memberID, int campaignID)
        {
            if (memberID == null && campaignID == null)
            {
                ModelState.AddModelError("Error", "Please Complete the field");
            }
            else
            {
                //cek campaign yang sama
                var cekParticipantSameCampaign = AutomaticCampaignParticipant.GetAll(false).Where(x => x.MemberId == memberID && x.IsActive == true && x.CampaignID == campaignID);
                if (cekParticipantSameCampaign != null && cekParticipantSameCampaign.Count() > 0)
                {
                    return RedirectToAction(MVC.MemberParticipantACB.Index("Failed, Member Has Been Added At That Campaign"));
                }

                //cek member ada dicampaign lain
                var cekParticipantACB = AutomaticCampaignParticipant.GetAll(false).Where(x => x.MemberId == memberID && x.IsActive == true);
                if (cekParticipantACB != null && cekParticipantACB.Count() > 0)
                {
                    AutomaticCampaignParticipant participantACBDelete = AutomaticCampaignParticipant.GetById(Convert.ToInt32(cekParticipantACB.Select(x => x.ID).FirstOrDefault()));
                    participantACBDelete.IsActive = false;
                    var resultDelete = participantACBDelete.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                }
                
                AutomaticCampaignParticipant participantACB = new AutomaticCampaignParticipant();
                ParticipantInsertModel participantModel = new ParticipantInsertModel();
                participantModel.CampaignID = campaignID;
                participantModel.MemberId = memberID;
                participantModel.CreatedBy = CurrentUser.FullName;
                participantModel.CreatedDate = DateTime.Now;
                participantModel.BonusPoint = 0;
                participantModel.Point = 0;
                participantModel.IsAchieve = false;
                participantModel.IsDeleted = false;
                participantModel.IsActive = true;
                
                participantACB.InjectFrom(participantModel);
                var result = participantACB.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                {
                    return RedirectToAction(MVC.MemberParticipantACB.Index("Create_Success"));
                }
                else
                {
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
                }
            }
            GetCampaignList();
            return View();
        }
        #endregion

        #region GridMember

        [HttpGet]
        public virtual ActionResult GridGetAllPager(int? campaignID, string search, string orderBy, string orderDirection, int? page)
        {
            int _page = page.GetValueOrDefault(1),
                _limit = 5,
                _totalRow = 0;
            string _orderBy = orderBy ?? "MemberName",
                _orderDirection = orderDirection ?? "Asc";
            if (_page < 1) _page = 1;
            if (campaignID == null)
            {
                ViewBag.CurrentPage = page.GetValueOrDefault(1);
                ViewBag.TotalPage = 0;
                ViewBag.TotalData = 0;
                ViewBag.CampaignID = 0;
                ViewBag.OrderColumn = _orderBy;
                ViewBag.OrderDirection = _orderDirection;
                return PartialView(new List<ParticipantModel>());
            }
            gridMvcHelper = new GridMvcHelper();
            var filteredCampaign = AutomaticCampaignParticipant.GetByCampaignID(campaignID.Value);
            var datas = from a in Member.GetAll()
                        join c in filteredCampaign on a.ID equals c.MemberId
                        select new ParticipantModel
                        {
                            MemberID = a.ID,
                            MemberName = a.Name,
                            Email = a.Email,
                            Phone = a.Phone,
                            Point = c.Point,
                            CampaignID = c.CampaignID
                        };
            _totalRow = datas.Count();
            var filteredDatas = datas
                                .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection))
                                .Skip((_page - 1) * _limit)
                                .Take(_limit)
                                .Select(x => new ParticipantModel
                                {
                                    MemberID = x.MemberID,
                                    MemberName = x.MemberName,
                                    Email = x.Email,
                                    Phone = x.Phone,
                                    Point = x.Point,
                                    CampaignID = x.CampaignID
                                });
            if (!string.IsNullOrEmpty(search))
            {
                filteredDatas = datas.Where(x => x.MemberName.Contains(search) || x.Email.Contains(search) || x.Phone.Contains(search))
                    .OrderBy(string.Format("{0} {1}", _orderBy, _orderDirection))
                    .Skip((_page - 1) * _limit)
                    .Take(_limit)
                    .Select(x => new ParticipantModel
                    {
                        MemberID = x.MemberID,
                        MemberName = x.MemberName,
                        Email = x.Email,
                        Phone = x.Phone,
                        Point = x.Point,
                        CampaignID = x.CampaignID
                    });
                _totalRow = filteredDatas.Count();
            }
            ViewBag.TotalPage = (int)Math.Ceiling((double)_totalRow / _limit);
            ViewBag.TotalData = _totalRow;
            ViewBag.CampaignID = campaignID;
            ViewBag.Search = search;
            ViewBag.CurrentPage = _page;
            ViewBag.OrderColumn = _orderBy;
            ViewBag.OrderDirection = _orderDirection;
            return PartialView(filteredDatas.ToList());
        }
        #endregion

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            var currentUser = CurrentUser;
            AutomaticCampaignParticipant Participant = AutomaticCampaignParticipant.GetAll(false).Where(x => x.MemberId == ID).FirstOrDefault();
            if (Participant != null)
            {
                Participant.IsDeleted = true;
                Participant.IsActive = false;
                var memberResponse = Participant.Delete(currentUser.FullName, Request.Url.AbsoluteUri, currentUser.ID);
                if (memberResponse.Success)
                {
                    return RedirectToAction(MVC.MemberParticipantACB.Index("Delete_Success"));
                }
                else
                {
                    return RedirectToAction(MVC.MemberParticipantACB.Index("Delete_Failed"));
                }
            }
            return RedirectToAction(MVC.MemberParticipantACB.Index("Delete_Failed"));
        }


        private void GetCampaignList()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Campaign> listdata = Campaign.GetAll().Where(x => x.IsAutomate == true);
            foreach (Campaign data in listdata)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.CampaignList = items;
        }
    }
}