﻿using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Notification;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Parameter.Notification;
using LoyaltyPointEngine.MemberLogicLayer;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class NotificationManagerController : BaseController
    {
        [BreadCrumb(Clear = true, Label = "Notification Manager")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            return Redirect(Url.Action(MVC.NotificationManager.Send()));
            //switch (status)
            //{
            //    case "Success_Send":
            //        ViewBag.Success = "Notification successfully send to member.";
            //        break;
            //    case "Success_Send_All":
            //        ViewBag.Success = "Notification successfully send to all member.";
            //        break;
            //    case "Failed_Send":
            //        ViewBag.Success = "Failed to send notification send to member.";
            //        break;
            //    case "Failed_Send_All":
            //        ViewBag.Success = "Failed to send notification to all member.";
            //        break;
            //    default:
            //        ViewBag.Success = status;
            //        break;
            //}
            //return View();
        }

        [BreadCrumb(Label = "Send Notification")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Send(string status)
        {
            switch (status)
            {
                case "Success_Send":
                    ViewBag.Success = "Notification successfully send to member.";
                    break;
                case "Success_Send_All":
                    ViewBag.Success = "Notification successfully send to all member.";
                    break;
                case "Failed_Send":
                    ViewBag.Success = "Failed to send notification send to member.";
                    break;
                case "Failed_Send_All":
                    ViewBag.Success = "Failed to send notification to all member.";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            GetDropdown();
            ViewBag.NotificationTypeList = EnumHelper.ToSelectList<NotificationType.Type>();
            return View(new ParamSendNotification
            {
                Point = 0
            });
        }

        [HttpPost]
        [BreadCrumb(Label = "Send Notification")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Send(ParamSendNotification model)
        {
            int[] arrayOfIDs = null;
            if (!model.ToAllMembers)
            {
                if (!string.IsNullOrEmpty(model.MemberIDs))
                {
                    try
                    {
                        arrayOfIDs = model.MemberIDs.Split(',').Select(x => int.Parse(x)).ToArray();
                        if (arrayOfIDs.Length < 1)
                        {
                            ModelState.AddModelError("MemberIDs", "Please select 1 or more members to send");
                        }
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("MemberIDs", "Invalid value of Member");
                    }
                }
                else
                {
                    ModelState.AddModelError("MemberIDs", "Please select 1 or more members to send");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.Month.ToString()))
                {
                    ModelState.AddModelError("Month", "Month can not be empty");
                }
                if (string.IsNullOrEmpty(model.Year.ToString()))
                {
                    ModelState.AddModelError("Year", "Year can not be empty");
                }

                arrayOfIDs = Member.GetAll().Where(x => !x.IsDeleted && x.IsActive && !x.IsBlacklist && x.CreatedDate.Value.Year == model.Year && x.CreatedDate.Value.Month == model.Month).Select(x => x.ID).ToArray();
            }

            if (string.IsNullOrEmpty(model.Title))
            {
                ModelState.AddModelError("Title", "Title can not be empty");
            }
            if (string.IsNullOrEmpty(model.Message))
            {
                ModelState.AddModelError("Message", "Message can not be empty");
            }
            if (string.IsNullOrEmpty(model.Type))
            {
                ModelState.AddModelError("Type", "Select one of availabe notification type");
            }
            if (ModelState.IsValid)
            {
                int errorCount = 0;
                var currentUserName = CurrentUser.Username;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    Notification notification = null;
                    EFResponse response = null;

                    foreach (int id in arrayOfIDs)
                    {
                        var member = Member.GetById(id);
                        if (member == null || (!member.IsActive || member.IsDeleted || member.IsBlacklist))
                        {
                            errorCount++;
                            ModelState.AddModelError("MemberIDs", string.Format("Member with ID {0} is not found or not active", id));
                        }
                        else
                        {
                            notification = new Notification();
                            notification.ID = Guid.NewGuid();
                            notification.MemberID = member.ID;
                            notification.Title = model.Title;
                            notification.Message = model.Message;
                            notification.Type = model.Type;
                            notification.ReferenceCode = model.ReferenceCode;
                            notification.Point = model.Point;
                            if (notification.Type == Convert.ToString(NotificationType.Type.Custom))
                            {
                                notification.LeftIcon = model.LeftIcon;
                                notification.RightIcon = model.RightIcon;
                            }
                            response = notification.Inserted(currentUserName);
                            if (response != null)
                            {
                                if (!response.Success)
                                {
                                    NotificationLogic.AppendNewData(notification.MemberID, new ParamCreateNotification
                                    {
                                        ID = notification.ID,
                                        Title = notification.Title,
                                        Message = notification.Message,
                                        Type = notification.Type,
                                        ReferenceCode = notification.ReferenceCode,
                                        Point = notification.Point ?? 0,
                                        LeftIcon = notification.LeftIcon,
                                        RightIcon = notification.RightIcon
                                    });
                                    errorCount++;
                                    ModelState.AddModelError("Common", response.ErrorMessage);
                                }
                            }
                            else
                            {
                                errorCount++;
                                ModelState.AddModelError("Common", string.Format("Failed while send notification to member {0}", member.ID));
                            }
                        }
                    }
                    if (errorCount > 0)
                    {
                        if (notification != null)
                        {
                            notification.IsDeleted = true;
                            notification.Updated(currentUserName);
                        }
                    }
                    else
                    {
                        transScope.Complete();
                        transScope.Dispose();
                    }
                }

                if (errorCount == 0)
                {
                    List<Data.PoolMemberToken> arrayOfAndroidToken = new List<Data.PoolMemberToken>(),
                        arrayOfIosToken = new List<Data.PoolMemberToken>();
                    foreach (int id in arrayOfIDs)
                    {
                        List<Data.PoolMemberToken> androidMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(id, "ANDROID"),
                            iosMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(id, "IOS");

                        if (androidMobileTokens != null && androidMobileTokens.Count > 0)
                        {
                            arrayOfAndroidToken.AddRange(androidMobileTokens);

                        }

                        if (iosMobileTokens != null && iosMobileTokens.Count > 0)
                        {
                            arrayOfIosToken.AddRange(iosMobileTokens);
                        }
                    }

                    if (arrayOfAndroidToken.Count > 0)
                    {
                        PushNotification.SendAndroid(arrayOfAndroidToken.Select(x => x.DeviceID).ToArray(), model.Title, model.Message, model.LeftIcon, model.RightIcon);
                    }

                    if (arrayOfIosToken.Count > 0)
                    {
                        PushNotification.SendIOS(arrayOfIosToken.Select(x => x.DeviceID).ToArray(), model.Title, model.Message, 1);
                    }

                    if (model.ToAllMembers)
                        return Redirect(Url.Action(MVC.NotificationManager.Send("Success_Send_All")));
                    return Redirect(Url.Action(MVC.NotificationManager.Send("Success_Send")));
                }
            }
            GetDropdown();
            ViewBag.NotificationTypeList = EnumHelper.ToSelectList<NotificationType.Type>();
            return View(model);
        }
        private void GetDropdown()
        {
            ViewBag.Months = new SelectList(Enumerable.Range(1, 12).Select(x =>
                 new SelectListItem()
                 {
                     Text = (new DateTime(2017, x, 1)).ToString("MMMM"),
                     Value = x.ToString()
                 }), "Value", "Text");

            ViewBag.Years = new SelectList(Enumerable.Range(2015, (DateTime.Today.Year - 2015) + 1).Select(x =>
                 new SelectListItem()
                 {
                     Text = x.ToString(),
                     Value = x.ToString()
                 }), "Value", "Text");
        }
    }
}