﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System.Globalization;
using LoyaltyPointEngine.Model.Parameter.Receipt;
using System.IO;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.PointLogicLayer;
using System.Transactions;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Receipt;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Model.Object.Common;
using System.Data.Objects;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ReceiptApprovalReportingController : BaseController
    {
        // GET: Receipt
        private IGridMvcHelper gridMvcHelper;
        private string spgRoleCode;

        [BreadCrumb(Clear = true, Label = "Receipt")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Receipt successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Receipt successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Receipt successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            ViewBag.AdminList = Data.CMSUser.GetUserAdminSuperAdmin().ToList();
            return View();
        }

        [HttpGet]
        public virtual ActionResult GridGetAllReceiptPager(int? page, string memberIDHidden, string hiddenAwal, string hiddenAkhir, string orderColumn = "UpdatedByID", string orderDirection = "Asc")
        {
            int tempMemberId = 0,
                limit = 5;

            System.Data.Objects.ObjectParameter outPutTotalRow = new System.Data.Objects.ObjectParameter("TotalRow", typeof(Int64));

            int? paramMemberId = null,
                paramPage = page.GetValueOrDefault(1) < 1 ? 1 : page.GetValueOrDefault(1);

            if (int.TryParse(memberIDHidden, out tempMemberId))
                paramMemberId = tempMemberId;

            if (string.IsNullOrEmpty(hiddenAkhir))
            {
                hiddenAkhir = DateTime.Now.ToString("dd/MM/yyyy");
            }

            ViewBag.MemberId = memberIDHidden;
            ViewBag.TglAwal = hiddenAwal;
            ViewBag.TglAkhir = hiddenAkhir;

            DateTime tempStartDate = DateTime.MinValue,
                tempEndDate = DateTime.MinValue;

            DateTime? startDate = null,
                endDate = null;

            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempStartDate))
                    startDate = tempStartDate;
            }
            else
            {
                startDate = DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", CultureInfo.CurrentCulture);
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempEndDate))
                    endDate = tempEndDate;
            }

            List<GetReceiptReport_Result> gridReceipt = DataRepositoryFactory.CurrentRepository
                .GetReceiptReport(paramMemberId, startDate, endDate, paramPage, limit, orderColumn, orderDirection, outPutTotalRow)
                .ToList();

            ViewBag.CurrentPage = paramPage;
            ViewBag.TotalPage = (int)Math.Ceiling(Convert.ToDouble(outPutTotalRow.Value) / limit);
            ViewBag.OrderColumn = orderColumn ?? "UpdatedByID";
            ViewBag.OrderDirection = orderDirection ?? "Asc";
            ViewBag.TotalRow = Convert.ToInt32(outPutTotalRow.Value);
            return PartialView(MVC.ReceiptApprovalReporting.Views.GridGetAllReceipt, gridReceipt);
        }
    }
}