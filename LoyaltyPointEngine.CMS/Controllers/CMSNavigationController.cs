﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Model.Object.CMSNavigation;

namespace LoyaltyPointEngine.CMS.Controllers
{

    public partial class CMSNavigationController : BaseController
    {

        private IGridMvcHelper gridMvcHelper;
        //
        // GET: /CMSNavigation/
        [BreadCrumb(Clear = true, Label = "Navigation")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Navigation successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Navigation successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Navigation successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Navigation")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            NavigationModel model = new NavigationModel();
            GetListNavigation();
            GetListPrevilege();
            return View(model);
        }
        
        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(NavigationModel model)
        {
            CMSUserInformation _CMSInformation = new CMSUserInformation();
            if (ModelState.IsValid)
            {
                CMSNavigation navigation = NavigationModelToCMSNavigation(model);


                var result = navigation.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                {
                    //set navigation previlage
                    NavigationAddPrevilage(model.Previlage, navigation.ID);

                    SetRemoveCurrentNavigationCache();
                    _CMSInformation.SetRemoveCurrentAllowedControllerActionCache();
                    return RedirectToAction(MVC.CMSNavigation.Index("Create_Success"));
                }
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListNavigation();
            GetListPrevilege();
            return View(model);
        }

        [BreadCrumb(Label = "Edit Navigation")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(long NavId)
        {
            CMSNavigation currentNav = CMSNavigation.GetByID(NavId);
            if (currentNav == null)
            {
                return RedirectToAction(MVC.CMSNavigation.Index());
            }
            GetListNavigation();
            GetListPrevilege();
            ViewBag.currentNavPrevilage = CMSNavigationPrivilegeMapping.GetByNavigation(currentNav.ID).ToList();
            NavigationModel model = CMSNavigationToNavigationModel(currentNav);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(NavigationModel model)
        {
            CMSUserInformation _CMSInformation = new CMSUserInformation();
            if (ModelState.IsValid)
            {
                CMSNavigation navigation = CMSNavigation.GetByID(model.ID);
                navigation.Name = model.Name;
                navigation.Controller = model.Controller;
                navigation.sort = model.sort;
                navigation.IsChild = (model.ParentID != null);
                navigation.ParentID = model.ParentID;
                navigation.IsHide = model.IsHide;
                var result = navigation.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                {
                    NavigationEditPrevilage(model.Previlage, navigation.ID);
                    SetRemoveCurrentNavigationCache();
                    _CMSInformation.SetRemoveCurrentAllowedControllerActionCache();
                    return RedirectToAction(MVC.CMSNavigation.Index("Edit_Success"));
                }
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListNavigation();
            GetListPrevilege();
            return View(model);
        }

        private void GetListNavigation()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<CMSNavigation> listdata = LoyaltyPointEngine.Data.CMSNavigation.GetAll();
            foreach (CMSNavigation data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListNavigationParent = items;
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(long ID)
        {
            CMSNavigation navigation = CMSNavigation.GetByID(ID);
            navigation.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.CMSNavigation.Index("Delete_Success"));
        }

        private CMSNavigation NavigationModelToCMSNavigation(NavigationModel model)
        {
            CMSNavigation navigation = new CMSNavigation();
            navigation.Name = model.Name;
            navigation.Controller = model.Controller;
            navigation.sort = model.sort;
            navigation.IsChild = (model.ParentID != null);
            navigation.ParentID = model.ParentID;
            navigation.IsHide = model.IsHide;
            return navigation;
        }

        private NavigationModel CMSNavigationToNavigationModel(CMSNavigation model)
        {
            NavigationModel navigation = new NavigationModel();
            navigation.ID = model.ID;
            navigation.Name = model.Name;
            navigation.Controller = model.Controller;
            navigation.sort = model.sort;
            navigation.ParentID = model.ParentID;
            navigation.IsHide = model.IsHide;
            return navigation;
        }
        private void NavigationEditPrevilage(long[] Previlage, long NavigationID)
        {
            List<CMSNavigationPrivilegeMapping> NavigationPrevilages = CMSNavigationPrivilegeMapping.GetByNavigation(NavigationID).ToList();

            foreach (CMSNavigationPrivilegeMapping NavigationPrevilage in NavigationPrevilages)
            {
                if (!Previlage.Any(x => x == NavigationPrevilage.CMSPrivilegeID))
                {
                    NavigationPrevilage.IsActive = false;
                    var result = NavigationPrevilage.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                }
                else
                {
                    NavigationPrevilage.IsActive = true;
                    NavigationPrevilage.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    Previlage = Previlage.Where(x => x != NavigationPrevilage.CMSPrivilegeID).ToArray();
                }

            }

            if (Previlage.Count() > 0)
            {
                foreach (long _Previlage in Previlage)
                {
                    CMSNavigationPrivilegeMapping _CMSNavigationPrivilegeMapping = CMSNavigationPrivilegeMapping.GetByNavigationAndPrevilage(NavigationID, _Previlage);
                    if (_CMSNavigationPrivilegeMapping != null)
                    {
                        _CMSNavigationPrivilegeMapping.IsActive = true;
                        _CMSNavigationPrivilegeMapping.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    }
                    else
                    {
                        CMSNavigationPrivilegeMapping _CMSNavigationPrivilegeMappingNew = new CMSNavigationPrivilegeMapping();
                        _CMSNavigationPrivilegeMappingNew.CMSNavigationID = NavigationID;
                        _CMSNavigationPrivilegeMappingNew.CMSPrivilegeID = _Previlage;
                        _CMSNavigationPrivilegeMappingNew.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                    }
                }
            }
        }

        private void NavigationAddPrevilage(long[] Previlage, long NavigationID)
        {
            if (Previlage != null)
            {
                foreach (long _Previlage in Previlage)
                {
                    CMSNavigationPrivilegeMapping _CMSNavigationPrivilegeMapping = new CMSNavigationPrivilegeMapping();
                    _CMSNavigationPrivilegeMapping.CMSNavigationID = NavigationID;
                    _CMSNavigationPrivilegeMapping.CMSPrivilegeID = _Previlage;
                    _CMSNavigationPrivilegeMapping.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                }
            }
        }
        private void GetListPrevilege()
        {
            List<CMSPrivilege> _CMSPrevilege = CMSPrivilege.GetAll().ToList();
            ViewBag.CMSPrevilage = _CMSPrevilege;
        }
        #region List Navigation Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllNavigation()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSNavigation> allNavigation = CMSNavigation.GetAll().OrderBy(x => x.sort);
            AjaxGrid<CMSNavigation> grid = this.gridMvcHelper.GetAjaxGrid(allNavigation);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllNavigationPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<CMSNavigation> allData = CMSNavigation.GetAll().OrderBy(x => x.sort);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch) || x.Controller.Contains(txtSearch)).OrderBy(x => x.sort);
            }
            AjaxGrid<CMSNavigation> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.CMSNavigation.Views.GridGetAllNavigation, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
