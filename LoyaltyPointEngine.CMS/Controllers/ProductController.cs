﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Product;
using LoyaltyPointEngine.Common;
using System.IO;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ProductController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Product")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Product successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Product successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Product successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Label = "Detail Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int id)
        {
            var data = Product.GetByID(id);
            ProductModel model = new ProductModel(data);
            PopulateListGroup(model.ProductGroupID);
            ListSize(model.Size);
            ListFlavour(model.Flavour);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ProductModel model, HttpPostedFileBase ProductImage)
        {
            ValidateEdit(model, ProductImage);

            if (ModelState.IsValid)
            {
                string by = CurrentUser.FullName;
                Product data = Product.GetByID(model.ID);
                if (data != null)
                {
                    data.ProductGroupID = model.ProductGroupID;
                    data.Code = model.Code;
                    data.Name = model.Name;
                    data.Description = model.Description;

                    if (ProductImage != null)
                    {
                        string fileName = SaveImageProduct(model.Code + ".jpg", ProductImage);
                        data.ProductImage = fileName;
                    }

                    data.Size = model.Size;
                    data.Flavour = model.Flavour;
                    data.IsDisplay = model.IsDisplay;
                    data.Price = model.Price;
                    data.IsPregnancyProduct = model.IsPregnancyProduct;
                    data.SnapcartSKU = model.SnapcartSKU;

                    var ressEF = data.Update(by);
                    if (ressEF.Success)
                    {
                        return RedirectToAction(MVC.Product.Index("Edit_Success"));
                    }
                    else
                    {
                        ModelState.AddModelError("", ressEF.ErrorMessage);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "ID not founded");
                }
            }

            PopulateListGroup(model.ProductGroupID);
            ListSize(model.Size);
            ListFlavour(model.Flavour);
            return View(model);
        }

        private void ValidateEdit(ProductModel model, HttpPostedFileBase ProductImage)
        {
            if (ProductImage == null)
            {
                // ModelState.AddModelError("ProductImage", "ProductImage is required");
            }


            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
            else
            {
                var predictedData = Data.Product.GetByCodeNotSelf(model.Code, model.ID);
                if (predictedData != null)
                {
                    ModelState.AddModelError("Code", "The code has already been used by other data");
                }
            }


            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }

            if (string.IsNullOrEmpty(model.Description))
            {
                ModelState.AddModelError("Description", "Description is required");
            }





            if (string.IsNullOrEmpty(model.Size))
            {
                ModelState.AddModelError("Size", "Size is required");
            }



            if (string.IsNullOrEmpty(model.Flavour))
            {
                ModelState.AddModelError("Flavour", "Flavour is required");
            }

        }

        [BreadCrumb(Label = "Add Product Group")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ProductModel model = new ProductModel();
            PopulateListGroup(model.ProductGroupID);
            ListSize(model.Size);
            ListFlavour(model.Flavour);
            return View();
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(ProductModel model, HttpPostedFileBase ProductImage)
        {
            ValidateAdd(model, ProductImage);

            if (ModelState.IsValid)
            {
                string fileName = SaveImageProduct(model.Code + ".jpg", ProductImage);
                if (!string.IsNullOrEmpty(fileName))
                {
                    string by = CurrentUser.FullName;
                    Product data = new Product();
                    data.ProductGroupID = model.ProductGroupID;
                    data.Code = model.Code;
                    data.Name = model.Name;
                    data.Description = model.Description;
                    data.ProductImage = fileName;
                    data.Size = model.Size;
                    data.Flavour = model.Flavour;
                    data.IsDisplay = model.IsDisplay;
                    data.Price = model.Price;
                    data.IsPregnancyProduct = model.IsPregnancyProduct;
                    data.SnapcartSKU = model.SnapcartSKU;
                    var ressEF = data.Insert(by);
                    if (ressEF.Success)
                    {
                        return RedirectToAction(MVC.Product.Index("Create_Success"));
                    }
                    else
                    {
                        ModelState.AddModelError("", ressEF.ErrorMessage);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Saving file is Fail");
                }
            }

            PopulateListGroup(model.ProductGroupID);
            ListSize(model.Size);
            ListFlavour(model.Flavour);
            return View(model);
        }

        private void ValidateAdd(ProductModel model, HttpPostedFileBase ProductImage)
        {
            if (ProductImage == null)
            {
                ModelState.AddModelError("ProductImage", "ProductImage is required");
            }


            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");
            }
            else
            {
                var predictedData = Data.Product.GetByCode(model.Code);
                if (predictedData != null)
                {
                    ModelState.AddModelError("Code", "The code has already been used by other data");
                }
            }


            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");
            }

            if (string.IsNullOrEmpty(model.Description))
            {
                ModelState.AddModelError("Description", "Description is required");
            }





            if (string.IsNullOrEmpty(model.Size))
            {
                ModelState.AddModelError("Size", "Size is required");
            }



            if (string.IsNullOrEmpty(model.Flavour))
            {
                ModelState.AddModelError("Flavour", "Flavour is required");
            }
        }

        private void PopulateListGroup(int groupID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<ProductGroup> listdata = ProductGroup.GetAll().Where(x => x.IsActived);
            foreach (var data in listdata)
            {

                items.Add(new SelectListItem { Text = data.ProductGroupCode, Value = data.ID.ToString(), Selected = data.ID == groupID });
            }
            ViewBag.ListProductGroup = items;
        }

        private string SaveImageProduct(string name, HttpPostedFileBase fileObj)
        {
            string MediaLocalImageDirectoryProduct = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryProduct);
            string RelativePathProduct = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathProduct);

            if (!Directory.Exists(MediaLocalImageDirectoryProduct))
            {
                Directory.CreateDirectory(MediaLocalImageDirectoryProduct);
            }


            string filename = StringHelper.SanitizePath(name, '_');
            string imageFulleName = MediaLocalImageDirectoryProduct.Substring(MediaLocalImageDirectoryProduct.Length - 1, 1).Contains("\\") ?
                    string.Format("{0}{1}", MediaLocalImageDirectoryProduct, filename) :
                    string.Format("{0}\\{1}", MediaLocalImageDirectoryProduct, filename);
            fileObj.SaveAs(imageFulleName);

            return RelativePathProduct + filename;

        }

        private void ListSize(string size)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            string productSizeList = Data.Settings.GetValueByKey(SiteSetting.ListProductSizeSettingKey);
            if (!string.IsNullOrEmpty(productSizeList) && !string.IsNullOrWhiteSpace(productSizeList))
            {
                string[] listProductSize = productSizeList.Split(',');
                foreach (string item in listProductSize)
                {
                    string newValue = item.Trim();
                    result.Add(new SelectListItem { Text = newValue, Value = newValue, Selected = newValue == size });
                }
            }
            else
            {
                var listEnum = Enum.GetValues(typeof(Size)).Cast<Size>().Select(v => v);
                foreach (var data in listEnum)
                {
                    string newValue = data.ToString().Replace("g", "") + " g";
                    result.Add(new SelectListItem { Text = newValue, Value = newValue, Selected = newValue == size });
                }
            }
            ViewBag.ListSize = result;
        }

        private void ListFlavour(string flavour)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            string productFlavourList = Data.Settings.GetValueByKey(SiteSetting.ListProductFlavourSettingKey);
            if (!string.IsNullOrEmpty(productFlavourList) && !string.IsNullOrWhiteSpace(productFlavourList))
            {
                string[] listProductFlavour = productFlavourList.Split(',');
                foreach (string item in listProductFlavour)
                {
                    string newValue = item.Trim();
                    result.Add(new SelectListItem { Text = newValue, Value = newValue, Selected = newValue == flavour });
                }
            }
            else
            {
                var listEnum = Enum.GetValues(typeof(Flavour)).Cast<Flavour>().Select(v => v);
                foreach (var data in listEnum)
                {
                    string newValue = data.ToString();
                    result.Add(new SelectListItem { Text = newValue, Value = newValue, Selected = newValue == flavour });
                }
            }
            ViewBag.ListFlavour = result;
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllProduct()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<LoyaltyPointEngine.Data.Product> datas = LoyaltyPointEngine.Data.Product.GetAll().OrderBy(x => x.Code);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllProductPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = LoyaltyPointEngine.Data.Product.GetAll().OrderBy(x => x.Code);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(x => x.Code.Contains(txtSearch) || x.Name.Contains(txtSearch) || x.Description.Contains(txtSearch) || x.Size.Contains(txtSearch)
                    || x.Flavour.Contains(txtSearch)

                    ).OrderBy(x => x.Code);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Product.Views.GridGetAllProduct, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}