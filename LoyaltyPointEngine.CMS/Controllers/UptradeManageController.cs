﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Extension;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Common;
using LoyaltyPointEngine.Model.Parameter.Uptrade;
using LoyaltyPointEngine.PointLogicLayer;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class UptradeManageController : BaseController
    {
        // GET: UptradeManage
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Manage Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Uptrade successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Uptrade successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Uptrade successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Clear = true, Label = "Edit Manage Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Edit(int ID)
        {
            UptradeManage model = UptradeManage.GetById(ID);
            if (model == null)
            {
                return RedirectToAction(MVC.UptradeManage.Index());
            }
            ParamUptradeManageModel param = new ParamUptradeManageModel();
            param.ID = model.ID;
            param.Channel = model.Channel;
            param.ProductAfter = model.ProductAfter.Value;
            param.ProductBefore = model.ProductBefore.Value;
            param.Point = model.Point.Value;
            param.Remarks = model.Remarks;
            param.FlagID = model.FlagID.Value;
            param.StartDate = model.StartDate.Value.ToString("dd/MM/yyyy");
            param.EndDate = model.EndDate.Value.ToString("dd/MM/yyyy");

            GetProductBefore(model.ProductBefore.Value);
            GetProductAfter(model.ProductAfter.Value);
            GetChannel(model.Channel);
            GetFlagUptradeManage(model.FlagID.Value);
            return View(param);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(ParamUptradeManageModel param)
        {
            ValidateEditModel(param);
            if (ModelState.IsValid)
            {
                var data = UptradeManage.GetById(param.ID);
                data.Channel = param.Channel;
                data.ProductBefore = param.ProductBefore;
                data.ProductAfter = param.ProductAfter;
                data.Point = param.Point;
                data.Remarks = param.Remarks;
                data.StartDate = DateTime.ParseExact( param.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                data.EndDate = DateTime.ParseExact( param.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                data.Update(CurrentUser.FullName);
                return RedirectToAction(MVC.UptradeManage.Index("Edit_Success"));
            }

            GetProductBefore(param.ProductBefore);
            GetProductAfter(param.ProductAfter);
            GetChannel(param.Channel);
            GetFlagUptradeManage(param.FlagID);
            return View(param);
        }

        [BreadCrumb(Clear = true, Label = "Add Manage Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Add()
        {
            GetProductBefore();
            GetProductAfter();
            GetChannel(null);
            GetFlagUptradeManage();
            return View();
        }

        [HttpPost]
        public virtual ActionResult Add(ParamUptradeManageModel param)
        {
            ValidateModel(param);
            if (ModelState.IsValid)
            {

                UptradeManage data = new UptradeManage();
                data.Channel = param.Channel;
                data.ProductBefore = param.ProductBefore;
                data.ProductAfter = param.ProductAfter;
                data.Point = param.Point;
                data.Remarks = param.Remarks;
                data.FlagID = param.FlagID;
                data.StartDate = DateTime.ParseExact(param.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                data.EndDate = DateTime.ParseExact(param.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                data.Insert(CurrentUser.FullName);
                return RedirectToAction(MVC.UptradeManage.Index("Create_Success"));
            }
            GetProductBefore(param.ProductBefore);
            GetProductAfter(param.ProductAfter);
            GetChannel(param.Channel);
            GetFlagUptradeManage(param.FlagID);
            return View(param);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            UptradeManage data = UptradeManage.GetById(ID);
            data.Delete(CurrentUser.FullName);
            return RedirectToAction(MVC.UptradeManage.Index("Delete_Success"));
        }

        [BreadCrumb(Clear = true, Label = "Sync Receipt in Uptrade")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Sync(int ID)
        {
            UptradeManage model = UptradeManage.GetById(ID);
            if (model == null)
            {
                return RedirectToAction(MVC.UptradeManage.Index());
            }
            var result = UptradeManageLogic.SyncTradelogic(Convert.ToInt32(model.ID), Request.Url.AbsoluteUri, CurrentUser.ID, CurrentUser.FullName);
            return View(result);
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllUpTradeManage()
        {
            gridMvcHelper = new GridMvcHelper();
            ParamUptradeManageModel param = new ParamUptradeManageModel();
            IOrderedQueryable<UptradeManageModel> allUptradeManage = GetListUptradeManage(param);
            AjaxGrid<UptradeManageModel> grid = this.gridMvcHelper.GetAjaxGrid(allUptradeManage);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllUpTradeManagePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            ParamUptradeManageModel param = new ParamUptradeManageModel();
            IOrderedQueryable<UptradeManageModel> allData = GetListUptradeManage(param);

            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Remarks.Contains(txtSearch) || x.Channel.Contains(txtSearch) || x.RemarksMember.Contains(txtSearch)
                    || x.ProductAfter.Contains(txtSearch) || x.ProductBefore.Contains(txtSearch) || x.Point.ToString().Contains(txtSearch)
                    || x.StartDate.ToString("dd-mm-yyyy").Contains(txtSearch) || x.EndDate.ToString("dd-mm-yyyy").Contains(txtSearch)
                    ).OrderBy(x => x.Remarks);
            }
            AjaxGrid<UptradeManageModel> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.UptradeManage.Views.GridGetAllUpTradeManage, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private static IOrderedQueryable<UptradeManageModel> GetListUptradeManage(ParamUptradeManageModel param)
        {
            var datas = UptradeManage.GetAll();
            var data = from a in datas
                       join b in Product.GetAll() on a.ProductAfter equals b.ID
                       join c in Product.GetAll() on a.ProductBefore equals c.ID
                       join d in FlagUptradeManage.GetAll() on a.FlagID equals d.ID
                       select new UptradeManageModel
                       {
                           ID = a.ID,
                           ProductAfter = b.Name,
                           ProductBefore = c.Name,
                           Remarks = a.Remarks,
                           Point = a.Point.Value,
                           Channel = a.Channel,
                           RemarksMember = d.StatusFlag,
                           CreatedDate = a.CreatedDate.Value,
                           StartDate = a.StartDate.Value,
                           EndDate = a.EndDate.Value
                       };

            var ret = data.AsEnumerable().Select(d => new UptradeManageModel
            {
                ID = d.ID,
                ProductAfter = d.ProductAfter,
                ProductBefore = d.ProductBefore,
                Remarks = d.Remarks,
                RemarksMember = d.RemarksMember,
                Channel = d.Channel,
                Point = d.Point,
                CreatedDate = d.CreatedDate,
                StartDate = d.StartDate,
                EndDate = d.EndDate
            });
            return ret.AsQueryable().OrderBy(x => x.CreatedDate);
        }

        public void GetProductBefore(long ID = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Product> listdata = Product.GetAll();
            foreach (Product data in listdata)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = ID.ToString() == data.ID.ToString() });
            }
            ViewBag.ListProductBefore = items;
        }

        private void GetChannel(string Channel)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listchannel = Enum.GetValues(typeof(Channel)).Cast<Channel>()
                        .Select(r => new { Value = (int)r, Name = r.ToString() }).ToList();
            string item = "All Channel";
            items.Add(new SelectListItem { Text = item, Value = item, Selected = Channel == item });
            foreach (var data in listchannel)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.Name, Selected = Channel == data.Name.ToString() });
            }
            ViewBag.ListChannel = items;
        }

        public void GetProductAfter(long ID = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Product> listdata = Product.GetAll();
            foreach (Product data in listdata)
            {
                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = ID.ToString() == data.ID.ToString() });
            }
            ViewBag.ListProductAfter = items;
        }

        public void GetFlagUptradeManage(long ID = 0)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<FlagUptradeManage> listdata = FlagUptradeManage.GetAll();
            foreach (var data in listdata)
            {
                items.Add(new SelectListItem { Text = data.StatusFlag, Value = data.ID.ToString(), Selected = ID.ToString() == data.ID.ToString() });
            }
            ViewBag.ListFlagUptradeManage = items;
        }
        private void ValidateModel(ParamUptradeManageModel model)
        {
            ValidateEditModel(model);
            DateTime dts = DateTime.Now;
            var checkData = UptradeManage.GetAll()
                .Where(x => x.ProductAfter == model.ProductAfter && x.ProductBefore == model.ProductBefore && x.StartDate <= dts && x.EndDate >= dts).ToList();
            if (checkData.Count >= 1)
            {
                ModelState.AddModelError("ProductAfter", "Uptrade is already");
            }
        }

        private void ValidateEditModel(ParamUptradeManageModel model)
        {
            if (model.ProductAfter == null || model.ProductAfter < 1)
            {
                ModelState.AddModelError("ProductAfter", "Product After is required");
            }

            if (model.ProductBefore == null || model.ProductBefore < 1)
            {
                ModelState.AddModelError("ProductBefore", "Product Before is required");
            }

            if (model.FlagID == null || model.FlagID < 1)
            {
                ModelState.AddModelError("FlagID", "Remarks Member is required");
            }

            if (string.IsNullOrEmpty(model.StartDate.ToString()))
            {
                ModelState.AddModelError("StartDate", "Start Date is required");
            }

            if (string.IsNullOrEmpty(model.EndDate.ToString()))
            {
                ModelState.AddModelError("EndDate", "End Date is required");
            }
            DateTime StartDate = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (EndDate <= StartDate)
            {
                ModelState.AddModelError("Error", "End Date below Start Date");
            }

            if (model.ProductBefore ==  model.ProductAfter)
            {
                ModelState.AddModelError("Error", "Product Before is same");
            }
        }

    }
}