﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Collect;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using System.Globalization;
using LoyaltyPointEngine.PointLogicLayer.Collect;
using System.Transactions;
using LoyaltyPointEngine.Model.Object.Action;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class CollectController : BaseController
    {
        // GET: Collect
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Collect")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Collect successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Collect successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Collect successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Collect")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            CollectCMSModel model = new CollectCMSModel();
            GetListAction();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(CollectCMSModel model)
        {
            if (ModelState.IsValid)
            {
                CollectLogic collect = new CollectLogic();
                model.QuantityApproved = model.Quantity;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    var res = collect.InsertCollect(model, CurrentPool.ID, CurrentUser.FullName, CurrentUser.ID, Request.Url.AbsoluteUri,0);
                    if (res.StatusCode == "00")
                    {
                        transScope.Complete();
                        transScope.Dispose();
                        PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(model.MemberID, CurrentUser.FullName);
                        return RedirectToAction(MVC.Collect.Index("Create_Success"));
                    }
                    else
                    {
                        transScope.Dispose();
                        ModelState.AddModelError("error data", res.StatusMessage);
                    }
                }
            }
            GetListAction();
            GetListCamapign();
            return View();
        }

        [BreadCrumb(Label = "Edit Collect")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(Guid CollectId)
        {
            Collect currentCollect = LoyaltyPointEngine.Data.Collect.GetById(CollectId);
            CollectCMSModel model = new CollectCMSModel();
            model.InjectFrom(currentCollect);
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(CollectCMSModel model)
        {
            if (ModelState.IsValid)
            {
                Collect Collect = LoyaltyPointEngine.Data.Collect.GetById(model.ID);
                Collect.InjectFrom(model);
                var result = Collect.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.Collect.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            return View();
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(Guid ID)
        {
            Collect Collect = Collect.GetById(ID);
            Collect.Deleted(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.Collect.Index("Delete_Success"));
        }

        private void GetListCamapign()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoyaltyPointEngine.Data.Campaign> listdata = LoyaltyPointEngine.Data.Campaign.GetAll();
            foreach (LoyaltyPointEngine.Data.Campaign data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListCampaign = items;
        }
        private void GetListAction()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoyaltyPointEngine.Data.Action> listdata = LoyaltyPointEngine.Data.Action.GetAll().Where(x=> x.IsActive);
            foreach (LoyaltyPointEngine.Data.Action data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListAction = items;
        }

        #region List Collect Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllCollect()
        {
            gridMvcHelper = new GridMvcHelper();
            //var allCollect = LoyaltyPointEngine.Data.Collect.GetAll().OrderByDescending(x => x.CreatedDate);
            var data = from a in LoyaltyPointEngine.Data.Collect.GetAll()
                       join b in LoyaltyPointEngine.Data.Member.GetAll() on a.MemberID equals b.ID into memberTemp
                       from b_temp in memberTemp.DefaultIfEmpty()
                       join c in LoyaltyPointEngine.Data.Campaign.GetAll() on a.CampaignID equals c.ID into TempC
                       from c_temp in TempC.DefaultIfEmpty()
                       select new CollectCMSTableModel
                       {
                           MemberName = b_temp.Name,
                           CampaignName = c_temp.Name,
                           ActionName = a.Action.Name,
                           Quantity = a.Quantity,
                           QuantityApproved = a.QuantityApproved,
                           Points = a.Points,
                           TotalPoints = a.TotalPoints,
                           Remarks = a.Remarks,
                           CreatedDateTime = a.CreatedDate,
                           ExpiryDateTime = a.ExpiryDate
                       };


            var grid = this.gridMvcHelper.GetAjaxGrid(data.OrderByDescending(x => x.CreatedDateTime));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllCollectPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            // IOrderedQueryable<Collect> allData = LoyaltyPointEngine.Data.Collect.GetAll().OrderByDescending(x => x.CreatedDate);

            var data = from a in LoyaltyPointEngine.Data.Collect.GetAll()
                       join b in LoyaltyPointEngine.Data.Member.GetAll() on a.MemberID equals b.ID into memberTemp
                       from b_temp in memberTemp.DefaultIfEmpty()
                       join c in LoyaltyPointEngine.Data.Campaign.GetAll() on a.CampaignID equals c.ID into TempC
                       from c_temp in TempC.DefaultIfEmpty()
                       select new CollectCMSTableModel
                       {
                           MemberName = b_temp.Name,
                           CampaignName = c_temp.Name,
                           ActionName = a.Action.Name,
                           Quantity = a.Quantity,
                           QuantityApproved = a.QuantityApproved,
                           Points = a.Points,
                           TotalPoints = a.TotalPoints,
                           Remarks = a.Remarks,
                           CreatedDateTime = a.CreatedDate,
                           ExpiryDateTime = a.ExpiryDate
                       };
            if (!string.IsNullOrEmpty(txtSearch))
            {
                //allData = allData.Where(x => x.Action.Name.Contains(txtSearch)).OrderByDescending(x => x.CreatedDate);
                data = data.Where(x => x.ActionName.Contains(txtSearch) || x.MemberName.Contains(txtSearch) || x.CampaignName.Contains(txtSearch)
                     || x.Remarks.Contains(txtSearch)
                    );
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(data.OrderByDescending(x => x.CreatedDateTime), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Collect.Views.GridGetAllCollect, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}