﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object.Promo;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class PromoReportingController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;
        // GET: PromoReporting
        [BreadCrumb(Clear = true, Label = "Promo Reporting")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Promo successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Promo successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Promo successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Member")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string txtSearch, string type = "")
        {
            List<GetReportPromoSMS_Result> datas = Data.Promo.GetReportPromoSMS().ToList();

            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas.Where(y => y.PromoCode.ToLower().Contains(txtSearch.ToLower()) || y.NAME.Contains(txtSearch.ToLower()) || y.Phone.Contains(txtSearch) || y.Email.ToLower().Contains(txtSearch.ToLower()) || y.KodeUnik.ToLower().Contains(txtSearch.ToLower())).ToList();
            }

            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetPromoReporting(datas.ToList());
            fileName = string.Format("ReportPromoSMS_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Name", "Email", "Phone", "Child DOB", "Promo Code", "Unik Kode", "Date Event", "Registration Date", "Retailer", "Domisili", "SPGCode" });
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        private List<List<string>> GetPromoReporting(List<GetReportPromoSMS_Result> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();

                //data.Add(item.Type);
                data.Add(item.NAME);
                data.Add(item.Email);
                data.Add(item.Phone);
                if (item.Childbirtdate.HasValue)
                {
                    data.Add(item.Childbirtdate.Value.ToString("dd/MM/yyyy"));
                }
                else
                {
                    data.Add("");
                }
                data.Add(item.PromoCode);
                data.Add(item.KodeUnik);
                data.Add(item.KodeEvent);
                data.Add(item.Registerdate.Value.ToString("dd/MM/yyyy H:m:s"));
                data.Add(item.Retailer);
                data.Add(item.Domisili);
                data.Add(item.SPGCode);
                datas.Add(data);
            }
            return datas;
        }

        [BreadCrumb(Label = "Reporting Detail")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Detail(int idMem,int idProm, string status = "")
        {

            gridMvcHelper = new GridMvcHelper();
            ViewBag.idMem = idMem;
            ViewBag.idProm = idProm;
            IQueryable<PromoActivity> temp = Data.PromoActivity.GetAll().Where(x => x.MemberID == idMem && x.PromoID == idProm);
            ViewBag.titleHead = "Promo Member " + temp.First().Member.Name;
            AjaxGrid<PromoActivity> grid = this.gridMvcHelper.GetAjaxGrid(temp.OrderByDescending(x => x.CreatedDate));

            return View();

        }

        public virtual ActionResult GridGetAllPromoReporting()
        {
            gridMvcHelper = new GridMvcHelper();
            ViewBag.type=0;

            IQueryable<PromoActivityReportModel> temp = Data.PromoActivity.GetAll().GroupBy(x => new { MemberId = x.MemberID ,promoID= x.PromoID})
                       .Select(x => new PromoActivityReportModel
                       {
                           MemberID = x.FirstOrDefault().Member,
                           PromoID = x.FirstOrDefault().Promo,
                           JmlData = x.Count()
                       });

            AjaxGrid<PromoActivityReportModel> grid = this.gridMvcHelper.GetAjaxGrid(temp.OrderByDescending(x => x.MemberID.CreatedDate));
            return PartialView(grid);
        }

        public virtual ActionResult GridPromoReportingDetail(int idMem,int idProm)
        {

            gridMvcHelper = new GridMvcHelper();
            ViewBag.type = 0;
            var temp = (from a in Data.PromoActivity.GetAll()
                        join b in Data.PromoDetails.GetAll() on a.PromoDetailsID equals b.ID
                        where a.MemberID == idMem && a.PromoID==idProm
                        select new PromoActivityReportDetail { MemberM = a.Member, PromDetM = b, PromM = a.Promo, CreatedDate = a.CreatedDate.Value }
                     );
            ViewBag.titleHead = "Promo Member " + temp.First().MemberM.Name;


            AjaxGrid<PromoActivityReportDetail> grid = this.gridMvcHelper.GetAjaxGrid(temp.OrderByDescending(x => x.CreatedDate));

            return PartialView(grid);
        }
        [HttpGet]
        public virtual ActionResult GridGetAllPromoPager(string hiddenSearch, int? page)
        {

            gridMvcHelper = new GridMvcHelper();
            IQueryable<PromoActivityReportModel> allData = Data.PromoActivity.GetAll().GroupBy(x => new { MemberId = x.MemberID, promoID = x.PromoID })
                       .Select(x => new PromoActivityReportModel
                       {
                           MemberID = x.FirstOrDefault().Member,
                           PromoID=x.FirstOrDefault().Promo,
                           JmlData = x.Count()
                       });

            if (!string.IsNullOrEmpty(hiddenSearch))
            {
                allData = allData.Where(y => y.MemberID.Phone.Contains(hiddenSearch) || y.MemberID.Email.Contains(hiddenSearch) || y.MemberID.Name.Contains(hiddenSearch) || y.PromoID.PromoCode.Contains(hiddenSearch));
            }
            AjaxGrid<PromoActivityReportModel> grid = this.gridMvcHelper.GetAjaxGrid(allData.OrderByDescending(x => x.MemberID.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.PromoReporting.Views.GridGetAllPromoReporting, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public virtual ActionResult GridGetAllPromoDetailReportPager(string txtSearch, int idm, int idProm, int? page) {
            gridMvcHelper = new GridMvcHelper();
            var temp = (from a in Data.PromoActivity.GetAll()
                         join b in Data.PromoDetails.GetAll() on a.PromoDetailsID equals b.ID
                         where a.MemberID == idm && a.PromoID==idProm
                         select new PromoActivityReportDetail { MemberM = a.Member, PromDetM = b, PromM = a.Promo, CreatedDate = a.CreatedDate.Value }
                      );
            if (!string.IsNullOrEmpty(txtSearch))
            {
                temp = temp.Where(y => y.MemberM.Phone.Contains(txtSearch) || y.MemberM.Email.Contains(txtSearch) || y.MemberM.Name.Contains(txtSearch) || y.PromDetM.CodeName.Contains(txtSearch));
            }

            AjaxGrid<PromoActivityReportDetail> grid = this.gridMvcHelper.GetAjaxGrid(temp.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.PromoReporting.Views.GridPromoReportingDetail, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
    }
}