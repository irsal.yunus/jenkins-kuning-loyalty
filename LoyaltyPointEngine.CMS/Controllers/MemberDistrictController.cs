﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.DataMember;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;

using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.Model.Object.Member;
using System.Security.Cryptography;
using System.Text;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class MemberDistrictController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "District")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "District successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "District successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "District successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add District")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            DistrictModel model = new DistrictModel();
            PopulateProvinces(model.ProvinceID.ToString());
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(DistrictModel model)
        {
            ValidateAddModel(model);
            if (ModelState.IsValid)
            {
                var data = new District();
                data.DistrictCode = model.DistrictCode;
                data.DistrictName = model.DistrictName;
                data.ProvinceID = model.ProvinceID;
                var result = data.Inserted(CurrentUser.FullName);
                if (result.Success)
                {
                    return RedirectToAction(MVC.MemberDistrict.Index("Create_Success"));
                }
                else
                {
                    ModelState.AddModelError("",result.ErrorEntity);
                }

            }
            PopulateProvinces(model.ProvinceID.ToString());
            return View(model);
        }

        private void ValidateAddModel(DistrictModel model)
        {

            if (string.IsNullOrEmpty(model.DistrictCode))
            {
                ModelState.AddModelError("DistrictCode", "Code is required");
            }
            else
            {
                var data = District.GetByCode(model.DistrictCode);
                if (data != null)
                {
                    ModelState.AddModelError("DistrictCode", "Code is already registered in system");
                }
            }


            if (string.IsNullOrEmpty(model.DistrictName))
            {
                ModelState.AddModelError("DistrictCode", "Name is required");
            }
        }

        [BreadCrumb(Label = "Edit District")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Edit(int ID)
        {
            var data = District.GetByID(ID);
            DistrictModel model = new DistrictModel(data);
            PopulateProvinces(model.ProvinceID.ToString());
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Edit(DistrictModel model)
        {
            ValidateEditModel(model);
            var data = District.GetByID(model.ID);
            if(data ==null)
            {
                ModelState.AddModelError("ID", "ID not found in system");
            }
            if (ModelState.IsValid)
            {
                data.ProvinceID = model.ProvinceID;
                data.DistrictCode = model.DistrictCode;
                data.DistrictName = model.DistrictName;
                var result = data.Updated(CurrentUser.FullName);
                if (result.Success)
                    return RedirectToAction(MVC.MemberDistrict.Index("Edit_Success"));

            }
            PopulateProvinces(model.ProvinceID.ToString());
            return View(model);
        }


        private void ValidateEditModel(DistrictModel model)
        {

            if (string.IsNullOrEmpty(model.DistrictCode))
            {
                ModelState.AddModelError("DistrictCode", "Code is required");
            }
            else
            {
                var data = District.GetByCodeNotSelf(model.DistrictCode, model.ID);
                if (data != null)
                {
                    ModelState.AddModelError("DistrictCode", "Code is already registered in system");
                }
            }


            if (string.IsNullOrEmpty(model.DistrictName))
            {
                ModelState.AddModelError("DistrictCode", "Name is required");
            }
        }

        private void PopulateProvinces(string ID)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = Province.GetAll();
            foreach (var data in listdata)
            {

                items.Add(new SelectListItem { Text = data.ProvinceName, Value = data.ID.ToString(), Selected = data.ID.ToString() == ID });
            }
            ViewBag.ListProvince = items;
        }

        #region List District Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllMemberDistrict()
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = District.GetAll().OrderBy(x => x.DistrictName);
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllDistrictPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var allData = District.GetAll().OrderBy(x => x.DistrictName);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.DistrictName.Contains(txtSearch) || x.DistrictCode.Contains(txtSearch)).OrderBy(x => x.DistrictCode);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.MemberDistrict.Views.GridGetAllMemberDistrict, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}