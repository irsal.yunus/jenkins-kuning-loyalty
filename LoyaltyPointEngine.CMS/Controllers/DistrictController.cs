﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Area;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class DistrictController : BaseController
    {
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "District")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "District successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "District successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "District successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add District")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            DistrictModel model = new DistrictModel();
            GetListRegion();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(DistrictModel model)
        {
            this.IsValidDistrict(model, false);
            if (ModelState.IsValid)
            {
                District District = new District();
                District.InjectFrom(model);
                var result = District.Insert(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.District.Index("Create_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListRegion();
            return View(model);
        }

        [BreadCrumb(Label = "Edit District")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int DistrictId)
        {
            District currentDistrict = LoyaltyPointEngine.Data.District.GetById(DistrictId);
            DistrictModel model = new DistrictModel();
            model.InjectFrom(currentDistrict);
            GetListRegion();
            return View(model);
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(DistrictModel model)
        {
            this.IsValidDistrict(model, true);
            if (ModelState.IsValid)
            {
                District District = LoyaltyPointEngine.Data.District.GetById(model.ID);
                District.InjectFrom(model);
                var result = District.Update(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
                if (result.Success)
                    return RedirectToAction(MVC.District.Index("Edit_Success"));
                else
                    ModelState.AddModelError(result.ErrorEntity, result.ErrorMessage);
            }
            GetListRegion();
            return View(model);
        }

        [HttpGet]
        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            District District = District.GetById(ID);
            District.Delete(CurrentUser.FullName, Request.Url.AbsoluteUri, CurrentUser.ID);
            return RedirectToAction(MVC.District.Index("Delete_Success"));
        }

        private void IsValidDistrict(DistrictModel Model, Boolean IsEdit)
        {
            if (!string.IsNullOrEmpty(Model.Code))
            {
                Data.District predictedData = Data.District.GetByCode(Model.Code);
                if (IsEdit)
                {
                    if (predictedData != null && predictedData.ID != Model.ID)
                        ModelState.AddModelError("Error", string.Format("District Code '{0}'  has been used by other District", Model.Code));
                }
                else if (predictedData != null && predictedData.ID > 0)
                    ModelState.AddModelError("Error", string.Format("District Code  '{0}'  has been used by other District", Model.Code));
            }
        }
        private void GetListRegion()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<Region> listdata = LoyaltyPointEngine.Data.Region.GetAll();
            foreach (Region data in listdata)
            {

                items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = false });
            }
            ViewBag.ListRegion = items;
        }

        #region List District Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllDistrict()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<District> allDistrict = LoyaltyPointEngine.Data.District.GetAll().OrderBy(x => x.Name);
            AjaxGrid<District> grid = this.gridMvcHelper.GetAjaxGrid(allDistrict);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllDistrictPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<District> allData = LoyaltyPointEngine.Data.District.GetAll().OrderBy(x => x.Name);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Name.Contains(txtSearch)).OrderBy(x => x.Name);
            }
            AjaxGrid<District> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.District.Views.GridGetAllDistrict, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}