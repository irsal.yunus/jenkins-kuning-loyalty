﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.MemberLogicLayer;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Object.StoreCode;
using LoyaltyPointEngine.Model.Parameter.Member;
using LoyaltyPointEngine.Model.Parameter.StoreCode;
using MvcBreadCrumbs;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class StoreCodeController : BaseController
    {
        /*
         * Data Lengkap SPG disimpan di Database Loyalty SPG 
         * 
         */

        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Kode Toko")]

        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Kode Toko successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Kode Toko successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Kode Toko successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add Kode Toko")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            ParamStoreCodeModel model = new ParamStoreCodeModel();
            PopulateCluster(model.ClusterId);
            return View(model);
        }

        public void PopulateCluster(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = Data.Cluster.GetAll().OrderBy(x => x.Name);
            foreach (var data in listdata)
            {
                items.Add(new SelectListItem
                    {Text = data.Name, Value = data.ID.ToString(), Selected = data.ID.ToString() == id.ToString()});
            }
            ViewBag.ListCluster = items;
        }

        [BreadCrumb(Label = "Add Kode Toko")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        [HttpPost]
        public virtual ActionResult Add(ParamStoreCodeModel model)
        {
            ValidateAddStoreCode(model);
            if (ModelState.IsValid)
            {
                try
                {
                    //var Dist = Data.District.GetById(model.DistrictID.Value);
                    StoreCodeModel data = new StoreCodeModel();
                    data.Code = model.Code;
                    data.Name = model.Name;
                    data.ClusterId = model.ClusterId;
                    data.IsActive = model.IsActive;
                    StoreCodeLogic.AddStoreCode(data, Convert.ToInt32(CurrentUser.ID), CurrentUser.FullName, Request.Url.AbsoluteUri);
                    return RedirectToAction(MVC.StoreCode.Index("Create_Success"));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }

            }
            PopulateCluster(model.ClusterId);
            return View(model);
        }

        private void ValidateAddStoreCode(ParamStoreCodeModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");

            }
            else
            {
                var checkAvaibility = StoreCodeLogic.GetStoreCodeByCode(model.Code);
                if (checkAvaibility.StatusCode == "00")
                {
                    ModelState.AddModelError("Code", "Code is already registered in system");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");

            }

            if (model.ClusterId == null || model.ClusterId < 1)
            {
                ModelState.AddModelError("ClusterId", "Cluster is required");

            }
            //if (string.IsNullOrEmpty(model.LeaderName))
            //{
            //    ModelState.AddModelError("LeaderName", "LeaderName is required");

            //}

            //if (string.IsNullOrEmpty(model.OutletName))
            //{
            //    ModelState.AddModelError("OutletName", "OutletName is required");

            //}
        }

        [BreadCrumb(Label = "Edit Kode Toko")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int id)
        {
            var data = StoreCodeLogic.GetStoreCodeById(id);
            ParamStoreCodeModel model = new ParamStoreCodeModel();
            model.ID = data.Value.ID;
            model.Code = data.Value.Code;
            model.Name = data.Value.Name;
            model.ClusterId = data.Value.ClusterId;
            model.IsActive = data.Value.IsActive;
            PopulateCluster(data.Value.ClusterId);
            return View(model);
        }

        [BreadCrumb(Label = "Edit Kode Toko")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        [HttpPost]
        public virtual ActionResult Edit(ParamStoreCodeModel model)
        {
            ValidateEditStoreCode(model);
            if (ModelState.IsValid)
            {
                var data = StoreCodeLogic.GetStoreCodeById(model.ID);
                if (data != null)
                {
                    //var Dist = Data.District.GetById(model.DistrictID);
                    StoreCodeModel _model = new StoreCodeModel();
                    _model.ID = model.ID;
                    _model.ClusterId = model.ClusterId;
                    _model.Code = model.Code;
                    _model.Name = model.Name;
                    _model.IsActive = model.IsActive;
                    StoreCodeLogic.UpdateStoreCode(_model, Convert.ToInt32(CurrentUser.ID), CurrentUser.FullName, Request.Url.AbsoluteUri);
                    return RedirectToAction(MVC.StoreCode.Index("Edit_Success"));
                }
                else
                {
                    ModelState.AddModelError("", "ID not founded in system");
                }
            }
            return View(model);
        }

        [UserActionFilter(Action = ActionCode.GuidTypes.Delete)]
        public virtual ActionResult Delete(int ID)
        {
            var data = StoreCodeLogic.GetStoreCodeById(ID);
            StoreCodeModel model = new StoreCodeModel();
            model.ID = data.Value.ID;
            model.ClusterId = data.Value.ClusterId;
            model.Code = data.Value.Code;
            model.Name = data.Value.Name;
            model.IsActive = data.Value.IsActive;
            StoreCodeLogic.DeleteStoreCode(model, Convert.ToInt32(CurrentUser.ID), CurrentUser.FullName, Request.Url.AbsoluteUri);
            return RedirectToAction(MVC.StoreCode.Index("Delete_Success"));
            return null;
        }

        private void ValidateEditStoreCode(ParamStoreCodeModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");

            }
            else
            {
                var checkAvaibility = StoreCodeLogic.GetStoreCodeByCodeNotSelfId(model.ID, model.Code);
                if (checkAvaibility.StatusCode == "00")
                {
                    ModelState.AddModelError("Code", "Code is already registered in system");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");

            }

            //if (string.IsNullOrEmpty(model.LeaderName))
            //{
            //    ModelState.AddModelError("LeaderName", "LeaderName is required");

            //}

            //if (string.IsNullOrEmpty(model.OutletName))
            //{
            //    ModelState.AddModelError("OutletName", "OutletName is required");

            //}
        }

        #region List Store Code Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllStoreCode()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<StoreCode> allStoreCode = StoreCode.GetAll().OrderBy(x=>x.Code);
            AjaxGrid<StoreCode> grid = this.gridMvcHelper.GetAjaxGrid(allStoreCode);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllStorePager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<StoreCode> allData = StoreCode.GetAll().OrderBy(x => x.Code);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Code.Contains(txtSearch) || x.Name.Contains(txtSearch))
                    .OrderBy(x => x.Code);
            }
            AjaxGrid<StoreCode> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.StoreCode.Views.GridGetAllStoreCode, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}