﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.Model.Parameter.Redeem;
using LoyaltyPointEngine.PointLogicLayer.Point;
using System.Globalization;
using LoyaltyPointEngine.Model.Object.Redeem;
using System.IO;
using System.Data.Objects;
using System.Text.RegularExpressions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class RedeemReportingController : BaseController
    {
        // GET: Redeem
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Redeem Reporting")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Redeem successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Redeem successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Redeem successfully deleted";
                    break;
                case "Void_Success":
                    ViewBag.Success = "Redeem successfully voided";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }



        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Redeem Reporting")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string tglAwal, string tglAkhir)
        {

            var datas = GetAllRedeemModel();
            if (!string.IsNullOrEmpty(tglAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(tglAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }

            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetRedeemReporting(datas.ToList());
            fileName = string.Format("RedeemReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "MemberName", "MemberPhone", "TransactionCode", "TransactionDate",
                "Point", "CreatedDate", "ElinaCode", "CatalogueName", "Qty", "Channel", "ReceiverName", "ReceiverPhone", "ReceiverAddress"});
            excelStream.Position = 0;

            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }


        private List<List<string>> GetRedeemReporting(List<RedeemCMSViewModel> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                string strReceiptAddress = item.ReceiverAddress != null ? Regex.Replace(item.ReceiverAddress, "<.*?>", " ") : string.Empty;
                data.Add(item.MemberName);
                data.Add(item.MemberPhone);
                data.Add(item.TransactionCode);
                data.Add(item.TransactionDateDate.ToString("dd/MM/yyyy"));
                data.Add(item.Point.ToString());
                data.Add(item.CreatedDate.ToString("dd/MM/yyyy"));
                data.Add(item.ElinaCode);
                data.Add(item.CatalogueName);
                data.Add(item.Quantity.ToString());
                data.Add(item.Channel);
                data.Add(item.ReceiverName);
                data.Add(item.ReceiverPhone);
                data.Add(strReceiptAddress);

                datas.Add(data);
            }
            return datas;
        }

        #region List Redeem Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllRedeem()
        {
            gridMvcHelper = new GridMvcHelper();
            var allRedeem = GetAllRedeemModel();
            AjaxGrid<RedeemCMSViewModel> grid = this.gridMvcHelper.GetAjaxGrid(allRedeem.OrderByDescending(x => x.CreatedDate));
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllRedeemPager(string hiddenAwal, string hiddenAkhir, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IQueryable<RedeemCMSViewModel> datas = GetAllRedeemModel();
            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }
            ViewBag.TotalDataRow = datas.Count();
            AjaxGrid<RedeemCMSViewModel> grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.CreatedDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.RedeemReporting.Views.GridGetAllRedeem, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private IQueryable<RedeemCMSViewModel> GetAllRedeemModel()
        {
            var data = from x in LoyaltyPointEngine.Data.Redeem.GetAll()
                       join y in LoyaltyPointEngine.Data.viwRedeemReport.GetAll() on x.ID equals y.ID
                       join member in LoyaltyPointEngine.Data.Member.GetAll() on x.MemberID equals member.ID
                       join z in LoyaltyPointEngine.Data.viwReportReceipt.GetAll() on member.ID equals z.ID
                       select new RedeemCMSViewModel
                       {
                           ID = x.ID,
                           TransactionCode = x.TransactionCode,
                           TransactionDateDate = x.TransactionDate,
                           Point = y.PointDetail,
                           CreatedDate = x.CreatedDate,
                           MemberName = member.Name,
                           MemberPhone = member.Phone,
                           ElinaCode = z.Code,
                           CatalogueName = y.CatalogueName,
                           Quantity = y.Quantity,
                           Channel = y.Channel,
                           ReceiverName = y.ReceiverName,
                           ReceiverPhone = y.ReceiverPhone,
                           ReceiverAddress = y.ReceiverAddress,
                           IsVoid = x.IsVoid
                       };

            ViewBag.TotalDataRow = data.Count();
            return data;
        }
        #endregion
    }
}