﻿using Grid.Mvc.Ajax.GridExtensions;
using LoyaltyPointEngine.CMS.Helper;
using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object.CMSUser;
using LoyaltyPointEngine.PointLogicLayer.CmsUser;
using MvcBreadCrumbs;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
//Create This File by Ucup
namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class LoginLogController : BaseController
    {
        // GET: LoginLog
        // GET: LoginLog
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "LoginLog")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            LoginLog model = new LoginLog();
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Product successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Product successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Product successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            GetListUsername();
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Report LoginLog")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string userName, string tglAwal, string tglAkhir)
        {
            var datas = CmsUserLogic.GetCmsUserActivityHistory().OrderBy(x => x.ActivityDate).AsQueryable();

            if (!string.IsNullOrEmpty(userName))
            {
                datas = datas.Where(x => x.UserName == userName);
            }
            if (!string.IsNullOrEmpty(tglAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.ActivityDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(tglAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(tglAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.ActivityDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }
            var export = datas.ToList();
            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetExportLoginLog(export);
            fileName = string.Format("ReportLoginLog_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "UserName", "Activity",
                "ActivitydDate", "RetailerName", "CheckInImage" });
            excelStream.Position = 0;

            GetListUsername();
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        [ChildActionOnly]
        public virtual ActionResult GridGetAllLoginLog()
        {
            gridMvcHelper = new GridMvcHelper();

            //IOrderedQueryable<LoginLog> allLoginLog = LoginLog.GetAll().OrderBy(x => x.ID);
            IOrderedQueryable<ActivityHistory> allLoginLog  = CmsUserLogic.GetCmsUserActivityHistory().OrderBy(x => x.ActivityDate);
            var grid = this.gridMvcHelper.GetAjaxGrid(allLoginLog);
            return PartialView(grid);
        }

        private void GetListUsername()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IQueryable<LoginLog> listdata = LoyaltyPointEngine.Data.LoginLog.GetAll().GroupBy(x => x.MemberID).Select(x => x.FirstOrDefault());

            foreach (LoginLog data in listdata)
            {

                items.Add(new SelectListItem { Text = data.UserName, Value = data.UserName, Selected = false });
            }
            ViewBag.ListUsername = items;
        }

        [HttpGet]
        public virtual ActionResult GridGetAllLoginLogPager(string hiddenUsername, string hiddenAwal, string hiddenAkhir, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            //IQueryable<LoginLog> datas = LoginLog.GetAll();
            IQueryable<ActivityHistory> datas = CmsUserLogic.GetCmsUserActivityHistory();
            if (!string.IsNullOrEmpty(hiddenUsername))
            {
                datas = datas.Where(x => x.UserName == hiddenUsername);
            }
            if (!string.IsNullOrEmpty(hiddenAwal))
            {
                DateTime firstDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAwal, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out firstDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.ActivityDate) >= EntityFunctions.TruncateTime(firstDT));
                }
            }

            if (!string.IsNullOrEmpty(hiddenAkhir))
            {
                DateTime endDT = DateTime.MinValue;
                if (DateTime.TryParseExact(hiddenAkhir, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDT))
                {
                    datas = datas.Where(x => EntityFunctions.TruncateTime(x.ActivityDate) <= EntityFunctions.TruncateTime(endDT));
                }
            }

            AjaxGrid<ActivityHistory> grid = this.gridMvcHelper.GetAjaxGrid(datas.OrderByDescending(x => x.ActivityDate), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.LoginLog.Views.GridGetAllLoginLog, this);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private List<List<string>> GetExportLoginLog(List<ActivityHistory> param)
        {
            var assetMainUrl = Data.Settings.GetValueByKey(SiteSetting.ASSET_URL);
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                data.Add(item.UserName);
                data.Add(item.Activity);
                data.Add(item.ActivityDate.ToString("dd/MM/yyyy HH:mm"));
                data.Add(item.RetailerName);
                data.Add(string.IsNullOrEmpty(item.CheckInImage) ? "" : assetMainUrl + "CmsUserCheckIn/" + item.CheckInImage);
                datas.Add(data);
            }
            return datas;
        }
    }
}