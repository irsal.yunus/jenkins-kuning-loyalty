﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Data;
using System.Security.Cryptography;

namespace LoyaltyPointCMS.Controllers
{
    public partial class AccountController : Controller
    {


        [HttpGet]
        public virtual ActionResult Login()
        {
            ViewBag.Title = "Login";
            if (CookieHelper.CookieExist("Administrator_Username") == true)
            {
                string userLoggedInString = CookieHelper.Get("Administrator_Username", true);
                CMSUser userLoggedIn = CMSUser.GetUserActiveByUsername(userLoggedInString);
                if (userLoggedIn == null)
                {
                    return RedirectToAction(MVC.Account.Logout());
                }
                return Redirect(Url.Content("~"));
            }
            else
                return View();

        }


        [HttpPost]
        public virtual ActionResult Login(UserLoginModel model)
        {
            if (ModelState.IsValid)
            {
                string _UserName = model.Username;
                CMSUser predictedUser = CMSUser.GetByUsername(model.Username);
                if (predictedUser == null)
                {
                    ModelState.AddModelError("", "Invalid UserName or password");
                    return View();
                }
                string _Password = GetEncriptedPassword(model.Password, predictedUser.UserPrivateSecret);
                CMSUser _User = CMSUser.GetUserActiveByUsernameAndPassword(_UserName, _Password);

                if (_User == null)
                {
                    ModelState.AddModelError("", "Invalid Email or password");
                    return View();
                }
                else
                {
                    // CookieHelper.RemoveAll();
                    //if (_User.IsActive)
                    //{
                        CookieHelper.Add("Administrator_Username", _User.Username, false, true);
                        return RedirectToAction(MVC.Home.Index());
                    //}
                    //else {
                    //    ModelState.AddModelError("", "This Account is InActive, Please Contact Your Supervisor");
                    //    return View();
                    //}
                }
            }


            return View();
        }

        public virtual ActionResult Logout()
        {
            //CMSUser _User = null;
            //if (CookieHelper.CookieExist("Administrator_Username") == true)
            //{
            //    string userLoggedInString = CookieHelper.Get("Administrator_Username", true);
            //    _User = CMSUser.GetByUsername(userLoggedInString);
            //}

            CookieHelper.Remove("Administrator_Username");
            //CookieHelper.RemoveAll();
            LoyaltyPointEngine.CMS.Controllers.BaseController baseC = new LoyaltyPointEngine.CMS.Controllers.BaseController();
            baseC.SetRemoveCurrentNavigationCache();

            return RedirectToAction(MVC.Account.Login());
        }


        private string GetEncriptedPassword(string PlainPassword, Guid Salts)
        {
            string raw = PlainPassword + Salts;
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(PlainPassword + Salts.ToString());
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);
            return BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();
        }
    }
}
