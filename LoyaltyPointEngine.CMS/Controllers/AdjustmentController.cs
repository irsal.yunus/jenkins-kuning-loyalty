﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.Data;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Product;
using LoyaltyPointEngine.Model.Object.Adjustment;
using System.Globalization;
using System.Transactions;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class AdjustmentController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;
        private DateTime ExpiredDate = new DateTime(DateTime.Today.AddYears(1).Year, 12, 31); // last day of next year

        [BreadCrumb(Clear = true, Label = "Adjustment")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Adjustment successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Adjustment successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Adjustment successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [BreadCrumb(Label = "Detail Adjustment")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(Guid id)
        {
            var data = Adjustment.GetById(id);
            AdjustmentCMSModel model = new AdjustmentCMSModel(data);
            return View(model);
        }

        [BreadCrumb(Label = "Add Adjustment")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            return View(new AdjustmentCMSModel
            {
                ExpiredDate = ExpiredDate
            });
        }

        [HttpPost]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add(AdjustmentCMSModel model)
        {
            ValidateAdd(model);
            if (ModelState.IsValid)
            {
                var currUser = CurrentUser;
                string by = currUser.FullName;
                long userCMSID = currUser.ID;
                int poolID = Data.Pool.GetAll().Where(x => x.IsMainProcess).FirstOrDefault().ID;

                bool IsSuccess = false;
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    Adjustment data = new Adjustment();
                    data.ID = Guid.NewGuid();
                    data.Code = GeneratedCode(by);
                    data.ExpiredDate = ExpiredDate;
                    data.TransactionDate = DateTime.Now;
                    data.Point = model.Point;
                    data.Reason = model.Reason;
                    data.MemberID = model.MemberID;
                    data.PoolID = poolID;
                    var ressEF = data.Insert(by, userCMSID);
                    if (ressEF.Success)
                    {
                        if (data.Point >= 0)
                        {
                            Data.Collect collect = new Data.Collect();
                            collect.ID = Guid.NewGuid();
                            collect.MemberID = model.MemberID;
                            // collect.ActionID = model.ActionID;
                            collect.Quantity = 1;
                            collect.QuantityApproved = 1;
                            collect.Points = model.Point;
                            collect.TotalPoints = model.Point;//model.TotalPoints;
                            collect.Remarks = model.Reason;
                            collect.TransactionDate = data.TransactionDate;
                            collect.AdjustmentReferenceID = data.ID;
                            collect.ExpiryDate = data.ExpiredDate;
                            collect.PoolID = poolID;
                            collect.IsApproved = true;
                            collect.IsActive = true;
                            collect.IsUsed = false;
                            var res = collect.Insert(by, Request.Url.AbsoluteUri, userCMSID, 0);
                            if (res.Success)
                            {
                                LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Credit(data.Point, model.Reason, model.MemberID, collect.PoolID, by, collect.ID, 0);

                                IsSuccess = true;
                                transScope.Complete();
                            }
                        }
                        else
                        {
                            //Logic pemotongan point dari collect
                            var collects = Data.Collect.GetAllActiveByMember(model.MemberID).OrderBy(x => x.ExpiryDate).ToList();
                            var reductionPoint = Math.Abs(model.Point);
                            int timeStamp = 0;
                            foreach (var collect in collects)
                            {
                                if (reductionPoint == 0)
                                {
                                    //finish
                                    break;
                                }
                                else
                                {
                                    if (reductionPoint >= collect.TotalPoints)
                                    {
                                        ///1. collect di habisin semua
                                        ///. respoint dikurangin
                                        collect.IsUsed = true;
                                        collect.AdjustmentReferenceID = data.ID;
                                        collect.Updated(by);
                                        reductionPoint = reductionPoint - collect.TotalPoints;
                                    }
                                    else
                                    {
                                        //2. di splits
                                        //collect.Remarks = "inactive due to be separated in the process redeem";
                                        collect.IsActive = false;
                                        collect.Updated(by);
                                        //collect point di pecah
                                        //collect used
                                        var collectUsed = SplitCollectRedeem(reductionPoint, data.ID, collect, true, by, 0);
                                        var restCollect = SplitCollectRedeem(collect.TotalPoints - collectUsed.Points, null, collect, false, by, 1);
                                        reductionPoint = 0;
                                    }
                                }
                            }

                            //if (reductionPoint > 0)
                            //{
                            //    var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(model.MemberID);
                            //    if (lastBalanceLog != null && lastBalanceLog.Balance > 0)
                            //    {
                            //        if ((lastBalanceLog.Balance - model.Point) >= 0)
                            //        {
                            //            reductionPoint = 0;
                            //        }
                            //    }
                            //}
                            var lastBalanceLog = BalanceLog.GetLastBalanceByMemberId(model.MemberID);
                            int newBalanceLog = model.Point + lastBalanceLog.Balance;
                            LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.Debit(model.Point, model.Reason, model.MemberID, data.PoolID, by, data.ID, newBalanceLog, 0);

                            IsSuccess = true;
                            transScope.Complete();
                        }

                        transScope.Dispose();
                        if (IsSuccess)
                        {
                            LoyaltyPointEngine.PointLogicLayer.Point.BalanceLogLogic.UpdateMemberPoint(model.MemberID, by);
                        }
                        return RedirectToAction(MVC.Adjustment.Index("Create_Success"));
                    }
                    else
                    {
                        ModelState.AddModelError("", ressEF.ErrorMessage);
                    }
                    transScope.Dispose();
                }
            }
            return View(model);
        }

        private static Data.Collect SplitCollectRedeem(int point, Guid? AdjID, Data.Collect parent, bool IsUsed, string by, int Timestamp)
        {
            var splitCollect = new Data.Collect();
            splitCollect.ID = Guid.NewGuid();
            splitCollect.MemberID = parent.MemberID;
            splitCollect.Points = point;

            splitCollect.IsUsed = true;
            splitCollect.ParentID = parent.ID;
            splitCollect.TransactionDate = DateTime.Now.AddSeconds(Timestamp);
            splitCollect.ExpiryDate = parent.ExpiryDate;
            splitCollect.PoolID = parent.PoolID;
            splitCollect.Quantity = parent.Quantity;
            splitCollect.QuantityApproved = parent.QuantityApproved;
            splitCollect.TotalPoints = point;
            splitCollect.Remarks = "Split Adjustment";
            splitCollect.IsApproved = true;
            splitCollect.IsActive = true;

            splitCollect.IsUsed = IsUsed;

            if (IsUsed)
            {
                splitCollect.AdjustmentReferenceID = AdjID;
            }

            splitCollect.Inserted(by, Timestamp);

            return splitCollect;
        }

        private void ValidateAdd(AdjustmentCMSModel model)
                {
            if (model.Point == 0)
            {
                ModelState.AddModelError("Point", "Point must more or less than zero");
            }

            if (string.IsNullOrEmpty(model.Reason))
            {
                ModelState.AddModelError("Reason", "Reason is required");
            }

            if (model.MemberID <= 0)
            {
                ModelState.AddModelError("MemberID", "Member is required");
            }

            if (model.Point < 0)
            {
                var MemberPoint = Data.BalanceLog.GetByMemberId(model.MemberID).OrderByDescending(x => x.CreatedDate).Take(1).Select(x => x.Balance).FirstOrDefault();
                var balance = model.Point + MemberPoint;
                if (balance < 0)
                {
                    ModelState.AddModelError("Point", "Deduction points must not be greater than balance");
                }
            }
        }

        private string GeneratedCode(string by)
        {
            return Data.Number.Generated(Number.Type.ADJ, by);
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllAdjustment()
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = GetAllAdjustment();
            var grid = this.gridMvcHelper.GetAjaxGrid(datas);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllAdjustmentPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            var datas = GetAllAdjustment();
            if (!string.IsNullOrEmpty(txtSearch))
            {
                datas = datas
                    .Where(x => x.MemberName.Contains(txtSearch) || x.MemberEmail.Contains(txtSearch) || x.Code.Contains(txtSearch) || x.Reason.Contains(txtSearch)).OrderByDescending(x => x.CreatedDate)
                    .OrderByDescending(x => x.CreatedDate);
            }
            var grid = this.gridMvcHelper.GetAjaxGrid(datas, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.Adjustment.Views.GridGetAllAdjustment, this);
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        private IOrderedQueryable<AdjustmentCMSModel> GetAllAdjustment()
        {
            var data = from a in LoyaltyPointEngine.Data.Adjustment.GetAll()
                       join b in Data.Member.GetAll() on a.MemberID equals b.ID
                       into memberTemp
                       from b_temp in memberTemp.DefaultIfEmpty()
                       select new AdjustmentCMSModel
                       {
                           ID = a.ID,
                           Code = a.Code,
                           ExpiredDate = a.ExpiredDate,
                           TransactionDateDT = a.TransactionDate,
                           Point = a.Point,
                           CreatedDate = a.CreatedDate,
                           MemberName = b_temp.Name,
                           Reason = a.Reason,
                           MemberEmail = b_temp.Email
                       };
            return data.OrderByDescending(x => x.CreatedDate);
        }
        #endregion
    }
}