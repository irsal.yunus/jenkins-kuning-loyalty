﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoyaltyPointEngine.DataMember;
using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Model.Object.Action;
using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using LoyaltyPointEngine.PointLogicLayer.Action;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Object.Receipt;
using System.Globalization;
using System.Data.Objects;
using System.IO;

namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class UnikKonsumenReportingController : BaseController
    {
        // GET: Action
        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "Reporting - Unik Konsumen")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {
            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "Action successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "Action successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "Action successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [BreadCrumb(Clear = true, Label = "Reporting - Unik Konsumen")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string tglAwal, string tglAkhir)
        {
            DateTime? stDate = null;
            DateTime? enDate = null;
            if (!string.IsNullOrEmpty(tglAwal))
                stDate = DateTime.ParseExact(tglAwal, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(tglAkhir))
                enDate = DateTime.ParseExact(tglAkhir, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            var data = DataRepositoryFactory.CurrentRepository.GetListForUnikKonsumenReport(stDate, enDate).ToList();
            var datas = (from a in data
                        select new MemberGridModel
                        {
                             ID = Convert.ToInt32(a.MemberID),
                            Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            Email = a.Email,
                            Phone = a.Phone,
                             IsBlacklist = Convert.ToBoolean(a.IsBacklist),
                            Point = a.MemberPoint,
                            //CreatedDate = a.CreatedDate,
                             ChildBOD = a.ChildBirthDate,
                            //Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                             SPGCode = a.SpgCode == null ? string.Empty : a.SpgCode,
                             Channel = a.ChannelName,
                             SPGName = a.SpgName,
                             Outlet = a.OutletName
                         });

            String fileName = String.Empty;
            List<List<string>> report = new List<List<string>>();
            MemoryStream excelStream = new MemoryStream();
            report = GetUnikMemberReporting(datas.ToList());
            fileName = string.Format("UnikKonsumenRegisterReport_{0}.xlsx", DateTime.Now.ToString("ddMMyyyy"));
            excelStream = ExcelHelper.ExportToExcelNewVersion(report, new List<string> { "Gender", "FirstName", "LastName", "Email", "Phone", "Child Name", "Child DOB", "IsBlacklist", "Point", "Channel" });
            excelStream.Position = 0;
            return File(excelStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        private List<List<string>> GetUnikMemberReporting(List<MemberGridModel> param)
        {
            List<List<string>> datas = new List<List<string>>();
            foreach (var item in param)
            {
                List<List<string>> emptyTempRows = new List<List<string>>();
                List<string> data = new List<string>();
                string x = item.ChildBOD.HasValue ? item.ChildBOD.Value.ToString("dd/MM/yyyy") : string.Empty;
                //data.Add(item.Type);
                data.Add(item.Gender);
                data.Add(item.FirstName);
                data.Add(item.LastName);
                data.Add(item.Email);
                data.Add(item.Phone);
                data.Add(item.ChildName);
                data.Add(x);
                data.Add(item.IsBlacklist ? "Ya" : "Tidak");
                data.Add(item.Point.ToString());
                //data.Add(item.SPGCode);
                //data.Add(item.SPGName);
                //data.Add(item.Outlet);
                //data.Add(item.CreatedDate.HasValue ? item.CreatedDate.Value.ToString("dd/MM/yyyy") : "");
                data.Add(item.Channel);
                datas.Add(data);
            }
            return datas;
        }

        #region List Action Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAll()
        {
            gridMvcHelper = new GridMvcHelper();
            DateTime? stDate = null;
            DateTime? enDate = null;
            var data = DataRepositoryFactory.CurrentRepository.GetListForUnikKonsumenReport(stDate, enDate).ToList();
            var datas = (from a in data
                        select new MemberGridModel
                        {
                            ID = Convert.ToInt32(a.MemberID),
                            Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            Email = a.Email,
                            Phone = a.Phone,
                            IsBlacklist = Convert.ToBoolean(a.IsBacklist),
                            Point = a.MemberPoint,
                            //CreatedDate = a.CreatedDate,
                            ChildBOD = a.ChildBirthDate,
                            //Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                            SPGCode = a.SpgCode == null ? string.Empty : a.SpgCode,
                            Channel = a.ChannelName,
                            SPGName = a.SpgName,
                            Outlet = a.OutletName
                        });
            ViewBag.TotalDataRow = datas.AsEnumerable().Count();
            var grid = this.gridMvcHelper.GetAjaxGrid(datas.AsQueryable().OrderBy(x => x.FirstName));
            return PartialView(grid);
        }


        [HttpGet]
        public virtual ActionResult GridGetAllPager(string hiddenAwal, string hiddenAkhir, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            DateTime? stDate = null;
            DateTime? enDate = null;
            if (!string.IsNullOrEmpty(hiddenAwal))
                stDate = DateTime.ParseExact(hiddenAwal, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(hiddenAkhir))
                enDate = DateTime.ParseExact(hiddenAkhir, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            var data = DataRepositoryFactory.CurrentRepository.GetListForUnikKonsumenReport(stDate, enDate).ToList();
            var datas = (from a in data
                        select new MemberGridModel
                        {
                             ID = Convert.ToInt32(a.MemberID),
                            Gender = a.Gender.ToLower() == "m" ? "Male" : "Female",
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            Email = a.Email,
                            Phone = a.Phone,
                             IsBlacklist = Convert.ToBoolean(a.IsBacklist),
                            Point = a.MemberPoint,
                            //CreatedDate = a.CreatedDate,
                             ChildBOD = a.ChildBirthDate,
                             //Type = !string.IsNullOrEmpty(a.SMSID) ? "Sms" : "Web",
                             SPGCode = a.SpgCode == null ? string.Empty : a.SpgCode,
                             Channel = a.ChannelName,
                             SPGName = a.SpgName,
                             Outlet = a.OutletName
                         }).AsQueryable();
            var filteredDatas = datas;

            ViewBag.TotalDataRow = filteredDatas.AsEnumerable().Count();
            var grid = this.gridMvcHelper.GetAjaxGrid(filteredDatas.AsQueryable().OrderBy(x => x.FirstName), page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.UnikKonsumenReporting.Views.GridGetAll, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}