﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Grid.Mvc.Ajax.GridExtensions;
using MvcBreadCrumbs;
using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Common.Helper;

using LoyaltyPointEngine.CMS.Helper;
using Omu.ValueInjecter;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using LoyaltyPointEngine.Common;

using System.Security.Cryptography;
using System.Transactions;
using System.Globalization;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Parameter.Member;
using LoyaltyPointEngine.Model.Object.Member;


namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class SalesPersonController : BaseController
    {
        /*
         * Data Lengkap SPG disimpan di Database Loyalty SPG 
         * 
         */

        private IGridMvcHelper gridMvcHelper;

        [BreadCrumb(Clear = true, Label = "SalesPerson")]
        [UserActionFilter(Action = ActionCode.GuidTypes.View)]
        public virtual ActionResult Index(string status)
        {

            switch (status)
            {
                case "Create_Success":
                    ViewBag.Success = "SPG successfully created";
                    break;
                case "Edit_Success":
                    ViewBag.Success = "SPG successfully updated";
                    break;
                case "Delete_Success":
                    ViewBag.Success = "SPG successfully deleted";
                    break;
                default:
                    ViewBag.Success = status;
                    break;
            }

            return View();
        }

        [BreadCrumb(Label = "Add SPG")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        public virtual ActionResult Add()
        {
            AddSalesPersonParameterModel model = new AddSalesPersonParameterModel();
            PopulateDistrict(model.DistrictID);
            return View(model);
        }

        //public void PopulateDistrict()
        //{
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    var listdata = Data.District.GetAll().OrderBy(x => x.Name);
        //    foreach (var data in listdata)
        //    {
        //        items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString() });
        //    }
        //    ViewBag.ListDistrict = items;
        //}

        public void PopulateDistrict(int? id)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var listdata = Data.District.GetAll().OrderBy(x => x.Name);
            foreach (var data in listdata)
            {
                if (id.HasValue)
                    items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString(), Selected = data.ID.ToString() == id.ToString() });
                else
                    items.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString() });
            }
            ViewBag.ListDistrict = items;
        }

        [BreadCrumb(Label = "Add SPG")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Insert)]
        [HttpPost]
        public virtual ActionResult Add(AddSalesPersonParameterModel model)
        {
            ValidateAddSPG(model);
            if (ModelState.IsValid)
            {
                try
                {
                    var Dist = Data.District.GetById(model.DistrictID.Value);
                    SPG data = new SPG();
                    data.Code = model.Code;
                    data.Name = model.Name;
                    data.OutletName = model.OutletName;
                    data.LeaderName = model.LeaderName;
                    data.District = model.DistrictID.ToString();
                    data.Region = Dist.RegionID.ToString();
                    data.Inserted(CurrentUser.FullName);
                    return RedirectToAction(MVC.SalesPerson.Index("Create_Success"));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }

            }
            PopulateDistrict(model.DistrictID);
            return View(model);
        }

        private void ValidateAddSPG(AddSalesPersonParameterModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");

            }
            else
            {
                var checkAvaibility = SPG.GetByCode(model.Code);
                if (checkAvaibility != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered in system");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");

            }

            if (model.DistrictID == null || model.DistrictID < 1)
            {
                ModelState.AddModelError("DistrictID", "District is required");

            }
            //if (string.IsNullOrEmpty(model.LeaderName))
            //{
            //    ModelState.AddModelError("LeaderName", "LeaderName is required");

            //}

            //if (string.IsNullOrEmpty(model.OutletName))
            //{
            //    ModelState.AddModelError("OutletName", "OutletName is required");

            //}
        }

        [BreadCrumb(Label = "Edit SPG")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        public virtual ActionResult Edit(int id)
        {
             var data = SPG.GetById(id);
            SalesPersonModel model = new SalesPersonModel(data);
            PopulateDistrict(model.DistrictID);
            return View(model);
        }

        [BreadCrumb(Label = "Edit SPG")]
        [UserActionFilter(Action = ActionCode.GuidTypes.Update)]
        [HttpPost]
        public virtual ActionResult Edit(SalesPersonModel model)
        {
            ValidateEditSPG(model);
            if (ModelState.IsValid)
            {
                var data = SPG.GetById(model.ID);
                if (data != null)
                {
                    var Dist = Data.District.GetById(model.DistrictID);
                    data.Code = model.Code;
                    data.Name = model.Name;
                    data.OutletName = model.OutletName;
                    data.LeaderName = model.LeaderName;
                    data.IsActive = model.IsActive;
                    data.District = model.DistrictID.ToString();
                    data.Region = Dist.RegionID.ToString();
                    data.Updated(CurrentUser.FullName);
                    return RedirectToAction(MVC.SalesPerson.Index("Edit_Success"));
                }
                else
                {
                    ModelState.AddModelError("", "ID not founded in system");
                }
            }
            return View(model);
        }

        private void ValidateEditSPG(SalesPersonModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
            {
                ModelState.AddModelError("Code", "Code is required");

            }
            else
            {
                var checkAvaibility = SPG.GetByCodeNotSelf(model.Code, model.ID);
                if (checkAvaibility != null)
                {
                    ModelState.AddModelError("Code", "Code is already registered in system");
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Name", "Name is required");

            }

            //if (string.IsNullOrEmpty(model.LeaderName))
            //{
            //    ModelState.AddModelError("LeaderName", "LeaderName is required");

            //}

            //if (string.IsNullOrEmpty(model.OutletName))
            //{
            //    ModelState.AddModelError("OutletName", "OutletName is required");

            //}
        }

        #region List SPG Grid
        [ChildActionOnly]
        public virtual ActionResult GridGetAllSPG()
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<SPG> allSPG = SPG.GetAll().OrderBy(x => x.Code);
            AjaxGrid<SPG> grid = this.gridMvcHelper.GetAjaxGrid(allSPG);
            return PartialView(grid);
        }

        [HttpGet]
        public virtual ActionResult GridGetAllSPGPager(string txtSearch, int? page)
        {
            gridMvcHelper = new GridMvcHelper();
            IOrderedQueryable<SPG> allData = DataMember.SPG.GetAll().OrderBy(x => x.Code);
            if (!string.IsNullOrEmpty(txtSearch))
            {
                allData = allData.Where(x => x.Code.Contains(txtSearch)
                    || x.Name.Contains(txtSearch)
                    || x.OutletName.Contains(txtSearch)
                    )
                    .OrderBy(x => x.Code);
            }
            AjaxGrid<SPG> grid = this.gridMvcHelper.GetAjaxGrid(allData, page);
            object jsonData = this.gridMvcHelper.GetGridJsonData(grid, MVC.SalesPerson.Views.GridGetAllSPG, this);

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}