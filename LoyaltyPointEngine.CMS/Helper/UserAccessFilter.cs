﻿using LoyaltyPointEngine.CMS.Models;
using LoyaltyPointEngine.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace LoyaltyPointEngine.CMS.Helper
{
    public class UserAccessFilter : ActionFilterAttribute 
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CMSUserInformation _CMSInformation = new CMSUserInformation();
            string controllerName = filterContext.Controller.ControllerContext.RouteData.Values["controller"].ToString();
            string actionName = filterContext.Controller.ControllerContext.RouteData.Values["action"].ToString();
            List<string> allowedController = new List<string>();
            List<UserAccessModel> listControllerAction = _CMSInformation.SetCurrentAllowedControllerActionCache();
            allowedController = listControllerAction.Select(x => x.Controler).Distinct().ToList();
            if (!allowedController.Contains(controllerName))
                filterContext.Result = new RedirectResult("/Error");
        }
        
        
    }
    public class UserActionFilter : ActionFilterAttribute
    {
        public ActionCode.GuidTypes Action { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CMSUserInformation _CMSInformation = new CMSUserInformation();
            ActionCode ac = new ActionCode();
            string controllerName = filterContext.Controller.ControllerContext.RouteData.Values["controller"].ToString();
            List<UserAccessModel> allowedNavPriv = new List<UserAccessModel>();
            allowedNavPriv = _CMSInformation.SetCurrentAllowedControllerActionCache(); ;
            if(allowedNavPriv.Count() > 0)
            {
                Guid actionGuid = ac.GetGuid(Action);
                UserAccessModel currentAction = allowedNavPriv
                    .Where(x => x.Controler == controllerName &&
                    x.Action == actionGuid).FirstOrDefault();
                if(currentAction == null)
                    filterContext.Result = new RedirectResult("/Error");
            }
        }

    }

    /* 
     * Action Code Harus Sesuai Dengan Tabel CMSPrivilege
     * Jika Menambahkan Privilege Baru di Database 
     * maka harus ditambahkan di bagian GuidTypes juga
     * namingnya harus sama
     */
    public class ActionCode
    {
        private Dictionary<GuidTypes, Guid> mGuidMap = new Dictionary<GuidTypes, Guid>();
        
        public enum GuidTypes: int
        {
            View,
            Insert,
            Update,
            Delete,
            ResetPassword,
            Void
        }

        IEnumerable<GuidTypes> items = CMSPrivilege.GetAll().Select(x => x.Name).ToArray()
            .Select(a => (GuidTypes)Enum.Parse(typeof(GuidTypes), a));

        public ActionCode()
        {
            foreach(CMSPrivilege priv in CMSPrivilege.GetAll())
            {
                GuidTypes curType = items.Where(x => x.ToString() == priv.Name).FirstOrDefault();
                mGuidMap.Add(curType, priv.Code);
            }
        }

        public Guid GetGuid(GuidTypes guidType)
        {
            if (mGuidMap.ContainsKey(guidType))
            {
                return mGuidMap[guidType];
            }
            return Guid.Empty;
        }
    }
}