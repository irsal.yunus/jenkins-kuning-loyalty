// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace LoyaltyPointEngine.CMS.Controllers
{
    public partial class ResendSMSController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ResendSMSController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected ResendSMSController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Index()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult ResendView()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ResendView);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult GridGetAllResendSMSPager()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllResendSMSPager);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Resend()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Resend);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ResendSMSController Actions { get { return MVC.ResendSMS; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "ResendSMS";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "ResendSMS";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string ResendView = "ResendView";
            public readonly string GridGetAllResendSMS = "GridGetAllResendSMS";
            public readonly string GridGetAllResendSMSPager = "GridGetAllResendSMSPager";
            public readonly string Resend = "Resend";
            public readonly string Upload = "Upload";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string ResendView = "ResendView";
            public const string GridGetAllResendSMS = "GridGetAllResendSMS";
            public const string GridGetAllResendSMSPager = "GridGetAllResendSMSPager";
            public const string Resend = "Resend";
            public const string Upload = "Upload";
        }


        static readonly ActionParamsClass_Index s_params_Index = new ActionParamsClass_Index();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Index IndexParams { get { return s_params_Index; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Index
        {
            public readonly string status = "status";
        }
        static readonly ActionParamsClass_ResendView s_params_ResendView = new ActionParamsClass_ResendView();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_ResendView ResendViewParams { get { return s_params_ResendView; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_ResendView
        {
            public readonly string status = "status";
        }
        static readonly ActionParamsClass_GridGetAllResendSMSPager s_params_GridGetAllResendSMSPager = new ActionParamsClass_GridGetAllResendSMSPager();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_GridGetAllResendSMSPager GridGetAllResendSMSPagerParams { get { return s_params_GridGetAllResendSMSPager; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_GridGetAllResendSMSPager
        {
            public readonly string txtSearch = "txtSearch";
            public readonly string page = "page";
        }
        static readonly ActionParamsClass_Resend s_params_Resend = new ActionParamsClass_Resend();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Resend ResendParams { get { return s_params_Resend; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Resend
        {
            public readonly string param = "param";
        }
        static readonly ActionParamsClass_Upload s_params_Upload = new ActionParamsClass_Upload();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Upload UploadParams { get { return s_params_Upload; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Upload
        {
            public readonly string excelFile = "excelFile";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string GridGetAllResendSMS = "GridGetAllResendSMS";
                public readonly string Index = "Index";
                public readonly string Upload = "Upload";
            }
            public readonly string GridGetAllResendSMS = "~/Views/ResendSMS/GridGetAllResendSMS.cshtml";
            public readonly string Index = "~/Views/ResendSMS/Index.cshtml";
            public readonly string Upload = "~/Views/ResendSMS/Upload.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_ResendSMSController : LoyaltyPointEngine.CMS.Controllers.ResendSMSController
    {
        public T4MVC_ResendSMSController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string status);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(string status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "status", status);
            IndexOverride(callInfo, status);
            return callInfo;
        }

        [NonAction]
        partial void ResendViewOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string status);

        [NonAction]
        public override System.Web.Mvc.ActionResult ResendView(string status)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ResendView);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "status", status);
            ResendViewOverride(callInfo, status);
            return callInfo;
        }

        [NonAction]
        partial void GridGetAllResendSMSOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult GridGetAllResendSMS()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllResendSMS);
            GridGetAllResendSMSOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void GridGetAllResendSMSPagerOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string txtSearch, int? page);

        [NonAction]
        public override System.Web.Mvc.ActionResult GridGetAllResendSMSPager(string txtSearch, int? page)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.GridGetAllResendSMSPager);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "txtSearch", txtSearch);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            GridGetAllResendSMSPagerOverride(callInfo, txtSearch, page);
            return callInfo;
        }

        [NonAction]
        partial void ResendOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, LoyaltyPointEngine.Model.Object.OTP.ResendSMSModel param);

        [NonAction]
        public override System.Web.Mvc.ActionResult Resend(LoyaltyPointEngine.Model.Object.OTP.ResendSMSModel param)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Resend);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "param", param);
            ResendOverride(callInfo, param);
            return callInfo;
        }

        [NonAction]
        partial void UploadOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult Upload()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Upload);
            UploadOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void UploadOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, System.Web.HttpPostedFileBase excelFile);

        [NonAction]
        public override System.Web.Mvc.ActionResult Upload(System.Web.HttpPostedFileBase excelFile)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Upload);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "excelFile", excelFile);
            UploadOverride(callInfo, excelFile);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
