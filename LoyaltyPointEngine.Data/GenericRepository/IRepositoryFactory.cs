﻿using LoyaltyPointEngine.Data.GenericRepository;
namespace LoyaltyPointEngine.Data
{
    public interface IRepositoryFactory
    {
        IRepository Repository();
    }
}
