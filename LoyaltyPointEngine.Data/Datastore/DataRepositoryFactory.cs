﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Linq;
using LoyaltyPointEngine.Data.GenericRepository;

namespace LoyaltyPointEngine.Data
{
    public static class DataRepositoryFactory
    {
        public static LoyaltyPointEngineNewEntities CurrentRepository
        {
            get
            {
                var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as LoyaltyPointEngineNewEntities;
                if (repository == null)
                {

                    repository = new LoyaltyPointEngineNewEntities();
                    DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = repository;
                }

                return repository;

            }
        }

        public static void CloseCurrentRepository()
        {
            var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as LoyaltyPointEngineNewEntities;
            if (repository != null)
            {
                repository.Dispose();
                DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = null;
            }
        }
    }
}
