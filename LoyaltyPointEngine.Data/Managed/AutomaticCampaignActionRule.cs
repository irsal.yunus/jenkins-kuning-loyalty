﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoyaltyPointEngine.Data
{
    public partial class AutomaticCampaignActionRule
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<AutomaticCampaignActionRule>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "AutomaticCampaignActionRule", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                //this.IsDeleted = false;
                this.UpdateSave<AutomaticCampaignActionRule>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Edit,
                    "AutomaticCampaignActionRule", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = true;
                this.UpdateSave<AutomaticCampaignActionRule>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Delete,
                    "AutomaticCampaignActionRule", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public static IQueryable<AutomaticCampaignActionRule> GetAll(bool? isDeleted)
        {
            if (isDeleted.HasValue)
            {
                return DataRepositoryFactory.CurrentRepository.AutomaticCampaignActionRule.Where(x => x.IsDeleted == isDeleted.Value);
            }
            return DataRepositoryFactory.CurrentRepository.AutomaticCampaignActionRule.AsQueryable();
        }

        public static IEnumerable<AutomaticCampaignActionRule> GetByCampaignID(int campaignID)
        {
            return GetAll(false).Where(x => x.CampaignID == campaignID).AsEnumerable();
        }

        public static IEnumerable<AutomaticCampaignActionRule> GetByCampaignIDAndActionID(int campaignID, int actionID)
        {
            return GetAll(false).Where(x => x.CampaignID == campaignID && x.ActionID == actionID).OrderBy(x => x.ID).AsEnumerable();
        }

        public static AutomaticCampaignActionRule GetByID(long id)
        {
            return GetAll(false).FirstOrDefault(x => x.ID == id);
        }
    }
}
