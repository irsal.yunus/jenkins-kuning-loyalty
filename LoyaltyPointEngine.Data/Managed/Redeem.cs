﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Redeem
    {

        public EFResponse Inserted(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<Redeem>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;

                this.UpdateSave<Redeem>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<Redeem> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Redeem;
        }

        public static IQueryable<Redeem> GetAllWithMember()
        {
            return GetAll().Join(Data.Member.GetAll(),
                R => R.MemberID,
                M => M.ID,
                (R, M) => new
                {
                    red = R,
                    mem = M
                })
                .Where(x => !x.mem.IsDeleted)
                .Select(x => new Redeem
                {
                    ID = x.red.ID,
                    MemberID = x.red.MemberID,
                    TransactionCode = x.red.TransactionCode,
                    TransactionDate = x.red.TransactionDate,
                    PoolID = x.red.PoolID,
                    Point = x.red.Point,
                    IsVoid = x.red.IsVoid,
                    CreatedBy = x.red.CreatedBy,
                    CreatedByID = x.red.CreatedByID.HasValue ? x.red.CreatedByID : null,
                    UpdatedBy = x.red.UpdatedBy,
                    UpdatedByID = x.red.UpdatedByID.HasValue ? x.red.UpdatedByID : null,
                    CreatedDate = x.red.CreatedDate,
                    UpdatedDate = x.red.UpdatedDate.HasValue ? x.red.UpdatedDate : null,
                    CurrentMember = x.mem
                });
        }

        public static Redeem GetById(Guid ID)
        {
            return DataRepositoryFactory.CurrentRepository.Redeem.FirstOrDefault(x => x.ID == ID);
        }

        public static IQueryable<Redeem> GetByMemberID(int ID)
        {
            return GetAll().Where(x => x.MemberID == ID);
        }

        public static Redeem GetByTransactionCode(string TransactionCode)
        {
            return DataRepositoryFactory.CurrentRepository.Redeem.FirstOrDefault(x => x.TransactionCode.ToLower() == TransactionCode.ToLower());
        }

        public Data.Member _CurrentMember;
        public Data.Member CurrentMember
        {
            get
            {
                return Data.Member.GetById(this.MemberID);
            }
            set
            {
                _CurrentMember = Data.Member.GetById(this.MemberID);
            }
        }

        public static EFResponse SubmitRedeem(Guid? redemptionID, string transactionCode, int memberID, int memberPoint, string receiverName, string receiverPhone, string receiverAddress, int poolId, string userId)
        {
            EFResponse model = new EFResponse();

            try
            {
                DataRepositoryFactory.CurrentRepository.sp_SubmitRedeem(redemptionID, transactionCode, memberID, memberPoint, receiverName, receiverPhone, receiverAddress, poolId, userId);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
    }
}
