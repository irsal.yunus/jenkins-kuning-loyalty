﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class CampaignActionDetail
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<CampaignActionDetail>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Campaign,
                    EventLogLogic.LogEventCode.Add,
                    "CampaignActionDetail", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<CampaignActionDetail>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Campaign,
                    EventLogLogic.LogEventCode.Edit,
                    "CampaignActionDetail", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CampaignActionDetail>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Campaign,
                    EventLogLogic.LogEventCode.Delete,
                    "CampaignActionDetail", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<CampaignActionDetail> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CampaignActionDetail.Where(x => !x.IsDeleted);
        }

        public static IQueryable<CampaignActionDetail> GetAllByCampaignID(int CampaignID)
        {
            return GetAll().Where(x => x.CampaignID == CampaignID);
        }

        public static IQueryable<CampaignActionDetail> GetAllActive()
        {
            return GetAll().Where(x => x.IsActive);
        }
        public static IQueryable<CampaignActionDetail> GetAllActiveByAction(int ActionID)
        {
            return GetAllActive().Where(x => x.ActionID == ActionID && x.Campaign.IsActive && !x.Campaign.IsDeleted &&
               EntityFunctions.TruncateTime(x.Campaign.StartDate) <=   EntityFunctions.TruncateTime( DateTime.Now)
               && EntityFunctions.TruncateTime(x.Campaign.EndDate) >= EntityFunctions.TruncateTime(DateTime.Now)
                
                );
        }

        public static CampaignActionDetail GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.CampaignActionDetail.FirstOrDefault(x => x.ID == ID);
        }

    }
}
