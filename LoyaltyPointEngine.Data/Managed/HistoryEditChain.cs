﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class HistoryEditChain
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.Save<HistoryEditChain>();
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.Receipt,
                //    EventLogLogic.LogEventCode.Add,
                //    "Receipt", this, Url, By);
                //model.AdditionalInfo = this.ID.ToString();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<HistoryEditChain> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.HistoryEditChain;
        }

        public static HistoryEditChain GetByReceiptId(Guid ReceiptId)
        {
            return DataRepositoryFactory.CurrentRepository.HistoryEditChain.FirstOrDefault(x => x.ReceiptID == ReceiptId);
        }
    }
}
