﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class FlagUptradeManage
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<FlagUptradeManage>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.IsDeleted = false;
                this.UpdateSave<FlagUptradeManage>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<FlagUptradeManage>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<FlagUptradeManage> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.FlagUptradeManage.Where(x => !x.IsDeleted.Value);
        }

        public static FlagUptradeManage GetById(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.FlagUptradeManage.FirstOrDefault(x => x.ID == ID && x.IsDeleted != true);
        }

        public static FlagUptradeManage GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.FlagUptradeManage.FirstOrDefault(x => x.Code == code && x.IsDeleted != true);
        }

        public static FlagUptradeManage GetByCodeNotSelf(string code, long ID)
        {
            return DataRepositoryFactory.CurrentRepository.FlagUptradeManage.FirstOrDefault(x => x.Code == code && x.ID != ID);
        }
    }
}
