﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Common;

namespace LoyaltyPointEngine.Data
{
    public partial class CodeGimmick
    {

        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<CodeGimmick>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdateSave<CodeGimmick>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CodeGimmick>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "CodeGimmick", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<CodeGimmick> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CodeGimmick.Where(x => x.IsDeleted != true).OrderByDescending(x => x.CreatedDate);
        }

        public static CodeGimmick GetByID(int Id)
        {
            return DataRepositoryFactory.CurrentRepository.CodeGimmick.Where(x => x.IsDeleted != true).FirstOrDefault(x => x.ID == Id);
        }

        public static CodeGimmick GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.CodeGimmick.FirstOrDefault(x => x.Name == code && x.IsDeleted != true);
        }

        public static CodeGimmick GetByCodeNotSelf(string code, long ID)
        {
            return DataRepositoryFactory.CurrentRepository.CodeGimmick.FirstOrDefault(x => x.Name == code && x.ID != ID);
        }
    }
}
