﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class IncentiveSpg
    {
        public EFResponse Insert()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Save<IncentiveSpg>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update()
        {
            EFResponse model = new EFResponse();

            try
            {

                this.UpdateSave<IncentiveSpg>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<IncentiveSpg>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<IncentiveSpg> GetBySpgCode(string spgCode)
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgs.Where(x => x.SpgCode == spgCode);
        }

        public static IQueryable<IncentiveSpg> GetBySpgCodeOnlyDisplayed(string spgCode)
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgs.Where(x => x.SpgCode == spgCode && x.IsDisplayed == 1);
        }

        public static IncentiveSpg GetBySpgCode(string spgCode, int month, int year)
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgs.Where(x => x.SpgCode == spgCode && x.Month == month && x.year == year).FirstOrDefault();
        }

        public static IQueryable<IncentiveSpg> GetByYear(int year)
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgs.Where(x => x.year == year);
        }
    }
}
