﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class RetailerSubAccount
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<RetailerSubAccount>();

            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                
                this.UpdateSave<RetailerSubAccount>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }


        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<RetailerSubAccount>();

            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<RetailerSubAccount> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.RetailerSubAccount.Where(x => !x.IsDeleted);
        }

        public static RetailerSubAccount GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.RetailerSubAccount.FirstOrDefault(x => x.ID == ID);
        }

        public static RetailerSubAccount GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.RetailerSubAccount.FirstOrDefault(x => x.Code == Code);
        }
    }
}
