﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class CMSNavigation
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.IsActive = true;
                this.Save<CMSNavigation>();
                EventLog.InsertEventLog(UserId, 
                    EventLogLogic.LogType.Information, 
                    EventLogLogic.LogSource.Navigation, 
                    EventLogLogic.LogEventCode.Add,
                    "CMSNavigation", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.IsDeleted = false;
                this.UpdateSave<CMSNavigation>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Navigation,
                    EventLogLogic.LogEventCode.Edit,
                    "CMSNavigation", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CMSNavigation>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Navigation,
                    EventLogLogic.LogEventCode.Delete,
                    "CMSNavigation", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<CMSNavigation> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CMSNavigation
                .Where(x => !x.IsDeleted);
        }
        public static IQueryable<CMSNavigation> GetAllParent()
        {
            return GetAll().Where(x => !x.IsChild);
        }

        public static CMSNavigation GetByID(long ID)
        {
            IQueryable<CMSNavigation> res = GetAll().Where(x => x.ID == ID);
            return res.FirstOrDefault();
        }

    }
}
