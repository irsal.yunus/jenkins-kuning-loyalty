﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class IncentiveSpgMapping
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Save<IncentiveSpgMapping>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<IncentiveSpgMapping>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<IncentiveSpgMapping>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<IncentiveSpgMapping> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgMappings;
        }
        public static IncentiveSpgMapping GetByCondition(string registeredBy, string uploadedBy)
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgMappings.Where(x => x.RegisteredBy == registeredBy && x.UploadedBy == uploadedBy).FirstOrDefault();
        }

    }
}
