﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class FacebookPost
    {

        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
               
                this.CreatedBy = By;
               
                this.Save<FacebookPost>();

                //to much junk
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.FacebookPost,
                //    EventLogLogic.LogEventCode.Add,
                //    "FacebookPost", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<FacebookPost> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.FacebookPost;
        }
        public static FacebookPost GetNewestPost()
        {
            return DataRepositoryFactory.CurrentRepository.FacebookPost.OrderByDescending(x=> x.CreatedDate).FirstOrDefault();
        }
        public static FacebookPost GetByID(Guid ID)
        {
            return DataRepositoryFactory.CurrentRepository.FacebookPost.FirstOrDefault(x => x.ID == ID);
        }
    }
}
