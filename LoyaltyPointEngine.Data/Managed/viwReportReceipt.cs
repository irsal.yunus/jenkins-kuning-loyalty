﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class viwReportReceipt
    {
        public static IQueryable<viwReportReceipt> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.viwReportReceipt.Where(x => !x.IsDeleted);
        }
    }
}
