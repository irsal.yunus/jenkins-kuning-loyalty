﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Adjustment
    {

        public EFResponse Insert(string By, long? UserCMSID)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.CreatedByID = UserCMSID;
                this.Save<Adjustment>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, long? UserCMSID)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdatedByID = UserCMSID;
                this.UpdateSave<Adjustment>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }


        public static Adjustment GetById(Guid ID)
        {
            return DataRepositoryFactory.CurrentRepository.Adjustment.FirstOrDefault(x => x.ID == ID);
        }


        public static IQueryable<Adjustment> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Adjustment;
        }
    }
}
