﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Promo
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.Save<Promo>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Add,
                    "Promo", this, Url, By);
                model.AdditionalInfo = this.ID.ToString();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.UpdateSave<Retailer>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Edit,
                    "Receipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<Promo> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Promo;
        }
        public static Promo GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Promo.FirstOrDefault(x => x.ID == ID);
        }
        public static Promo GetByCode(string Code)
        {
            return GetAll().FirstOrDefault(x => x.PromoCode.ToLower() == Code.ToLower() && x.IsActive.HasValue && x.IsActive.Value);
        }
        public static ObjectResult<GetReportPromoSMS_Result> GetReportPromoSMS()
        {
            return DataRepositoryFactory.CurrentRepository.GetReportPromoSMS();
        }
    }
}
