﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class PoolMemberToken
    {
        public static PoolMemberToken GetByToken(string Token)
        {
            return DataRepositoryFactory.CurrentRepository.PoolMemberToken.FirstOrDefault(x => x.VerificationToken == Token);
        }

        public static IQueryable<PoolMemberToken> GetByMemberID(int MemberID)
        {
            return DataRepositoryFactory.CurrentRepository.PoolMemberToken.Where(x => x.MemberId == MemberID);
        }

        public static PoolMemberToken GetLatestByMemberID(int MemberID)
        {
            return GetByMemberID(MemberID).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        public static PoolMemberToken GetLatestByMemberIDAndDeviceType(int MemberID, string deviceType)
        {
            return GetByMemberID(MemberID)
                .Where(x => !string.IsNullOrEmpty(x.DeviceID) && !string.IsNullOrEmpty(x.DeviceType) && x.DeviceType.Trim().ToLower() == deviceType.Trim().ToLower())
                .OrderByDescending(x => x.CreatedDate)
                .FirstOrDefault();
        }

        public static List<PoolMemberToken> GetLatestsByMemberIDAndDeviceType(int MemberID, string deviceType)
        {
            return GetByMemberID(MemberID)
                .Where(x => x.VerificationTokenExpirationDate >= DateTime.Now)
                .Where(x => !string.IsNullOrEmpty(x.DeviceID) && !string.IsNullOrEmpty(x.DeviceType) && x.DeviceType.Trim().ToLower() == deviceType.Trim().ToLower())
                .OrderByDescending(x => x.CreatedDate)
                .ToList();
        }

        public void Inserted()
        {
            this.TotalRefresh = 0;
            this.CreatedDate = DateTime.Now;
            this.InsertSave<PoolMemberToken>();
        }

        public void Updated()
        {
            this.ModifyDate = DateTime.Now;
            this.UpdateSave<PoolMemberToken>();
        }

        public EFResponse HardDeleted()
        {
            EFResponse model = new EFResponse();

            try
            {

                this.Delete<PoolMemberToken>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException.ToString();
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
    }
}
