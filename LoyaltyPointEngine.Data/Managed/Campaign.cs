﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Campaign
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Campaign>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Campaign,
                    EventLogLogic.LogEventCode.Add,
                    "Campaign", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Campaign>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Campaign,
                    EventLogLogic.LogEventCode.Edit,
                    "Campaign", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<Campaign>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Campaign,
                    EventLogLogic.LogEventCode.Delete,
                    "Campaign", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<Campaign> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Campaign.Where(x => !x.IsDeleted);
        }

        public static Campaign GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Campaign.FirstOrDefault(x => x.ID == ID);
        }

        public static Campaign GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.Campaign.FirstOrDefault(x => x.Code == Code);
        }

        public static Campaign GetByCodeNotSelf(string Code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Campaign.FirstOrDefault(x => x.Code == Code&& x.ID != ID);
        }

        public static IQueryable<Campaign> GetDataCampaignExpiredPerUser()
        {
            return DataRepositoryFactory.CurrentRepository.Campaign.Where(x => !x.IsDeleted && x.IsAutomate == true 
                && x.IsExpiredPerUser == true && x.ExpireIn != null);
        }
    }
}
