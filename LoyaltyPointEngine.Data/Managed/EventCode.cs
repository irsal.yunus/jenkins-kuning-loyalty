﻿using System;
using System.Linq;
using LoyaltyPointEngine.Common;

namespace LoyaltyPointEngine.Data
{
    public partial class EventCode
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<EventCode>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.EventCode,
                    EventLogLogic.LogEventCode.Add,
                    "EventCode", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<EventCode>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.EventCode,
                    EventLogLogic.LogEventCode.Edit,
                    "EventCode", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<EventCode>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.EventCode,
                    EventLogLogic.LogEventCode.Delete,
                    "EventCode", this, Url, By);

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<EventCode> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.EventCode.Where(x => !x.IsDeleted);
        }

        public static EventCode GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.EventCode.FirstOrDefault(x => !x.IsDeleted && x.ID == ID);
        }

        public static EventCode GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.EventCode.FirstOrDefault(x => !x.IsDeleted && x.Code == code);
        }

        public static EventCode GetByCodeNotSelf(int ID, string Code)
        {
            return DataRepositoryFactory.CurrentRepository.EventCode.FirstOrDefault(x => x.Code == Code && x.ID != ID);
        }

        public static bool IsExist(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.EventCode.Any(x => !x.IsDeleted && x.ID == ID);
        }
    }
}
