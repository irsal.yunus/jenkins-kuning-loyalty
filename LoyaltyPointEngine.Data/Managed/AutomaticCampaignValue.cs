﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoyaltyPointEngine.Data
{
    public partial class AutomaticCampaignValue
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<AutomaticCampaignValue>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "AutomaticCampaignValue", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdateSave<AutomaticCampaignValue>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Edit,
                    "AutomaticCampaignValue", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public static IQueryable<AutomaticCampaignValue> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.AutomaticCampaignValue.AsQueryable();
        }

        public static IEnumerable<AutomaticCampaignValue> GetAllByActionIDndCampaignIDAndMemberID(int actionID, int campaignID, int memberID)
        {
            return GetAll().Where(x => x.ActionID == actionID && x.CampaignID == campaignID && x.MemberID == memberID);
        }

        public static AutomaticCampaignValue GetAllByActionIDAndRuleIDAndCampaignIDAndMemberID(int actionID, int ruleID, long campaignID, int memberID)
        {
            return GetAll().FirstOrDefault(x => x.ActionID == actionID && x.RuleID == ruleID && x.CampaignID == campaignID && x.MemberID == memberID);
        }
    }
}
