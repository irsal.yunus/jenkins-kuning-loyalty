﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using System;
using System.Linq;

namespace LoyaltyPointEngine.Data
{
    public partial class Member
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Name = StringHelper.SanitizeFromWhiteSpace(this.Name);
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Member>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Member,
                    EventLogLogic.LogEventCode.Add,
                    "Member", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity =e.InnerException != null? e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId, object Before = null)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Name = StringHelper.SanitizeFromWhiteSpace(this.Name);
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Member>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Member,
                    EventLogLogic.LogEventCode.Edit,
                    "Member", Before, this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<Member>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Member,
                    EventLogLogic.LogEventCode.Delete,
                    "Member", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdatedBy = By;
                this.UpdatedDate = DateTime.Now;
                this.UpdateSave<Member>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<Member> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Member.Where(x => !x.IsDeleted);
        }

        public static Member GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => x.ID == ID && !x.IsDeleted);
        }

        public static Member GetByPhone(string key)
        {
            return DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.Phone != null && x.Phone != "" && x.Phone.ToLower() == key.ToLower());
        }


        public static Member GetByIdOrEmailOrPhone(string key)
        {
            Member result = null;
            int id = 0;

            if (int.TryParse(key, out id))
            {
               result = DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => x.ID == id);
            }

            result = DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.Email != null && x.Email != "" && x.Email.ToLower() == key.ToLower());
            if(result == null)
            {
                result = DataRepositoryFactory.CurrentRepository.Member.FirstOrDefault(x => !x.IsDeleted && x.Phone != null && x.Phone != "" && x.Phone.ToLower() == key.ToLower());
            }


            return result;
           
        }

    }
}
