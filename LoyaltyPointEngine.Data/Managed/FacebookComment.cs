﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class FacebookComment
    {
   

        public EFResponse Inserted(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedBy = By;
                this.Save<FacebookComment>();
                //to much junk
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.FacebookComment,
                //    EventLogLogic.LogEventCode.Add,
                //    "FacebookComment", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
              
                this.UpdateSave<FacebookComment>();
                //to much junk
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.FacebookComment,
                //    EventLogLogic.LogEventCode.Add,
                //    "FacebookComment", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<FacebookComment> GetByPostID(Guid PostID)
        {
            return DataRepositoryFactory.CurrentRepository.FacebookComment.Where(x => x.FacebookPostID == PostID);
        }

      
    }
}
