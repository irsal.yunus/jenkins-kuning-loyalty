﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class IncentiveSpgLog
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.Save<IncentiveSpgLog>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<IncentiveSpgLog>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<IncentiveSpgLog>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<IncentiveSpgLog> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgLogs;
        }
        public static IQueryable<IncentiveSpgLog> GetBySpgCode(string spgCode)
        {
            return DataRepositoryFactory.CurrentRepository.IncentiveSpgLogs.Where(x => x.SpgCode == spgCode);
        }

    }
}
