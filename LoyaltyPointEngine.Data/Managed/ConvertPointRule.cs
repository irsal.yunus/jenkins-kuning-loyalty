﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class ConvertPointRule
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.ModifiedDate = this.CreatedDate = DateTime.Now;
                this.ModifiedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<ConvertPointRule>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "Area", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.ModifiedDate = DateTime.Now;
                this.ModifiedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<ConvertPointRule>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "ConvertPointRule", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<ConvertPointRule>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "ConvertPointRule", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<ConvertPointRule> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ConvertPointRules.Where(x => !x.IsDeleted);
        }

        public static ConvertPointRule GetById(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.ConvertPointRules.FirstOrDefault(x => x.ID == ID);
        }

        public static ConvertPointRule GetByStoreId(long storeId)
        {
            return GetAll().FirstOrDefault(x => x.StoreId == storeId);
        }

        public static IQueryable<ConvertPointRule> GetAllByStoreId(long storeId, bool allIncludeDeleted = false)
        {
            if (allIncludeDeleted)
                return DataRepositoryFactory.CurrentRepository.ConvertPointRules.Where(x => x.StoreId == storeId);
            return DataRepositoryFactory.CurrentRepository.ConvertPointRules.Where(x => x.IsDeleted == false && x.StoreId == storeId);
        }

        public static IQueryable<ConvertPointRule> GetAllByActionCode(string actionCode, bool allIncludeDeleted = false)
        {
            if (allIncludeDeleted)
                return DataRepositoryFactory.CurrentRepository.ConvertPointRules.Where(x => x.ActionCode == actionCode);
            return DataRepositoryFactory.CurrentRepository.ConvertPointRules.Where(x => x.IsDeleted == false && x.ActionCode == actionCode);
        }

        public static ConvertPointRule GetByActionCode(string actionCode)
        {
            return GetAllByActionCode(actionCode).FirstOrDefault();
        }
    }
}
