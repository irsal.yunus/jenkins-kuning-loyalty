﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class OCRTemplate
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Save<OCRTemplate>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "OCRTemplate", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<OCRTemplate>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "OCRTemplate", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<OCRTemplate>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "OCRTemplate", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<OCRTemplate> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.OCRTemplate.Where(x => !x.IsDeleted );
        }

        public static OCRTemplate GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.OCRTemplate.FirstOrDefault(x => x.ID == ID && !x.IsDeleted);
        }

        public static OCRTemplate GetByStoreName(string storeName)
        {
            return DataRepositoryFactory.CurrentRepository.OCRTemplate.FirstOrDefault(x => x.StoreName == storeName);
        }

        //public static IEnumerable<GetPredictedOcrTemplateByStoreName_Result> GetPredictedOcrTemplateByStoreName(string storeName)
        //{
        //    return DataRepositoryFactory.CurrentRepository.GetPredictedOcrTemplateByStoreName(storeName).AsEnumerable();
        //}
    }
}
