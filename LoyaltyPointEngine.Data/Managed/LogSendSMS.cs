﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class LogSendSMS
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.Save<LogSendSMS>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.LogSendSMS,
                    EventLogLogic.LogEventCode.Add,
                    "LogSendSMS", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CallbackDate = DateTime.Now;
                this.UpdateSave<LogSendSMS>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.LogSendSMS,
                    EventLogLogic.LogEventCode.Edit,
                    "LogSendSMS", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<LogSendSMS> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.LogSendSMS.AsQueryable();
        }

        public static LogSendSMS GetByTransid(string transid)
        {
            return DataRepositoryFactory.CurrentRepository.LogSendSMS
                .FirstOrDefault(x => x.Transid == transid);
        }
    }
}
