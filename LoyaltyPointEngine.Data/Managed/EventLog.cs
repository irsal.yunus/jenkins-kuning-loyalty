﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class EventLog
    {
        public void Inserted(string by)
        {
            
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = by;
            this.InsertSave<EventLog>();
        }

        public static void InsertEventLog(long? UserId, 
            LoyaltyPointEngine.Common.EventLogLogic.LogType logType, 
            LoyaltyPointEngine.Common.EventLogLogic.LogSource source, 
            LoyaltyPointEngine.Common.EventLogLogic.LogEventCode eventCode, 
            string Message, object Value, string url, string createdBy)
        {
            EventLog log = new EventLog();
            log.ID = Guid.NewGuid();
            log.UserID = UserId;
            log.LogType = logType.ToString();
            log.Source = source.ToString();
            log.EventCode = eventCode.ToString();
            log.Message = Message;
            if (Value != null)
            {
                // Serializer settings
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ContractResolver = new CustomResolver();
                settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
                settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                settings.Formatting = Formatting.Indented;

                log.SourceValue = JsonConvert.SerializeObject(Value, settings);
            }

            log.SourceUrl = url;
            //log.Inserted(createdBy);


        }

        public static void InsertEventLog(long? UserId,
            LoyaltyPointEngine.Common.EventLogLogic.LogType logType,
            LoyaltyPointEngine.Common.EventLogLogic.LogSource source,
            LoyaltyPointEngine.Common.EventLogLogic.LogEventCode eventCode,
            string Message, object BeforeValue, object AfterValue, string url, string createdBy)
        {
            EventLog log = new EventLog();
            log.ID = Guid.NewGuid();
            log.UserID = UserId;
            log.LogType = logType.ToString();
            log.Source = source.ToString();
            log.EventCode = eventCode.ToString();
            log.Message = Message;
            if (BeforeValue != null)
            {
                // Serializer settings
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ContractResolver = new CustomResolver();
                settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
                settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                settings.Formatting = Formatting.Indented;

                log.SourceValue = JsonConvert.SerializeObject(BeforeValue, settings) + " ";
            }
            if (AfterValue != null)
            {
                // Serializer settings
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.ContractResolver = new CustomResolver();
                settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
                settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                settings.Formatting = Formatting.Indented;

                log.SourceValue = log.SourceValue + JsonConvert.SerializeObject(AfterValue, settings);
            }

            log.SourceUrl = url;
            //log.Inserted(createdBy);


        }
        private class CustomResolver : DefaultContractResolver
        {
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty prop = base.CreateProperty(member, memberSerialization);

                if (prop.DeclaringType != typeof(object) &&
                    prop.PropertyType.IsClass &&
                    prop.PropertyType != typeof(string))
                {
                    prop.ShouldSerialize = obj => false;
                }

                return prop;
            }
        }

    }
}
