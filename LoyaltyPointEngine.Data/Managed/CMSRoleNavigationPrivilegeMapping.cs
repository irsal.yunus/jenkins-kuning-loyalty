﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class CMSRoleNavigationPrivilegeMapping
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.IsActive = true;
                this.Save<CMSRoleNavigationPrivilegeMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Add,
                    "CMSRoleNavigationPrivilegeMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.IsDeleted = false;
                this.UpdateSave<CMSRoleNavigationPrivilegeMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Edit,
                    "CMSRoleNavigationPrivilegeMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CMSRoleNavigationPrivilegeMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Delete,
                    "CMSRoleNavigationPrivilegeMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<CMSRoleNavigationPrivilegeMapping> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CMSRoleNavigationPrivilegeMapping
                .Where(x => !x.IsDeleted);
        }

        public static CMSRoleNavigationPrivilegeMapping GetByID(long ID)
        {
            IQueryable<CMSRoleNavigationPrivilegeMapping> res = GetAll().Where(x => x.ID == ID);
            return res.FirstOrDefault();
        }

        public static IQueryable<CMSRoleNavigationPrivilegeMapping> GetByRole(long RoleID)
        {
            return DataRepositoryFactory.CurrentRepository.CMSRoleNavigationPrivilegeMapping
                .Where(x => !x.IsDeleted && x.CMSRoleID ==RoleID && x.IsActive);
        }



        public static CMSRoleNavigationPrivilegeMapping GetByRoleAndNavigationPrevilage(long RoleID, long NavigationPrevilageID)
        {
            CMSRoleNavigationPrivilegeMapping res = CMSRoleNavigationPrivilegeMapping.GetAll().Where(x => x.CMSRoleID == RoleID && x.CMSNavigationPrivilegeID == NavigationPrevilageID).FirstOrDefault();
            return res;
        }

    }
}
