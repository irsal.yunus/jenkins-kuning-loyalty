﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class FacebookLike
    {
        public EFResponse Inserted(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {

                this.CreatedBy = By;
                this.CreatedDate = DateTime.Now;
                this.Save<FacebookLike>();

                //to much junk
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.FacebookLike,
                //    EventLogLogic.LogEventCode.Add,
                //    "FacebookLike", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public EFResponse Updated(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {

                this.CreatedBy = By;
                this.CreatedDate = DateTime.Now;
                this.UpdateSave<FacebookLike>();

                //to much junk
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.FacebookLike,
                //    EventLogLogic.LogEventCode.Add,
                //    "FacebookLike", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<FacebookLike> GetByPostID(Guid PostID)
        {
            return DataRepositoryFactory.CurrentRepository.FacebookLike.Where(x=> x.FacebookPostID == PostID);
        }
    }
}
