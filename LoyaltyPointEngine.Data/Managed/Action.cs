﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Action
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Action>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "Action", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Action>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Edit,
                    "Action", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<Action>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Delete,
                    "Action", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<Action> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Action.Where(x => !x.IsDeleted);
        }
        public static IQueryable<Action> GetByCategoryID(int CategoryID)
        {
            return DataRepositoryFactory.CurrentRepository.Action.Where(x => !x.IsDeleted && x.IsActive && x.ActionCategoryID == CategoryID);
        }
        public static Action GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Action.FirstOrDefault(x => x.ID == ID);
        }

        public static Action GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.Action.FirstOrDefault(x => x.Code == Code && x.IsActive && !x.IsDeleted);
        }

        public static Action GetByCodeNotSelf(string Code, int id)
        {
            return DataRepositoryFactory.CurrentRepository.Action.FirstOrDefault(x => x.Code == Code && x.ID != id && !x.IsDeleted);
        }


        public static Action GetByProductID(int ProductID)
        {
            return DataRepositoryFactory.CurrentRepository.Action.FirstOrDefault(x => x.ProductID == ProductID && !x.IsDeleted);
        }

        public static Action GetByProductIDNotSelf(int ProductID, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Action.FirstOrDefault(x => x.ID != null && x.ID == ID && x.ProductID == ProductID);
        }


        public static Action GetByProductVendorId(int ProductVendorId)
        {
            return DataRepositoryFactory.CurrentRepository.Action.FirstOrDefault(x => x.ProductIdVendor == ProductVendorId);
        }

        public static bool IsExist(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Action.Any(x => !x.IsDeleted && x.ID == ID && x.IsActive);
        }

        public static IQueryable<Action> GetByProductGroupID(int GroupID)
        {
            return DataRepositoryFactory.CurrentRepository.Action.Where(x => x.ProductID != null && x.Product.ProductGroupID == GroupID && !x.IsDeleted && !x.Product.IsDeleted);
        }
    }
}
