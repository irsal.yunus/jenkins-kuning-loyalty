﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class ConvertPointTransactionLog
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Save<ConvertPointTransactionLog>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "ConvertPointTransactionLog", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<ConvertPointTransactionLog>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "ConvertPointTransactionLog", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<ConvertPointTransactionLog>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "ConvertPointTransactionLog", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<ConvertPointTransactionLog> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ConvertPointTransactionLogs;
        }

        public static IQueryable<ConvertPointTransactionLog> GetByMemberIdAndDate(int memberId, DateTime from, DateTime to)
        {
            return GetAll().Where(x => x.memberId == memberId && x.TransactionDate >= from && x.TransactionDate <= to
                    && x.Status == 1 && x.Type.ToLower().Equals("out"));
        }

        public static long GetStorePointsByMemberIdTransDate(int memberId, DateTime from, DateTime to)
        {
            long points = 0;
            var data = GetByMemberIdAndDate(memberId, from, to).Select(x => x.StorePoint);
            if(data.Count() > 0)
            {
                points = data.Sum();
            }
            return points;
        }

        public static int GetConvertCount(int memberId, DateTime from, DateTime to)
        {
            int count = 0;
            var data = GetByMemberIdAndDate(memberId, from, to);
            if(data != null)
            {
                count = data.Count();
            }
            return count;
        }
    }
}
