﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class PromoActivity
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UniqueID = Guid.NewGuid();
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.Save<PromoActivity>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.PromoActivity,
                    EventLogLogic.LogEventCode.Add,
                    "PromoActivity", this, Url, By);
                model.AdditionalInfo = this.ID.ToString();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.UpdateSave<Retailer>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Edit,
                    "Receipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<PromoActivity> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.PromoActivity.OrderBy(x => x.ID);
        }
        public static PromoActivity GetById(Guid ID)
        {
            return DataRepositoryFactory.CurrentRepository.PromoActivity.FirstOrDefault(x => x.UniqueID == ID);
        }
        

    }
}
