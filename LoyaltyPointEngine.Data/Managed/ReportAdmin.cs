﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public class ReportAdmin
    {
        public static List<SP_REPORTADMIN_Result> getReportAdmin(DateTime? startdate,DateTime? enddate)
        {

            var data = DataRepositoryFactory.CurrentRepository.SP_REPORTADMIN(startdate, enddate).ToList();
            return data;
        }
    }
}
