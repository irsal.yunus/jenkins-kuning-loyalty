﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class SnapcartReceipt
    {
        public enum State
        {
            Queueing = 0,
            FailedSend = -2,
            Failed = -1,
            Success = 1
        }

        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.Save<SnapcartReceipt>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "SnapcartReceipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<SnapcartReceipt>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Edit,
                    "SnapcartReceipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<SnapcartReceipt> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.SnapcartReceipt.AsQueryable();
        }

        public static SnapcartReceipt GetById(Guid Id)
        {
            return GetAll().FirstOrDefault(x => x.ID == Id);
        }

        public static SnapcartReceipt GetByReceiptId(Guid receiptId)
        {
            //return GetAll().FirstOrDefault(x => x.ReceiptID == receiptId);
            return GetAll().Where(x => x.ReceiptID == receiptId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        public static SnapcartReceipt GetByStatusAndRequestId(string statusSnapcart, Guid RequestId)
        {
            return GetAll().FirstOrDefault(x => x.StatusSnapcart.ToLower() == statusSnapcart.ToLower() && x.RequestID == RequestId);
        }

        public static SnapcartReceipt GetByReceiptIdTopOne(Guid receiptId)
        {
            return GetAll().Where(x => x.ReceiptID == receiptId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }
    }
}
