﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class CMSRole
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.IsActive = true;
                this.Save<CMSRole>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Add,
                    "CMSRole", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.IsDeleted = false;
                this.UpdateSave<CMSRole>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Edit,
                    "CMSRole", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CMSRole>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Delete,
                    "CMSRole", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<CMSRole> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CMSRole
                .Where(x => !x.IsDeleted);
        }

        public static CMSRole GetByID(long ID)
        {
            IQueryable<CMSRole> res = GetAll().Where(x => x.ID == ID);
            return res.FirstOrDefault();
        }
    }
}
