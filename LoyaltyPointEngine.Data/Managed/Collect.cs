﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LoyaltyPointEngine.Data
{
    public partial class Collect
    {
        public EFResponse Insert(string By, string Url, long? UserId, int timeStamp)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now.AddSeconds(timeStamp);
                this.CreatedBy = By;
                this.UpdatedDate = this.CreatedDate;
                this.UpdatedBy = this.CreatedBy;
                this.IsDeleted = false;
                this.Save<Collect>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Collect,
                    EventLogLogic.LogEventCode.Add,
                    "Collect", this, Url, By);

                model.AdditionalInfo = this.ID.ToString();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                // this.IsDeleted = false;
                this.UpdateSave<Collect>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Collect,
                    EventLogLogic.LogEventCode.Edit,
                    "Collect", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Deleted(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = true;
                this.UpdateSave<Collect>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Collect,
                    EventLogLogic.LogEventCode.Delete,
                    "Collect", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<Collect> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Collect.Where(x => !x.IsDeleted);
        }

        public static IQueryable<Collect> GetAllByReceiptID(Guid ReceiptID)
        {
            return GetAll().Where(x => x.ReceiptID == ReceiptID);
        }

        public static IQueryable<Collect> GetAllActiveByMember(int MemberId)
        {
            return DataRepositoryFactory.CurrentRepository.Collect.Where(x => !x.IsDeleted && x.MemberID == MemberId && x.IsApproved && x.IsActive && !x.IsUsed
                && x.Points > 0
                && x.ExpiryDate > DateTime.Now
                );
        }

        public static IQueryable<dynamic> GetAllWithMember()
        {
            return GetAll().Join(Data.Member.GetAll(),
                C => C.MemberID,
                M => M.ID,
                (C, M) => new
                {
                    coll = C,
                    mem = M
                })
                .Where(x => !x.coll.IsDeleted && !x.mem.IsDeleted);
        }

        public static Collect GetById(Guid ID)
        {
            return DataRepositoryFactory.CurrentRepository.Collect.FirstOrDefault(x => x.ID == ID);
        }

        public static IQueryable<Collect> GetByRedeemId(Guid RedeemID)
        {
            return DataRepositoryFactory.CurrentRepository.Collect.Where(x => !x.IsDeleted && x.RedeemID == RedeemID);
        }


        public static IQueryable<Collect> GetAllExpired(DateTime dtNow)
        {
            return DataRepositoryFactory.CurrentRepository.Collect.Where(x => !x.IsDeleted && x.ExpiryDate <= dtNow && x.TotalPoints > 0 && x.IsApproved && x.IsActive && !x.IsUsed);
        }

        public static IQueryable<Collect> GetAllActiveByStartDateAndEndDate(DateTime startdate, DateTime enddate)
        {
            return DataRepositoryFactory.CurrentRepository.Collect.Where(x => !x.IsDeleted &&
                (EntityFunctions.TruncateTime(x.ExpiryDate) >= EntityFunctions.TruncateTime(startdate) &&
                EntityFunctions.TruncateTime(x.ExpiryDate) <= EntityFunctions.TruncateTime(enddate))
                && x.TotalPoints > 0 && x.IsApproved && x.IsActive && !x.IsUsed);
        }

        public static IQueryable<Collect> GetAllActiveByStartDateAndEndDateByMember(DateTime startdate, DateTime enddate, int MemberID)
        {
            return DataRepositoryFactory.CurrentRepository.Collect.Where(x => !x.IsDeleted &&
                (EntityFunctions.TruncateTime(x.ExpiryDate) >= EntityFunctions.TruncateTime(startdate) &&
                EntityFunctions.TruncateTime(x.ExpiryDate) <= EntityFunctions.TruncateTime(enddate))
                && x.TotalPoints > 0 && x.IsApproved && x.IsActive && !x.IsUsed && x.MemberID == MemberID);
        }
        public void Inserted(string by, int timeStamp)
        {
            this.CreatedDate = DateTime.Now.AddSeconds(timeStamp);
            this.CreatedBy = by;
            this.IsDeleted = false;
            this.InsertSave<Collect>();
        }


        public void Updated(string by)
        {
            this.UpdatedDate = DateTime.Now;
            this.UpdatedBy = by;
            this.UpdateSave<Collect>();
        }

        public static int GetPointsToBeExpiredByMemberID(int MemberId)
        {
            int result = 0;
            var data = GetAllActiveByMember(MemberId)
                .GroupBy(x => EntityFunctions.TruncateTime(x.ExpiryDate))
                .Select(c => new
                {
                    Expire = c.Sum(x => x.TotalPoints)
                    ,
                    ExpiryDate = c.Key
                }).OrderBy(x => x.ExpiryDate).FirstOrDefault();

            if (data != null)
            {
                result = data.Expire;
            }
            return result;
        }

        public static DateTime GetPointsToBeExpiredDateByMemberID(int MemberId)
        {
            DateTime result = new DateTime(DateTime.Now.Year, 12, 31);
            var data = GetAllActiveByMember(MemberId)
                .GroupBy(x => x.ExpiryDate)
                .Select(c => new
                {
                    Expire = c.Key
                }).OrderBy(x => x.Expire).FirstOrDefault();

            if (data != null && (data.Expire.Value - DateTime.Now) < TimeSpan.FromDays(7))
            {
                result = data.Expire.Value;
            }
            return result;
        }

        public static int CollectCountByChildID(int childID)
        {
            return GetAll().Where(x => x.ChildID == childID).Count();
        }
    }
}
