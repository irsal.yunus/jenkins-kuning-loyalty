﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Create This File by Ucup
namespace LoyaltyPointEngine.Data
{
    public partial class LoginLog
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Save<LoginLog>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<LoginLog>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<LoginLog> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.LoginLog;
        }
        public static IQueryable<LoginLog> GetByID(int Id)
        {
            return DataRepositoryFactory.CurrentRepository.LoginLog.Where(x => x.MemberID == Id);
        }
    }
}
