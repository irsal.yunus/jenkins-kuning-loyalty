﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class UptradeManage
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<UptradeManage>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<UptradeManage>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<UptradeManage>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<UptradeManage> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.UptradeManage.Where(x => !x.IsDeleted);
        }

        public static UptradeManage GetById(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.UptradeManage.FirstOrDefault(x => x.ID == ID && x.IsDeleted != true);
        }

        public static UptradeManage GetByProductAfter(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.UptradeManage.FirstOrDefault(x => x.ProductAfter == ID && x.IsDeleted != true);
        }

        public static UptradeManage GetByProductBefore(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.UptradeManage.FirstOrDefault(x => x.ProductBefore == ID && x.IsDeleted != true);
        }
    }
}
