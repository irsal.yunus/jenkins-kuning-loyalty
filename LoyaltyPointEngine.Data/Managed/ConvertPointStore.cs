﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class ConvertPointStore
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.ModifiedDate = this.CreatedDate = DateTime.Now;
                this.ModifiedBy = this.CreatedBy = By;
                this.Save<ConvertPointStore>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "ConvertPointStore", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.ModifiedDate = DateTime.Now;
                this.ModifiedBy = By;
                this.UpdateSave<ConvertPointStore>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "ConvertPointStore", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = true;
                this.UpdateSave<ConvertPointStore>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "ConvertPointStore", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public static IQueryable<ConvertPointStore> GetAll(bool allIncludeDeleted = false)
        {
            if (allIncludeDeleted)
                return DataRepositoryFactory.CurrentRepository.ConvertPointStores;
            return DataRepositoryFactory.CurrentRepository.ConvertPointStores.Where(x => !x.IsDeleted);
        }

        public static ConvertPointStore GetById(long ID)
        {
            return DataRepositoryFactory.CurrentRepository.ConvertPointStores.FirstOrDefault(x => x.ID == ID);
        }
    }
}
