﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class PromoDetails
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.Save<PromoDetails>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Add,
                    "Receipt", this, Url, By);
                model.AdditionalInfo = this.ID.ToString();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.UpdateSave<PromoDetails>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Edit,
                    "Receipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<PromoDetails> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.PromoDetails;
        }
        public static PromoDetails GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.PromoDetails.FirstOrDefault(x => x.ID == ID);
        }
        public static PromoDetails GetByCode(string Code)
        {
            return GetAll().FirstOrDefault(x => x.CodeName.ToLower() == Code.ToLower());
        }
        public static PromoDetails GetByCodeAndCityID(string Code, int districtID)
        {
            return GetAll().FirstOrDefault(x => x.CodeName.ToLower() == Code.ToLower() && x.DistrictID == districtID);
        }
    }
}
