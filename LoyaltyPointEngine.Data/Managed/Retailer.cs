﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Retailer
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Retailer>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "Retailer", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Retailer>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "Retailer", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<Retailer>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "Retailer", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<Retailer> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Retailer.Where(x => !x.IsDeleted);
        }

        public static Retailer GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Retailer.FirstOrDefault(x => x.ID == ID);
        }

        public static Retailer GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.Retailer.FirstOrDefault(x => x.Code == Code);
        }

        public static IQueryable<Retailer> GetByCityID(int CityID)
        {
            return DataRepositoryFactory.CurrentRepository.Retailer.Where(x => !x.IsDeleted && x.CityID == CityID);
        }


        public void InsertMigrate(string By)
        {
           
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Retailer>();
               
         

            
        }

        public void UpdateMigrate(string By)
        {

            this.UpdatedDate = DateTime.Now;
            this.UpdatedBy = By;
            this.IsDeleted = false;
            this.UpdateSave<Retailer>();
        }
    }
}
