﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class WhiteListIP
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedData = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Area>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Global,
                    EventLogLogic.LogEventCode.Add,
                    "WhiteListIP", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedData = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Area>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Global,
                    EventLogLogic.LogEventCode.Edit,
                    "WhiteListIP", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<WhiteListIP>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Global,
                    EventLogLogic.LogEventCode.Delete,
                    "Area", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<WhiteListIP> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.WhiteListIP.Where(x => !x.IsDeleted);
        }

        public static WhiteListIP GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.WhiteListIP.FirstOrDefault(x => x.ID == ID);
        }

        public static WhiteListIP GetByIP(string IP)
        {
            return DataRepositoryFactory.CurrentRepository.WhiteListIP.FirstOrDefault(x => x.IP == IP);
        }

    }
}
