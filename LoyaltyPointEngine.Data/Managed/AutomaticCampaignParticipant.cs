﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoyaltyPointEngine.Data
{
    public partial class AutomaticCampaignParticipant
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<AutomaticCampaignParticipant>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "AutomaticCampaignParticipant", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<AutomaticCampaignParticipant>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Edit,
                    "AutomaticCampaignParticipant", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.IsDeleted = true;
                this.UpdateSave<AutomaticCampaignParticipant>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Delete,
                    "AutomaticCampaignParticipant", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }

        public static IQueryable<AutomaticCampaignParticipant> GetAll(bool? isDeleted)
        {
            if (isDeleted.HasValue)
            {
                return DataRepositoryFactory.CurrentRepository.AutomaticCampaignParticipant.Where(x => x.IsDeleted == isDeleted.Value);
            }
            return DataRepositoryFactory.CurrentRepository.AutomaticCampaignParticipant.AsQueryable();
        }

        public static AutomaticCampaignParticipant GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.AutomaticCampaignParticipant.FirstOrDefault(x => x.ID == ID);
        }

        public static IEnumerable<AutomaticCampaignParticipant> GetByCampaignID(int campaignID, bool isActive = true)
        {
            return GetAll(false).Where(x => x.CampaignID == campaignID && x.IsActive == isActive).AsEnumerable();
        }

        public static bool IsAutomaticCampaignParticipant(int memberID, int campaignID)
        {
            return GetAll(false).Any(x => x.CampaignID == campaignID && x.MemberId == memberID);
        }

        public static IEnumerable<AutomaticCampaignParticipant> GetCampaignIDExpired(int campaignID, int expireIn, bool isDeleted = false)
        {
            return GetAll(isDeleted).Where(x => x.CampaignID == campaignID 
                && System.Data.Objects.EntityFunctions.AddDays(x.CreatedDate, expireIn) <= DateTime.Now && x.IsAchieve == false).AsEnumerable();
        }
    }
}
