﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LoyaltyPointEngine.Data
{
    public partial class PoolOTPMember
    {
        public enum OTPStatus
        {
            Waiting,
            Void,
            Success,
            Limited,
            Expired,
            Failed

        }
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<PoolOTPMember>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "PoolOTPMember", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<PoolOTPMember>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "PoolOTPMember", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<PoolOTPMember>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "PoolOTPMember", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<PoolOTPMember> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.Where(x => x.IsDeleted == false);
        }

        public static PoolOTPMember GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.FirstOrDefault(x => x.ID == ID && x.IsDeleted == false);
        }

        public static PoolOTPMember GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.FirstOrDefault(x => x.OTPCode == Code && x.IsDeleted == false);
        }

        public static PoolOTPMember GetByPhone(string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember
                .FirstOrDefault(x => x.Phone == Phone && x.IsDeleted == false && x.OTPExpiredDate > DateTime.Now);
        }

        public static PoolOTPMember GetByPhoneWaiting(string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember
                .FirstOrDefault(x => x.Phone == Phone && x.IsDeleted == false && x.OTPExpiredDate > DateTime.Now && x.Status == "Waiting");
        }

        public static IQueryable<PoolOTPMember> GetAllbyPhoneEx(string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.Where(x => x.Phone == Phone && x.IsDeleted == false && x.OTPExpiredDate > DateTime.Now);
        }

        public static PoolOTPMember GetPhoneLastRequest(string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.Where(x => x.Phone == Phone).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        public static IQueryable<PoolOTPMember> GetAllbyPhoneRequest(string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.Where(x => x.Phone == Phone && x.OTPExpiredDate > DateTime.Now);
        }

        public static PoolOTPMember GetByPhoneEx(string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember
                .FirstOrDefault(x => x.Phone == Phone && x.IsDeleted == false);
        }

        public static PoolOTPMember GetByOTPReady(string Code, string Phone)
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember
                .FirstOrDefault(x => x.OTPCode == Code && x.Phone == Phone && x.IsDeleted == false && x.OTPExpiredDate > DateTime.Now);
        }

        public static IQueryable<PoolOTPMember> GetByNonPhoneAndNonSuccess()
        {
            return DataRepositoryFactory.CurrentRepository.PoolOTPMember.Where(x => x.Status.ToUpper() != "SUCCESS" && !x.CreatedBy.Contains("08") && x.IsDeleted == false && x.OTPExpiredDate > DateTime.Now); 
        }

        public static string GetByNonPhoneAndNonSuccessWhereID(long id)
        {
            string phone = "";
            var result = DataRepositoryFactory.CurrentRepository.PoolOTPMember.Where(x => x.ID == id && x.Status.ToUpper() != "SUCCESS" && !x.CreatedBy.Contains("08") && x.IsDeleted == false).Select(x => x.Phone);
            foreach (var item in result)
            {
                phone = item;
            }
            return phone;
        }
    }
}
