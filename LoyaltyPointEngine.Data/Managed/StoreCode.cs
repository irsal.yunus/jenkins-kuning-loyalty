﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Common;

namespace LoyaltyPointEngine.Data
{
    public partial class StoreCode
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<StoreCode>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.StoreCode,
                    EventLogLogic.LogEventCode.Add,
                    "StoreCode", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<StoreCode>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.StoreCode,
                    EventLogLogic.LogEventCode.Edit,
                    "StoreCode", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<StoreCode>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.StoreCode,
                    EventLogLogic.LogEventCode.Delete,
                    "StoreCode", this, Url, By);

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<StoreCode> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.StoreCode.Where(x => !x.IsDeleted);
        }

        public static IQueryable<StoreCode> GetActive()
        {
            return GetAll().Where(x => x.IsActive);
        }

        public static StoreCode GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.StoreCode.FirstOrDefault(x => !x.IsDeleted && x.ID == ID);
        }

        public static StoreCode GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.StoreCode.FirstOrDefault(x => !x.IsDeleted && x.Code == code);
        }

        public static StoreCode GetByCodeNotSelf(int ID, string Code)
        {
            return DataRepositoryFactory.CurrentRepository.StoreCode.FirstOrDefault(x => x.Code == Code && x.ID != ID);
        }

        public static bool IsExist(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.StoreCode.Any(x => !x.IsDeleted && x.ID == ID && x.IsActive);
        }
    }
}
