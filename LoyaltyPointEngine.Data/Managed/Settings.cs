﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial  class Settings
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Settings>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Settings,
                    EventLogLogic.LogEventCode.Add,
                    "Settings", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Settings>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Settings,
                    EventLogLogic.LogEventCode.Edit,
                    "Settings", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<Settings>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Settings,
                    EventLogLogic.LogEventCode.Delete,
                    "Settings", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<Settings> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Settings.Where(x => !x.IsDeleted);
        }
        public static IQueryable<Settings> GetAllByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.Settings.Where(x => !x.IsDeleted && x.Code == code);
        }
        public static Settings GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Settings.FirstOrDefault(x => x.ID == ID);
        }

        public static Settings GetByKey(string key)
        {
            return DataRepositoryFactory.CurrentRepository.Settings.FirstOrDefault(x => x.Key == key);
        }

        public static Settings GetByKeyNotSelf(string key, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Settings.FirstOrDefault(x => x.Key == key && x.ID != ID);
        }

        public static string GetValueByKey(string Key)
        {
            var data= DataRepositoryFactory.CurrentRepository.Settings.FirstOrDefault(x => x.Key == Key);
            if(data != null)
            {
                return data.Value;
            }

            return "";
        }

       

    }

   
}
