﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Pool
    {
        public static IQueryable<Pool> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Pool;
        }

        public static Pool GetByClientId(string ClientId)
        {
            return DataRepositoryFactory.CurrentRepository.Pool.FirstOrDefault(x => x.ClientId == ClientId);
        }


    }
}
