﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class ReceiptImage
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.CreatedByID = UserId;
                this.Save<ReceiptImage>();
                model.AdditionalInfo = this.ID.ToString();
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdatedByID = UserId;
                this.UpdateSave<ReceiptImage>();

            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse HardDeleted()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Delete<ReceiptImage>();

            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;

        }


        public static IQueryable<ReceiptImage> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ReceiptImage;
        }

        public static IQueryable<ReceiptImage> GetAllByReceiptID(Guid ReceiptID)
        {
            return GetAll().Where(x => x.ReceiptID == ReceiptID);
        }




    }
}
