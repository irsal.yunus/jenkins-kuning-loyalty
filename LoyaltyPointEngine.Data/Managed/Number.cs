﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Number
    {
        public enum Type
        {
            /// <summary>
            /// Receipt Code
            /// </summary>
            RCP = 1,

            /// <summary>
            /// MEMBER
            /// </summary>
            MEMB=2,

            /// <summary>
            /// ADJUSTMENT
            /// </summary>
            ADJ =3,



        }

        public static string Generated(Type type, string by)
        {
            string result = "";
            var currentNumber = GetCurrentNumberByType(type);
            if (currentNumber == null)
            {
                currentNumber = new Number();
                currentNumber.Insert(by, type);
            }
            else
            {
                currentNumber.Update(by);
            }

            string year = currentNumber.Year.ToString();
            string month = String.Format("{0:00}", currentNumber.Month);

            return String.Format("{1}{2}{0:0000000}", currentNumber.Number1, year, month);
        }

        public static Number GetCurrentNumberByType(Type type)
        {
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            string _type = type.ToString();
            return DataRepositoryFactory.CurrentRepository.Number.FirstOrDefault(x => x.Month == month && x.Year == year && x.NumberType == _type);
        }



        public EFResponse Insert(string By, Type type)
        {
            EFResponse model = new EFResponse();

            try
            {
                int month = DateTime.Now.Month;
                int year = DateTime.Now.Year;
                string _type = type.ToString();
                this.NumberType = _type;
                this.Year = year;
                this.Month = month;
                this.Number1 = 1;
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<Number>();

            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.Number1 = this.Number1 + 1;
                this.UpdateSave<Number>();

            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

    }
}
