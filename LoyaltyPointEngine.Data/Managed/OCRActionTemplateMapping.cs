﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class OCRActionTemplateMapping
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<OCRActionTemplateMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "OCRActionTemplateMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdateSave<OCRActionTemplateMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "OCRActionTemplateMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<OCRActionTemplateMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "OCRActionTemplateMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<OCRActionTemplateMapping> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.OCRActionTemplateMapping.Where(x => x.IsActive);
        }

        public static OCRActionTemplateMapping GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.OCRActionTemplateMapping.FirstOrDefault(x => x.ID == ID);
        }

        public static IQueryable<OCRActionTemplateMapping> GetByTemplateId(int templateId)
        {
            return DataRepositoryFactory.CurrentRepository.OCRActionTemplateMapping.Where(x => x.TemplateId == templateId);
        }

        public static OCRActionTemplateMapping GetByActionId(int actionId)
        {
            return DataRepositoryFactory.CurrentRepository.OCRActionTemplateMapping.FirstOrDefault(x => x.ActionId == actionId);
        }

    }
}
