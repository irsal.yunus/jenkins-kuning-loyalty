﻿using LoyaltyPointEngine.Common;
using System;

namespace LoyaltyPointEngine.Data
{
    public partial class AutomaticCampaignReceiptLog
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();
            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.Save<AutomaticCampaignReceiptLog>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "AutomaticCampaignReceiptLog", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }
            return model;
        }
    }
}
