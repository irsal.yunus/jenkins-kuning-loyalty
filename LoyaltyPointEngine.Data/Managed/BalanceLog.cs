﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class BalanceLog
    {


        public EFResponse Inserted(string By, int timeStamp, DateTime? createdDate = null)
        {
            EFResponse model = new EFResponse();

            try
            {
                if (createdDate.HasValue)
                {
                    this.CreatedDate = createdDate.Value;
                }
                else
                {
                    this.CreatedDate = DateTime.Now.AddSeconds(timeStamp);
                }
                this.CreatedBy = By;
                this.Save<BalanceLog>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Updated()
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateSave<BalanceLog>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<BalanceLog> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.BalanceLog;
        }

        public static int GetTotalPointByMemberId(int memberId)
        {
            var data = DataRepositoryFactory.CurrentRepository.GetTotalPointbyMember(memberId).FirstOrDefault();
            return data != null ? data.Value : 0;
        }

        public static IQueryable<BalanceLog> GetByMemberId(int memberId)
        {
            return DataRepositoryFactory.CurrentRepository.BalanceLog.Where(x => x.MemberID == memberId);
        }


        public static IQueryable<BalanceLog> GetByMemberIdAndCreatedDate(int memberId, DateTime? startdate, DateTime? enddate)
        {
            var result = DataRepositoryFactory.CurrentRepository.BalanceLog.Where(x => x.MemberID == memberId);

            if (startdate.HasValue && startdate.Value > DateTime.MinValue)
            {

                result = result.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(startdate.Value));
            }



            if (enddate.HasValue && enddate.Value > DateTime.MinValue)
            {

                result = result.Where(x => EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(enddate.Value));
            }

            return result;
        }

        public static BalanceLog GetLastBalanceByMemberId(int memberId)
        {
            return DataRepositoryFactory.CurrentRepository.BalanceLog.Where(x => x.MemberID == memberId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }


        public static IQueryable<BalanceLog> GetByStartDateAndEndDate(DateTime startdate, DateTime enddate)
        {
            return DataRepositoryFactory.CurrentRepository.BalanceLog.Where(x => (EntityFunctions.TruncateTime(x.CreatedDate) >= EntityFunctions.TruncateTime(startdate) &&
                EntityFunctions.TruncateTime(x.CreatedDate) <= EntityFunctions.TruncateTime(enddate)));
        }

        public static ObjectResult<GetHistoryBalanceLog_Result> GetHistoryBalanceLog(int memberId, DateTime? startdate, DateTime? enddate)
        {
            return DataRepositoryFactory.CurrentRepository.GetHistoryBalanceLog(memberId, startdate, enddate);
        }

        public static BalanceLog GetById(Guid Id)
        {
            return DataRepositoryFactory.CurrentRepository.BalanceLog.Where(x => x.ID.Equals(Id)).FirstOrDefault();
        }
    }
}
