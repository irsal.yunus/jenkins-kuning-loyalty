﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Receipt
    {

        public enum ReceiptStatus
        {
            Waiting = 0,
            Approve = 1,
            Reject = -1,
            Unverify = -2, //Waiting for Member Unverify account
            OcrReject = -3,
            WaitingUnverify = 2,
            WaitingVerify = 3,
            OcrProcessing = 4,
            OcrApproved = 5,
        }

        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.CreatedByID = UserId;
                if(this.Status==(int)Receipt.ReceiptStatus.Reject && this.RejectReason == null)
                {
                    throw new ArgumentNullException("RejectReason");
                }
                this.Save<Receipt>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Add,
                    "Receipt", this, Url, By);
                model.AdditionalInfo = this.ID.ToString();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse UpdateReceipt(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdatedByID = UserId;
                if (this.Status == (int)Receipt.ReceiptStatus.Reject && this.RejectReason == null)
                {
                    throw new ArgumentNullException("RejectReason");
                }
                this.UpdateSave<Receipt>();
                //EventLog.InsertEventLog(UserId,
                //    EventLogLogic.LogType.Information,
                //    EventLogLogic.LogSource.Receipt,
                //    EventLogLogic.LogEventCode.Edit,
                //    "Receipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdatedByID = UserId;
                if (this.Status == (int)Receipt.ReceiptStatus.Reject && this.RejectReason == null)
                {
                    throw new ArgumentNullException("RejectReason");
                }
                this.UpdateSave<Retailer>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Receipt,
                    EventLogLogic.LogEventCode.Edit,
                    "Receipt", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<Receipt> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Receipt;
        }

        public static IQueryable<Receipt> GetAllPerMonth()
        {
            int batasBulan = DateTime.Now.AddMonths(-1).Month;
            return GetAll().Where(x => x.CreatedDate.Month >= batasBulan);
        }

        public static Receipt GetById(Guid ID)
        {
            return DataRepositoryFactory.CurrentRepository.Receipt.FirstOrDefault(x => x.ID == ID);
        }

        public static Receipt GetByCode(string Code)
        {
            return GetAll().FirstOrDefault(x => x.Code.ToLower() == Code.ToLower());
        }

        public static IQueryable<Receipt> GetByReceiptCode(string ReceiptCode)
        {
            return GetAll().Where(x => x.ReceiptCode != null
            && x.ReceiptCode.ToLower() == ReceiptCode.ToLower()
            && x.Status != -1
            );
        }

        public static IQueryable<Receipt> GetByReceiptCodeNotSelf(string ReceiptCode, Guid ID)
        {
            return GetAll().Where(x => x.ReceiptCode != null && x.ReceiptCode.ToLower() == ReceiptCode.ToLower() && x.ID != ID);
        }

        public static IQueryable<Receipt> GetByMemberId(int MemberId)
        {
            return DataRepositoryFactory.CurrentRepository.Receipt.Where(x => x.MemberID == MemberId);
        }

        public static IQueryable<Receipt> GetByMemberIdAndStatusAndTransDate(int MemberId, int? status, bool? IsReupload, DateTime? startdate, DateTime? enddate)
        {
            var result = status.HasValue ? DataRepositoryFactory.CurrentRepository.Receipt.Where(x => x.MemberID == MemberId && x.Status == status.Value) :
                DataRepositoryFactory.CurrentRepository.Receipt.Where(x => x.MemberID == MemberId);

            if (IsReupload.HasValue)
            {
                result = result.Where(x => x.IsNeedReuploadData == IsReupload.Value);
            }

            if (startdate.HasValue && startdate.Value > DateTime.MinValue)
            {

                result = result.Where(x => EntityFunctions.TruncateTime(x.TransactionDate) >= EntityFunctions.TruncateTime(startdate.Value));
            }



            if (enddate.HasValue && enddate.Value > DateTime.MinValue)
            {

                result = result.Where(x => EntityFunctions.TruncateTime(x.TransactionDate) <= EntityFunctions.TruncateTime(enddate.Value));
            }

            return result;
        }

        public static List<GetLatestProductApproved_Result> getLatestUploadedProduct(int memberId)
        {

            var data = DataRepositoryFactory.CurrentRepository.GetLatestProductApproved(memberId).ToList();

            return data;
        }

        public static Receipt GetByMemberRetailerDateAndAddressWithOutReceiptCode(int MemberId, int RetailerID, DateTime transactionDate, string address)
        {
            address = address == null ? "" : address;
            return GetAll().FirstOrDefault(x => (x.ReceiptCode == null || x.ReceiptCode == "") && x.MemberID == MemberId && x.RetailerID == RetailerID
                && x.TransactionDate == transactionDate
                && x.RetailerAddress.ToLower() == address.ToLower()
                && x.Status != -1
                );
        }

        public static Receipt GetByMemberRetailerDateAddressWithOutReceiptCodeNotSelf(int MemberId, int RetailerID, DateTime transactionDate, string address, Guid ID, int? totalPrice = null)
        {
            address = address == null ? "" : address;
            return GetAll().FirstOrDefault(x => (x.ReceiptCode == null || x.ReceiptCode == "")
                && x.MemberID == MemberId
                && x.RetailerID == RetailerID
                && x.TransactionDate == transactionDate
                && x.RetailerAddress.ToLower() == address.ToLower()
                && x.ID != ID
                && (x.Status != (int)ReceiptStatus.Reject || x.Status != (int)ReceiptStatus.OcrReject)
                && x.TotalPrice == totalPrice);
        }

        public void Inserted(string by)
        {
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = by;
            this.UpdatedDate = DateTime.Now;
            this.InsertSave<Receipt>();
        }

        //public Data.Member _CurrentMember;
        //public Data.Member CurrentMember
        //{
        //    get
        //    {
        //        return Data.Member.GetById(this.MemberID);
        //    }
        //    set
        //    {
        //        _CurrentMember = Data.Member.GetById(this.MemberID);
        //    }
        //}

        public static int CheckTotalDuplicateReceipt(int memberID, DateTime? transactionDate = null, int? retailerID = null, List<int> listActionID = null)
        {
            int result = 0;
            if (memberID == 0)
                return 0;
            if (transactionDate < new DateTime(1900, 1, 1))
                transactionDate = null;
            if (retailerID == 0)
                retailerID = null;
            string arrActionID = string.Empty;
            if (listActionID != null)
            {
                foreach (var item in listActionID)
                {
                    if (item > 0)
                    {
                        if (string.IsNullOrEmpty(arrActionID))
                            arrActionID = item.ToString();
                        else
                            arrActionID = arrActionID + "," + item.ToString();
                    }
                }
            }

            var res = DataRepositoryFactory.CurrentRepository.SP_CheckTotalDuplicateReceipt((int?)memberID, transactionDate, retailerID, arrActionID);
            if (res != null)
            {
                result = res.Select(x => x.Value).FirstOrDefault();
            }
            return result;
        }

        public static List<string> GetInactiveMemberReceipt()
        {
            var data = from a in DataRepositoryFactory.CurrentRepository.GetInactiveMemberReceipt()
                       select (a.ID);
            return data.ToList();
        }
    }
}
