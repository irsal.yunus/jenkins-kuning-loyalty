﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data.Managed
{
    public static class SettingExtention
    {
        public static string GetValue(this List<Data.Settings> param, string Key)
        {
            var data = param.FirstOrDefault(x => x.Key == Key);
            if (data != null)
            {
                return data.Value;
            }

            return "";
        }
    }
}
