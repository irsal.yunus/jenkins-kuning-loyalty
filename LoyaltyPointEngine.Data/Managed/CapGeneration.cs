﻿using LoyaltyPointEngine.Common;
using System;
using System.Linq;

namespace LoyaltyPointEngine.Data
{
    public partial class CapGeneration
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<CapGeneration>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CapGeneration,
                    EventLogLogic.LogEventCode.Add,
                    "CapGeneration", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdateSave<CapGeneration>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CapGeneration,
                    EventLogLogic.LogEventCode.Edit,
                    "CapGeneration", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }


        public static IQueryable<CapGeneration> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CapGeneration;
        }

        public static CapGeneration GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.CapGeneration.FirstOrDefault(x => x.ID == ID);
        }

        public static IQueryable<CapGeneration> GetByMemberID(int MemberID)
        {
            return DataRepositoryFactory.CurrentRepository.CapGeneration.Where(x => x.MemberID == MemberID);
        }
    }
}
