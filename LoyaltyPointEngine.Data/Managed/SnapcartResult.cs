﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class SnapcartResult
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.Save<SnapcartResult>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "SnapcartResult", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<SnapcartResult> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.SnapcartResult.AsQueryable();
        }

        public static SnapcartResult GetByCorrelationId(Guid correlationId)
        {
            return GetAll().Where(x => x.CorrelationID == correlationId).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }
    }
}
