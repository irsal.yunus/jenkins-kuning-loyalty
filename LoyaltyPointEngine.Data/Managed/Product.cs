﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Product
    {

        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Product>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Product>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }


        public static IQueryable<Product> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Product.Where(x => !x.IsDeleted);
        }

        public static Product GetByID(int id)
        {
            return DataRepositoryFactory.CurrentRepository.Product.FirstOrDefault(x => !x.IsDeleted && x.ID == id);
        }

        public static Product GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.Product.FirstOrDefault(x => !x.IsDeleted && x.Code.ToLower() == code.ToLower());
        }

        public static Product GetByCodeNotSelf(string code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Product.FirstOrDefault(x => !x.IsDeleted && x.Code.ToLower() == code.ToLower() && x.ID != ID);
        }
        public static IQueryable<Product> GetAllByGroupID(int groupid)
        {
            return DataRepositoryFactory.CurrentRepository.Product.Where(x => !x.IsDeleted && x.ProductGroupID == groupid);
        }

    }
}
