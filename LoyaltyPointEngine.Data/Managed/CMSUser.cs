﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class CMSUser
    {

        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            CMSUser userCheckUsernameAvailability = CMSUser.GetByUsername(this.Username);
            if (userCheckUsernameAvailability != null)
            {
                model.ErrorEntity = "Username";
                model.ErrorMessage = "Username already exist";
                model.Success = false;
                return model;
            }

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<CMSUser>();
                LoyaltyPointEngine.Data.EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Add,
                    "CMSUser", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.IsDeleted = false;
                this.UpdateSave<CMSUser>();
                LoyaltyPointEngine.Data.EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Edit,
                    "CMSUser", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CMSUser>();
                LoyaltyPointEngine.Data.EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Delete,
                    "CMSUser", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<CMSUser> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CMSUser
                .Where(x => !x.IsDeleted);
        }

        public static CMSUser GetByID(long ID)
        {
            IQueryable<CMSUser> res = GetAll().Where(x => x.ID == ID);
            return res.FirstOrDefault();
        }

        public static IQueryable<CMSUser> GetByEmail(string Email)
        {
            return GetAll().Where(x => x.Email == Email);
        }

        public static CMSUser GetByUsername(string Username)
        {
            IQueryable<CMSUser> res = GetAll().Where(x => x.Username.ToLower() == Username.ToLower());
            return res.FirstOrDefault();
        }

        public static CMSUser GetByUsernameAndPassword(string Username, string Password)
        {
            IQueryable<CMSUser> res = GetAll().Where(x => x.Username.ToLower() == Username.ToLower() && x.Password == Password);
            return res.FirstOrDefault();
        }

        public static CMSUser Get_ByEmail(string email)
        {
            return DataRepositoryFactory.CurrentRepository.CMSUser.FirstOrDefault(x => !x.IsDeleted && x.Email != "" && x.Email.ToLower() == email.ToLower());
        }

        public static CMSUser GetByEmailExceptID(int Id, string email)
        {
            return DataRepositoryFactory.CurrentRepository.CMSUser.FirstOrDefault(x => !x.IsDeleted && x.ID != Id && x.Email != "" && x.Email.ToLower() == email.ToLower());
        }

        public static CMSUser GetUserActiveByUsername(string Username)
        {
            IQueryable<CMSUser> res = GetAll().Where(x => x.IsActive == true && x.Username != null && x.Username.ToLower() == Username.ToLower());

            return res.FirstOrDefault();
        }

        public static CMSUser GetUserActiveByUsernameAndPassword(string Username, string Password)
        {
            IQueryable<CMSUser> res = GetAll().Where(x => x.IsActive == true && x.Username != null && x.Username.ToLower() == Username.ToLower() && x.Password == Password);
            return res.FirstOrDefault();
        }

        public static IQueryable<CMSUser> GetUserAdminSuperAdmin()
        {
            return GetAll().Where(x => x.CMSRole.Name == "administrator" || x.CMSRole.Name == "super admin");
        }
    }
}
