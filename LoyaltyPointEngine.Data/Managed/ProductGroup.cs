﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class ProductGroup
    {
        public EFResponse Insert(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<ProductGroup>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<ProductGroup>();

            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }


        public static IQueryable<ProductGroup> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ProductGroup.Where(x => !x.IsDeleted);
        }

        public static ProductGroup GetByID(int id)
        {
            return DataRepositoryFactory.CurrentRepository.ProductGroup.FirstOrDefault(x => !x.IsDeleted && x.ID == id);
        }

        public static ProductGroup GetByCode(string code)
        {
            return DataRepositoryFactory.CurrentRepository.ProductGroup.FirstOrDefault(x => !x.IsDeleted && x.ProductGroupCode.ToLower() == code.ToLower());
        }

        public static ProductGroup GetByCodeNotSelf(string code, int ID)
        {
            return DataRepositoryFactory.CurrentRepository.ProductGroup.FirstOrDefault(x => !x.IsDeleted && x.ProductGroupCode.ToLower() == code.ToLower() && x.ID != ID);
        }
    }
}
