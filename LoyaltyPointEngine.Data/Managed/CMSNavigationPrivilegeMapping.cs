﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class CMSNavigationPrivilegeMapping
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = this.CreatedDate = DateTime.Now;
                this.UpdateBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.IsActive = true;
                this.Save<CMSNavigationPrivilegeMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Add,
                    "CMSNavigationPrivilegeMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdateDate = DateTime.Now;
                this.UpdateBy = By;
                this.IsDeleted = false;
                this.UpdateSave<CMSNavigationPrivilegeMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Edit,
                    "CMSNavigationPrivilegeMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<CMSNavigationPrivilegeMapping>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CMSUser,
                    EventLogLogic.LogEventCode.Delete,
                    "CMSNavigationPrivilegeMapping", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<CMSNavigationPrivilegeMapping> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CMSNavigationPrivilegeMapping
                .Where(x => !x.IsDeleted);
        }

        public static CMSNavigationPrivilegeMapping GetByID(long ID)
        {
            IQueryable<CMSNavigationPrivilegeMapping> res = GetAll().Where(x => x.ID == ID);
            return res.FirstOrDefault();
        }

        public static IQueryable<CMSNavigationPrivilegeMapping> GetByNavigation(long CMSNavigationID)
        {
            IQueryable<CMSNavigationPrivilegeMapping> res = CMSNavigationPrivilegeMapping.GetAll()
                .Where(x => x.CMSNavigationID == CMSNavigationID && x.IsActive);

            return res;
        }

        public static CMSNavigationPrivilegeMapping GetByNavigationAndPrevilage(long CMSNavigationID, long PrevilageID)
        {
            CMSNavigationPrivilegeMapping res = CMSNavigationPrivilegeMapping.GetAll()
                .Where(x => x.CMSNavigationID == CMSNavigationID && x.CMSPrivilegeID == PrevilageID).FirstOrDefault();
            return res;
        }
    }
}
