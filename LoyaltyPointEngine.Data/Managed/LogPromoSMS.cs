﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class LogPromoSMS
    {
        public EFResponse Insert(string Phone, string Message)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.Phone = (Phone != null) ? Phone.Trim() : Phone;
                this.Message = Message;
                this.CreatedDate = DateTime.Now;
                this.Save<LogPromoSMS>();
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse TruncateTable()
        {
            EFResponse model = new EFResponse();

            try
            {
                int res = DataRepositoryFactory.CurrentRepository.LogPromoSMS_TruncateFunction();
                model.Success = true;
            }
            catch (Exception e)
            {
                model.ErrorEntity = (e.InnerException != null) ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
                throw e;
            }

            return model;
        }
        public static IQueryable<LogPromoSMS> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.LogPromoSMS;
        }
    }
}
