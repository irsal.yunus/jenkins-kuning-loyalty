﻿using LoyaltyPointEngine.Common;
using System;
using System.Linq;

namespace LoyaltyPointEngine.Data
{
    public partial class CapActionPoint
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.Save<CapActionPoint>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CapGeneration,
                    EventLogLogic.LogEventCode.Add,
                    "CapActionPoint", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.UpdateSave<CapActionPoint>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.CapGeneration,
                    EventLogLogic.LogEventCode.Edit,
                    "CapActionPoint", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }


        public static IQueryable<CapActionPoint> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.CapActionPoint;
        }

        public static CapActionPoint GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.CapActionPoint.FirstOrDefault(x => x.ID == ID);
        }

        public static IQueryable<CapActionPoint> GetByMemberID(int MemberID)
            {
                return DataRepositoryFactory.CurrentRepository.CapActionPoint.Where(x => x.MemberID == MemberID);
            }
        
    }
}
