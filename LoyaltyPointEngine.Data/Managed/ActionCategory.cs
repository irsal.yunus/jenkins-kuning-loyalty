﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{   
    public partial class ActionCategory
    {
        public enum CategoryType
        {
            PRODUCT = 1,
            FB = 2,
            GLOBAL = 3
        }

        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = this.CreatedDate = DateTime.Now;
                this.UpdatedBy = this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<ActionCategory>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Add,
                    "ActionCategory", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<ActionCategory>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Edit,
                    "ActionCategory", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<ActionCategory>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.Action,
                    EventLogLogic.LogEventCode.Delete,
                    "ActionCategory", this, Url, By);
            }
            catch (Exception e)
            {
                  model.ErrorEntity = e.InnerException != null?  e.InnerException.ToString():e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }
        public static IQueryable<ActionCategory> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.ActionCategory.Where(x => !x.IsDeleted);
        }

        public static ActionCategory GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.ActionCategory.FirstOrDefault(x => x.ID == ID);
        }

        public static ActionCategory GetByCode(string Code)
        {
            return DataRepositoryFactory.CurrentRepository.ActionCategory.FirstOrDefault(x => x.Code == Code);
        }

        public static ActionCategory GetByCodeNotSelf(string Code , int ID)
        {
            return DataRepositoryFactory.CurrentRepository.ActionCategory.FirstOrDefault(x => x.Code == Code && x.ID != ID);
        }

    }
}
