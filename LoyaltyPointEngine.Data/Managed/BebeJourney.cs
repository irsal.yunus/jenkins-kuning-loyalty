﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Common;

namespace LoyaltyPointEngine.Data
{
    public partial class BebeJourney
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<BebeJourney>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.StoreCode,
                    EventLogLogic.LogEventCode.Add,
                    "BebeJouney", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<BebeJourney> GetPagination(int page, int size)
        {
            return DataRepositoryFactory.CurrentRepository.BebeJourney.OrderByDescending(x => x.CreatedDate).Skip(page * size).Take(size);
        }

        public static int CountRow()
        {
            return DataRepositoryFactory.CurrentRepository.BebeJourney.Count();
        }
    }
}
