﻿using LoyaltyPointEngine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.Data
{
    public partial class Cluster
    {
        public EFResponse Insert(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.CreatedDate = DateTime.Now;
                this.CreatedBy = By;
                this.IsDeleted = false;
                this.Save<Cluster>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Add,
                    "Cluster", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Update(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.UpdatedDate = DateTime.Now;
                this.UpdatedBy = By;
                this.IsDeleted = false;
                this.UpdateSave<Cluster>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Edit,
                    "Cluster", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public EFResponse Delete(string By, string Url, long? UserId)
        {
            EFResponse model = new EFResponse();

            try
            {
                this.IsDeleted = true;
                this.UpdateSave<Cluster>();
                EventLog.InsertEventLog(UserId,
                    EventLogLogic.LogType.Information,
                    EventLogLogic.LogSource.AreaAndRegion,
                    EventLogLogic.LogEventCode.Delete,
                    "Cluster", this, Url, By);
            }
            catch (Exception e)
            {
                model.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                model.ErrorMessage = e.Message;
                model.Success = false;
            }

            return model;
        }

        public static IQueryable<Cluster> GetAll()
        {
            return DataRepositoryFactory.CurrentRepository.Cluster.Where(x => !x.IsDeleted);
        }

        public static IQueryable<Cluster> GetActive()
        {
            return GetAll().Where(x => x.IsActive);
        }

        public static Cluster GetById(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Cluster.FirstOrDefault(x => x.ID == ID);
        }

        public static bool IsExist(int ID)
        {
            return DataRepositoryFactory.CurrentRepository.Cluster.Any(x => !x.IsDeleted && x.ID == ID && x.IsActive);
        }
    }
}
