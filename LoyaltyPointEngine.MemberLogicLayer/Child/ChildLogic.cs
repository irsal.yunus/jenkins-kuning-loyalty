﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter.Member;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class ChildLogic
    {
        public static ResultModel<ChildModel> Add(AddNewChildParameterModel param, string by)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();


            var child = new Child();
            var dataMember = Member.GetById(param.ParentID);
            if (dataMember == null)
            {
                result.StatusCode = "40";
                result.StatusMessage = string.Format("Parent Id is not found in system");
                return result;
            }
            child.ParentID = dataMember.ID;


            if (string.IsNullOrEmpty(param.Gender))
            {
                result.StatusCode = "32";
                result.StatusMessage = string.Format("Child's Gender is required");
                return result;
            }
            else
            {
                if (param.Gender.ToLower() != "f" && param.Gender.ToLower() != "m")
                {
                    result.StatusCode = "35";
                    result.StatusMessage = string.Format("Child's  Gender format is wrong. Please use 'F' for Female and 'M' for Male");
                    return result;
                }
            }

            child.Gender = param.Gender.ToUpper();

            if (string.IsNullOrEmpty(param.Name))
            {
                result.StatusCode = "33";
                result.StatusMessage = string.Format("Child's Name is required");
                return result;
            }

            child.Name = param.Name;
            if (!string.IsNullOrEmpty(param.Birthdate))
            {
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(param.Birthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                {
                    child.Birthdate = dt;
                }
                else
                {
                    result.StatusCode = "30";
                    result.StatusMessage = string.Format("Format of Child's Birthdate is wrong");
                    return result;
                }
            }

            child.PlaceOfBirth = param.PlaceOfBirth;
            child.Inserted(by);
            result.Value = new ChildModel(child);
            result.StatusCode = "00";
            result.StatusMessage = string.Format("success add new child with ID {0}", child.ID);

            return result;
        }
        public static ResultModel<List<ChildModel>> Get(int ParentId)
        {
            ResultModel<List<ChildModel>> result = new ResultModel<List<ChildModel>>();

            var dataChilds = Child.GetByParentId(ParentId).ToList();
            if (dataChilds != null && dataChilds.Count > 0)
            {

                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get {0} child(s)", dataChilds.Count);

                //use this exted method to translate from object datalayer to model
                result.Value = dataChilds.Translated();
            }
            else
            {
                result.StatusCode = "40";
                result.StatusMessage = string.Format("Parent Id is not found in system");

            }

            return result;
        }

        public static ResultModel<ChildModel> Update(EditChildParameterModel param, string by)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();

            var child = Child.GetById(param.ID);
            if (child != null)
            {
                if (string.IsNullOrEmpty(param.Gender))
                {
                    result.StatusCode = "32";
                    result.StatusMessage = string.Format("Child's Gender is required");
                    return result;
                }
                else
                {
                    if (param.Gender.ToLower() != "f" && param.Gender.ToLower() != "m")
                    {
                        result.StatusCode = "35";
                        result.StatusMessage = string.Format("Child's  Gender format is wrong. Please use 'F' for Female and 'M' for Male");
                        return result;
                    }
                }

                child.Gender = param.Gender.ToUpper();

                if (string.IsNullOrEmpty(param.Name))
                {
                    result.StatusCode = "33";
                    result.StatusMessage = string.Format("Child's Name is required");
                    return result;
                }

                child.Name = param.Name;
                if (!string.IsNullOrEmpty(param.Birthdate))
                {
                    DateTime dt = DateTime.MinValue;
                    if (DateTime.TryParseExact(param.Birthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                    {
                        child.Birthdate = dt;
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.StatusMessage = string.Format("Format of Child's Birthdate is wrong");
                        return result;
                    }
                }

                child.PlaceOfBirth = param.PlaceOfBirth;
                child.Updated(by);
                result.Value = new ChildModel(child);
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success update data child with ID {0}", child.ID);
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id is not found in system");
            }
            return result;
        }


        public static ResultModel<ChildModel> Delete(int ID, string by)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();
            var child = Child.GetById(ID);
            if (child != null)
            {
                child.Deleted(by);
                result.Value = new ChildModel(child);
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success deleted data child with ID {0}", child.ID);

            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id is not found in system");
            }
            return result;

        }

        public static ResultModel<ChildModel> CollectCount(int ID)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();
            try
            {
                var child = Child.GetById(ID);
                if (child != null)
                {
                    var collectCheck = Data.Collect.CollectCountByChildID(child.ID);
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("This child has {0} collects", collectCheck);
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = "Id is not found in system";
                }
            }
            catch (Exception e)
            {
                result.StatusCode = "500";
                result.StatusMessage = e.InnerException != null ? e.InnerException.ToString() : e.Message;
            }

            return result;
        }

        public static ResultModel<ChildModel> DeleteByCollect(int ID, string by)
        {
            ResultModel<ChildModel> result = new ResultModel<ChildModel>();
            try
            {
                var child = Child.GetById(ID);
                if (child != null)
                {
                    var collectCheck = Data.Collect.CollectCountByChildID(child.ID);
                    if (collectCheck == 0)
                    {
                        var check = child.Deleted(by);
                        if (check.Success)
                        {
                            result.Value = new ChildModel(child);
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("success deleted data child with ID {0} and Name {1}", child.ID, child.Name);
                        }
                        else
                        {
                            result.Value = new ChildModel(child);
                            result.StatusCode = "500";
                            result.StatusMessage = check.ErrorMessage;
                        }

                    }
                    else
                    {
                        result.Value = new ChildModel(child);
                        result.StatusCode = "500";
                        result.StatusMessage = "User can't delete child's data with collect.";
                    }
                }
                else
                {
                    result.StatusCode = "500";
                    result.StatusMessage = "Id is not found in system";
                }
            }
            catch (Exception e)
            {
                result.StatusCode = "500";
                result.StatusMessage = e.InnerException != null ? e.InnerException.ToString() : e.Message;
            }

            return result;

        }

        public static ResultModel<int> ChildsCountByMemberID(int MemberID)
        {
            ResultModel<int> result = new ResultModel<int>();
            try
            {
                result.StatusCode = "00";
                result.StatusMessage = "Success get member childs count.";
                result.Value = Child.GetByParentId(MemberID).Count();
            }
            catch (Exception e)
            {
                result.StatusCode = "500";
                result.StatusMessage = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                result.Value = 0;
            }

            return result;
        } 

    }
}
