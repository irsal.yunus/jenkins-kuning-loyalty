﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.Common.Notification;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.WishList;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoyaltyPointEngine.MemberLogicLayer.WishList
{
    public class WishListLogic
    {
        private static LoyaltyPointEngine.DataMember.EFResponse dbResponse;
        public static ResultModel<IEnumerable<WishListModel>> GetActiveWishList()
        {
            string accessToken = Encrypt.LoyalEncript(string.Format("{0}#{1}", SiteSetting.Rewards_ClientId, SiteSetting.Rewards_ClientSecret));
            return RESTHelper.Get<ResultModel<IEnumerable<WishListModel>>>(string.Format("{0}?CLIENTID={1}", SiteSetting.Rewards_WishListEndPoint, SiteSetting.Rewards_ClientId), accessToken);
        }

        public static ResultModel<bool> NotifyMember(out int totalNotified)
        {
            totalNotified = 0;
            ResultModel<bool> result = new ResultModel<bool>();
            ResultModel<IEnumerable<WishListModel>> activeWishListResponse = GetActiveWishList();
            if (activeWishListResponse.StatusCode != "00")
            {
                result.StatusCode = activeWishListResponse.StatusCode;
                result.StatusMessage = activeWishListResponse.StatusMessage;
                result.Value = false;
                return result;
            }
            foreach (var item in activeWishListResponse.Value)
            {
                DataMember.Member member = DataMember.Member.GetById(item.MemberID);
                if (member != null)
                {
                    int memberPoint = BalanceLog.GetTotalPointByMemberId(member.ID);
                    string message = "";
                    if (memberPoint > SiteSetting.Rewards_WishListTreshold)
                    {
                        if (memberPoint > item.Catalogue.Points)
                        {
                            message = string.Format("Point Anda cukup untuk redeem hadiah ini {0}", item.Catalogue.Name);
                        }
                        else
                        {
                            message = string.Format("Point Anda belum cukup untuk redeem hadiah ini {0}", item.Catalogue.Name);
                        }
                        string title = "Informasi Point";
                        var responseNotif = NotificationLogic.Create(member.ID, Common.Notification.NotificationType.Type.Information, title, message, "", memberPoint, member.FirstName);
                       
                        if (!dbResponse.Success)
                        {
                            result.StatusCode = "50";
                            result.StatusMessage = dbResponse.ErrorEntity + "\n" + dbResponse.ErrorMessage;
                            result.Value = false;
                            return result;
                        }
                        totalNotified++;
                    }
                }
            }
            result.StatusCode = "00";
            result.StatusMessage = "Success notify wishlist to member";
            result.Value = true;
            return result;
        }
    }
}
