﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.EventCode;
using LoyaltyPointEngine.Model.Parameter.EventCode;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class EventCodeLogic
    {
        public static ResultModel<EventCodeModel> AddEventCode(EventCodeModel param, int id, string name, string url)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();

            var data = EventCode.GetByCode(param.Code);
            if (data == null)
            {
                data = new EventCode();
                data.Code = param.Code;
                data.StartDate = param.StartDate;
                data.EndDate = param.EndDate;
                data.Insert(name, url, id);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("can not add Event Code with duplicated Code", param.Code);
            }
            
            return result;
        }

        public static ResultModel<EventCodeModel> GetEventCodeById(int ID)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();
            var dataToko = EventCode.GetById(ID);
            if (dataToko != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Event Code with ID {0}", ID);
                result.Value = Translate(dataToko);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Event with ID {0} not found in system", ID);
            }

            return result;
        }

        public static ResultModel<EventCodeModel> GetEventCodeByCode(string code)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();
            var dataToko = EventCode.GetByCode(code);
            if (dataToko != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Event Code with code {0}", code);
                result.Value = Translate(dataToko);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Event with ID {0} not found in system", code);
            }

            return result;
        }

        public static ResultModel<EventCodeModel> GetEventCodeByCodeNotSelfId(int id, string code)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();
            var dataToko = EventCode.GetByCodeNotSelf(id, code);
            if (dataToko != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Event Code with code {0}", code);
                result.Value = Translate(dataToko);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Event with ID {0} not found in system", code);
            }



            return result;
        }

        public static ResultModel<EventCodeModel> UpdateEventCode(EventCodeModel param, int id, string name, string url)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();

            var dataToko = EventCode.GetById(param.ID);
            if (dataToko != null)
            {

                var checkCode = EventCode.GetByCode(param.Code);
                if (checkCode != null && checkCode.ID != param.ID)
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("can not add Event Code with duplicated Code", param.Code);
                }
                else
                {
                    dataToko.Code = param.Code;
                    dataToko.StartDate = param.StartDate;
                    dataToko.EndDate = param.EndDate;
                    dataToko.Update(name, url, id);

                    result.Value = Translate(dataToko);
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success update Event Code with ID {0}", dataToko.ID);
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id {0} not found in system", param.ID);
            }


            return result;
        }

        public static ResultModel<EventCodeModel> DeleteEventCode(EventCodeModel param, int id, string name, string url)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();

            var dataToko = EventCode.GetById(param.ID);
            if (dataToko != null)
            {
                dataToko.Code = param.Code;
                dataToko.StartDate = param.StartDate;
                dataToko.EndDate = param.EndDate;
                dataToko.Delete(name, url, id);

                result.Value = Translate(dataToko);
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success Delete Event Code with ID {0}", dataToko.ID);
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id {0} not found in system", param.ID);
            }


            return result;
        }

        public static ResultModel<List<EventCodeModel>> GetAllEventCodeMobile()
        {
            ResultModel<List<EventCodeModel>> result = new ResultModel<List<EventCodeModel>>();
            List<EventCodeModel> listEventCode = new List<EventCodeModel>();
            var data = EventCode.GetAll();
            if (data != null)
            {
                foreach (var items in data)
                {
                    if (DateTime.Now >= items.StartDate && DateTime.Now <= items.EndDate)
                    {
                        EventCodeModel model = new EventCodeModel();
                        model.ID = items.ID;
                        model.Code = items.Code;
                        model.StartDate = items.StartDate;
                        model.EndDate = items.EndDate;
                        listEventCode.Add(model);
                    }
                }

                if (listEventCode.Count == 0)
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Event Code not found";
                }
                else
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Success Get Event Code";
                    result.Value = listEventCode;
                }

                return result;
            }

            result.StatusCode = "50";
            result.StatusMessage = "Event Code not found";
            return result;
        }

        private static ResultModel<EventCodeModel> ValidateAddModel(EventCodeModel param)
        {
            ResultModel<EventCodeModel> result = new ResultModel<EventCodeModel>();
            if (string.IsNullOrEmpty(param.Code))
            {
                result.StatusCode = "20";
                result.StatusMessage = "Code is required";
                return result;
            }

            if (param.StartDate == null && param.EndDate == null)
            {
                result.StatusCode = "21";
                result.StatusMessage = "Date is required";
                return result;
            }

            result.StatusCode = "00";
            result.StatusMessage = "Validation Success";
            return result;
        }

        private static EventCodeModel Translate(EventCode param)
        {
            EventCodeModel model = new EventCodeModel();
            model.ID = param.ID;
            model.Code = param.Code;
            model.StartDate = param.StartDate;
            model.EndDate = param.EndDate;
            return model;
        }
    }
}
