﻿using System.Collections.Generic;
using System.Linq;
using LoyaltyPointEngine.Data;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.StoreCode;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class StoreCodeLogic
    {
        public static ResultModel<StoreCodeModel> AddStoreCode(StoreCodeModel param, int id, string name, string url)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();

            var validate = ValidateAddModel(param);

            if (validate.StatusCode == "00")
            {
                var dataToko = StoreCode.GetByCode(param.Code);
                if (dataToko == null)
                {
                    dataToko = new StoreCode();
                    dataToko.Code = param.Code;
                    dataToko.ClusterId = param.ClusterId;
                    dataToko.Name = param.Name;
                    dataToko.IsActive = param.IsActive;
                    dataToko.Insert(name, url, id);
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("can not add Store Code with duplicated Code", param.Code);
                }
            }

            return validate;
        }

        public static ResultModel<StoreCodeModel> GetStoreCodeById(int ID)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();
            var dataToko = StoreCode.GetById(ID);
            if (dataToko != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Store Code with ID {0}", ID);
                result.Value = Translate(dataToko);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Store with ID {0} not found in system", ID);
            }

            return result;
        }

        public static ResultModel<StoreCodeModel> GetStoreCodeByCode(string code)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();
            var dataToko = StoreCode.GetByCode(code);
            if (dataToko != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Store Code with code {0}", code);
                result.Value = Translate(dataToko);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Store with ID {0} not found in system", code);
            }
            
            return result;
        }

        public static ResultModel<StoreCodeModel> GetStoreCodeByCodeNotSelfId(int id, string code)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();
            var dataToko = StoreCode.GetByCodeNotSelf(id, code);
            if (dataToko != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Store Code with code {0}", code);
                result.Value = Translate(dataToko);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Store with ID {0} not found in system", code);
            }



            return result;
        }

        public static ResultModel<StoreCodeModel> UpdateStoreCode(StoreCodeModel param, int id, string name, string url)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();

            var dataToko = StoreCode.GetById(param.ID);
            if (dataToko != null)
            {

                var checkCode = StoreCode.GetByCode(param.Code);
                if (checkCode != null && checkCode.ID != param.ID)
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("can not add Store Code with duplicated Code", param.Code);
                }
                else
                {
                    dataToko.Code = param.Code;
                    dataToko.Name = param.Name;
                    dataToko.ClusterId = param.ClusterId;
                    dataToko.IsActive = param.IsActive;
                    dataToko.Update(name, url, id);

                    result.Value = Translate(dataToko);
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success update Store Code with ID {0}", dataToko.ID);
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id {0} not found in system", param.ID);
            }


            return result;
        }

        public static ResultModel<StoreCodeModel> DeleteStoreCode(StoreCodeModel param, int id, string name, string url)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();

            var dataToko = StoreCode.GetById(param.ID);
            if (dataToko != null)
            {
                dataToko.Code = param.Code;
                dataToko.Name = param.Name;
                dataToko.ClusterId = param.ClusterId;
                dataToko.IsActive = param.IsActive;
                dataToko.Delete(name, url, id);

                result.Value = Translate(dataToko);
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success Delete Store Code with ID {0}", dataToko.ID);
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id {0} not found in system", param.ID);
            }


            return result;
        }

        public static IQueryable<StoreCodeModel> GetAllStoreCode()
        {
            List<StoreCodeModel> listStoreCode = new List<StoreCodeModel>();
            var data = StoreCode.GetAll();
            foreach (var items in data)
            {
                StoreCodeModel model = new StoreCodeModel();
                model.ID = items.ID;
                model.ClusterId = items.ClusterId;
                model.Code = items.Code;
                model.Name = items.Name;
                model.IsActive = items.IsActive;
                listStoreCode.Add(model);
            }
            
            return listStoreCode.AsQueryable();
        }

        private static ResultModel<StoreCodeModel> ValidateAddModel(StoreCodeModel param)
        {
            ResultModel<StoreCodeModel> result = new ResultModel<StoreCodeModel>();
            if (string.IsNullOrEmpty(param.Code))
            {
                result.StatusCode = "20";
                result.StatusMessage = string.Format("Code is required");
                return result;
            }

            if (string.IsNullOrEmpty(param.Name))
            {
                result.StatusCode = "21";
                result.StatusMessage = string.Format("Name is required");
                return result;
            }

            result.StatusCode = "00";
            result.StatusMessage = string.Format("Validation Success");
            return result;
        }

        private static StoreCodeModel Translate(StoreCode param)
        {
            StoreCodeModel model = new StoreCodeModel();
            model.ID = param.ID;
            model.ClusterId = param.ClusterId;
            model.Code = param.Code;
            model.Name = param.Name;
            model.IsActive = param.IsActive;
            return model;
        }
    }
}
