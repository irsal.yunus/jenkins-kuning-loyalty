﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using LoyaltyPointEngine.Model.Parameter.Member;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class SalesPersonLogic
    {

        public static ResultPaginationModel<List<SalesPersonModel>> GetSalesPersonModelByPagination(ParamPagination param)
        {
            ResultPaginationModel<List<SalesPersonModel>> result = new ResultPaginationModel<List<SalesPersonModel>>();

            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var spgLinq = SPG.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        spgLinq = spgLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        spgLinq = spgLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        spgLinq = spgLinq.Where(x => x.Code.Contains(param.Search) || x.Name.Contains(param.Search));
                        total = SPG.GetAll().Count(x => x.Code.Contains(param.Search) || x.Name.Contains(param.Search));
                    }
                    else
                    {
                        total = SPG.GetAll().Count();
                    }


                    var salesPersons = spgLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} SalesPerson(s)", salesPersons.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = salesPersons.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<SalesPersonModel> GetSalesPersonModelById(int ID)
        {
            ResultModel<SalesPersonModel> result = new ResultModel<SalesPersonModel>();
            var dataSPG = DataMember.SPG.GetById(ID);
            if (dataSPG != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get SPG with ID {0}", ID);
                result.Value = new SalesPersonModel(dataSPG);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("SPG with ID {0} not found in system", ID);
            }



            return result;
        }

        public static ResultModel<SalesPersonModel> Add(AddSalesPersonParameterModel param, string by)
        {
            ResultModel<SalesPersonModel> result = new ResultModel<SalesPersonModel>();

            if (string.IsNullOrEmpty(param.Code))
            {
                result.StatusCode = "20";
                result.StatusMessage = string.Format("Code is required");
                return result;
            }

            if (string.IsNullOrEmpty(param.Name))
            {
                result.StatusCode = "21";
                result.StatusMessage = string.Format("Name is required");
                return result;
            }

            var dataSPG = DataMember.SPG.GetByCode(param.Code);
            if (dataSPG == null)
            {
                dataSPG = new DataMember.SPG();
                dataSPG.Code = param.Code;
                dataSPG.Name = param.Name;
                dataSPG.OutletName = param.OutletName;
                dataSPG.LeaderName = param.LeaderName;
                dataSPG.Inserted(by);
                result.Value = new SalesPersonModel(dataSPG);
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success add new SPG with ID {0}", dataSPG.ID);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("can not add SPG with duplicated Code", param.Code);
            }


            return result;
        }

        public static ResultModel<SalesPersonModel> UpdateSPGBySPGModel(SalesPersonModel param, string by)
        {
            ResultModel<SalesPersonModel> result = new ResultModel<SalesPersonModel>();

            var dataSPG = SPG.GetById(param.ID);
            if (dataSPG != null)
            {

                var checkCode = SPG.GetByCode(param.Code);
                if (checkCode != null && checkCode.ID != param.ID)
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("can not add SPG with duplicated Code", param.Code);
                }
                else
                {
                    dataSPG.Code = param.Code;
                    dataSPG.Name = param.Name;
                    dataSPG.LeaderName = param.LeaderName;
                    dataSPG.OutletName = param.OutletName;
                    dataSPG.Updated(by);

                    result.Value = new SalesPersonModel(dataSPG);
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success update SPG with ID {0}", dataSPG.ID);
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Id {0} not found in system", param.ID);
            }


            return result;
        }

        public static ResultPaginationModel<List<SalesPersonModel>> PublicGetAll(string search, string orderBy, string orderDirection, int page = 1, int pageSize = 10)
        {
            ResultPaginationModel<List<SalesPersonModel>> result = new ResultPaginationModel<List<SalesPersonModel>>();
            try
            {
                orderBy = orderBy ?? "FirstName";
                orderDirection = orderDirection ?? "ASC";

                var rawSpg = SPG.GetAll()
                    .OrderBy(string.Format("{0} {1}", orderBy, orderDirection));

                if (!string.IsNullOrWhiteSpace(search) && !string.IsNullOrEmpty(search))
                {
                    rawSpg = rawSpg
                        .Where(x => x.Code.Contains(search) || x.Name.Contains(search) || x.OutletName.Contains(search) || x.LeaderName.Contains(search))
                        .OrderBy(string.Format("{0} {1}", orderBy, orderDirection));
                }

                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.TotalRow = rawSpg.Count();
                result.Value = rawSpg
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList()
                    .Translated();
            }
            catch (Exception ex)
            {
                result.StatusCode = "50";
                result.StatusMessage = ex.Message;
            }
            return result;
        }
    }
}
