﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class ProductBeforeLogic
    {

        public static ResultPaginationModel<List<ProductBeforeModel>> Search(ParamPagination param)
        {
            ResultPaginationModel<List<ProductBeforeModel>> result = new ResultPaginationModel<List<ProductBeforeModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var productBeforeLinq = ProductBefore.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        productBeforeLinq = productBeforeLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        productBeforeLinq = productBeforeLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        productBeforeLinq = productBeforeLinq.Where(x => x.Name.Contains(param.Search));
                        total = ProductBefore.GetAll().Count(x => x.Name.Contains(param.Search));
                    }
                    else
                    {
                        total = ProductBefore.GetAll().Count();
                    }


                    var productBefore = productBeforeLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} stage(s)", productBefore.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = productBefore.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<ProductBeforeModel> GetStageModelById(int ID)
        {
            ResultModel<ProductBeforeModel> result = new ResultModel<ProductBeforeModel>();
            var dataProductBefore = DataMember.ProductBefore.GetById(ID);
            if (dataProductBefore != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Product Before with ID {0}", ID);
                result.Value = new ProductBeforeModel(dataProductBefore);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Product Before with ID {0} not found in system", ID);
            }
            return result;
        }

        public static string GetLastProductBeforeByMemberId(int memberId)
        {
            var dataProductBefore = ProductBefore.GetLastProductBeforeByMemberId(memberId);
            return dataProductBefore;
        }
    }
}
