﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class StagesLogic
    {

        public static ResultPaginationModel<List<StagesModel>> Search(ParamPagination param)
        {
            ResultPaginationModel<List<StagesModel>> result = new ResultPaginationModel<List<StagesModel>>();


            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var stagesLinq = Stages.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        stagesLinq = stagesLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        stagesLinq = stagesLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        stagesLinq = stagesLinq.Where(x => x.Name.Contains(param.Search) || x.AttributeName.Contains(param.Search));
                        total = Stages.GetAll().Count(x => x.Name.Contains(param.Search) || x.AttributeName.Contains(param.Search));
                    }
                    else
                    {
                        total = Stages.GetAll().Count();
                    }


                    var stages = stagesLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} stage(s)", stages.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = stages.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<StagesModel> GetStageModelById(int ID)
        {
            ResultModel<StagesModel> result = new ResultModel<StagesModel>();
            var dataStage = DataMember.Stages.GetById(ID);
            if (dataStage != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get Stages with ID {0}", ID);
                result.Value = new StagesModel(dataStage);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Stages with ID {0} not found in system", ID);
            }



            return result;
        }

        public static ResultModel<List<StagesModel>> GetStageModel(int StagesID, Member dataMom)
        {
            ResultModel<List<StagesModel>> result = new ResultModel<List<StagesModel>>();
            #region Part Validasi
            if (dataMom == null)
            {
                result.StatusCode = "40";
                result.StatusMessage = "Data Member Not Found";
                return result;
            }
            if (StagesID == null || StagesID <= 1)
            {
                //hot fix buat stagenya null dari mobile
                var allStages = Stages.GetAll().Where(x => x.IsActive).ToList();
                result.Value = allStages.Translated();
                result.StatusCode = "00";
                result.StatusMessage = "Stage is null, but return all stages";
                return result;
            }
            if (dataMom.StagesID != StagesID)
            {
                //hot fix buat stagenya null dari mobile
                var allStages = Stages.GetAll().Where(x => x.IsActive).ToList();
                result.Value = allStages.Translated();
                result.StatusCode = "00";
                result.StatusMessage = "Stages is not match with Stage member return all stages";
                return result;
            }
            var dataStage = DataMember.Stages.GetById(dataMom.StagesID.HasValue ? dataMom.StagesID.Value : 7);
            if (dataStage == null)
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Stages with ID {0} not found in system", StagesID);
                //return result;
            }
            #endregion

            #region Part Flow
            var stages = Stages.GetAll().Where(x => x.IsActive).ToList();
            result.StatusCode = "00";
            result.Value = stages.Translated();
            if (dataStage.Name.ToLower() == "belum hamil")
            {
                result.Value = stages.Translated();
            }
            else if (dataStage.Name.ToLower() == "sedang hamil")
            {
                stages = stages.Where(x => x.Name.ToLower() != "belum hamil").ToList();
                result.Value = stages.Translated();
            }
            else if (dataStage.Name.ToLower() == "sedang hamil & memiliki anak" || dataStage.Name.ToLower() == "tidak hamil & memiliki anak")
            {
                stages = stages.Where(x => x.Name.ToLower() != "belum hamil" && x.Name.ToLower() != "sedang hamil").ToList();
                result.Value = stages.Translated();
            }
            #endregion

            return result;
        }
    }
}
