﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Parameter.Consent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.MemberLogicLayer.Consent
{
    public static class ConsentLogic
    {
        public static ResultModel<ConsentCookieMember> SaveCookieConsent(ConsentModel param)
        {
            ResultModel<ConsentCookieMember> result = new ResultModel<ConsentCookieMember>();
            result.StatusCode = "55";
            result.StatusMessage = "Failed";
            EFResponse response = new EFResponse();
            bool newDataCookie = false;
            string byName = string.Empty;
            var dataMember = Member.GetById(param.MemberId);
            var dataCookie = ConsentCookieMember.GetById(param.ID);
            if (param.MemberId != 0)
            {
                if (dataMember == null)
                {
                    result.StatusCode = "33";
                    result.StatusMessage = "Member with ID : " + param.MemberId + " is not found";
                    return result;
                }
                else
                {
                    byName = dataMember.FirstName;
                }
            }
            if (dataCookie == null)
            {
                newDataCookie = true;
            }

            try
            {
                if (newDataCookie)
                {
                    ConsentCookieMember data = new ConsentCookieMember();
                    data.ID = param.ID;
                    data.MemberID = param.MemberId;
                    data.IsAcceptCookie = param.IsAcceptCookie;
                    response = data.Inserted(byName);
                    result.Value = data;
                }
                else
                {
                    dataCookie.MemberID = param.MemberId;
                    dataCookie.IsAcceptCookie = param.IsAcceptCookie;
                    response = dataCookie.Updated(byName);
                    result.Value = dataCookie;
                }

                if (response.Success)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Data saved";
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;

        }
    }
}
