﻿using LoyaltyPointEngine.Common;
using LoyaltyPointEngine.Common.Helper;
using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Member;
using LoyaltyPointEngine.Model.Parameter;
using LoyaltyPointEngine.Model.Parameter.Member;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Security.Cryptography;
using System.Transactions;
using System.Data.Objects.SqlClient;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using LoyaltyPointEngine.Model.Parameter.UploadKK;
using LoyaltyPointEngine.Common.Log;
using System.Data.Objects;
using LoyaltyPointEngine.Model.Object.BebeJourney;
using LoyaltyPointEngine.Model.Object.OTP;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public enum ChannelType
    {
        web = 1,
        sms = 3,
        elina = 4,
        @event = 5,
        mobile = 9,
        alfamart = 8,
        alfamidi = 10,
        jdid = 12,
        smsothers = 11,
        woozin = 14,
        sms_promo = 27
    }

    public static class MemberLogic
    {
        /// <summary>
        /// Register new member to system. Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> Add(AddMemberParameterModel param, string by, string url = "", bool IsFromCMS = false)
        {
            ResultModel<Member> result = new ResultModel<Member>();
            try
            {
                var UniqueCode = IsFromCMS ? "" : Data.Number.Generated(Data.Number.Type.MEMB, by);
                string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
                string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);

                Stages stages = new Stages();
                string stagesName = "";
                if (param.StagesID.HasValue)
                {
                    stages = Stages.GetById(param.StagesID.Value);
                    if (stages != null)
                    {
                        stagesName = stages.Name;
                    }
                }

                Member dataMember = new Member();
                // change district id with point district id
                if (param.DistrictID.HasValue)
                    param.DistrictPointID = District.GetPointDistrictId(param.DistrictID.Value);
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    if (IsFromCMS)
                    {
                        //nothing todo
                    }
                    else
                    {
                        string err = "";
                        //insert IMAGE
                        param.ProfilePicture = InsertMemberImage(param.ProfilePicture, UniqueCode, MediaLocalImageDirectoryMember,
                            RelativePathMember, "ProfilePicture", out err);
                        if (!string.IsNullOrEmpty(err))
                        {
                            transScope.Dispose();
                            result.StatusCode = "500";
                            result.StatusMessage = err;
                            return result;
                        }

                        param.KTPImage = InsertMemberImage(param.KTPImage, UniqueCode, MediaLocalImageDirectoryMember,
                        RelativePathMember, "KTPImage", out err);
                        if (!string.IsNullOrEmpty(err))
                        {
                            transScope.Dispose();
                            result.StatusCode = "500";
                            result.StatusMessage = err;
                            return result;
                        }

                        param.KKImage = InsertMemberImage(param.KKImage, UniqueCode, MediaLocalImageDirectoryMember,
                        RelativePathMember, "KKImage", out err);
                        if (!string.IsNullOrEmpty(err))
                        {
                            transScope.Dispose();
                            result.StatusCode = "500";
                            result.StatusMessage = err;
                            return result;
                        }
                    }

                    //add Member
                    ResultModel<Member> res = AddMemberOnly(param, by, stagesName);
                    if (res.StatusCode == "00")
                    {
                        dataMember = res.Value;
                        ResultModel<bool> responseApprovalImage = new ResultModel<bool>();
                        responseApprovalImage.StatusCode = "00";

                        if (!string.IsNullOrEmpty(dataMember.KKImage) || !string.IsNullOrEmpty(dataMember.KTPImage) || !string.IsNullOrEmpty(dataMember.AkteImage))
                        {
                            AddApprovalImageParameterModel paramApprovalImage = new AddApprovalImageParameterModel();
                            paramApprovalImage.MemberID = dataMember.ID;
                            paramApprovalImage.KKImage = dataMember.KKImage;
                            paramApprovalImage.KTPImage = dataMember.KTPImage;
                            paramApprovalImage.AkteImage = dataMember.AkteImage;
                            responseApprovalImage = AddApprovalImage(paramApprovalImage, by);
                        }
                        if (responseApprovalImage.StatusCode == "00")
                        {
                            //add child
                            if (param.Childs != null && param.Childs.Count > 0)
                            {
                                var responseChild = AddChildOnly(param.Childs, by, dataMember.ID);
                                if (responseChild.StatusCode == "00")
                                {
                                    result.Value = dataMember;
                                    var efRespinse = AddMemberInDBPoint(dataMember, by, url);
                                    result.StatusCode = "00";
                                    result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);
                                }
                                else
                                {
                                    result.StatusCode = responseChild.StatusCode;
                                    result.StatusMessage = responseChild.StatusMessage;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(param.ChildBirthdate))
                                {
                                    DateTime dt = DateTime.MinValue;
                                    if (DateTime.TryParseExact(param.ChildBirthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                                    {
                                        Child firstChild = new Child();
                                        firstChild.Name = param.ChildName ?? string.Empty;
                                        firstChild.Birthdate = dt;
                                        firstChild.Gender = "F";
                                        firstChild.PlaceOfBirth = string.Empty;
                                        firstChild.Order = 0;
                                        firstChild.ParentID = dataMember.ID;
                                        firstChild.ProductBeforeID = param.ProductBeforeID;
                                        var childResponse = firstChild.Inserted(by);

                                        if (childResponse.Success)
                                        {
                                            result.StatusCode = "00";
                                            result.StatusMessage = string.Format("success add member child with member ID {0}", dataMember.ID);
                                            result.Value = res.Value;
                                        }
                                        else
                                        {
                                            result.StatusCode = "50";
                                            result.StatusMessage = childResponse.ErrorMessage;
                                        }
                                    }
                                    else
                                    {
                                        result.StatusCode = "10";
                                        result.StatusMessage = string.Format("Format Child's Birthdate is wrong");
                                        return result;
                                    }
                                }
                                if (stagesName.ToLower().IndexOf("memiliki anak") == -1)
                                {
                                    result.StatusCode = "00";
                                    result.Value = res.Value;
                                }
                            }
                        }
                        else
                        {
                            result.StatusCode = responseApprovalImage.StatusCode;
                            result.StatusMessage = responseApprovalImage.StatusMessage;
                        }

                        if (result.StatusCode != "00")
                        {
                            transScope.Dispose();
                        }
                        else
                        {
                            transScope.Complete();
                            transScope.Dispose();
                        }
                    }
                    else
                    {
                        result.StatusCode = res.StatusCode;
                        result.StatusMessage = res.StatusMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Register new member to system. Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> PublicAdd(PublicAddMemberModel param, Data.Pool pool)
        {
            ResultModel<Member> result = new ResultModel<Member>();

            #region Channel Validation
            Channel _channel = null;
            _channel = Channel.GetByCode(param.Channel);
            if (_channel == null)
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("Channel '{0}' not found in system.", param.Channel);
                return result;
            }
            #endregion

            #region FirstName Validation
            if (string.IsNullOrEmpty(param.FirstName) || string.IsNullOrWhiteSpace(param.FirstName))
            {
                result.StatusCode = "41";
                result.StatusMessage = "First name is required.";
                return result;
            }
            else
            {
                result.StatusCode = "41";
                if (param.FirstName.Length > 200)
                {
                    result.StatusMessage = "First name max length reached. The max length is 200.";
                    return result;
                }

                if (!Regex.IsMatch(param.FirstName, @"^[a-zA-Z]+$"))
                {
                    result.StatusMessage = "First name can not contains any characters except alphabets.";
                    return result;
                }
            }
            #endregion

            #region Phone Validation
            if (string.IsNullOrEmpty(param.Phone) || string.IsNullOrWhiteSpace(param.Phone))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Phone is required.";
                return result;
            }
            else
            {
                result.StatusCode = "41";
                if (!PhoneHelper.isPhoneValid(param.Phone))
                {
                    result.StatusMessage = "Phone number is invalid.";
                    return result;
                }

                if (Member.IsExist(param.Phone))
                {
                    result.StatusMessage = "Phone number is already registered.";
                    return result;
                }
            }
            #endregion

            #region Child Birthdate Validation
            DateTime _childBirthDate = DateTime.MinValue;
            if (string.IsNullOrEmpty(param.ChildBirthDate) || string.IsNullOrWhiteSpace(param.ChildBirthDate))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Child BirthDate is required.";
                return result;
            }
            else
            {
                result.StatusCode = "41";
                if (!DateTime.TryParseExact(param.ChildBirthDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out _childBirthDate))
                {
                    result.StatusMessage = "Child BirthDate is invalid. Use this format instead \"dd/MM/yyyy\".";
                    return result;
                }
                if (_childBirthDate > DateTime.Now)
                {
                    result.StatusMessage = "Child BirthDate is invalid. Child Date can not be bigger dan today.";
                    return result;
                }
            }
            #endregion

            #region LastName Validation
            if (!string.IsNullOrEmpty(param.LastName) || !string.IsNullOrWhiteSpace(param.LastName))
            {
                result.StatusCode = "41";
                if (param.LastName.Length > 200)
                {
                    result.StatusMessage = "Last name max length reached. The max length is 200.";
                    return result;
                }

                if (!Regex.IsMatch(param.LastName, @"^[a-zA-Z]+$"))
                {
                    result.StatusMessage = "Last name can not contains any characters except alphabets.";
                    return result;
                }
            }
            #endregion

            #region Email Validation
            if (!string.IsNullOrEmpty(param.Email) || !string.IsNullOrWhiteSpace(param.Email))
            {
                result.StatusCode = "41";
                if (param.Email.Length > 200)
                {
                    result.StatusMessage = "Email max length reached. The max length is 200.";
                    return result;
                }

                if (!EmailHelper.IsValidEmail(param.Email))
                {
                    result.StatusMessage = "Email is invalid.";
                    return result;
                }
                if (Member.IsExist(param.Email))
                {
                    result.StatusMessage = "Email is already registered.";
                    return result;
                }
            }
            else
            {
                param.Email = string.Format("{0}@{0}.com", param.Phone);
            }
            #endregion

            #region Gender Validation
            if (string.IsNullOrEmpty(param.Gender) || string.IsNullOrWhiteSpace(param.Gender))
            {
                result.StatusCode = "41";
                result.StatusMessage = "Gender is required.";
                return result;
            }
            else
            {
                result.StatusCode = "41";
                param.Gender = param.Gender.Trim();
                if (param.Gender.ToLower() != "f" && param.Gender.ToLower() != "m" && param.Gender.ToLower() != "female" && param.Gender.ToLower() != "male")
                {
                    result.StatusMessage = "Invalid Gender value. The valid value must consists one of this opts [f|m|female|male]";
                    return result;
                }
                if (param.Gender.ToLower() == "female") param.Gender = "F";
                else if (param.Gender.ToLower() == "male") param.Gender = "M";
            }
            #endregion

            try
            {
                string newPassword = StringHelper.GeneratedPassword(),
                    encNewPassword = Encrypt.LoyalEncript(newPassword);

                var UniqueCode = Data.Number.Generated(Data.Number.Type.MEMB, pool.PoolName);

                Member newMember = new Member();
                var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                {
                    newMember.FirstName = param.FirstName;
                    newMember.LastName = param.LastName;
                    newMember.Phone = param.Phone;
                    newMember.Email = param.Email;
                    newMember.Gender = param.Gender;
                    newMember.Password = encNewPassword;
                    newMember.ChannelId = _channel.ID;
                    newMember.AcceptDate = DateTime.Now;
                    var dbResponse = newMember.Inserted(pool.PoolName);

                    if (dbResponse.Success)
                    {
                        Child newMemberChild = new Child();
                        newMemberChild.ParentID = newMember.ID;
                        newMemberChild.Name = "";
                        newMemberChild.Birthdate = _childBirthDate;
                        newMemberChild.Gender = "F";
                        newMemberChild.Order = 1;
                        newMemberChild.PlaceOfBirth = "";
                        dbResponse = newMemberChild.Inserted(pool.PoolName);

                        if (dbResponse.Success)
                        {
                            #region "Send SMS"
                            ResultModel<bool> resultSms = new ResultModel<bool>();
                            bool isSuccess = false;
                            string templateText = SiteSetting.ForgotPasswordSmsText;
                            string channelToLixus = SiteSetting.ChannelWeb;
                            string paramMessage = string.Format(SiteSetting.SuccessRegisterSmsText, newMember.Phone, newPassword);
                            string smsParam = string.Format(SiteSetting.FormatMessageLixus, paramMessage, newMember.Phone, channelToLixus);
                            string errSMSMsg = "";
                            LogSendType smsType = new LogSendType();
                            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                            smsType.Value = LogSendType.RegisterMember.Value + " " + textInfo.ToTitleCase(param.Channel);

                            var smsResponse = BlastSMS.SendSMSNotifOTP(smsType.Value, SiteSetting.ApiSendOTP, newMember.Phone, smsParam, newMember.Phone, SiteSetting.ApiSendOTP, 0, channelToLixus);
                            if (smsResponse != null)
                            {
                                if (!string.IsNullOrEmpty(smsResponse.Code))
                                {
                                    if (smsResponse.Code.ToString() == "7702")
                                        isSuccess = true;
                                    else
                                        errSMSMsg = SiteSetting.telcomnetAPIURLBebe + " " + smsResponse.Code;
                                }
                                else
                                {
                                    errSMSMsg = smsResponse.ToString();

                                    if (errSMSMsg.Contains("success"))
                                    {
                                        isSuccess = true;
                                    }
                                }
                            }
                            else
                                errSMSMsg = "No response from sms API";
                            #endregion

                            if (!isSuccess)
                            {
                                transScope.Dispose();
                                result.StatusCode = "50";
                                result.StatusMessage = "Failed when sending message to Member.";
                            }
                            else
                            {
                                transScope.Complete();
                                transScope.Dispose();

                                if (param.Channel.ToLower() == "woozin")
                                {
                                    #region 1 Million Participations
                                    var model1million = new Model.Parameter.Participations.ParticipationsModel();
                                    model1million.NameUser = param.FirstName + " " + param.LastName;
                                    model1million.Phone = param.Phone;
                                    model1million.ChildDOB = _childBirthDate.ToString("dd/MM/yyyy");
                                    model1million.Channel = "Event";
                                    model1million.UserId = pool.ID;
                                    model1million.Description = "Insert by Register channel Woozin with Member ID" + newMember.ID.ToString();
                                    model1million.By = pool.PoolName;

                                    var million = RESTHelper.Post<ResultModel<Model.Object.Participations.ResultParticipationsModel>>(Data.Settings.GetValueByKey("RewardsAPI") + "/Participations/AddParticipationsJson", model1million);
                                    #endregion
                                }

                                AddMemberInDBPoint(newMember, string.Format("{0} {1}", newMember.FirstName, newMember.LastName).Trim(), "");
                                result.StatusCode = "00";
                                result.StatusMessage = "Success adding new Member.";
                                result.Value = newMember;
                            }
                        }
                        else
                        {
                            transScope.Dispose();
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format("An error occured while inserting new Member Child. \n{0} \n{1}", dbResponse.ErrorMessage, dbResponse.ErrorEntity);
                        }
                    }
                    else
                    {
                        transScope.Dispose();
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("An error occured while inserting new Member. \n{0} \n{1}", dbResponse.ErrorMessage, dbResponse.ErrorEntity);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }
        public static ResultModel<bool> IsExist(string phoneOrEmail)
        {
            bool isExist = Member.IsExist(phoneOrEmail);
            if (phoneOrEmail.Contains('@'))
            {
                return new ResultModel<bool>
                {
                    StatusCode = isExist ? "00" : "50",
                    StatusMessage = isExist ? "Email is already registered." : "Email is available.",
                    Value = isExist
                };
            }
            else
            {
                return new ResultModel<bool>
                {
                    StatusCode = isExist ? "00" : "50",
                    StatusMessage = isExist ? "Phone is already registered." : "Phone is available.",
                    Value = isExist
                };
            }
        }

        public static ResultModel<List<string>> IsExistWithChildName(string phoneOrEmail)
        {
            Member isExist = new Member();
            List<Child> childObj = new List<Child>();
            List<string> ChildName = new List<string>();
            if (phoneOrEmail.Contains('@'))
            {
                isExist = Member.GetByEmail(phoneOrEmail);
            }
            else
            {
                isExist = Member.GetByPhoneNew(phoneOrEmail);
            }

            if (phoneOrEmail.Contains('@'))
            {
                isExist = Member.GetByEmail(phoneOrEmail);
                //childObj = isExist.Child.FirstOrDefault();
                childObj = isExist.Child.Where(x => x.IsDeleted == false).ToList();
                foreach (var item in childObj)
                {
                    ChildName.Add(item.Name);
                }

                return new ResultModel<List<string>>
                {
                    StatusCode = isExist != null ? "00" : "50",
                    StatusMessage = isExist != null ? "Email is already registered." : "Email is available.",
                    //Value = childObj != null ? string.IsNullOrEmpty(childObj.Name) ? "nama anak kosong" : childObj.Name : "nama anak kosong."
                    Value = ChildName
                };
            }
            else
            {
                isExist = Member.GetByPhoneNew(phoneOrEmail);
                if (isExist != null)
                {
                    childObj = isExist.Child.Where(x => x.IsDeleted == false).ToList();
                    foreach (var item in childObj)
                    {
                        ChildName.Add(item.Name);
                    }
                }

                return new ResultModel<List<string>>
                {
                    StatusCode = isExist != null ? "00" : "50",
                    StatusMessage = isExist != null ? "Phone is already registered." : "Phone is available.",
                    //Value = childObj != null ? string.IsNullOrEmpty(childObj.Name) ? "nama anak kosong" : childObj.Name : "nama anak kosong."
                    Value = ChildName
                };
            }
        }

        /// <summary>
        /// Register new member to system. With Sort Register Parameter Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> AddSortRegister(MemberRegisterModelWithUtm param, string by, string url = "")
        {
            ResultModel<Member> result = new ResultModel<Member>();

            var UniqueCode = Data.Number.Generated(Data.Number.Type.MEMB, by);
            string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
            string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);

            string err = "";

            AddMemberParameterModel objectTobeValidate = new AddMemberParameterModel();
            objectTobeValidate.FirstName = param.Firstname;
            objectTobeValidate.LastName = param.Lastname;
            objectTobeValidate.Email = param.Email;
            objectTobeValidate.Phone = param.Phone;
            objectTobeValidate.Password = param.Password;
            objectTobeValidate.StagesID = param.StagesID;
            objectTobeValidate.ChildBirthdate = param.ChildDOB;
            objectTobeValidate.ChildName = param.ChildName;
            objectTobeValidate.AgePregnant = param.AgePregnant;

            Channel dataChannel = Channel.GetByCode(ChannelType.web.ToString());
            if (!string.IsNullOrEmpty(param.Channel))
            {
                var channel = Channel.GetByCode(param.Channel);
                if (channel != null)
                {
                    dataChannel = channel;
                }
            }

            SPG spgBySpgCode = null;
            int? spgDistrictId = null;
            string eventCode = string.Empty;
            if (!string.IsNullOrEmpty(param.SpgCode))
            {
                //if (param.SpgCode.Length == 3)
                //    spgBySpgCode = SPG.GetByCode(param.SpgCode);
                //else
                eventCode = param.SpgCode;
            }

            //sisipin buat ponta
            if (!string.IsNullOrEmpty(eventCode))
            {
                if (eventCode.ToLower().Contains("ponta"))
                {
                    dataChannel = Channel.GetByCode(ChannelType.alfamart.ToString());

                }
            }

            //var spgBySpgCode = SPG.GetByCode(param.SpgCode);
            if (spgBySpgCode != null)
            {
                //    result.StatusCode = "50";
                //    result.StatusMessage = string.Format("Kode spg {0} tidak dikenali, anda akan di registrasikan tanpa Kode SPG, silahkan hubungi {1} untuk informasi lebih lanjut", param.SpgCode, SiteSetting.CareLineNumber);
                //    return result;
                int districtId = 0;
                if (!string.IsNullOrEmpty(spgBySpgCode.District) && int.TryParse(spgBySpgCode.District.Trim(), out districtId))
                {
                    spgDistrictId = districtId;
                }
            }

            #region Part Validasi Register
            var resSortRegisterModel = _validateSortRegister(objectTobeValidate);
            if (resSortRegisterModel.StatusCode != "00")
                return resSortRegisterModel;

            var Pregnancy = _pregnant(param.StagesID, param.ProductBeforeID.HasValue ? param.ProductBeforeID.Value : 0);
            var ValRegister = _validatePregnancy(objectTobeValidate, Pregnancy);
            if (ValRegister.StatusCode != "00")
                return ValRegister;
            #endregion

            //sisipin buat ponta
            //if (!string.IsNullOrEmpty(param.EventCode))
            //{
            //    if (param.EventCode.ToLower().Contains("ponta"))
            //    {
            //        dataChannel = Channel.GetByCode(ChannelType.alfamart.ToString());

            //    }
            //}

            if (dataChannel == null)
            {
                result.StatusCode = "29";
                result.StatusMessage = string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.web.ToString());
                return result;
            }

            string KKPict = null;
            if (!string.IsNullOrEmpty(param.KKPicture))
            {
                KKPict = InsertMemberImage(param.KKPicture, UniqueCode, MediaLocalImageDirectoryMember, RelativePathMember, "KKPicture", out err);
            }

            var dataMember = DataMember.Member.GetByPhoneOrEmail(param.Phone, param.Email);
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                //add Member
                try
                {
                    if (dataMember == null)
                    {
                        dataMember = new DataMember.Member();
                        dataMember.ChannelId = dataChannel.ID;
                        dataMember.FirstName = param.Firstname;
                        dataMember.LastName = param.Lastname;
                        dataMember.Gender = param.Gender;
                        dataMember.Email = param.Email;
                        dataMember.KKImage = KKPict;
                        dataMember.Phone = param.Phone;
                        dataMember.Password = Encrypt.LoyalEncript(param.Password);
                        dataMember.DistrictPointID = spgDistrictId;
                        dataMember.AcceptDate = DateTime.Now;
                        dataMember.StagesID = param.StagesID;

                        if (Pregnancy.Value && param.AgePregnant != null && param.AgePregnant >= 1)
                        {
                            dataMember.AgePregnant = param.AgePregnant > 43 ? 43 : param.AgePregnant;
                            if (Pregnancy.StatusCode == "60" || Pregnancy.StatusCode == "30") //sedang hamil/sdg hamil memiliki anak serta product utk hamil
                            {
                                dataMember.ProductBeforeID = param.ProductBeforeID;
                            }
                        }
                        if (spgBySpgCode != null)
                        {
                            dataMember.SpgID = spgBySpgCode.ID;
                        }

                        if (!string.IsNullOrEmpty(eventCode))
                        {
                            dataMember.EventCode = eventCode;
                        }

                        dataMember.Source = param.utm_source;
                        dataMember.TouchPoint = param.utm_medium;
                        dataMember.ActivityPoint = param.utm_campaign;

                        var res = dataMember.Inserted(by);

                        if (res.Success)
                        {
                            result.Value = dataMember;
                            result.StatusCode = "00";
                        }
                        else
                        {
                            result.Value = dataMember;
                            result.StatusCode = "500";
                            result.StatusMessage = string.Format("Failed add new member with ID {0}", dataMember.ID);
                        }

                        if (result.StatusCode == "00")
                        {
                            //if (!string.IsNullOrWhiteSpace(param.SpgCode) && spgBySpgCode == null)
                            //{
                            //    result.StatusMessage = string.Format("Kode spg {0} tidak dikenali, anda akan di registrasikan tanpa Kode SPG, silahkan hubungi {1} untuk informasi lebih lanjut", param.SpgCode, SiteSetting.CareLineNumber);
                            //}
                            //else
                            //{
                            //    result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);
                            //}

                            result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);
                            var efRespinse = AddMemberInDBPoint(dataMember, by, url);

                            if (efRespinse.Success)
                            {
                                result.Value = dataMember;
                                result.StatusCode = "00";
                            }
                            else
                            {
                                result.StatusMessage = string.Format("Failed add new memberpoint with ID {0}", dataMember.ID);
                            }

                            transScope.Complete();
                        }

                        transScope.Dispose();
                    }
                    else
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("can not add member with duplicated email or phone", param.Email);
                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }

            #region InsertChild
            try
            {
                if (result.StatusCode == "00")
                {
                    if (!string.IsNullOrEmpty(param.ChildDOB))
                    {
                        DateTime ChildBirhtDate = DateTime.MinValue;
                        param.ChildDOB = param.ChildDOB.Replace("/", "-");
                        DateTime.TryParseExact(param.ChildDOB, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ChildBirhtDate);
                        Child ch = new Child();
                        DateTime dt = DateTime.Now;
                        ch.Birthdate = ChildBirhtDate;
                        ch.ParentID = dataMember.ID;
                        ch.Name = param.ChildName;
                        ch.Order = 0;
                        ch.Gender = "f";
                        ch.PlaceOfBirth = "";
                        ch.ProductBeforeID = Pregnancy.StatusCode == "40" || Pregnancy.StatusCode == "65" ? param.ProductBeforeID : 0;
                        var res = ch.Inserted(by);
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            #endregion

            return result;
        }

        /// <summary>
        /// Register new member to system. With Sort Register Parameter Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> AddMobileFacebookRegister(MobileRegisterFacebookModel param, string by, string url = "")
        {
            ResultModel<Member> result = new ResultModel<Member>();

            Channel dataChannel = Channel.GetByCode(ChannelType.mobile.ToString());
            if (dataChannel == null)
            {
                result.StatusCode = "29";
                result.StatusMessage = string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.mobile.ToString());
                return result;
            }

            AddMemberParameterModel objectTobeValidate = new AddMemberParameterModel();
            objectTobeValidate.FirstName = param.Firstname;
            objectTobeValidate.LastName = param.Lastname;
            objectTobeValidate.FacebookID = param.FacebookID;

            var resRegisterModel = ValidateFacebookRegister(objectTobeValidate);
            if (resRegisterModel.StatusCode != "00")
                return resRegisterModel;

            var predictedMember = Member.GetByFacebookId(param.FacebookID.Trim());
            if (predictedMember != null)
            {
                result.StatusCode = "10";
                result.StatusMessage = "Facebook account is already in use.";
                return result;
            }

            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                //add Member
                try
                {
                    var dataMember = DataMember.Member.GetByFacebookId(param.FacebookID);
                    if (dataMember == null)
                    {
                        dataMember = new Member();
                        dataMember.ChannelId = dataChannel.ID;
                        dataMember.FirstName = param.Firstname;
                        dataMember.FacebookID = param.FacebookID;
                        dataMember.LastName = param.Lastname ?? string.Empty;
                        dataMember.Gender = param.Gender ?? string.Empty;
                        dataMember.Email = param.Email ?? string.Empty;
                        dataMember.Phone = string.Empty;
                        dataMember.Password = string.Empty;
                        dataMember.AcceptDate = DateTime.Now;

                        var res = dataMember.Inserted(by);
                        if (res.Success)
                        {
                            result.Value = dataMember;
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);

                            transScope.Complete();
                            transScope.Dispose();

                            if (result.StatusCode == "00")
                            {
                                var efRespinse = AddMemberInDBPoint(dataMember, by, url);
                            }
                        }
                        else
                        {
                            result.Value = dataMember;
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format(res.ErrorMessage);
                        }
                    }
                    else
                    {
                        result.StatusCode = "10";
                        result.StatusMessage = "Member already exist in database";
                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            return result;
        }

        /// <summary>
        /// Register new member to system. With Sort Register Parameter Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> AddMobileRegister(MobileRegisterModel param, string by, string url = "")
        {
            ResultModel<Member> result = new ResultModel<Member>();

            AddMemberParameterModel objectTobeValidate = new AddMemberParameterModel();
            objectTobeValidate.FirstName = param.Firstname;
            objectTobeValidate.LastName = param.Lastname;
            objectTobeValidate.Email = param.Email;
            objectTobeValidate.Phone = param.Phone;
            objectTobeValidate.Password = param.Password;
            objectTobeValidate.ChildName = param.ChildName;
            objectTobeValidate.ChildBirthdate = param.ChildBirthDate;
            objectTobeValidate.StagesID = param.StagesID;
            objectTobeValidate.AgePregnant = param.AgePregnant;

            SPG spgBySpgCode = null;
            string eventCode = string.Empty;
            int? spgDistrictId = null;
            if (!string.IsNullOrEmpty(param.SpgCode))
            {
                //if (param.SpgCode.Length == 3)
                //    spgBySpgCode = SPG.GetByCode(param.SpgCode);
                //else
                eventCode = param.SpgCode;
            }

            if (spgBySpgCode != null)
            {
                //    //result.StatusCode = "50";
                //    result.StatusMessage = string.Format("Kode spg {0} tidak dikenali, anda akan di registrasikan tanpa Kode SPG, silahkan hubungi {1} untuk informasi lebih lanjut", param.SpgCode, SiteSetting.CareLineNumber);
                //    //return result;
                int districtId = 0;
                if (!string.IsNullOrEmpty(spgBySpgCode.District) && int.TryParse(spgBySpgCode.District.Trim(), out districtId))
                {
                    spgDistrictId = districtId;
                }
            }

            //DateTime ChildBirhtDate;
            //if (string.IsNullOrEmpty(param.ChildBirthDate))
            //{
            //    result.StatusCode = "50";
            //    result.StatusMessage = string.Format("Silahkan isi tanggal lahir anak.");
            //    return result;
            //}
            //else
            //{
            //    if (!DateTime.TryParseExact(param.ChildBirthDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out ChildBirhtDate))
            //    {
            //        result.StatusCode = "50";
            //        result.StatusMessage = string.Format("Format tanggal lahir anak salah.");
            //        return result;
            //    }
            //}

            #region Part Validasi Register
            var resSortRegisterModel = _validateSortRegister(objectTobeValidate);
            if (resSortRegisterModel.StatusCode != "00")
                return resSortRegisterModel;

            var Pregnancy = _pregnant(param.StagesID, int.Parse(param.ProductBeforeID));
            var ValRegister = _validatePregnancy(objectTobeValidate, Pregnancy);
            if (ValRegister.StatusCode != "00")
                return ValRegister;
            #endregion

            var predictedMember = Member.GetByPhoneOrEmail(param.Phone.Trim(), param.Email.Trim());
            if (predictedMember != null)
            {
                result.StatusCode = "10";
                result.StatusMessage = "Phone or email is already in use.";
                return result;
            }

            Channel dataChannel = Channel.GetByCode(ChannelType.mobile.ToString());
            //sisipin buat ponta
            if (!string.IsNullOrEmpty(eventCode))
            {
                if (eventCode.ToLower().Contains("ponta"))
                {
                    dataChannel = Channel.GetByCode(ChannelType.alfamart.ToString());
                }
            }
            if (dataChannel == null)
            {
                result.StatusCode = "29";
                result.StatusMessage = string.Format("Channel Code '{0}' not found in system, please contact middleware administrator", ChannelType.mobile.ToString());
                return result;
            }

            string err = string.Empty,
                UniqueCode = Data.Number.Generated(Data.Number.Type.MEMB, by),
                MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember),
                RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember),
                newProfilePict = InsertMemberImage(param.ProfilePicture, UniqueCode, MediaLocalImageDirectoryMember, RelativePathMember, "ProfilePicture", out err);

            var dataMember = DataMember.Member.GetByPhoneOrEmail(param.Phone, param.Email);
            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                //add Member
                try
                {
                    if (dataMember == null)
                    {
                        dataMember = new DataMember.Member();
                        dataMember.ChannelId = dataChannel.ID;
                        dataMember.FirstName = param.Firstname;
                        dataMember.LastName = param.Lastname;
                        dataMember.Gender = param.Gender;
                        dataMember.Email = param.Email;
                        dataMember.Phone = param.Phone;
                        dataMember.Password = param.Password;
                        dataMember.ProfilePicture = newProfilePict;
                        dataMember.DistrictPointID = spgDistrictId;
                        dataMember.AcceptDate = DateTime.Now;
                        dataMember.StagesID = param.StagesID;

                        if (Pregnancy.Value && param.AgePregnant != null && param.AgePregnant >= 1)
                        {
                            dataMember.AgePregnant = param.AgePregnant > 43 ? 43 : param.AgePregnant;
                            if (Pregnancy.StatusCode == "60" || Pregnancy.StatusCode == "30") //sedang hamil/sdg hamil memiliki anak serta product utk hamil
                            {
                                dataMember.ProductBeforeID = int.Parse(param.ProductBeforeID);
                            }
                        }
                        if (spgBySpgCode != null)
                            dataMember.SpgID = spgBySpgCode.ID;
                        if (!string.IsNullOrEmpty(eventCode))
                            dataMember.EventCode = eventCode;
                        var res = dataMember.Inserted(by);

                        if (res.Success)
                        {
                            result.Value = dataMember;
                            result.StatusCode = "00";
                            //if (spgBySpgCode == null)
                            //{
                            //    result.StatusMessage = string.Format("Kode spg {0} tidak dikenali, anda akan di registrasikan tanpa Kode SPG, silahkan hubungi {1} untuk informasi lebih lanjut", param.SpgCode, SiteSetting.CareLineNumber);
                            //}
                            //else
                            //{
                            //    result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);
                            //}
                            result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);
                        }
                        else
                        {
                            result.Value = dataMember;
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format(res.ErrorMessage);
                        }

                        if (result.StatusCode == "00")
                        {
                            var efRespinse = AddMemberInDBPoint(dataMember, by, url);
                            if (efRespinse.Success)
                            {
                                result.Value = dataMember;
                                result.StatusCode = "00";
                            }
                            else
                            {
                                result.StatusMessage = string.Format("Failed add new memberpoint with ID {0}", dataMember.ID);
                            }

                            transScope.Complete();
                        }

                        transScope.Dispose();
                    }
                    else
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("can not add member with duplicated email or phone", param.Email);
                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }

            #region AddChild
            try
            {
                if (result.StatusCode == "00")
                {
                    if (!string.IsNullOrEmpty(param.ChildBirthDate))
                    {
                        DateTime ChildBirhtDate = DateTime.MinValue;
                        param.ChildBirthDate = param.ChildBirthDate.Replace("/", "-");
                        DateTime.TryParseExact(param.ChildBirthDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ChildBirhtDate);

                        if (ChildBirhtDate <= DateTime.MinValue)
                        {
                            result.StatusCode = "24";
                            result.StatusMessage = "Child Date is required";
                            return result;
                        }

                        Child firstChild = new Child();
                        firstChild.Birthdate = ChildBirhtDate;
                        firstChild.Name = param.ChildName;
                        firstChild.Order = 0;
                        firstChild.ParentID = dataMember.ID;
                        firstChild.PlaceOfBirth = string.Empty;
                        firstChild.Gender = "F";
                        firstChild.ProductBeforeID = Pregnancy.StatusCode == "40" || Pregnancy.StatusCode == "65" ? int.Parse(param.ProductBeforeID) : 0;
                        var childResponse = firstChild.Inserted(by);
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            #endregion

            return result;
        }

        /// <summary>
        /// Register new member only to system. Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> AddMemberOnly(AddMemberParameterModel param, string by, string stagesName)
        {
            ResultModel<Member> result = new ResultModel<Member>();
            try
            {
                Channel dataChannel = null;
                if (!string.IsNullOrEmpty(param.Channel))
                {
                    dataChannel = Channel.GetByCode(param.Channel);
                    if (dataChannel == null)
                    {
                        result.StatusCode = "29";
                        result.StatusMessage = string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.web.ToString());
                        return result;
                    }
                }

                if (stagesName.ToLower().IndexOf("belum hamil") == -1 && (param.ProductBeforeID == null || param.ProductBeforeID.Value <= 0))
                {
                    result.StatusCode = "30";
                    result.StatusMessage = "Product before must be selected";
                    return result;
                }

                //sisipin buat ponta
                if (!string.IsNullOrEmpty(param.EventCode))
                {
                    if (param.EventCode.ToLower().Contains("ponta"))
                    {
                        dataChannel = Channel.GetByCode(ChannelType.alfamart.ToString());

                    }
                }
                #region Part Validation
                var resSortRegisterModel = _validateSortRegister(param);
                if (resSortRegisterModel.StatusCode != "00") return resSortRegisterModel;

                var Pregnancy = _pregnant(param.StagesID.Value, param.ProductBeforeID.HasValue ? param.ProductBeforeID.Value : 0);
                var ValRegister = _validatePregnancy(param, Pregnancy);
                if (ValRegister.StatusCode != "00") return ValRegister;
                #endregion
                //if (string.IsNullOrEmpty(param.Gender))
                //{
                //    result.StatusCode = "22";
                //    result.StatusMessage = string.Format("Gender is required");
                //    return result;
                //}
                //else
                //{
                //    if (param.Gender.ToLower() != "f" && param.Gender.ToLower() != "m")
                //    {
                //        result.StatusCode = "25";
                //        result.StatusMessage = string.Format("Gender format is wrong. Please use 'F' for Female and 'M' for Male");
                //        return result;
                //    }
                //}

                var dataMember = DataMember.Member.GetByPhoneOrEmail(param.Phone, param.Email);
                if (dataMember == null)
                {
                    dataMember = new DataMember.Member();
                    if (dataChannel == null)
                        dataMember.ChannelId = Channel.GetByCode(Convert.ToString(ChannelType.web)).ID;
                    else
                        dataMember.ChannelId = dataChannel.ID;
                    dataMember.FacebookID = param.FacebookID;
                    dataMember.Gender = string.IsNullOrEmpty(param.Gender) ? "F" : param.Gender.ToUpper();
                    dataMember.FirstName = param.FirstName;
                    dataMember.LastName = param.LastName;
                    dataMember.Email = param.Email;
                    dataMember.Phone = param.Phone;
                    dataMember.Password = param.Password;
                    dataMember.StagesID = param.StagesID;
                    dataMember.StagesValue = param.StagesValue;
                    dataMember.Address = param.Address;
                    dataMember.ProvinceID = param.ProvinceID;
                    dataMember.DistrictID = param.DistrictID;
                    dataMember.SubDistrictID = param.SubDistrictID;
                    dataMember.SpgID = param.SpgID;
                    dataMember.KTPImage = param.KTPImage;
                    dataMember.KTPNumber = param.KTPNumber;
                    dataMember.KKImage = param.KKImage;
                    dataMember.KKNumber = param.KKNumber;
                    dataMember.AkteImage = param.AkteImage;
                    dataMember.ZipCode = param.ZipCode;
                    dataMember.Remarks = param.Remarks;
                    dataMember.EventCode = param.EventCode;
                    dataMember.IsNewsLetter = param.IsNewsLetter;
                    dataMember.DistrictPointID = param.DistrictPointID;
                    dataMember.AcceptDate = DateTime.Now;
                    dataMember.ProductBeforeID = Pregnancy.Value ? param.ProductBeforeID : 0;
                    dataMember.ESignature = param.ESignature;

                    if (param.AgePregnant > 0)
                    {
                        dataMember.AgePregnant = Pregnancy.Value ? (param.AgePregnant > 43 ? 43 : param.AgePregnant) : 0;
                    }

                    //dataMember.Consent = param.Consent;
                    //if (param.DistrictID == null && param.SpgID != null)
                    //{
                    //    dataMember.DistrictID = param.SpgID.Value;
                    //}

                    if (!string.IsNullOrEmpty(param.Birthdate))
                    {
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(param.Birthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                            dataMember.Birthdate = dt;
                        else
                        {
                            result.StatusCode = "10";
                            result.StatusMessage = string.Format("Format Birthdate is wrong");
                            return result;
                        }
                    }

                    dataMember.PlaceOfBirth = param.PlaceOfBirth;
                    dataMember.ProfilePicture = param.ProfilePicture;
                    var res = dataMember.Inserted(by);
                    if (res.Success)
                    {
                        result.Value = dataMember;
                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);
                    }
                    else
                    {

                        result.Value = dataMember;
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format(res.ErrorMessage);
                    }

                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("can not add member with duplicated email or phone", param.Email);
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Register new member to system. With Sort Register Parameter Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> AddFacebookRegister(MemberRegisterModel param, string by, string url = "")
        {
            ResultModel<Member> result = new ResultModel<Member>();

            Channel dataChannel = Channel.GetByCode(ChannelType.web.ToString());
            if (dataChannel == null)
            {
                result.StatusCode = "29";
                result.StatusMessage = string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.web.ToString());
                return result;
            }

            AddMemberParameterModel objectTobeValidate = new AddMemberParameterModel();
            objectTobeValidate.FirstName = param.Firstname;
            objectTobeValidate.LastName = param.Lastname;
            objectTobeValidate.FacebookID = param.FacebookID;

            var resRegisterModel = ValidateFacebookRegister(objectTobeValidate);
            if (resRegisterModel.StatusCode != "00") return resRegisterModel;


            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                //add Member
                try
                {
                    var dataMember = DataMember.Member.GetByFacebookId(param.FacebookID);
                    if (dataMember == null)
                    {
                        dataMember = new Member();
                        dataMember.ChannelId = dataChannel.ID;
                        dataMember.FirstName = param.Firstname;
                        dataMember.FacebookID = param.FacebookID;
                        dataMember.LastName = param.Lastname ?? string.Empty;
                        dataMember.Gender = param.Gender ?? string.Empty;
                        dataMember.Email = param.Email ?? string.Empty;
                        dataMember.Phone = param.Phone ?? string.Empty;
                        dataMember.Password = param.Password ?? string.Empty;
                        var res = dataMember.Inserted(by);
                        if (res.Success)
                        {
                            result.Value = dataMember;
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);

                            transScope.Complete();
                            transScope.Dispose();


                            if (result.StatusCode == "00")
                            {
                                var efRespinse = AddMemberInDBPoint(dataMember, by, url);
                            }


                        }
                        else
                        {

                            result.Value = dataMember;
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format(res.ErrorMessage);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(param.Password))
                        {
                            // dataMember.Password = param.Password;
                            var res = dataMember.Updated(by);
                            if (res.Success)
                            {
                                transScope.Complete();
                            }
                        }

                        transScope.Dispose();

                        result.StatusCode = "10";
                        result.StatusMessage = string.Format("Member {0} Already Registered", param.Firstname);


                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }




            return result;
        }

        private static ResultModel<Member> _validatePregnancy(AddMemberParameterModel param, ResultModel<bool> Pregnancy)
        {
            var result = new ResultModel<Member>();
            result.StatusCode = "00";
            //Pregnancy.StatusCode = 00 Insert ke Database
            //Pregnancy.StatusCode = 50 not Insert ke Database
            //Pregnancy.StatusCode = 30 perlu validasi Age Pregnant
            //Pregnancy.StatusCode = 40 perlu validasi data child
            //Pregnancy.StatusCode = 60 perlu validasi Age Pregnant n data child
            if (Pregnancy.StatusCode == "30" || Pregnancy.StatusCode == "60" || Pregnancy.StatusCode == "65")
            {
                if (param.AgePregnant < 1)
                {
                    result.StatusCode = "24";
                    result.StatusMessage = "Age Pregnant is required";
                    return result;
                }
            }
            if (Pregnancy.StatusCode == "40" || Pregnancy.StatusCode == "60" || Pregnancy.StatusCode == "65")
            {
                if (string.IsNullOrEmpty(param.ChildName))
                {
                    result.StatusCode = "24";
                    result.StatusMessage = "Child name is required";
                    return result;
                }
                if (string.IsNullOrEmpty(param.ChildBirthdate))
                {
                    result.StatusCode = "24";
                    result.StatusMessage = "Child Date is required";
                    return result;
                }
                else
                {
                    DateTime dateTimeChildBirthdate = DateTime.MinValue;
                    if (!DateTime.TryParseExact(param.ChildBirthdate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTimeChildBirthdate))
                    {
                        result.StatusCode = "24";
                        result.StatusMessage = "Format Child Date is wrong";
                        return result;
                    }
                    else
                    {
                        if (dateTimeChildBirthdate <= DateTime.MinValue)
                        {
                            result.StatusCode = "24";
                            result.StatusMessage = "Child Date is required";
                            return result;
                        }
                    }
                }
            }
            return result;
        }

        private static ResultModel<Member> _validateSortRegister(AddMemberParameterModel param)
        {
            ResultModel<Member> result = new ResultModel<Member>();
            result.StatusCode = "00";
            if (string.IsNullOrEmpty(param.Email))
            {
                //result.StatusCode = "20";
                //result.StatusMessage = string.Format("Email is required");
                return result;
            }
            else
            {
                if (!EmailHelper.IsValidEmail(param.Email))
                {
                    result.StatusCode = "26";
                    result.StatusMessage = string.Format("Format email is wrong");
                    return result;
                }
            }

            if (string.IsNullOrEmpty(param.Phone))
            {
                result.StatusCode = "21";
                result.StatusMessage = string.Format("Phone is required");
                return result;
            }
            else
            {
                if (!PhoneHelper.isPhoneValid(param.Phone))
                {
                    result.StatusCode = "27";
                    result.StatusMessage = string.Format("Format Phone is wrong");
                    return result;
                }
            }

            if (string.IsNullOrEmpty(param.FirstName))
            {
                result.StatusCode = "23";
                result.StatusMessage = string.Format("FirstName is required");
                return result;
            }

            if (string.IsNullOrEmpty(param.Password))
            {
                result.StatusCode = "24";
                result.StatusMessage = string.Format("Password is required");
                return result;
            }

            return result;
        }

        private static ResultModel<Member> ValidateFacebookRegister(AddMemberParameterModel param)
        {
            ResultModel<Member> result = new ResultModel<Member>();
            result.StatusCode = "00";

            if (string.IsNullOrEmpty(param.FirstName))
            {
                result.StatusCode = "23";
                result.StatusMessage = string.Format("FirstName is required");
                return result;
            }

            if (string.IsNullOrEmpty(param.FacebookID))
            {
                result.StatusCode = "25";
                result.StatusMessage = string.Format("Facebook ID is required");
                return result;
            }
            return result;
        }

        public static ResultModel<bool> AddApprovalImage(AddApprovalImageParameterModel param, string by)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            var approvalImage = new ApprovalImage();
            approvalImage.MemberID = param.MemberID;
            approvalImage.KKImage = param.KKImage;
            approvalImage.KTPImage = param.KTPImage;
            approvalImage.AkteImage = param.AkteImage;
            var res = approvalImage.Inserted(by);

            result.StatusCode = "00";
            result.StatusMessage = "success add Approval Image";

            if (!res.Success)
            {
                result.StatusCode = "500";
                result.StatusMessage = res.ErrorMessage;
            }


            return result;

        }

        /// <summary>
        /// Register new child only to system. Please encript password with SHA256 and then ecript value with MD5.
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<bool> AddChildOnly(List<AddChildParameterModel> Childs, string by, int MemberId)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                if (Childs != null && Childs.Count > 0)
                {
                    foreach (var item in Childs)
                    {
                        var child = new Child();
                        child.ParentID = MemberId;


                        if (string.IsNullOrEmpty(item.Gender))
                        {
                            result.StatusCode = "32";
                            result.StatusMessage = string.Format("Child's Gender is required");
                            return result;
                        }
                        else
                        {
                            if (item.Gender.ToLower() != "f" && item.Gender.ToLower() != "m")
                            {
                                result.StatusCode = "35";
                                result.StatusMessage = string.Format("Child's  Gender format is wrong. Please use 'F' for Female and 'M' for Male");
                                return result;
                            }
                        }

                        child.Gender = item.Gender.ToUpper();

                        if (string.IsNullOrEmpty(item.Name))
                        {
                            result.StatusCode = "33";
                            result.StatusMessage = string.Format("Child's Name is required");
                            return result;
                        }

                        child.Name = item.Name;
                        if (!string.IsNullOrEmpty(item.Birthdate))
                        {
                            DateTime dtChildDateOfBirth = DateTime.MinValue;
                            if (DateTime.TryParseExact(item.Birthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dtChildDateOfBirth))
                            {
                                //if (dtChildDateOfBirth.Year >= 2016)
                                //{
                                //    result.StatusCode = "30";
                                //    string mgd = string.Format("Maaf kode unik tdk dpt dikirimkan krn usia anak<1 th. Konsultasi seputar nutrisi&kesehatan di 08041112233 atau temukan 7KEHEBATAN PERUT di http://bit.ly/2nC56FH");
                                //    result.StatusMessage = string.Format(mgd);
                                //    return result;
                                //}

                                if (dtChildDateOfBirth <= DateTime.MinValue)
                                {
                                    result.StatusCode = "30";
                                    result.StatusMessage = string.Format("Child's Birthdate is required");
                                    return result;
                                }

                                child.Birthdate = dtChildDateOfBirth;
                            }
                            else
                            {
                                result.StatusCode = "30";
                                result.StatusMessage = string.Format("Format of Child's Birthdate is wrong");
                                return result;
                            }
                        }

                        child.PlaceOfBirth = item.PlaceOfBirth;
                        child.ProductBeforeID = item.ProductBeforeId;
                        child.Inserted(by);
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                return result;
            }
            result.StatusCode = "00";
            result.StatusMessage = "success add childs";
            result.Value = true;
            return result;

        }

        public static Data.EFResponse AddMemberInDBPoint(DataMember.Member param, string by, string url, string exBrand = "")
        {
            Data.EFResponse result = new Data.EFResponse();
            try
            {
                var dataMember = Data.Member.GetById(param.ID);
                if (dataMember == null)
                {
                    dataMember = new Data.Member();
                    dataMember.Email = param.Email;
                    dataMember.Phone = param.Phone;
                    dataMember.Name = string.Format("{0} {1}", param.FirstName, param.LastName);
                    dataMember.ID = param.ID;
                    dataMember.EventCode = param.EventCode;

                    if (param.SPG != null)
                    {
                        //add Code By BRY FOR DRY WET SAMPLE
                        if (!string.IsNullOrEmpty(exBrand))
                        {
                            dataMember.ExBrand = exBrand;
                            if (exBrand.ToUpper().Contains("GOLD2"))
                            {
                                dataMember.SampleType = "WET";
                            }
                            else if (exBrand.ToUpper().Contains("GOLD1"))
                            {
                                dataMember.SampleType = "DRY";
                            }
                        }
                        // END CODE
                    }
                    result = dataMember.Insert(by, url, null);
                }
            }
            catch (Exception e)
            {
                result.ErrorEntity = e.InnerException != null ? e.InnerException.ToString() : e.Message;
                result.ErrorMessage = e.Message;
                result.Success = false;
            }

            if (result != null && !result.Success && !string.IsNullOrEmpty(result.ErrorEntity))
            {
                LoyaltyPointEngine.Data.EventLog.InsertEventLog(null, LoyaltyPointEngine.Common.EventLogLogic.LogType.Error, LoyaltyPointEngine.Common.EventLogLogic.LogSource.Member, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode.Add, result.ErrorEntity, param, url, by);
            }

            return result;
        }

        /// <summary>
        /// Search list of member base on search criteria and Pagination.
        /// Relation table members will not appear in the results
        /// For all relation used api get by Id
        /// </summary>
        /// <param name="param">ParamPagination</param>
        /// <returns></returns>
        public static ResultPaginationModel<List<MemberModel>> GetMemberModelByPagination(ParamPagination param)
        {
            ResultPaginationModel<List<MemberModel>> result = new ResultPaginationModel<List<MemberModel>>();

            if (param != null)
            {
                try
                {
                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var membersLinq = Member.GetAll();

                    if (!string.IsNullOrEmpty(param.OrderByColumnName))
                    {
                        //note: use extended order by for dynamic order by
                        string direction = !string.IsNullOrEmpty(param.OrderByDirection) && param.OrderByDirection.ToLower() == "desc" ? "Desc" : "Asc";
                        membersLinq = membersLinq.OrderBy(string.Format("{0} {1}", param.OrderByColumnName, direction));
                    }
                    else
                    {
                        membersLinq = membersLinq.OrderBy(x => x.ID);
                    }

                    int total = 0;
                    if (!string.IsNullOrEmpty(param.Search))
                    {
                        membersLinq = membersLinq.Where(x => x.Email.Contains(param.Search) || x.Phone.Contains(param.Search));
                        total = Member.GetAll().Count(x => x.Email.Contains(param.Search) || x.Phone.Contains(param.Search));
                    }
                    else
                    {
                        total = Member.GetAll().Count();
                    }


                    var members = membersLinq.Skip((CurrentPage - 1) * totalRowPerPage).Take(totalRowPerPage).ToList();

                    result.TotalRow = total;


                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} member(s)", members.Count);

                    //use this exted method to translate from object datalayer to model
                    result.Value = members.Translated();
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        /// <summary>
        /// Get Detail information of member with spesific ID include all relation table
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static ResultModel<MemberDetailModel> GetMemberModelById(int ID)
        {
            ResultModel<MemberDetailModel> result = new ResultModel<MemberDetailModel>();
            var dataMember = DataMember.Member.GetById(ID);
            if (dataMember != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get member with ID {0}", ID);
                result.Value = new MemberDetailModel(dataMember);
                //itung banyaknya dia upload

                result.Value.CountUpload = CountReceipt(ID);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("member with ID {0} not found in system", ID);
            }



            return result;
        }

        /// <summary>
        /// Is member agree with terms and conditions
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static ResultModel<bool> IsAgreeWithTermsAndConditions(int ID)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            var dataMember = DataMember.Member.GetById(ID);
            if (dataMember != null)
            {
                var member = new MemberDetailModel(dataMember);

                result.StatusCode = "00";
                result.StatusMessage = "Success.";
                result.Value = !(member.AcceptDate == null);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("member with ID {0} not found in system", ID);
                result.Value = false;
            }

            return result;
        }

        /// <summary>
        /// Update data member by parameter Member Model. If you want update please encript password with SHA256 and then ecript value with MD5.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static ResultModel<MemberModel> UpdateMemberByMemberModel(EditMemberParameterModel param, string by, int memberId, bool IsFromCMS = false, EditMemberCMSModel cmsModel = null)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();
            try
            {
                if (param == null)
                {
                    result.StatusCode = "101";
                    result.StatusMessage = string.Format("parameter Model is required");
                    return result;
                }

                var dataMember = Member.GetById(memberId);
                if (dataMember != null)
                {
                    if (!string.IsNullOrWhiteSpace(param.Phone))
                    {
                        if (!PhoneHelper.isPhoneValid(param.Phone))
                        {
                            result.StatusCode = "27";
                            result.StatusMessage = string.Format("Format Phone is wrong");
                            return result;
                        }
                        else
                        {
                            var checkPhone = Member.GetByPhone(param.Phone);
                            if (checkPhone != null && checkPhone.ID != dataMember.ID)
                            {
                                result.StatusCode = "31";
                                result.StatusMessage = string.Format("Phone has been used by other user");
                                return result;
                            }
                            //dataMember.Phone = param.Phone;
                        }
                    }
                    else
                    {
                        result.StatusCode = "26";
                        result.StatusMessage = string.Format("Phone can not be empty");
                        return result;
                    }

                    // Jika memiliki anak lebih dari 1
                    if (param.Childs != null && param.Childs.Count > 1)
                    {
                        // Validasi jika anak yg sama diinput lebih dari sekali
                        var sameChilds = param.Childs.GroupBy(x => new { Name = x.Name.Trim().ToLower(), x.Birthdate })
                          .Where(x => x.Count() > 1)
                          .Select(x => new { Element = x.Key, Counter = x.Count() })
                          .ToList();

                        if (sameChilds.Count > 0)
                        {
                            result.StatusCode = "32";
                            result.StatusMessage = string.Format("Tidak boleh memiliki data anak yang sama");
                            return result;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(param.Email))
                    {
                        if (!EmailHelper.IsValidEmail(param.Email))
                        {
                            result.StatusCode = "26";
                            result.StatusMessage = string.Format("Format email is wrong");
                            return result;
                        }
                        else
                        {
                            var checkEmail = Member.GetByEmail(param.Email);
                            if (checkEmail != null && checkEmail.ID != dataMember.ID)
                            {
                                result.StatusCode = "30";
                                result.StatusMessage = string.Format("Email has been used by other user");
                                return result;
                            }
                        }
                        dataMember.Email = param.Email;
                    }
                    else
                        dataMember.Email = string.Format("{0}@{0}.com", param.Phone);

                    if (!string.IsNullOrEmpty(param.FacebookID))
                        dataMember.FacebookID = param.FacebookID;

                    if (!string.IsNullOrEmpty(param.Gender))
                    {
                        if (param.Gender.ToLower() != "f" && param.Gender.ToLower() != "m")
                        {
                            result.StatusCode = "25";
                            result.StatusMessage = string.Format("Gender format is wrong. Please use 'F' for Female and 'M' for Male");
                            return result;
                        }
                        else
                            dataMember.Gender = "F";
                    }
                    else
                        dataMember.Gender = string.Empty;

                    if (!string.IsNullOrEmpty(param.FirstName))
                        dataMember.FirstName = param.FirstName;

                    if (!string.IsNullOrEmpty(param.LastName))
                        dataMember.LastName = param.LastName;

                    if (!string.IsNullOrEmpty(param.Subscriber))
                    {
                        dataMember.subscribe = param.Subscriber;
                    }
                    if (!string.IsNullOrEmpty(param.UsiaKehamilan))
                    {
                        dataMember.UsiaKehamilan = param.UsiaKehamilan;
                    }

                    if (string.IsNullOrWhiteSpace(param.Channel))
                    {
                        int channelID = dataMember.ChannelId;
                        if (channelID == null || channelID <= 0)
                        {
                            var dtaChannel = Channel.GetByCode(ChannelType.web.ToString());
                            if (dtaChannel != null)
                            {
                                dataMember.Channel = dtaChannel;
                                dataMember.ChannelId = dtaChannel.ID;
                            }
                        }
                    }
                    else
                    {
                        var dtaChannel = Channel.GetByCode(param.Channel);
                        if (dtaChannel != null)
                        {
                            dataMember.Channel = dtaChannel;
                            dataMember.ChannelId = dtaChannel.ID;
                        }
                    }

                    if (param.StagesID.HasValue)
                        dataMember.StagesID = param.StagesID;

                    if (!string.IsNullOrEmpty(param.StagesValue))
                        dataMember.StagesValue = param.StagesValue;

                    if (!string.IsNullOrEmpty(param.Address))
                        dataMember.Address = param.Address;

                    if (param.ProvinceID.HasValue)
                        dataMember.ProvinceID = param.ProvinceID;

                    if (param.DistrictID.HasValue)
                    {
                        dataMember.DistrictID = param.DistrictID;
                        dataMember.DistrictPointID = District.GetPointDistrictId(param.DistrictID.Value);
                    }

                    if (param.SubDistrictID.HasValue)
                        dataMember.SubDistrictID = param.SubDistrictID;

                    if (param.SpgID.HasValue)
                        dataMember.SpgID = param.SpgID;

                    if (!string.IsNullOrEmpty(param.Birthdate))
                    {
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(param.Birthdate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            dataMember.Birthdate = dt;
                        }
                        else
                        {
                            result.StatusCode = "10";
                            result.StatusMessage = string.Format("Format Birthdate is wrong");
                            return result;
                        }
                    }
                    if (!param.StagesID.HasValue)
                    {
                        if (dataMember.StagesID.HasValue)
                        {
                            param.StagesID = dataMember.StagesID;
                        }
                        else
                        {
                            param.StagesID = 10;
                        }
                    }
                    var Pregnancy = _pregnant(param.StagesID.Value, param.ProductBeforeID.HasValue ? param.ProductBeforeID.Value : 0);

                    if (Pregnancy.Value) // Pregnancy == True 
                    {
                        if (param.ProductBeforeID.HasValue)
                        {
                            dataMember.ProductBeforeID = param.ProductBeforeID;
                        }
                        if (param.AgePregnant != null && param.AgePregnant == 1)
                        {
                            //dataMember.AgePregnant = param.AgePregnant;
                            if (param.AgePregnant > 0)
                            {
                                dataMember.AgePregnant = Pregnancy.Value ? (param.AgePregnant > 43 ? 43 : param.AgePregnant) : 0;
                            }
                        }
                        else
                        {
                            dataMember.AgePregnant = param.AgePregnant;
                        }
                    }
                    else // Pregnancy == False 
                    {
                        dataMember.AgePregnant = 0;
                        dataMember.ProductBeforeID = 0;
                    }
                    if (IsFromCMS)
                    {
                        if (!string.IsNullOrEmpty(param.ProfilePicture))
                            dataMember.ProfilePicture = param.ProfilePicture;

                        if (!string.IsNullOrEmpty(param.KTPImage))
                            dataMember.KTPImage = param.KTPImage;

                        if (!string.IsNullOrEmpty(param.KKImage))
                            dataMember.KKImage = param.KKImage;

                        if (!string.IsNullOrEmpty(param.AkteImage))
                            dataMember.AkteImage = param.AkteImage;

                        if (!string.IsNullOrEmpty(param.ESignature))
                            dataMember.ESignature = param.ESignature;

                        if (cmsModel != null)
                        {
                            dataMember.IsActive = cmsModel.IsActive;
                            dataMember.IsApproved = cmsModel.IsApproved;
                            dataMember.IsBlacklist = cmsModel.IsBlackList;
                            dataMember.Remarks = cmsModel.Remarks;
                            dataMember.EventCode = cmsModel.EventCode;
                            dataMember.IsNewsLetter = param.IsNewsletter;
                            if (!dataMember.AcceptDate.HasValue && cmsModel.Consent)
                            {
                                dataMember.AcceptDate = DateTime.Now;
                            }
                            else if (dataMember.AcceptDate.HasValue && !cmsModel.Consent)
                            {
                                dataMember.AcceptDate = null;
                            }
                        }
                    }
                    else
                    {
                        string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
                        string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);
                        string memberImageCode = "";
                        if (!string.IsNullOrEmpty(dataMember.ProfilePicture) && dataMember.ProfilePicture.Contains("/"))
                        {
                            var arr = dataMember.ProfilePicture.Split('/');
                            if (arr != null && arr.Count() - 2 >= 0)
                            {
                                memberImageCode = arr[arr.Count() - 2];
                            }
                        }
                        else if (!string.IsNullOrEmpty(dataMember.KTPImage) && dataMember.KTPImage.Contains("/"))
                        {
                            var arr = dataMember.KTPImage.Split('/');
                            if (arr != null && arr.Count() - 2 >= 0)
                            {
                                memberImageCode = arr[arr.Count() - 2];
                            }
                        }
                        else if (!string.IsNullOrEmpty(dataMember.KKImage) && dataMember.KKImage.Contains("/"))
                        {
                            var arr = dataMember.KKImage.Split('/');
                            if (arr != null && arr.Count() - 2 >= 0)
                            {
                                memberImageCode = arr[arr.Count() - 2];
                            }
                        }
                        else if (!string.IsNullOrEmpty(dataMember.AkteImage) && dataMember.AkteImage.Contains("/"))
                        {
                            var arr = dataMember.AkteImage.Split('/');
                            if (arr != null && arr.Count() - 2 >= 0)
                            {
                                memberImageCode = arr[arr.Count() - 2];
                            }
                        }
                        else
                        {
                            memberImageCode = Data.Number.Generated(Data.Number.Type.MEMB, by);
                        }
                        string err = "";
                        if (!string.IsNullOrEmpty(param.ProfilePicture))
                        {
                            dataMember.ProfilePicture = InsertMemberImage(param.ProfilePicture, memberImageCode, MediaLocalImageDirectoryMember,
                            RelativePathMember, "ProfilePicture", out err);
                            if (!string.IsNullOrEmpty(err))
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = err;
                                return result;
                            }
                        }
                        if (!string.IsNullOrEmpty(param.KTPImage))
                        {
                            dataMember.KTPImage = InsertMemberImage(param.KTPImage, memberImageCode, MediaLocalImageDirectoryMember,
                            RelativePathMember, "KTPImage", out err);
                            if (!string.IsNullOrEmpty(err))
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = err;
                                return result;
                            }
                        }
                        if (!string.IsNullOrEmpty(param.KKImage))
                        {
                            dataMember.KKImage = InsertMemberImage(param.KKImage, memberImageCode, MediaLocalImageDirectoryMember,
                            RelativePathMember, "KKImage", out err);
                            if (!string.IsNullOrEmpty(err))
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = err;
                                return result;
                            }
                        }
                        if (!string.IsNullOrEmpty(param.AkteImage))
                        {
                            dataMember.AkteImage = InsertMemberImage(param.AkteImage, memberImageCode, MediaLocalImageDirectoryMember,
                            RelativePathMember, "AkteImage", out err);
                            if (!string.IsNullOrEmpty(err))
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = err;
                                return result;
                            }
                        }

                        if (!string.IsNullOrEmpty(param.KTPImage) || !string.IsNullOrEmpty(param.AkteImage)) //Reupload KTP/Akte
                        {
                            dataMember.IsApprovedKKImage = null;
                            dataMember.ReasonRejectKK = null;
                        }

                    }
                    if (!string.IsNullOrEmpty(param.PlaceOfBirth))
                    {
                        dataMember.PlaceOfBirth = param.PlaceOfBirth;
                    }
                    if (!string.IsNullOrEmpty(param.Password))
                    {
                        dataMember.Password = param.Password;
                    }
                    if (!string.IsNullOrEmpty(param.KTPNumber))
                    {
                        dataMember.KTPNumber = param.KTPNumber;
                    }
                    if (!string.IsNullOrEmpty(param.KKNumber))
                    {
                        dataMember.KKNumber = param.KKNumber;
                    }
                    if (!string.IsNullOrEmpty(param.ZipCode))
                    {
                        dataMember.ZipCode = param.ZipCode;
                    }
                    if (param.StagesID.HasValue && param.StagesID > 0 && param.Childs != null)
                    {
                        var Child = _toChild(0, param.Childs, param.StagesID.Value, by);

                        if (dataMember.StagesID != param.StagesID)
                        {
                            dataMember.StagesID = param.StagesID;
                        }

                    }
                    if (param.StagesID.HasValue)
                    {
                        dataMember.StagesID = param.StagesID;
                    }

                    dataMember.AgePregnantStart = param.AgePregnant;

                    var resultEF = dataMember.Updated(by);
                    if (!resultEF.Success)
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = resultEF.ErrorMessage;

                        return result;
                    }

                    if (!dataMember.IsActive || !dataMember.IsApproved || dataMember.IsBlacklist)
                    {
                        int MemberID = dataMember.ID;
                        var datas = Data.PoolMemberToken.GetByMemberID(dataMember.ID).ToList();
                        if (datas != null && datas.Count > 0)
                        {
                            foreach (var item in datas)
                            {
                                Data.EFResponse resultDelete = item.HardDeleted();

                                if (!resultDelete.Success)
                                {
                                    result.StatusCode = "500";
                                    result.StatusMessage = resultDelete.ErrorMessage;

                                    return result;
                                }
                            }
                        }
                    }

                    // If Change Password, must be LogOff
                    bool IsChangePassword = false;
                    if (IsFromCMS)
                    {
                        if (!string.IsNullOrEmpty(cmsModel.Password))
                        {
                            IsChangePassword = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(param.Password))
                        {
                            IsChangePassword = true;
                        }

                        ResultModel<bool> responseApprovalImage = new ResultModel<bool>();
                        responseApprovalImage.StatusCode = "00";

                        if (!string.IsNullOrEmpty(param.KTPImage) || !string.IsNullOrEmpty(param.AkteImage))//Reupload KTP/Akte
                        {
                            AddApprovalImageParameterModel paramApprovalImage = new AddApprovalImageParameterModel();
                            paramApprovalImage.MemberID = dataMember.ID;
                            paramApprovalImage.KTPImage = !string.IsNullOrEmpty(param.KTPImage) ? dataMember.KTPImage : "";
                            paramApprovalImage.AkteImage = !string.IsNullOrEmpty(param.AkteImage) ? dataMember.AkteImage : "";
                            responseApprovalImage = AddApprovalImage(paramApprovalImage, by);

                            if (responseApprovalImage.StatusCode != "00")
                            {
                                result.StatusCode = responseApprovalImage.StatusCode;
                                result.StatusMessage = responseApprovalImage.StatusMessage;

                                return result;
                            }
                        }

                    }

                    bool IsActivated = false;
                    if (dataMember.ActiveDate != null)
                    {
                        if (dataMember.ActiveDate.Value.ToString("dd/MM/YYYY HH:mm:00").Equals(DateTime.Now.ToString("dd/MM/YYYY HH:mm:00")))
                        {
                            IsActivated = true;
                        }
                    }

                    if (IsChangePassword && !IsActivated)
                    {
                        ResultModel<bool> resultLogOff = MemberLogic.LogOff(dataMember.ID);
                    }

                    result.Value = new MemberModel(dataMember);
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success update new member with ID {0}", dataMember.ID);
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("Id {0} not found in system", memberId);
                }
            }

            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }

        public static ResultModel<MemberModel> UpdateMemberByBebeJourneyModel(BebeJourneyModel param, int memberID, string by)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();
            string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
            string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);
            string memberImageCode = "";

            try
            {
                var dataMember = Member.GetById(memberID);
                if (dataMember != null)
                {

                    if (!string.IsNullOrEmpty(dataMember.KTPImage) && dataMember.KTPImage.Contains("/"))
                    {
                        var arr = dataMember.KTPImage.Split('/');
                        if (arr != null && arr.Count() - 2 >= 0)
                        {
                            memberImageCode = arr[arr.Count() - 2];
                        }
                    }
                    else if (!string.IsNullOrEmpty(dataMember.AkteImage) && dataMember.AkteImage.Contains("/"))
                    {
                        var arr = dataMember.AkteImage.Split('/');
                        if (arr != null && arr.Count() - 2 >= 0)
                        {
                            memberImageCode = arr[arr.Count() - 2];
                        }
                    }
                    else
                    {
                        memberImageCode = Data.Number.Generated(Data.Number.Type.MEMB, by);
                    }

                    string err = "";

                    if (!string.IsNullOrEmpty(param.KTPImage))
                    {
                        dataMember.KTPImage = InsertMemberImage(param.KTPImage, memberImageCode, MediaLocalImageDirectoryMember,
                        RelativePathMember, "KTPImage", out err);
                        if (!string.IsNullOrEmpty(err))
                        {
                            result.StatusCode = "500";
                            result.StatusMessage = err;
                            return result;
                        }
                    }
                    if (!string.IsNullOrEmpty(param.AkteImage))
                    {
                        dataMember.AkteImage = InsertMemberImage(param.AkteImage, memberImageCode, MediaLocalImageDirectoryMember,
                        RelativePathMember, "AkteImage", out err);
                        if (!string.IsNullOrEmpty(err))
                        {
                            result.StatusCode = "500";
                            result.StatusMessage = err;
                            return result;
                        }
                    }

                    dataMember.IsApprovedKKImage = null;
                    dataMember.ReasonRejectKK = null;

                    var resultEF = dataMember.Updated(by);
                    if (!resultEF.Success)
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = resultEF.ErrorMessage;

                        return result;
                    }

                    ResultModel<bool> responseApprovalImage = new ResultModel<bool>();
                    responseApprovalImage.StatusCode = "00";

                    if (!string.IsNullOrEmpty(param.KTPImage) || !string.IsNullOrEmpty(param.AkteImage))//Reupload KTP/Akte
                    {
                        AddApprovalImageParameterModel paramApprovalImage = new AddApprovalImageParameterModel();
                        paramApprovalImage.MemberID = dataMember.ID;
                        paramApprovalImage.KTPImage = !string.IsNullOrEmpty(param.KTPImage) ? dataMember.KTPImage : "";
                        paramApprovalImage.AkteImage = !string.IsNullOrEmpty(param.AkteImage) ? dataMember.AkteImage : "";
                        responseApprovalImage = AddApprovalImage(paramApprovalImage, by);

                        if (responseApprovalImage.StatusCode != "00")
                        {
                            result.StatusCode = responseApprovalImage.StatusCode;
                            result.StatusMessage = responseApprovalImage.StatusMessage;

                            return result;
                        }
                    }

                    result.Value = new MemberModel(dataMember);
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success insert  KTP & AKTE Image with MEMBER ID {0}", dataMember.ID);
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("Id {0} not found in system", param.MemberID);
                }
            }

            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Register new member to system via SMS. Please encript password with SHA256 and then ecript value with MD5.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static ResultModel<Member> Sms(AddMemberSMSParameterModel param, string by, out string outPassword, string url = "", string channel = "sms")
        {
            outPassword = "";
            ResultModel<Member> result = new ResultModel<Member>();
            try
            {
                if (string.IsNullOrEmpty(channel))
                {
                    channel = ChannelType.sms.ToString();
                }
                Channel dataChannel = Channel.GetByCode(channel);
                if (dataChannel == null)
                {
                    result.StatusCode = "29";
                    result.StatusMessage = string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.sms.ToString());
                    return result;
                }

                if (string.IsNullOrEmpty(param.Phone))
                {
                    result.StatusCode = "21";
                    result.StatusMessage = string.Format("Phone is required");
                    return result;
                }
                else
                {
                    if (!PhoneHelper.isPhoneValid(param.Phone))
                    {
                        result.StatusCode = "27";
                        result.StatusMessage = string.Format("Maaf, Format No Handphone yang dimasukan salah, silahkan masukan dengan format 08xxxxxxxxxx");
                        return result;
                    }
                }

                if (string.IsNullOrEmpty(param.Name))
                {
                    result.StatusCode = "23";
                    result.StatusMessage = string.Format("Name is required");
                    return result;
                }


                SPG spgBySpgCode = null;
                int? spgDistrictId = null;
                string eventCode = string.Empty;
                if (!string.IsNullOrEmpty(param.SPGCode))
                {
                    spgBySpgCode = SPG.GetByCode(param.SPGCode);
                    if (spgBySpgCode == null)
                    {
                        result.StatusCode = "24";
                        result.StatusMessage = string.Format("Maaf, Kode {0} tidak ada, hubungi Supervisor atau gunakan kode lain.", param.SPGCode);
                        return result;
                    }
                    else
                    {
                        int districtId = 0;
                        if (!string.IsNullOrEmpty(spgBySpgCode.District.Trim()) && int.TryParse(spgBySpgCode.District.Trim(), out districtId))
                        {
                            spgDistrictId = districtId;
                        }
                        param.SampleType = "DRY";
                        if (param.SPGCode.ToUpper().ToString() == "GLD")
                            param.SampleType = "WET";
                    }
                }
                else
                    eventCode = param.SPGCode;


                //sisipin buat ponta
                if (!string.IsNullOrEmpty(eventCode))
                {
                    if (eventCode.ToLower().Contains("ponta"))
                    {
                        dataChannel = Channel.GetByCode(ChannelType.alfamart.ToString());

                    }
                }

                var nameSegments = param.Name.Trim().Split(' ');
                string firstName = param.Name, lastName = firstName;
                if (nameSegments.Length > 1)
                {
                    firstName = nameSegments[0];
                    lastName = nameSegments[1];
                }

                string newPassword = StringHelper.GeneratedPassword();
                string EncripPassword = Encrypt.LoyalEncript(newPassword);
                var dataMember = DataMember.Member.GetByPhone(param.Phone);
                if (dataMember == null)
                {
                    bool success = false;
                    var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
                    {
                        dataMember = new DataMember.Member();
                        dataMember.ChannelId = dataChannel.ID;
                        dataMember.Gender = "";// param.Gender.ToUpper();
                        dataMember.FirstName = firstName;
                        dataMember.LastName = lastName;
                        dataMember.Email = string.Format("{0}@{0}.com", param.Phone);// param.Email;
                        dataMember.Phone = param.Phone;
                        dataMember.SMSID = param.SMSID;
                        var district = DataMember.District.GetByID(param.DistrictID.GetValueOrDefault(0));
                        if (district != null)
                        {
                            dataMember.DistrictID = param.DistrictID;
                            dataMember.ProvinceID = district.ProvinceID;
                        }
                        dataMember.DistrictPointID = spgDistrictId;
                        if (dataMember.ProductBeforeID == null)
                            dataMember.ProductBeforeID = 0;

                        if (url == "PromoSMS")
                            dataMember.EventCode = "RegisterSMSPromo";

                        if (!string.IsNullOrEmpty(param.Source))
                        {
                            switch (param.Source.Trim())
                            {
                                case "1":
                                    dataMember.IsAlfamart = 4;
                                    break;
                            }
                        }
                        if (spgBySpgCode != null)
                        {
                            dataMember.SpgID = spgBySpgCode.ID;
                        }

                        dataMember.Password = EncripPassword;
                        var memberResponse = dataMember.Inserted(by);
                        if (memberResponse.Success)
                        {
                            var child = new Child();
                            child.Name = "";
                            child.ParentID = dataMember.ID;
                            child.Gender = "";
                            if (!string.IsNullOrEmpty(param.ChildDateOfBirth))
                            {
                                DateTime dt = DateTime.MinValue;
                                if (DateTime.TryParseExact(param.ChildDateOfBirth, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                                {
                                    if (string.IsNullOrEmpty(param.SPGCode) ? false : param.SPGCode.ToUpper() == "GMD")
                                    {
                                        if (dt.Year >= 2016)
                                        {
                                            result.StatusCode = "30";
                                            string mgd = string.Format("Maaf kode unik tdk dpt dikirimkan krn usia anak<1 th. Konsultasi seputar nutrisi&kesehatan di 08041112233 atau temukan 7KEHEBATAN PERUT di http://bit.ly/2nC56FH");
                                            result.StatusMessage = string.Format(mgd);
                                            transScope.Dispose();
                                            return result;
                                        }
                                    }
                                    child.Birthdate = dt;
                                }
                                else
                                {
                                    result.StatusCode = "30";
                                    result.StatusMessage = string.Format("Format of Child's Birthdate is wrong");
                                    transScope.Dispose();
                                    return result;
                                }
                            }
                            else
                            {
                                result.StatusCode = "30";
                                result.StatusMessage = string.Format("Child's Birthdate is required");
                                transScope.Dispose();
                                return result;
                            }
                            var childResponse = child.Inserted(by);

                            if (childResponse.Success)
                            {
                                success = true;
                                transScope.Complete();
                                transScope.Dispose();
                            }
                            else
                            {
                                result.Value = null;
                                result.StatusCode = "50";
                                result.StatusMessage = memberResponse.ErrorMessage;
                                transScope.Dispose();
                            }
                        }
                        else
                        {
                            result.Value = null;
                            result.StatusCode = "50";
                            result.StatusMessage = memberResponse.ErrorMessage;
                            transScope.Dispose();
                        }
                    }

                    if (success)
                    {
                        var efRespinse = AddMemberInDBPoint(dataMember, by, url, param.ExBrand);
                        if (efRespinse != null && efRespinse.Success)
                        {
                            outPassword = newPassword;
                            result.Value = dataMember;
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("Registrasi Anda berhasil. Gunakan ID: {0} dan password: {1} untuk cek poin dan redeem di www.bebeclub.co.id/beberewards", dataMember.Phone, newPassword);
                        }
                        else
                        {
                            if (efRespinse != null)
                            {
                                result.StatusCode = "20";
                                result.StatusMessage = efRespinse.ErrorMessage;
                            }
                        }
                    }
                }
                else
                {
                    result.StatusCode = "50";
                    if (string.IsNullOrEmpty(param.SPGCode) ? false : param.SPGCode.ToUpper() == "GMD")
                    {
                        string msgErr = SiteSetting.msgGMDFailed;
                        result.StatusMessage = string.Format(msgErr);
                    }
                    else
                    {
                        result.StatusMessage = string.Format("Maaf, No Handphone ini sudah terdaftar. Silakan langsung login ke www.bebeclub.co.id/beberewards");
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;
        }

        /// <summary>
        /// encript password with SHA256 and then ecript value with MD5.
        /// </summary>
        /// <param name="PlainPassword"></param>
        /// <returns></returns>
        public static string GetEncriptedPassword(string PlainPassword)
        {
            string raw = PlainPassword;
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(PlainPassword);
            var sha1 = SHA256.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);
            string sha256 = BitConverter.ToString(hashBytes).Replace("-", "").ToLowerInvariant();

            System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sha256));

                // Loop through each byte of the hashed data
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private static string InsertMemberImage(string sourceImage, string memberImageCode, string MediaLocalImageDirectoryMember, string RelativePathMember, string type, out string err)
        {
            err = "";

            string imageURL = sourceImage;
            if (!string.IsNullOrEmpty(imageURL))
            {
                if (StringHelper.IsBase64String(imageURL))
                {
                    try
                    {
                        if (RelativePathMember.Substring(RelativePathMember.Length - 1, 1).Contains("/"))
                        {
                            RelativePathMember = RelativePathMember + memberImageCode + "/";
                        }
                        else
                        {
                            RelativePathMember = RelativePathMember + "/" + memberImageCode + "/";
                        }

                        if (MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\"))
                        {
                            MediaLocalImageDirectoryMember = MediaLocalImageDirectoryMember + memberImageCode + "\\";
                        }
                        else
                        {
                            MediaLocalImageDirectoryMember = MediaLocalImageDirectoryMember + "\\" + memberImageCode + "\\";
                        }

                        if (!Directory.Exists(MediaLocalImageDirectoryMember))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(Path.Combine(MediaLocalImageDirectoryMember));
                        }

                        string filename = type + ".jpg";
                        string imageFulleName = MediaLocalImageDirectoryMember.Substring(MediaLocalImageDirectoryMember.Length - 1, 1).Contains("\\") ?
                                string.Format("{0}{1}", MediaLocalImageDirectoryMember, filename) :
                                string.Format("{0}\\{1}", MediaLocalImageDirectoryMember, filename);

                        ImageHelper.SaveImageFromBase64(imageURL, imageFulleName, 50L);

                        return RelativePathMember + filename;
                    }
                    catch (Exception ex)
                    {
                        err = "Error when create file: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                    }
                }
                else
                {
                    err = type + "is not Base64";
                }
            }
            return imageURL;
        }

        public static EFResponse UpdateMemberPoint(int MemberID, int TotalPoint, string by)
        {
            EFResponse response = new EFResponse();
            try
            {
                var data = Member.GetById(MemberID);
                if (data != null)
                {
                    data.MemberPoint = TotalPoint;
                    response = data.Updated(by);
                }
                else
                {
                    response.Success = false;
                    response.ErrorEntity = "ID not found in system";
                    response.ErrorMessage = response.ErrorEntity;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorEntity = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                response.ErrorMessage = response.ErrorEntity;
            }
            return response;
        }

        public static EFResponse UpdateMemberIsDeleted(int MemberID, bool IsDeleted, string by)
        {
            EFResponse response = new EFResponse();
            try
            {
                var data = Member.GetById(MemberID);
                if (data != null)
                {
                    data.DeletedBy = "SystemMigration";
                    data.DeletedDate = DateTime.Now;
                    response = data.Delete(by);
                }
                else
                {
                    response.Success = false;
                    response.ErrorEntity = "ID not found in system";
                    response.ErrorMessage = response.ErrorEntity;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorEntity = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                response.ErrorMessage = response.ErrorEntity;
            }
            return response;
        }

        public static EFResponse UpdateMemberIsVendor(string phone, int stat, string by, string vendor)
        {
            EFResponse response = new EFResponse();
            try
            {
                Boolean ven = false;

                var data = Member.GetByPhone(phone);
                switch (vendor)
                {
                    case "1": ven = data.IsAlfamart != 3 ? true : false; break;
                    case "2": ven = (data != null ? (data.IsAlfamart != 3 ? true : false) : false); break;
                }
                if (data != null && ven)
                {
                    response = data.UpdatedVendorStat(stat, vendor);
                }
                else
                {
                    if (data == null)
                    {
                        response.Success = false;
                        response.ErrorEntity = "ID not found in system";
                        response.ErrorMessage = response.ErrorEntity;
                    }
                    else
                    {
                        response.Success = false;
                        response.ErrorEntity = "Phone No Already Ponta's Member";
                        response.ErrorMessage = response.ErrorEntity;
                    }
                    response.Success = false;
                    response.ErrorEntity = "ID not found in system";
                    response.ErrorMessage = response.ErrorEntity;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorEntity = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                response.ErrorMessage = response.ErrorEntity;
            }
            return response;
        }

        public static ResultModel<MemberModel> UpdateMemberAcceptDate(int MemberID, EditAcceptDateModel param)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();
            try
            {
                var dataMember = Member.GetById(MemberID);
                if (dataMember != null)
                {
                    var by = dataMember.FirstName + " " + dataMember.LastName;

                    if (dataMember.AcceptDate == null)
                    {
                        dataMember.AcceptDate = param.AcceptDate; // DateTime.Now;
                    }

                    var resultEF = dataMember.Updated(by);
                    if (resultEF.Success)
                    {
                        result.Value = new MemberModel(dataMember);
                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("Success update the AcceptDate for a member with ID {0}", MemberID);
                    }
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("Id {0} not found in system", MemberID);
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Change password member
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static ResultModel<bool> ChangePhone(ChangePhoneModel param)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = false;
            if (string.IsNullOrEmpty(param.OldPhone))
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("OldPhone is required");
                return result;
            }
            if (string.IsNullOrEmpty(param.NewPhone))
            {
                result.StatusCode = "51";
                result.StatusMessage = string.Format("NewPhone is required");
                return result;
            }

            // Checking data in Data Member
            var dataMember = DataMember.Member.GetByPhone(param.OldPhone);
            if (dataMember == null)
            {
                result.StatusCode = "10";
                result.StatusMessage = string.Format("Member not found in system");
                return result;
            }

            // Checking data in Data Point
            var member = Data.Member.GetById(dataMember.ID);
            if (member == null)
            {
                result.StatusCode = "20";
                result.StatusMessage = string.Format("Member not found in system");
                return result;
            }

            member.Phone = param.NewPhone;
            dataMember.Phone = param.NewPhone;

            // Update Data Member First
            var resultUpdate = dataMember.Updated(dataMember.FirstName);
            if (!resultUpdate.Success)
            {
                result.StatusCode = "11";
                result.StatusMessage = string.Format("Failed when changing Phone");
                return result;
            }

            // Update Data Point
            var resultUpdatePoint = dataMember.Updated(dataMember.FirstName);
            if (resultUpdatePoint.Success)
            {
                result.StatusCode = "00";
                result.Value = true;
                result.StatusMessage = string.Format("Changing Password successfully");
            }
            else
            {
                result.StatusCode = "11";
                result.StatusMessage = string.Format("Failed when changing Phone");
            }

            return result;
        }

        /// <summary>
        /// Change password member
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static ResultModel<bool> ChangePassword(ChangePasswordModel param, int MemberID)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            if (!string.IsNullOrEmpty(param.OldPassword))
            {
                if (!string.IsNullOrEmpty(param.NewPassword))
                {
                    if (!string.IsNullOrEmpty(param.ConfirmPassword))
                    {
                        if (param.ConfirmPassword == param.NewPassword)
                        {
                            var dataMember = DataMember.Member.GetById(MemberID);
                            if (dataMember != null)
                            {
                                if (dataMember.Password == param.OldPassword)
                                {
                                    dataMember.Password = Common.Encrypt.LoyalEncript(param.NewPassword);
                                    dataMember.Updated(dataMember.FirstName);
                                    result.StatusCode = "00";
                                    result.StatusMessage = string.Format("Changing Password successfully");
                                }
                                else
                                {
                                    result.StatusCode = "11";
                                    result.StatusMessage = string.Format("Old Password not match with password in Loyal System");
                                }
                            }
                            else
                            {
                                result.StatusCode = "10";
                                result.StatusMessage = string.Format("Member not found in system");
                            }
                        }
                        else
                        {
                            result.StatusCode = "52";
                            result.StatusMessage = string.Format("ConfirmPassword and NewPassword  not match");
                        }
                    }
                    else
                    {
                        result.StatusCode = "52";
                        result.StatusMessage = string.Format("ConfirmPassword is required");
                    }
                }
                else
                {
                    result.StatusCode = "51";
                    result.StatusMessage = string.Format("NewPassword is required");
                }
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("OldPassword is required");
            }
            return result;
        }

        /// <summary>
        /// Change password member with no old password
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static ResultModel<bool> ChangePasswordNoOldPassword(ChangePasswordModel param, int MemberID)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            if (!string.IsNullOrEmpty(param.NewPassword))
            {
                if (!string.IsNullOrEmpty(param.ConfirmPassword))
                {
                    if (param.ConfirmPassword == param.NewPassword)
                    {
                        var dataMember = DataMember.Member.GetById(MemberID);
                        if (dataMember != null)
                        {
                            dataMember.Password = Common.Encrypt.LoyalEncript(param.NewPassword);
                            dataMember.Updated(dataMember.FirstName);
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("Changing Password successfully");
                        }
                        else
                        {
                            result.StatusCode = "10";
                            result.StatusMessage = string.Format("Member not found in system");
                        }
                    }
                    else
                    {
                        result.StatusCode = "52";
                        result.StatusMessage = string.Format("ConfirmPassword and NewPassword  not match");
                    }
                }
                else
                {
                    result.StatusCode = "52";
                    result.StatusMessage = string.Format("ConfirmPassword is required");
                }
            }
            else
            {
                result.StatusCode = "51";
                result.StatusMessage = string.Format("NewPassword is required");
            }
            return result;
        }

        /// <summary>
        /// Register new member to system. With Long Register Parameter Please encript password with umbraco logic
        /// Format Date: dd/MM/yyyy
        /// </summary>
        /// <param name="param">AddMemberParameterModel</param>
        /// <returns></returns>
        public static ResultModel<Member> AddLongRegister(MemberLongRegisterModel param, string by, string url = "")
        {
            ResultModel<Member> result = new ResultModel<Member>();

            //string err_ ="";
            //  param.ProfilePicture = InsertMemberImage(param.ProfilePicture, UniqueCode, MediaLocalImageDirectoryMember,
            //                    RelativePathMember, "ProfilePicture", out err_);
            //  if (!string.IsNullOrEmpty(err_))
            //  {
            //      param.ProfilePicture ="error: "+ err_;
            //  }
            //result.StatusCode = "101";
            //result.StatusMessage = param.ProfilePicture;
            //return result;

            var resSortRegisterModel = ValidateLongRegister(param);
            if (resSortRegisterModel.StatusCode != "00")
                return resSortRegisterModel;

            var UniqueCode = Data.Number.Generated(Data.Number.Type.MEMB, by);
            string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
            string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);

            var transOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transOptions))
            {
                //add Member
                try
                {
                    string err = "";
                    Channel dataChannel = Channel.GetByCode(ChannelType.web.ToString());
                    if (dataChannel == null)
                    {
                        result.StatusCode = "29";
                        result.StatusMessage = string.Format("ChannelCode '{0}' not found in system, please contact yg middleware administrator", ChannelType.web.ToString());
                        return result;
                    }

                    string newProfilePict = InsertMemberImage(param.ProfilePicture, UniqueCode, MediaLocalImageDirectoryMember, RelativePathMember, "ProfilePicture", out err);

                    int currStagesID = 0;
                    if (!string.IsNullOrEmpty(param.Stages))
                    {
                        var dataStages = Stages.GetAll().FirstOrDefault(x => x.AttributeName == param.Stages);
                        if (dataStages == null)
                        {
                            dataStages = new Stages();
                            dataStages.AttributeName = param.Stages;
                            dataStages.Name = param.Stages;
                            dataStages.IsActive = true;
                            dataStages.IsDeleted = false;
                            dataStages.Inserted(by);
                        }
                        currStagesID = dataStages.ID;
                    }

                    DateTime curDOB = DateTime.MinValue;
                    if (!string.IsNullOrEmpty(param.DOB))
                    {
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(param.DOB, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            curDOB = dt;
                        }
                        else
                        {
                            transScope.Dispose();
                            result.StatusCode = "10";
                            result.StatusMessage = string.Format("Format Birthdate is wrong");
                            return result;
                        }
                    }

                    var dataMember = !string.IsNullOrEmpty(param.OldEmailBeforeUpdated) ? DataMember.Member.GetByEmail(param.OldEmailBeforeUpdated) : DataMember.Member.GetByPhoneOrEmail(param.Phone, param.Email);
                    if (dataMember == null)
                    {
                        if (string.IsNullOrEmpty(param.Password))
                        {
                            result.StatusCode = "24";
                            result.StatusMessage = string.Format("Password is required");
                            return result;
                        }

                        dataMember = new DataMember.Member();
                        dataMember.ChannelId = dataChannel.ID;
                        dataMember.FirstName = param.Firstname;
                        dataMember.LastName = param.Lastname;
                        dataMember.Gender = param.Gender;
                        dataMember.Email = param.Email;
                        dataMember.Phone = param.Phone;
                        dataMember.Password = param.Password;
                        dataMember.ProfilePicture = newProfilePict;
                        dataMember.subscribe = param.subscriber;
                        dataMember.UsiaKehamilan = param.UsiaKehamilan;
                        dataMember.ProvinceID = param.ProvinceID;
                        dataMember.DistrictID = param.DistrictID;
                        dataMember.SubDistrictID = param.SubDistrictID;
                        dataMember.Address = param.Address;
                        if (currStagesID > 0)
                            dataMember.StagesID = currStagesID;
                        if (curDOB != DateTime.MinValue)
                            dataMember.Birthdate = curDOB;

                        var res = dataMember.Inserted(by);
                        if (res.Success)
                        {
                            //if (!string.IsNullOrEmpty(param.ChildName))
                            //{
                            var child = new Child();
                            child.ParentID = dataMember.ID;
                            child.Gender = "";
                            child.Name = param.ChildName ?? string.Empty;

                            if (!string.IsNullOrEmpty(param.ChildBirthDate))
                            {
                                DateTime dt = DateTime.MinValue;
                                if (DateTime.TryParseExact(param.ChildBirthDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                                {
                                    child.Birthdate = dt;
                                }
                            }
                            child.PlaceOfBirth = "";
                            child.Inserted(by);
                            //}

                            result.Value = dataMember;
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("success add new member with ID {0}", dataMember.ID);

                            transScope.Complete();
                            transScope.Dispose();

                            if (result.StatusCode == "00")
                            {
                                var efRespinse = AddMemberInDBPoint(dataMember, by, url);
                            }
                        }
                        else
                        {
                            result.Value = dataMember;
                            result.StatusCode = "50";
                            result.StatusMessage = string.Format(res.ErrorMessage);
                        }
                    }
                    else
                    {
                        //if member exist Update Data Member
                        dataMember.ChannelId = dataChannel.ID;
                        if (!string.IsNullOrEmpty(param.Firstname))
                            dataMember.FirstName = param.Firstname;
                        if (!string.IsNullOrEmpty(param.Lastname))
                            dataMember.LastName = param.Lastname;
                        if (!string.IsNullOrEmpty(param.Gender))
                            dataMember.Gender = param.Gender;
                        if (!string.IsNullOrEmpty(param.Email))
                            dataMember.Email = param.Email;
                        if (!string.IsNullOrEmpty(param.Phone))
                            dataMember.Phone = param.Phone;
                        if (!string.IsNullOrEmpty(param.Password))
                        {
                            // dataMember.Password = Encrypt.LoyalEncript(param.Password);
                            dataMember.Password = param.Password;// Encrypt.LoyalEncript(param.Password);
                        }
                        if (!string.IsNullOrEmpty(newProfilePict))
                            dataMember.ProfilePicture = newProfilePict;
                        if (currStagesID > 0)
                            dataMember.StagesID = currStagesID;
                        if (curDOB != DateTime.MinValue)
                            dataMember.Birthdate = curDOB;
                        if (!string.IsNullOrEmpty(param.ProvinceID.ToString()))
                            dataMember.ProvinceID = param.ProvinceID;
                        if (!string.IsNullOrEmpty(param.DistrictID.ToString()))
                            dataMember.DistrictID = param.DistrictID;
                        if (!string.IsNullOrEmpty(param.SubDistrictID.ToString()))
                            dataMember.SubDistrictID = param.SubDistrictID;
                        if (!string.IsNullOrEmpty(param.Address))
                            dataMember.Address = param.Address;

                        dataMember.subscribe = param.subscriber;
                        dataMember.UsiaKehamilan = param.UsiaKehamilan;

                        var res = dataMember.Updated(by);
                        if (res.Success)
                        {
                            DateTime childBOD = DateTime.MinValue;
                            if (!string.IsNullOrEmpty(param.ChildBirthDate))
                            {
                                DateTime dt = DateTime.MinValue;
                                if (DateTime.TryParseExact(param.ChildBirthDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                                {
                                    childBOD = dt;
                                }
                            }

                            //if (!string.IsNullOrEmpty(param.ChildName))
                            //{
                            if (dataMember.Child.Any())
                            {
                                // Check Update DOB Child Default insert
                                if (dataMember.Child.Count == 1)
                                {
                                    var UpdateDefaultChild = dataMember.Child.FirstOrDefault();
                                    if (UpdateDefaultChild != null && UpdateDefaultChild.ID > 0)
                                    {
                                        UpdateDefaultChild.ParentID = dataMember.ID;
                                        UpdateDefaultChild.Gender = "";
                                        UpdateDefaultChild.Name = param.ChildName ?? string.Empty;
                                        UpdateDefaultChild.PlaceOfBirth = "";
                                        UpdateDefaultChild.Birthdate = childBOD;
                                        UpdateDefaultChild.Updated(by);
                                    }
                                }
                                else
                                {
                                    var predictedchild = dataMember.Child.Where(x => x.Name == param.ChildName).FirstOrDefault();
                                    if (predictedchild != null && predictedchild.ID > 0)
                                    {
                                        predictedchild.ParentID = dataMember.ID;
                                        predictedchild.Gender = "";
                                        predictedchild.Name = param.ChildName ?? string.Empty;
                                        predictedchild.PlaceOfBirth = "";
                                        predictedchild.Birthdate = childBOD;
                                        predictedchild.Updated(by);
                                    }
                                    else
                                    {
                                        var child = new Child();
                                        child.ParentID = dataMember.ID;
                                        child.Gender = "";
                                        child.Name = param.ChildName ?? string.Empty;
                                        child.PlaceOfBirth = "";
                                        child.Birthdate = childBOD;
                                        child.Inserted(by);
                                    }
                                }
                            }
                            else
                            {
                                var child = new Child();
                                child.ParentID = dataMember.ID;
                                child.Gender = "";
                                child.Name = param.ChildName ?? string.Empty;
                                child.PlaceOfBirth = "";
                                child.Birthdate = childBOD;
                                child.Inserted(by);
                            }
                        }

                        result.Value = dataMember;
                        result.StatusCode = "000";
                        result.StatusMessage = string.Format("success update member with ID {0}", dataMember.ID);

                        transScope.Complete();
                        transScope.Dispose();
                        //}
                        //else
                        //{
                        //    result.Value = dataMember;
                        //    result.StatusCode = "50";
                        //    result.StatusMessage = string.Format(res.ErrorMessage);
                        //}
                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            return result;
        }

        private static ResultModel<Member> ValidateLongRegister(MemberLongRegisterModel param)
        {
            ResultModel<Member> result = new ResultModel<Member>();
            result.StatusCode = "00";
            if (string.IsNullOrEmpty(param.Email))
            {
                result.StatusCode = "20";
                result.StatusMessage = string.Format("Email is required");
                return result;
            }
            else
            {
                if (!EmailHelper.IsValidEmail(param.Email))
                {
                    result.StatusCode = "26";
                    result.StatusMessage = string.Format("Format email is wrong");
                    return result;
                }
            }

            //if (string.IsNullOrEmpty(param.Phone))
            //{
            //    result.StatusCode = "21";
            //    result.StatusMessage = string.Format("Phone is required");
            //    return result;
            //}
            //else
            //{
            //    if (!PhoneHelper.isPhoneValid(param.Phone))
            //    {
            //        result.StatusCode = "27";
            //        result.StatusMessage = string.Format("Format Phone is wrong");
            //        return result;
            //    }
            //}

            //if (string.IsNullOrEmpty(param.Firstname))
            //{
            //    result.StatusCode = "23";
            //    result.StatusMessage = string.Format("FirstName is required");
            //    return result;
            //}

            //if (string.IsNullOrEmpty(param.Lastname))
            //{
            //    result.StatusCode = "25";
            //    result.StatusMessage = string.Format("LastName is required");
            //    return result;
            //}
            return result;
        }

        public static ResultModel<string> GetEmailByForgotPasswordToken(EmailByForgotPasswordTokenParamModel param)
        {
            ResultModel<string> result = new ResultModel<string>
            {
                StatusCode = "10",
                StatusMessage = string.Format("No member found with token {0}", param.Token),
                Value = null
            };
            result.StatusCode = "10";
            var memberByForgetPasswordToken = Member.GetByForgotPasswordToken(param.Token);
            if (memberByForgetPasswordToken != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("Member found with token {0}", param.Token);
                result.Value = memberByForgetPasswordToken.Email;
            }
            return result;
        }

        public static ResultModel<bool> GenerateForgotPasswordToken(ForgotPasswordByTokenModel param)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            bool isValidPhone = PhoneHelper.isPhoneValid(param.PhoneNumber);
            bool isValidEmail = EmailHelper.IsValidEmail(param.Email);

            if (isValidPhone && isValidEmail)
            {
                if (param.Email.Contains(param.PhoneNumber))
                    isValidEmail = false;
                else
                    isValidEmail = true;
            }

            var dtaMember = Member.GetByPhoneOrEmail(param.PhoneNumber, param.Email);

            if (dtaMember != null)
            {
                TokenValidation data = new TokenValidation();
                data.UniqueCode = StringHelper.CreateRandomString(8, "abcdefghijklmnopqrstuvwxyz0123456789");
                data.DataValue = dtaMember.ID.ToString();
                data.Type = "ForgotPassword";
                data.Status = "created";
                data.Insert("Admin");

                DateTime expireDateLink = DateTime.Now.AddHours(SiteSetting.ForgotPasswordLinkExpiredInHour);
                string uid = string.Format("{0}-{1}", data.UniqueCode, expireDateLink.Ticks);
                string uidEncrypt = Encrypt.EncryptTripleDES(uid);
                string urlLong = SiteSetting.VerifyForgotPaswordURL + uidEncrypt;
                string shortenErrMsg = "";
                //dkomen dulu sementara bitlynya, mario
                //string urlShort = BitlyHelper.ShortenBitlyUsingAPiKey(urlLong, out shortenErrMsg);
                string urlShort = urlLong;
                if (!string.IsNullOrWhiteSpace(urlShort))
                {
                    if (isValidPhone)
                    {
                        result = SendSMSForgotPassword(param.PhoneNumber, urlShort);
                    }
                    else if (isValidEmail)
                    {
                        result = SendEmailForgotPassword(param.Email, urlShort);
                    }
                    else
                    {
                        result.StatusCode = "10";
                        result.StatusMessage = "Invalid email or phone number";
                        result.Value = false;
                    }
                }
                else
                {
                    result.StatusCode = "10";
                    result.StatusMessage = "Failed minify token : " + shortenErrMsg;
                    result.Value = false;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Member not found in system";
                result.Value = false;
            }
            return result;
        }

        public static ResultModel<bool> VerifyForgotPasswordToken(string token)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            if (!string.IsNullOrWhiteSpace(token))
            {
                string tokenDecrypt = Encrypt.DecryptTripleDES(token);
                string[] arrToken = tokenDecrypt.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                if (arrToken != null && arrToken.Count() == 2)
                {
                    string uniqueCode = arrToken[0];
                    string strExpire = arrToken[1];
                    long tickDateTime = 0; long.TryParse(strExpire, out tickDateTime);
                    if (tickDateTime > 0)
                    {
                        DateTime dtExpire = new DateTime(tickDateTime);
                        if (dtExpire >= DateTime.Now)
                        {
                            TokenValidation dtaToken = TokenValidation.GetByCodeAndType(uniqueCode, "ForgotPassword");
                            if (dtaToken != null)
                            {
                                if (dtaToken.Status == "created")
                                {
                                    result.StatusCode = "00";
                                    result.StatusMessage = "Token is valid";
                                    result.Value = true;
                                }
                                else
                                {
                                    result.StatusCode = "10";
                                    result.StatusMessage = "Token status is not valid";
                                    result.Value = false;
                                }
                            }
                            else
                            {
                                result.StatusCode = "10";
                                result.StatusMessage = "Token is not found in system";
                                result.Value = false;
                            }
                        }
                        else
                        {
                            result.StatusCode = "10";
                            result.StatusMessage = "Token time is expired";
                            result.Value = false;
                        }
                    }
                    else
                    {
                        result.StatusCode = "10";
                        result.StatusMessage = "Token time is invalid";
                        result.Value = false;
                    }
                }
                else
                {
                    result.StatusCode = "10";
                    result.StatusMessage = "Token is invalid";
                    result.Value = false;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Token is null or empty";
                result.Value = false;
            }
            return result;
        }

        public static ResultModel<bool> ChangeResetPassword(ChangeResetPasswordModel param, string bebeUmbracoSecretCode)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            if (param != null)
            {
                result = VerifyForgotPasswordToken(param.Token);
                if (result.StatusCode == "00")
                {
                    string[] arrToken = Encrypt.DecryptTripleDES(param.Token).Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
                    TokenValidation dtaToken = TokenValidation.GetByCodeAndType(arrToken[0], "ForgotPassword");
                    string strMemberID = dtaToken.DataValue;
                    int memberID = 0; int.TryParse(strMemberID, out memberID);
                    DataMember.Member dtaMember = DataMember.Member.GetById(memberID);
                    if (dtaMember != null)
                    {
                        if (dtaMember.IsActive)
                        {
                            if (!string.IsNullOrWhiteSpace(param.NewPassword) && !string.IsNullOrWhiteSpace(param.ConfirmPassword))
                            {
                                if (param.NewPassword == param.ConfirmPassword)
                                {
                                    dtaMember.Password = Encrypt.LoyalEncript(param.NewPassword);
                                    var memResp = dtaMember.Updated(dtaMember.FirstName + " " + dtaMember.LastName);
                                    if (memResp.Success)
                                    {
                                        dtaToken.Status = "done";
                                        dtaToken.Update(dtaMember.FirstName + " " + dtaMember.LastName);

                                        // LogOff
                                        var resultLogOff = LogOff(dtaMember.ID);

                                        result.StatusCode = "00";
                                        result.StatusMessage = "Success change password";
                                        result.Value = true;
                                    }
                                    else
                                    {
                                        result.StatusCode = "10";
                                        result.StatusMessage = "Failed update password in system";
                                        result.Value = false;
                                    }
                                }
                                else
                                {
                                    result.StatusCode = "10";
                                    result.StatusMessage = "New password and confirm password is different";
                                    result.Value = false;
                                }
                            }
                            else
                            {
                                result.StatusCode = "10";
                                result.StatusMessage = "New password or confirm password is empty";
                                result.Value = false;
                            }
                        }
                        else
                        {
                            result.StatusCode = "10";
                            result.StatusMessage = "Data member is not active";
                            result.Value = false;
                        }
                    }
                    else
                    {
                        result.StatusCode = "10";
                        result.StatusMessage = "Data member not found in system";
                        result.Value = false;
                    }
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is null";
                result.Value = false;
            }
            return result;
        }

        private static ResultModel<bool> SendSMSForgotPassword(string phoneNumber, string urlShort)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            bool isSuccess = false;
            string templateText = SiteSetting.ForgotPasswordSmsText;
            string paramMessage = SiteSetting.FormatMessageLixus;
            string channelToLixus = SiteSetting.ChannelForgotPassword;
            string smsParam = string.Format(paramMessage, string.Format(templateText, urlShort), phoneNumber, channelToLixus);
            string errSMSMsg = "";
            string smsType = LogSendType.ForgetPassword.Value;

            try
            {
                var smsResponse = BlastSMS.SendSMSNotifOTP(smsType, SiteSetting.ApiSendOTP, phoneNumber, smsParam, phoneNumber, SiteSetting.ApiSendOTP, 0, channelToLixus);
                if (smsResponse != null)
                {
                    if(!string.IsNullOrEmpty(smsResponse.Code))
                    {
                        if (smsResponse.Code.ToString() == "7702")
                            isSuccess = true;
                        else
                            errSMSMsg = SiteSetting.telcomnetAPIURLBebe + " " + smsResponse.Code;
                    }
                    else
                    {
                        errSMSMsg = JsonConvert.SerializeObject(smsResponse);
                        var message = smsResponse.MessageOTP == null ? smsResponse.MessageNonOTP : smsResponse.MessageOTP;

                        if (message.Contains("sent"))
                        {
                            isSuccess = true;
                        }
                    }
                }
                else
                    errSMSMsg = "No response from sms API";
            }
            catch (Exception ex)
            {
                errSMSMsg = (ex.InnerException == null) ? ex.Message : ex.InnerException.Message;
            }

            if (isSuccess)
            {
                result.StatusCode = "00";
                result.StatusMessage = errSMSMsg;
                result.Value = true;
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = errSMSMsg;
            }

            return result;
        }

        private static ResultModel<bool> SendEmailForgotPassword(string email, string urlShort)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                string templateEmail = EmailHelper.GetEmailTemplate(SiteSetting.EmailForgotPasswordTemplateURL);
                string emailMsg = "";
                emailMsg = templateEmail.Replace("[URLSHORT]", urlShort);
                string siteEmail = Data.Settings.GetValueByKey(SiteSetting.SiteEmail);
                string smtpNameDisp = Data.Settings.GetValueByKey(SiteSetting.SMTP_NameDisplay);
                string smtpHost = Data.Settings.GetValueByKey(SiteSetting.SMTPHost);
                bool smtpSSL = false; bool.TryParse(Data.Settings.GetValueByKey(SiteSetting.SMTPSSL), out smtpSSL);
                string smtpUserName = Data.Settings.GetValueByKey(SiteSetting.SMTPUsername);
                string smtpPassword = Data.Settings.GetValueByKey(SiteSetting.SMTPPassword);
                int smtpPort = 0; int.TryParse(Data.Settings.GetValueByKey(SiteSetting.SMTPPort), out smtpPort);

                EmailHelper.SendEmail("Bebelac Forgot Password", emailMsg, email, siteEmail, smtpNameDisp,
                    smtpHost, smtpSSL, smtpUserName, smtpPassword, smtpPort);

                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.Value = true;
            }
            catch (Exception ex)
            {
                result.StatusCode = "10";
                result.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResultModel<bool> SetForgotPasswordTokenByEmail(SetForgotPasswordTokenByEmailParamModel param)
        {
            ResultModel<bool> result = new ResultModel<bool>
            {
                StatusCode = "10",
                StatusMessage = string.Format("No member found with email {0}", param.Email),
                Value = false
            };
            result.StatusCode = "10";
            var member = Member.GetByEmail(param.Email);
            if (member != null)
            {
                member.ForgetPasswordToken = param.Token;
                var dbResult = member.Updated(member.Email);
                if (!dbResult.Success)
                {
                    result.StatusCode = "50";
                    result.StatusMessage = dbResult.ErrorEntity + "|" + dbResult.ErrorMessage;
                }
                else
                {
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("Member found with id {0}", param.Token);
                    result.Value = true;
                }
            }
            return result;
        }

        public static bool IsMemberChildBelow1YearOrPregnant(int memberId)
        {
            var memberObj = DataMember.Member.GetById(memberId);
            var firstChild = DataMember.Child.GetByParentId(memberId).OrderBy(x => x.Order).FirstOrDefault();
            if (firstChild != null)
            {
                if (firstChild.Birthdate != null)
                    return ((DateTime.Today.Subtract(firstChild.Birthdate.Value)).TotalDays / 365) < 1;
            }
            else
            {
                //cek hamil
                if (memberObj != null)
                {
                    if (string.IsNullOrEmpty(memberObj.UsiaKehamilan))
                    {
                        return false;
                    }
                }


            }

            return true;
        }

        public static int CheckMemberChildBelow1YearOrPregnant(int memberId)
        {
            var memberObj = DataMember.Member.GetById(memberId);
            var firstChild = DataMember.Child.GetByParentId(memberId).OrderBy(x => x.Order).FirstOrDefault();

            if (firstChild != null)
            {
                if (firstChild.Birthdate == null)
                {
                    return 1;
                }

                //if (firstChild.Birthdate > DateTime.MinValue)
                //{
                //    if (((DateTime.Today.Subtract(firstChild.Birthdate.Value)).TotalDays / 365) < 1)
                //    {
                //        return 2;
                //    }
                //}
            }
            else
            {
                if (memberObj != null)
                {
                    if (!string.IsNullOrEmpty(memberObj.UsiaKehamilan))
                    {
                        return 3;
                    }
                }
            }

            return 4;
        }

        public static string ReceiptRejectedMessage(int responseCode)
        {
            if (responseCode == 1)
            {
                return SiteSetting.BlankChildBirthDateRuleValidationMessage;
            }
            else if (responseCode == 2)
            {
                return SiteSetting.ChildAgeBelow1YearRuleValidationMessage;
            }
            else
            {
                return SiteSetting.InPregnancyPeriodRuleValidationMessage;
            }
        }

        public static ResultPaginationModel<List<MemberGridModel>> PublicGetAll(string search, string orderBy, string orderDirection, int page = 1, int pageSize = 10)
        {
            ResultPaginationModel<List<MemberGridModel>> result = new ResultPaginationModel<List<MemberGridModel>>();
            try
            {
                orderBy = orderBy ?? "FirstName";
                orderDirection = orderDirection ?? "ASC";

                var rawMember = Member.GetAll().OrderBy(string.Format("{0} {1}", orderBy, orderDirection));
                if (!string.IsNullOrWhiteSpace(search) && !string.IsNullOrEmpty(search))
                {
                    rawMember = rawMember
                        .Where(x => x.FirstName.Contains(search) || x.LastName.Contains(search) || x.Email.Contains(search) || x.Phone.Contains(search) || (!string.IsNullOrEmpty(x.SMSID) && x.SMSID.Contains(search)) || x.Channel.Name.Contains(search))
                        .OrderBy(string.Format("{0} {1}", orderBy, orderDirection));
                }

                result.StatusCode = "00";
                result.StatusMessage = "Success";
                result.TotalRow = rawMember.Count();
                result.Value = rawMember
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .Select(x => new MemberGridModel
                    {
                        ID = x.ID,
                        Gender = x.Gender.ToLower() == "m" ? "Male" : "Female",
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Phone = x.Phone,
                        IsBlacklist = x.IsBlacklist,
                        Point = x.MemberPoint,
                        CreatedDate = x.CreatedDate,
                        Type = !string.IsNullOrEmpty(x.SMSID) ? "Sms" : "Web",
                        Channel = x.Channel.Name
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                result.StatusCode = "50";
                result.StatusMessage = ex.Message;
            }
            return result;
        }

        public static ResultModel<string> MemberOptOut(string phoneNumber)
        {
            ResultModel<string> result = new ResultModel<string>();
            var resultText = Data.Settings.GetByKey("MemberOptOutText");
            try
            {
                if (!string.IsNullOrEmpty(phoneNumber))
                {
                    var member = Member.GetByPhone(phoneNumber);
                    if (member != null)
                    {
                        var memberResponse = member.Delete("System by SMS");
                        if (memberResponse.Success)
                        {
                            var member2 = Data.Member.GetById(member.ID);
                            if (member2 != null)
                            {
                                var memberResponse2 = member2.Delete("System by SMS");
                                if (memberResponse2.Success)
                                {
                                    result.StatusCode = "00";
                                    result.Value = resultText != null ? resultText.Value : "";
                                    result.StatusMessage = "Member is deleted Successfully";
                                    return result;
                                }
                                else
                                {
                                    member.IsDeleted = false;
                                    memberResponse = member.Updated("System by SMS");
                                    if (memberResponse.Success)
                                    {
                                        result.StatusCode = "10";
                                        result.Value = "";
                                        result.StatusMessage = "Member delete failed";
                                        return result;
                                    }
                                }
                            }
                            else
                            {
                                result.StatusCode = "00";
                                result.Value = resultText != null ? resultText.Value : "";
                                result.StatusMessage = "Member is deleted Successfully";
                                return result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.Value = "Exception error occured";
                result.StatusMessage = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                return result;
            }
            result.StatusCode = "10";
            result.StatusMessage = "Member delete failed";
            result.Value = "";
            return result;
        }

        private static int CountReceipt(int memberId)
        {
            var receiptObj = Data.Receipt.GetByMemberId(memberId);
            int result = 0;

            if (receiptObj != null)
            {
                result = receiptObj.Count();
            }

            return result;
        }

        public static ResultModel<MemberModel> UpdateMemberRatingDate(int MemberID, EditRatingDateModel param)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();
            try
            {
                var dataMember = Member.GetById(MemberID);
                if (dataMember != null)
                {
                    var by = dataMember.FirstName + " " + dataMember.LastName;

                    if (dataMember.RatingDate == null)
                    {
                        if (param.IsRated)
                        {
                            dataMember.RatingDate = DateTime.Now; // DateTime.Now;
                        }
                    }
                    else
                    {
                        if (!param.IsRated)
                        {
                            dataMember.RatingDate = null;
                        }
                    }

                    var resultEF = dataMember.Updated(by);
                    if (resultEF.Success)
                    {
                        result.Value = new MemberModel(dataMember);
                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("Success update the AcceptDate for a member with ID {0}", MemberID);
                    }
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("Id {0} not found in system", MemberID);
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Get Detail information of member with spesific ID include all relation table
        /// </summary>
        /// <param name="Phone No"></param>
        /// <returns></returns>
        public static ResultModel<MemberDetailModel> GetMemberModelByPhoneNo(string phoneNo)
        {
            ResultModel<MemberDetailModel> result = new ResultModel<MemberDetailModel>();
            var dataMember = DataMember.Member.GetByPhone(phoneNo);
            if (dataMember != null)
            {
                result.StatusCode = "00";
                result.StatusMessage = string.Format("success get member with phone {0}", phoneNo);
                result.Value = new MemberDetailModel(dataMember);
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = string.Format("member with phone {0} not found in system", phoneNo);
            }



            return result;
        }
        public static ResultModel<List<MemberModel>> GetMemberModelByPeriod(MemberParameterModel param)
        {
            ResultModel<List<MemberModel>> result = new ResultModel<List<MemberModel>>();

            if (param != null)
            {
                try
                {

                    DateTime tempStartDate = DateTime.MinValue,
                    tempEndDate = DateTime.MinValue;


                    if (string.IsNullOrEmpty(param.StartDate) || string.IsNullOrEmpty(param.EndDate))
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("start_date and end_date is required");
                        return result;
                    }


                    bool isValidStartDate = DateTime.TryParseExact(param.StartDate, "yyyy-MM-dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempStartDate);
                    bool isValidEndDate = DateTime.TryParseExact(param.EndDate, "yyyy-MM-dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out tempEndDate);
                    string dateNow = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    if (!isValidStartDate || !isValidEndDate)
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("start_date or end_date is not a valid DATE.[Valid date format: yyyy-MM-dd (example:{0})]", dateNow);
                        return result;
                    }

                    var membersLinq = Member.GetAll();

                    membersLinq = membersLinq
                                      .Where(x => EntityFunctions.TruncateTime(x.UpdatedDate) >= EntityFunctions.TruncateTime(tempStartDate) &&
                                                  EntityFunctions.TruncateTime(x.UpdatedDate) <= EntityFunctions.TruncateTime(tempEndDate));


                    if (param.MemberId != null)
                    {
                        membersLinq = membersLinq
                                           .Where(x => x.ID == param.MemberId);
                    }

                    if (!string.IsNullOrEmpty(param.Phone))
                    {
                        membersLinq = membersLinq
                                           .Where(x => x.Phone == param.Phone);
                    }

                    if (param.IsApprovedKKImage != null)
                    {
                        //bool? IsApprovedKKImage = param.IsApprovedKKImage == 0 ? (bool?)null : param.IsApprovedKKImage==1 ? true : false ;

                        //membersLinq = membersLinq
                        //                  .Where(x => Equals(x.IsApprovedKKImage,IsApprovedKKImage));
                        membersLinq = membersLinq
                                           .Where(x => param.IsApprovedKKImage == 0 ? x.IsApprovedKKImage == null : param.IsApprovedKKImage == 1 ? x.IsApprovedKKImage == true : x.IsApprovedKKImage == false);
                    }


                    var members = membersLinq.ToList();

                    if (members.Count > 0)
                    {
                        result.Value = members.Translated();

                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("success get member with these parameter");
                    }
                    else
                    {
                        result.StatusCode = "50";
                        result.StatusMessage = string.Format("member with these parameter not found in system");
                    }


                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static ResultModel<bool> UpdateMemberActive(string phone)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            var member = Member.GetByPhone(phone);

            return UpdateMemberActive(member);
        }

        public static ResultModel<bool> UpdateMemberActive(int MemberID, string ClientID)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            var pool = Data.Pool.GetByClientId(ClientID);
            var member = Member.GetById(MemberID);

            return UpdateMemberActive(member);
        }

        public static ResultModel<bool> UpdateMemberActive(Member member)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            if (member != null)
            {
                member.IsActive = true;
                member.ActiveDate = DateTime.Now;
                member.ActiveBy = member.FirstName + " " + member.LastName;
                var memberresponse = member.Updated(member.ActiveBy);
                if (memberresponse.Success)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Success Update Active";
                    result.Value = true;
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Failed to update active";
                    result.Value = false;
                }
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = "Member not found";
                result.Value = false;
            }

            return result;
        }

        public static ResultModel<List<AddChildParameterModel>> ValidateUpdateMultipleChild(List<AddChildParameterModel> listChilds)
        {
            ResultModel<List<AddChildParameterModel>> result = new ResultModel<List<AddChildParameterModel>>();

            foreach (var item in listChilds)
            {
                if (String.IsNullOrEmpty(item.Name) || String.IsNullOrWhiteSpace(item.Name))
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Nama anak harus di isi.";
                    result.Value = null;
                    return result;
                }

                if (String.IsNullOrEmpty(item.Birthdate))
                {
                    result.StatusCode = "51";
                    result.StatusMessage = "Tanggal lahir anak harus di isi.";
                    result.Value = null;
                    return result;
                }

                DateTime dtChild = DateTime.MinValue;
                if (!DateTime.TryParseExact(item.Birthdate.Replace("-", "/"), "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dtChild))
                {
                    result.StatusCode = "52";
                    result.StatusMessage = "Format date child is wrong";
                    result.Value = null;
                    return result;
                }
                else
                {
                    if (dtChild <= DateTime.MinValue)
                    {
                        result.StatusCode = "52";
                        result.StatusMessage = "Format date child is wrong";
                        result.Value = null;
                        return result;
                    }
                    else
                    {
                        item.BirthdateChild = dtChild;
                    }
                }
            }

            result.StatusCode = "00";
            result.StatusMessage = "";
            result.Value = listChilds;
            return result;
        }

        public static ResultModel<bool> UpdateChildNameAndChildBirthDate(int memberId, int childId, DateTime childBirthDate, string childName, string by)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            EFResponse childResponse = new EFResponse();

            Child firstChilds = Child.GetById(childId);
            bool isNewChild = false;

            // Jika anak belum ada maka insert data baru
            if (firstChilds == null)
            {
                isNewChild = true;
                firstChilds = new Child();
            }

            firstChilds.ParentID = memberId;
            firstChilds.Name = childName ?? string.Empty;
            firstChilds.Birthdate = childBirthDate;
            firstChilds.Gender = "F";
            firstChilds.PlaceOfBirth = string.Empty;
            firstChilds.Order = 0;

            if (!isNewChild)
            {
                childResponse = firstChilds.Updated(by);

                if (childResponse.Success)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Success Update Child";
                    result.Value = true;
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Failed to update Child";
                    result.Value = false;
                }
            }
            else
            {
                childResponse = firstChilds.Inserted(by);

                if (childResponse.Success)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Success Insert Child";
                    result.Value = true;
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Failed to Insert Child";
                    result.Value = false;
                }
            }

            return result;
        }

        private static bool _toChild(int ParentID, List<EditChildParameterModel> Childs, int StageID, string by)  // Proses Child
        {
            var ret = false;
            foreach (EditChildParameterModel item in Childs)
            {
                if (item.ID == 0)
                {
                    //insert baru
                    if (!string.IsNullOrEmpty(item.Name) && !string.IsNullOrEmpty(item.Birthdate))
                    {
                        Child newChild = new Child();
                        DateTime dt = DateTime.MinValue;
                        if (DateTime.TryParseExact(item.Birthdate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            newChild.Birthdate = dt;
                        }
                        newChild.ParentID = item.ParentID;
                        newChild.Name = item.Name ?? string.Empty;
                        newChild.Gender = "F";
                        newChild.PlaceOfBirth = string.Empty;
                        newChild.Order = 0;
                        newChild.ProductBeforeID = item.ProductBeforeId;
                        newChild.Inserted(by);
                    }
                }
                else
                {
                    //cek benerga ada di data dan bener gak anaknya dia
                    Child existChild = Child.GetByIdAndParentId(item.ID, item.ParentID);
                    DateTime dt = DateTime.MinValue;
                    if (existChild != null)
                    {
                        if (DateTime.TryParseExact(item.Birthdate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt))
                        {
                            existChild.Birthdate = dt;
                        }
                        existChild.Name = item.Name ?? string.Empty;
                        existChild.Gender = "F";
                        existChild.PlaceOfBirth = string.Empty;
                        existChild.Order = 0;
                        existChild.ProductBeforeID = item.ProductBeforeId;
                        existChild.Updated(by);
                    }

                }
            }
            return ret;
        }

        private static ResultModel<bool> _pregnant(int StageID, int ProductBeforeID) // Cek Stage dengan ProductBefore
        {
            // Value = false maka memiliki anak
            // Value = true maka hamil
            //StatusCode = 00 Insert ke Database
            //StatusCode = 50 not Insert ke Database
            //StatusCode = 30 perlu validasi Age Pregnant
            //StatusCode = 40 perlu validasi data child
            //StatusCode = 60 perlu validasi Age Pregnant n data child
            var ret = new ResultModel<bool>();
            ret.StatusCode = "50";
            ret.Value = false;

            Stages Stage = Stages.GetById(StageID);
            ProductBefore Product = ProductBefore.GetById(ProductBeforeID);
            switch (Stage.Name.ToLower())
            {
                case "belum hamil":
                    {
                        //kalo stage belum hamil
                        //ProductBefore gak di insert/update
                        break;
                    }
                case "sedang hamil":
                    {
                        //kalo stage sedang hamil
                        //product before di member, sama add age pregnant
                        ret.StatusCode = "30";
                        ret.Value = true;
                        break;
                    }
                case "sedang hamil & memiliki anak":
                    {
                        //kalo stage sedang hamil, punya anak
                        //product before di cek jika dipilih pregnancy maka dimasukan ke member jika tidak masuk ke child
                        ret.Value = true;
                        ret.StatusCode = "65"; //Product utk Anak
                        if (Product != null)
                        {
                            if (Product.IsPregnancyProduct)
                            {
                                ret.StatusCode = "60"; //Product utk Ibu Hamil
                            }
                        }
                        break;
                    }
                case "tidak hamil & memiliki anak":
                    {
                        //KALO STAGE PUNYA ANAK
                        //product before ke child
                        ret.StatusCode = "40";
                        //ret.Value = true;
                        break;
                    }
            }
            return ret;
        }

        private static bool _checkIsPregnant(int StagesID, Member member)
        {
            //              sedang hamil & memiliki anak || tidak hamil & memiliki anak
            //                                          ||||
            //                                           VV
            //                                      sedang hamil    
            //                                      belum hamil
            // True Suksess Validate
            // False Failed Validate
            var ret = true;
            Stages Objstages = Stages.GetById(StagesID);
            if (Objstages.Name.ToLower() == "belum hamil")
            {
                if (StagesID != member.StagesID)
                {
                    ret = false;
                }
            }
            return ret;
        }

        public static ResultModel<List<MemberChildModel>> GetAllChild(int parentID)
        {
            ResultModel<List<MemberChildModel>> result = new ResultModel<List<MemberChildModel>>();
            try
            {
                var children = (from a in Child.GetByParentId(parentID)
                                select new MemberChildModel
                                {
                                    ChildID = a.ID,
                                    ChildName = a.Name
                                }).ToList();
                if (children != null && children.Count > 0)
                {
                    result.StatusCode = "00";
                    result.StatusMessage = "Children data found";
                    result.Value = children;
                }
                else
                {
                    result.StatusCode = "30";
                    result.StatusMessage = "No children data found";
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "30";
                result.StatusMessage = ex.Message;
            }

            return result;
        }

        public static bool CheckMemberIsVerify(int MemberId)
        {

            var memberObj = Member.GetById(MemberId);
            return memberObj.IsActive;
        }

        public static ResultModel<MemberModel> AddUploadKK(UploadKKModel param, string by, int ID)
        {
            ResultModel<MemberModel> result = new ResultModel<MemberModel>();
            //EFResponse response = new EFResponse();
            string err = "";
            var UniqueCode = Data.Number.Generated(Data.Number.Type.MEMB, by);
            string MediaLocalImageDirectoryMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.MediaLocalImageDirectoryMember);
            string RelativePathMember = LoyaltyPointEngine.Data.Settings.GetValueByKey(SiteSetting.RelativePathMember);


            var UploadKKRegisterModel = ValidateUploadKK(param);
            if (UploadKKRegisterModel.StatusCode != "00")
                return result;

            string KKPict = null;
            if (!string.IsNullOrEmpty(param.KKImage))
            {
                KKPict = InsertMemberImage(param.KKImage, UniqueCode, MediaLocalImageDirectoryMember, RelativePathMember, "KKPicture", out err);
            }

            try
            {
                var data = Member.GetById(ID);
                if (data != null)
                {
                    data.KKImage = KKPict;
                    var response = data.Updated(by);
                    if (response.Success)
                    {
                        result.Value = new MemberModel(data);
                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("Success update KK for a member with ID {0}", ID);
                    }
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = string.Format("Failed upload KK for Id {0} to the system", ID);
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;

        }

        private static ResultModel<Member> ValidateUploadKK(UploadKKModel param)
        {
            ResultModel<Member> result = new ResultModel<Member>();
            result.StatusCode = "00";

            if (string.IsNullOrEmpty(param.KKImage))
            {
                result.StatusCode = "21";
                result.StatusMessage = string.Format("File is required");
                return result;
            }

            return result;
        }

        public static ResultModel<bool> DeleteMember(int ID, string BebeUmbracoAPISecretCode, string Url, Data.CMSUser currentUser)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            try
            {
                Member member = Member.GetById(ID);
                if (member != null)
                {
                    var transScopeOptions = new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted };
                    using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.Required, transScopeOptions))
                    {
                        #region Delete OTP
                        var OTPDel = Data.PoolOTPMember.GetAllbyPhoneEx(member.Phone);
                        foreach (var delete in OTPDel)
                        {
                            Data.EFResponse resultDeleteOTP = delete.Delete(currentUser.FullName, Url, currentUser.ID);

                            if (!resultDeleteOTP.Success)
                            {
                                result.Value = false;
                                result.StatusCode = "50";
                                result.StatusMessage = resultDeleteOTP.ErrorEntity;
                                transScope.Dispose();

                                return result;
                            }
                        }
                        #endregion

                        #region "Delete Member in DataMember"
                        var memberResponse = member.Delete(currentUser.FullName);
                        if (!memberResponse.Success)
                        {
                            result.Value = false;
                            result.StatusCode = "50";
                            result.StatusMessage = memberResponse.ErrorEntity;
                            transScope.Dispose();

                            return result;
                        }
                        #endregion

                        #region "Delete Member in PointEngine"
                        var member2 = Data.Member.GetById(ID);
                        if (member2 != null)
                        {
                            var member2Response = member2.Delete(currentUser.FullName, Url, currentUser.ID);
                            if (!member2Response.Success)
                            {
                                result.Value = false;
                                result.StatusCode = "50";
                                result.StatusMessage = member2Response.ErrorEntity;
                                transScope.Dispose();

                                return result;
                            }
                        }
                        #endregion

                        #region "Logout"
                        ResultModel<bool> resultLogOff = LogOff(member.ID);
                        if (!resultLogOff.Value)
                        {
                            result.Value = false;
                            result.StatusCode = resultLogOff.StatusCode;
                            result.StatusMessage = resultLogOff.StatusMessage;
                            transScope.Dispose();

                            return result;
                        }
                        #endregion

                        transScope.Complete();

                        result.Value = true;
                        result.StatusCode = "00";
                        result.StatusMessage = "Success delete member";
                    }
                }
                else
                {
                    result.Value = false;
                    result.StatusCode = "50";
                    result.StatusMessage = "Failed to get Member";
                }
            }
            catch (Exception ex)
            {
                result.Value = false;
                result.StatusCode = "50";
                result.StatusMessage = ex.InnerException != null ? ex.InnerException.ToString() : ex.Message;
            }

            return result;
        }

        public static ResultModel<bool> LogOff(string accessToken)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            List<Data.PoolMemberToken> listToken = new List<Data.PoolMemberToken>();
            Data.PoolMemberToken token = Data.PoolMemberToken.GetByToken(accessToken);
            listToken.Add(token);

            result = LogOff(listToken);

            return result;
        }

        public static ResultModel<bool> LogOff(int memberId)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            List<Data.PoolMemberToken> token = Data.PoolMemberToken.GetByMemberID(memberId).ToList();

            result = LogOff(token);

            return result;
        }

        public static ResultModel<bool> LogOff(List<Data.PoolMemberToken> token)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            try
            {
                if (token == null)
                {
                    result.StatusCode = "40";
                    result.StatusMessage = "Token is not found";
                    result.Value = false;
                    return result;
                }

                if (token != null && token.Count > 0)
                {
                    foreach (var item in token)
                    {
                        item.HardDeleted();
                    }
                }

                result.StatusCode = "00";
                result.StatusMessage = "Succeed";
                result.Value = true;
                return result;
            }
            catch (Exception ex)
            {
                result.StatusCode = "50";
                result.StatusMessage = JsonConvert.SerializeObject(ex);
                result.Value = false;
                return result;
            }
        }
    }

    public static class BlastSMS
    {
        //Function Generate password dgn mencari phone member //by Ucup
        public static ResultModel<string> UPMember(string Phone)  //UPMember = Update Password Member
        {
            string By = "Scheduler";
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                if (!string.IsNullOrEmpty(Phone))
                {
                    if (!PhoneHelper.isPhoneValid(Phone))
                    {
                        result.StatusCode = "404";
                        result.StatusMessage = string.Format("Format nomor handphone yang anda masukkan salah");
                    }
                    else
                    {
                        int Ch = 3;
                        string newPassword = StringHelper.GeneratedPassword();
                        string EncripPassword = Encrypt.LoyalEncript(newPassword);

                        var checkPhone = Member.GetByPhoneChannel(Phone, Ch);
                        if (checkPhone != null)
                        {
                            checkPhone.Password = EncripPassword;
                            var res = checkPhone.Updated(By);

                            result.StatusCode = "00";
                            result.StatusMessage = newPassword;
                        }
                        else
                        {
                            result.StatusCode = "300";
                            result.StatusMessage = "Phone Not found";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.Value = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                result.StatusMessage = result.Value;
            }
            return result;
        }
        //Tambahan By Ucup
        public static dynamic SendSMSNotifRegistration(string smsParam, string phone, string channelToLixus)
        {
            dynamic result = null;
            string urlApi = SiteSetting.telcomnetAPIURLBebe;
            Data.LogSendSMS logSendSMS = new Data.LogSendSMS();
            logSendSMS.SmsType = LogSendType.Scheduler.Value;
            logSendSMS.Url = urlApi;
            logSendSMS.Phone = phone;
            logSendSMS.RequestMessage = smsParam;
            logSendSMS.ChannelToLixus = channelToLixus;

            try
            {
                //result = RESTHelper.Post<dynamic>(SiteSetting.TelcomMateApiUrl, smsParam);
                result = RESTHelper.PostNotJson<dynamic>(urlApi, smsParam);

                if (result.code != null)
                {
                    result.message = urlApi + smsParam + " " + result.code.ToString();
                    logSendSMS.ResponseCode = result.code.ToString();
                }
                else
                {
                    result.message = urlApi + smsParam + " " + result.ToString();
                }

                logSendSMS.ResponseMessage = result.ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.ErrorEntity = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                result.ErrorMessage = result.ErrorEntity;

                logSendSMS.ResponseMessage = result.ErrorEntity;
            }

            logSendSMS.Insert("Scheduler", "Scheduler", 0);

            return result;
        }
        public static ResultModel<string> MemberSMS(List<SchedulerModel> param)
        { ///By Ucup
            ResultModel<string> result = new ResultModel<string>();
            try
            {
                if (param != null && param.Count > 0)
                {
                    foreach (var item in param)
                    {
                        ResultModel<string> remember = UPMember(item.Phone);  ///Cek Member Terdaftar atau belum
                        if (remember.StatusCode == "00")
                        {
                            string smsText = SiteSetting.SuccessRegisterSmsText;
                            if (string.IsNullOrEmpty(smsText))
                                smsText = remember.StatusMessage;
                            //Kirim pesan Password Baru
                            string channelToLixus = SiteSetting.ChannelWeb;
                            string smsParam = string.Format("api_id={0}&access_token={1}&phone={2}&message={3}&channel={4}", SiteSetting.TelcomMateCId, SiteSetting.TelcomMateCToken, item.Phone, string.Format(smsText, item.Phone, remember.StatusMessage), channelToLixus);
                            dynamic smsResponse = SendSMSNotifRegistration(smsParam, item.Phone, channelToLixus);
                            if (smsResponse.code.ToString() == "7702")
                            {
                                result.StatusCode = "00";
                                result.StatusMessage = "Password baru Anda : " + remember.StatusMessage;

                            }
                            else
                            {
                                result.StatusCode = "100";
                                result.StatusMessage = "SendSMS API gagal, Password bar Anda : " + remember.StatusMessage;
                                result.Value = smsResponse.ErrorMessage;
                            }
                        }
                        else
                        {   //Jika belum terdaftar maka akan diregister dgn Memberlogic.Sms
                            AddMemberSMSParameterModel sss = new AddMemberSMSParameterModel();
                            sss.ClientID = "SmsChannel";
                            sss.Name = item.MemberName;
                            sss.Phone = item.Phone;
                            sss.SMSID = "xxxScheduler";

                            var clientData = "Scheduler";
                            string newPassword = "";
                            var ESms = MemberLogic.Sms(sss, clientData, out newPassword);
                            if (ESms.StatusCode == "00")
                            {
                                //Kirim Pesan register sukses{Kirim User dan Password}
                                string channelToLixus = SiteSetting.ChannelWeb;
                                string smsParam = string.Format("api_id={0}&access_token={1}&phone={2}&message={3}&channel={4}", SiteSetting.TelcomMateCId, SiteSetting.TelcomMateCToken, item.Phone, ESms.StatusMessage, channelToLixus);
                                dynamic smsResponse = SendSMSNotifRegistration(smsParam, item.Phone, channelToLixus);
                                result.StatusCode = "00";
                                result.StatusMessage = "Register Member Suksess";
                            }
                            else
                            {
                                result.StatusCode = "100";
                                result.StatusMessage = "Register Gagal dan SMS tidak terkirim, " + ESms.StatusMessage + "kunyuk koe";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.StatusCode = "500";
                result.Value = (ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                result.StatusMessage = result.Value;
            }
            return result;
        }

        public static SMSToLixusModel SendSMSNotifOTP(string smsType, string urlApi, string phone, string smsParam, string Username, string Url, long UserID, string channelToLixus)
        {
            SMSToLixusModel result = null;
            Data.LogSendSMS logSendSMS = new Data.LogSendSMS();
            logSendSMS.SmsType = smsType;
            logSendSMS.Url = urlApi;
            logSendSMS.Phone = phone;
            logSendSMS.RequestMessage = smsParam;
            logSendSMS.ChannelToLixus = channelToLixus;

            try
            {
                result = RESTHelper.PostNotJson<SMSToLixusModel>(urlApi, smsParam);

                if (result.Code != null)
                {
                    logSendSMS.ResponseCode = result.Code.ToString();
                }
                logSendSMS.Transid = result.TransactionIdOTP;
                logSendSMS.ResponseMessage = JsonConvert.SerializeObject(result);
            }
            catch (Exception ex)
            {
                logSendSMS.ResponseMessage = ex.Message;
                throw ex;
            }

            logSendSMS.Insert(Username, Url, UserID);

            return result;
        }
    }
}
