﻿using LoyaltyPointEngine.DataMember;
using LoyaltyPointEngine.Model.Object;
using LoyaltyPointEngine.Model.Object.Notification;
using LoyaltyPointEngine.Model.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using LoyaltyPointEngine.Common.Notification;
using LoyaltyPointEngine.Model.Parameter.Notification;
using System.Globalization;
using Nest;
using LoyaltyPointEngine.Common.ElasticSearch;
using LoyaltyPointEngine.Model.Object.BalanceLog;
using LoyaltyPointEngine.Common;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class NotificationLogic
    {
        public static ResultPaginationModel<List<NotificationModel>> Get(ParamPagination param, int MemberId)
        {
            ResultPaginationModel<List<NotificationModel>> result = new ResultPaginationModel<List<NotificationModel>>();
            if (param != null)
            {
                try
                {
                    if (MemberId <= 0)
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = "MemberId is required";
                        return result;
                    }


                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    var dataNotif = GetNotificationElastic(MemberId, CurrentPage, totalRowPerPage);

                    List<NotificationModel> listModel = new List<NotificationModel>();
                    foreach (var item in dataNotif)
                    {
                        NotificationModel model = new NotificationModel();
                        model.ID = item.NotificationID;
                        model.MemberID = item.MemberID;
                        model.Message = item.Detail;
                        model.Type = item.NotificationType.ToString();
                        model.CreatedDate = item.NotificationDate;
                        model.IsRead = item.IsRead;
                        model.Title = item.Title;
                        model.ReferenceCode = item.ReferenceCode;
                        model.Point = item.Point;
                        //model.LeftIcon = item.LeftIcon;
                        //model.RightIcon = item.RightIcon;
                        //model.LinkUrl = item.SourceData;
                        listModel.Add(model);
                    }

                    result.TotalRow = listModel.Count();
                    result.StatusCode = "00";
                    result.StatusMessage = string.Format("success get {0} notification(s)", listModel.Count);
                    result.Value = listModel;
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }

            return result;
        }

        public static List<Notification> GetByReferenceCode(string referenceCode, int memberId)
        {
            return Notification.GetByMemberId(memberId).Where(x => x.ReferenceCode == referenceCode).ToList();
        }

        public static ResultModel<int> TotalUnread(int MemberId)
        {
            ResultModel<int> result = new ResultModel<int>();

            result.StatusCode = "00";
            result.StatusMessage = string.Format("success get total notification(s) of member {0}", MemberId);
            //result.Value = Notification.GetTotalUnreadNotificationByMemberId(MemberId);
            result.Value = Convert.ToInt32(GetNotificationElasticCount(MemberId));

            return result;
        }

        /// <summary>
        /// Update notification to be Read
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static ResultModel<NotificationModel> Read(string Id, int MemberId, string by)
        {
            ResultModel<NotificationModel> result = new ResultModel<NotificationModel>();

            Guid NotifId = Guid.Empty;

            if (Guid.TryParse(Id, out NotifId))
            {
                var data = Notification.GetById(NotifId);
                if (data != null)
                {
                    if (data.MemberID == MemberId)
                    {
                        if (!data.IsRead)
                        {
                            data.IsRead = true;
                            data.Updated(by);
                            result.StatusCode = "00";
                            result.StatusMessage = string.Format("successfully updating notif be read");
                            result.Value = new NotificationModel(data);
                        }
                        else
                        {
                            result.StatusCode = "40";
                            result.StatusMessage = string.Format("Notifications have been read by the member", MemberId);
                        }
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.StatusMessage = string.Format("Notification's not owned by this member", MemberId);
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = "Notification's Id not found in the system";
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Notification's Id does not match the format";
            }

            return result;

        }

        public static ResultModel<List<NotificationModel>> Reads(string Ids, int MemberId, string by)
        {
            ResultModel<List<NotificationModel>> result = new ResultModel<List<NotificationModel>>();
            result.Value = new List<NotificationModel>();

            foreach (string Id in Ids.Split(','))
            {
                Guid NotifId = Guid.Empty;
                if (Guid.TryParse(Id, out NotifId))
                {
                    var data = Notification.GetById(NotifId);
                    if (data != null)
                    {
                        if (data.MemberID == MemberId)
                        {
                            if (!data.IsRead)
                            {
                                data.IsRead = true;
                                data.Updated(by);
                                result.StatusCode = "00";
                                result.StatusMessage = string.Format("successfully updating notif be read");
                                result.Value.Add(new NotificationModel(data));
                            }
                            else
                            {
                                result.StatusCode = "40";
                                result.StatusMessage = string.Format("Notifications have been read by the member", MemberId);
                            }
                        }
                        else
                        {
                            result.StatusCode = "30";
                            result.StatusMessage = string.Format("Notification's not owned by this member", MemberId);
                        }
                    }
                    else
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = "Notification's Id not found in the system";
                    }
                }
                else
                {
                    result.StatusCode = "10";
                    result.StatusMessage = "Notification's Id does not match the format";
                }
            }
            return result;
        }

        public static ResultModel<NotificationModel> Create(int MemberID, NotificationType.Type type, string Title, string Message, string ReferenceCode, int Point, string by, string LeftIcon = "", string RightIcon = "")
        {
            ResultModel<NotificationModel> result = new ResultModel<NotificationModel>();

            if (MemberID > 0)
            {
                if (!string.IsNullOrEmpty(Message))
                {

                    if (!string.IsNullOrEmpty(Title))
                    {
                        if (!string.IsNullOrEmpty(by))
                        {
                            Notification notifNew = new Notification();
                            notifNew.ID = Guid.NewGuid();
                            notifNew.Type = type.ToString();
                            notifNew.MemberID = MemberID;
                            notifNew.Message = Message;
                            notifNew.Title = Title;
                            notifNew.ReferenceCode = ReferenceCode;
                            if (Point > 0)
                            {
                                notifNew.Point = Point;
                            }

                            var EFResponse = notifNew.Inserted(by);
                            if (EFResponse != null && EFResponse.Success)
                            {
                                try
                                {
                                    AppendNewData(MemberID, new ParamCreateNotification
                                    {
                                        ID = notifNew.ID,
                                        Type = notifNew.Type,
                                        Message = notifNew.Message,
                                        Title = notifNew.Title,
                                        ReferenceCode = notifNew.ReferenceCode,
                                        Point = notifNew.Point ?? 0
                                    });
                                }
                                catch (Exception)
                                {

                                }

                                result.StatusCode = "00";
                                result.StatusMessage = string.Format("successfully Created notif");
                                result.Value = new NotificationModel(notifNew);

                                List<Data.PoolMemberToken> androidMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(MemberID, "ANDROID"),
                                    iosMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(MemberID, "IOS");

                                if (androidMobileTokens != null && androidMobileTokens.Count > 0)
                                {
                                    PushNotification.SendAndroid(androidMobileTokens.Select(x => x.DeviceID).ToArray(), Title, Message, LeftIcon, RightIcon);
                                }
                                if (iosMobileTokens != null && iosMobileTokens.Count > 0)
                                {
                                    PushNotification.SendIOS(iosMobileTokens.Select(x => x.DeviceID).ToArray(), Title, Message, 1);
                                }
                            }
                            else
                            {
                                result.StatusCode = "500";
                                result.StatusMessage = string.Format("Error when insert new notif in Database: {0}", EFResponse.ErrorEntity);
                            }

                        }
                        else
                        {
                            result.StatusCode = "10";
                            result.StatusMessage = string.Format("CreatedBy is required");
                        }
                    }
                    else
                    {
                        result.StatusCode = "40";
                        result.StatusMessage = string.Format("Title is required");
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = string.Format("Parameter Message is required");
                }
            }
            else
            {
                result.StatusCode = "30";
                result.StatusMessage = string.Format("MemberID is required");
            }



            return result;
        }

        public static ResultModel<NotificationModel> Create(ParamCreateNotification param, int MemberID)
        {
            ResultModel<NotificationModel> result = new ResultModel<NotificationModel>();
            if (MemberID > 0)
            {
                var memberData = Member.GetById(MemberID);

                if (!string.IsNullOrEmpty(param.Message))
                {
                    if (memberData != null)
                    {
                        string by = memberData.FirstName;

                        var listEnum = Enum.GetValues(typeof(NotificationType.Type))
                            .Cast<NotificationType.Type>()
                            .Select(v => v.ToString())
                            .ToList();

                        if (!string.IsNullOrEmpty(param.Type))
                        {
                            NotificationType.Type _myType;
                            if (Enum.TryParse<NotificationType.Type>(param.Type, out _myType))
                            {
                                if (!string.IsNullOrEmpty(param.Title))
                                {
                                    Notification notifNew = new Notification();
                                    notifNew.ID = Guid.NewGuid();
                                    notifNew.Type = _myType.ToString();
                                    notifNew.MemberID = MemberID;
                                    notifNew.Message = param.Message;
                                    notifNew.Title = param.Title;
                                    notifNew.ReferenceCode = param.ReferenceCode;
                                    if (param.Point > 0)
                                    {
                                        notifNew.Point = param.Point;
                                    }

                                    var EFResponse = notifNew.Inserted(by);
                                    if (EFResponse != null && EFResponse.Success)
                                    {
                                        if (!string.IsNullOrEmpty(param.SourceData) && param.SourceData.ToLower().Equals("redeem"))
                                        {
                                            try
                                            {
                                                param.ID = notifNew.ID;
                                                AppendNewData(MemberID, param);
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        result.StatusCode = "00";
                                        result.StatusMessage = string.Format("successfully Created notif");
                                        result.Value = new NotificationModel(notifNew);

                                        List<Data.PoolMemberToken> androidMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(MemberID, "ANDROID"),
                                        iosMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(MemberID, "IOS");

                                        if (androidMobileTokens != null && androidMobileTokens.Count > 0)
                                        {
                                            PushNotification.SendAndroid(androidMobileTokens.Select(x => x.DeviceID).ToArray(), param.Title, param.Message, param.LeftIcon, param.RightIcon);
                                        }
                                        if (iosMobileTokens != null && iosMobileTokens.Count > 0)
                                        {
                                            PushNotification.SendIOS(iosMobileTokens.Select(x => x.DeviceID).ToArray(), param.Title, param.Message, 1);
                                        }
                                    }
                                    else
                                    {
                                        result.StatusCode = "500";
                                        result.StatusMessage = string.Format("Error when insert new notif in Database: {0}", EFResponse.ErrorEntity);
                                    }
                                }
                                else
                                {
                                    result.StatusCode = "42";
                                    result.StatusMessage = string.Format("Title is required");
                                }

                            }
                            else
                            {
                                result.StatusCode = "41";
                                result.StatusMessage = string.Format("Type is not register in system. {0} ",
                                    listEnum != null ? ("( Please using one of these: " + string.Join(",", listEnum) + " )") : "");
                            }
                        }
                        else
                        {
                            result.StatusCode = "40";
                            result.StatusMessage = string.Format("Type is required");
                        }


                    }
                    else
                    {
                        result.StatusCode = "10";
                        result.StatusMessage = string.Format("Member ID not found in system");
                    }
                }
                else
                {
                    result.StatusCode = "20";
                    result.StatusMessage = string.Format("Parameter Message is required");
                }
            }
            else
            {
                result.StatusCode = "30";
                result.StatusMessage = string.Format("MemberID is required");
            }

            return result;
        }

        public static ResultPaginationModel<List<FusionNotificationModel>> GetFusionNotification(ParamPaginationWithDate param, int MemberID)
        {
            ResultPaginationModel<List<FusionNotificationModel>> result = new ResultPaginationModel<List<FusionNotificationModel>>();
            if (param != null)
            {
                try
                {
                    if (MemberID <= 0)
                    {
                        result.StatusCode = "20";
                        result.StatusMessage = "MemberId is required";
                        return result;
                    }

                    DateTime startDate = DateTime.MinValue;
                    if (!DateTime.TryParseExact(param.StartDate + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out startDate))
                    {
                        result.StatusCode = "21";
                        result.StatusMessage = "StartDate is required";
                        return result;
                    }

                    DateTime endDate = DateTime.MinValue;
                    if (!DateTime.TryParseExact(param.EndDate + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out endDate))
                    {
                        result.StatusCode = "22";
                        result.StatusMessage = "EndDate is required";
                        return result;
                    }

                    if (startDate > endDate)
                    {
                        result.StatusCode = "23";
                        result.StatusMessage = "EndDate < StartDate ";
                        return result;
                    }

                    int CurrentPage = param.Page.HasValue ? param.Page.Value : 1;
                    if (CurrentPage <= 0)
                    {
                        CurrentPage = 1;
                    }

                    int totalRowPerPage = param.TotalRowPerPage.HasValue ? param.TotalRowPerPage.Value : 100;

                    //var dtaNotif = DataMember.Notification.GetFusionNotifications(MemberID, startDate, endDate, CurrentPage, totalRowPerPage);
                    var dtaNotif = GetFusionNotificationElastic(MemberID, startDate, endDate, CurrentPage, totalRowPerPage);

                    foreach (var notif in dtaNotif)
                    {
                        notif.NotificationDate = notif.NotificationDate.HasValue ? notif.NotificationDate.Value.AddHours(7) : notif.NotificationDate;
                    }

                    //dtaNotif = dtaNotif.OrderByDescending(x => x.NotificationDate).ToList();

                    if (dtaNotif != null && dtaNotif.Count > 0)
                    {
                        result.StatusCode = "00";
                        //result.TotalRow = (int)dtaNotif.FirstOrDefault().TotalRowCount;
                        //result.Value = dtaNotif.Translated();
                        result.TotalRow = (int)GetFusionNotificationElasticCount(MemberID, startDate, endDate);
                        result.Value = dtaNotif;
                        result.StatusMessage = string.Format("success get {0} notification(s)", dtaNotif.Count);
                    }
                    else
                    {
                        result.StatusCode = "30";
                        result.TotalRow = 0;
                        result.Value = new List<FusionNotificationModel>();
                        result.StatusMessage = "Data Notification is empty";
                    }
                }
                catch (Exception ex)
                {
                    result.StatusCode = "500";
                    result.StatusMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                }
            }
            else
            {
                result.StatusCode = "10";
                result.StatusMessage = "Parameter is required";
            }
            return result;
        }

        public static ResultModel<bool> CreateTelcomate(ParamNotifTelcomate param)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = false;
            if (!string.IsNullOrEmpty(param.MemberPhone))
            {

                var member = DataMember.Member.GetByPhone(param.MemberPhone);
                if (member == null)
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Member Phone not found";
                    return result;
                }

                if (!string.IsNullOrEmpty(param.Message))
                {
                    var Fmt = FormatNotif(param);

                    Notification notifNew = new Notification();
                    notifNew.ID = Guid.NewGuid();
                    notifNew.Message = Fmt.Message;
                    notifNew.MemberID = member.ID;
                    notifNew.Title = Fmt.Title;
                    notifNew.Type = Fmt.Type;
                    notifNew.ReferenceCode = Fmt.ReferenceCode;
                    notifNew.Point = Fmt.Point;
                    notifNew.LinkUrl = Fmt.Url;

                    var EFResponse = notifNew.Inserted(member.FirstName);
                    if (EFResponse != null && EFResponse.Success)
                    {
                        try
                        {
                            AppendNewData(member.ID, new ParamCreateNotification
                            {
                                ID = notifNew.ID,
                                Message = notifNew.Message,
                                Title = notifNew.Title,
                                Type = notifNew.Type,
                                ReferenceCode = notifNew.ReferenceCode,
                                Point = notifNew.Point ?? 0
                            });
                        }
                        catch (Exception)
                        {
                        }

                        result.StatusCode = "00";
                        result.StatusMessage = string.Format("successfully Created notif");
                        result.Value = true;
                        if (string.IsNullOrEmpty(param.Title))
                        {
                            param.Title = Fmt.Message;
                        }
                        if (!string.IsNullOrEmpty(Fmt.Url)) //  Add tracker Telcomate in Firebase Mobile
                        {
                            param.Url = string.Format("{0}?s=m", Fmt.Url);
                        }
                        List<Data.PoolMemberToken> androidMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(member.ID, "ANDROID"),
                        iosMobileTokens = Data.PoolMemberToken.GetLatestsByMemberIDAndDeviceType(member.ID, "IOS");

                        if (androidMobileTokens != null && androidMobileTokens.Count > 0)
                        {
                            PushNotification.SendAndroid(androidMobileTokens.Select(x => x.DeviceID).ToArray(), param.Title, Fmt.Message, param.LeftIcon, param.RightIcon, param.Url);
                        }
                        if (iosMobileTokens != null && iosMobileTokens.Count > 0)
                        {
                            PushNotification.SendIOS(iosMobileTokens.Select(x => x.DeviceID).ToArray(), param.Title, Fmt.Message, 1, param.Url);
                        }
                    }
                    else
                    {
                        result.StatusCode = "500";
                        result.StatusMessage = string.Format("Error when insert new notif in Database: {0}", EFResponse.ErrorEntity);
                    }
                }
                else
                {
                    result.StatusCode = "50";
                    result.StatusMessage = "Message is empty";
                }
            }
            else
            {
                result.StatusCode = "50";
                result.StatusMessage = "Phone is empty";
            }
            return result;
        }

        private static ParamNotifTelcomate FormatNotif(ParamNotifTelcomate param)
        {
            ParamNotifTelcomate result = new ParamNotifTelcomate();
            if (!string.IsNullOrEmpty(param.Title))
            {
                result.Title = param.Title;
            }
            else
            {
                result.Title = string.Empty;
            }
            if (!string.IsNullOrEmpty(param.Url))
            {
                result.Url = param.Url;
                result.Message = param.Message.Replace("[URL]", param.Url);
            }
            else
            {
                result.Url = string.Empty;
                result.Message = param.Message;
            }
            if (string.IsNullOrEmpty(param.Type))
            {
                result.Type = NotificationType.Type.Information.ToString();
            }
            else
            {
                result.Type = param.Type;
            }
            if (string.IsNullOrEmpty(param.ReferenceCode))
            {
                result.ReferenceCode = null;
            }
            else
            {
                result.ReferenceCode = param.ReferenceCode;
            }
            if (param.Point == null)
            {
                result.Point = 0;
            }
            else
            {
                result.Point = param.Point;
            }
            return result;
        }

        private static List<FusionNotificationModel> GetNotificationElastic(int memberId, int currentPage, int totalRowPerPage)
        {
            if (currentPage > 0)
            {
                currentPage = currentPage - 1;
            }
            ElasticConnection elasticConn = new ElasticConnection();

            var data = elasticConn.Client.Search<FusionNotificationModel>(s => s
                .Index(SiteSetting.ElasticNotificationIndex)
                .AllTypes()
                .From(currentPage * totalRowPerPage)
                .Size(totalRowPerPage)
                .Sort(ss => ss
                    .Descending("notificationdate")
                )
                .Query(q => q
                    .Bool(b => b
                        .Filter(
                            f => f.Term(t => t.Field("memberid").Value(memberId.ToString()))
                        )
                    )
                )
                .Sort(ss => ss
                    .Descending("notificationdate")
                )
            );

            return data.Hits.Select(x => x.Source).ToList();
        }

        private static long GetNotificationElasticCount(int memberId)
        {
            ElasticConnection elasticConn = new ElasticConnection();

            var data = elasticConn.Client.Count<FusionNotificationModel>(c => c
                .Index(SiteSetting.ElasticNotificationIndex)
                .AllTypes()
                .Query(q => q
                    .Bool(b => b
                        .Filter(
                            f => f.Term(t => t.Field("memberid").Value(memberId.ToString())),
                            f => f.Term(t => t.Field("isread").Value(false))
                        )
                    )
                )
            );

            return data.Count;
        }

        private static List<FusionNotificationModel> GetFusionNotificationElastic(int memberId, DateTime startDate, DateTime endDate, int currentPage, int totalRowPerPage)
        {
            if (currentPage > 0)
            {
                currentPage = currentPage - 1;
            }
            ElasticConnection elasticConn = new ElasticConnection();

            var data = elasticConn.Client.Search<FusionNotificationModel>(s => s
                .Index(SiteSetting.ElasticNotificationIndex)
                .AllTypes()
                .From(currentPage * totalRowPerPage)
                .Size(totalRowPerPage)
                .Sort(ss => ss
                    .Descending("notificationdate")
                )
                .Query(q => q
                    .Bool(b => b
                        .Filter(
                            f => f.Term(t => t.Field("memberid").Value(memberId.ToString())),
                            f => f.DateRange(dr => dr
                                  .Field("notificationdate")
                                  .GreaterThanOrEquals(startDate)
                                  .LessThanOrEquals(endDate)
                                  .TimeZone("+07:00")
                            )
                        )
                    )
                )
                .Sort(ss => ss
                    .Descending("notificationdate")
                )
            );

            return data.Hits.Select(x => x.Source).ToList();
        }

        private static long GetFusionNotificationElasticCount(int memberId, DateTime startDate, DateTime endDate)
        {
            ElasticConnection elasticConn = new ElasticConnection();

            var data = elasticConn.Client.Count<FusionNotificationModel>(c => c
                .Index(SiteSetting.ElasticNotificationIndex)
                .AllTypes()
                .Query(q => q
                    .Bool(b => b
                        .Filter(
                            f => f.Term(t => t.Field("memberid").Value(memberId.ToString())),
                            f => f.DateRange(dr => dr
                                  .Field("notificationdate")
                                  .GreaterThanOrEquals(startDate)
                                  .LessThanOrEquals(endDate)
                                  .TimeZone("+07:00")
                            )
                        )
                    )
                )
            );

            return data.Count;
        }

        public static IIndexResponse AppendNewData(int memberId, ParamCreateNotification model)
        {
            ElasticConnection elasticConn = new ElasticConnection(Common.SiteSetting.ElasticNotificationIndex);
            var data = new ParamCreateFusionNotification();
            var receiptType = new List<string> { "OnProgress", "Reject", "RejectReupload", "Approved" };
            var otherType = new List<string> { "Information", "Warning", "Event", "Custom" };

            data.NotificationID = model.ID.HasValue ? model.ID.Value.ToString() : Guid.NewGuid().ToString();
            data.MemberID = memberId;
            data.NotificationDate = DateTime.Now;
            if (!string.IsNullOrEmpty(model.Type) && receiptType.Contains(model.Type))
            {
                var receiptData = Data.Receipt.GetByCode(model.ReferenceCode);
                if (receiptData != null)
                {
                    var collectData = Data.Collect.GetAllByReceiptID(receiptData.ID);
                    if (collectData != null)
                    {
                        List<BalanceLogModel> balanceLogData = new List<BalanceLogModel>();
                        foreach (var collect in collectData)
                        {
                            var balanceLog = Data.BalanceLog.GetAll().Where(x => x.ReferenceID == collect.ID && x.Type.ToLower().Equals("C")).FirstOrDefault();
                            balanceLogData.Add(new BalanceLogModel(balanceLog));
                        }
                        if (balanceLogData != null && balanceLogData.Count > 0)
                        {
                            #region Title
                            if (model.Type.Equals("Reject") || model.Type.Equals("RejectReupload"))
                            {
                                data.Title = "Upload struk gagal " + model.ReferenceCode;
                            }
                            else
                            {
                                data.Title = model.ReferenceCode;
                            }
                            #endregion
                            #region SubTitle
                            if (model.Type.Equals("OnProgress"))
                            {
                                data.SubTitle = "Struk sedang diproses";
                            }
                            if (model.Type.Equals("Reject"))
                            {
                                data.SubTitle = "Struk ditolak oleh admin";
                            }
                            if (model.Type.Equals("RejectReupload"))
                            {
                                data.SubTitle = "Struk harus diupload ulang";
                            }
                            if (model.Type.Equals("Approved"))
                            {
                                data.SubTitle = "Struk berhasil diproses";
                            }
                            #endregion
                            #region Detail
                            if (model.Type.Equals("Reject") || model.Type.Equals("RejectReupload"))
                            {
                                data.Detail = receiptData.RejectReason;
                            }
                            else
                            {
                                data.Detail = model.Message;
                            }
                            #endregion
                            data.NotificationType = model.Type;
                            data.ReferenceID = receiptData.ID.ToString();
                            data.ReferenceCode = model.ReferenceCode;
                            data.Point = collectData.Sum(x => x.TotalPoints);
                            #region LastPoint
                            if (model.Type.Equals("Approved"))
                            {
                                data.LastPoint = balanceLogData.Max(x => x.Balance);
                            }
                            else
                            {
                                data.LastPoint = 0;
                            }
                            #endregion
                            data.SourceData = "Receipt";
                        }
                    }
                }
            }
            else if (!string.IsNullOrEmpty(model.SourceData) && model.SourceData.ToLower().Equals("redeem"))
            {
                data.Title = model.FusionTitle;
                data.SubTitle = model.SubTitle;
                data.Detail = model.Detail;
                data.NotificationType = model.Type;
                data.Point = model.Point;
                data.LastPoint = model.LastPoint ?? 0;
                data.LeftIcon = model.LeftIcon;
                data.RightIcon = model.RightIcon;
                data.SourceData = model.SourceData;
            }
            else if (!string.IsNullOrEmpty(model.Type) && model.Type.Equals("Expired"))
            {
                var collectData = Data.Collect.GetAll().Where(x => x.ID.Equals(model.ReferenceCode) && !x.IsDeleted).FirstOrDefault();
                if (collectData != null)
                {
                    var balanceLogData = Data.BalanceLog.GetAll().Where(x => x.ReferenceID.Equals(collectData.ID) && x.Type.Equals("D"));
                    if (balanceLogData != null)
                    {
                        data.Title = model.Title;
                        data.SubTitle = "Point sudah kadaluarsa";
                        data.Detail = model.Message;
                        data.NotificationType = model.Type;
                        data.ReferenceID = collectData.ID.ToString();
                        data.ReferenceCode = model.ReferenceCode;
                        data.Point = model.Point;
                        data.LastPoint = balanceLogData.Min(x => x.Balance);
                        data.LeftIcon = model.LeftIcon;
                        data.RightIcon = model.RightIcon;
                        data.SourceData = "Collect";
                    }
                }
            }
            else if (!string.IsNullOrEmpty(model.Type) && otherType.Contains(model.Type))
            {
                data.Title = model.Title;
                #region SubTitle
                if (model.Type.Equals("Information"))
                    data.SubTitle = "Info";
                if (model.Type.Equals("Warning"))
                    data.SubTitle = "Warning";
                if (model.Type.Equals("Event"))
                    data.SubTitle = "Event";
                if (model.Type.Equals("Custom"))
                    data.SubTitle = "";
                #endregion
                data.Detail = model.Message;
                data.NotificationType = model.Type;
                data.ReferenceCode = model.ReferenceCode;
                data.Point = model.Point;
                data.LastPoint = 0;
                data.LeftIcon = model.LeftIcon;
                data.RightIcon = model.RightIcon;
                data.SourceData = "Other";
            }

            var result = elasticConn.Client.IndexDocument<ParamCreateFusionNotification>(data);
            return result;
        }
    }
}
