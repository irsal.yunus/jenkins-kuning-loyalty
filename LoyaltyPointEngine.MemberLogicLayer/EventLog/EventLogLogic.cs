﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoyaltyPointEngine.Data;
using Newtonsoft.Json;

namespace LoyaltyPointEngine.MemberLogicLayer
{
    public class EventLogLogic
    {
        public static void InsertEventLog(long? UserId, LoyaltyPointEngine.Common.EventLogLogic.LogType logType,
            LoyaltyPointEngine.Common.EventLogLogic.LogSource source, LoyaltyPointEngine.Common.EventLogLogic.LogEventCode eventCode,
            string Message, object Value, string url, string createdBy)
        {
            try
            {
                EventLog log = new EventLog();
                log.ID = Guid.NewGuid();
                log.UserID = UserId;
                log.LogType = logType.ToString();
                log.Source = source.ToString();
                log.EventCode = eventCode.ToString();
                log.Message = Message;
                if (Value != null)
                {
                    log.SourceValue = JsonConvert.SerializeObject(Value);
                }

                log.SourceUrl = url;
                log.Inserted(createdBy);
            }
            catch
            {

            }
        }
    }
}
