﻿using LoyaltyPointEngine.DataMember;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoyaltyPointEngine.MemberLogicLayer.Scheduler
{
    public class SynchMember
    {
        public static void Synchronize(string by, out int totalInserted, out int totalUpdated)
        {
            DateTime dtNow = DateTime.Now;
             totalInserted = SynchInsert(dtNow, by);
             totalUpdated = SynchUpdate(dtNow, by);
        }


        public static int SynchInsert(DateTime dtNow, string by)
        {
            int result = 0;
            var members = Member.GetAllCreatedToday(dtNow).ToList();
            if (members != null)
            {
                foreach (var item in members)
                {
                    var memberSimple = Data.Member.GetById(item.ID);
                    if (memberSimple == null)
                    {
                        memberSimple = new Data.Member();
                        memberSimple.ID = item.ID;
                        memberSimple.Email = item.Email;
                        memberSimple.Name = string.Format("{0} {1}", item.FirstName, item.LastName);
                        memberSimple.Phone = item.Phone;
                        memberSimple.Insert(by, "", null);
                        result++;
                    }
                }
            }


            return result;
        }


        public static int SynchUpdate(DateTime dtNow, string by)
        {
            int result = 0;
            var members = Member.GetAllUpdatedToday(dtNow).ToList();
            if (members != null)
            {
                foreach (var item in members)
                {
                    var memberSimple = Data.Member.GetById(item.ID);
                    if (memberSimple != null)
                    {
                        bool IsUpdated = false;
                        string newName = string.Format("{0} {1}", item.FirstName, item.LastName);
                        if (memberSimple.Name != newName)
                        {
                            memberSimple.Name = newName;
                            IsUpdated = true;
                        }

                        if (memberSimple.Email != item.Email)
                        {
                            memberSimple.Email = item.Email;
                            IsUpdated = true;
                        }

                        if (memberSimple.Phone != item.Phone)
                        {
                            memberSimple.Phone = item.Phone;
                            IsUpdated = true;
                        }
                        if (IsUpdated)
                        {
                            memberSimple.Update(by, "", null);
                            result++;
                        }

                    }
                }
            }
            return result;
        }



    }
}
